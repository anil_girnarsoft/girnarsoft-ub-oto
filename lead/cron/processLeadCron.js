const CronJob = require('cron').CronJob;
const RawLeadLogsMOngo = require(MODULE_PATH+"lead/model/rawLeadMongoModel");
const RawLeadLogs = require(MODULE_PATH+"lead/model/leadRawLogModel");
const LeadLogs = require(MODULE_PATH+"lead/model/leadLogsModel");
const SubSourceModel = require(MODULE_PATH + "source/model/subSourceModel");
const CustomerModel = require(MODULE_PATH + "customer/model/customerModel");
const LeadModel = require(MODULE_PATH + "lead/model/leadModel");
const LeadConversion = require(MODULE_PATH + "lead/model/conversionModel");
const LeadCarModel = require(MODULE_PATH + "lead/model/leadCarModel");
const moment = require("moment");
const dbConfig = require(CONFIG_PATH + 'db.config');
const AppModel = require(COMMON_MODEL + 'appModel');
const MobileLocality = require(MODULE_PATH + "mobile-locality/model/mobileLocality");
const InventoryService    = require(COMMON_MODEL + 'inventoryService');
const DealerService    = require(COMMON_MODEL + 'dealerService');
const CustomerRequirement = require(MODULE_PATH + "customer/model/customerRequirementModel");

//////////////////////////////////tester function open to test//////////////////
// exports.processLeadTest = async (req,res) => {
    
//     const Op = dbConfig.Sequelize.Op;
    
//     let responseData = {}
//     let response = await RawLeadLogs.findAll({
//         where :{
//             pushed_status : '0'
//         },
//         limit : 50
//     })
    
//     if(response && response.length){
       
//         const transaction = await AppModel.dbObj.sequelize.transaction();
//         // ////////// update each record with pushed status of 2;
//         try{
//             const logId = response.map(e=>e.id)
//             await RawLeadLogs.update({
//                 pushed_status : '2'
//             },{
//                 where :{
//                     id : {
//                         [Op.in] : logId
//                     }
//                 },
//                 limit : 50,
//                 transaction : transaction
//             });
//             for(let rawLead of response){
//                 let requestedData = JSON.parse(rawLead.leads_data)
//                 // console.log(requestedData);
//                 requestedData.customer_city = rawLead.city_id;
//                 responseData =await processLead(requestedData,rawLead.source_id,rawLead.sub_source_id,rawLead.id,transaction);
//                 // console.log(responseData);
//             }
//             // await transaction.commit();
//             await transaction.rollback();
//         }catch(error){
//             console.log(error);
//             await transaction.rollback();
//             // throw new Error(error);

//         }
//     }
//     res.json({data:responseData})
// }
exports.processLeadCron = (req, res, time) => new CronJob(time,async function(){
    const Op = dbConfig.Sequelize.Op;
    
    let response = await RawLeadLogs.findAll({
        where :{
            pushed_status : '0'
        },
        limit : 50
    })
    // console.log("allnew records ",response.length);
    if(response && response.length){
       
        const transaction = await AppModel.dbObj.sequelize.transaction();
        // ////////// update each record with pushed status of 2;
        try{
            const logId = response.map(e=>e.id)
            // console.log("all log id",logId);
            await RawLeadLogs.update({
                pushed_status : '2'
            },{
                where :{
                    id : {
                        [Op.in] : logId
                    }
                },
                limit : 50,
                transaction : transaction
            });
            for(let rawLead of response){
                let requestedData = JSON.parse(rawLead.leads_data)
                // console.log(requestedData);
                requestedData.customer_city = rawLead.city_id;
                let responseData = await processLead(req, requestedData,rawLead.source_id,rawLead.sub_source_id,rawLead.id,transaction);
                // console.log(responseData);
            }
            // await transaction.commit();
            await transaction.commit();
        }catch(error){
            await transaction.rollback();
            throw new Error(error);

        }
    }
})
async function processLead(req, leadlogdata,source_id,sub_source_id,logid,transaction){
    
    let querydata = {},customer_mobile = "",customer_id="",response = {},telecomLocation="",lead_id = '',lead_date = "";
    
    if(leadlogdata.querystring){
        querydata = processquerystring(leadlogdata.querystring);
    }

    if(leadlogdata.customer_mobile){
        //////TRIM SPACE OR - 
        customer_mobile = leadlogdata.customer_mobile;
    }
    if(customer_mobile.length<10){
        return {
            status : false,
            message : 'Customer mobile is not valid',
            lead : false
        }
    }
     
    let logdata = {}
    logdata['mobile'] = leadlogdata.customer_mobile;
    logdata['source'] = source_id;
    logdata['source_id'] = source_id;
    logdata['added'] = new Date();
    logdata['data'] = JSON.stringify(leadlogdata);
    RawLeadLogsMOngo.create(logdata);
    ////////////checking customer mobile //////////
    let dnd_check = 0;
    //////////customer mobile field should be without country codes//////////
    let dealer_lead_status = await checkMobileAuthentication(req, customer_mobile,leadlogdata,transaction);
    dnd_check = await checkMobileNDNC(customer_mobile);//// response in 1,2
    // console.log("dealer_lead_status",dealer_lead_status);
    if((dnd_check && dnd_check === 1) && (sub_source_id === 487)){
        response['response'] = {
            status : false,
            msg : "Lead Sub Source in Mobile Intrest and Mobile number in NDNC."
        }
        response['lead_id'] = 0
    }else if(dealer_lead_status){
        response['response'] = {
            lead_status : 'Rejected',
            reason  : 'Rejected as lead from dealers mobile',
            lead : false
        }
        response['lead_id'] = 0
    }
    else
    {
        // Matching the source details
        // When Source is `dealer` only if source detail is not Empty
        //////to check for source id /////// === 7
        if(source_id === 7){
            if( leadlogdata.sub_source_dealer_id && leadlogdata.sub_source_dealer_name){
                const dealers_result = await SubSourceModel.findOne({
                    source_id : source_id,
                    sub_source_name : leadlogdata.sub_source_dealer_name,
                    sub_source_dealer_id : leadlogdata.sub_source_dealer_id,
                    status : '1'
                });
                if(dealers_result){
                    sub_source_id = dealers_result.id
                }else{
                    let insertedRecord = await SubSourceModel.create({
                        source_id : source_id,
                        sub_source_name : sub_source_name,
                        sub_source_description : sub_source_description,
                        status : active,
                        created_at : new Date(),
                        added_by : 0,
                        sub_source_dealer_id : leadlogdata.sub_source_dealer_id
                    },{transaction:transaction})
                    sub_source_id = insertedRecord.id;
                }
            }else{
                return {
                    status : false,
                    message : "Provide Sub source dealer id and dealer name",
                    lead :false
                }
            }
        }
        /////////////////////////////check this above function ///////////////////////
        let customer_data = await CustomerModel.findOne({
            where :{
                customer_mobile : customer_mobile
            }
        })
        // console.log("customerdataid",customer_data.id);
        let source_lead_id = "",customer_name = "",customer_email = "",customer_city ="",checkCampaign="",lead_platform= "",customer_location=  "",customer_location_id= "",finance="",purchase_time="",dealer_id="",temp_contact_id="",mobile_true_location="",customer_city_id="",lead_source = "",knowlarity_dealer_id="",customers_lead_data = null;;

        if(leadlogdata.source_lead_id) source_lead_id = +leadlogdata.source_lead_id
        if(leadlogdata.customer_name) customer_name = leadlogdata.customer_name
        if(leadlogdata.customer_email) customer_email = leadlogdata.customer_email;
        customer_city_id = leadlogdata.customer_city

        if(customer_city){
            ///// get city data
            checkCampaign = await getCompaignName(customer_city);
        }
        if(!checkCampaign && customer_data && customer_data.customer_city_id){
            ///get city name and id
            customer_city_id = customer_data.customer_city_id
            customer_city = customer_data.customer_city_name ///////////city name should not be taken
        }

        if(leadlogdata.lead_platform) lead_platform = leadlogdata.lead_platform

        if(leadlogdata.location){
            ////check this/////
            customer_location = leadlogdata.location;
            location_data = await getLocationId(customer_location);
            if(location_data){
                customer_location_id = location_data.id;
            }
        }
        if(leadlogdata.finance) finance = leadlogdata.finance
        if(leadlogdata.purchase_time) purchase_time = leadlogdata.purchase_time
        if(leadlogdata.dealer_id) knowlarity_dealer_id = leadlogdata.dealer_id
        if(leadlogdata.temp_contact_id) temp_contact_id = leadlogdata.temp_contact_id;

        let insert_lead = false,lead_status='',lead_status_reason="";

        if(!customer_data){
            lead_status = "New";
            lead_status_reason = `${lead_status} customer lead`;
            insert_lead  =true;
            mobile_true_location = "Code Not Exist";
            telecomLocation = await getMobileLocation(customer_mobile);
            if(telecomLocation){
                mobile_true_location  = telecomLocation['mobile_telecom_circle'];
            }
            let customerInfo = await CustomerModel.create({
                name : customer_name,
                customer_city_id : customer_city_id,
                customer_city_name : customer_city, ///customer city name to be excluded
                customer_email : customer_email,
                customer_mobile : customer_mobile,
                alternate_email : '',
                alternate_mobile : '',
                customer_location_id : customer_location_id,
                customer_location_name : customer_location,
                created_at : new Date(),
                status : 1,
                ndnc : dnd_check,
                mobile_true_location : mobile_true_location

            },{transaction:transaction});
            customer_id = customerInfo.id;
        }else{
            customer_id = customer_data.id;
            customers_lead_data = await LeadModel.findOne({
                where :{
                    customer_id : customer_id
                },
                orderBy : {
                    id : 'DESC'
                }
            });
            // console.log("customerLeadDataid",customers_lead_data.id);
            if(customer_data.status === 1){
                if(customers_lead_data){
                    let lastLeadDate = customers_lead_data.created_at;
                    // console.log(lastLeadDate);
                    let dateDifference = moment().diff(lastLeadDate,'days').valueOf();
                    // console.log("lastleaddate",lastLeadDate,"current date",new Date());

                    // console.log("customerLeadStatus",customers_lead_data.status_id);
                    if(customers_lead_data.status_id === 7 || (customers_lead_data.status_id === 6 && customers_lead_data.sub_status_id === 12 )){
                        lead_status = 'Accepted';
                        lead_status_reason = `${lead_status} lead as entered new lead of existing customer as earlier lead was marked Closed/Purchased`;
                        insert_lead = true;
                    }
                    else if( (customers_lead_data.status_id === 3 && (customers_lead_data.sub_status_id===7 || customers_lead_data.sub_status_id===5)) || customers_lead_data.status_id === 5 || customers_lead_data.status_id === 6 ){
                        lead_status = 'Duplicate';
                        lead_status_reason = `L2 staged ${lead_status} lead and it's life is ${dateDifference} days`
                        insert_lead = false
                    }
                    else if(dateDifference<=LEAD_EXPIRATION && (customers_lead_data.status_id !== 7 && customers_lead_data.sub_status_id!==12) ){
                        lead_status = 'Duplicate';
                        lead_status_reason = `${lead_status} lead as it's life is ${dateDifference} days only`
                        insert_lead = false
                    }
                    else if(dateDifference>LEAD_EXPIRATION){
                        /////////////close lead /////////////
                        await LeadModel.update({
                            status_id : 7,
                            sub_status_id : 19,
                            leads_status : 0
                        },{
                            where :{
                                id : customers_lead_data.id
                            },
                            transaction:transaction
                        })
                        lead_status = "Accepted";
                        lead_status_reason = `${lead_status} added successfully.`;
                        insert_lead = true;
                    }
                }else{
                    lead_status = "Accepted";
                    lead_status_reason = `${lead_status} added successfully.`;
                    insert_lead = true;
                }
            }else{
                lead_status = 'Rejected'
                lead_status_reason = `${lead_status} as customer is inactivce`;
                insert_lead = false;
            }
            // console.log("leadStatus",lead_status);
            // console.log("leadStatusReason",lead_status_reason);
            // console.log("insert_lead",insert_lead);
        }
        /////check lead source
        if(leadlogdata.lead_source){
            lead_source =leadlogdata.lead_source;
            const leadSourceResult = await SubSourceModel.findOne({
                where :{
                    source_id:source_id,
                    name : lead_source,
                    status : '1'
                }
            });
            if(leadSourceResult){
                sub_source_id = leadSourceResult.id
            }
        }
        // Getting sub source through lead source is available by mapped table
        
        let leadsource = leadlogdata.lead_source;
        let subsourcearray = await getSubSourceMapped(leadsource,source_id);
        if(lead_source && (source_id === 90 || source_id === 368)  && (subsourcearray && subsourcearray.id > 0) ){
            sub_source_id = subsourcearray.id;
        }

        // Inserting the Leads log data
        const LeadLogResponseData = {
            source_lead_id : source_lead_id,
            temp_contact_id : temp_contact_id,
            customer_id : customer_id,
            customer_mobile : customer_mobile,
            customer_name : customer_name,
            customer_email : customer_email,
            customer_city : customer_city,
            source_id : source_id,
            sub_source_id : sub_source_id,
            dealer_id : knowlarity_dealer_id,
            created_at : new Date(),
            lead_status : lead_status,
            leads_status_reason : lead_status_reason,
            lead_platform : leadlogdata.lead_platform,
            params_string : JSON.stringify(leadlogdata)
        }
        // console.log("leadLogData",LeadLogResponseData);
        let leadLogResponse = await LeadLogs.create(LeadLogResponseData,{transaction:transaction});
        let leads_log_id = leadLogResponse.id
        // console.log("leadLogResponseid",leads_log_id);
        // Inserting the accepted leads data
        
        if(insert_lead && (['New','Accepted'].includes(lead_status))){
            const leadInfo = {
                source_lead_id : source_lead_id,
                customer_id       : customer_id,
                source_id         : source_id,
                sub_source_id     : sub_source_id,
                status_id         : '1',
                sub_status_id     : '1',
                calling_status_id : '1',
                dealer_id         : knowlarity_dealer_id,
                ratings           : '',
                is_finance_req    : finance,
                purchase_time     : purchase_time,
                due_date          : dueDateInLead(),
                created_at        : new Date(),
                updated_by   : '0',
                platform          : leadlogdata.lead_platform,
                leads_status      : '1'
            }
            // console.log("leadModelInfo",leadInfo);
            lead_id = await LeadModel.create(leadInfo,{
                transaction:transaction
            });
            lead_id = lead_id.id
            // console.log("leadModelInfoID",lead_id);
            lead_date = new Date();
            const leadConversionInfo ={
                lead_id : lead_id,
                status_id : '1',
                sub_status_id : '1',
                calling_status_id : '1',
                comment : `${lead_status} Lead`,
                latest : '1',
                added_on : new Date(),
                added_by : '0'
            }
            // console.log("leadConvcersionInfo",leadConversionInfo);
            await LeadConversion.create(leadConversionInfo,{transaction:transaction})
        }else{
            if(customers_lead_data){
                lead_id = customers_lead_data.id;
            }
        }

        // Inserting the customers car preference
        let query_string = "",car_id = "",car_make = "",car_model ="",car_make_id="",car_model_id="",car_ver_id="",gaadi_version_id ="",car_make_name="",car_model_name="",car_ver_name="";

        if(leadlogdata.car_id) car_id = leadlogdata.car_id
        if(leadlogdata.car_make) car_make = leadlogdata.car_make
        if(leadlogdata.car_model) car_model = leadlogdata.car_model
        if(leadlogdata.car_variant_id) {
            car_variant_id = leadlogdata.car_variant_id
            gaadi_version_id = await getGaadiMakeModelVersionFromCarDekhoDealerAPI(req, car_variant_id);
        }
        else if(leadlogdata.gaadi_variant_id) gaadi_version_id = leadlogdata.gaadi_variant_id
        
        // Setting customers car preference data
        let dialerCity = "",body_style = "", car_city = "", transmission = "", color = "", fuel = "", max_price = "", min_price = "", max_year = "", min_year = "", max_km = "", min_km = "", listing_type = "", certification_id = "", otp_verified = "";
        ////check for existing car id
        if(car_id && !Number.isNaN(+car_id)){
            let used_car_data = getUsedCarDetail(req,car_id);
            if(used_car_data && Object.keys(used_car_data).length){
                car_make_id = used_car_data.make_id
                car_model_id = used_car_data.model_id
                car_ver_id = used_car_data.version_id
                body_style = used_car_data.car_body_type
                car_city  = used_car_data.city
                transmission  = used_car_data.transmission
                color  = used_car_data.uc_colour
                fuel  = used_car_data.fuel_type
                max_price  = used_car_data.car_price
                min_price  = ''//used_car_data.car_price
                max_year  = ''//used_car_data.reg_valid_year
                min_year  = used_car_data.make_year
                max_km  = used_car_data.km_driven
                min_km  = ''
                listing_type  = ''
                certification_id  = ''
                otp_verified  = ''
                ////////////// MARKING lead as premium ////////////////////
                
                if(max_price>=PREMIUM_LEAD_PRICE || min_price>=PREMIUM_LEAD_PRICE){
                    
                    await LeadModel.update({
                        is_premium : 1
                    },{
                        where : {
                            id : lead_id
                        },
                        transaction:transaction
                    });
                    await LeadLogs.update({
                        is_premium : 1
                    },{
                        where :{
                            lead_log_id : leads_log_id
                        },
                        transaction:transaction
                    })

                }

                // Inserting the customer's city if customer city is blank and preferred car city is available
                if(lead_status === "New" && customer_city === "" && car_city){
                    ///////////presuming city id ///////////////////
                    //////////// UPDATING CITY CAR ID NOT NAME////////////////
                    await CustomerModel.update({
                        customer_city_id : car_city
                    },{
                        where :{
                            id : customer_id
                        },
                        transaction : transaction
                    })
                    dialerCity = car_city
                }
            }
        }
        else if(gaadi_version_id)
        {
            ////////// get car information //////
            let gaadi_result = await getGaadiInformation(req,gaadi_version_id);
            if(gaadi_result && Object.keys(gaadi_result).length){
                car_make_name = gaadi_result.make_name
                car_make_id = gaadi_result.make_id
                car_model_name = gaadi_result.model_name
                car_model_id = gaadi_result.model_id
                car_ver_name = gaadi_result.version_name
                car_ver_id = gaadi_result.version_id
            }
        }
        ///check this
        // setting the customer's city when car-city & customer-city not set
        if(!car_city && !customer_city){
            if(telecomLocation && Object.keys(telecomLocation).length){
                if(telecomLocation['city_id'] !== 0){
                    // customer_city = telecomLocation.city_name
                    customer_city_id = telecomLocation.city_id;
                    ////////////// UPDATE CUSTOMER ///////////
                    await CustomerModel.update({
                        customer_city_id : customer_city_id
                    },{
                        where :{
                            id : customer_id
                        },
                        transaction:transaction
                    })
                    dialerCity = customer_city
                }
            }
        }

        // Adding Dialer API for Hyderabad City
        // if ($insert_lead && ($lead_status == 'New' || $lead_status == 'Accepted') && ($source_id != 7) && ($source_id != 372)):
        //    if($premiumLead == '1') {
        //         $campaign_name = PREMIUM_L1_COMPAIGN;
        //     } else {
        //         $campaign_name = $this->Customhelper->getCompaignName($dialerCity);
        //     }
        //     $this->queeindailer($customer_mobile, $campaign_name, date("Y-m-d"), $customer_name, $lead_id, $premiumLead);
        // endif;

        // // Getting CarMakeId using CarMakeName
        // if (trim($car_make_id) == '' && trim($car_make) != ''):
        //     $car_make_name = $car_make;
        //     $make = $this->getCarMakeId($car_make_name);
        //     if (!empty($make)): $car_make_id = $make[0]['id'];
        //     endif;
        // endif;
        // // Getting CarModelId using CarModelName
        // if (trim($car_model_id) == '' && trim($car_model) != ''):
        //     $car_model_name = $car_model;
        //     $model = $this->getCarModelId($car_model_name);
        //     if (!empty($model)): $car_model_id = $model[0]['id'];
        //     endif;
        // endif;
      
        // Inserting the customers car preference data to leads-car table 
        let prefResult = null;
        if(lead_id && ( (car_id>0) || (car_make_name && car_model_name) ) ){
            if(car_id){
                prefResult  = await LeadCarModel.findOne({
                    where :{
                        id : lead_id,
                        car_id : car_id
                    }
                })
            }
            else if(car_make_name && car_model_name){
                prefResult = await LeadCarModel.findOne({
                    where :{
                        id : lead_id,
                        make_name : car_make_name,
                        model_name : car_model_name
                    }
                })
            }
            if(prefResult){
                // console.log(color);
                let preferenceData = {
                    car_id : car_id,
                    make_id : car_make_id,
                    make_name : car_make_name,
                    model_id : car_model_id,
                    model_name : car_model_name,
                    variant_id : car_ver_id,
                    variant_name : car_ver_name,
                    car_availability : '',
                    body_type : body_style,
                    car_city : car_city,
                    transmission: $transmission,
                    color : color,
                    fuel : fuel,
                    price_from : max_price,
                    price_to : min_price,
                    max_year : max_year,
                    min_year : min_year,
                    max_km : max_km,
                    min_km : min_km,
                    listing_type : listing_type,
                    certification_id : certification_id,
                    status_id:'1',
                    sub_status_id:'1',
                    calling_status_id:'1',                
                    source_id : source_id,
                    sub_source_id : sub_source_id,
                    otp_verified : otp_verified,
                    added_on : new Date(),
                    added_by : '0'
                };
                
                await LeadCarModel.create(preferenceData,{transaction:transaction})
            }
        }
        // If lead is Duplicate then insert it as REPEAT lead
        let leadscore  = await leadOnlineScore(lead_id,customer_mobile);
        // console.log("lead score",leadscore);

        await LeadModel.update({
            lead_online_score : leadscore
        },{
            where : {
                id : lead_id
            },
            transaction : transaction
        });

        response['response'] = {
            lead_status : lead_status,
            reason : lead_status_reason,
            lead : insert_lead
        }
        response['lead_id'] = lead_id;
        // console.log("response data here",response);
    }
    // console.log("here");
    leadCurrentStatusdata = await getCurrentStatusofLead(1);//lead_id
    // console.log(leadCurrentStatusdata);
    if(leadCurrentStatusdata && leadCurrentStatusdata.status_id && leadCurrentStatusdata.status_id<=2){
        // console.log("inside function");
        response = await getCustomerRequirementDetails(req, lead_id,customer_id,transaction);
    }
    return response;
}
function processquerystring(){
//process query string here 
/////to do
return {};
}
async function checkMobileAuthentication(req, customer_mobile,leadlogdata,transaction){
    let dealerList = []
    if(global.hasOwnProperty('dealerList')){
        dealerList = global.dealerList
    }else{
        /////// call dealer list api once per global cycle of initiation/////
        global.dealerList = []
        ////////////// call dealer external api and get dealers with mobile ///////////////
       
        // console.log("fetching dealer list");
        let dealerListResponse  = await DealerService.getDealerList(req);
        // console.log("dealer list fetched",dealerListResponse.status);
        if(dealerListResponse.status === 200){
            let {data} = dealerListResponse;
            dealerList = data;
            global.dealerList = dealerList
        }
    }
    ///////////////////////dealer list got /////////////
    let dealerInfo = dealerList.filter(e=>(e.dealership_contact && e.dealership_contact.includes(customer_mobile)))
    if(dealerInfo && dealerInfo.length){
        await LeadLogs.create({
            customer_mobile : customer_mobile,
            created_at : new Date(),
            lead_status : 'Rejected',
            leads_status_reason : "Rejected lead as it's originating from dealers mobile",
            params_string : JSON.stringify(leadlogdata)
        },{transaction:transaction})
        
        return true;
    }
    return false;
}
async function checkMobileNDNC(mobile){
    let ndnccheck = 2;
    if(mobile){

    }
    return ndnccheck;
}
async function getCompaignName(customer_city){
    //////////// get campaign name /////////////
    const sql = `SELECT culcamp.campaign_name FROM ublms_city_cluster cc LEFT JOIN ublms_cluster_campaign culcamp ON (cc.cluster_id = culcamp.cluster_id) WHERE cc.status = '1' AND cc.city_id = ${customer_city}`;
    const campaignResult = await dbConfig.sequelize.query(sql,{raw:true,type:dbConfig.sequelize.QueryTypes.SELECT});
    return campaignResult[0]?campaignResult[0]:'';
}
async function getLocationId(customer_location){
    ////get location from city id
}
async function getMobileLocation(mobileNo){
    let returnValue = {};
    if(mobileNo && !Number.isNaN(+mobileNo)){
        /// get first four digit of mobile
        mobileNo = ""+mobileNo;
        const substr = mobileNo.substr(0,4);
        returnValue = await MobileLocality.findOne({
            where :{
                mobile_locality_no : substr
            },
            orderBy : {
                mobile_locality_id : "DESC"
            }
        })
    }
    return returnValue
}
async function getSubSourceMapped(leadsource,source_id){
    leadsource = leadsource?leadsource.toLowerCase():'';
    const sql = `SELECT submap.sub_source_id,subsource.name FROM ublms_sub_source_mapping AS submap LEFT JOIN	ublms_sub_source AS subsource ON (submap.sub_source_id = subsource.id) WHERE submap.source_id = ${source_id} AND  LOWER(submap.sub_source_slug)='${leadsource}'`;
    const result = await dbConfig.sequelize.query(sql,{raw:true,type:dbConfig.sequelize.QueryTypes.SELECT});
    return result[0]
}
async function dueDateInLead(){

}
async function getGaadiMakeModelVersionFromCarDekhoDealerAPI(req, car_variant_id){
    /////////////////////get make model variant/////////
    let mmvList = []
    //////////////get db version///////
    if(global.hasOwnProperty('mmvList') && Object.keys(global.mmvList).length){
        mmvList = global.mmvList
    }else{
        /////// call mmv list api once per global cycle of initiation/////
        global.mmvList = []
        ////////////// call mmv external api ///////////////
        
        let mmv = await InventoryService.getMMVList(req);
        if(mmv.status === 200){
            let {data} = mmv;
            mmvList = data;
            global.mmvList = mmvList
        }
    }
    let {version} = mmvList?mmvList:{}
    return "";
}
async function getUsedCarDetail(req,car_id){
    let responseData = {};

    let query ={
        "user_id":66,
        "stock_used_car_id":car_id
    }
    let usedCarDetails = await InventoryService.getCarDetails(req,query);
    if(usedCarDetails.status === 200){
        let {data} = usedCarDetails;
        responseData = data;
    }
    return responseData;
}
async function leadOnlineScore(lead_id,customer_mobile){
    return "";
}
async function getCurrentStatusofLead(lead_id){
    return await LeadModel.findOne({
        where :{
            id : lead_id
        }
    })
}
async function getCustomerRequirementDetails(req, lead_id,customer_id,transaction){
    // console.log(lead_id);
    let allLeadCars = await LeadCarModel.findAll({
        where :{
            lead_id : 1
            // lead_id
        }
    });
    if(allLeadCars){
        //////////// fetch used car detail for each car id associated to lead_id
        ////////////////////get details of each used car ////////////////////
        let carLeads = [];
        for(let i of allLeadCars){
            let carDetail = await getUsedCarDetail(req, i.car_id);
            // console.log(carDetail);
            i.carDetail = carDetail
            carLeads.push(i)
        }
       

        let requirementdata = await getCustomerRequirementDetailsData(allLeadCars,lead_id);
        if(requirementdata){
            await CustomerModel.update(requirementdata,{
                where :{
                    id : customer_id
                },
                transaction : transaction
            })
            requirementdata['lead_id']= lead_id;
            requirementdata['customer_id']=customer_id;
            requirementdata['added_date']= new Date();
            requirementdata['created_at']= new Date();
            await CustomerRequirement.create(requirementdata,{transaction:transaction});
        }
    }
}
async function getCustomerRequirementDetailsData(data,lead_id){
    let requirementData = {
        car_id : [],
        fuel : [],
        min_price : 0,
        max_price : 0,
        totalPrice : 0,
        colors : [],
        model_ids : [],
        make_ids : [],
        min_km : 0,
        max_km : 0,
        totalyears : "",
        owner : [],
        transmission : [],
        budget : 0
    };
    
    for(let i of data){
        if(i.car_id) requirementData.car_id.push(i.car_id);
        if(i.carDetail.fuel_type) requirementData.fuel.push(i.carDetail.fuel_type)
        if(i.carDetail.car_price) requirementData.min_price = Math.min(requirementData.min_price,i.carDetail.car_price)
        if(i.carDetail.car_price) requirementData.max_price = Math.max(requirementData.max_price,i.carDetail.car_price);
        if(i.carDetail.car_price) requirementData.totalPrice = +i.carDetail.car_price + requirementData.totalPrice;
        if(i.carDetail.uc_colour) requirementData.colors.push(i.carDetail.uc_colour);
        if(i.carDetail.model_id) requirementData.model_ids.push(i.carDetail.model_id)
        if(i.carDetail.make_id) requirementData.make_ids.push(i.carDetail.make_id)
        if(i.carDetail.max_km) requirementData.max_km = Math.max(requirementData.max_km,+i.carDetail.km_driven);
        if(i.carDetail.make_year){
            if(!requirementData.hasOwnProperty('min_year')) requirementData.min_year = +i.carDetail.make_year;
            else requirementData.min_year = Math.min(requirementData.min_year,+i.carDetail.make_year);
            if(!requirementData.hasOwnProperty('max_year')) requirementData.max_year = +i.carDetail.make_year;
            else requirementData.max_year = Math.max(requirementData.max_year,+i.carDetail.make_year);
        }
        if(!Number.isNaN(+i.carDetail.owner_type) && i.carDetail.owner_type!==null) requirementData.owner.push(+i.carDetail.owner_type);
        if(!Number.isNaN(+i.carDetail.transmission) && i.carDetail.transmission !==null) requirementData.transmission.push(+i.carDetail.transmission);
    }
    ////////////////stringify data ///////////
    for (let i in requirementData){
        if(requirementData[i] instanceof Array){
            requirementData[i] = requirementData[i].join(",")
        }
    }
    requirementData['budget'] = requirementData['max_price']
    return requirementData;
}
async function getGaadiInformation(req, version_id){
 /////////////////////get make model variant/////////
    let mmvList = []
    //////////////get db version///////
    if(global.hasOwnProperty('mmvList') && Object.keys(global.mmvList).length){
        mmvList = global.mmvList
    }else{
        /////// call mmv list api once per global cycle of initiation/////
        global.mmvList = []
        ////////////// call mmv external api ///////////////
        
        let mmv = await InventoryService.getMMVList(req);
        if(mmv.status === 200){
            let {data} = mmv;
            mmvList = data;
            global.mmvList = mmvList
        }
    }
    let {make,model,version} = mmvList?mmvList:{}
    if(version){
        let versionInfo = version.find(e=>e.vn_id === version_id);
        if(versionInfo){
            let modelInfo =model.find(e=>e.id === versionInfo.md_id);
            let makeInfo = make.find(e=>e.id === versionInfo.mk_id);
            if(modelInfo && makeInfo){
                return {
                    make_name : makeInfo.make,
                    make_id : makeInfo.id,
                    model_name : modelInfo.m,
                    model_id : modelInfo.id,
                    version_name : versionInfo.vn,
                    version_id : versionInfo.vn_id
                }
            }
        }
    }
    return {};
}