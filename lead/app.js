const express = require('express');
const app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
const { commonMiddleware } = require('./src/middleware/common');
const CommonHelper = require(HELPER_PATH + 'CommonHelper');

const api_logger = require(MIDDLEWARE_PATH + 'logger');
const jwt = require('./src/helpers/jwt');

var i18n = require("i18n");
let APP_LANG = process.env.APP_LANG;
i18n.configure({
    // locales: ['en','id','ph'],
    locales:[APP_LANG],
    directory: LANG_PATH,
    defaultLocale: APP_LANG,
    register: global,
    updateFiles: false,
    syncFiles: true
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(i18n.init);
app.use(api_logger.res_logger);

app.use(express.static(__dirname+'/public'));

// use JWT auth to secure the api
//app.use(jwt());

app.use(commonMiddleware, require(ROUTE_PATH + 'routes'));

app.use((req, res, next) => {
    const error = new Error('Sorry the requested method not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    CommonHelper.sendErrorResponse(req, res, error);
});

module.exports = app;