var path = require('path');
let countryCode = process.env.COUNTRY_CODE || 'id';
global.ROOT_PATH = path.resolve(__dirname) + '/';
global.CONFIG_PATH = ROOT_PATH + 'config/';
global.ROUTE_PATH = ROOT_PATH + 'routes/';
global.MODULE_PATH = ROOT_PATH + 'src/modules/';
global.MIDDLEWARE_PATH = ROOT_PATH + 'src/middleware/';
global.HELPER_PATH = ROOT_PATH + 'src/helpers/';
global.LANG_PATH = ROOT_PATH + 'src/lang';
global.COMMON_MODULE = MODULE_PATH + 'common/';
global.COMMON_MODEL = COMMON_MODULE + 'model/';
global.COMMON_CONTROLLER = COMMON_MODULE + 'controller/';
global.PAGE_LIMIT = 10;
global.PAGE_NUMBER = 1;
global.TEAM_LEAD = 2;
global.L1_USER_ROLE_ID = 3;
global.L2_USER_ROLE_ID = 4;
global.SITE_SETTINGS = ROOT_PATH + 'config/'+countryCode+'/site_settings';
global.ADMIN_ROLE_ID = 5;
global.AWS_CONFIG = ROOT_PATH + 'config/'+countryCode+'/aws_config';
global.PURCHASED_USED_CAR = 36;
global.CLOSED_NON_GAADI_DEALER = 17;
global.CLOSED_GAADI_INDIVIDUAL = 15
global.CLOSED_NON_GAADI_INDIVIDUAL = 16
global.ADDED_DATE_LEAD_OTP = '2018-01-10 01:00:00';
global.VALIDPLATFORM = ['androidapp','androidsapp', 'android app','android','iosapp','ios app','ios', 'windowsapp','windows app','windows'];
global.LEAD_ADD_API_KEY = 'p3zW7sni08WqoeQ=';

global.CLUSTER_DELHI_NCR = '-999';
global.DELHI_NCR_CITY_IDS =  "'125','562','160','535','162','9159','9160'";
global.OWNER_TYPE = {
    0 : 'Unregistered',
    1 : 'First',
    2 : 'Second',
    3 : 'Third',
    4 : 'Fourth'
}
global.GAADI_SOURCE_ID = 4;
global.ZIGWHEELS_SOURCE_ID = 368;
global.VIEW_DETAILS_UTM_PARAMS = '?utm_source=UBLMS&utm_medium=Email&utm_campaign=Recommendation&utm_content=view-details-button';
global.PDP_DEALER_EMAIL_TEXT = '(Preferred Digital Partner of CarDekho)';
global.PDP_DEALER_SMS_TEXT = '(Preferred Digital Partner of CarDekho)';
global.UBLMS_CAR_URL = 'ublms_car_url';
global.CAR_DEKHO_CAR_DETAIL_LINK = '';
global.ZIGWHEEL_CAR_DETAIL_LINK = '';
global.USED_CAR_EMAIL_DETAIL_LINK = '';
global.LEAD_EXPIRATION = 30 ///// in days
global.PREMIUM_LEAD_PRICE = "";
global.L2_LEAD_EXPIRATION = 90;
global.CAR_DISTANCE_CUSTOMER = 50;
global.UB_API_KEY = '422fd916-3cab-462d-ad9c-f1000cc8e548';
global.OTO_API_KEY = 'e8fe3567-00cd-4121-b34b-495c797526a4';
global.validPlatform = ['androidapp','androidsapp', 'android app','android','iosapp','ios app','ios', 'windowsapp','windows app','windows'];
global.ArrDCStatus = {1: 'new', 2: 'follow_up', 3: 'interested', 4: 'walk_in', 
9: 'walked_in_done', 10: 'customer_offer', 11: 'booked', 12: 'converted', 
13: 'closed'};
global.BUYER_TEMPLATE_ID = 6;
global.DEALER_TEMPLATE_ID = 7;
global.SILENT_NOTIFICATION_INTERVAL = 15;
global.SFA_TEMPLATE_ID = 9;
global.USED_CAR_URL_SIMILAR_CARS = (process.env.COUNTRY_CODE == 'id') ? 'https://www.oto.com/en/mobil-bekas/' : 'https://www.oto.com/en/mobil-bekas/';
global.CURRENCY_SYMBOL = (process.env.COUNTRY_CODE == 'id') ? 'Rp' : '₱';
global.URL_SHORTNER_API = 'http://oto.cardk.in/api/v2/action/shorten';
global.URL_SHORTNER_KEY = 'be027e6ec516a2ae3934ea4dc618c7';
global.WA_NOTIFICATION_CHECK_URL = 'http://newcarsapi.carbay.com/v2/botify/is-subscribed';
