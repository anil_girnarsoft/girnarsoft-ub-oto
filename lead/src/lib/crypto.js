const crypto = require("crypto");
const conf = require("../../config/config");
const CIPHER_ALGO = "bf-ecb";
const iv = "";
module.exports.encode = (text) => {
    var cipher = crypto.createCipheriv(CIPHER_ALGO, conf.CRYPTO_KEY, iv)
    var crypted = cipher.update("" + text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}
module.exports.decode = (text) => {
    try {
        var decipher = crypto.createDecipheriv(CIPHER_ALGO, conf.CRYPTO_KEY, iv)
        var dec = decipher.update("" + text, 'hex', 'utf8')
        dec += decipher.final('utf8');
        return dec;
    } catch (error) {
        return null;
    }
}