const CONFIG = require("./../../constant");
const AWS = require('aws-sdk');
const fs = require('fs');
const siteSetting = require(SITE_SETTINGS);
const _ = require("lodash");

let merge = (...arguments) => {

    // Variables
    let target = {};

    // Merge the object into the target object
    let merger = (obj) => {
        for (let prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                if (Object.prototype.toString.call(obj[prop]) === '[object Object]') {
                    // If we're doing a deep merge and the property is an object
                    target[prop] = merge(target[prop], obj[prop]);
                } else {
                    // Otherwise, do a regular merge
                    target[prop] = obj[prop];
                }
            }
        }
    };

    //Loop through each object and conduct a merge
    for (let i = 0; i < arguments.length; i++) {
        merger(arguments[i]);
    }

    return target;
}
module.exports = {
    getMessage: function (code) {
        var status = new Object();
        status['200'] = 'Success';
        status['201'] = 'Created';
        status['202'] = 'Accepted';
        status['203'] = 'Non-Authoritative Information';
        status['204'] = 'No Content';
        status['205'] = 'Reset Content';
        status['206'] = 'Partial Content';
        status['400'] = 'Bad Request';
        status['401'] = 'Unauthorized';
        status['402'] = 'Payment Required';
        status['403'] = 'Forbidden';
        status['404'] = 'Not Found';
        status['405'] = 'Method Not Allowed';
        status['406'] = 'Not Acceptable';
        status['407'] = 'Proxy Authentication Required';
        status['408'] = 'Request Timeout';
        status['409'] = 'Conflict';
        status['410'] = 'Gone';
        status['411'] = 'Length Required';
        status['412'] = 'Precondition Failed';
        status['413'] = 'Request Entity Too Large';
        status['414'] = 'Request-URI Too Long';
        status['415'] = 'Unsupported Media Type';
        status['416'] = 'Requested Range Not Satisfiable';
        status['417'] = 'Expectation Failed';
        status['500'] = 'Internal Server Error';
        status['501'] = 'Not Implemented';
        status['502'] = 'Bad Gateway';
        status['503'] = 'Service Unavailable';
        status['504'] = 'Gateway Timeout';
        status['505'] = 'HTTP Version Not Supported';
        status['509'] = 'Bandwidth Limit Exceeded';
        return status[code];
    },

    sendSuccessResponse: function (req, res, data, msg = null) {
        this.sendResponse(req, res, 200, msg, data);
    },

    sendPaginationResponse: function (req, res, data, pagination) {
       // let _page = {total: (pagination && pagination.total) ? pagination.total : 0};
        let _page = (pagination && pagination.total) ? pagination : null;
        this.sendResponse(req, res, 200, null, data, null, { pagination: _page });
    },

    sendErrorResponse: function (req, res, errors) {
        this.sendResponse(req, res, (errors.status || 400), false, false, errors);
    },
    
    sendResponse: function (req, res, status, message, data, errors, other) {
        var errorType = 1;
        var response = new Object();
        //var agent = useragent.parse(req.headers['user-agent']);
        var userIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection.socket ? req.connection.socket.remoteAddress : '');
        let apiStatus = status; //Default status
        //var userAgent = agent.toString();
        //var userDevice = req.device && req.device.type ? req.device.type : '';

        if (status) {
            if (status == 400 || status == 401) {
                errorType = 2;
            }
            //If status is 401 send 401 status
            if(status == 401) {
                apiStatus = 401;
            }
            response.status = status;
            response.message = this.getMessage(status);
            response.cached = req.cached;
        }
        if (errors) {
            if (typeof errors === 'string') {
                response.message = res.__(errors);
            }
            else if (Array.isArray(errors)) {
                response.message = res.__(errors[0], errors[1]);
            }
            else if (errors instanceof ReferenceError) {
                response.message = errors.message;
            }
            else if (errors && errors.name == "SequelizeDatabaseError") {
                response.message = res.__("INTERNAL_SERVER_ERROR");
                errors = (errors.stack + "").split("\n")[0];
            }
            else if (errors instanceof Error && errors.isJoi == true) {
                const { details } = errors;
                response.message = details.map(v=>v.message).join(", ");
                errors = details;
            }
            else if (errors instanceof Error) {
                response.message = res.__(errors.message);
            }
        }
        if (message) {
            response.message = res.__(message);
        }
        if (data) {
            response.data = data;
        }

        if (errors) {
            if (errors instanceof Error) {
                errors = (errors.stack + "").split("\n");
            }
            response.errors = errors;
        }
        if(other && other.pagination) {
            response.pagination = other.pagination;
        }

        var url = req.protocol + '://' + req.headers.host + req.originalUrl;
        var urlParts = require('url').parse(req.url, true);
        var urlPathname = urlParts.pathname;
        if (!res.headersSent) {
            res.status(apiStatus).send(response);
        }
    },

    setActivity: function (req, name, data) {
        req.activity = {};
        req.activity.activity_name = name;
        req.activity.activity_display_name = ACTIVITY[name];
        if (data.account_id) {
            req.activity.account_id = data.account_id;
        }
        if (data.payment_id) {
            req.activity.payment_id = data.payment_id;
        }
        req.activity.created_by = req.user && req.user.id ? req.user.id : '';
    },

    getCurrentDate: function () {
        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        var dateText = year + "-" + month + "-" + day;
        return dateText;
    },

    capitalizeFirstLetter: function (str) {
        return str.charAt(0).toUpperCase() + str.substr(1);
    },

    isEmpty: function (value) {
        if (Array.isArray(value) && value.length == 0) {
            return true;
        }
        if (!value || value === '0') {
            return true;
        }
        if (Object.keys(value).length === 0 && value.constructor === Object) {
            return true;
        }
        return false;
    },

    removeMultipleSpace: function (str) {
        return str.replace(/\s\s+/g, ' ');
    },

    removeLineCharacters: function (str) {
        return str.replace(/\r?\n|\r/g, ' ');
    },

    replaceCharactersWithX: function (str, skip, atStart, fixedLength) {
        str = this.removeLineCharacters(str);
        fixedLength = fixedLength || str.length;
        if (skip) {
            if (atStart) {
                str = str.substring(0, skip) + new Array(fixedLength - skip + 1).join('X');
            } else {
                str = new Array(fixedLength - skip + 1).join('X') + str.substring(str.length - skip);
            }
        } else {
            str = new Array(fixedLength + 1).join('X');
        }
        return str;
    },

    isJsonString: function (data) {
        try {
            JSON.parse(data);
        } catch (e) {
            return false;
        }
        return true;
    },
    getUserLanguageId: function(langCode) {        
        let lang_id = DEFAULT_LANGUAGE_ID;
        if (!(typeof jsonLanguageArr[langCode] == 'undefined') && parseInt(jsonLanguageArr[langCode]) > 0) {
            lang_id = jsonLanguageArr[langCode];
        }
        return lang_id;
    },
    merge: merge,
    blockingReasons: () => {
        return [
            {value: '0', label: 'Unblock'},
            {value: '1', label: 'Dealer'},
            {value: '2', label:'Broker'},
            {value: '3', label:'Scraping agent (Cars 24, etc)'},
            {value: '4', label:"Customer don't want any call/DND customer"}
        ]
    },

    timeDifference: async (fromTime, toTime) => {
        let d = Math.abs(fromTime - toTime) / 1000;                           // delta
        let r = {};                                                                // result
        let s = {                                                                  // structure
            year: 31536000,
            month: 2592000,
            week: 604800, // uncomment row to ignore
            day: 86400,   // feel free to add your own row
            hour: 3600,
            minute: 60,
            second: 1
        };
    
        Object.keys(s).forEach(function(key){
            r[key] = Math.floor(d / s[key]);
            d -= r[key] * s[key];
        });
    
        // for example: {year:0,month:0,week:1,day:2,hour:34,minute:56,second:7}
        return r;
    },

    dealerPriority : () => {
        
        return [
            {value : '1', label : 'Low'},
            {value : '2', label :'Medium'}, 
            {value : '3', label : 'High'}
        ];
    },

    dealerBoostActive : () => {
        return [
            {value : '0', label : 'No'},
            {value : '1', label :'Yes'} 
        ];
    },

    sendBackendLeads : () => {
        return [
            { value: '1', label: 'No' },
            { value: '2', label: 'Yes' },
        ]
    },

    platform: () =>  ['web','wap','iosapp','androidapp'],


    DialshreeAddApiConfig : (country) => {
        if(country == 'id'){
            return {
                'function'          : 'add_lead',
                'source'            : 'external',
                'user'              : 'api',
                'pass'              : '1234'
            }
        }else{ //FOR PH
            return {
                'action'        : 'add_multi_lead',
                'source'        : 'ubph'
            }
        }
        
    },
    DialshreeAddApiConfigListIdCode: (country) => {
        if(country == 'id'){
            return {
                "L1" : 405, 
                "L2" : 406,
                "L3" : 407
            }
        }else{ //FOR PH
            return {
                "L1" : 209, 
                "L2" : 210,
                "L3" : 211
            }
        }
        
    },
    isToday: (someDate) => {
        someDate = new Date(someDate);
        const today = new Date()
        return someDate.getDate() == today.getDate() &&
          someDate.getMonth() == today.getMonth() &&
          someDate.getFullYear() == today.getFullYear()
    },

    getCurrencySymbol: (country) => {
        if(country == 'id'){
            return 'Rp';
        }else{
            return '₱';
        }
    }
}