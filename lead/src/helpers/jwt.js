const expressJwt = require('express-jwt');
const config = require('../../config/jwt_config');
const jwtWebToken = require('jsonwebtoken');
const userTokenModel = require('../modules/user/model/userTokenModel');

module.exports = jwt;

 function jwt() {
    const { secret } = config;
    return  expressJwt({ 
        secret, requestProperty: 'authUser',
        isRevoked: isNotRevokedCallback,
        getToken: function fromHeaderOrQuerystring (req) {
            if (req.headers.authorization) {
                return req.headers.authorization;
            }
            return null;
        }
        }).unless({
        path: [
            // public routes that don't require authentication
            '/user/login',
            '/user/logout',
            '/user/getCaptchaImage',
            '/user/forgotPassword',
            '/user/reset-password',
            '/user/getRefreshToken',
            '/user/resendOtp',
            '/user/getResetPasswordToken',
            '/user/validateResetToken',
            '/user/resetPassword',
            '/vehicles/getCarMake',
            '/vehicles/getCarModel',
            '/user/getSiteSettings',
            '/runCron',
            '/lead/getDialerLeadResponse',
            '/notification/getNotificationDetail'
        ]
    });
}

//FUNCTION TO CHECK ACCESS TOKEN FROM DATABASE
async function isNotRevokedCallback(req, payload, done) {
    let accessToken = (req.headers.authorization && req.headers.authorization);

            await userTokenModel.getOne({'access_token': accessToken}).then((resp)=>{
                if(!resp){
                    done('access_token_expired');
                }
                else {
                    done(null, false)
                }
            });
   }