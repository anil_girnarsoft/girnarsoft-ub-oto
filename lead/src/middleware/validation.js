const CommonHelper = require('../helpers/CommonHelper');
const messages = require("../lang/joi");    // Ref: https://github.com/hapijs/joi/issues/2037
module.exports = (schema, property) => {
    return (req, res, next) => {
        
        // const lang = req.headers['accept-language'];
        const lang = process.env.APP_LANG || 'id';
        const { error } = schema.validate(req[property], { abortEarly: false, messages, errors: { language: lang } });
        const valid = error == null;
        if (valid) { next(); }
        else {
            CommonHelper.sendErrorResponse(req, res, error)
        }
    }
}
