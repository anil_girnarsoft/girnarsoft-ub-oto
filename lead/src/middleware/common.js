module.exports.commonMiddleware = (req, res, next) => {
    try {
        if(req.headers['gateway-user']) req.user = JSON.parse(req.headers['gateway-user']);
        if(req.headers['gateway-services']) req.services = JSON.parse(req.headers['gateway-services']);
        next();
    } catch (error) {
        next();
    }
};