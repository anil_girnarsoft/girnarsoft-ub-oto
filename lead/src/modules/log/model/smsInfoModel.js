const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class SmsInfoModel extends Model{
}

SmsInfoModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    walkin_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    mobile:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    message:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    source:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    priority:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    NDNC:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    sender: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    sms_sent_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    about_config:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    dealer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    lead_car_id:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    is_sent:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:2
    },
    is_locked: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:2
    },
    num_try: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    sms_click_status:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:0
    },
    isResend: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    type: {
        type: dbConfig.Sequelize.ENUM('1','2','3','4','5','6'),
        allowNull: false,
        defaultValue:1
    },
    sent_flag:{
        type: dbConfig.Sequelize.ENUM('-1','0','1','2','3'),
        allowNull: false
    },
    sms_type:{
        type: dbConfig.Sequelize.ENUM('0','1','2','3'),
        allowNull: false,
        defaultValue:0
    },
    created_at:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_at:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
    
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_SMS_INFO,
    freezeTableName: true
});


SmsInfoModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await SmsInfoModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

SmsInfoModel.updateOne = async (_where, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await SmsInfoModel.update(data, { where: _where });
            
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


SmsInfoModel.getSmsInfo = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select mobile, message, source, priority, NDNC, sender, sms_sent_by, about_config, \n"+
        "lead_id, car_id, is_sent, is_locked, num_try, isResend, created_at, `updated_at` from " + UBLMS_SMS_INFO  + "\n"+
        "where lead_id = '"+leadId+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}




module.exports = SmsInfoModel;