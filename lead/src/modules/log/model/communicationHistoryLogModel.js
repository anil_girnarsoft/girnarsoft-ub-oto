const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class CommunicationHistoryLogModel extends Model{
}

CommunicationHistoryLogModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    mobile:{
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    email:{
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    campaign_type:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    channel_type:{
        type: dbConfig.Sequelize.ENUM('0','1','2','3','4','5','6'),
        allowNull: false,
        defaultValue:'0'
    },
    message:{
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    reference_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    click_status:{
        type: dbConfig.Sequelize.ENUM('0','1','2'),
        allowNull: false,
        defaultValue:'0'
    },
    status:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false
    },
    sent_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull: false,
        defaultValue:0
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    update_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    open_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_COMMUNICATION_HISTORY_LOG,
    freezeTableName: true
});


CommunicationHistoryLogModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await CommunicationHistoryLogModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

CommunicationHistoryLogModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await CommunicationHistoryLogModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


CommunicationHistoryLogModel.getHistoryLogToInsert = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select mobile, email, campaign_type, channel_type, message, reference_id, \n"+
        "lead_id,click_status, status, sent_date, created_at, update_at, open_date from "+UBLMS_COMMUNICATION_HISTORY_LOG+" where lead_id = '"+leadId+"' ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}


module.exports = CommunicationHistoryLogModel;