const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class EmailTrackingLogModel extends Model{
}

EmailTrackingLogModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    email_from:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    email_to:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    email_subject:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    attachments:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    email_body:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    from_name:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    to_name:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    template_name:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    is_sent: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    },
    num_try: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    email_sent_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    sent_status:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    click_status: {
        type: dbConfig.Sequelize.ENUM('0','1','2'),
        allowNull: false,
        defaultValue:'0'
    },
    tracking_key: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    about_config: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    sent_flag: {
        type: dbConfig.Sequelize.ENUM('0','1','2','3','-1'),
        allowNull: false
    },
    sent_datetime: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_EMAIL_TREACKING_LOG,
    freezeTableName: true
});


EmailTrackingLogModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await EmailTrackingLogModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

EmailTrackingLogModel.updateOne = async (id, data) => { 
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await EmailTrackingLogModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


EmailTrackingLogModel.getEmailTrackInfo = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select email_from, email_to, email_subject, attachments, email_body, from_name,\n"+
        "to_name, template_name,is_sent, num_try,email_sent_by, sent_status,lead_id,about_config,\n"+
        "sent_datetime from " + UBLMS_EMAIL_TREACKING_LOG  + " where lead_id = '"+leadId+"'  ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}



module.exports = EmailTrackingLogModel;