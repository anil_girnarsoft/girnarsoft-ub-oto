const express = require('express');
const router = express.Router();
const schemas = require('./validation_schema');
const validation = require(MIDDLEWARE_PATH + 'validation');
const CarMakeController = require('./controller/carMakeController');

router.post('/listCarMakeModel', CarMakeController.getMakeModelList);
router.post('/listCarMake', CarMakeController.getCarMake);
router.post('/listCarMakeModelVariant', CarMakeController.listCarMakeModelVariant);

module.exports = router;