const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class CarMakeModel extends Model{
}

CarMakeModel.get = async (_where,pagination) => {

    let whereCond = await CarMakeModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS id,make_id,make,model,model_slug,created_at,updated_at,added_by,updated_by FROM "+GAADI_CAR_MAKE_MODELS+"\n\
        "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await CarMakeModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

CarMakeModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
        paginationCond = '';
        
    if(params.isPaginationRequired != false)
        paginationCond = await CarMakeModel.bindPagination(params,pagination,isPaginationApply);
    if(params && params.name)
        condition = "where name like '%" + params.name + "%'";
    
    if(params && params.id){
        let id = "id='"+ CarMakeModel.decrypt(params.id)+"'";
        condition = (condition == '')? condition + "where "+id : condition+" and "+id 
    }
    if(params && params.make_id){
        let makeId = "make_id='"+params.make_id+"'";
        condition = (condition == '')? condition + "where "+makeId : condition+" and "+id 
    }
    if(params && params.parent_model_id){
        let parent_model_id = "parent_model_id='"+ CarMakeModel.decrypt(params.parent_model_id)+"'";
        condition = (condition == '')? condition + "where "+parent_model_id : condition+" and "+parent_model_id 
    }
    if(params && params.created_at){
        let inputDate = dateFormat(params.created_at, "yyyy-mm-dd");
        let datetCond = "date_format(created_at,'%Y-%m-%d') <= '" + inputDate+ "'"
        condition = (condition == '')? condition + "where "+datetCond : condition+" and "+datetCond 
    }
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

CarMakeModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

CarMakeModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

CarMakeModel.encrypt = (data) => {
    if(Array.isArray(data)) data = data.map(v => {
        if(v && v.id != null) v.car_make_id_hash = crypto.encode(v.id);
        return v;
    })
    else if (data && data.id) data.car_make_id_hash = crypto.encode(data.id);
    return data;
}

CarMakeModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

module.exports = CarMakeModel;