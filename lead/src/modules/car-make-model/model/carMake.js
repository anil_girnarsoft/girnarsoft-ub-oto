const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const md5 = require('md5');
const crypto = require("../../../lib/crypto");
class CarMake extends Model{
}

CarMake.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    make: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    make_slug: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    despn: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    img: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    largeImg: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    official_site: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    dis_cont: {
        type: dbConfig.Sequelize.NUMBER,
        allowNull: true
    },
    priority: {
        type: dbConfig.Sequelize.NUMBER,
        allowNull: true
    },
    launch_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    updated_date_cloud: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    central_make_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    }
    
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_GAADI_CAR_MAKE,
    freezeTableName: true
});



CarMake.getModel = (params) => {
    let dataModel = {};

    if(params.id)
        dataModel.status = params.id;
    if(params.make)
        dataModel.make = params.make
    if(params.make_slug)
        dataModel.make_slug = params.make_slug;
    if(params.despn)
        dataModel.despn = params.despn;
    if(params.img)
        dataModel.img = params.img;
    if(params.largeImg)
        dataModel.largeImg = params.largeImg;
    if(params.official_site)
        dataModel.official_site = params.official_site;
    if(params.dis_cont)
        dataModel.dis_cont = params.dis_cont;
    if(params.priority)
        dataModel.priority = params.priority;
    if(params.launch_date)
        dataModel.launch_date = params.launch_date;
    if(params.updated_date_cloud)
        dataModel.updated_date_cloud = params.updated_date_cloud;
    if(params.central_make_id)
        dataModel.central_make_id = params.central_make_id;
    return dataModel;
}

CarMake.get = async (_where,pagination) => {

    let whereCond = await CarMake.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS id,make,make_slug,despn,img,largeImg,official_site,dis_cont,priority,launch_date,updated_date_cloud,central_make_id FROM "+UBLMS_GAADI_CAR_MAKE+"\n\
        "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await CarMake.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

CarMake.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
        paginationCond = '';
        
    if(params.isPaginationRequired != false)
        paginationCond = await CarMake.bindPagination(params,pagination,isPaginationApply);
    if(params && params.name)
        condition = "where name like '%" + params.name + "%'";
    
    if(params && params.id){
        let id = "id='"+ CarMake.decrypt(params.id)+"'";
        condition = (condition == '')? condition + "where "+id : condition+" and "+id 
    }
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

CarMake.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

CarMake.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

CarMake.encrypt = (data) => {
    if(Array.isArray(data)) data = data.map(v => {
        if(v && v.id != null) v.car_make_id_hash = crypto.encode(v.id);
        return v;
    })
    else if (data && data.id) data.car_make_id_hash = crypto.encode(data.id);
    return data;
}

CarMake.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}
module.exports = CarMake;