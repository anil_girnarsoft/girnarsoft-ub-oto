const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");

class CityModel extends Model{
}

// CityModel.init({
//     id:{
//         type: dbConfig.Sequelize.INTEGER,
//         allowNull: false,
//         primaryKey: true,
//         autoIncrement: true
//     },
//     name: {
//         type: dbConfig.Sequelize.STRING,
//         allowNull: false
//     },
//     state_id: {
//         type: dbConfig.Sequelize.INTEGER,
//         allowNull: true
//     },
//     region_code: {
//         type: dbConfig.Sequelize.STRING,
//         allowNull: true
//     },
//     city_order: {
//         type: dbConfig.Sequelize.INTEGER,
//         allowNull: true
//     },
//     created_at: {
//         type: dbConfig.Sequelize.DATE,
//         allowNull: false
//     },
//     added_by: {
//         type: dbConfig.Sequelize.INTEGER,
//         allowNull: true
//     },
//     updated_at: {
//         type: dbConfig.Sequelize.DATE,
//         allowNull: true
//     },
//     updated_by: {
//         type: dbConfig.Sequelize.INTEGER,
//         allowNull: true
//     },
//     status: {
//         type:dbConfig.Sequelize.ENUM('0', '1'),
//         allowNull: false,
//         defaultValue: '1'
        
//     }
// },{
//     sequelize,
//     timestamps: false,
//     modelName: UBLMS_CITY,
//     freezeTableName: true
// });

// CityModel.encrypt = (data) => {
//     if(Array.isArray(data)) data = data.map(v => {
//         if(v && v.id != null) v.city_id_hash = crypto.encode(v.id);
//         return v;
//     })
//     else if (data && data.id) data.city_id_hash = crypto.encode(data.id);
//     return data;
// }

// CityModel.decrypt = (hashId) => {
//     let id = '';
//     id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
//     return id;
// }

// CityModel.createOne = async (data) => {
//     return new Promise(async (resolve, reject) => {
//         try {
//             data = data || {};
//             const role_data = await CityModel.create(data);
//             resolve(role_data);
//         } catch (error) {
//             reject(error)
//         }
//     })
// }

// CityModel.updateOne = async (id, data) => {
//     return new Promise(async (resolve, reject) => {
//         try {
//             data = data || {};
//             const role_data = await CityModel.update(data, { where: { id: id } });
//             resolve(role_data);
//         } catch (error) {
//             reject(error)
//         }
//     })
// }

// CityModel.get = async (_where,pagination) => {

//     let whereCond = await CityModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
//     return new Promise(function (resolve, reject) {
//         var sql = "SELECT SQL_CALC_FOUND_ROWS cty.id,cty.name,ctyLan.lang_id,cty.created_at,cty.added_by,cty.updated_by,cty.updated_at,cty.region_code,cty.state_id,cty.status FROM "+UBLMS_CITY+" AS cty \n\
//         INNER JOIN "+UBLMS_CITY_LANG+" AS ctyLan ON cty.id=ctyLan.city_id \n\
//         "+whereCond;
//         AppModel.dbObj.sequelize
//         .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
//         .then(async (result) => {
//             let totalCount = await CityModel.getRowsCount();
//             resolve([result, totalCount]);
//         }).catch(function (error) {
//             reject(error);
//         });
//     });
// }

CityModel.getCityListForCluster = async (_where,pagination) => {
    return new Promise(function (resolve, reject) {
        // var sql = "SELECT SQL_CALC_FOUND_ROWS cty.id,cty.name,ctyLan.lang_id,cty.created_at,cty.updated_at,cty.status FROM "+UBLMS_CITY+" AS cty \n\
        // INNER JOIN "+UBLMS_CITY_LANG+" AS ctyLan ON cty.id=ctyLan.city_id where cty.id not in (select ctyCluster.city_id from "+UBLMS_CITY_CLUSTER+" as ctyCluster)";

        let _whereCond = "";
        
        if(_where['cluster_id']){
            _whereCond = "where cluster_id in ('"+_where['cluster_id']+"') order by city_id ASC";
        }
        var sql = "SELECT city_id from "+UBLMS_CITY_CLUSTER+" "+_whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            // let totalCount = await CityModel.getRowsCount();
            resolve([result]);
        }).catch(function (error) {
            reject(error);
        });
    });
}




// CityModel.getModel = (params) => {
//     let dataModel = {};
//     if(!params.id)
//         dataModel.created_at = new Date().toISOString();
//     else
//         dataModel.updated_at = new Date().toISOString();
//     if(params.id && (params.hasOwnProperty('status')))
//         dataModel.status = params.status;
//     else if((!params.id))
//         dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
//     if(params.name)
//         dataModel.name = params.name;
//     if(params.state_id)
//         dataModel.state_id = params.state_id;
//     if(params.city_order)
//         dataModel.city_order = params.city_order;
//     if(params.central_city_id)
//         dataModel.central_city_id = params.central_city_id;
//     if(params.region_code)
//         dataModel.region_code = params.region_code;
//     if(params.added_by)
//         dataModel.added_by = params.added_by
//     if(params.updated_by)
//         dataModel.updated_by = params.updated_by
//     return dataModel;
// }

// CityModel.bindCondition = async (params,pagination,isPaginationApply) => {
//     let condition = '',
//         sortCond = '',
//         paginationCond = '';
//     if(params.isPaginationRequired != false)
//         paginationCond =  await CityModel.bindPagination(params,pagination,isPaginationApply);
//     if(params && params.name)
//         condition = "where cty.name like '%" + params.name + "%'";
//     if(params && params.id){
//         let id = "cty.id='"+CityModel.decrypt(params.id)+"'";
//         condition = (condition == '') ? condition + "where "+id : condition+" and "+id 
//     }
//     if(params && params.hasOwnProperty('status')){
//         let status = "cty.status='"+params.status+"'";
//         condition = (condition == '')? condition + "where "+status : condition+" and "+status 
//     }
//     if(params && params.created_at){
//         let inputDate = dateFormat(params.created_at, "yyyy-mm-dd");
//         let datetCond = "date_format(cty.created_at,'%Y-%m-%d') <= '" + inputDate+ "'"
//         condition = (condition == '')? condition + "where "+datetCond : condition+" and "+datetCond 
//     }
//     if(params && params.sort && params.sort.length){
//         let srtType = '',
//             srtBy = '',
//             finalSort = '';
//         _.forEach(params.sort,(srtKey) => {
//             srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
//             srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
//             if(srtBy && srtBy == 'id')
//                 srtBy = "cty."+srtBy
//             if(srtBy && srtBy == 'name')
//                 srtBy = "cty."+srtBy
//             if(srtBy && srtBy == 'created_at')
//                 srtBy = "cty."+srtBy
//             if(srtBy && srtBy == 'status')
//                 srtBy = "cty."+srtBy
//             let sortData = srtBy + ' ' + srtType;
//             finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
//         })
//         sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
//     }
//     return condition+" "+sortCond+" "+paginationCond;
// }

// CityModel.bindPagination = (params,pagination,isPaginationApply) => {
//     let condition = '';
//     if(isPaginationApply){
//         let  page_no  = pagination;
//         let limit = PAGE_LIMIT;
//         let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
//         let offset = limit * (page - 1);
//         condition = "LIMIT "+offset+","+limit;
//     }
//     return condition;
// }

// CityModel.getRowsCount = async ()=>{
//     return new Promise((resolve, reject)=>{
//         let sql = "SELECT FOUND_ROWS() as total";
//         AppModel.dbObj.sequelize
//         .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
//         .then(result=>{
//             resolve(result[0].total);
//         }).catch(error=>{
//             reject(error);
//         });
//     })
// }

// CityModel.getAllCities = async () => {

//     return await CityModel.findAll({
//         where: {
//             [Op.and]: [{"status": "1"}],
//             "region_code": {[Op.not]: ""},
//             "state_id": {[Op.not]: "0"} 
//         }
//     });
// }

CityModel.getCompaignListIDByCityId = (city_id) => {
    return new Promise((resolve, reject)=>{
        let conditions = '';
        if(city_id){ 
            conditions += " where cc.status = '1' and city.id = '"+city_id+"'";
        }        
        let sql = "Select cc.l1_list_id from "+UBLMS_CITY+" city Left Join "+UBLMS_CLUSTER_CAMPAIGN+" cc ON city.cluster_id = cc.cluster_id "+conditions+" order by city_order ASC";
        
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(compaignList=>{
            let campaign_name = '';
            if((compaignList && compaignList.length && compaignList['0']['l1_list_id'])){ 
                campaign_name = compaignList['0']['l1_list_id'];
            } 
            resolve(campaign_name);
        }).catch(error=>{
            reject(error);
        });
    })
}

CityModel.getRTOData = () => {
    return new Promise((resolve, reject)=>{
        let sql = "Select id,rto_code, rto_name, city_id, city_name,zone  FROM "+RTO_DATA;
        
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

CityModel.getClusterIdFromCity = (cityId) => {
    return new Promise((resolve, reject)=>{
        let sql = "Select cluster_id  from  " + UBLMS_CITY_CLUSTER + " where city_id = '" +cityId + "' ";
        
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

CityModel.getLocationList = (cityId) => {
    return new Promise((resolve, reject)=>{
        let sql = "Select id,location_name as name,city_id  from  " + UBLMS_LOCATIONS + "";
        
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

module.exports = CityModel;