const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;

class CityLangModel extends Model{
}

CityLangModel.init({
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    lang_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    city_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CITY_LANG,
    freezeTableName: true,
    underscored: true
});


CityLangModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const city_lang_data = await CityLangModel.create(data);
            resolve(city_lang_data);
        } catch (error) {
            reject(error)
        }
    })
}

CityLangModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const city_data = await CityLangModel.update(data, { where: { city_id: id } });
            resolve(city_data);
        } catch (error) {
            reject(error)
        }
    })
}

CityLangModel.getModel = (params) => {
    let dataModel = {};
    dataModel.city_id = (params.id) ? params.id : params.city_id;
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    dataModel.lang_id = (params.lang_id) ? params.lang_id : 1;
    if(params.name)
        dataModel.name = params.name;
    if(params.added_by)
        dataModel.added_by = params.added_by;
    if(params.updated_by)
        dataModel.updated_by = params.updated_by
    return dataModel;
}

module.exports = CityLangModel;