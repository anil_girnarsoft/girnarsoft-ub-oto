const Q = require("q");
const _ = require("lodash");
const cityModel = require('./../model/cityModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const InventoryService = require(COMMON_MODEL + 'inventoryService');
const cityController = require('../../city/controller/cityController');

exports.getCityDetailsForCluster = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let value = {cluster_id: (params['cluster_id']) ? ((params['cluster_id']).join(',')) : '' };
    let pagination = {};
    let data = await cityModel.getCityListForCluster(value,page_number);
    let cityClusterList = data[0];
        cityClusterList = cityClusterList.map(cl=>{ return cl.city_id; });

    let cityParams = {cb_function: true, city_id: cityClusterList};
    req.body = cityParams; //update request

    let cityList = await cityController.getCityStateList(req, res, next);
    ApiController.sendPaginationResponse(req, res, cityList);
  }catch(err){
    // console.log('err', err);
  }
}

exports.getAllCities = async (req, res, next)=>{
  
  try{
    let params = req.body
    
    let listData = await cityModel.getAllCities();
    const result = [];
   
    if(listData){
      
      listData.forEach(element => {
        
          result.push({name:element.name, id:element.id})
        
      });
    }

      ApiController.sendResponse(req, res, 200, res.__('cities_list'), result);

  } catch (error) {
    next(error);
  }

}

exports.getCityStateList = async (req, res, next) => {
  let params = _.cloneDeep(req.body);

  try{
      let resp = await InventoryService.getStateCityList(req);
      let result = (resp && resp.data) ? resp.data : [];
      
      if(params['cb_function']){
        return result || [];
      }else{
        ApiController.sendResponse(req, res, 200, res.__('cities_list'), result);
      }
  } catch(error) {
    // next(error);
  }
}

exports.getRTOList = async (req, res, next)=>{
    try{
      let rtoData = await cityModel.getRTOData();
      ApiController.sendPaginationResponse(req, res, rtoData,{});
    }catch(err){
      
    }
  }

  exports.getLocationList = async (req, res, next)=>{
    try{
      let locationData = await cityModel.getLocationList();
      ApiController.sendPaginationResponse(req, res, locationData,{});
    }catch(err){
      
    }
  }
