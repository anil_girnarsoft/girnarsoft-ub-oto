const express = require('express');
const router = express.Router();
const schemas = require('./validation_schema');
const validation = require(MIDDLEWARE_PATH + 'validation');
const CityController = require('./controller/cityController');

// router.post('/listCity', CityController.getCityList);
// router.post('/saveCity',validation(schemas.createCity, 'body'), CityController.saveCity);
// router.post('/editCity',validation(schemas.updateCity, 'body'),CityController.saveCity);
// router.post('/updateStatus',validation(schemas.updateStatus, 'body'),CityController.updateStatus);
router.post('/listCityForCluster',CityController.getCityDetailsForCluster);
router.get('/getAllCities', CityController.getAllCities);
router.post('/getCityState', CityController.getCityStateList);
router.post('/getRTOList', CityController.getRTOList);
router.post('/getLocationList', CityController.getLocationList);

module.exports = router;