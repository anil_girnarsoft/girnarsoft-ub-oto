const Joi = require('@hapi/joi')

module.exports = {
    createCity: Joi.object().keys({
        name: Joi.string().required(),
        state_id: Joi.number().optional(),
        region_code: Joi.string().optional(),
        city_order: Joi.number().optional(),
        state_id: Joi.number().optional(),
        lang_id: Joi.number().optional(),
        status: Joi.string().valid('0', '1').optional()
    }),
    updateCity: Joi.object().keys({
        id: Joi.number().required(),
        name: Joi.string().required(),
        state_id: Joi.number().optional(),
        region_code: Joi.string().optional(),
        city_order: Joi.number().optional(),
        lang_id: Joi.number().optional(),
        state_id: Joi.number().optional(),
        status: Joi.string().valid('0', '1').optional()
    }),
    updateStatus: Joi.object().keys({
        id: Joi.number().required(),
        status: Joi.string().valid('0', '1').required()
    }),
    getList: Joi.object().keys({
        id: Joi.number().required(),
        _with: Joi.array().items(Joi.string().required()).optional()
    }).unknown(true)
}