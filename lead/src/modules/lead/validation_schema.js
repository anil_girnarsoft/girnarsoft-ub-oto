const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));
const array_items = Joi.number()
module.exports = {
    createLead: Joi.object().keys({
        customer_name: Joi.string().required(),
        customer_email: Joi.string().optional(),
        customer_mobile: Joi.string().required(),
        city_id: Joi.number().required(),
        source_id: Joi.number().required(),
        sub_source_id: Joi.number().required(),
        is_premium: Joi.number().optional()
    }),
    updateRole: Joi.object().keys({
        id: Joi.number().required(),
        name: Joi.string().required(),
        description: Joi.string().optional(),
        status: Joi.string().valid('0', '1').optional()
    }),
    updateStatus: Joi.object().keys({
        id: Joi.number().required(),
        status: Joi.string().valid('0', '1').required()
    }),
    getList: Joi.object().keys({
        id: Joi.string().required(),
        _with: Joi.array().items(Joi.string().required()).optional()
    }).unknown(true),
    saveClosedLeadInfo: Joi.object().keys({
        closed_sub_status_id: Joi.number().required(),
        lead_id : Joi.number().required(),
        is_broker: Joi.boolean().required(),
        closed_lead_info_id : Joi.number(),
        make : Joi.number().required(),
        model : Joi.number().required(),
        is_broker : Joi.number(),
        lead_id : Joi.number(),
        assigned_arr : Joi.array().items(array_items),
        car_id : Joi.number(),
        lead_car_id : Joi.number(),
        price : Joi.number(),
        lead_close_date : Joi.date().format("YYYY-MM-DD"),
        registration_no : Joi.string()
    }),
    carLeadCount: Joi.object().keys({
        carIds: Joi.array().items(Joi.number().required()).required(),
        startDate : Joi.date().format("YYYY-MM-DD"),
        endDate : Joi.date().format("YYYY-MM-DD"),
        backendLead: Joi.number().valid(0, 1).optional()
    })
}