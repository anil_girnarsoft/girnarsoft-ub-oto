const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');


const Schema = dbConfig.schema;

const UblmsLeadLog = new Schema ({
    mobile: { type: String, required: false },
    source: { type: String, required: false },
    source_id: { type: Number, required: false },
    added: { type: Date, required: false },
    data: { type: String, required: false }
});


module.exports = dbConfig.mongoosedb.model('UblmsLeadLog', UblmsLeadLog)