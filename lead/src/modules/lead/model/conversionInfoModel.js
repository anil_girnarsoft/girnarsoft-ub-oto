const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class ConversionInfoModel extends Model{
}

ConversionInfoModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    city_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    leads_cars_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    dealer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    dealer:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    make_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    make_name:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    model_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    model_name:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    variant_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    variant_name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    registration_no:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    purchased_price: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    purchased_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    conversion_source: {
        type: dbConfig.Sequelize.TINYINT,
        allowNull: true,
        defaultValue:0
    },
    kms_driven: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    manufacture_year:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    fuel_type:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    buyer_user_type:{
        type: dbConfig.Sequelize.ENUM('P','D'),
        allowNull: true
    },
    seller_user_type:{
        type:dbConfig.Sequelize.ENUM('P','D'),
        allowNull: true
    },
    conversion_comments:{
        type:dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    cnfm_status:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:'0'

    },
    cnfm_status_sec:{
        type:dbConfig.Sequelize.ENUM('1','2','3','4','5'),
        allowNull:true,
        defaultValue:'1'
    },
    cnfm_reason: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true,
        defaultValue:''
    },
    cnfm_comment: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true,
        defaultValue:''

    },
    status: {
        type: dbConfig.Sequelize.TINYINT,
        allowNull: true,
        defaultValue:0
    },
    updated_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    added_on: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    user_type: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true,
        defaultValue:0
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CONVERSION_INFO,
    freezeTableName: true
});


ConversionInfoModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await ConversionInfoModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

ConversionInfoModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await ConversionInfoModel.update(data, { where: { id: id } });
            
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


ConversionInfoModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.added_on = new Date().toISOString();
    else
        dataModel.updated_on = new Date().toISOString();

        return dataModel;
}

ConversionInfoModel.get = async (_where,pagination) => {
    
    let statusCond = " AND (ci.cnfm_status in(1,2,3) or (ci.cnfm_status=0 and ci.status='1'))";
    if(_where.cnfm_status){
        statusCond = " AND (ci.cnfm_status in ("+_where.cnfm_status.join(',')+") and (ci.status='1'))"
    }

    let whereCond = await ConversionInfoModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {

        sql = "SELECT SQL_CALC_FOUND_ROWS ci.id,ci.added_on,ci.make_name, ci.city_id, ci.model_name, ci.variant_name, ci.dealer, uc.name as cluster_name, \n\
        l.id as leadId,ci.cnfm_status_sec,ci.dealer_id,ci.updated_date,ci.car_id,ci.make_id,ci.model_id,ci.variant_id,ci.cnfm_status,ci.cnfm_reason,ci.cnfm_comment,ci.registration_no,cus.name,\n\
        cus.customer_mobile, ci.user_type,ci.updated_by,\n\
        ci.updated_date as updated_date,u.name FROM "+UBLMS_CONVERSION_INFO+" ci \n\
        LEFT JOIN "+UBLMS_LEADS+" l ON ci.lead_id = l.id \n\
        LEFT JOIN "+UBLMS_USER+" u ON u.id = ci.updated_by \n\
        LEFT JOIN "+UBLMS_CITY_CLUSTER+" ucc on ci.city_id=ucc.city_id \n\
        LEFT JOIN "+UBLMS_CLUSTER+" uc on ucc.cluster_id = uc.id \n\
        LEFT JOIN "+UBLMS_CUSTOMER+" cus ON l.customer_id = cus.id ";
        sql += statusCond;
        sql += whereCond;
        
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await ConversionInfoModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}


ConversionInfoModel.bindCondition = async (params,pagination,isPaginationApply) => {
    
    let condition = '',
        sortCond = '';

    let paginationCond = (params.importXls) ? "" : await ConversionInfoModel.bindPagination(params,pagination,isPaginationApply);
    let GROUPBY = 'GROUP BY ci.id, ci.lead_id ORDER BY ci.added_on ASC';
    
    if(params && params.customer_mobile){
        let customerMobile = "cus.customer_mobile ="+(params.customer_mobile).toString();
        condition = (condition == '')? condition + "where "+customerMobile : condition+" and "+customerMobile; 
    }

    if(params && params.id){
        // let customerMobile = "cus.customer_mobile ="+(params.customer_mobile).toString();
        let conversionId = "ci.id='"+params.id+"'";
        condition = (condition == '')? condition + "where "+conversionId : condition+" and "+conversionId; 
    }

    if(params && params.dealer){
        let dealer = "ci.dealer_id='"+params.dealer.value+"'";
        condition = (condition == '')? condition + "where "+dealer : condition+" and "+dealer; 
    }

    if(params && params.startDate){
        let fromDate = dateFormat(params.startDate, "yyyy-mm-dd");
        let toDate = dateFormat(params.endDate, "yyyy-mm-dd");
        // let is_whatsapp_optin = "ci.added_on='"+ params.is_whatsapp_optin+"'";
        let updatedDateCond = "date_format(ci.added_on,'%Y-%m-%d') >= '" + fromDate+ "' and date_format(ci.added_on,'%Y-%m-%d') <= '" + toDate+ "'"
        condition = (condition == '')? condition + "where "+updatedDateCond : condition+" and "+updatedDateCond 
    }

    if(params && params.searchBy && params.search_input){
        let searchBy = (params.searchBy.value === 'leadId') ? "l.id='"+params.search_input+"'" : "cus.customer_mobile='"+params.search_input+"'";
        condition = (condition == '')? condition + "where "+searchBy : condition+" and "+searchBy 
    }

        // $srhCluster = (@$srhData['cluster']?$srhData['cluster']:'');
    	// $srh_city = (@$srhData['srh_city']?$srhData['srh_city']:'');
    	// $srh_dealer = (@$srhData['srh_dealer']?$srhData['srh_dealer']:'');
    	// $srh_from = (@$srhData['srh_addedDateForm']?dateTimeFormat($srhData['srh_addedDateForm']):date('Y-m-d 00:00:00',strtotime("-1 days")));
    	// $srh_to = (@$srhData['srh_addedDateTo']?dateTimeFormat($srhData['srh_addedDateTo'],1):date('Y-m-d 23:59:59',strtotime("-1 days")));
        // $srh_status = (@$srhData['status'] || $srhData['status']=='0' ? $srhData['status'] : '');
        // $selectBy = (@$srhData['selectBy']?$srhData['selectBy']:'');
        // $searchFor = (@$srhData['searchFor']?$srhData['searchFor']:'');
    	// $srhConditions = array();
    	// if(@$srhCluster && !empty($srhCluster)){
    	// 	$srhCluster = implode(',', $srhCluster);
    	// 	$srhConditions[] = " cl.cluster_id IN (".$srhCluster.")";
    	// } else if(isset($AdminUsersAccess['allowed_city_ids']) && !empty($AdminUsersAccess['allowed_city_ids'])) {
        //     $srhConditions[] = " cl.cluster_id IN (".$AdminUsersAccess['allowed_city_ids'].")";
        // }
    	// if(@$srh_city && !empty($srh_city)){
    	// 	$srh_city = implode(',', $srh_city);
    	// 	$srhConditions[] = " ct.city_id IN (".$srh_city.")";
    	// }
    	// if(@$srh_dealer && !empty($srh_dealer)){
    	// 	$srh_dealer = implode(',', $srh_dealer);
    	// 	$srhConditions[] = " d.dealer_id IN (".$srh_dealer.")";
    	// }
    	// if(@$srh_from && @$srh_to){
    	// 	$srhConditions[] = " ci.added_on >='".$srh_from."' and ci.added_on <='".$srh_to."'";
    	// }
        // if((@$srh_status || $srhData['status']=='0')){
        //     $srhConditions[] = " ci.cnfm_status IN(".$srh_status.")";
        // }
        // if (@$selectBy && @$searchFor && @$selectBy=='mobile') {
        //         $srhConditions[] = "  cus.customer_mobile ='".$srhData['searchFor']."'";
        //     }
        //     if (@$selectBy && @$searchFor && @$selectBy=='lead_id') {
        //         $srhConditions[] = "  l.lead_id ='".$srhData['searchFor']."'";
        //     }
    	// if(@$srhConditions && !empty($srhConditions)){
    	// 	$condition = '('.implode(') AND (', $srhConditions).')';
        // }

    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }
    return condition+" "+GROUPBY+" "+sortCond+" "+paginationCond;
}

ConversionInfoModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no    = pagination;
        let limit       = PAGE_LIMIT;
        let page        = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset      = limit * (page - 1);
        condition       = "LIMIT "+offset+","+limit;
    }
    return condition;
}

ConversionInfoModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

ConversionInfoModel.inactivePurchase = async (mainLeadId) => {
    let data = {
        'updated_date' : new Date().toISOString(),
        'status':2
    }
    let _where = {
        'lead_id':mainLeadId
    }

    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const res_data = await ConversionInfoModel.update(data, { where: _where });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

ConversionInfoModel.getConversionInfoToInsert = async (leadsCarsId,leadId) => {

    return new Promise((resolve, reject)=>{
        let sql = "Select lead_id, leads_cars_id, car_id, dealer_id, dealer, make_id, make_name,\n"+
        "model_id, model_name, variant_id, variant_name, registration_no,purchased_price,\n"+
        "purchased_date, conversion_source, kms_driven, manufacture_year, fuel_type, buyer_user_type,\n"+
        "seller_user_type,conversion_comments,cnfm_status,cnfm_reason, cnfm_comment, status,\n"+
        "updated_date, added_on,added_by from "+UBLMS_CONVERSION_INFO+" where leads_cars_id = '"+leadsCarsId+"' and lead_id = '"+leadId+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

ConversionInfoModel.getConverstionStatus = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT cnfm_status, case when (cnfm_status = 1) THEN 'Confirmed' when (cnfm_status = 2) THEN 'Denied' when (cnfm_status = 3) \n"+
        "THEN 'Given Token' ELSE 'Pending' End as confirmStatus, cnfm_reason FROM  " + UBLMS_CONVERSION_INFO + " ci  WHERE ci.lead_id = '"+leadId+"' and status = '1'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}
ConversionInfoModel.getTotalConversion = async (dealer_id, fromDate, toDate) => {
    let replaceValue = [dealer_id];
    return new Promise((resolve, reject) => {
        let sql = `SELECT dealer_id, lead_id FROM ${UBLMS_CONVERSION_INFO} WHERE cnfm_status = 1 AND dealer_id = ? `;
        if (fromDate) {
            replaceValue.push(fromDate);
            sql += ` AND  DATE_FORMAT(DATE(added_on),'%Y-%m-%d') >= ?`;
        }
        if (toDate) {
            replaceValue.push(toDate);
            sql += ` AND  DATE_FORMAT(DATE(added_on),'%Y-%m-%d') <= ?`;
        }
        sql += ` GROUP BY dealer_id , lead_id`;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceValue })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    })
}

module.exports = ConversionInfoModel;