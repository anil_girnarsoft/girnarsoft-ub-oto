const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const { param } = require("express-validator");
const cityController = require('../../city/controller/cityController');

class BlockedRawLeadsLogModel extends Model{
}

BlockedRawLeadsLogModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    raw_lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    mobile:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    blocked_type:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    lead_added_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    added_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    status:{
        type:dbConfig.Sequelize.ENUM('0','1'),
        allowNull: true,
        defaultValue:'0'
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_BLOCKED_RAW_LEADS_LOG,
    freezeTableName: true
});


BlockedRawLeadsLogModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const raw_lwad_data = await BlockedRawLeadsLogModel.create(data);
            resolve(raw_lwad_data);
        } catch (error) {
            reject(error)
        }
    })
}

BlockedRawLeadsLogModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await BlockedRawLeadsLogModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


module.exports = BlockedRawLeadsLogModel;