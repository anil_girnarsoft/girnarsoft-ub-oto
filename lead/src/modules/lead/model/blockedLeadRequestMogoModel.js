const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
// const Model = dbConfig.Sequelize.Model;
// const sequelize = dbConfig.sequelize;
// const Op = dbConfig.Sequelize.Op;

const Schema = dbConfig.schema;

const BlockedLeadRequest = new Schema ({
    request: { type: String, required: false },
    type: { type: String, required: false },
    active: { type: String, required: false },
    response: { type: String, required: false },
    log_date: { type: Date, required: false },
});

module.exports = dbConfig.mongoosedb.model('BlockedLeadRequest', BlockedLeadRequest)
