const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const { param } = require("express-validator");
class rawLeadsAdditionalParamModel extends Model{
}

rawLeadsAdditionalParamModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    raw_lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    customer_mobile:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    source_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    sub_source_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },    
    language:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    lead_source: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    updated_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    lead_platform:{
        type:dbConfig.Sequelize.ENUM('WEB','WAP','AndroidApp','iOSApp','WindowsApp'),
        allowNull: true,
        defaultValue:'0'
    },
    lead_ux_source: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_campaign: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    ad_group_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_content: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_device: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_creative: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_keyword: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_matchtype: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_network: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_placement: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_adposition: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_term: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    location: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    source_lead_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    lead_medium: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    app_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    is_whatsapp: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: true,
        defaultValue:'0'
    },
    otp_verified: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: true,
        defaultValue: '0'
    },
    ip_address: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    featured: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: true,
        defaultValue: '0'
    },
    slot: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    car_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    lead_url: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    landing_url: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    referring_url: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    latitude: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    longitude: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    added_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    connecto_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    sub_city_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },

},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_RAW_LEADS_ADDITIONAL_PARAMS,
    freezeTableName: true
});


rawLeadsAdditionalParamModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const raw_lwad_data = await rawLeadsAdditionalParamModel.create(data);
            resolve(raw_lwad_data);
        } catch (error) {
            reject(error)
        }
    })
}

rawLeadsAdditionalParamModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await rawLeadsAdditionalParamModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

rawLeadsAdditionalParamModel.get = async (_where,pagination) => {

    let whereCond = await rawLeadsAdditionalParamModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    
    return new Promise(function (resolve, reject) {
        // var sql = "SELECT SQL_CALC_FOUND_ROWS id,name,description,created_at,updated_at,added_by,updated_by,status FROM "+UBLMS_ROLES+"\n\
        // "+whereCond;
        
        var sql = "select SQL_CALC_FOUND_ROWS source_id, sub_source_id, lead_source, lead_platform, lead_ux_source, lead_campaign, lead_content, lead_device, lead_creative, lead_keyword, lead_matchtype, lead_network, lead_placement, lead_adposition, lead_term, location, source_lead_id, lead_medium, app_id, featured, slot, car_id, connecto_id FROM ublms_raw_leads_additional_params "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            resolve(result);
        }).catch(function (error) {
            reject(error);
        });
    });
}

rawLeadsAdditionalParamModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond =  await rawLeadsAdditionalParamModel.bindPagination(params,pagination,isPaginationApply);
    let GROUPBY = '';
   
    if(params && params.raw_lead_id){
        let id = "raw_lead_id='"+ params.raw_lead_id +"'";
        condition = (condition == '')? condition + "where "+id : condition+" and "+id 
    }
    
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }
    return condition+" "+GROUPBY+" "+sortCond+" "+paginationCond;
}

rawLeadsAdditionalParamModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

rawLeadsAdditionalParamModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}


module.exports = rawLeadsAdditionalParamModel;