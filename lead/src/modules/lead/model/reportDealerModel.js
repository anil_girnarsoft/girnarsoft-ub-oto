const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const crypto = require("../../../lib/crypto");
const { async } = require("q");

class ReportDealerModel extends Model {
}

ReportDealerModel.getLeadsCount = async (params) => {
  let replaceVal = [];
  replaceVal.push(params.dealer_id);
  replaceVal.push(params.days);
  if (params.front_end_lead) {
    replaceVal.push(0);
  }
  if (params.back_end_lead) {
    replaceVal.push(0);
  }
  return new Promise((resolve, reject) => {
    let sql = `SELECT COUNT(ulc.id) AS total
              FROM ${UBLMS_LEADS_CARS} ulc
              INNER JOIN ${UBLMS_DEALER_PRIORITY} udcp ON udcp.car_id = ulc.car_id
              WHERE udcp.dealer_id = ? AND DATEDIFF(CURDATE() , ulc.created_at) <= ?`;
    if (params.front_end_lead) {
      sql += ` AND ulc.added_by = ?`;
    }
    if (params.back_end_lead) {
      sql += ` AND ulc.added_by > ?`;
    }
    AppModel.dbObj.sequelize
      .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceVal })
      .then(result => {
        resolve(result);
      }).catch(error => {
        reject(error);
      });
  })
}

ReportDealerModel.getInventoryCount = async (params) => {
  let replaceVal = [];  
  replaceVal.push(params.dealer_id);
  replaceVal.push(params.car_status);
  replaceVal.push(params.is_classified);
  if(params.days) replaceVal.push(params.days);
  if(params.mtd) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var current_date = yyyy + '-' + mm + '-' + dd;
    var first_month_date = yyyy + '-' + mm + '-01';
    replaceVal.push(first_month_date);
    replaceVal.push(current_date);    
  }
  let sql = `SELECT COUNT(id) AS total
            FROM ${UBLMS_DEALER_PRIORITY}
            WHERE dealer_id = ? AND car_status = ?
            AND is_classified = ?`;
  if (params.days) {
    sql += ` AND DATEDIFF(CURDATE(), updated_at) <= ?`;
  }
  if(params.mtd) {
    sql += ` AND DATE_FORMAT(updated_at, '%Y-%m-%d') >= ? AND DATE_FORMAT(updated_at, '%Y-%m-%d') <= ?`;
  }
  return new Promise((resolve, reject) => {
    AppModel.dbObj.sequelize
      .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceVal })
      .then(result => {
        resolve(result);
      }).catch(error => {
        reject(error);
      });
  });
}

ReportDealerModel.getUniqueInventory = async (params) => {
  let replaceVal = [];
  replaceVal.push(params.dealer_id);
  replaceVal.push(params.days);

  let sql = `SELECT COUNT(distinct ulc.car_id) AS total
            FROM ${UBLMS_LEADS_CARS} ulc
            INNER JOIN ${UBLMS_DEALER_PRIORITY} udcp ON udcp.car_id = ulc.car_id
            WHERE udcp.dealer_id = ? AND DATEDIFF(CURDATE(), ulc.created_at) <= ?`;
            return new Promise((resolve, reject) => {
              AppModel.dbObj.sequelize
                .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceVal })
                .then(result => {
                  resolve(result);
                }).catch(error => {
                  reject(error);
                });
            });            
}

ReportDealerModel.getLeadTime = async (params) => {
  let replaceVal = [];
  let start_date = params.start_date;
  let expire_date = params.expire_date;
  let sql = `SELECT ulc.created_at AS lead_date
            FROM ${UBLMS_LEADS_CARS} ulc
            INNER JOIN ${UBLMS_DEALER_PRIORITY} udcp ON udcp.car_id = ulc.car_id
            WHERE DATE_FORMAT(ulc.created_at, '%Y-%m-%d') >= '${start_date}' AND DATE_FORMAT(ulc.created_at, '%Y-%m-%d') <= '${expire_date}' AND udcp.dealer_id=${params.dealer_id}`;
  if(params.package_car_ids){
    sql +=` AND ulc.car_id IN (${params.package_car_ids})`; 
  }            
  if(params.first_lead) {
    sql += ` ORDER BY ulc.id ASC LIMIT 1`;
  } 
  if(params.last_lead) {
    sql += ` ORDER BY ulc.id DESC LIMIT 1`;
  }             
            return new Promise((resolve, reject) => {
              AppModel.dbObj.sequelize
                .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
                .then(result => {
                  resolve(result);
                }).catch(error => {
                  reject(error);
                });
            });

}

ReportDealerModel.getOrganicLeads = async (params) => {
  let replaceVal = [];
  replaceVal.push(params.dealer_id);
  if(params.days) replaceVal.push(params.days);
  if(params.mtd) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var current_date = yyyy + '-' + mm + '-' + dd;
    var first_month_date = yyyy + '-' + mm + '-01';    
    replaceVal.push(first_month_date);
    replaceVal.push(current_date);
  }
  let sql = `SELECT COUNT(l.id) AS total
            FROM ${UBLMS_LEADS_CARS} l
            INNER JOIN ${UBLMS_DEALER_PRIORITY} dc ON l.car_id = dc.car_id
            WHERE l.added_by = 0 AND dc.dealer_id = ? `;
  if(params.days) {
    sql += ` AND DATEDIFF(CURDATE(), l.created_at) <= ?`; 
  }
  if(params.mtd) {
    sql += ` AND DATE_FORMAT(l.created_at, '%Y-%m-%d') >= ? AND DATE_FORMAT(l.created_at, '%Y-%m-%d') <= ?`;
  }            
            return new Promise((resolve, reject) => {
              AppModel.dbObj.sequelize
                .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceVal })
                .then(result => {
                  resolve(result);
                }).catch(error => {
                  reject(error);
                });
            }); 

}

ReportDealerModel.getUniqueBackendLeads = async (params) => {  
  let replaceVal = [];
  replaceVal.push(params.dealer_id);
  if(params.mtd) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var current_date = yyyy + '-' + mm + '-' + dd;
    var first_month_date = yyyy + '-' + mm + '-01';   
    replaceVal.push(first_month_date);
    replaceVal.push(current_date);
  }
  let sql = `SELECT COUNT(DISTINCT ulc.lead_id) AS total
              FROM ${UBLMS_LEADS_CARS} ulc
              INNER JOIN ${UBLMS_DEALER_PRIORITY} udcp ON udcp.car_id = ulc.car_id
              WHERE udcp.dealer_id = ?  AND ulc.added_by > 0`;
  if(params.mtd) {
    sql += ` AND DATE_FORMAT(ulc.created_at, '%Y-%m-%d') >= ? AND DATE_FORMAT(ulc.created_at, '%Y-%m-%d') <= ?`;
  }   
  return new Promise((resolve, reject) => {
    AppModel.dbObj.sequelize
      .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceVal })
      .then(result => {
        resolve(result);
      }).catch(error => {
        reject(error);
      });
  });            
}
module.exports = ReportDealerModel;