const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');


const Schema = dbConfig.schema;

const ConversionPanelExportLogModel = new Schema ({
    downloaded_by: { type: Number, required: false },
    download_filename: { type: String, required: false },
    records_write: { type: Number, required: false },
    added_on: { type: Date, required: false },
});

module.exports = dbConfig.mongoosedb.model('ConversionPanelExportLogModel', ConversionPanelExportLogModel)
