const { param } = require("express-validator");
const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
var AppModel = require(COMMON_MODEL + 'appModel');
const crypto = require("../../../lib/crypto");

class FacebookFormModel extends Model {
}

FacebookFormModel.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    form_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false,
    },
    status: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    }
}, {
    sequelize,
    timestamps: false,
    modelName: UBLMS_FACEBOOK_FORM,
    freezeTableName: true
});


FacebookFormModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await FacebookFormModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

FacebookFormModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await FacebookFormModel.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

FacebookFormModel.get = async (_where, pagination) => {

    let isPaginationApply = (typeof _where.isPaginationApply !== 'undefined') ? _where.isPaginationApply : true;
    let whereCond = await FacebookFormModel.bindCondition(_where, pagination, isPaginationApply)  //true is used to apply pagination limit

    return new Promise(function (resolve, reject) {
        // var sql = "select SQL_CALC_FOUND_ROWS uff.form_id,uff.id,uff.status,\n" +
        //     " uff.created_at,uff.updated_at, uflt.prev_cursor, uflt.next_cursor, uflt.prev_token, uflt.next_token from " + UBLMS_FACEBOOK_FORM + " as uff LEFT JOIN (SELECT * from "+ UBLMS_FACEBOOK_LEAD_TOKEN + " where  from_date LIKE '%"+_where.fromDate+"%' ORDER BY id DESC LIMIT 1) as uflt ON uflt.facebook_form_id = uff.id" + whereCond;
        var sql = "select SQL_CALC_FOUND_ROWS uff.form_id,uff.id,uff.status,\n" +
            " uff.created_at,uff.updated_at, uflt.prev_cursor, uflt.next_cursor, uflt.prev_token, uflt.next_token, uflt.created_at as lead_from_created_date , uflt.from_date as lead_from_date from " + UBLMS_FACEBOOK_FORM + " as uff LEFT JOIN (SELECT * from "+ UBLMS_FACEBOOK_LEAD_TOKEN + " where  Date(from_date) = '"+(_where.fromDate)+"'  ORDER BY id DESC LIMIT 1) as uflt ON uflt.facebook_form_id = uff.id" + whereCond;
            
            AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {

                ////// map with car details for car price ////

                let totalCount = await FacebookFormModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}

FacebookFormModel.bindCondition = async (params, pagination, isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond = await FacebookFormModel.bindPagination(params, pagination, isPaginationApply);
    let GROUPBY = ' GROUP BY uff.id';

    if (params && params.form_id)
        condition = " where uff.form_id = '" + params.form_id + "' ";

    if (params && params.sort && params.sort.length) {
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort, (srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort + ', ' + sortData : sortData
        })
        sortCond = (finalSort != '') ? 'ORDER BY ' + finalSort : '';
    }
    return condition + " " + GROUPBY + " " + sortCond + " " + paginationCond;
}

FacebookFormModel.bindPagination = (params, pagination, isPaginationApply) => {
    let condition = '';
    if (isPaginationApply) {
        let page_no = pagination;
        let limit = params.page_limit || PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT " + offset + "," + limit;
    }
    return condition;
}

FacebookFormModel.getRowsCount = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result[0].total);
            }).catch(error => {
                reject(error);
            });
    })
}


FacebookFormModel.encrypt = (data) => {
    if (Array.isArray(data)) data = data.map(v => {
        v.lead_id_hash = (v && v.leadId != null) ? crypto.encode(v.leadId) : ((v && v.id != null) ? crypto.encode(v.id) : '');
        return v;
    })
    else if (data && data.leadId) data.lead_id_hash = crypto.encode(data.leadId);
    else if (data && data.id) data.lead_id_hash = crypto.encode(data.id);
    return data;
}

FacebookFormModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

module.exports = FacebookFormModel;
