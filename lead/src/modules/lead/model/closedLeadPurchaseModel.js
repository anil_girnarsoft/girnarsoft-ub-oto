const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const AsyncLeadModel = require('./asyncLeadModel');
class ClosedLeadPurchaseModel extends Model{
}

ClosedLeadPurchaseModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    lead_car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    closed_sub_status_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    assigned:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    price:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    model_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    make_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    reg:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    reg_car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    dealer_name:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    city_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    isBroker: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:2
    },
    purchase_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    reg_number: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    by_whom: {
        type: dbConfig.Sequelize.ENUM('0','1','2','3'),
        allowNull: false,
        defaultValue:'0'
    },
    // car_detail: {
    //     type: dbConfig.Sequelize.TEXT,
    //     allowNull: true
    // },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
        
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CLOSED_LEAD_PURCHASE,
    freezeTableName: true
});


ClosedLeadPurchaseModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await AsyncLeadModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

ClosedLeadPurchaseModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await AsyncLeadModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

ClosedLeadPurchaseModel.getClosedLeadPurchase = (leadId) => {
    return new Promise((resolve, reject)=>{
        let sql = "Select * from " + UBLMS_CLOSED_LEAD_PURCHASE + " where lead_id = '"+leadId+"' ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

ClosedLeadPurchaseModel.checkClosedLeadSubStatus = (leadId) => {
    return new Promise((resolve, reject)=>{
        let sql = "SELECT closed_sub_status_id FROM " + UBLMS_CLOSED_LEAD_PURCHASE + " where  lead_id = '"+leadId+"' order by id DESC limit 0,1";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            let sub_status_id = '';
            if((result && result.length && result[0]['closed_sub_status_id'])) {
                sub_status_id =  result['0']['closed_sub_status_id'];
            }
            resolve(sub_status_id);
        }).catch(error=>{
            reject(error);
        });
    })
}

module.exports = ClosedLeadPurchaseModel;