const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class RecomendCarSearchLogLogModel extends Model{
}

RecomendCarSearchLogLogModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    user_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    custmer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    min_price:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    max_price:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    make_id:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    model_id:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    min_year:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    max_year:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    max_km:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    city:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true,
    },
    dealer_id: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true,
        defaultValue:'0'
    },
    fuel_type:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    owner: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    color: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    body_type: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    transmission: {
        type:dbConfig.Sequelize.STRING,
        allowNull: true
        
    },
    filter_certified:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    filter_picture:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    filter_dealer:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    filter_paid_individual:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    filter_free_individual:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_RECOMEND_CAR_SEARCH_LOG,
    freezeTableName: true
});


RecomendCarSearchLogLogModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const raw_lwad_data = await RecomendCarSearchLogLogModel.create(data);
            resolve(raw_lwad_data);
        } catch (error) {
            reject(error)
        }
    })
}

RecomendCarSearchLogLogModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await RecomendCarSearchLogLogModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

RecomendCarSearchLogLogModel.getCarSearchInfoToInsert = async(customerId,leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select user_id, lead_id, custmer_id, min_price, max_price, make_id, model_id,\n"+
        "min_year, max_km, city, dealer_id, fuel_type, owner, color, body_type, transmission, \n"+
        "filter_certified, filter_picture, filter_dealer, filter_paid_individual, filter_free_individual,\n"+
        "created_at from "+UBLMS_RECOMEND_CAR_SEARCH_LOG+" where custmer_id = '"+customerId+"' and lead_id = '"+leadId+"' "
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

module.exports = RecomendCarSearchLogLogModel;