const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const { param } = require("express-validator");
class OtherDealerLeadsModel extends Model{
}

OtherDealerLeadsModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    raw_lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    dealer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    lead_mobile:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },    
    owner_type:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    added_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    status:{
        type:dbConfig.Sequelize.ENUM('0','1'),
        allowNull: true,
        defaultValue:'0'
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_OTHER_DEALER_LEADS,
    freezeTableName: true
});


OtherDealerLeadsModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const raw_lwad_data = await OtherDealerLeadsModel.create(data);
            resolve(raw_lwad_data);
        } catch (error) {
            reject(error)
        }
    })
}

OtherDealerLeadsModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await OtherDealerLeadsModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


module.exports = OtherDealerLeadsModel;