const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
var AppModel = require(COMMON_MODEL + 'appModel');
const crypto = require("../../../lib/crypto");

class FacebookLeadTokenModel extends Model {
}

FacebookLeadTokenModel.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    facebook_form_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false,
    },
    from_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    prev_cursor: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    next_cursor: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    prev_token: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    next_token: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    response: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    }
}, {
    sequelize,
    timestamps: false,
    modelName: UBLMS_FACEBOOK_LEAD_TOKEN,
    freezeTableName: true
});


FacebookLeadTokenModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await FacebookLeadTokenModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

FacebookLeadTokenModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await FacebookLeadTokenModel.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

FacebookLeadTokenModel.get = async (_where, pagination) => {

    let isPaginationApply = (typeof _where.isPaginationApply !== 'undefined') ? _where.isPaginationApply : true;
    let whereCond = await FacebookLeadTokenModel.bindCondition(_where, pagination, isPaginationApply)  //true is used to apply pagination limit

    return new Promise(function (resolve, reject) {
        var sql = "select SQL_CALC_FOUND_ROWS uflt.facebook_form_id,uflt.id,uflt.prev_cursor,\n" +
            " uflt.next_cursor,uflt.prev_token,uflt.next_token from " + UBLMS_FACEBOOK_LEAD_TOKEN + " as uflt " + whereCond;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {

                ////// map with car details for car price ////

                let totalCount = await FacebookLeadTokenModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}

FacebookLeadTokenModel.bindCondition = async (params, pagination, isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond = await FacebookLeadTokenModel.bindPagination(params, pagination, isPaginationApply);
    let GROUPBY = 'GROUP BY uflt.id';

    if (params && params.form_id)
        condition = "where uflt.form_id = '" + params.form_id + "'";

    if (params && params.sort && params.sort.length) {
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort, (srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort + ', ' + sortData : sortData
        })
        sortCond = (finalSort != '') ? 'ORDER BY ' + finalSort : '';
    }
    return condition + " " + GROUPBY + " " + sortCond + " " + paginationCond;
}

FacebookLeadTokenModel.bindPagination = (params, pagination, isPaginationApply) => {
    let condition = '';
    if (isPaginationApply) {
        let page_no = pagination;
        let limit = params.page_limit || PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT " + offset + "," + limit;
    }
    return condition;
}

FacebookLeadTokenModel.getRowsCount = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result[0].total);
            }).catch(error => {
                reject(error);
            });
    })
}


FacebookLeadTokenModel.encrypt = (data) => {
    if (Array.isArray(data)) data = data.map(v => {
        v.lead_id_hash = (v && v.leadId != null) ? crypto.encode(v.leadId) : ((v && v.id != null) ? crypto.encode(v.id) : '');
        return v;
    })
    else if (data && data.leadId) data.lead_id_hash = crypto.encode(data.leadId);
    else if (data && data.id) data.lead_id_hash = crypto.encode(data.id);
    return data;
}

FacebookLeadTokenModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

module.exports = FacebookLeadTokenModel;
