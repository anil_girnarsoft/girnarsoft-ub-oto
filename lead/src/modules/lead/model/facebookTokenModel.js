const { param } = require("express-validator");
const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
var AppModel = require(COMMON_MODEL + 'appModel');
const crypto = require("../../../lib/crypto");

class FacebookTokenModel extends Model {
}

FacebookTokenModel.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    app_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false,
    },
    app_secret: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    access_token: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    expires_in: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    }
}, {
    sequelize,
    timestamps: false,
    modelName: UBLMS_FACEBOOK_TOKEN,
    freezeTableName: true
});


FacebookTokenModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await FacebookTokenModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

FacebookTokenModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await FacebookTokenModel.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

FacebookTokenModel.get = async (_where, pagination) => {

    let whereCond = await FacebookTokenModel.bindCondition(_where, pagination, true)  //true is used to apply pagination limit

    return new Promise(function (resolve, reject) {
        var sql = "select SQL_CALC_FOUND_ROWS uft.app_id,uft.id,uft.access_token,uft.app_secret,\n" +
            " uft.created_at,uft.updated_at from " + UBLMS_FACEBOOK_TOKEN + " as uft " + whereCond;
            
            AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {

                ////// map with car details for car price ////

                let totalCount = await FacebookTokenModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}

FacebookTokenModel.bindCondition = async (params, pagination, isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond = await FacebookTokenModel.bindPagination(params, pagination, isPaginationApply);
    let GROUPBY = ' GROUP BY uft.id';

    if (params && params.access_token)
        condition = " where uft.access_token = '" + params.access_token + "' ";

    if (params && params.sort && params.sort.length) {
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort, (srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort + ', ' + sortData : sortData
        })
        sortCond = (finalSort != '') ? 'ORDER BY ' + finalSort : '';
    }
    return condition + " " + GROUPBY + " " + sortCond + " " + paginationCond;
}

FacebookTokenModel.bindPagination = (params, pagination, isPaginationApply) => {
    let condition = '';
    if (isPaginationApply) {
        let page_no = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT " + offset + "," + limit;
    }
    return condition;
}

FacebookTokenModel.getRowsCount = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result[0].total);
            }).catch(error => {
                reject(error);
            });
    })
}


FacebookTokenModel.encrypt = (data) => {
    if (Array.isArray(data)) data = data.map(v => {
        v.lead_id_hash = (v && v.leadId != null) ? crypto.encode(v.leadId) : ((v && v.id != null) ? crypto.encode(v.id) : '');
        return v;
    })
    else if (data && data.leadId) data.lead_id_hash = crypto.encode(data.leadId);
    else if (data && data.id) data.lead_id_hash = crypto.encode(data.id);
    return data;
}

FacebookTokenModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

module.exports = FacebookTokenModel;
