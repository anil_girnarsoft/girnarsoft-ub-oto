const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
var AppModel = require(COMMON_MODEL + 'appModel');
const crypto = require("../../../lib/crypto");
const LeadDialerHistoryModel = require("./leadDialerHistoryModel");

class LeadDialerModel extends Model {
}

LeadDialerModel.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    lead_id_hash: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    list_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
    },
    dialer_type: {
        type: dbConfig.Sequelize.ENUM('L1','L2','L3'),
        allowNull: false,
    },
    dialer_response: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true,
    },
    phone: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    status: {
        type: dbConfig.Sequelize.ENUM('pending', 'processed', 'failed'),
        allowNull: false,
        defaultValue: '0'
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    attempt: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    last_attempt: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
}, {
    sequelize,
    timestamps: false,
    modelName: UBLMS_LEAD_DIALER,
    freezeTableName: true
});


LeadDialerModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadDialerModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

LeadDialerModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadDialerModel.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

LeadDialerModel.get = async (_where, pagination) => {

    let isPaginationApply = (typeof _where.isPaginationApply !== 'undefined') ? _where.isPaginationApply : true;
    let whereCond = await LeadDialerModel.bindCondition(_where, pagination, isPaginationApply)  //true is used to apply pagination limit

    return new Promise(function (resolve, reject) {
        var sql = "select SQL_CALC_FOUND_ROWS uld.lead_id,uld.id,uld.lead_id_hash,uld.dialer_response, uld.list_id,uld.dialer_type,uld.dialer_response,\n" +
            "uld.status,uld.created_at, uld.attempt,uld.last_attempt from " + UBLMS_LEAD_DIALER + " as uld " + whereCond;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {

                ////// map with car details for car price ////

                let totalCount = await LeadDialerModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}

LeadDialerModel.getModel = (params) => {
    let dataModel = {};
    if (!params.id)
        dataModel.created_at = new Date().toISOString();
    if (params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if ((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 'pending';
    if (params.lead_id)
        dataModel.name = params.lead_id;
    if (params.lead_id_hash)
        dataModel.lead_id_hash = params.lead_id_hash;
    if (params.list_id)
        dataModel.list_id = params.list_id
    if (params.dialer_type)
        dataModel.dialer_type = params.dialer_type;
    if (params.dialer_response)
        dataModel.dialer_type = params.dialer_response;
    return dataModel;
}

LeadDialerModel.bindCondition = async (params, pagination, isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond = await LeadDialerModel.bindPagination(params, pagination, isPaginationApply);
    let GROUPBY = 'GROUP BY uld.id';
    
    if (params && params.lead_id)
        condition = "where uld.lead_id = '" + params.lead_id + "'";

    if (params && params.created_at){
        condition = "where date_format(uld.created_at,'%Y-%m-%d') = '" + params.created_at +"'";
    }

    if (params && params.sort && params.sort.length) {
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort, (srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort + ', ' + sortData : sortData
        })
        sortCond = (finalSort != '') ? 'ORDER BY ' + finalSort : '';
    }
    return condition + " " + GROUPBY + " " + sortCond + " " + paginationCond;
}

LeadDialerModel.bindPagination = (params, pagination, isPaginationApply) => {
    let condition = '';
    if (isPaginationApply) {
        let page_no = pagination;
        let limit = params.page_limit || PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT " + offset + "," + limit;
    }
    return condition;
}

LeadDialerModel.getRowsCount = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result[0].total);
            }).catch(error => {
                reject(error);
            });
    })
}


LeadDialerModel.encrypt = (data) => {
    if (Array.isArray(data)) data = data.map(v => {
        v.lead_id_hash = (v && v.leadId != null) ? crypto.encode(v.leadId) : ((v && v.id != null) ? crypto.encode(v.id) : '');
        return v;
    })
    else if (data && data.leadId) data.lead_id_hash = crypto.encode(data.leadId);
    else if (data && data.id) data.lead_id_hash = crypto.encode(data.id);
    return data;
}

LeadDialerModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

LeadDialerModel.getDialerLeadPendingStatus = (params) => {
    return new Promise(function (resolve, reject) {
        var sql = "select uld.lead_id,uld.id,uld.lead_id_hash,uld.dialer_response, uld.list_id,uld.dialer_type,uld.dialer_response,\n" +
            "uld.status,uld.created_at, uld.attempt,uld.last_attempt,uld.phone, ul.id as leadId,ul.dialer_status, cus.name, cus.customer_city_name, cus.customer_email from " + UBLMS_LEAD_DIALER + " as uld LEFT JOIN " + UBLMS_LEADS + " ul ON ul.id = uld.lead_id LEFT JOIN " +UBLMS_CUSTOMER+ " as cus ON cus.id = ul.customer_id where uld.status = 'pending' limit 0, "+params.page_limit;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {
                let totalCount = await LeadDialerModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}



LeadDialerModel.getLeadIds = (params) => {
    return new Promise(function (resolve, reject) {
        var sql = "select lead_id from " + UBLMS_LEAD_DIALER;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {
                resolve(result);
            }).catch(function (error) {
                reject(error);
            });
    });
}


module.exports = LeadDialerModel;
