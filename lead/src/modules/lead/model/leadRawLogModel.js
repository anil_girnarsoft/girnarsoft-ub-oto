const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const { param } = require("express-validator");
const cityController = require('../../city/controller/cityController');
const leadLogMongoModel = require('./rawLeadMongoModel');
const { async, resolve } = require("q");

class LeadRawLogModel extends Model {
}

LeadRawLogModel.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    source_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    sub_source_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    mobile: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    leads_data: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    lead_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    reason: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    log_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    updated_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    response: {
        type: dbConfig.Sequelize.TEXT('tiny'),
        allowNull: true,
        defaultValue: ''
    },
    type: {
        type: dbConfig.Sequelize.INTEGER('tiny'),
        allowNull: true,
    },
    dealer_id: {
        type: dbConfig.Sequelize.BIGINT(50),
        allowNull: true,
        defaultValue: '0'
    },
    dc_pushed_status: {
        type: dbConfig.Sequelize.ENUM('0', '1', '2'),
        allowNull: true,
        defaultValue: '0'
    },
    pushed_status: {
        type: dbConfig.Sequelize.ENUM('0', '1', '2', '3'),
        allowNull: true,
        defaultValue: '0'
    },
    dc_response: {
        type: dbConfig.Sequelize.STRING(240),
        allowNull: true,
        defaultValue: ''
    },
    city_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    apitype: {
        type: dbConfig.Sequelize.ENUM('0', '1'),
        allowNull: true,
        defaultValue: '0'

    },
    is_ub: {
        type: dbConfig.Sequelize.ENUM('0', '1'),
        allowNull: true,
        defaultValue: '0'
    },
    connecto_id: {
        type: dbConfig.Sequelize.STRING(60),
        allowNull: true,
    },
    source_key: {
        type: dbConfig.Sequelize.STRING(200),
        allowNull: true,
        defaultValue: ''
    },
    blocking_type: {
        type: dbConfig.Sequelize.INTEGER('tiny'),
        allowNull: true,
    },
    lead_status: {
        type: dbConfig.Sequelize.ENUM('0', '1', '2'),
        allowNull: true,
        defaultValue: '0'
    }
}, {
    sequelize,
    timestamps: false,
    modelName: UBLMS_RAW_LEADS_LOG,
    freezeTableName: true
});


LeadRawLogModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const raw_lwad_data = await LeadRawLogModel.create(data);
            resolve(raw_lwad_data);
        } catch (error) {
            reject(error)
        }
    })
}

LeadRawLogModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadRawLogModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

LeadRawLogModel.getModel = (params) => {
    let dataModel = {};
    if (!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if (params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if ((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
    if (params.source_id)
        dataModel.source_id = params.source_id;
    if (params.sub_source_id)
        dataModel.sub_source_id = params.sub_source_id;
    if (params.city_id)
        dataModel.city_id = params.city_id;
    if (params.mobile)
        dataModel.mobile = params.mobile
    if (params.updated_by)
        dataModel.updated_by = params.updated_by;
    return dataModel;
}

LeadRawLogModel.checktotalleads = async () => {
    let toDate = dateFormat("yyyy-mm-dd HH:MM:ss");
    let fromDate = dateFormat(new Date(new Date().setMinutes(new Date().getMinutes() - 10)), 'yyyy-mm-dd HH:MM:ss'); //10 minutes old time

    return new Promise((resolve, reject) => {
        var sql = "SELECT count(id) as total from " + UBLMS_RAW_LEADS_LOG + " where log_date>? and log_date<=?";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [fromDate, toDate] })
            .then(result => {
                resolve(result[0].total);
            }).catch((err) => {
                reject(err);
            })
    });
}

LeadRawLogModel.unprocessedLeads = async () => {
    return new Promise((resolve, reject) => {
        var sql = "select * from " + UBLMS_RAW_LEADS_LOG + " where pushed_status='0' order by id asc limit 30";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result);
            }).catch((err) => {
                reject(err);
            })
    });
}

LeadRawLogModel.updateMultiple = async (params) => {
    return new Promise((resolve, reject) => {
        let _where = '';
        let _update = '';

        if (params && params.id) {
            _where = "id in (" + params.id + ")";
        }

        if (params && params.pushedStatus) {
            _update = "pushed_status = '" + params.pushedStatus + "'";
        }

        var sql = "update " + UBLMS_RAW_LEADS_LOG + " set " + _update + "  where " + _where;

        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.UPDATE })
            .then(result => {
                resolve(result);
            }).catch((err) => {
                reject(err);
            })
    });
}

LeadRawLogModel.get = async (_where, pagination) => {

    let whereCond = await LeadRawLogModel.bindCondition(_where, pagination, true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM \n\
        "+ UBLMS_RAW_LEADS_LOG + " " + whereCond;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {
                let totalCount = await LeadRawLogModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}

LeadRawLogModel.bindCondition = async (params, pagination, isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond = await LeadRawLogModel.bindPagination(params, pagination, isPaginationApply);

    if (params && params.id) {
        let id = "id='" + LeadRawLogModel.decrypt(params.id) + "'";
        condition = (condition == '') ? condition + "where " + id : condition + " and " + id
    }

    if (params && params.idIn) {
        let id = "id IN (" + params.idIn + ")";
        condition = (condition == '') ? condition + "where " + id : condition + " and " + id
    }

    if (params && params.pushed_status) {
        let pushed_status = "pushed_status='" + params.pushed_status + "'";
        condition = (condition == '') ? condition + "where " + pushed_status : condition + " and " + pushed_status
    }

    if (params && params.sort && params.sort.length) {
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort, (srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort + ', ' + sortData : sortData
        })
        sortCond = (finalSort != '') ? 'ORDER BY ' + finalSort : '';
    }
    return condition + " " + sortCond + " " + paginationCond;
}

LeadRawLogModel.bindPagination = (params, pagination, isPaginationApply) => {
    let condition = '';
    if (isPaginationApply) {
        let page_no = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT " + offset + "," + limit;
    }
    return condition;
}

LeadRawLogModel.getRowsCount = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result[0].total);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadRawLogModel.filerCustomerMobile = async (req, res, next, leadLogData, apiType) => {
    let leadData = {};
    let cityData = {};

    if (apiType == 1) {
        customerMobile = leadLogData['customer']['mobile'];
        customerName = leadLogData['customer']['name'];
        customerEmail = leadLogData['customer']['email'];
        customerCentralCityId = leadLogData['customer']['city_id'];

        if (customerCentralCityId > 0) {
            // customerRes = $this->getCityNameByCentralCityID($customer_central_city_id);
            req.body['cb_function'] = true;
            let cityList = await cityController.getCityStateList(req, res, next);


            if (Object.keys(cityList).length && cityList['city']) {
                cityList['city'].forEach(el => { if (el.id == customerCentralCityId) { cityData = el; } });
            }
            // console.log('cityData', cityData);
            customerCity = cityData['name'];
            //$city_ic = $customer_res['city_id'];
            leadData['customer_name'] = customerName;
            leadData['customer_email'] = customerEmail;
            leadData['customer_city'] = customerCity;
        }
    } else {
        leadData['customer_name'] = leadLogData['customer_name'] || '';
        leadData['customer_email'] = leadLogData['customer_email'] || '';
        leadData['customer_city'] = leadLogData['customer_city'] || '';
    }
    return leadData;
}

LeadRawLogModel.insertIntoUblmsLeadsLogMongo = async (logData) => {
    let arrResCardekho = {};
    leadLogMongoModel.create(logData, function (err, res) {
        if (err) {
            //   throw err
        }

    });

    return (arrResCardekho);
}

LeadRawLogModel.insertDncRawLeadMapping = async (logid) => {
    return new Promise((resolve, reject) => {
        let sql = "INSERT INTO " + UBLMS_DNC_RAW_LEAD_MAPPING + " (raw_lead_log_id) VALUES (?)";
        AppModel.dbObj.sequelize
            .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: [logid.raw_lead_log_id] })
            .then((insertLead) => {
                resolve(insertLead[0]);
            }).catch(error => {
                reject(error)
            });
    });
}

LeadRawLogModel.getCustomerDetail = async (mobile) => {
    try {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM " + UBLMS_CUSTOMER + " WHERE customer_mobile=?";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [mobile] })
                .then((res) => {
                    resolve(res);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {

    }
}

LeadRawLogModel.getCityId = async (city_name) => {
    try {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM " + UBLMS_CITY + " WHERE name=?";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [city_name] })
                .then((res) => {
                    resolve(res[0]);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {

    }
}

LeadRawLogModel.getCityIdmapping = async (city_name) => {
    try {
        return new Promise((resolve, reject) => {
            let sql = "SELECT c.city_id,c.city_name FROM " + UBLMS_CITY_MAPPING + " cm LEFT JOIN " + UBLMS_CITY + " c ON c.city_id=cm.city_id WHERE cm.city_name=?";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [city_name] })
                .then((res) => {
                    resolve(res[0]);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {

    }
}
LeadRawLogModel.getLocationId = async (customer_location) => {
    try {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM " + UBLMS_LOCATIONS + " WHERE location_name=? AND active =1";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [customer_location] })
                .then((res) => {
                    resolve(res);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {

    }
}

LeadRawLogModel.insertCustomer = async (data) => {
    try {
        return new Promise((resolve, reject) => {
            let sql = "INSERT INTO " + UBLMS_CUSTOMER + " (name, customer_city_id, customer_city_name, customer_email, customer_mobile, alternate_email, alternate_mobile, customer_location_id, customer_location_name, customer_added, status, ndnc) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: [data.customer_name, data.customer_city_id, data.customer_city_name, data.customer_email, data.customer_mobile, data.alternate_email, data.alternate_mobile, data.customer_location_id, data.customer_location_name, data.customer_added, data.active, data.ndnc] })
                .then((insertLead) => {
                    resolve(insertLead[0]);
                }).catch(error => {
                    reject(error)
                });
        });
    } catch (error) {

    }
}

LeadRawLogModel.getCustomersLeadDetail = async (customer_id) => {
    try {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM " + UBLMS_LEADS + " WHERE customer_id=?";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [customer_id] })
                .then((res) => {
                    resolve(res[0]);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {

    }
}
LeadRawLogModel.updateCustomer = async (data) => {
    try {        
        let custObj = data.custData;
        let allKeys = Object.keys(custObj);
        let columns = allKeys.join("=?,");
        let allValues = Object.values(custObj);
        allValues.push(data.id);        
        return new Promise((resolve, reject) => {
            let update_customer = `UPDATE ${UBLMS_CUSTOMER} SET ${columns}=? WHERE id=?`;
            AppModel.dbObj.sequelize
                .query(update_customer, { type: AppModel.dbObj.Sequelize.QueryTypes.UPDATE, replacements: allValues})
                .then(updateCust => {
                    resolve(updateCust);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {
        // console.log('Error in updateCustomer----', error);
    }
}
LeadRawLogModel.getBlockingInfo = async (customer_id) => {
    try {
        return new Promise((resolve, reject) => {
            let sql = "select * from " + UBLMS_CLOSED_BLOCKED_LEADS + " where customer_id = ? order by id DESC limit 0,1";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [customer_id] })
                .then(result => {
                    resolve(result[0]);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {

    }
}
LeadRawLogModel.updateLeads = async (updateData, cond) => {
    try {
        let allKeys = Object.keys(updateData);
        let columns = allKeys.join("=?,");
        let allValues = Object.values(updateData);
        return new Promise((resolve, reject) => {
            let update_sql = "update " + UBLMS_LEADS + " set " + columns + "=? where id=" + cond.lead_id;
            AppModel.dbObj.sequelize
                .query(update_sql, { type: AppModel.dbObj.Sequelize.QueryTypes.UPDATE, replacements: allValues })
                .then(updateLead => {
                    resolve(updateLead);
                }).catch(error => {
                    reject(error);
                });
        });
    } catch (error) {
        // console.log('error:', error);
    }
}

const variableToUpdate = async (arr) => {
    let replacementVal = [];
    let allKeys = Object.keys(arr);
    let updateKeys = [];
    let return_data = [];
    for (let i = 0; i < allKeys.length; i++) {
        updateKeys.push(allKeys[i] + "=?");
        replacementVal.push(arr[allKeys[i]]);
    }

    return return_data[updateKeys, replacementVal];
}

LeadRawLogModel.getSubsourceBysourceId = async (data) => {
    try {
        return new Promise((resolve, reject) => {
            let sql = "SELECT sub_src.id AS sub_source_id, sub_src_lng.description AS sub_source_description FROM " + UBLMS_SUB_SOURCE + " sub_src INNER JOIN " + UBLMS_SUB_SOURCE_LANG + " sub_src_lng ON sub_src.id = sub_src_lng.sub_source_id WHERE sub_src.source_id =? AND sub_src_lng.name =? AND sub_src.status =?";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [data.source_id, data.sub_source_name, data.status] })
                .then(result => {
                    resolve(result[0]);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {

    }
}

LeadRawLogModel.insertData = async (data, tablename) => {
    let arrayD = await insertVariables(data);
    return new Promise((resolve, reject) => {
        let sql = "INSERT INTO " + tablename + " (" + arrayD[0].join() + ") VALUES (" + arrayD[1].join() + ")";
        AppModel.dbObj.sequelize
            .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: arrayD[1] })
            .then((insertLead) => {
                resolve(insertLead[0]);
            }).catch(error => {
                reject(error)
            });
    });
}

const insertVariables = async (arr) => {
    let allKeys = Object.keys(arr);
    let valuesArr = [];
    let replacementVal = [];
    let returnData = [];
    for (let i = 0; i < allKeys.length; i++) {
        valuesArr.push('?');        
        replacementVal.push(additionaldata[allKeys[i]]);
    }
    return returnData[allKeys, valuesArr, replacementVal];
}
LeadRawLogModel.getLeadsCarByLeadId = async (lead_id, car_id) => {
    try {        
        let sql = "SELECT * FROM " + UBLMS_LEADS_CARS + "  WHERE lead_id =? AND car_id=?";
        AppModel.dbObj.sequelize
            .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [lead_id, car_id] })
            .then((result) => {                
                resolve(result[0]);
            }).catch(error => {
                reject(error)
            });
    } catch (error) {

    }
}
LeadRawLogModel.statusUpdate = async (params) =>{
    let sql = `UPDATE ${UBLMS_RAW_LEADS_LOG} SET pushed_status=?, updated_date=?,reason=?,lead_id=? WHERE id=?`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.UPDATE, replacements: [params.pushed_status, params.updated_date, params.reason, params.lead_id, params.id] })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}
LeadRawLogModel.insertRecord = async (data, TABLE) => {
    try {        
        let custObj = data;        
        let allKeys = Object.keys(custObj);        
        let columns = allKeys.join("=?,");
        let allValues = Object.values(custObj);
        return new Promise((resolve, reject) => {
            let sql = `INSERT INTO ${TABLE} SET ${columns}=?`;
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: allValues})
                .then(res => {
                    resolve(res[0]);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {
        // console.log('Error in updateCustomer----', error);
    }
}
module.exports = LeadRawLogModel;