const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
var AppModel = require(COMMON_MODEL + 'appModel');
const crypto = require("../../../lib/crypto");

class FacebookLeadsModel extends Model {
}

FacebookLeadsModel.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    facebook_form_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
    },
    raw_lead_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    lead_token_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    fb_lead_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
    },
    customer_name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    customer_email: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    customer_phone: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    free_form_text: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true,
    },
    car_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    status: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue: '1'
    },
    is_sent_to_raw_leads: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue: '0'
    },
    lead_created_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    }
}, {
    sequelize,
    timestamps: false,
    modelName: UBLMS_FACEBOOK_LEAD,
    freezeTableName: true
});


FacebookLeadsModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await FacebookLeadsModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

FacebookLeadsModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await FacebookLeadsModel.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

FacebookLeadsModel.get = async (_where, pagination) => {

    let isPaginationApply = (typeof _where.isPaginationApply !== 'undefined') ? _where.isPaginationApply : true;
    let whereCond = await FacebookLeadsModel.bindCondition(_where, pagination, isPaginationApply)  //true is used to apply pagination limit

    return new Promise(function (resolve, reject) {
        var sql = "select SQL_CALC_FOUND_ROWS ufl.facebook_form_id,ufl.id,ufl.fb_lead_id,ufl.customer_name,ufl.customer_email, ufl.customer_phone,ufl.free_form_text,ufl.car_id,ufl.status,ufl.lead_created_date,\n" +
            " ufl.created_at,ufl.updated_at from " + UBLMS_FACEBOOK_LEAD + " as ufl " + whereCond;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {

                ////// map with car details for car price ////

                let totalCount = await FacebookLeadsModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}

FacebookLeadsModel.bindCondition = async (params, pagination, isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond = await FacebookLeadsModel.bindPagination(params, pagination, isPaginationApply);
    let GROUPBY = 'GROUP BY ufl.id';

    if(params && typeof params.status != 'undefined'){
        let status = " ufl.status = '" + params.status + "'";
        condition = condition ? condition + " AND "+ status : " where "+status;
    }
    if(params && typeof params.is_sent_to_raw_leads != 'undefined'){
        let rawLeadCheck = " ufl.is_sent_to_raw_leads = '" + params.is_sent_to_raw_leads + "'";
        condition = condition ? condition + " AND "+ rawLeadCheck : " where "+rawLeadCheck;
    }
    if(params && typeof params.fb_lead_id != 'undefined'){
        let fbLeadId = " ufl.fb_lead_id = '" + params.fb_lead_id + "'";
        condition = condition ? condition + " AND "+ fbLeadId : " where "+fbLeadId;
    }
   

    if (params && params.sort && params.sort.length) {
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort, (srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort + ', ' + sortData : sortData
        })
        sortCond = (finalSort != '') ? 'ORDER BY ' + finalSort : '';
    }
    return condition + " " + GROUPBY + " " + sortCond + " " + paginationCond;
}

FacebookLeadsModel.bindPagination = (params, pagination, isPaginationApply) => {
    let condition = '';
    if (isPaginationApply) {
        let page_no = pagination;
        let limit = params.page_limit || PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT " + offset + "," + limit;
    }
    return condition;
}

FacebookLeadsModel.getRowsCount = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result[0].total);
            }).catch(error => {
                reject(error);
            });
    })
}


FacebookLeadsModel.encrypt = (data) => {
    if (Array.isArray(data)) data = data.map(v => {
        v.lead_id_hash = (v && v.leadId != null) ? crypto.encode(v.leadId) : ((v && v.id != null) ? crypto.encode(v.id) : '');
        return v;
    })
    else if (data && data.leadId) data.lead_id_hash = crypto.encode(data.leadId);
    else if (data && data.id) data.lead_id_hash = crypto.encode(data.id);
    return data;
}

FacebookLeadsModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

module.exports = FacebookLeadsModel;
