const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
class LeadLogsModel extends Model{
}

LeadLogsModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    customer_mobile : {
        type : dbConfig.Sequelize.STRING(15)
    },
    source_lead_id : {
        type : dbConfig.Sequelize.BIGINT(20)
    }, 
    temp_contact_id :{
        type : dbConfig.Sequelize.INTEGER(11)
    }, 
    customer_id : {
        type : dbConfig.Sequelize.BIGINT(20)
    },
    customer_name :{
        type : dbConfig.Sequelize.STRING
    },
    customer_email :{
        type : dbConfig.Sequelize.STRING(150)
    },
    customer_city :{
        type : dbConfig.Sequelize.STRING(15)
    },
    source_id :{
        type : dbConfig.Sequelize.INTEGER(11)
    },
    sub_source_id :{
        type : dbConfig.Sequelize.INTEGER(11)
    },
    dealer_id :{
        type : dbConfig.Sequelize.BIGINT(20)
    },
    created_at : {
        type : dbConfig.Sequelize.DATE
    },
    lead_status : {
        type : dbConfig.Sequelize.STRING
    },
    leads_status_reason : {
        type : dbConfig.Sequelize.STRING
    },
    lead_platform : {
        type : dbConfig.Sequelize.STRING
    },
    params_string : {
        type : dbConfig.Sequelize.TEXT
    },
    is_premium : {
        type : dbConfig.Sequelize.TINYINT
    }

},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_LEADS_LOG,
    freezeTableName: true
});

module.exports = LeadLogsModel