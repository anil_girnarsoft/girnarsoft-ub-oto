const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');


const Schema = dbConfig.schema;

const UblmsCarNotExistsModel = new Schema ({
    mobile: { type: String, required: false },
    log_id: { type: Number, required: false },
    added_date: { type: Date, required: false },
});

module.exports = dbConfig.mongoosedb.model('ublms_car_not_exists', UblmsCarNotExistsModel)
