const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');


const Schema = dbConfig.schema;

const UblmsDialerLog = new Schema ({
    curl_data: { type: String, required: false },
    customer_name: { type: String, required: false },
    list_id: { type: Number, required: false },
    dialshree_url: { type: String, required: false },
    lead_id: { type: String, required: false },
    city: { type: String, required: false },
    response: { type: String, required: false },
    created_at: { type: Date, required: false },
    type:{type:String,required:false}
});

module.exports = dbConfig.mongoosedb.model('UblmsDialerLog', UblmsDialerLog)
