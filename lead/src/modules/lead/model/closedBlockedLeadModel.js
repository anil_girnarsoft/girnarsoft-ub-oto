const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const CommonHelper = require('../../../helpers/CommonHelper');
const customerModel = require('../../customer/model/customerModel');

class closedBlockedLeadModel extends Model{
}

closedBlockedLeadModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    customer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    blocked_reason:{
        type: dbConfig.Sequelize.ENUM('0','1','2','3','4'),
        allowNull: false,
        defaultValue:'0'
    },
    lead_comment:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: false,
    },
    user_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
    },
    status:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'1'
    },
    is_sent_gaadi:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    },
    is_sent_cardekho:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    },
    sent_gaadi_time:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    sent_cardekho_time:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    created_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    updated_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CLOSED_BLOCKED_LEADS,
    freezeTableName: true
});


closedBlockedLeadModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await closedBlockedLeadModel.create(data);
            resolve(role_data);
        } catch (error) { 
            reject(error)
        }
    })
}

closedBlockedLeadModel.updateOne = async (id, data) => { 
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await closedBlockedLeadModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

closedBlockedLeadModel.get = async (_where,pagination) => {

    let whereCond = await closedBlockedLeadModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS cbl.*,cus.customer_mobile,cus.name,u.name FROM "+UBLMS_CLOSED_BLOCKED_LEADS+" \n\
        cbl inner join "+UBLMS_CUSTOMER+" cus on cbl.customer_id = cus.id inner join "+UBLMS_USER+" u ON cbl.user_id = u.id "
        +whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await closedBlockedLeadModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

closedBlockedLeadModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if(params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
    if(params.name)
        dataModel.name = params.name;
    if(params.description)
        dataModel.description = params.description;
    if(params.added_by)
        dataModel.added_by = params.added_by
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
        return dataModel;
}

closedBlockedLeadModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond =  await closedBlockedLeadModel.bindPagination(params,pagination,isPaginationApply);
    let GROUPBY = '';
    
    let status  = "cbl.status='1'";
    condition   = "where "+status; 
    
    if(params && params.customerMobile){
        let customerCity = "(cus.customer_mobile = '"+params.customerMobile+"')";
        condition = (condition == '')? condition + "where "+customerCity : condition+" and "+customerCity 
    }

    if(params && params.blockingReason){
        let blockingReason = "(cbl.blocked_reason = '"+params.blockingReason+"')";
        condition = (condition == '')? condition + "where "+blockingReason : condition+" and "+blockingReason 
    }

    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }
    return condition+" "+GROUPBY+" "+sortCond+" "+paginationCond;
}

closedBlockedLeadModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

closedBlockedLeadModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

closedBlockedLeadModel.checkCSVvalidationForUnblock = (customerId,customerName,customerMobile,blockedReason) => {  
    let errorMessage = '';
    let status = true;
    let response = {};
    
    if(customerId<1){
        errorMessage += '*Customer does not exist';
        status = false;
    }

    let blockingReasonsArry = CommonHelper.blockingReasons();
    let blockingReasonId = [];
    blockingReasonsArry.forEach(el => {
        blockingReasonId.push(el['value']);
    })
    
    if(blockedReason=='0' || blockedReason){
        let indexof = blockingReasonId.indexOf(blockedReason);
        if(indexof != -1){
            errorMessage += '*'+blockingReasonsArry[indexof]['label'];
            status = true;
        }else{
            errorMessage += '*Blocking reason code is invalid';
            status = false;
        }
    } else {
        errorMessage += '*Blocking reason code is blank';
        status = false;
    }
            
    response['status'] = status;
    response['errorMessage'] = errorMessage;  
   
    return response;
}

closedBlockedLeadModel.unblockBlockedCustomer = async (updatedBy, leadId, customerId, customerName, customerMobile, blockedReason) => {
            
    if(customerId) {
        
        //update customer table
        customerArray = {'status' : 1, 'updated_on' : dateFormat('yyyy-mm-dd')};
        
        let updateCustVal = await customerModel.updateOne(customerId, customerArray); 
        
        return true;
        
    }
}

closedBlockedLeadModel.encrypt = (data) => {
    if(Array.isArray(data)) data = data.map(v => {
        if(v && v.lead_id != null) v.lead_id_hash = crypto.encode(v.lead_id);
        return v;
    })
    else if (data && data.lead_id) data.lead_id_hash = crypto.encode(data.lead_id);
    return data;
}

closedBlockedLeadModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

module.exports = closedBlockedLeadModel;