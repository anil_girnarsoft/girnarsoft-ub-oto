const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const { param } = require("express-validator");
const cityController = require('../../city/controller/cityController');

class OtherDealersCarOrderModel extends Model{
}

OtherDealersCarOrderModel.init({
    car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_count:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    last_updated_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_OTHER_DEALERS_CAR_ORDER,
    freezeTableName: true
});


OtherDealersCarOrderModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const raw_lwad_data = await OtherDealersCarOrderModel.create(data);
            resolve(raw_lwad_data);
        } catch (error) {
            reject(error)
        }
    })
}

OtherDealersCarOrderModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await OtherDealersCarOrderModel.update(data, { where: { car_id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


OtherDealersCarOrderModel.get = async (_where,pagination) => {

    let whereCond = await OtherDealersCarOrderModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM \n\
        "+UBLMS_OTHER_DEALERS_CAR_ORDER+" "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await OtherDealersCarOrderModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

OtherDealersCarOrderModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond =  await OtherDealersCarOrderModel.bindPagination(params,pagination,isPaginationApply);
    
    if(params && params.carId){
        let id = "car_id='"+params.carId+"'";
        condition = (condition == '') ? condition + "where "+id : condition+" and "+id 
    }

    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

OtherDealersCarOrderModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

OtherDealersCarOrderModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}
module.exports = OtherDealersCarOrderModel;