const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class AsyncLeadModel extends Model{
}

AsyncLeadModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    mobile:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue:'NULL'
    },
    car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    lead_dealer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    api_data:{
        type: dbConfig.Sequelize.TEXT,
        // allowNull: false
    },
    domain_id:{
        type: dbConfig.Sequelize.ENUM('0','1','2','3','4'),
        allowNull: false,
        defaultValue:'0'
    },
    api_type:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    flag:{
        type: dbConfig.Sequelize.ENUM('0','1','2','3','4'),
        allowNull: false,
        defaultValue:'0'
    },
    api_response:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    response_code:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    num_try:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    repush_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    queued_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_ASYNC_LEADS_API,
    freezeTableName: true
});


AsyncLeadModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await AsyncLeadModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

AsyncLeadModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await AsyncLeadModel.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

AsyncLeadModel.getLeadAsyncInfo = (leadId) => {
    return new Promise((resolve, reject)=>{
        let sql = "Select * from " + UBLMS_ASYNC_LEADS_API + " where lead_id = '"+leadId+"' ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}


module.exports = AsyncLeadModel;