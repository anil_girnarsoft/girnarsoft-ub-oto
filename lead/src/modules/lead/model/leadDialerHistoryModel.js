const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const crypto = require("../../../lib/crypto");
var AppModel = require(COMMON_MODEL + 'appModel');



class LeadDialerHistoryModel extends Model {
}

LeadDialerHistoryModel.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    lead_id_hash: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    dialer_status: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    status: {
        type: dbConfig.Sequelize.ENUM('processed', 'failed'),
        allowNull: false,
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    call_start_time: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    call_end_time: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    recording_url: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true,
    },
    list_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    agent_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue: 0
    },
    status_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    sub_status_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    calling_status: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    call_attempt: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    call_back_date_time: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
}, {
    sequelize,
    timestamps: false,
    modelName: UBLMS_LEAD_DIALER_CALL_HISTORY,
    freezeTableName: true
});


LeadDialerHistoryModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadDialerHistoryModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

LeadDialerHistoryModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadDialerHistoryModel.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}
LeadDialerHistoryModel.encrypt = (data) => {
    if (Array.isArray(data)) data = data.map(v => {
        v.lead_id_hash = (v && v.leadId != null) ? crypto.encode(v.leadId) : ((v && v.id != null) ? crypto.encode(v.id) : '');
        return v;
    })
    else if (data && data.leadId) data.lead_id_hash = crypto.encode(data.leadId);
    else if (data && data.id) data.lead_id_hash = crypto.encode(data.id);
    return data;
}

LeadDialerHistoryModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}


LeadDialerHistoryModel.get = async (_where, pagination) => {

    let isPaginationApply = (typeof _where.isPaginationApply !== 'undefined') ? _where.isPaginationApply : true;
    let whereCond = await LeadDialerHistoryModel.bindCondition(_where, pagination, isPaginationApply)  //true is used to apply pagination limit

    return new Promise(function (resolve, reject) {
        var sql = "select SQL_CALC_FOUND_ROWS uldc.lead_id,uldc.id,uldc.lead_id_hash,uldc.dialer_status, uldc.status,uldc.created_date,uldc.call_start_time,\n" +
            "uldc.call_end_time,uldc.recording_url, uldc.list_id,uldc.agent_id,uldc.calling_status,uldc.call_attempt from " + UBLMS_LEAD_DIALER_CALL_HISTORY + " as uldc " + whereCond;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {

                ////// map with car details for car price ////

                let totalCount = await LeadDialerHistoryModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}

LeadDialerHistoryModel.getModel = (params) => {
    let dataModel = {};
    if (!params.id)
        dataModel.created_at = new Date().toISOString();
    if (params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if ((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 'pending';
    if (params.lead_id)
        dataModel.name = params.lead_id;
    if (params.lead_id_hash)
        dataModel.lead_id_hash = params.lead_id_hash;
    if (params.list_id)
        dataModel.list_id = params.list_id
    if (params.dialer_type)
        dataModel.dialer_type = params.dialer_type;
    if (params.dialer_response)
        dataModel.dialer_type = params.dialer_response;
    return dataModel;
}

LeadDialerHistoryModel.bindCondition = async (params, pagination, isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond = await LeadDialerHistoryModel.bindPagination(params, pagination, isPaginationApply);
    let GROUPBY = 'GROUP BY uldc.id';
    
    if (params && params.sort && params.sort.length) {
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort, (srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort + ', ' + sortData : sortData
        })
        sortCond = (finalSort != '') ? 'ORDER BY ' + finalSort : '';
    }
    return condition + " " + GROUPBY + " " + sortCond + " " + paginationCond;
}

LeadDialerHistoryModel.bindPagination = (params, pagination, isPaginationApply) => {
    let condition = '';
    if (isPaginationApply) {
        let page_no = pagination;
        let limit = params.page_limit || PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT " + offset + "," + limit;
    }
    return condition;
}

LeadDialerHistoryModel.getRowsCount = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result[0].total);
            }).catch(error => {
                reject(error);
            });
    })
}

module.exports = LeadDialerHistoryModel;
