const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const { async } = require("q");
class LeadCarModel extends Model{
}

LeadCarModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    dealer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    make_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    make_name:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    model_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    model_name:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    variant_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    variant_name:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    distance:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    status_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    sub_status_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0

    },
    calling_status_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    source_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    sub_source_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    car_availability: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    purchase_time: {
        type: dbConfig.Sequelize.TINYINT,
        allowNull: true,
        defaultValue:0
    },
    body_type: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue:0
    },
    car_city: {
        type:dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue:0
    },
    transmission:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue:0
    },
    color:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    fuel:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    price_from:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    price_to:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    max_year:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    min_year:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true,
        defaultValue:0
    },
    max_km:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull:true,
        defaultValue:0
    },
    min_km:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0
    },
    listing_type:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    cd_car_id:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0
    },
    certification_id:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0
    },
    otp_verified: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    due_date:{
        type:dbConfig.Sequelize.DATE,
        allowNull: true
    },
    call_later_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull:true
    },
    user_type: {
        type:dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue:'0'
    },
    is_email_sms:{
        type: dbConfig.Sequelize.TINYINT,
        allowNull: true,
        defaultValue:0
    },
    is_dealer_sent:{
        type: dbConfig.Sequelize.TINYINT,
        allowNull: true,
        defaultValue:1
    },
    booked_amount:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull:true,
        defaultValue:0
    },
    is_featured:{
        type:dbConfig.Sequelize.TINYINT,
        allowNull: true,
        defaultValue:0
    },
    slot_num:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull:true,
        defaultValue:0
    },
    lead_platform:{
        type: dbConfig.Sequelize.STRING,
        allowNull:true
    },
    is_outstation:{
        type:dbConfig.Sequelize.ENUM('0','1'),
        allowNull: true,
        defaultValue:'0'
    },
    is_new_assigned: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    walkin_reschedule_canceled: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue: '0'
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_LEADS_CARS,
    freezeTableName: true
});


LeadCarModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadCarModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

LeadCarModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadCarModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


LeadCarModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if(params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
    if(params.name)
        dataModel.name = params.name;
    if(params.description)
        dataModel.description = params.description;
    if(params.added_by)
        dataModel.added_by = params.added_by
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
        return dataModel;
}

LeadCarModel.getWalkinPurchaseInfo = async(lead_car_id) =>{ 
    return new Promise((resolve, reject)=>{
        let sql = "SELECT id, data FROM "+UBLMS_WALKIN_PURCHASE_INFO+" where lead_car_id = '"+lead_car_id+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.isPurchased = async(carId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT count(lead_id) as totalRecord FROM "+UBLMS_CONVERSION_INFO+" WHERE status = 1 and car_id = '"+carId+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.getWalkinDealerInfo = async(idsArr) =>{
    let ids = idsArr.toString();
    return new Promise((resolve, reject)=>{
        let sql = "SELECT walkin_feedback_id, walkin_feedback FROM "+UBLMS_WALKIN_FEEDBACK+" where walkin_feedback_id IN ("+ids+")";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.isValidPurchase = async(mainLeadId, reopenFlag, subLeadId) =>{
    let response = 1;
    let subLeadCondition = '';
    if(reopenFlag == '1') {
        subLeadCondition = " and leads_cars_id != '"+subLeadId+"' ";
    }
    return new Promise((resolve, reject)=>{
        let sql = "SELECT count(lead_id) as totalRecord FROM " +UBLMS_CONVERSION_INFO +" WHERE status = 1 and lead_id = " + mainLeadId + subLeadCondition + " ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            if (result && result.length && result[0]['totalRecord']) {
                response = 2;
            } 
            resolve(response);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.updateLeadsCar = async (_where, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadCarModel.update(data, { where: _where});

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

LeadCarModel.getUpdateMainLeadStatus = async(mainLeadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select status_id, sub_status_id, calling_status_id from "+UBLMS_LEADS_CARS +" where lead_id = (" +mainLeadId + ") and status_id != '7' and sub_status_id NOT IN('34','35')  order by status_id desc, sub_status_id desc, updated_at desc LIMIT 0,1 ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.getLeadsCarsData = async(LeadsCarsId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select ulc.car_id, ulc.make_id, ulc.model_id, ulc.variant_id, ulc.make_name as make,\n"+
        "ulc.model_name as model, ulc.variant_name as carversion, ulc.is_new_assigned from "+UBLMS_LEADS_CARS +" as ulc where id IN (" +LeadsCarsId + ") ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

//Function for checking the car is exist or not for same lead

LeadCarModel.isLeadCarExist = async(leadId, carId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select ulc.id from "+UBLMS_LEADS_CARS+" as ulc ";
        sql += " where ulc.lead_id = '"+leadId+"' and ulc.car_id = '"+carId+"' ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.updateSubLeadStatusInfo = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadCarModel.update(data, { where: {lead_id:{[Op.eq]: id},status:{[Op.ne]: 7}}});
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

LeadCarModel.getLeadDetails = async(leadCarIds, is_sent_to_dealer=null) =>{
    let carsCond = '';
    if(leadCarIds){
        carsCond = "ulc.id IN (" +leadCarIds + ") and ";
    }
    if(is_sent_to_dealer == 1){
        carsCond += " ulc.is_dealer_sent =1 and ";
    }
    return new Promise((resolve, reject)=>{
        let field = 'ulc.distance,ulc.lead_platform,ulc.otp_verified,ulc.created_at,ulc.added_by,ulc.status_id,ulc.id as leads_cars_id,ulc.car_id,ulc.make_name as make, ulc.model_name as model, ulc.variant_name as carversion, ulc.price_from, ulc.price_to,ulc.is_outstation,ulc.dealer_id';
        let leadSql = "Select " + field + " from " + UBLMS_LEADS_CARS + " as ulc  where  "+carsCond;
        //leadSql += " ulc.status_id != '7' and ulc.sub_status_id != '12' and ulc.car_id != '0'";  
        leadSql += " ulc.car_id != '0'";      
        
        AppModel.dbObj.sequelize
        .query(leadSql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.getLeadCarInfo = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select *  from " + UBLMS_LEADS_CARS + " where  lead_id = '"+leadId+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.getLeadCarModel = async(obj,lead_id) =>{
    let leadObj = {
        'lead_id'   : lead_id, 
        'dealer_id' : obj['dealer_id'], 
        'car_id'    : obj['car_id'], 
        'make_id'   : obj['make_id'], 
        'make_name' : obj['make_name'], 
        'model_id'  : obj['model_id'], 
        'model_name': obj['model_name'], 
        'variant_id': obj['variant_id'], 
        'variant_name': obj['variant_name'], 
        'distance': obj['distance'], 
        'status_id': obj['status_id'], 
        'sub_status_id': obj['sub_status_id'], 
        'calling_status_id': obj['calling_status_id'], 
        'source_id' : 4, 
        'sub_source_id': 1919, 
        'car_availability': obj['car_availability'], 
        'purchase_time': obj['purchase_time'], 
        'body_type': obj['body_type'], 
        'car_city': obj['car_city'], 
        'transmission': obj['transmission'], 
        'color' : obj['color'], 
        'fuel': obj['fuel'], 
        'price_from' : obj['price_from'], 
        'price_to': obj['price_to'], 
        'max_year': obj['max_year'], 
        'min_year': obj['min_year'], 
        'max_km': obj['max_km'], 
        'min_km'    : obj['min_km'], 
        'listing_type': obj['listing_type'], 
        'cd_car_id': obj['cd_car_id'], 
        'certification_id': obj['certification_id'], 
        'otp_verified': obj['otp_verified'], 
        'due_date': obj['due_date'], 
        'call_later_date': obj['call_later_date'], 
        'created_at' : obj['created_at'], 
        'added_by': obj['added_by'], 
        'updated_by': obj['updated_by'], 
        'is_email_sms': obj['is_email_sms'], 
        'is_dealer_sent': obj['is_dealer_sent'], 
        'booked_amount': obj['booked_amount'], 
        'is_featured': obj['is_featured'], 
        'updated_at':obj['updated_at'], 
        'lead_platform' : obj['lead_platform'], 
        'slot_num' : obj['slot_num'],
        'is_outstation': obj['is_outstation']
    };
    return leadObj;
}

LeadCarModel.getSubLeadHigherStatus = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select status_id, sub_status_id, calling_status_id  from " + UBLMS_LEADS_CARS + " where  lead_id = '"+leadId+"' and lower(user_type) IS NULL and status_id!='7'  order by status_id desc, sub_status_id desc, updated_at desc LIMIT 0,1";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.isDealerCarAssigned = async(dealer_id) =>{
    return new Promise((resolve, reject)=>{
        let sql = "select count(distinct(lc.lead_id)) as total from "+UBLMS_LEADS_CARS+ "lc \n"+
        " where lc.is_dealer_sent != '2' and lc.dealer_id='" +dealer_id + "'  and lc.created_at >='" +todatedatetime + "' and lc.created_at <= '" +currentdate + "'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.getNewCarPrice = async(city,versionId,priceTo) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select opa.ex_showroom, opa.registration, opa.insurance, opa.logistic from "+GAADI_TECHNICAL_SPECIFICATION+" ts INNER JOIN "+GAADI_ORP_PRICE_ACTUAL+" opa ON ts.id = opa.technical_specification_id and opa.city ='"+city+"'  where ts.version_id = '"+versionId+"' ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(carData=>{
            let priceResponse = {
                'newCarPrice':0,
                'newCarDiffPrice':0
            }
            if(carData.length) {
                priceResponse.newCarPrice = carData[0]['ex_showroom'] + carData[0]['registration'] + carData[0]['insurance'] + carData[0]['logistic'];
                //$arr['newCarPrice'] = $this->Customhelper->formatInIndianStyle($newCarPrice);
                priceResponse.newCarDiffPrice = priceResponse.newCarPrice - priceTo;
                //$arr['newCarDiffPrice'] = $this->Customhelper->formatInIndianStyle($newCarDiffPrice);
            }
            resolve(priceResponse);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.getPriceInfoFromLeadsCar = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT MIN( id ) , price_to FROM  " + UBLMS_LEADS_CARS + " WHERE lead_id =  '" + leadId + "' AND  price_to > 0";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.getUniqueLeadsCars = async(params, srhLeadCarConditions, leadsCondition, leadsJoin,usedCarJoin) =>{
    return new Promise((resolve, reject)=>{
        let sql = "select count(distinct lead_id) as sumDistLead from "+ UBLMS_LEADS_CARS + " l "+leadsJoin+" where l.added_by > 0 AND l.is_dealer_sent = 1 "+srhLeadCarConditions+" "+leadsCondition+" "+usedCarJoin+" group by l.dealer_id";        
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}


LeadCarModel.getUniqueTotalLeads = async(params, srhLeadCarConditions, leadsCondition, leadsJoin,usedCarJoin) =>{
    let strPlatform = VALIDPLATFORM.join("','");
    return new Promise((resolve, reject)=>{
        let sql = "select count(distinct l.lead_id) as totalLead from "+ UBLMS_LEADS_CARS + " l "+leadsJoin+" inner join "+UBLMS_DEALER_PRIORITY+" dc on l.car_id = dc.car_id where l.otp_verified = 1 and l.lead_platform NOT IN('"+strPlatform+"') and l.added_by != '-2' and ((l.is_dealer_sent = 1 and l.added_by != 0) OR (l.is_dealer_sent != 2 and l.added_by = 0)) AND dc.dealer_id IN ('"+params.dealerIds.join("','")+"') "+srhLeadCarConditions+" "+leadsCondition+" group by l.dealer_id";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}


LeadCarModel.getSumOrgaicGreater = async(params, srhLeadCarConditions, leadsCondition, leadsJoin,usedCarJoin) =>{
    let strPlatform = VALIDPLATFORM.join("','");
    return new Promise((resolve, reject)=>{
        let sql = "select count(distinct l.lead_id) as sumOrganicGreater100 from "+ UBLMS_LEADS_CARS + " l "+leadsJoin+" where l.is_dealer_sent != 2 and l.added_by = 0 and otp_verified != 1 and l.lead_platform NOT IN('"+strPlatform+"') and l.added_by != '-2' "+srhLeadCarConditions+" "+leadsCondition+" "+usedCarJoin+" group by l.dealer_id";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.getSumOrgaic = async(params, srhLeadCarConditions, leadsCondition, leadsJoin,usedCarJoin) =>{
    let strPlatform = VALIDPLATFORM.join("','");
    return new Promise((resolve, reject)=>{
        let sql = "select count(distinct l.lead_id) as sumOrganic from "+ UBLMS_LEADS_CARS + " l "+leadsJoin+" inner join "+UBLMS_DEALER_PRIORITY+" dc on l.car_id=dc.car_id where l.is_outstation ='0' and l.added_by = 0 and otp_verified = 1 and l.lead_platform NOT IN('"+strPlatform+"') and l.added_by != '-2' and dc.dealer_id IN ('"+params.dealerIds.join("','")+"') "+srhLeadCarConditions+" "+leadsCondition;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.sumBackend = async(params, srhLeadCarConditions, leadsCondition, leadsJoin,usedCarJoin) =>{
    let strPlatform = VALIDPLATFORM.join("','");
    return new Promise((resolve, reject)=>{
        let sql = "select count(distinct l.lead_id) as sumBackend from "+ UBLMS_LEADS_CARS + " l inner join "+UBLMS_DEALER_PRIORITY+" dc on l.car_id=dc.car_id "+leadsJoin+" where l.added_by > 0 and l.lead_platform IS NULL and dc.dealer_id IN ('"+params.dealerIds.join("','")+"') "+srhLeadCarConditions+" "+leadsCondition;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.getSumBooked = async(params, srhLeadCarConditions, leadsCondition, leadsJoin, usedCarIds) =>{
    return new Promise((resolve, reject)=>{
let sql = `SELECT COUNT(DISTINCT l.id) AS sumBooked
FROM
${UBLMS_LEADS_CARS} l
inner join ${UBLMS_DEALER_PRIORITY} dc on l.car_id=dc.car_id ${leadsJoin}
WHERE l.status_id = 6 AND l.sub_status_id = 26 AND dc.dealer_id in (${params.dealerIds.join(',')}) ${srhLeadCarConditions} ${leadsCondition}
GROUP BY l.dealer_id`;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}


LeadCarModel.totalOrganicAndBackendLeads = async(params) =>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT ulc.car_id , Count(DISTINCT CASE WHEN ( ( ulc.added_by != 0 ) ) THEN ulc.car_id ELSE NULL END) as totalBackendLeads, Count(DISTINCT CASE WHEN ( ( ulc.added_by = 0 ) ) THEN ulc.car_id ELSE NULL END) as totalOrganicLeads FROM "+ UBLMS_LEADS_CARS + " AS ulc WHERE ulc.car_id in ('"+params['carsId'].join("','")+"') GROUP BY ulc.car_id";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.totalUniqueLeads = async(params) =>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT ulc.car_id ,count(DISTINCT ulc.lead_id) totalLeads FROM "+ UBLMS_LEADS_CARS + " AS ulc WHERE ulc.car_id in ('"+params['carsId'].join("','")+"') GROUP BY ulc.lead_id";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.totalFTD = async(params) =>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT ulc.car_id ,count(DISTINCT ulc.lead_id) ftd FROM "+ UBLMS_LEADS_CARS + " AS ulc WHERE ulc.car_id in ('"+params['carsId'].join("','")+"') and DATE(`created_at`) = CURDATE() GROUP BY ulc.lead_id";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.organicLeads = async (params) => {
    let replaceValue = [];
    let days = '';
    let dealer_id = params.dealer_id;
    replaceValue.push(dealer_id);
    if (params.days) {
        days = params.days;
        replaceValue.push(days);
    }
    if (params.start_date) replaceValue.push(params.start_date);
    if (params.cur_date) replaceValue.push(params.cur_date);
    return new Promise((resolve, reject) => {
        sql = `SELECT COUNT(DISTINCT l.lead_id) AS organic
            FROM
                ${UBLMS_LEADS_CARS} l
            INNER JOIN
                ${UBLMS_DEALER_PRIORITY} dc ON l.car_id = dc.car_id
            WHERE l.is_dealer_sent != 2
                    AND l.is_outstation = '0'
                    AND l.added_by = 0
                    AND otp_verified = 1
                -- AND l.lead_platform NOT IN ('androidapp' , 'androidsapp', 'android app', 'android', 'iosapp', 'ios app', 'ios', 'windowsapp', 'windows app', 'windows')
                    AND l.added_by != '-2'
                    AND dc.dealer_id IN (?)`;
        if (days) {
            sql += ` AND l.created_at > DATE(NOW()) - INTERVAL ? DAY`;
        }
        if (params.start_date && params.cur_date) {
            sql += ` AND date_format(l.created_at, '%Y-%m-%d') >= ? AND date_format(l.created_at, '%Y-%m-%d')  <= ?`;
        }
        sql += ` GROUP BY l.dealer_id`;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceValue })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadCarModel.getCityWiseTotalLeads = async (from_date, to_date, type = null) => {
    let dealerType = '';
    if (type == 'I') {
        dealerType = 'I';
    }
    return new Promise((resolve, reject) => {
        let sql = `SELECT dcarp.city_id, COUNT(ldcar.car_id) AS total_lds
    FROM ${UBLMS_LEADS}  lds
    INNER JOIN ${UBLMS_LEADS_CARS} ldcar ON ldcar.lead_id = lds.id
    INNER JOIN ${UBLMS_DEALER_PRIORITY} dcarp ON dcarp.car_id = ldcar.car_id
    WHERE DATE_FORMAT(ldcar.created_at, '%Y-%m-%d') >= ? AND DATE_FORMAT(ldcar.created_at, '%Y-%m-%d') <= ?`;
        if (dealerType) sql += ` AND dcarp.type=?`;
        sql += ` AND ldcar.added_by=0`;
        sql += ` GROUP BY dcarp.city_id`;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [from_date, to_date, dealerType] })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}

LeadCarModel.getCityWiseDealerLead = async (from_date, to_date, priority) => {
    let dealer_priority = 1;
    if (priority == 'paid') {
        dealer_priority = 2;
    }
    return new Promise((resolve, reject) => {
        let sql = `SELECT dcarp.city_id, dcarp.dealer_id, COUNT(ldcar.car_id) AS total_lds
    FROM ${UBLMS_LEADS}  lds
    INNER JOIN ${UBLMS_LEADS_CARS} ldcar ON ldcar.lead_id = lds.id
    INNER JOIN ${UBLMS_DEALER_PRIORITY} dcarp ON dcarp.car_id = ldcar.car_id
    WHERE  dcarp.type=? AND dcarp.priority=? AND DATE_FORMAT(ldcar.created_at, '%Y-%m-%d') >= ? AND DATE_FORMAT(ldcar.created_at, '%Y-%m-%d') <= ? AND ldcar.added_by =? GROUP BY dcarp.city_id`;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: ['D', dealer_priority, from_date, to_date, 0] })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}

LeadCarModel.getUniquLeadsOfEachCity = async (from_date, to_date) => {
    let replaceVal = [];
    if (from_date) replaceVal.push(from_date);
    if (to_date) replaceVal.push(to_date);
    replaceVal.push(0);
    return new Promise((resolve, reject) => {
        let sql = `SELECT dc.city_id, COUNT(DISTINCT l.lead_id) AS total_lds
        FROM ${UBLMS_LEADS_CARS} l
        INNER JOIN ${UBLMS_DEALER_PRIORITY} dc ON l.car_id = dc.car_id
        WHERE DATE_FORMAT(l.created_at, '%Y-%m-%d') >= ? AND DATE_FORMAT(l.created_at, '%Y-%m-%d') <= ? AND l.added_by = ?
        GROUP BY dc.city_id`;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceVal })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}

LeadCarModel.getDealerUniqueLeadsOfEachCity = async (from_date, to_date, priority) => {
    let replaceVal = [];
    let dealer_priority = 1;
    
    if (priority == 'paid') {
        dealer_priority = 2;
    }
    replaceVal.push(dealer_priority);
    if (from_date) replaceVal.push(from_date);
    if (to_date) replaceVal.push(to_date);
    replaceVal.push(0);
    return new Promise((resolve, reject) => {
        let sql = `SELECT dcarp.city_id, dcarp.dealer_id,  COUNT(DISTINCT ldcar.lead_id) AS total_lds
    FROM ${UBLMS_LEADS_CARS} ldcar 
    INNER JOIN ${UBLMS_DEALER_PRIORITY} dcarp ON dcarp.car_id = ldcar.car_id
    WHERE dcarp.type='D' AND dcarp.priority=? AND DATE_FORMAT(ldcar.created_at, '%Y-%m-%d') >= ? AND DATE_FORMAT(ldcar.created_at, '%Y-%m-%d') <= ? AND ldcar.added_by=? GROUP BY dcarp.city_id`;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceVal })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}

LeadCarModel.getUniqueIndividualLeadsOfEachCity = async (from_date, to_date, type = null) => {
    let dealerType = '';
    if (type == 'I') {
        dealerType = 'I';
    }
    return new Promise((resolve, reject) => {
        let sql = `SELECT dcarp.city_id,  COUNT(DISTINCT ldcar.lead_id) AS total_lds
    FROM ${UBLMS_LEADS_CARS} ldcar 
    INNER JOIN ${UBLMS_DEALER_PRIORITY} dcarp ON dcarp.car_id = ldcar.car_id
    WHERE DATE_FORMAT(ldcar.created_at, '%Y-%m-%d') >= ? AND DATE_FORMAT(ldcar.created_at, '%Y-%m-%d') <= ?`;
        if (dealerType) sql += ` AND dcarp.type=?`;
        sql += ` GROUP BY dcarp.city_id`;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [from_date, to_date, dealerType] })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}

LeadCarModel.getInventory = async (params) => {
    let priority = '';
    let type = '';
    let replaceVal = [params.car_status, params.is_classified];    
    if (params.priority) {
        priority = params.priority;
        replaceVal.push(priority);
    }
    if (params.type) {
        type = params.type;
        replaceVal.push(type);
    }
    return new Promise((resolve, reject) => {
        let sql = `SELECT COUNT(*) AS total FROM ${UBLMS_DEALER_PRIORITY} WHERE car_status=? AND is_classified=? `;
        if (priority) sql += ` AND priority = ?`;
        if (type) sql += ` AND type = ?`;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceVal })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}

LeadCarModel.getUniqueTotalLeadsReport = async (from_date, to_date, prio=null) => {
    return new Promise((resolve, reject) => {
        let sql = `SELECT COUNT(DISTINCT ldcar.lead_id) AS total
    FROM ${UBLMS_LEADS_CARS} ldcar 
    INNER JOIN ${UBLMS_DEALER_PRIORITY} dcarp ON dcarp.car_id = ldcar.car_id
    WHERE DATE_FORMAT(ldcar.created_at, '%Y-%m-%d') >= ? AND DATE_FORMAT(ldcar.created_at, '%Y-%m-%d') <= ? AND ldcar.added_by = 0`;
    if(prio == 2){
        sql += ` AND dcarp.type='D' AND dcarp.priority=?`;
    }
    if(prio == 1){
        sql += `  AND dcarp.type='D' AND dcarp.priority=?`;
    }
    if(prio == -1){
        prio = 'I';
        sql += ` AND dcarp.type=?`;
    }
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [from_date, to_date, prio] })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });

}

LeadCarModel.getLeadsCarByIds = async(params) => {
    return new Promise((resolve, reject)=>{
        let sql = "SELECT ulc.car_id,ulc.lead_id FROM "+ UBLMS_LEADS_CARS + " AS ulc WHERE ulc.lead_id in ('"+params['leadsIds'].join("','")+"')";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

LeadCarModel.getCityWiseInventory = async (params) => {
    let replaceVal = [];
    replaceVal.push(params.car_status);
    replaceVal.push(params.is_classified);
    if (params.priority) {
        replaceVal.push(params.priority);
    }
    if (params.type) {
        replaceVal.push(params.type);
    }
    let sql = `SELECT city_id, COUNT(*) AS total_lds FROM ${UBLMS_DEALER_PRIORITY} WHERE car_status=? AND is_classified=? `;
    if (params.priority) sql += ` AND priority = ?`;
    if (params.type) sql += ` AND type = ?`;
    sql += ` GROUP BY city_id`;

    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: replaceVal })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    })

}


LeadCarModel.getLeadDetailsData = async(params) => {
    return new Promise((resolve, reject)=>{
        let sql = "SELECT ulc.id,ulc.created_at,ulc.updated_at,ulc.added_by,ulc.updated_by, user.name as added_by_name, userModified.name as modified_by_name, ulc.status_id, us.name as status_name, ulsst.name as sub_status_name, ulcst.name as calling_status_name  FROM "+ UBLMS_LEADS_CARS + " AS ulc LEFT JOIN "+ UBLMS_USER + " as user ON ulc.added_by = user.id LEFT JOIN "+ UBLMS_USER + " as userModified ON ulc.added_by = userModified.id LEFT JOIN " + UBLMS_STATUS + " AS us ON us.id=ulc.status_id and us.status='1' LEFT JOIN " + UBLMS_SUB_STATUS + " as ulsst on ulc.sub_status_id = ulsst.id and ulsst.status='1' LEFT JOIN " + UBLMS_CALL_STATUS + " as ulcst on ulc.calling_status_id = ulcst.id and ulcst.status='1' WHERE ulc.id ='"+params['id']+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

module.exports = LeadCarModel;