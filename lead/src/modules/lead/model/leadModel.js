const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const axios = require('axios');
const siteSetting = require(SITE_SETTINGS);
const BlockedLeadRequest = require('./blockedLeadRequestMogoModel');
const { CARDEKHO_BLOCK_LEAD_API_URL, GAADI_BLOCK_LEAD_API_URL } = require("../../../../config/config");

const CustomerModel = require('../../customer/model/customerModel');
const closedBlockedLeadModel = require('./closedBlockedLeadModel');
const SourceModel = require('../../source/model/sourceModel');
const SubSourceModel = require('../../source/model/subSourceModel');

class LeadModel extends Model {
}

LeadModel.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    source_lead_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    customer_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    source_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
    },
    sub_source_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
    },
    status_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
    },
    sub_status_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
    },
    calling_status_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
    },
    whatsapp_reply: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    ratings: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    is_finance_req: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue:''
    },
    purchase_time: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue:''
    },
    due_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    l2_added_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    platform: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    leads_status: {
        type: dbConfig.Sequelize.ENUM('0', '1'),
        allowNull: false,
        defaultValue: '1'

    },
    is_premium: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 2
    },
    dealer_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    dialer_status: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    l2_dialer_status: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    is_main_lead_save: {
        type: dbConfig.Sequelize.ENUM('0', '1', '2'),
        allowNull: false,
        defaultValue: 0
    },
    lead_sent_2_finance: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    lead_online_score: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    agent_rating: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    budget: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    lead_offline_score: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    num_attempt: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    isExchange: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    isIndividualLeadDialer: {
        type: dbConfig.Sequelize.ENUM('0', '1'),
        allowNull: false,
        defaultValue: '0'
    },
    isSentToDialer: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
    },
    followup_reason: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    blocked_reason: {
        type: dbConfig.Sequelize.ENUM('0', '1', '2', '3', '4'),
        allowNull: false,
        defaultValue: '0'
    },
    isEmail: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
    },
    isSms: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
    },
    connecto_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    car_user_type: {
        type: dbConfig.Sequelize.ENUM('-1', '0', '1'),
        allowNull: false,
        defaultValue: '-1'
    },
    pre_status: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 1
    },
    pre_sub_status: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    close_req_status: {
        type: dbConfig.Sequelize.ENUM('0', '1', '2'),
        allowNull: false,
        defaultValue: '0'
    },
    is_outstation: {
        type: dbConfig.Sequelize.ENUM('0', '1'),
        allowNull: false,
        defaultValue: '0'
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    }
}, {
    sequelize,
    timestamps: false,
    modelName: UBLMS_LEADS,
    freezeTableName: true
});


LeadModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

LeadModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await LeadModel.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

LeadModel.get = async (_where, pagination) => {

    let isPaginationApply = (typeof _where.isPaginationApply !== 'undefined') ? _where.isPaginationApply : true;
    let whereCond = await LeadModel.bindCondition(_where, pagination, isPaginationApply)  //true is used to apply pagination limit

    return new Promise(function (resolve, reject) {
        // var sql = "SELECT SQL_CALC_FOUND_ROWS id,name,description,created_at,updated_at,added_by,updated_by,status FROM "+UBLMS_ROLES+"\n\
        // "+whereCond;

        var sql = "select SQL_CALC_FOUND_ROWS ulc.car_id,ulc.id as lead_cars_id,ubl.id,ubl.is_premium,us.name as status_name, ubl.customer_id,udp.dealer_id,ubl.is_main_lead_save,\n" +
            "cus.customer_city_name,ubl.source_id, ubl.sub_source_id,ubl.purchase_time, ubl.status_id, ubl.due_date, ubl.ratings,\n" +
            "ubl.created_at,ubl.updated_at, cus.customer_email,cus.customer_mobile,cus.alternate_email,cus.alternate_mobile ,cus.name as customer_name,\n" +
            "cus.customer_city_id,cus.custmer_location_name_other,cus.other_location_lat,cus.other_location_lng, cus_req.budget, cus_req.max_km,\n" +
            "cus_req.min_year, cus_req.max_year, cus_req.fuel, cus_req.make_ids,cus_req.model_ids, cus_req.owner, cus_req.color, cus_req.transmission,ulcst.id as call_status_id, \n" +
            "cus_req.req_body_type, cus_req.drive_car, cus_req.min_avg_daily_travel_km, cus_req.max_avg_daily_travel_km, cus_req.seat_num,ubl.blocked_reason,\n" +
            "cus_req.buyer_type, cus_req.purpose, ulsst.id as sub_status_id,ulsst.name as sub_status_name,ulcst.name as call_status_name,ubl.is_finance_req,ubl.followup_reason,ubl.dialer_status,\n" +
            "ubl.lead_offline_score,ubl.lead_online_score,ulc.make_id, ulc.variant_name,ubl.agent_rating,ubl.num_attempt,cus.whatsapp_mobile,ubs.name as source_name,ubss.name as sub_source_name,\n" +
            "ulc.model_name, ulc.make_name,uci.whatsapp_opt_in,ubl.isExchange,ubl.is_outstation, uu.name as modified_by, ( select  calling_status from "+UBLMS_LEAD_DIALER_CALL_HISTORY+" uldch where uldch.lead_id=ubl.id  order by id desc limit 1) as  lead_dialer_history_call_status,uwi.walkin_datetime  from " + UBLMS_LEADS + " as ubl LEFT JOIN " + UBLMS_CUSTOMER + " as cus\n" +
            "on ubl.customer_id = cus.id LEFT JOIN " + UBLMS_CUSTOMER_REQUIREMENT_AGENT + " AS cus_req ON ubl.id = cus_req.lead_id LEFT JOIN " + UBLMS_STATUS + " AS us ON us.id=ubl.status_id \n" +
            "LEFT JOIN " + UBLMS_SOURCE + " AS ubs ON ubl.source_id=ubs.id LEFT JOIN " + UBLMS_SUB_SOURCE + " as ubss on ubl.sub_source_id=ubss.id\n" +
            "LEFT JOIN " + UBLMS_SUB_STATUS + " as ulsst on ubl.sub_status_id = ulsst.id \n" +
            "LEFT JOIN " + UBLMS_CALL_STATUS + " as ulcst on ubl.calling_status_id = ulcst.id \n" +
            "LEFT JOIN " + UBLMS_USER+ " as uu on ubl.updated_by = uu.id \n" +
            "LEFT JOIN " + UBLMS_LEADS_CARS + " as ulc on ubl.id = ulc.lead_id LEFT JOIN " + UBLMS_DEALER_PRIORITY + " as udp on ulc.car_id=udp.car_id LEFT JOIN " + UBLMS_CUSTOMER_INFO +
            " uci on uci.customer_id = cus.id LEFT JOIN " + UBLMS_WALKING_INFO +
            " uwi on uwi.lead_id = ubl.id LEFT JOIN (SELECT uwpi.lead_car_id, uwpi.id, uwpi.data,uwpi.status,lct.lead_id from " + UBLMS_WALKIN_PURCHASE_INFO +
            " uwpi LEFT JOIN " + UBLMS_LEADS_CARS + " lct ON lct.id = uwpi.lead_car_id ) uwpi on uwpi.lead_id = ubl.id " + whereCond;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {

                ////// map with car details for car price ////

                let totalCount = await LeadModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}

LeadModel.getModel = (params) => {
    let dataModel = {};
    if (!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if (params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if ((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
    if (params.name)
        dataModel.name = params.name;
    if (params.description)
        dataModel.description = params.description;
    if (params.added_by)
        dataModel.added_by = params.added_by
    if (params.updated_by)
        dataModel.updated_by = params.updated_by;
    return dataModel;
}

LeadModel.bindCondition = async (params, pagination, isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond = await LeadModel.bindPagination(params, pagination, isPaginationApply);
    let GROUPBY = (params.groupByCarAndleadId) ? 'GROUP BY ubl.id, ulc.car_id' : 'GROUP BY ubl.id';
    if (params && params.name)
        condition = "where cus.name like '%" + params.name + "%'";

    let status = "cus.status='1'";
    condition = (condition == '') ? condition + "where " + status : condition + " and " + status

    if (params && params.customer_city_id && params.customer_city_id.length) {
        let customerCity = "udp.city_id IN (" + (params.customer_city_id).toString() + ")";
        condition = (condition == '') ? condition + "where " + customerCity : condition + " and " + customerCity
    }
     if(params && params.car_make_model && params.car_make_model.length){
         let carModelIds = (params.car_make_model).toString();
         let makeModel = "(ulc.model_id IN ("+carModelIds+"))";
         condition = (condition == '')? condition + "where "+makeModel : condition+" and "+makeModel 
     }
    if (params && params.id) {
        let id = "ubl.id='" + LeadModel.decrypt(params.id) + "'";
        condition = (condition == '') ? condition + "where " + id : condition + " and " + id
    }
    if (params && params.lead_id) {
        let leadId = "ubl.id='" + params.lead_id + "'";
        condition = (condition == '') ? condition + "where " + leadId : condition + " and " + leadId
    }
    if (params && params.email) {
        let email = "cus.customer_email like '%" + params.email + "%'";
        condition = (condition == '') ? condition + "where " + email : condition + " and " + email
    }
    if (params && params.mobile_number) {
        let mobile = "(cus.customer_mobile='" + params.mobile_number + "' OR cus.alternate_mobile='" + params.mobile_number + "')";
        condition = (condition == '') ? condition + "where " + mobile : condition + " and " + mobile
    }
    if (params && params.num_attempt) {
        let num_attempt = "ubl.num_attempt='" + params.num_attempt + "'";
        condition = (condition == '') ? condition + "where " + num_attempt : condition + " and " + num_attempt
    }
    if (params && params.source_id) {
        let source_id = "ubl.source_id='" + params.source_id + "'";
        condition = (condition == '') ? condition + "where " + source_id : condition + " and " + source_id
    }

    if (params && params.sub_source_id) {
        let sub_source_id = "ubl.sub_source_id='" + params.sub_source_id + "'";
        condition = (condition == '') ? condition + "where " + sub_source_id : condition + " and " + sub_source_id
    }
    if (params && params.status_id && !Array.isArray(params.status_id)) {
        let status_id = "ubl.status_id='" + params.status_id + "'";
        condition = (condition == '') ? condition + "where " + status_id : condition + " and " + status_id
    } else if (params && params.status_id && Array.isArray(params.status_id)) {
        let status_id = "ubl.status_id IN ('" + (params.status_id).join("','") + "')";
        condition = (condition == '') ? condition + "where " + status_id : condition + " and " + status_id
    }
    if (params && params.sub_status_id && !Array.isArray(params.sub_status_id)) {
        let sub_status_id = "ubl.sub_status_id='" + params.sub_status_id + "'";
        condition = (condition == '') ? condition + "where " + sub_status_id : condition + " and " + sub_status_id
    } else if (params && params.sub_status_id && Array.isArray(params.sub_status_id)) {
        let sub_status_id = "ubl.sub_status_id IN ('" + (params.sub_status_id).join("','") + "')";
        condition = (condition == '') ? condition + "where " + sub_status_id : condition + " and " + sub_status_id
    }
    if (params && params.call_status_id) {
        let call_status_id = "ubl.calling_status_id='" + params.call_status_id + "'";
        condition = (condition == '') ? condition + "where " + call_status_id : condition + " and " + call_status_id
    }

    if (params && params.excludedLeads) {
        let excludedLeadIds = "ubl.id NOT IN(" + params.excludedLeads.join(',') + ")";
        condition = (condition == '') ? condition + "where " + excludedLeadIds : condition + " and " + excludedLeadIds
    }

    if (params && params.budget_range_from && params.budget_range_to) { 
        let frmPrice = params.budget_range_from;
        let toPrice = params.budget_range_to;
        // let priceCond = "ulc.price_to >= '" + frmPrice + "' and ulc.price_to <= '" + toPrice + "'"
        let priceCond = "cus_req.budget >= '" + frmPrice + "' and cus_req.budget <= '" + toPrice + "'"
        condition = (condition == '') ? condition + "where " + priceCond : condition + " and " + priceCond
    }else if (params && params.budget_range_from && !params.budget_range_to) { 
        let frmPrice = params.budget_range_from;
        // let priceCond = "ulc.price_to >= '" + frmPrice +  "'"
        let priceCond = "cus_req.budget >= '" + frmPrice +  "'"
        condition = (condition == '') ? condition + "where " + priceCond : condition + " and " + priceCond
    }else if (params && !params.budget_range_from && params.budget_range_to) {
        let toPrice = params.budget_range_to;
        // let priceCond = "ulc.price_to <= '" + toPrice + "'"
        let priceCond = "cus_req.budget <= '" + toPrice + "'"
        condition = (condition == '') ? condition + "where " + priceCond : condition + " and " + priceCond
    }
    
    let followUpCheckFound = false;
    if (params && params.follow_up_from_date && params.follow_up_to_date) {
        followUpCheckFound = true;
        let frmDate = params.follow_up_from_date//dateFormat(params.follow_up_from_date, "yyyy-mm-dd HH:MM:ss");
        let toDate = params.follow_up_to_date//dateFormat(params.follow_up_to_date, "yyyy-mm-dd HH:MM:ss");
        // let dueDatetCond = "date_format(ubl.due_date,'%Y-%m-%d %H:%i:%s') >= '" + frmDate + "' and date_format(ubl.due_date,'%Y-%m-%d %H:%i:%s') <= '" + toDate + "'"
        let dueDatetCond = "ubl.due_date >= '" + frmDate + "' and ubl.due_date <= '" + toDate + "'"
        condition = (condition == '') ? condition + "where " + dueDatetCond : condition + " and " + dueDatetCond
    } else if (params && params.follow_up_from_date && !params.follow_up_to_date) {
        followUpCheckFound = true;
        let frmDate = params.follow_up_from_date//dateFormat(params.follow_up_from_date, "yyyy-mm-dd");
        let dueDatetCond = "ubl.due_date >= '" + frmDate + "'"
        condition = (condition == '') ? condition + "where " + dueDatetCond : condition + " and " + dueDatetCond
    } else if (params && !params.follow_up_from_date && params.follow_up_to_date) {
        followUpCheckFound = true;
        let toDate = params.follow_up_to_date//dateFormat(params.follow_up_to_date, "yyyy-mm-dd");
        let dueDatetCond = "ubl.due_date <= '" + toDate + "'"
        condition = (condition == '') ? condition + "where " + dueDatetCond : condition + " and " + dueDatetCond
    }
    //IF Followup dates found then remove leads having status id 7 i.e closed 
    if(followUpCheckFound){
        let statusIdCond = "ubl.status_id != '7'";
        condition = (condition == '') ? condition + "where " + statusIdCond : condition + " and " + statusIdCond
    }

    
    if (params && params.created_from_date && params.created_to_date) {
        let frmDate = params.created_from_date//dateFormat(params.created_from_date, "yyyy-mm-dd");
        let toDate = params.created_to_date//dateFormat(params.created_to_date, "yyyy-mm-dd");
        let createdDatetCond = "ubl.created_at >= '" + frmDate + "' and ubl.created_at <= '" + toDate + "'"
        condition = (condition == '') ? condition + "where " + createdDatetCond : condition + " and " + createdDatetCond
    }else if (params && params.created_from_date && !params.created_to_date) {
        let frmDate = params.created_from_date//dateFormat(params.created_from_date, "yyyy-mm-dd");
        let createdDatetCond = "ubl.created_at >= '" + frmDate + "'"
        condition = (condition == '') ? condition + "where " + createdDatetCond : condition + " and " + createdDatetCond
    }else if (params && !params.created_from_date && params.created_to_date) {
        let toDate = params.created_to_date//dateFormat(params.created_to_date, "yyyy-mm-dd");
        let createdDatetCond = "ubl.created_at <= '" + toDate + "'";
        condition = (condition == '') ? condition + "where " + createdDatetCond : condition + " and " + createdDatetCond
    }

    if (params && params.updated_from_date && params.updated_to_date) {
        let frmDate = params.updated_from_date//dateFormat(params.updated_from_date, "yyyy-mm-dd");
        let toDate = params.updated_to_date//dateFormat(params.updated_to_date, "yyyy-mm-dd");
        let updatedDateCond = "ubl.updated_at >= '" + frmDate + "' and ubl.updated_at <= '" + toDate + "'"
        condition = (condition == '') ? condition + "where " + updatedDateCond : condition + " and " + updatedDateCond
    }else if (params && params.updated_from_date && !params.updated_to_date) {
        let frmDate = params.updated_from_date//dateFormat(params.updated_from_date, "yyyy-mm-dd");
        let updatedDateCond = "ubl.updated_at >= '" + frmDate + "'";
        condition = (condition == '') ? condition + "where " + updatedDateCond : condition + " and " + updatedDateCond
    }else if (params && !params.updated_from_date && params.updated_to_date) {
        let toDate = params.updated_to_date//dateFormat(params.updated_to_date, "yyyy-mm-dd");
        let updatedDateCond = "and ubl.updated_at <= '" + toDate + "'"
        condition = (condition == '') ? condition + "where " + updatedDateCond : condition + " and " + updatedDateCond
    }
    if(params && params.modified_by){
        let modified_by = "ubl.updated_by='" + params.modified_by + "'";
        condition = (condition == '') ? condition + "where " + modified_by : condition + " and " + modified_by;
    }
    if(params && params.priority){
        let priority = "udp.priority IN(" + params.priority.join(',') + ")";
        condition = (condition == '') ? condition + "where " + priority : condition + " and " + priority;
    }
    if (params && params.walk_in_schedule_date_from && params.walk_in_schedule_date_to) {
        let walkinFrmDate = params.walk_in_schedule_date_from//dateFormat(params.walk_in_schedule_date_from, "yyyy-mm-dd");
        let walkinToDate = params.walk_in_schedule_date_to//dateFormat(params.walk_in_schedule_date_to, "yyyy-mm-dd");
        let walkinUpdatedDateCond = "uwi.walkin_datetime >= '" + walkinFrmDate + "' and uwi.walkin_datetime <= '" + walkinToDate + "'"
        condition = (condition == '') ? condition + "where " + walkinUpdatedDateCond : condition + " and " + walkinUpdatedDateCond
    }else if (params && params.walk_in_schedule_date_from && !params.walk_in_schedule_date_to) {
        let walkinFrmDate = params.walk_in_schedule_date_from//dateFormat(params.walk_in_schedule_date_from, "yyyy-mm-dd");
        let walkinUpdatedDateCond = "uwi.walkin_datetime >= '" + walkinFrmDate + "'";
        condition = (condition == '') ? condition + "where " + walkinUpdatedDateCond : condition + " and " + walkinUpdatedDateCond
    }else if (params && !params.walk_in_schedule_date_from && params.walk_in_schedule_date_to) {
        let walkinToDate = params.walk_in_schedule_date_to//dateFormat(params.walk_in_schedule_date_to, "yyyy-mm-dd");
        let walkinUpdatedDateCond = "and uwi.walkin_datetime <= '" + walkinToDate + "'"
        condition = (condition == '') ? condition + "where " + walkinUpdatedDateCond : condition + " and " + walkinUpdatedDateCond
    }

    if (params && params.walk_in_completed_date_from && params.walk_in_completed_date_to) {
        let walkinCompletedFrmDate = params.walk_in_completed_date_from//dateFormat(params.walk_in_completed_date_from, "yyyy-mm-dd");
        let walkinCompletedToDate = params.walk_in_completed_date_to//dateFormat(params.walk_in_completed_date_to, "yyyy-mm-dd");
        let walkinCompleteUpdatedDateCond = "JSON_EXTRACT_FUN(uwpi.data , '$.walkInDate') >= '" + walkinCompletedFrmDate + "'  and JSON_EXTRACT_FUN(uwpi.data , '$.walkInDate') <= '" + walkinCompletedToDate + "'"
        condition = (condition == '') ? condition + "where " + walkinCompleteUpdatedDateCond : condition + " and " + walkinCompleteUpdatedDateCond
    }else if (params && params.walk_in_completed_date_from && !params.walk_in_completed_date_to) {
        let walkinCompletedFrmDate = params.walk_in_completed_date_from//dateFormat(params.walk_in_completed_date_from, "yyyy-mm-dd");
        let walkinCompleteUpdatedDateCond = "JSON_EXTRACT_FUN(uwpi.data , '$.walkInDate') >= '" + walkinCompletedFrmDate + "'";
        condition = (condition == '') ? condition + "where " + walkinCompleteUpdatedDateCond : condition + " and " + walkinCompleteUpdatedDateCond
    }else if (params && !params.walk_in_completed_date_from && params.walk_in_completed_date_to) {
        let walkinCompletedToDate = params.walk_in_completed_date_to//dateFormat(params.walk_in_completed_date_to, "yyyy-mm-dd");
        let walkinCompleteUpdatedDateCond = "and JSON_EXTRACT_FUN(uwpi.data , '$.walkInDate') <= '" + walkinCompletedToDate + "'"
        condition = (condition == '') ? condition + "where " + walkinCompleteUpdatedDateCond : condition + " and " + walkinCompleteUpdatedDateCond
    }

    let quickFilter = '';
    if (params && params.is_premium) {
        let is_premium = "ubl.is_premium='" + params.is_premium + "'";
        quickFilter = (quickFilter == '') ? quickFilter + is_premium : quickFilter + " OR " + is_premium
    }
    if (params && params.is_finance_req) {
        let is_finance = "ubl.is_finance_req='" + params.is_finance_req + "'";
        quickFilter = (quickFilter == '') ? quickFilter + is_finance : quickFilter + " OR " + is_finance
    }
    if (params && params.is_individual_lead) {
        let is_individual_lead = "ubl.isindividualleaddialer ='" + params.is_individual_lead + "'";
        quickFilter = (quickFilter == '') ? quickFilter + is_individual_lead : quickFilter + " OR " + is_individual_lead
    }
    if (params && params.is_whatsapp_optin) {
        let is_whatsapp_optin = "uci.whatsapp_opt_in='" + params.is_whatsapp_optin + "'";
        quickFilter = (quickFilter == '') ? quickFilter + is_whatsapp_optin : quickFilter + " OR " + is_whatsapp_optin
    }
    if (quickFilter !== '') {
        condition = (condition == '') ? condition + "where " + quickFilter : condition + " and " + quickFilter
    }
    if (params && typeof params.dialer_status !== 'undefined') {
        let dialer_status = "ubl.dialer_status='" + params.dialer_status + "'";
        condition = (condition == '') ? condition + "where " + dialer_status : condition + " and " + dialer_status
    }
    if(params && params.created_date_within_days) {
        let createdDateDayCond = " ubl.created_at BETWEEN CURDATE() - INTERVAL "+params.created_date_within_days+" DAY AND NOW() ";
        condition = (condition == '') ? condition + " where " + createdDateDayCond : condition + " and " + createdDateDayCond
    }
    if(params && params.dealer_id) {
        condition += " AND udp.dealer_id = "+params.dealer_id+"  AND ((ulc.is_outstation ='0'   and ulc.otp_verified = 1 and ulc.lead_platform NOT IN('"+VALIDPLATFORM.join("','")+"') AND ulc.added_by = 0 AND ulc.added_by != '2') OR (ulc.lead_platform IS NULL  AND ulc.added_by > 0)) " 
    }
    if(params && params.rating) {
        condition += " AND ubl.agent_rating IN ('"+params.rating.join("','")+"')";
    }
    if (params && params.sort && params.sort.length) {
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort, (srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort + ', ' + sortData : sortData
        })
        sortCond = (finalSort != '') ? 'ORDER BY ' + finalSort : '';
    }
    return condition + " " + GROUPBY + " " + sortCond + " " + paginationCond;
}

LeadModel.bindPagination = (params, pagination, isPaginationApply) => {
    let condition = '';
    if (isPaginationApply) {
        let page_no = pagination;
        let limit = params.page_limit || PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT " + offset + "," + limit;
    }
    return condition;
}

LeadModel.getRowsCount = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result[0].total);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadModel.encrypt = (data) => {
    if (Array.isArray(data)) data = data.map(v => {
        v.lead_id_hash = (v && v.leadId != null) ? crypto.encode(v.leadId) : ((v && v.id != null) ? crypto.encode(v.id) : '');
        return v;
    })
    else if (data && data.leadId) data.lead_id_hash = crypto.encode(data.leadId);
    else if (data && data.id) data.lead_id_hash = crypto.encode(data.id);
    return data;
}

LeadModel.decrypt = (hashId) => { 
    let id = '';
    id = crypto.decode(hashId);
    return id;
}

LeadModel.getTimeLine = async (_where, pagination) => {

    let leadId = _where.lead_id;
    leadId = (typeof leadId === 'string') ? LeadModel.decrypt(leadId) : leadId
    return new Promise(function (resolve, reject) {
        var sql = "SELECT ubco.lead_id, ubco.status_id,ubco.dealer_id, ubco.leads_cars_id, ubco.sub_status_id,\n" +
            "ubco.calling_status_id, ubco.call_later_date, ubco.rating_value,ubco.updated_on,\n" +
            "ubco.purchase_intention, ubco.comment, ubco.added_by, ubco.added_on,ubs.name as status_name ,\n" +
            "ubss.name as sub_status_name, ubcs.name as calling_status_name, ubco.followup_reason, ubco.due_date,\n" +
            "ubco.dc_user_id, ubco.dc_user_type, u.name  FROM " + UBLMS_CONVERSION + " AS ubco \n" +
            "LEFT JOIN " + UBLMS_STATUS + " AS ubs on ubco.status_id = ubs.id LEFT JOIN " + UBLMS_SUB_STATUS + " AS ubss \n" +
            "on ubco.sub_status_id = ubss.id LEFT JOIN " + UBLMS_CALL_STATUS + " AS ubcs on ubco.calling_status_id = ubcs.id LEFT JOIN \n" +
            UBLMS_USER + " AS u on ubco.added_by = u.id WHERE lead_id=" + leadId + " ORDER BY ubco.id DESC";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {
                let totalCount = await LeadModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}

LeadModel.getAssignedCarDetails = async (_where, pagination) => {
    
    return new Promise(function (resolve, reject) {
        let whereCond = '';

        let leadId
        if(_where.lead_id){
            leadId = (typeof _where.lead_id === 'string') ? LeadModel.decrypt(_where.lead_id) : _where.lead_id
            whereCond = " ulc.lead_id = " + leadId +" ";
        }
        if(typeof _where.lead_ids_array !== 'undefined'){
            whereCond = " ulc.lead_id IN ('" + _where.lead_ids_array.join("','") +"') ";
        }
        if(_where.car_ids){
            let carIdCond = " ulc.car_id IN ('" + _where.car_ids.join("','") + "') ";
            whereCond = whereCond ? whereCond + " and "+carIdCond : carIdCond;
        }

        let extraFields = '';
        if(leadId){
            extraFields = ' uw.walkin_datetime,uw.walkin_feedback,uw.walkin_status,uw.created_at as walkin_created_at, '
        }
        
        var sql = "SELECT "+extraFields+" case when (ulc.car_id = 0 and ulc.added_by = 0 and ulc.source_id = 90)\n" +
            "THEN 1 ELSE 0 END as carDekhoIndividual, ulc.id ,ulc.lead_id,ulc.make_name,ulc.model_name,\n" +
            "ulc.car_id, ulc.make_id, ulc.model_id, ulc.variant_id, ulc.status_id, ulc.sub_status_id,\n" +
            "ulc.variant_name,ulc.calling_status_id, ulc.updated_by, ulc.car_availability, ulc.due_date,ulc.is_new_assigned,\n" +
            "ulc.added_by, ulc.user_type as leadcarUserType, user.name, ulc.is_email_sms, \n" +
            "ulc.is_dealer_sent, ulc.created_at, ulc.distance, ulc.is_featured, ubss.name \n" +
            "as sub_status_name, ubcs.name as call_status_name,ulc.dealer_id, \n" +
            "numIn.active_lead_count as num_interest,ulc.is_outstation,ulc.car_city,ul.customer_id, customer.name as customer_name,customer.customer_mobile, dlr_boost.priority as dealer_type \n" +
            "FROM " + UBLMS_LEADS_CARS + " as ulc LEFT JOIN " + UBLMS_DEALER_BOOST + " as dlr_boost on dlr_boost.dealer_id = ulc.dealer_id LEFT JOIN " + UBLMS_STATUS + " as ubs on \n" +
            "ulc.status_id = ubs.id LEFT JOIN " + UBLMS_SUB_STATUS + " as ubss on \n" +
            "ulc.sub_status_id = ubss.id LEFT JOIN " + UBLMS_CALL_STATUS + " as ubcs on \n" +
            "ulc.calling_status_id = ubcs.id LEFT JOIN " + UBLMS_USER + " as user on \n" +
            "ulc.added_by = user.id LEFT JOIN " + UBLMS_CAR_ORDER + " as numIn on numIn.car_id = ulc.car_id \n" +
            "LEFT JOIN " + UBLMS_LEADS + " as ul on ul.id = ulc.lead_id \n"+
            "LEFT JOIN " + UBLMS_CUSTOMER + " as customer on customer.id = ul.customer_id ";
            if(leadId){
                sql += " left join ublms_walking_info   as uw on uw.id =(select max(id) from "+ UBLMS_WALKING_INFO +   
                " as uu where uu.status='1' and uu.lead_id=  " + leadId + "  and uu.leads_cars_id= ulc.id) "; 
            } 
            
            sql +=" WHERE "+whereCond+" ORDER BY created_at desc";

            AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {
                let totalCount = await LeadModel.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}

LeadModel.getSubLeadDealer = (param) => {

    return new Promise((resolve, reject) => {
        // let sql = "SELECT ulc.`car_id` FROM `" + UBLMS_LEADS_CARS + "` AS ulc INNER JOIN `" + USED_CAR + "` AS uuc ON uuc.`id`=ulc.`car_id` WHERE ulc.`car_id`!='0' AND ulc.`lead_id`='" + param['lead_id'] + "' AND uuc.`dealer_id`='" + param['dealer_id'] + "' ORDER BY ulc.`car_id` DESC";
        let sql = "SELECT ulc.`car_id` FROM `" + UBLMS_LEADS_CARS + "` AS ulc  WHERE ulc.`car_id`!='0' AND ulc.`lead_id`='" + param['lead_id'] + "' AND ulc.`dealer_id`='" + param['dealer_id'] + "' ORDER BY ulc.`car_id` DESC";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result.length && result || []);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadModel.sentCardekhoBlockUnblock = async (req, params, mode, mobileNumber) => {
    //const responseCardekho     =  await axios.get(CARDEKHO_BLOCK_LEAD_API_URL+params)
    //    let responseCardekho = {};
    //let arrResCardekho = (responseCardekho.status == 200) ? (responseCardekho.data) : {}; 

    //FAKE RESPONSE
    let arrResCardekho = { data: { customerMobile : params['customer_mobile'], msg: 'Unsubscribed' }, status:true };
    let logData = {};
    logData['request'] = mobileNumber;
    logData['type'] = 'Cardekho';
    logData['active'] = mode;
    logData['response'] = JSON.stringify(arrResCardekho);
    logData['log_date'] = dateFormat("yyyy-mm-dd");
    BlockedLeadRequest.create(logData, function (err, res) {
        if (err) {
            //   throw err
        }

    });

    return (arrResCardekho);
}

LeadModel.sentGaadiBlockUnblock = async (params, mode) => {
    // const responseGaadi     =  await axios.post(GAADI_BLOCK_LEAD_API_URL, params)
    // let arrResGaadi = (responseGaadi.status == 200) ? (responseGaadi.data) : {};
    let arrResGaadi = {success: true, status: true};
    let logData = {};
    logData['request'] = params;
    logData['type'] = 'Gaadi';
    logData['active'] = mode;
    logData['response'] = JSON.stringify(arrResGaadi);
    logData['log_date'] = dateFormat("yyyy-mm-dd");
    BlockedLeadRequest.create(logData, function (err, res) {
        if (err) {
            //   throw err
        }
    });
    return (arrResGaadi);
}

LeadModel.getLeadIdForBulkBlock = (customerId) => {
    // $leadId = 0;
    // $query['sql'] = "SELECT lead_id FROM ".UBLMS_LEADS." WHERE customer_id = '".$customerId."' and status_id != '7' and sub_status_id != '12' ";
    // $query['connectionKey'] = $this->slaveRemoteKey;
    // $data = $this->db->query($query);
    // if(isset($data[0]['lead_id']) && !empty($data[0]['lead_id'])) {
    //     $leadId = $data[0]['lead_id'];
    // }
    // return $leadId;

    return new Promise((resolve, reject) => {
        let sql = "SELECT id FROM " + UBLMS_LEADS + " WHERE customer_id = '" + customerId + "' and status_id != '7' and sub_status_id != '12' ";

        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result || []);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadModel.getWalkinDetails = (params) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT walkin_datetime,walkin_feedback,walkin_status,created_at FROM " + UBLMS_WALKING_INFO + " where status = 1 and leads_cars_id = " + params.leads_car_id + " and lead_id = " + params.lead_id + " order by created_at desc limit 0,1";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result.length && result || []);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadModel.getLeadDetailsInfo = async (mainLeadId) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT ubl.id, ubl.customer_id, ubl.source_id, ubl.sub_source_id, ubl.purchase_time,\n" +
            "ubl.due_date,ubl.call_later_date, ubl.status_id, ubl.sub_status_id,ucs.name as call_status_name,\n" +
            "ubl.is_finance_req,ubl.calling_status_id,ubl.ratings,ubl.created_at,ubst.name as status_name,\n" +
            "ubsst.name as sub_status_name FROM " + UBLMS_LEADS + " AS ubl LEFT JOIN " + UBLMS_STATUS + "\n" +
            "AS ubst ON ubl.status_id=ubst.id LEFT JOIN " + UBLMS_SUB_STATUS + " AS ubsst \n" +
            "ON ubl.sub_status_id=ubsst.id LEFT JOIN " + UBLMS_CALL_STATUS + "\n" +
            "AS ucs ON ucs.id=ubl.calling_status_id WHERE ubl.id = '" + mainLeadId + "'";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result.length && result || []);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadModel.getMakeModelVersionById = (list, id, key) => {
    let returnData = {}
    if (list.length && key == 'make') {
        returnData = _.find(list, { 'id': id });
    }
    if (list.length && key == 'model') {
        returnData = _.find(list, { 'id': id });
    }
    if (list.length && key == 'version') {
        returnData = _.find(list, { 'id': id });
    }
    return returnData;
}


LeadModel.getLeadInfo = async (leadId) => {

    return new Promise((resolve, reject) => {
        let sql = "Select status_id, sub_status_id, calling_status_id, ratings, is_finance_req,\n " +
            "purchase_time,due_date, call_later_date,created_at,l2_added_date,updated_at,updated_by, \n" +
            "platform, leads_status,is_premium, dealer_id,dialer_status,l2_dialer_status,is_main_lead_save,\n" +
            "lead_sent_2_finance,agent_rating, budget,num_attempt, isEmail,isSms, isExchange, isSentToDialer from \n" +
            UBLMS_LEADS + " where id = '" + leadId + "' ";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadModel.getLeadInfoToInsert = async (leadId) => {

    return new Promise((resolve, reject) => {
        let sql = "Select  customer_id, source_lead_id, source_id, sub_source_id, status_id,\n" +
            "sub_status_id, calling_status_id, ratings, is_finance_req, purchase_time, due_date, call_later_date,\n" +
            "created_at,l2_added_date, updated_at, updated_by, platform, leads_status, is_premium, \n" +
            "dealer_id, dialer_status, l2_dialer_status, is_main_lead_save, lead_sent_2_finance, \n" +
            "lead_online_score, agent_rating, budget, lead_offline_score, num_attempt, isEmail, isSms,\n" +
            "isExchange, isSentToDialer,is_outstation from " + UBLMS_LEADS + " where id = '" + leadId + "' ";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadModel.getNYFCallStatus = async (leadId) => {

    return new Promise((resolve, reject) => {
        let sql = "Select id from " + UBLMS_CALL_STATUS + " where name LIKE 'NYF' ";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                let call_status_id = 0;
                if ((result && result.length && result[0]['id'])) {
                    call_status_id = result[0]['call_status_id'];
                }
                resolve(call_status_id);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadModel.getModelsByMakeID = (list, id) => {
    let returnData = {}
    let modelList = list.model;
    returnData = (modelList && modelList.length) ? modelList.filter((e) => e.mk_id === id) : []
    return returnData;
}

LeadModel.getVariantByModelID = (list, id) => {
    let returnData = {}
    let versionList = list.version
    versionList = (versionList && versionList.length) ? list.version : {};
    returnData = (versionList && versionList.length) ? versionList.filter(e => e.md_id === id) : []
    return returnData;
}

LeadModel.getDCUserName = async (userid) => {

    return new Promise((resolve, reject) => {
        let sql = "select name from " + SFA_DC_USERS + " where id='" + +userid + "' ";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadModel.getBMUserName = async (userid) => {

    return new Promise((resolve, reject) => {
        let sql = "select name from " + SFA_BM_USERS + " where id='" + userid + "'";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    })
}

/**
 * 
 */

LeadModel.checkLeadValidation = async (req, res, data, helperData) => {

    return new Promise(async (resolve, reject) => {
        try {
            let errorMessage = {};
            let status = 0;
            let response = {};

            if (!data['customer_mobile'].startsWith(siteSetting.mobileFormat['startsFrom']) || data['customer_mobile'].length < siteSetting.mobileFormat['minLength'] || data['customer_mobile'].length > siteSetting.mobileFormat['maxLength']) {
                errorMessage['customer_mobile'] = "Mobile number is invalid";
            }

            if (!errorMessage['customer_mobile']) {
                let checkCustomerMobile = await CustomerModel.getOne({ customer_mobile: data['customer_mobile'] });
                if (checkCustomerMobile && checkCustomerMobile['id'] && checkCustomerMobile['status'] != '1') {

                    let checkBlockingReason = await closedBlockedLeadModel.get(checkCustomerMobile['id']);

                    let searchReason = _.find(helperData.blockingReasonsArry, { value: checkBlockingReason['blocked_reason'] });
                    if (searchReason) {
                        errorMessage['customer_mobile'] = 'This mobile number is blocked because it belongs to a ' + searchReason['label'] + '.';
                    }
                }
            }
            if (data['customer_city']) {
                let checkCity = false;
                let getCities = helperData.getCities;
                if (getCities && getCities['data'] && getCities['data']['city']) {
                    checkCity = _.find(getCities['data']['city'], { name: data['customer_city'] });
                    response['customer_city'] = checkCity;
                }

                if (!checkCity) {
                    errorMessage['customer_city'] = 'City is invalid';
                }
            }

            if (data['source_name']) {
                let validSource = await SourceModel.get({ name: data['source_name'] }) //$this->checksourcesubsource($source);
                if (!validSource[0]) {
                    errorMessage['source_name'] = 'Source is Invalid';
                } else {
                    response['source_id'] = (validSource[0] && validSource[0][0]) ? validSource[0][0]['id'] : '';
                }
            } else {
                errorMessage['source_name'] = 'Source is blank';
            }
            if (data['sub_source_name']) {
                let validSubsource = await SubSourceModel.get({ source_id: (response['source_id'] || 0), name: data['sub_source_name'] }, false); //$this->checksourcesubsource($source,$subsource);
                if (!validSubsource[0]) {
                    errorMessage['source_name'] = 'Subsource is Invalid';
                } else {
                    response['sub_source_id'] = (validSubsource[0] && validSubsource[0][0]) ? validSubsource[0][0]['id'] : '';
                }
            } else {
                errorMessage['sub_source_name'] = 'Subsource is blank';
            }

            if (data['car_id']) {
                // let getarlist = helperData.getCarmmvList;
                let checkCar = false;
                if (helperData.getCarmmvList && helperData.getCarmmvList['data'] && helperData.getCarmmvList['data']['make']) {
                    checkCar = _.find(helperData.getCarmmvList['data']['make'], { id: +data['car_id'] });
                }

                if (!checkCar) {
                    errorMessage['car_id'] = 'Car is invalid';
                }
            }

            if (data['lead_platform']) {
                // let getarlist = helperData.getCarmmvList;
                let checkPlatform = helperData.platform.includes((data['lead_platform']).toLowerCase());

                if (!checkPlatform) {
                    errorMessage['lead_platform'] = 'Platform is invalid';
                }
            }

            if (!data['dealer_id']) {
                errorMessage['gcd_code'] = 'GCD code is Invalid';
            }

            resolve({ errorMessage: errorMessage, response: response });
        } catch (err) {
            reject(err);
        }
    });
}

LeadModel.getUserByClusterAndPrice = async (clusterId, pRangeId) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT id FROM " + UBLMS_USER + " WHERE  find_in_set(" + clusterId + ",allowed_city_ids) <> 0 and find_in_set(" + pRangeId + ",price_range) <> 0 and status = '1' and role_id = " + L2_USER_ROLE_ID + " order by id asc limit 0,1";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    })
}

LeadModel.getleaddetails = async (mobileWithCountryCode, lead_ref_id) => {
    let mobile_number = mobileWithCountryCode.replace('+62', '');
    let sql = `select c.status AS customerStatus, l.id as lead_id, l.customer_id, lc.id as leads_cars_id, lc.car_id,
    lc.sub_status_id from ${UBLMS_CUSTOMER} c inner join  ${UBLMS_LEADS} l on l.customer_id=c.id inner join ${UBLMS_LEADS_CARS} lc on lc.lead_id=l.id where l.id=${lead_ref_id} order by l.id desc limit 1`
    return new Promise((resolve, reject) => {
        // let sql = `select c.status AS customerStatus, l.id as lead_id, l.customer_id, lc.id as leads_cars_id, lc.car_id,
        // lc.sub_status_id from ${UBLMS_CUSTOMER} c inner join  ${UBLMS_LEADS} l on l.customer_id=c.customer_id inner join ${UBLMS_LEADS_CARS} lc on lc.lead_id=l.lead_id where c.customer_mobile=? order by l.lead_id desc limit 1`;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT})
            .then(result => {
                resolve(result);
                if(result[0].sub_status_id ==12){
                    resolve(-1);
                }else{
                    resolve(result[0]); 
                }
            }).catch(error => {
                reject(error);
            });
    });
}

LeadModel.getSubleadsFromLeadId = async (lead_id) => {
    let sql = `SELECT 
    ulc.id, ulc.car_id, ulc.status_id,
    CASE
        WHEN ulc.sub_status_id = 34 THEN '3'
        WHEN ulc.sub_status_id = 35 THEN '9'
        ELSE ulc.sub_status_id
    END AS sub_status_id,
    ulc.calling_status_id, ulc.make_id, ulc.model_id, ulc.variant_id, ulc.make_name AS make, ulc.model_name AS model, ulc.variant_name AS carversion, uwi.walkin_datetime
FROM ${UBLMS_LEADS_CARS} ulc
LEFT JOIN ${UBLMS_WALKING_INFO} uwi ON uwi.leads_cars_id = ulc.id and uwi.status='1'
WHERE ulc.lead_id = ${lead_id}`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}
LeadModel.getWalkinDetailByLeadId = async (params) => {
    let field = `id, leads_cars_id, lead_id,walkin_status`;
    let sql = `select ${field}  from ${UBLMS_WALKING_INFO} where lead_id=? and leads_cars_id =? and walkin_status =?   order by id desc LIMIT 0,1 `;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: params })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}
LeadModel.updateWalkinInfoData = async (params) => {
    let replaceValue = [params.cur_date, params.walking_id];
    let sql = `update ${UBLMS_WALKING_INFO} set status=2, updated_at=? where id=?`;   
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.UPDATE, replacements: replaceValue })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}

LeadModel.insertWalkingInfo = async (params) => {
    let sql = `insert into ${UBLMS_WALKING_INFO} set leads_cars_id=?, lead_id=?, walkin_datetime=?,walkin_status=?,status=?,created_at=?,api_flag=?,added_by=?,user_type=?`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: params })
            .then(insertCust => {
                return resolve(insertCust[0]);
            })
            .catch(error => {
                reject(error);
            });
    });
}

LeadModel.insertConversionInfo= async(params)=>{
    let sql = `insert into ${UBLMS_CONVERSION_INFO} set lead_id=?, car_id=?, leads_cars_id=?, dealer_id=?,make_id=?, model_id=?, variant_id=?, kms_driven=?, make_name=?, model_name=?, variant_name=?, registration_no=?, purchased_price=?, purchased_date=?, manufacture_year=?,fuel_type=?, buyer_user_type=?,seller_user_type=?, added_on=?, added_by=?, user_type=?, cnfm_status=?, conversion_source=?`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: params })
            .then(insertCust => {
                return resolve(insertCust[0]);
            })
            .catch(error => {
                reject(error);
            });
    });
}

LeadModel.getmainleadstatusdetails = async (lead_id) => {
    let sql = `select status_id,sub_status_id,due_date from ${UBLMS_LEADS} where id=?`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: [lead_id] })
            .then(result => {
                resolve(result[0]);
            })
            .catch(error => {
                reject(error);
            });
    });
}

LeadModel.updateLead = async(params) => {
    let sql = `update ${UBLMS_LEADS} set status_id=?, sub_status_id=?, calling_status_id=?, num_attempt=?, due_date=? where id=?`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.UPDATE, replacements: params})
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}
LeadModel.getBlockingInfo = async(customer_id)=>{
    let sql = `select * from ${UBLMS_CLOSED_BLOCKED_LEADS} where customer_id = ? order by id DESC limit 0,1`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: [customer_id] })
            .then(result => {
                resolve(result[0]);
            })
            .catch(error => {
                reject(error);
            });
    });
}
LeadModel.insertRecord = async (data, TABLE) => {
    try {        
        let custObj = data;        
        let allKeys = Object.keys(custObj);        
        let columns = allKeys.join("=?,");
        let allValues = Object.values(custObj);
        return new Promise((resolve, reject) => {
            let sql = `INSERT INTO ${TABLE} SET ${columns}=?`;
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: allValues})
                .then(res => {
                    resolve(res[0]);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {
        // console.log('Error in updateCustomer----', error);
    }
}
LeadModel.updateSubleads = async (status_id, sub_status_id, calling_status, lead_id, dealer_id) => {
    let sql = `UPDATE ${UBLMS_LEADS_CARS} SET status_id = ${status_id}, sub_status_id = ${sub_status_id}, calling_status_id = ${calling_status} WHERE lead_id = ${lead_id} AND dealer_id = ${dealer_id}`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.UPDATE })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}
LeadModel.getLeadDetailsByCarId = async(params)=>{
let sql = `SELECT 
ldscar.car_id,
cstmr.name as customer_name,
cstmr.customer_city_name,
cstmr.customer_mobile,
lds.id as lead_id,
lds.source_id AS lead_source,
lds.created_at AS created_date
FROM
${UBLMS_LEADS} lds
    INNER JOIN
${UBLMS_LEADS_CARS} ldscar ON ldscar.lead_id = lds.id
    INNER JOIN
${UBLMS_CUSTOMER} cstmr ON cstmr.id = lds.customer_id WHERE ldscar.car_id IN (${params.join()}) GROUP BY lds.id ORDER BY lds.id DESC`;
return new Promise((resolve, reject) => {
    AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result => {
            resolve(result);
        }).catch(error => {
            reject(error);
        });
});
}
LeadModel.getSubLeadsStatus = async (leadId) => {
    let sql = `SELECT id AS leads_car_id, lead_id, dealer_id, car_id, status_id, sub_status_id
FROM ${UBLMS_LEADS_CARS}
WHERE
    lead_id = ${leadId}
        AND status_id IN (SELECT 
            MAX(status_id) AS id
        FROM
        ${UBLMS_LEADS_CARS}
        WHERE
            lead_id =${leadId}
        GROUP BY lead_id , dealer_id)
GROUP BY lead_id , dealer_id`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}
LeadModel.checkSubleadExistanceByCarIdAndMobile = async (params) => {
    let sql = `SELECT COUNT(*) AS total FROM ${UBLMS_LEADS} l
    INNER JOIN ${UBLMS_LEADS_CARS} lc ON l.id = lc.lead_id
    INNER JOIN ${UBLMS_CUSTOMER} c ON c.id = l.customer_id
    WHERE lc.car_id =? AND c.customer_mobile =? `;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: params})
            .then(result => {
                resolve(result[0]);
            }).catch(error => {
                reject(error);
            });
    });
}

LeadModel.updateDealerId = async (carId, dealer_id) => {
    let sql = `update ${UBLMS_LEADS_CARS} set dealer_id = ${dealer_id} where car_id =${carId}`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.UPDATE, replacements: [dealer_id, carId] })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}

LeadModel.getLeadCountByCarIDandDate = async (param) => {
    let sql = `SELECT car_id, COUNT(lead_id) AS total_leads
                FROM ${UBLMS_LEADS_CARS}
            WHERE
                car_id IN (`+ param.carIds.join() + `)
                    AND DATE_FORMAT(created_at, '%Y-%m-%d') >= '`+ param.startDate + `'
                    AND DATE_FORMAT(created_at, '%Y-%m-%d') <= '`+ param.endDate + `'`;
    if (param.backendLead == 0) {
        sql += ` AND added_by > ` + param.backendLead;
    }
    sql += ` GROUP BY car_id`;
    return new Promise((resolve, reject) => {
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}

module.exports = LeadModel;
