const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class RecomendCarDetailsSharedModel extends Model{
}

RecomendCarDetailsSharedModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    temp_lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    customer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    temp_customer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    agent_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    message:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_RECOMM_CAR_DETAILS_SHARED,
    freezeTableName: true
});


RecomendCarDetailsSharedModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await RecomendCarDetailsSharedModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

RecomendCarDetailsSharedModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkingInfoModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


RecomendCarDetailsSharedModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if((params.status))
        dataModel.status = params.status;
    if(params.added_by)
        dataModel.added_by = params.added_by
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
        return dataModel;
}


module.exports = RecomendCarDetailsSharedModel;