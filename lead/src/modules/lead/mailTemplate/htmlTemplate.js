const ApiController = require(COMMON_CONTROLLER + 'ApiController');
var AppModel = require(COMMON_MODEL+'appModel')
const dbConfig = require(CONFIG_PATH + 'db.config');
const sequelize = dbConfig.sequelize;
const Op = sequelize.Op;
const conf = require("../../../../config/config");

class MailTemplate extends AppModel {
  constructor() {
    super();
  }
}

MailTemplate.getCarmudiMailTemplate = async (details, carDetails, dealerData, isCust, status, sublead) => {
  return new Promise((resolve, reject) => {     
   let name = '';
   let carmudi_url = '';
   let zigwheel_url = '';
   if(carDetails.web_url){
      let carmudi = carDetails.web_url.filter(cars => cars.key == 'carmudi');
      carmudi_url = carmudi[0].url;
      let zigwheel = carDetails.web_url.filter(cars => cars.key == 'zigwheels');
      zigwheel_url = zigwheel[0].url;
   }

   let datas = '';
   let content = '';
   let footerNote = '';
   let thnkmsg = '';
     if(isCust == 1) {
      name = `Hi ${details.name}`;
      datas = `<tr>
               <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: right;vertical-align: top;padding-top: 15px;">Inquired Vehicle :</td>
               <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">${carDetails.make} ${carDetails.modelVersion}</td>
            </tr>
            <tr>
               <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: right;vertical-align: top; padding-top: 15px;">Dealership Name :</td>
               <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">${dealerData.organization}</td>
            </tr>
            <tr>
            <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: right;vertical-align: top;padding-top: 15px;">Dealership Loaction :</td>
            <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">${dealerData.address}, ${dealerData.city_name}, ${dealerData.state_name}</td>
         </tr>
         <tr>
            <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: right;vertical-align: top;padding-top: 15px;">Dealership Number :</td>
            <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">${dealerData.dealership_contact}</td>
         </tr>`;
         if(status == 5 && (sublead.car_id == carDetails.id)){
            let walkinDate = new Date(sublead.walkin_datetime);
            walkinDate.setHours(walkinDate.getHours() + 8);
            let follow_up_date = new Date(walkinDate).toLocaleString('en-US', {hour12: true});
            datas += `<tr>
                        <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">Unit Viewing Date/Time :</td>
                        <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">${follow_up_date}</td>
                     </tr>`;
         }      
         datas += `<tr>
            <td colspan="2" style="padding-top: 30px;"><a href="${carmudi_url}" style="background-image: linear-gradient(to top, #0093ff, #1b6fb8);background-color:#1b6fb8;width:100%;border:0;color:#fff;font-size:16px;text-decoration:none;text-align:center;padding: 10px 52px;
            margin-top: 15px;" rel="" target="_blank">View Detail Carmudi</a></td>
          </tr>         
         <tr>
            <td colspan="2" style="padding-top: 30px;"><a href="${zigwheel_url}" style="background-image: linear-gradient(to top, #0093ff, #1b6fb8);background-color:#1b6fb8;width:100%;border:0;color:#fff;font-size:16px;text-decoration:none;text-align:center;padding: 10px 52px;
               margin-top: 15px;" rel="" target="_blank">View Detail Zigwheels</a></td>
         </tr>`;
        if (status == 3) {
           content = `Thank you for taking our call earlier regarding your vehicle inquiry.
                                  Here are the details of the recommended Dealership for your reference:`;
           footerNote = `Please expect a call or email from our certified dealer for them to discuss 
         further information regarding your inquiry.`;
         thnkmsg = `Once again, thank you for trusting Carmudi Philippines. Have a great day ahead.`;

        } else if (status == 5) {
           content = `Greetings from Carmudi Philippines!<br/><br/>Thank you for taking our call earlier regarding your vehicle inquiry.
                                  Here are the details of the recommended Dealership for your reference:`;
           footerNote = `We also endorsed you to the said dealership, they may also reach out should there be changes in your appointment.`;
           thnkmsg = `Once again, thank you for trusting Carmudi Philippines. Have a great day ahead.<br/><br/>Best Regards,`;
        }                              
     }else if(isCust == 0){
      name = "Dear Valued Partner";
      datas = `<tr>
               <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: right;vertical-align: top;padding-top: 15px;">Inquired Vehicle :</td>
               <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">${carDetails.make} ${carDetails.modelVersion}</td>
            </tr>
            <tr>
               <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: right;vertical-align: top; padding-top: 15px;">Buyer Name :</td>
               <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">${details.name}</td>
            </tr>           
            <tr>
               <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: right;vertical-align: top;padding-top: 15px;">Buyer Contact Details :</td>
               <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">${details.mobile}</td>
            </tr>
            <tr>
               <td colspan="2" style="padding-top: 30px;"><a href="${carmudi_url}" style="background-image: linear-gradient(to top, #0093ff, #1b6fb8);background-color:#1b6fb8;width:100%;border:0;color:#fff;font-size:16px;text-decoration:none;text-align:center;padding: 10px 52px;
               margin-top: 15px;" rel="" target="_blank">View Detail Carmudi</a></td>
            </tr>         
            <tr>
               <td colspan="2" style="padding-top: 30px;"><a href="${zigwheel_url}" style="background-image: linear-gradient(to top, #0093ff, #1b6fb8);background-color:#1b6fb8;width:100%;border:0;color:#fff;font-size:16px;text-decoration:none;text-align:center;padding: 10px 52px;
                  margin-top: 15px;" rel="" target="_blank">View Detail Zigwheels</a></td>
            </tr>`;
            if (status == 3) {
               content = `Greetings from Carmudi Philippines!<br/><br/>Thank you for taking our call earlier regarding your vehicle inquiry.
                                      Here are the details of the recommended Dealership for your reference:`;
               footerNote = `Please contact the buyer to further discuss the vehicle details.`;
               thnkmsg = `Thank you for trusting Carmudi Philippines. Happy Selling! <br/> <br/>Best Regards,<br/> Carmudi.`;
    
            } else if (status == 5) {
               content = `Greetings from Carmudi Philippines!<br/><br/>Thank you for taking our call earlier regarding your vehicle inquiry.
                                      Here are the details of the recommended Dealership for your reference:`;
               footerNote = `Please contact the buyer to further discuss the vehicle details.`;
               thnkmsg = `Thank you for trusting Carmudi Philippines. Happy Selling! <br/> <br/>Best Regards,<br/> Carmudi,`;
            }              
     }
     
    let html = `<!doctype html>
    <html lang="en">
       <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="icon" href="images/fav/favicon.ico">
          <title>CARMUDI.COM</title>
       </head>
       <style>
       </style>
       <body style="font-family: Roboto,Sans-Serif,Arial;font-size:12px;">
          <table  style="width: 100%;max-width:600px;margin: 0 auto;border: 1px solid #ddd; border-collapse: collapse; border-spacing: 0; ">
             <tbody>
                <tr>
                   <td>
                      <table  style="width: 100%;max-width:600px; margin: 0 auto; border-collapse: collapse; border-spacing: 0; ">
                         <tbody>
                            <tr>
                               <td style="text-align:center" class="im"><a href="#" target="_blank"><img src="${conf.CARMUDI_HEADER_LOGO}" alt="carmudi.com.ph - Best place to buy New and Used Cars in India" title="Carmudi" style="width:100%"></a></td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td style=" padding: 15px 20px">
                      <table style="width: 100%;">
                         <tbody>
                            <tr>
                               <td style="text-align:left;font-size: 16px;font-weight: 500; color: #24272c;"><strong>${name},</strong></td>
                            </tr>
                            <tr>
                               <td style="text-align:left;color: #0b0c0e;font-size: 16px;font-weight: 400;line-height: 20px;padding-top: 15px;">${content}</td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td style="padding: 5px 20px;">
                      <table  style="width: 100%; background: #d7d7d7;  background-image: linear-gradient(to top, #d7d7d7, #f8f8f8);">
                         <tbody>
                            <tr>
                               <td style="">
                                  <table style="text-align: center;margin: 0 auto 25px;padding: 0px 10px;">
                                     <tbody> ${datas} </tbody>
                                  </table>
                               </td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td style=" padding: 15px 20px">
                      <table  style="width: 100%;">
                         <tbody>
                            <tr>
                               <td style="font-size: 15px;color: #0b0c0e; font-style: italic;">${footerNote}</td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td style=" padding: 5px 20px">
                      <table  style="width: 100%;">
                         <tbody>
                            <tr>
                               <td style="font-size: 15px;color: #0b0c0e;text-align: center;">${thnkmsg}</td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td style="padding-top: 15px;">
                      <table  style="width: 100%;background:#000000; padding: 15px 15px;">
                         <tbody>
                            <tr>
                               <td style="width: 50%;vertical-align: top;">
                                  <table style="width: 100%">
                                     <tbody>
                                        <tr>
                                           <td style="color: #fff; font-size: 11px;">Help Center / Customer Care.</td>
                                        </tr>
                                        <tr>
                                           <td style="padding-top: 10px;"><a href="#" target="_blank"><img src="${conf.CARMUDI_SMALL_LOGO}" alt="carmudi.com.ph - Best place to buy New and Used Cars in India" title="Carmudi"></a></td>
                                        </tr>
                                     </tbody>
                                  </table>
                               </td>
                               <td style="width: 50%;vertical-align: top;">
                                  <table style="width: 100%; margin: 0 30px;">
                                     <tbody>
                                        <tr>
                                           <td style="color: #fff; font-size: 11px;">Stay Connected with Us</td>
                                        </tr>
                                        <tr>
                                           <td style="padding-top: 10px;">
                                              <ul style="margin: 0; padding: 0;">
                                                 <li style="list-style: none;float: left; margin: 0 5px;"><a href=""><img src="${conf.CARMUDI_FACEBOOK_IMG}" alt="" title=""></a></li>
                                                 <li style="list-style: none;float: left; margin: 0 5px;"><a href=""><img src="${conf.CARMUDI_TWITTER_IMG}" alt="" title=""></a></li>
                                                 <li style="list-style: none;float: left; margin: 0 5px;"><a href=""><img src="${conf.CARMUDI_INSTAGRAM_IMG}" alt="" title=""></a></li>
                                                 <li style="list-style: none;float: left; margin: 0 5px;"><a href=""><img src="${conf.CARMUDI_LINKEDIN_IMG}" alt="" title=""></a></li>
                                              </ul>
                                           </td>
                                        </tr>
                                     </tbody>
                                  </table>
                               </td>
                            </tr>
                            <tr>
                               <td colspan="2" style="font-size: 12px;color: #ffffff;text-align: center; padding-top: 15px;">8F Fort Legend Tower, 3rd Ave Cor. 31st St. Bonifacio Global City, Taguig, Philippines, 1634
                               </td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
             </tbody>
          </table>
       </body>
    </html>`;
   resolve(html);
  });
}

MailTemplate.getZigwheelsMailTemplate = async (details, carDetails, dealerData, isCust, status, sublead) => {
  return new Promise((resolve, reject) => {

   let name = '';
   let carmudi_url = '';
   let zigwheel_url = '';
   if(carDetails.web_url){
      let carmudi = carDetails.web_url.filter(cars => cars.key == 'carmudi');
      carmudi_url = carmudi[0].url;
      let zigwheel = carDetails.web_url.filter(cars => cars.key == 'zigwheels');
      zigwheel_url = zigwheel[0].url;
   }

   let datas = '';
   let content = '';
   let footerNote = '';
   let thnkmsg = '';
   if(isCust == 1) {
      name = `Hi ${details.name}`;
      datas = `<tr>
                  <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">Inquired Vehicle :</td>
                  <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">${carDetails.make} ${carDetails.modelVersion}</td>
               </tr>
               <tr>
                  <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">Dealership Name :</td>
                  <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">${dealerData.organization}</td>
               </tr>
               <tr>
                  <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">Dealership Loaction :</td>
                  <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">${dealerData.address}, ${dealerData.city_name}, ${dealerData.state_name}</td>
               </tr>
               <tr>
                  <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">Dealership Number :</td>
                  <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">${dealerData.dealership_contact}</td>
               </tr>`;
         if(status == 5 && (sublead.car_id == carDetails.id)){
            let walkinDate = new Date(sublead.walkin_datetime);
            walkinDate.setHours(walkinDate.getHours() + 8);
            let follow_up_date = new Date(walkinDate).toLocaleString('en-US', {hour12: true});
            datas += `<tr>
                        <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">Unit Viewing Date/Time :</td>
                        <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">${follow_up_date}</td>
                     </tr>`;
         }
            datas += `<tr>
                  <td colspan="2" style="padding-top: 30px;"><a href="${zigwheel_url}" style="background-image: linear-gradient(to top, #c05f00, #ff9f41);background-color:#ff9f41;width:100%;border:0;color:#fff;font-size:16px;text-decoration:none;text-align:center;padding: 10px 52px;
                     margin-top: 15px;" rel="" target="_blank">View Detail Zigwheels</a></td>
               </tr>
               <tr>
                  <td colspan="2" style="padding-top: 30px;"><a href="${carmudi_url}" style="background-image: linear-gradient(to top, #c05f00, #ff9f41);background-color:#ff9f41;width:100%;border:0;color:#fff;font-size:16px;text-decoration:none;text-align:center;padding: 10px 52px;
                     margin-top: 15px;" rel="" target="_blank">View Detail Carmudi</a></td>
               </tr>`;
         if (status == 3) {
            content = `Thank you for taking our call earlier regarding your vehicle inquiry.
                                    Here are the details of the recommended Dealership for your reference:`;
            footerNote = `Please expect a call or email from our certified dealer for them to discuss 
            further information regarding your inquiry.`;
            thnkmsg = `Once again, thank you for trusting Zigwheels Philippines. Have a great day ahead.`;
   
         } else if (status == 5) {
            content = `Greetings from Zigwheels Philippines!<br/><br/>Thank you for taking our call earlier regarding your vehicle inquiry.
                                    Here are the details of the recommended Dealership for your reference:`;
            footerNote = `We also endorsed you to the said dealership, they may also reach out should there be changes in your appointment.`;
            thnkmsg = `Once again, thank you for trusting Zigwheels Philippines. Have a great day ahead.<br/><br/>Best Regards,`;
         }         
   }
   if(isCust == 0) {
      name = "Dear Valued Partner";
      datas = `<tr>
                  <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">Inquired Vehicle :</td>
                  <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">${carDetails.make} ${carDetails.modelVersion}</td>
               </tr>
               <tr>
                  <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">Buyer Name :</td>
                  <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top; padding-top: 15px;">${details.name}</td>
               </tr>
               <tr>
                  <td style="color: #7c7c7c; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">Buyer Contact Details :</td>
                  <td style="color: #000; font-size: 16px;font-weight: 400;text-align: left;vertical-align: top;padding-top: 15px;">${details.mobile}</td>
               </tr>
               <tr>
                  <td colspan="2" style="padding-top: 30px;"><a href="${zigwheel_url}" style="background-image: linear-gradient(to top, #c05f00, #ff9f41);background-color:#ff9f41;width:100%;border:0;color:#fff;font-size:16px;text-decoration:none;text-align:center;padding: 10px 52px;
                     margin-top: 15px;" rel="" target="_blank">View Detail Zigwheels</a></td>
               </tr>
               <tr>
                  <td colspan="2" style="padding-top: 30px;"><a href="${carmudi_url}" style="background-image: linear-gradient(to top, #c05f00, #ff9f41);background-color:#ff9f41;width:100%;border:0;color:#fff;font-size:16px;text-decoration:none;text-align:center;padding: 10px 52px;
                     margin-top: 15px;" rel="" target="_blank">View Detail Carmudi</a></td>
               </tr>`;
               if (status == 3) {
                  content = `Greetings from Zigwheels Philippines!<br/> We are pleased to endorse to you an interested client who inquired for your listing. See details below:`;
                  footerNote = `Please contact the buyer to further discuss the vehicle details.`;
                  thnkmsg = `Thank you for trusting Zigwheels Philippines. Happy Selling! <br/> <br/>Best Regards,<br/> Zigwheels`;
         
               } else if (status == 5) {
                  content = `Greetings from Zigwheels Philippines!<br/><br/>We are pleased to endorse to you an interested client who inquired for your listing. See details below`;
                  footerNote = `Please contact the buyer to further discuss the vehicle details.`;
                  thnkmsg = `Thank you for trusting Zigwheels Philippines. Happy Selling! <br/> <br/>Best Regards,<br/> Zigwheels`;
               }      
   }
   
    let html = `<!doctype html>
    <html lang="en">
       <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="icon" href="images/fav/favicon.ico">
          <title>ZigWheels - New Cars, Used Cars, Bikes Prices, News, Reviews, Forum</title>
       </head>
       <style>
       </style>
       <body style="font-family: Roboto,Sans-Serif,Arial;font-size:12px;">
          <table  style="width: 100%;max-width:600px;margin: 0 auto;border: 1px solid #ddd; border-collapse: collapse; border-spacing: 0; ">
             <tbody>
                <tr>
                   <td>
                      <table  style="width: 100%;max-width:600px; margin: 0 auto; border-collapse: collapse; border-spacing: 0; ">
                         <tbody>
                            <tr>
                               <td style="text-align:center" class="im"><a href="#" target="_blank"><img src="${conf.ZIGWHEELS_HEADER_LOGO}" alt="www.zigwheels.com -  New Cars, Used Cars, Bikes Prices, News, Reviews, Forum" title="zigwheels" style="width:100%"></a></td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td style=" padding: 15px 20px">
                      <table style="width: 100%;">
                         <tbody>
                            <tr>
                               <td style="text-align:left;font-size: 16px;font-weight: 500; color: #24272c;"><strong>${name},</strong></td>
                            </tr>
                            <tr>
                               <td style="text-align:left;color: #0b0c0e;font-size: 16px;font-weight: 400;line-height: 20px;padding-top: 15px;">Thank you for taking our call earlier regarding your vehicle inquiry.
                                  Here are the details of the recommended Dealership for your reference:  
                               </td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td style="padding: 5px 20px;">
                      <table  style="width: 100%; background: #d7d7d7;  background-image: linear-gradient(to top, #d7d7d7, #f8f8f8);">
                         <tbody>
                            <tr>
                               <td style="">
                                  <table style="text-align: center;margin: 0 auto 25px;padding: 0px 10px;">
                                     <tbody>
                                       ${datas}
                                     </tbody>
                                  </table>
                               </td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td style=" padding: 15px 20px">
                      <table  style="width: 100%;">
                         <tbody>
                            <tr>
                               <td style="font-size: 15px;color: #0b0c0e; font-style: italic;">${footerNote}</td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td style=" padding: 5px 20px">
                      <table  style="width: 100%;">
                         <tbody>
                            <tr>
                               <td style="font-size: 15px;color: #0b0c0e;text-align: center;">${thnkmsg}</td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
                <tr>
                   <td style="padding-top: 15px;">
                      <table  style="width: 100%;background:#000000; padding: 15px 15px;">
                         <tbody>
                            <tr>
                               <td style="width: 50%;vertical-align: top;">
                                  <table style="width: 100%">
                                     <tbody>
                                        <tr>
                                           <td style="color: #fff; font-size: 11px;">Help Center / Customer Care.</td>
                                        </tr>
                                        <tr>
                                           <td style="padding-top: 10px;"><a href="#" target="_blank"><img src="${conf.ZIGWHEELS_SMALL_LOGO}" alt="www.zigwheels.com -  New Cars, Used Cars, Bikes Prices, News, Reviews, Forum" title="zigwheels" ></a></td>
                                        </tr>
                                     </tbody>
                                  </table>
                               </td>
                               <td style="width: 50%;vertical-align: top;">
                                  <table style="width: 100%; margin: 0 30px;">
                                     <tbody>
                                        <tr>
                                           <td style="color: #fff; font-size: 11px;">Stay Connected with Us</td>
                                        </tr>
                                        <tr>
                                           <td style="padding-top: 10px;">
                                              <ul style="margin: 0; padding: 0;">
                                                 <li style="list-style: none;float: left; margin: 0 5px;"><a href=""><img src="${conf.ZIGWHEELS_FACEBOOK_IMG}" alt="" title=""></a></li>
                                                 <li style="list-style: none;float: left; margin: 0 5px;"><a href=""><img src="${conf.ZIGWHEELS_TWITTER_IMG}" alt="" title=""></a></li>
                                                 <li style="list-style: none;float: left; margin: 0 5px;"><a href=""><img src="${conf.ZIGWHEELS_INSTAGRAM_IMG}" alt="" title=""></a></li>
                                                 <li style="list-style: none;float: left; margin: 0 5px;"><a href=""><img src="${conf.ZIGWHEELS_LINKEDIN_IMG}" alt="" title=""></a></li>
                                              </ul>
                                           </td>
                                        </tr>
                                     </tbody>
                                  </table>
                               </td>
                            </tr>
                            <tr>
                               <td colspan="2" style="font-size: 12px;color: #ffffff;text-align: center; padding-top: 15px;">8F Fort Legend Tower, 3rd Ave Cor. 31st St. Bonifacio Global City, Taguig, Philippines, 1634
                               </td>
                            </tr>
                         </tbody>
                      </table>
                   </td>
                </tr>
             </tbody>
          </table>
       </body>
    </html>`;
    resolve(html);
  });
}


MailTemplate.reportEmailCsvTemplate = async () => {
   return new Promise((resolve, reject) => {  
    
     let html = `<!doctype html>
     <html lang="en">
        <head>
           <meta charset="utf-8">
           <meta http-equiv="X-UA-Compatible" content="IE=edge">
           <meta name="viewport" content="width=device-width, initial-scale=1">
           <link rel="icon" href="images/fav/favicon.ico">
           <title></title>
        </head>
        <style>
        </style>
        <body>
           Hi User,<br/><br/>


           As per your request please find the attached dealer delivery report.
           <br/><br/>
           Thanks


        </body>
     </html>`;
     resolve(html);
   });
 }

module.exports = MailTemplate;

