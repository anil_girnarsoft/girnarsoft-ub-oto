const Q = require("q");
const _ = require("lodash");
const { async } = require("q");
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const ConversionModel = require("../model/conversionInfoModel");
const dateFormat = require('dateformat');

exports.getTotalConversionByDealerId = async (req, res, next) => {
  try {
    let params = req.body;
    let todate;
    let fromdate;
    let msg = [];
    let result = {};
    if (!('from_date' in params) || !(params.from_date)) {
      msg.push("From date required");
    } else {
      fromdate = dateFormat(params['from_date'], "yyyy-mm-dd");
    }
    if (!('to_date' in params) || !(params.to_date)) {
      msg.push("To date required");
    } else {
      todate = dateFormat(params['to_date'], "yyyy-mm-dd");
    }
    if ((fromdate !== undefined) || (todate !== undefined)) {
      let dealerId = params.dealerId
      let res = await ConversionModel.getTotalConversion(dealerId, fromdate, todate);
      result = { dealer_id: dealerId, totalCount: res.length };
    }
    ApiController.sendResponse(req, res, 200, msg.join(","), result, '');
  } catch (error) {
    // console.log('Error in getTotalConversionByDealerId function ----', error);
  }
}