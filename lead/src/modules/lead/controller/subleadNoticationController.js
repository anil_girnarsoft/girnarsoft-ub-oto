const ApiController = require(COMMON_CONTROLLER + 'ApiController');
var AppModel = require(COMMON_MODEL + 'appModel');
const Q = require("q");
const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const sequelize = dbConfig.sequelize;
const Op = sequelize.Op;
const MailTemplate = require("../mailTemplate/htmlTemplate");
const { async } = require("q");
const DealerService = require(COMMON_MODEL + 'dealerService');
const NotificationService = require(COMMON_MODEL + 'notificationService');
const InventoryService = require(COMMON_MODEL + 'inventoryService');

class MailAndNotification extends AppModel {
  constructor() {
    super();
  }
}

MailAndNotification.SendMailAndSMS = async (req, params) => {
  return new Promise(async(resolve, reject)=>{
  try {    
    let cust = params.custInfo;
    let sublead = params.subleadInfo;
    let walkinStatus = params.walkinStatus;
    let walkinSubStatus = params.walkinSubStatus;
    let lead_id = params.lead_id;    
    let cust_email = (cust['email']) ? [cust['email']] : ['karan.thakur@girnarsoft.co.in'];
    let dealer_id = 0;
    let dealerRes='';
    let custmr = 0;
    let cardetails = '';
    let leadsCarDetails = await InventoryService.getStockList(req, { stock_used_car_id_arr: [sublead.car_id], call_server: 'ub', car_status: [1, 2] });    
    if (leadsCarDetails.status == 200 && leadsCarDetails.data) {
      cardetails = leadsCarDetails.data[0];
      dealer_id = cardetails.dealer_id;
    }
    try {
      dealerRes = await DealerService.getDealerDetailsByIds(req, { dealer_id: [dealer_id] });
    } catch (error) {
      // console.log('Error at dealerssss-----------------------------------------------', error);
    }
    
    let dealerData = (dealerRes && dealerRes.data) ? dealerRes.data : '';
    let html = '';
    let dealerHtml = '';
    let sub = '';
    let dlrMailSub = '';
    if (cust.source_id == 90) {
      let sender = "support@carmudi.com.ph";
      html = await MailTemplate.getCarmudiMailTemplate(cust, cardetails, dealerData[0], custmr = 1, walkinStatus, sublead);
      if (walkinStatus == 3) {
        sub = "Your Carmudi Vehicle Inquiry!";
      } else if (walkinStatus == 5) {
        sub = "Your Carmudi Unit Viewing Appointment";
      }
      //send mail to customer
      let sendEmailNotificationStatus = {};
      let emailNotificationRes = await Q.allSettled([
        NotificationService.sendEmailNotification(req, { sender: sender, receiver: cust_email, html: html, subject: sub, text: 'Car details shared', source: 'ub', template_id: BUYER_TEMPLATE_ID, lead_id: lead_id, request_type: 'status_update' })
      ]);
      sendEmailNotificationStatus = _.get(emailNotificationRes, "[0].value.data");
      
      //send mail to dealer      
      let dealerEmail = (dealerData[0].dealership_email)?[dealerData[0].dealership_email]:['anil.kumar1@girnarsoft.com'];
      dealerHtml = await MailTemplate.getCarmudiMailTemplate(cust, cardetails, dealerData, custmr = 0, walkinStatus, sublead);
      if (walkinStatus == 3) {
        dlrMailSub = "Carmudi Lead Endorsement.";
      } else if (walkinStatus == 5) {
        dlrMailSub = "We have confirmed an appointment for you!";
      }
      let sendDlrEmailNotificationStatus = {};
      let emailDlrNotificationRes = await Q.allSettled([
        NotificationService.sendEmailNotification(req, { sender: sender, receiver: dealerEmail, html: dealerHtml, subject: dlrMailSub, text: 'Car details shared', source: 'ub', template_id: DEALER_TEMPLATE_ID, lead_id: lead_id, request_type: 'status_update' })
      ]);
      sendDlrEmailNotificationStatus = _.get(emailDlrNotificationRes, "[0].value.data");
    }

    if (cust.source_id == 368) {

      let sender = "support@zigwheels.ph";
      html = await MailTemplate.getZigwheelsMailTemplate(cust, cardetails, dealerData[0], custmr = 1, walkinStatus, sublead);
      if (walkinStatus == 3) {
        sub = "Your Zigwheels Vehicle Inquiry!";
      } else if (walkinStatus == 5) {
        sub = "Your Zigwheels Unit Viewing Appointment.";
      }

      let sendEmailNotificationStatus = {};
      let emailZigNotificationRes = await Q.allSettled([
        NotificationService.sendEmailNotification(req, { sender: sender, receiver: cust_email, html: html, subject: sub, text: 'Car details shared', source: 'ub', template_id: BUYER_TEMPLATE_ID, lead_id: lead_id, request_type: 'status_update' })
      ]);
      sendEmailNotificationStatus = _.get(emailZigNotificationRes, "[0].value.data");

      //send mail to dealer      
      let dealerEmail = (dealerData[0].dealership_email)?[dealerData[0].dealership_email]:['anil.kumar1@girnarsoft.com'];
      dealerHtml = await MailTemplate.getZigwheelsMailTemplate(cust, cardetails, dealerData[0], custmr = 0, walkinStatus, sublead);
      if (walkinStatus == 3) {
        dlrMailSub = "Zigwheels Lead Endorsement.";
      } else if (walkinStatus == 5) {
        dlrMailSub = "We have confirmed an appointment for you!";
      }

      let sendDlrEmailNotificationStatus = {};
      let emailNotificationRes = await Q.allSettled([
        NotificationService.sendEmailNotification(req, { sender: sender, receiver: dealerEmail, html: dealerHtml, subject: dlrMailSub, text: 'Car details shared', source: 'ub', template_id: DEALER_TEMPLATE_ID, lead_id: lead_id, request_type: 'status_update' })
      ]);
      sendDlrEmailNotificationStatus = _.get(emailNotificationRes, "[0].value.data");

    }

    //Send SMS notification
    try {
      let buyer_msg = '';
      let seller_msg = '';
      if (walkinStatus == 3) {
        buyer_msg = `Hi ${cust.name}! Thank you for taking our call earlier regarding your vehicle inquiry. Here are the details of the recommended Dealership for your reference:\n 
<b>Inquired Vehicle:</b>${cardetails.make} ${cardetails.modelVersion} ${cardetails.reg_year}  ${cardetails.transmission} \n 
<b>Dealership Name:</b>${ dealerData[0].organization}\n 
<b>Dealership Location:</b>${ dealerData[0].address}, ${dealerData[0].city_name}, ${dealerData[0].state_name}\n 
<b>Dealership Contact Number:</b>${ dealerData[0].dealership_contact}\n 
Please expect a call / email from our partner dealer to discuss further information regarding this.\n 
Thank you and have a great day ahead.`;

        seller_msg = `Hi Valued Partner! We are pleased to endorse ${cust.name} who inquired for ${cardetails.make} ${cardetails.modelVersion}. Please contact the customer at ${cust.mobile} to further discuss the vehicle details. Thank you and happy selling!`;
      } else if (walkinStatus == 5) {
        buyer_msg = `Hi ${cust.name}! Please be reminded that you have a unit viewing appointment on ${cust.follow_up_date} at ${dealerData[0].organization} with mobile phone number ${dealerData[0].dealership_contact}. Thank you and have a great day ahead.`;

        seller_msg = `Hi Valued Partner! We are pleased to inform you that we have scheduled an appointment for an interested client who inquired for your listing:\n
<b>Inquired Vehicle:</b>${cardetails.make} ${cardetails.modelVersion} ${cardetails.reg_year} ${cardetails.transmission}\n
<b>Buyer Name:</b>${cust.name}\n
<b>Buyer Contact:</b>${cust.mobile} \n
<b>Viewing Date/Time:</b>${cust.follow_up_date}\n
We encourage you to contact the buyer to nurture the lead. Likewise, should you wish to change the appointment please do inform them as well. Happy Selling!`;
      }
      //SMS to buyer
      let sendNotificationStatus = {};
      let notificationRes = await Q.allSettled([
        NotificationService.sendSMS(req, { mobile: cust.mobile, message: buyer_msg, source: 'ub', template_id: BUYER_TEMPLATE_ID, lead_id: lead_id, request_type: 'status_update' })
      ]);
      sendNotificationStatus = _.get(notificationRes, "[0].value.data");
      //SMS to Dealer
      let dealerNotificationStatus = {};
      let dlrnotificationRes = await Q.allSettled([
        NotificationService.sendSMS(req, { mobile: dealerData[0].dealership_contact, message: seller_msg, source: 'ub', template_id: DEALER_TEMPLATE_ID, lead_id: lead_id, request_type: 'status_update' })
      ]);
      
      dealerNotificationStatus = _.get(dlrnotificationRes, "[0].value.data");
    } catch (err) {
      // console.log('Error in SMS section:----------------', err);
    }
    //Send mobile Notification to Dealer
    
    try {      
      let notification_msg = notification_title = '';
      if (cardetails.user_type == 'D') {        
        if (walkinStatus == 3 && walkinSubStatus == 3) {
          notification_msg = 'You have received a call verified lead! Check it out now!';
          notification_title = 'New Lead Created';
        } else if (walkinStatus == 5 && walkinSubStatus == 9) {
          notification_msg = 'You have a new unit viewing appointment. Check it out now!"';
          notification_title = 'Walk In Scheduled';
        }
      
          let mobileNotificationRes = await Q.allSettled([
            NotificationService.sendMobileNotification(req, { dealer_id: dealerData[0].id, user_id: dealerData[0].owner.id, message_body: notification_msg, title: notification_title, tag: 'Notification', source: 'ub', template_id: DEALER_TEMPLATE_ID, lead_id: lead_id, request_type: 'status_update' })
          ]);          
          mobileNotification = _.get(mobileNotificationRes, "[0].value.data");
       
      } 
         
    } catch (error) {
      // console.log('Error in mobile notification send to dealer:----------------', error);
    }    
    resolve('success');
  } catch (errors) {
    reject(errors)
  }
  //End Notification to Dealer
});
}


module.exports = MailAndNotification;