const Q = require("q");
const _ = require("lodash");
const { async } = require("q");
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const LeadModel = require("../model/leadModel");
const InventoryService = require(COMMON_MODEL + 'inventoryService');
const DealerService = require(COMMON_MODEL + 'dealerService');
const dateFormat = require('dateformat');

exports.updateLeadStatusFromDC = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body);
    let dc_dealerid = (params['dc_dealer_id']) ? params['dc_dealer_id']:0;  
    let status = (params['status']) ? params['status'] : '';
    let mobile = (params['mobile']) ? params['mobile'] : '';
    let expected_date = (params['followdate']) ? params['followdate'] : '';
    let commets = (params['comment']) ? params['comment'] : '';
    let usertype = (params['user_type']) ? params['user_type'] : '';
    let userid = (params['user_id']) ? params['user_id'] : '';
    let comid = (params['com_id']) ? params['com_id'] : '';
    let datetime = (params['followdate']) ? params['followdate'] : date("Y-m-d H:i:s");
    let lead_ref_id = (params['lead_reference_no']) ? params['lead_reference_no'] : '';
    let currentdate = new Date().toISOString();       
    //let currenttime = time();   
    let message = '';
    if (dc_dealerid == 0 || dc_dealerid == "undefined") {
      message = "Dealer ID can't be blank. ";
    }
    if (mobile == "") {
      message = "Mobile number is blank. ";
    }
    if (usertype == "" || userid == "") {
      message = "User type or user id is blank. ";
    }   
    let leaddetails = ''; 
    if(dc_dealerid && mobile) {
      leaddetails = await LeadModel.getleaddetails(mobile, lead_ref_id);      
    } 
    
    if (leaddetails == undefined) {
      message = "Customer not exist with dealer id or mobile number. ";
    } else if (leaddetails == '-1') {
      message = "Lead already marked as converted on this dealer id and mobile number. ";
    }  
    if(message == '') {
      let lead_id = leaddetails[0].lead_id;
      let customer_id = leaddetails[0].customer_id;
      let customerStatus = leaddetails[0].customerStatus;
      let leads_cars_id = leaddetails[0].leads_cars_id;
      let dealer_id = dc_dealerid;
      let arrInfo = {dealer_id: dealer_id, mobile: mobile, customer_id: customer_id, customer_status: customerStatus, lead_id: lead_id, car_id: leaddetails[0].car_id, data: params};  
      await savedcomments(req, arrInfo);  
      message = "Comments added successfully.";
    } 
    let lead_id = leads_cars_id = customer_id = 0; 
    if(leaddetails.lead_id) {
      lead_id = leaddetails.lead_id;
      leads_cars_id = leaddetails.leads_cars_id;
      customer_id = leaddetails.customer_id;
    }
    ApiController.sendResponse(req, res, 200, message);
  } catch (error) {

  }
}

const savedcomments = async (req, leadsData) => {
  try {
    let newstatus = {};
    let dealer_id = leadsData.dealer_id;
    let mobile = leadsData.mobile;
    let lead_id = leadsData['lead_id'];
    let customer_id = leadsData['customer_id'];
    let customer_status = leadsData['customer_status'];
    let lead_data = leadsData.data;
    let car_id = (lead_data['car_id']) ? lead_data['car_id'] : '0';
    let usertype = lead_data['user_type'];
    let userid = lead_data['user_id'];
    let comment = lead_data['comment'];
    let status = ArrDCStatus[lead_data['status']];
    let followdate = new Date(lead_data['followdate']).toISOString();
    let walkin_date = lead_data['walkindate'];//walkindate        
    let currentDate = new Date().toISOString();
    if (followdate == "undefined") {
      followdate = new Date().toISOString();
    }
    if (walkin_date == "undefined") {
      walkin_date = new Date().toISOString();
    }
    if (status) {
      let call_duration = (lead_data['call_duration']) ? lead_data['call_duration'] : 0;
      newstatus = await getstatusdetails(status, usertype, call_duration);
    }
    let newLeadCarFlag = false;
    let leadscarsdata =  await getallsubleadsofdealers(req, lead_id, dealer_id, status, car_id);
    if (leadscarsdata == undefined) {
      if (car_id && (status == 'customer_offer' || status == 'booked' || status == 'converted')) {
        let carInfoArr = await InventoryService.getStockList(req, { stock_used_car_id_arr: [car_id] });
        let leadcarsData = {
          lead_id: lead_id, car_id: car_id, make_id: carInfoArr['0']['make_id'],
          make_name: carInfoArr['0']['make'], model_id: carInfoArr['0']['model_id'],
          model_name: carInfoArr['0']['model'], variant_id: $carInfoArr['0']['db_version_id'],
          variant_name: carInfoArr['0']['db_version'], status_id: newstatus['status'],
          sub_status_id: newstatus['sub_status'], calling_status_id: newstatus['call_status'],
          price_to: carInfoArr['0']['priceto'], added_by: '-2', user_type: lead_data['user_type'],
          created_at: currentDate, is_email_sms: '1', is_dealer_sent: '1'
        };
        if (status == 'booked' && (lead_data['booking_amount'])) {
          leadcarsData['booked_amount'] = lead_data['booking_amount'];
        }//debug($leadcarsData); exit;
        //$this->db->insert(array('table' => UBLMS_LEADS_CARS, 'values' => $leadcarsData));
        await LeadModel.insertRecord(commentsData, UBLMS_LEADS_CARS);
        let newLeadCarFlag = true;
      }
    }    
    let comparedSubStatus = '';
    if (newstatus.sub_status == '34') {
      comparedSubStatus = '3';
    } else if (newstatus.sub_status == '35') {
      comparedSubStatus = '9';
    } else {
      comparedSubStatus = newstatus.sub_status;
    }
    let allSubleadsCarids = [];
    if (leadscarsdata) {
      leadscarsdata.forEach(async item => {
        if(status == 'walk_in') {
          let walkinstatus = 3;
          let walkincommentsData = [lead_id, item.car_id, walkinstatus];
          let walkinResult = await LeadModel.getWalkinDetailByLeadId(walkincommentsData);
          if(walkinResult.length)
              await LeadModel.updateWalkinInfoData({walking_id:walkinResult[0].id, cur_date: currentDate});
          if(status == 'customer_offer' && (lead_data['offer_amount'])) {
              walkincommentsData['offer_price'] = lead_data['offer_amount'];
          }
          walkincommentsData = [item.car_id, lead_id, walkin_date, walkinstatus, 1, currentDate, '1', '-2', usertype];
          await LeadModel.insertWalkingInfo(walkincommentsData);
      } else if((status == 'converted' || status=='booked') && (usertype != 'dealer' && usertype != 'telecaller')) {
        let purchaseInsertArray = [lead_id, car_id,item.leads_cars_id, dealer_id,item.make_id, item.model_id, item.variant_id, item.km, item.make, item.model, item.carversion, item.regno,lead_data.sale_amount, currentDate, item.myear, item.fuel_type, 'D', 'D', currentDate, '-2', lead_data.usertype, 1, 1];
        if(usertype == 'bm') {
            purchaseInsertArray.conversion_source = '4';
        } else if(usertype == 'dc') {
            purchaseInsertArray.conversion_source = '5';
        }
        await LeadModel.insertConversionInfo(purchaseInsertArray);     

    }
    
    await LeadModel.updateSubleads(newstatus['status'], newstatus['sub_status'], newstatus['call_status'], lead_id, dealer_id);
      //  console.log('foreach elements---------------', item);
      });
    }
    let dealerUserId=0;
    if(usertype == 'dealer'){
      let leadsCarDealersDetails = await DealerService.getDealerDetailsByIds(req, {dealer_id:[userid]});
      let dealersData = [];
      if(leadsCarDealersDetails.status == 200 && leadsCarDealersDetails.data){
        dealersData = leadsCarDealersDetails.data;
        dealerUserId = dealersData.id;
      }
    }else {
      dealerUserId = userid;
   }
   comment = (comment)?comment+" By "+usertype : " By "+usertype; 
   //console.log('*************************leadscarsdata********************************', leadscarsdata);
    
    let commentsData = {
      lead_id: lead_id,
      status_id: newstatus.status,
      sub_status_id: newstatus.sub_status,
      calling_status_id: '0',
      leads_cars_id:leadscarsdata[0].car_id,
      comment:comment,
      due_date:followdate,
      is_dialer_count:0,
      created_at:new Date().toISOString(),
      added_by:0,
      dc_user_id:dealerUserId,
      dc_user_type:usertype     
    };      

    if (newstatus.sub_status == 5) {
      commentsData = commentsData.push('set_by_dealer');
    } 
    await LeadModel.insertRecord(commentsData, UBLMS_CONVERSATION);
    let conversionData = {
      lead_id: lead_id,
      status_id: newstatus.status,
      sub_status_id: newstatus.sub_status,
      calling_status_id: '0',
      leads_cars_id:leadscarsdata[0].car_id,
      comment:comment,
      due_date:followdate,
      is_dialer_count:0,
      added_on:new Date().toISOString(),
      added_by:0,
      dc_user_id:dealerUserId,
      dc_user_type:usertype     
    }; 
    await LeadModel.insertRecord(conversionData, UBLMS_CONVERSION);
    
    let convComment = '';
    if ((usertype == 'dealer' || usertype == 'telecaller') && status == 'converted') {
      convComment = 'Dealer has marked this lead as converted.';
      commentsDataNew = {
        lead_id: lead_id,
        status_id: newstatus.status,
        sub_status_id: newstatus.sub_status,
        calling_status_id: '0',
        latest: '1',
        comment: convComment,
        is_dialer_count: '0',
        created_at: dateFormat('yyyy-mm-dd HH:MM:ss'),
        added_by: 0,
        dc_user_id: dealerUserId,
        dc_user_type: usertype
      };
      //await LeadModel.insertConversation(commentsDataNew);
      await LeadModel.insertRecord(commentsDataNew, UBLMS_CONVERSATION);
      await LeadModel.insertRecord(commentsDataNew, UBLMS_CONVERSION);
    }
     let due_date = followdate;
    // let leadstatus = LeadModel.getmainleadstatusdetails(lead_id);
    // if(leadstatus.due_date <= currentDate){
    //   due_date = new Date(Date.now() + ( 3600 * 1000 * 24));
    // }else{
    //   due_date = leadstatus.due_date;
    // }
    let leadDataToUpdate = [];
    leadDataToUpdate= [newstatus.status, newstatus.sub_status, newstatus.call_status, 0];

    if (status == 'walk_in') {
      leadDataToUpdate.push(due_date);
    } else if (status == 'interested' || (status == 'follow_up' && call_duration == '1') || status == 'walked_in' || status == 'customer_offer' || status == 'booked' || (status == 'converted' && (usertype == 'dealer' || usertype == 'telecaller'))) {
      leadDataToUpdate.push(due_date);
    } else if (status == 'converted') {
      leadDataToUpdate.push(due_date);
    }
    leadDataToUpdate.push(lead_id);
    await LeadModel.updateLead(leadDataToUpdate);
    /*
    arrUnblockSubStatus = ['10', '11', '26'];
    if(customer_status != '1' && (arrUnblockSubStatus.indexOf(newstatus.sub_status)!= -1)) {
      let arrBlockingStatus = LeadModel.getBlockingInfo(customer_id);
      if(arrBlockingStatus && arrBlockingStatus.blocked_reason) {
          if(arrBlockingStatus.blocked_reason == '4') {
              let blockedArray['status'] = '0';
              $blockedArray['updated_date'] = date('Y-m-d H:i:s');
              $blockedUpdate = array('table' => UBLMS_CLOSED_BLOCKED_LEADS,
                  'values' => $blockedArray,
                  'condition' => array('id' => $arrBlockingStatus['0']['id'])
              );
              $updateVal = $this->db->update($blockedUpdate);
              
              $conditionUpdate = array('table' => UBLMS_CUSTOMER,
                  'values' => array('active' => '1', 'updated_on' => date('Y-m-d H:i:s')),
                  'condition' => array('customer_id' => $customer_id)
              );
              $updateVal = $this->db->update($conditionUpdate);
          }
      }
  }
  */

  } catch (error) {
    // console.log('line no 135', error);
  }
}

const getallsubleadsofdealers = async (req, lead_id, dealer_id, status, car_id) => {
  try {
    let leadCarData = await LeadModel.getSubleadsFromLeadId(lead_id); 
    let car_ids = [];
    for (i of leadCarData) {
      car_ids.push(i.car_id);
    }
    let cars = await InventoryService.getStockList(req, { stock_used_car_id_arr: car_ids });
    let cardetails = cars.data;
    for (i of leadCarData) {
      cardetails.map(carData => {
        if (carData.id == i.car_id) {
          i.regno = carData.reg_no;
          i.km = carData.km_driven;
          i.myear = carData.reg_no;
          i.fuel_type = carData.fuel_type;
          i.dealer_id = carData.dealer_id;
        }
      });
    }
    return leadCarData;
  } catch (error) {
    // console.log('getallsubleadsofdealers --------------', error);
  }
}

const getstatusdetails = async (status, usertype, call_duration = 0) => {
  let newStatuses = {}; 
  switch (status) {
    /*case ($status == 'follow_up' && $call_duration == '1'):
        $newStatuses = array('status' => '2', 'sub_status' => '2', 'call_status' => 5);
        break;*/
    case 'interested':     
      newStatuses = { status: 3, sub_status: 34, call_status: 7 };
      break;
    /*case ($status == 'interested' && $usertype == 'dealer'):
        $newStatuses = array('status' => '3', 'sub_status' => '5', 'call_status' => 18);        break;*/
    case 'walked_in_done':      
      newStatuses = { status: 5, sub_status: 38, call_status: 38 };
    break;

    case 'walk_in':      
      newStatuses = { status: 5, sub_status: 35, call_status: 38 };
      break;
    case 'walked_in':
      newStatuses = { status: 6, sub_status: 10, call_status: 43 };
      break;
    case 'customer_offer':
      newStatuses = { status: 6, sub_status: 11, call_status: 48 };
      break;
    case 'booked':
      newStatuses = { status: 6, sub_status: 12, call_status: 105 };
      break;
    case 'converted':
      if (usertype == 'dealer' || usertype == 'telecaller') {
        newStatuses = { status: 6, sub_status: 10, call_status: 43 };
      } else {
        newStatuses = { status: 6, sub_status: 12, call_status: 0 };
      }
      break;
    case 'closed':
      newStatuses = { status: 7, sub_status: 27, call_status: 0 };
      break;
      endswitch;
      // print_r($newStatuses);    
  } 
  return newStatuses;
}
