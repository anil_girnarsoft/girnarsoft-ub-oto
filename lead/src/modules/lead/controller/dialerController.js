const leadCarModel = require('../model/leadCarModel');
const leadDialerModel = require('../model/leadDialerModel');

const LeadController = require('./leadController');
const ClusterController = require('../../cluster/controller/clusterController');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
var async = require("async");
const _ = require("lodash");
const CommonHelper = require(HELPER_PATH + 'CommonHelper');
const leadModel = require('../../lead/model/leadModel');
const moment = require("moment");


exports.getDialerLeads = async (req, res, next) => {

    let params = req.body;

    let paramDialerType = (params.dialer_type) ? (params.dialer_type).toUpperCase() : '';
    let dialerLeadsParams = {isPaginationApply: false};

    req['body']['isPaginationApply'] = false;
    req['body']['addToDialer'] = true;

    //FOR L2
    if(paramDialerType == 'L2'){
        //GET PREVIOUS DATE
        let curD = new Date().toISOString().split('T')[0];
        dialerLeadsParams['created_at'] = curD;
    }

    //CHECK ALREADY ADDED LEADS AND NOT TO FIND THEM 
    let addedDialerLead = await leadDialerModel.get(dialerLeadsParams);
    let excludedLeadIds = [];
    if(addedDialerLead && addedDialerLead[0] && addedDialerLead[0].length){
        excludedLeadIds = addedDialerLead[0].map(el=>el.lead_id);
        req['body']['excludedLeads'] = excludedLeadIds;
    }
    //FOR L1 & L3 Cron Function
    if(!paramDialerType){
        req['body']['page_number'] = 1;
        req['body']['page_limit'] = 200;
        req['body']['isPaginationApply'] = true;
        req['body']['created_date_within_days'] = 22;
        req['body']['dialer_status'] = '0';
        req['body']['sort'] = [{sort_type: 'DESC', sort_by: 'ubl.id'}];
        req['body']['status_id'] = 1; //NON-CONTACTED LEADS ONLY  
    }

    //IF FOLLOW UP DATES NOT GIVEN
    if(!params['follow_up_from_date'] && !params['follow_up_to_date']){
        let endOfDay = moment.utc().endOf('day'); //TODAY START DATE & TIME
        req['body']['follow_up_to_date'] = new Date(endOfDay._d).toISOString();
    }

    //GET ALL MATCHED LEADS
    let leadsData = await LeadController.getLeadList(req, res, next);

    if(leadsData && leadsData.length){
        //GET LEAD CARS
        let dialerLeadsCars = await getAddToDialerLeads(req, res, next, leadsData)

        let leadIds = [];
        let leadArrayMapping = [];

        if(dialerLeadsCars && dialerLeadsCars.length){
            dialerLeadsCars.forEach(element => {
                let addIdsObj = {lead_id: element.lead_id, customer_mobile: element.customer_mobile}; //OBJECT TO PUSH

                if(!leadArrayMapping[element.lead_id]){
                    leadArrayMapping[element.lead_id] = {"paid_scores": [], 'city_id': []};
                }

                //FOR DIALER TYPE L1/L3 & PAID SCORE FREE FOR DEALER/INDIVIDUAL
                //FOR DIALER TYPE L2 & AT LEAST ONE PAID SCORE IS PAID
                if(paramDialerType == '' || (paramDialerType == 'L2' && element.user_type == 'D')){
                    leadArrayMapping[element.lead_id] = {
                            ...leadArrayMapping[element.lead_id], 
                               "paid_scores": 
                               [...leadArrayMapping[element.lead_id]["paid_scores"], element.paid_score_id],
                               "city_id": 
                               [...leadArrayMapping[element.lead_id]["city_id"], element.car_city_id],
                               addIdsObj
                    };
                }
            });
        }
        //IN CASE OF LIST ID 1 CHECK IS PAID SCORE SHOULD BE FREE
        if(Object.keys(leadArrayMapping).length){
            async.forEach(Object.keys(leadArrayMapping), async (el, callback) => {
                
                //FIND CITY CLUSTER
                let getLeadCarClusterData = await getLeadCarCluster(req, res, leadArrayMapping[el]);
                
                leadArrayMapping[el]['paid_scores'] = leadArrayMapping[el]['paid_scores'].filter(elm=> elm);

                if(!leadArrayMapping[el]['paid_scores'].length) {
                    await leadModel.updateOne(el, {dialer_status:'4'}); //NO PAID SCORE FOUND
                }

                //CHECK IF PAID NOT INCLUDED THEN ACCEPT THAT LEAD FOR L1 and L3
                if(paramDialerType !== 'L2' && leadArrayMapping[el]['paid_scores'].length && !leadArrayMapping[el]['paid_scores'].includes(2)){
                    //IF CLUSTER MATCHES FOR L1
                    if(getLeadCarClusterData){
                        leadArrayMapping[el]["addIdsObj"]['dialer_type'] = 'L1';
                        leadIds.push(leadArrayMapping[el]["addIdsObj"]); 
                    }else{
                        leadArrayMapping[el]["addIdsObj"]['dialer_type'] = 'L3';
                        leadIds.push(leadArrayMapping[el]["addIdsObj"]); 
                    }

                } 
                //CHECK IF ATLEAST ONE PAID INCLUDED THEN ACCEPT THAT LEAD FOR L2
                else if(paramDialerType == 'L2' && leadArrayMapping[el]['paid_scores'].length && leadArrayMapping[el]['paid_scores'].includes(2)){
                    //ACCEPT IF CLUSTER MACTHES
                    // if(getLeadCarClusterData){
                        leadArrayMapping[el]["addIdsObj"]['dialer_type'] = 'L2';
                        leadIds.push(leadArrayMapping[el]["addIdsObj"]); 
                    // }
                }
                callback(); 
            
            }, function(err) {
                let dialerResponse = [];
                if(leadIds.length) {
                    async.forEach((leadIds), async (el, callback) => {
                        let key = _.findIndex(leadIds, el);//FIND INDEX
                        dialerResponse[key] = {};

                        //CREATE NEW RECORD
                            let decryptedLeadId = await leadDialerModel.encrypt({leadId: el.lead_id});

                            let getListIdCodes  = await CommonHelper.DialshreeAddApiConfigListIdCode(process.env.COUNTRY_CODE); 
                            let getListIdType   = getListIdCodes[el.dialer_type];

                            let leadDialerResponse = {};
                            leadDialerResponse['lead_id'] = el.lead_id;
                            leadDialerResponse['lead_id_hash'] = decryptedLeadId.lead_id_hash
                            leadDialerResponse['list_id'] = getListIdType
                            leadDialerResponse['dialer_type'] = el.dialer_type;
                            leadDialerResponse['dialer_response'] = ''
                            leadDialerResponse['created_at'] = new Date().toISOString();
                            leadDialerResponse['attempt'] = 0;
                            leadDialerResponse['last_attempt'] = null;
                            leadDialerResponse['status'] = 'pending';
                            leadDialerResponse['phone'] = el.customer_mobile || '';
                            await leadDialerModel.createOne(leadDialerResponse);
            
                            dialerResponse[key]['status'] = true;
                            dialerResponse[key]['msg'] = 'Added Successfully';
                            dialerResponse[key]['lead_id'] = el.lead_id;
            
                        callback(); 
            
                    }, function(err) {
                        dialerResponse = dialerResponse.filter(el=>el);
                        let message = 'Leads pushed to dialer successfully';

                        ApiController.sendResponse(req, res, 200, message, dialerResponse);     

                    });
                }else{
                    ApiController.sendResponse(req, res, 200, "No leads selected to add to dialer.");
                }
          });

        }else{
            ApiController.sendResponse(req, res, 200, "No leads selected to add to dialer.");
        }

        

    }else{
        msg = "No leads selected to add to dialer.";

        if(excludedLeadIds.length){
            msg = 'Lead already pushed to dialer.';
        }
        ApiController.sendResponse(req, res, 200, msg);
    }

}

//GET CLUSTER OF LEAD CITIES
getLeadCarCluster = async(req, res, params) => {
    let cityIdsUnique = _.uniq(params.city_id);
    req.body = {isPaginationApply: false, city_ids: cityIdsUnique, returnResponse: true};
    
    let getCluster = await ClusterController.getClusterList(req, res); 
    
    let findInCluster = [];
    if(getCluster.length){
        getCluster.forEach(el => {
            if(process.env.COUNTRY_CODE == 'id' && (el.name).toLowerCase() == 'jabodetabek'){
                findInCluster.push(true);
            } else if(process.env.COUNTRY_CODE == 'ph' && (el.name).toLowerCase() == 'ncr'){
                findInCluster.push(true);
            }
        })
        return findInCluster.length ? true : false ;
    }else{
        return false;
    }
}

/**
 * Get Lead Cars With city data
 */
getAddToDialerLeads = async(req, res, next, data) => {
    let leadsIds = data.map(el=>el.id);
    let findLeadsCars = await leadCarModel.getLeadsCarByIds({leadsIds: leadsIds});
    
    if(findLeadsCars.length){
        let carIds = []
        findLeadsCars.forEach(el=>{
            if(el.car_id && !carIds.includes(el.car_id)) carIds.push(el.car_id);
        });

        let result = [];
        if(carIds.length){
            req['body'] = {}
            // req['body']['car_ids'] = carIds;
            req['body']['lead_ids_array'] = leadsIds;
            req['body']['return_function'] = true;
            result = await LeadController.getAssignedCarDetails(req,res,next);
        }
        return (result || []);
  
    }else{
        return [];
    }
}

