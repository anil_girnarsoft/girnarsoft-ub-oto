const Q = require("q");
const _ = require("lodash");
const leadModel = require('../model/leadModel');
const leadCarModel = require('../model/leadCarModel');
const closedBlockedLeadModel = require('../model/closedBlockedLeadModel');
const mobileLocalityModel = require('../../mobile-locality/model/mobileLocality');
const customerModel = require('../../customer/model/customerModel');
const conversionModel = require('../model/conversionModel');
const conversionInfoModel = require('../../conversion/model/conversionInfoModel');
const leadRawLogModel = require('../model/leadRawLogModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../validation_schema");
const siteSetting = require(SITE_SETTINGS);
var fs = require('fs');
const moment = require('moment');
const walkingInfoModel = require('../../walkin/model/walkinInfoModel');
const walkinFeedbackDealerInfoModel = require('../../walkin/model/walkinFeedbackDealerInfoModel');

const carMakeController = require('../../car-make-model/controller/carMakeController');
const dealerController = require('../../dealer/controller/dealerController');
const cityController = require('../../city/controller/cityController');
const recomendCarDetailsSharedModel = require('../model/recomendCarDetailsSharedModel');
const customerReqAgentModel = require('../../customer/model/customerReqAgentModel');
const customerRequirementModel = require('../../customer/model/customerRequirementModel');
const recomendCarSearchLogModel = require('../model/recomendCarSearchLogModel');
const asyncLeadModel = require('../model/asyncLeadModel');
const walkinPurchaseInfoModel = require('../../walkin/model/walkinPurchaseInfoModel');
const communicationHistoryLogModel = require('../../log/model/communicationHistoryLogModel');
const emailTrackingLogModel = require('../../log/model/emailTrackingLogModel');
const smsInfoModel = require('../../log/model/smsInfoModel');
const closedLeadPurchaseModel = require('../model/closedLeadPurchaseModel');
const cityModel = require('../../city/model/cityModel');
const dealerBoostModel = require('../../dealer/model/dealerBoostModel');

const CommonHelper = require('../../../helpers/CommonHelper');
const ConversionPanelExportLogModel = require('../model/conversionPanelExportLogModel');

const csv = require('fast-csv');
const createCsvWriter = require('csv-writer').createArrayCsvWriter;
const { CARDEKHO_BLOCK_LEAD_API_AUTH_KEY, WA_NOTIFICATION_ENABLE } = require("../../../../config/config");
const DealerService = require(COMMON_MODEL + 'dealerService');
const InventoryService = require(COMMON_MODEL + 'inventoryService');
const LeadService = require(COMMON_MODEL + 'leadService');
const DialerLog = require('../model/dialerLogMongoModel');
const dbConfig = require(CONFIG_PATH + 'db.config');
const sequelize = dbConfig.sequelize;
const Op = sequelize.Op;
const CommonEmailHelper = require(HELPER_PATH + 'EmailHelper');

const walkinFeedbackModel = require('../../walkin/model/walkinFeedback');
const LeadCarModel = require("../model/leadCarModel");
const LeadModel = require("../model/leadModel");
const subLeadNotification = require("../controller/subleadNoticationController");
const ReportController = require("../../report/controller/reportController");
const axios = require('axios');
const clusterModel = require('../../cluster/model/clusterModel');
var async = require("async");
const WhatsappController = require('../../notifications/controller/WhatsappController');


let _this = this;

exports.saveLead = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body);
    const authUser = _.get(req, 'user');
    let id = params.id;
    if (!id) {
      params.added_by = authUser.user_id;
      let roleObj = leadModel.getModel(params);

      roleObj['customer_name'] = params['customer_name'];
      roleObj['customer_mobile'] = params['customer_mobile'];
      roleObj['customer_email'] = params['customer_email'];
      roleObj['customer_city'] = params['customer_city'];

      if ([L1_USER_ROLE_ID, L2_USER_ROLE_ID, TEAM_LEAD].includes(authUser)) {
        if (params['source_id'] == '90') {
          roleObj['sub_source_id'] = '488';
        } else if ($source_id == '4') {
          roleObj['sub_source_id'] = '1';
        } else if ($source_id == '368') {
          roleObj['sub_source_id'] = '612';
        }
      }
      roleObj['is_premium'] = params['is_premium'];
      roleObj['lead_platform'] = 'UB';

      let leadDataJson = JSON.stringify(roleObj);
      let log_date = new Date().toISOString();
      const leadRawLogData = {};
      leadRawLogData.source_id = params['source_id'];
      leadRawLogData.sub_source_id = params['sub_source_id'];
      leadRawLogData.mobile = roleObj['customer_mobile'];
      leadRawLogData.log_date = log_date;
      leadRawLogData.pushed_status = '0';
      leadRawLogData.is_ub = '1';
      leadRawLogData.city_id = params['city_id'];
      leadRawLogData.leads_data = leadDataJson;
      leadRawLogModel.createOne(leadRawLogData)
        .then(response => {
          if (response.id > 0) {
            ApiController.sendSuccessResponse(req, res, roleObj, 'lead_created_successfully');
          } else {
            ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
          }
        })
        .catch(err => {
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        })

    } else {
      params.updated_by = userId;
      let roleObjToUpdate = leadModel.getModel(params);
      leadModel.updateOne(id, roleObjToUpdate)
        .then(response => {
          if (response && response.length) {
            ApiController.sendResponse(req, res, 200, 'lead_updated_successfully');
          } else {
            ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
          }
        })
        .catch(error => {
          next(error);
        })


    }
  } catch (error) {
    next(error);
  }

}

exports.getLeadList = async (req, res, next) => {
  try {
    let page_number;
    let dealer_id='';
    let params = req.body;
    if (params.page_number) {
      page_number = params.page_number;
      delete req.body.page_number;
    } else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};    
    let data = await leadModel.get(value, page_number);
    let dealerData = data[0];
    let leadCarIds = [];


    if(dealerData.length){
      for (let i of dealerData) {
        let { car_id } = i;
        if(car_id)  leadCarIds.push(car_id);  
      }
      leadCarIds = leadCarIds.filter(el=> el)
      //get All car details by car ids
      
      let stockPostData = {};
      stockPostData['call_server'] = 'ub';
      stockPostData['car_status'] = [1,2];
      if(leadCarIds.length){
        stockPostData['stock_used_car_id_arr'] = leadCarIds;
      }
      stockPostData['page'] = 1;
      stockPostData['classified'] = true;

      let carDetails = [];
      if(leadCarIds.length){
        carDetails = await ReportController.findUsedCars(req, stockPostData);
      }

      let leadCarsDealerIds = [];  
      let leadCarsData=[];
      if (carDetails && carDetails.allUsedCars && carDetails.allUsedCars.length > 0) {
        //get all dealer ids from cars
        assignedCarDetails = carDetails.allUsedCars;
  
        if(params.priority){
          leadCarsData = assignedCarDetails.filter(car => {        
            return params.priority.indexOf(car.paid_score) !== -1;
          });
        }

        leadCarsData = assignedCarDetails;     
        for (j of leadCarsData) {        
            let { dealer_id } = j;
            leadCarsDealerIds.push(dealer_id);        
        }
      }  
      //Get All dealer details
      let leadsCarDealersDetails = await DealerService.getDealerDetailsByIds(req, {dealer_id:leadCarsDealerIds});
      let dealersData = [];
      if(leadsCarDealersDetails.status == 200 && leadsCarDealersDetails.data){
        dealersData = leadsCarDealersDetails.data;
      }

      //Map inventory car dealer id with leads car ids     
      for (let i of dealerData) {
        i.make_name = '';
        i.model_name = '';
        if(leadCarsData.length > 0){
          leadCarsData.map(async carData => {
            if(carData.id == i.car_id){
              i.car_price           = carData.car_price;
              i.dealer_id           = carData.dealer_id;
              i.make_name           = carData.make;
              i.model_name          = carData.model;
              i.make_year           = carData.make_year;
              i.reg_car_city_name   = carData.reg_car_city_name;
              i.reg_place_city_id   = carData.reg_place_city_id;
              i.user_type           = carData.user_type;
              i.car_dealer_priority = carData.paid_score;
              i.web_url             = carData.web_url;
            }
          });
        }
      }
  
      //Map dealer city name and city id with leads car customer Id
      for (k of dealerData) {
        k.customer_city_id = 0;
        k.customer_city_name = "";
        if (dealersData.length > 0) {
          dealersData.map(async dealerDetail => {
            if (dealerDetail.id == k.dealer_id) {
              k.customer_city_id    = dealerDetail.city_id;
              k.customer_city_name  = dealerDetail.city_name;
            }

            if (k.dealer_id == dealerDetail.id /*&& dealerDetail.source_id == k.source_id*/) {
              k.seller_organization = dealerDetail.organization;
              k.seller_address      = dealerDetail.address;
            }

          });
        }
      }
    }

    data[0] = dealerData;

    let roleList = leadModel.encrypt(data[0]);

    if(params.addToDialer){
      return roleList;
    }else{
      pagination['total'] = data[1];
      pagination['limit'] = PAGE_LIMIT;
      pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
      pagination.cur_page = Number(page_number);
      pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
      pagination.next_page = (pagination.pages > page_number) ? true : false;
      ApiController.sendPaginationResponse(req, res, roleList, pagination);
    }

    
  } catch (error) {
    // console.log(error)
    // next(error);
  }
}

exports.getTimeLineDetails = async (req, res, next) => {
  try {
    let page_number;
    let params = req.body
    if (params.page_number) {
      page_number = params.page_number;
      delete req.body.page_number;
    } else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await leadModel.getTimeLine(value, page_number);
    let roleList = leadModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, roleList, pagination);
  } catch (error) {
    next(error);
  }
}

exports.getAssignedCarDetails = async (req, res, next) => {
  try {
    let page_number;
    let params = req.body
    if (params.page_number) {
      page_number = params.page_number;
      delete req.body.page_number;
    } else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};

    let data = await leadModel.getAssignedCarDetails(value, page_number);
    let leadCarsData = data[0];
    let assignedCarDetails = [];
    let assignedCarsDealerIds =[];
    //append car data in lead data
    let stock_used_car_id_arr = [];
    let assignedCarDealerArr = [];
    // for (let i of leadCarsData) {
    //   if(i.car_id) stock_used_car_id_arr.push(i.car_id);
    // }
    stock_used_car_id_arr = leadCarsData.map(el=>el.car_id) 

    let stockPostData = {};
    stockPostData['call_server'] = 'ub';
    stockPostData['car_status'] = [1,2];
    stockPostData['stock_used_car_id_arr'] = stock_used_car_id_arr;
    stockPostData['page'] = 1;
    stockPostData['classified'] = true;
    // let carDetails = await InventoryService.getStockList(req, stockPostData);
    let carDetails = [];
    if(leadCarsData.length){
      carDetails = await ReportController.findUsedCars(req, stockPostData);
    }

    if (carDetails && carDetails.allUsedCars && carDetails.allUsedCars.length > 0) {
      //get all dealer ids from cars
      assignedCarDetails = carDetails.allUsedCars;
      
      //MAKE ARRAY OF [dealer_id, source_id] to get dealer having that macthed source
      assignedCarsDealerIds = assignedCarDetails.map(el=> [el.dealer_id, el.source_id])

      //Map carinfo with assigned cars
      let dealer_priority = '';
      for (let i of leadCarsData) {
        assignedCarDetails.map(carDetail => {
          if (carDetail.id == i.car_id) {         
            if (carDetail.paid_score) {
              if (carDetail.paid_score == 1) {
                dealer_priority = "Free";
              } else if (carDetail.paid_score == 2) {
                dealer_priority = "Paid";
              } else if (carDetail.paid_score == 3) {
                dealer_priority = "Paid Pro";
              }
            }
            let dealer_prt_type = null;
            if(i.dealer_type == 1){
              dealer_prt_type = 'Low';
            }else if(i.dealer_type == 2){
              dealer_prt_type = 'Medium';
            }else if(i.dealer_type == 3){
              dealer_prt_type = 'High';
            }
            i.dealer_id         = carDetail.dealer_id;
            i.car_price         = carDetail.car_price;
            i.make_year         = carDetail.make_year;
            i.make_name         = carDetail.make;
            i.model_name        = carDetail.modelVersion;
            i.make_id           = carDetail.make_id;
            i.model_id          = carDetail.model_id;
            i.variant_id        = carDetail.version_id;
            i.variant_name      = carDetail.carversion;
            i.color             = carDetail.uc_colour;
            i.km_driven         = carDetail.km_driven;
            i.fuel_type         = carDetail.fuel_type;
            i.transmission      = carDetail.transmission;
            i.paid_score        = dealer_priority;
            i.car_status        = carDetail.car_status;
            i.owner_type        = carDetail.owner_type;
            i.user_type         = carDetail.user_type;
            i.car_city_id       = carDetail.city;
            i.car_city_name     = carDetail.city_name;
            i.source_id         = carDetail.source_id;
            i.paid_score_id     = carDetail.paid_score;
            i.orig_model        = carDetail.model;
            i.web_url           = carDetail.web_url;
            i.dealer_type       = dealer_prt_type;
            i.car_image         = carDetail.car_image;
            i.display_price     = carDetail.display_price;
          }
        });
      }
    }

    //get Dealer Details from dealeids
    if (assignedCarsDealerIds.length > 0) {
      let dealerDetailRes = await DealerService.getDealerDetailsByIds(req, {seller:assignedCarsDealerIds}); 
      if (dealerDetailRes.status == 200 && dealerDetailRes.data) { 
        assignedCarDealerArr = dealerDetailRes.data;
      }
    }

    //MATCH DEALER ID & SOURCE ID WITH CAR DETAIL TO GET ADDRESS & ORGANIZATION AND MAP WITH CARINFO
    if (assignedCarDealerArr.length > 0) {
      for (let j of leadCarsData) {
        assignedCarDealerArr.map(assignedCarDealer => {
          if (assignedCarDealer.id == j.dealer_id && assignedCarDealer.source_id == j.source_id) {
            j.organization        = assignedCarDealer.organization;
            j.seller_address      = assignedCarDealer.address;
            j.dealership_contact  = assignedCarDealer.dealership_contact;
            j.latitude            = assignedCarDealer.latitude;
            j.longitude           = assignedCarDealer.longitude;
          }
        });
      }
    }

    data[0] = leadCarsData
    //end
    let roleList = leadModel.encrypt(data[0]);
    let assignedCarsArry = []
    if(roleList.length){
        //SORT LEADS CARS BY PAID/CLOSED/FREE ORDER
          let paidCars = [];
          let freeCars = [];
          let closedCarsPaid = [];
          let closedCarsFree = [];
          let newArray = [];

          roleList.forEach(elm=> {
              if(typeof elm.paid_score_id != 'undefined' && elm.paid_score_id !== 1){ //PAID CARS
                  if(elm.status_id === 7){ //PAID CLOSED CARS
                      closedCarsPaid.push(elm);
                  }else{ // OTHER PAID CARS
                      paidCars.push(elm);
                  }
              }else if(typeof elm.paid_score_id == 'undefined' || elm.paid_score_id === 1){ //FREE CARS
                  if(elm.status_id === 7){ //FREE CLOSED CARS
                      closedCarsFree.push(elm);
                  }else{ //OTHER FREE CARS
                      freeCars.push(elm);
                  }
              }
          });
          //SORT IN REVERSE PAID CARS
          let sortedArrayPaid = _.sortBy(paidCars, function(o) { return o.status_id; })
              sortedArrayPaid = sortedArrayPaid.reverse();
          
          //SORT IN REVERSE FREE CARS 
          let sortedArrayFree = _.sortBy(freeCars, function(o) { return o.status_id; })
              sortedArrayFree = sortedArrayFree.reverse();
          
          //UPDATED ARRAY
          newArray = [...sortedArrayPaid, ...closedCarsPaid, ...sortedArrayFree, ...closedCarsFree];
          assignedCarsArry = newArray;
    }

    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;

    if(params['return_function']){
      return assignedCarsArry;
    }else{
      ApiController.sendPaginationResponse(req, res, assignedCarsArry, pagination);
    }
  } catch (error) {
    // next(error);
    // console.log(error)
  }
}

exports.saveConversion = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body);
    const authUser = _.get(req, 'user');

    let customerLead = [];
    isPremium = (params['isPremium']) ? params['isPremium'] : 0;
    let message = 'Customer saved successfully';

    //CHECK MOBILE
    if (params['customerMobile']) {

      customerLead = await customerModel.getCustomerLead(params);

    }
      let dataArray = [];
      let leadId = '';
      
      //SET LEAD ARRAY IF LEAD FOUND
      // if (customerLead.length) {
      //   if (customerLead['0']['status_id'] == '6' && customerLead['0']['sub_status_id'] == '12') {
      //     dataArray = { 'converted_lead_data': 'yes', 'message': 'Lead <' + (customerLead['0']['id']) + '> already marked converted' };
      //   }
      //   dataArray = { ...dataArray, 'customer_data': customerLead[0] };
      // }else{
        // let message = "";
        let customerData = {};
        let checkSubLeadDealer = [];
  
        //IF LEAD NOT FOUND
        if (!customerLead.length) {
          // CHECK MOBILE LOCALITY
          let mobileNoFirstFour = (params['customerMobile'][0] == '0') ? (params['customerMobile']).substr(1, 4) : (params['customerMobile']).substr(0, 4);
          let telecomLocation = await mobileLocalityModel.get({ 'mobile_locality_no': mobileNoFirstFour }, '');
          
          let mobileTrueLocation = '';
          if (telecomLocation.length) {
            mobileTrueLocation = telecomLocation[0][0]['mobile_telecom_circle'];
          }
  
          customerData = {
            'name': (params['customerName']),
            'customer_email': params['customerEmail'],
            'customer_mobile': params['customerMobile'],
            'customer_city_id': params['customerCity'],
            'customer_city_name': params['customerCityName'],
            'customer_added': dateFormat("yyyy-mm-dd"),
            'customer_location_name': mobileTrueLocation,
            'status': '1',
            'ndnc': 0,
            'mobile_true_location': mobileTrueLocation,
            'created_at': new Date().toISOString()
          };
          

          //CREATE CUSTOMER
          let addCustomer = await customerModel.createOne(customerData);
          let customerId = addCustomer.id;
         
          let roleObj = { trackingSubSource: params['trackingSubSource'] };
          if ([L1_USER_ROLE_ID, L2_USER_ROLE_ID, TEAM_LEAD].includes(authUser.role_id) && !params['trackingSubSource']) {
            if (params['trackingSource'] == '90') {
              roleObj['trackingSubSource'] = '488';
            } else if ($source_id == '4') {
              roleObj['trackingSubSource'] = '1';
            } else if ($source_id == '368') {
              roleObj['trackingSubSource'] = '612';
            }
          }
  
          leadData = {
            'customer_id': customerId,
            'source_id': params['trackingSource'] || 0,
            'sub_source_id': roleObj['trackingSubSource'] || 0,
            'status_id': '6',
            'sub_status_id': '12',
            'calling_status_id': '0',
            'is_main_lead_save': '1',
            'isIndividualLeadDialer': '1',
            'dialer_status': '0',
            'platform': 'UB',
            'added_date': dateFormat("yyyy-mm-dd"),
            'is_premium': isPremium,
            'leads_status': '0',
            'created_at': dateFormat("yyyy-mm-dd")
          };
          //CREATE LEAD
          let addLead = await leadModel.createOne(leadData);
          leadId = addLead.id;
          message += ". Generated lead id is <" + leadId + ">";
        } else {
          //IF LEAD EXIST THEN UPDATE
          leadId = customerLead['0']['id'];
          let leadDataUpdate = {
            'status_id': '6',
            'sub_status_id': '12',
            'calling_status_id': '0',
            'is_main_lead_save': '1',
            'last_updated_date': dateFormat("yyyy-mm-dd"),
            'last_updated_by': authUser.role_id,
            'is_premium': isPremium,
            'leads_status': '0'
          };
  
          let updateLead = await leadModel.updateOne(leadId, leadDataUpdate);
  
          checkSubLeadDealer = await leadModel.getSubLeadDealer({ lead_id: leadId, 'dealer_id': params['sellerDealerId'] });
          message = "Conversion saved successfully for existing lead id <" + leadId + ">";
        }
  
        let leadsCars = {};
        let leads_cars_id = '';
  
        if (checkSubLeadDealer.length) {
          leads_cars_id = checkSubLeadDealer['0']['car_id'];
          leadsCars = {
            'dealer_id': params['sellerDealerId'] || 0,
            'city_id': params['sellerCityId'] || 0,
            'make_id': params['carMakeId'] || 0,
            'make_name': params['carMakeName'],
            'model_id': params['carModelId'] || 0,
            'model_name': params['carModelName'],
            'variant_id': params['carVariantId'] || 0,
            'variant_name': params['carVariantName'],
            'status_id': '6',
            'sub_status_id': '12',
            'calling_status_id': '0',
            'source_id': params['trackingSource'] || 0,
            'sub_source_id': params['trackingSubSource'] || 0,
            'last_updated_on': dateFormat("yyyy-mm-dd"),
            'added_by': params['trackingTrackBy'],
            'last_updated_by': authUser.role_id
          };
  
          let updateLeadCars = await leadCarModel.updateOne(leads_cars_id, leadsCars);
        } else {
          leadsCars = {
            'lead_id': leadId,
            'dealer_id': params['sellerDealerId'] || 0,
            'city_id': params['sellerCityId'] || 0,
            'make_id': params['carMakeId'] || 0,
            'make_name': params['carMakeName'],
            'model_id': params['carModelId'] || 0,
            'model_name': params['carModelName'],
            'variant_id': params['carVariantId'] || 0,
            'variant_name': params['carVariantName'],
            'status_id': '6',
            'sub_status_id': '12',
            'calling_status_id': '0',
            'source_id': params['trackingSource'] || 0,
            'sub_source_id': params['trackingSubSource'] || 0,
            'added_on': dateFormat("yyyy-mm-dd"),
            'added_by': params['trackingTrackBy'],
            'last_updated_on': dateFormat("yyyy-mm-dd"),
            'last_updated_by': authUser.role_id
          };
  
          let addLeadCar = await leadCarModel.createOne(leadsCars);
          leads_cars_id = addLeadCar.id;
        }
  
        //SAVE CONVERSION
        let conversationData = {};
  
        conversationData = {
          'lead_id': leadId,
          'leads_cars_id': leads_cars_id,
          'dealer_id': params['sellerDealerId'] || 0,
          'status_id': 6,
          'sub_status_id': 12,
          'calling_status_id': 0,
          'comment': 'Conversion Tracker',
          'latest': '1',
          'added_on': dateFormat("yyyy-mm-dd"),
          'added_by': params['trackingTrackBy'],
          'dc_user_id': params['trackingConversionSource'],
          'dc_user_type': params['trackingConversionSourceType']
        };
        let addConversion = await conversionModel.createOne(conversationData);
  
        //SAVE CONVERSION INFO
        let conversionInfo = {};
  
        conversionInfo = {
          'lead_id': leadId,
          'leads_cars_id': leads_cars_id,
          'car_id': '0',
          'city_id': params['customerCity'] || 0,
          'dealer_id': params['sellerDealerId'],
          'dealer': params['sellerDealerName'],
          'make_id': params['carMakeId'],
          'make_name': params['carMakeName'],
          'model_id': params['carModelId'],
          'model_name': params['carModelName'],
          'variant_id': params['carVariantId'],
          'variant_name': params['carVariantName'],
          'registration_no': params['carRegistrationNo'],
          'purchased_price': params['carPurchasePrice'],
          'conversion_source': params['trackingConversionSource'],
          'conversion_comments': 'ConversionTracker',
          'status': '1',
          'added_on': dateFormat("yyyy-mm-dd"),
          'added_by': params['trackingTrackBy']
        };
        if (params['trackingPurchaseDate']) {
          conversionInfo['purchased_date'] = params['trackingPurchaseDate'];
        }
        let addConversionInfo = await conversionInfoModel.createOne(conversionInfo);
  
        if (!leads_cars_id) {
          message = 'Not able to track correct information of Conversion';
        }
        
        dataArray = { 'customer_data': customerLead[0], 'conversion_status': message };
   
      ApiController.sendResponse(req, res, 200, message, [dataArray]);

  } catch (error) {
    next(error);
  }
}

exports.searchBlockedLeads = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body);
    const authUser = _.get(req, 'user');
    let pagination = {};
    let page_number = PAGE_NUMBER;

    if (params.page_number) {
      page_number = params.page_number;
      delete req.body.page_number;
    }

    let getBlockedLeads = await closedBlockedLeadModel.get(params, page_number);

    let updatedBlockedLeads = await closedBlockedLeadModel.encrypt(getBlockedLeads[0]);
    pagination['total'] = getBlockedLeads[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;


    ApiController.sendPaginationResponse(req, res, updatedBlockedLeads, pagination);

  } catch (error) {
    next(error);
  }
}

exports.unblockLead = async (req, res, next) => { 
  try {
    let params = _.cloneDeep(req.body);
    let resFlag = 2;
    let arrResCardekho = {};
    let arrResGaadi = {};
    let blockedArray = {};
    let cardekhoParams = {};
    let data = {};
    let blockedUpdate = {};

    if (params['customer_id']) {
      if (['1', '2', '3'].includes(params['blocked_reason'])) {
        cardekhoParams = {parameter:'subscribeUnsubscribeSMS',format:'Gson',authenticateKey: CARDEKHO_BLOCK_LEAD_API_AUTH_KEY ,MobileNo: params['customer_mobile'] ,mode:1};

        arrResCardekho = await leadModel.sentCardekhoBlockUnblock(req, cardekhoParams, '0', params['customer_mobile']);
        data = { 'mobile': params['customer_mobile'], 'mode': '0', 'reason': '' };

        arrResGaadi = await leadModel.sentGaadiBlockUnblock(data, '0');
      }

      if (Object.keys(arrResCardekho).length && arrResCardekho['status']) {
        //update blocked table
        blockedArray = { 'is_sent_cardekho': '1', 'sent_cardekho_time': dateFormat("yyyy-mm-dd") };

        let updateClosedBlockedLeadsTable = await closedBlockedLeadModel.updateOne(params['id'], blockedArray);
      }
      if (Object.keys(arrResGaadi).length && arrResGaadi['success']) {
        //update blocked table
        blockedArray['is_sent_gaadi'] = '1';
        blockedArray['sent_gaadi_time'] = dateFormat("yyyy-mm-dd");
      }
      if (params['is_sent_cardekho'] == '1') {
        arrResCardekho['status'] = true;
      }
      if (params['is_sent_gaadi'] == '1') {
        arrResGaadi['status'] = true;
      }

      if ((Object.keys(arrResCardekho).length && (arrResCardekho['status']) && Object.keys(arrResGaadi).length && arrResGaadi['status']) || (params['blocked_reason'] == '4')) {
        blockedArray['status'] = '0';
        //update customer table
        customerArray = { 'status': 1, 'updated_on': dateFormat("yyyy-mm-dd") };

        let updateCustomerTable = await customerModel.updateOne(params['customer_id'], customerArray);
        resFlag = 1;
      }
      
      if (Object.keys(blockedArray).length) {
        blockedArray['updated_date'] = dateFormat("yyyy-mm-dd");
        let updateClosedBlockedLeadsTable = await closedBlockedLeadModel.updateOne(params['id'], blockedArray);
      }
    }
    // return resFlag;
    ApiController.sendResponse(req, res, 200, '', resFlag);

  } catch (error) {
    next(error);
  }
}


exports.blockUploadBlockedLeads = async (req, res, next) => {
  let params = _.cloneDeep(req.body);
  const authUser = _.get(req, 'user');

  const results = [];
  let headers = [];
  const requiredformat = ['Customer Name', 'Customer Mobile', 'Blocking reason code'];

  const fileName = Date.now() + '_' + params['filename']; //FILE PATH
  const paths = './public/block-leads/'; //STORED DIRECTORY PATH

  //IF FOLDER NOT EXISTS THEN CREATE
  if (!fs.existsSync(paths)) {
    fs.mkdirSync(paths);
  }

  //UPLOAD FILE IN PATH
  let fileUpload = fs.writeFileSync(paths + fileName, params.myfile, { encoding: 'base64' });

  //READ FILE
  fs.createReadStream(paths + fileName)
    .pipe(csv.parse({ headers: true }))
    .on('error', error => console.error(error))

    .on('data', async (row) => {
      //create array of each parsed row
      results.push(row);

    })
    // .pipe(process.stdout)
    .on('end', (ed) => {

      try {
        let parsedData = [];

        let data = { 'reason': '' };


        if (results.length) {
          headers = Object.keys(results[0]); //PARSED HEADERS
          //DIFFERENCE BETWEEN REQUIRED HEADERS & INCOMING HEADERS
          let differences = _.difference(requiredformat, headers);

          if (differences.length) {
            //Mismatched headers
            ApiController.sendResponse(req, res, 200, res.__("headers_not_matched"), { imported: false });

          } else {

            let iteration = 0;

            results.forEach(async el => {
              let customerName = el['Customer Name'];
              let customerMobile = el['Customer Mobile'];
              let blockingCode = el['Blocking reason code'];

              let validData = {};
              let customerInfo = {};
              let customerId = '-100';

              //GET CUSTOMER
              customerInfo = await customerModel.getOne({ 'customer_mobile': customerMobile });

              if (blockingCode == '0') {
                customerId = (customerInfo && Object.keys(customerInfo)) ? customerInfo['id'] : customerId;
              } else {
                customerId = (customerInfo && Object.keys(customerInfo) && customerInfo['status'] == '1') ? customerInfo['id'] : customerId;
              }

              //DATA VALIDATION 
              validData = await closedBlockedLeadModel.checkCSVvalidationForUnblock(customerId, customerName, customerMobile, blockingCode);//check csv validation

              if (validData['status']) {
                let curTime = dateFormat('yyyy-mm-dd');
                let updatedBy = authUser['user_id'];

                let leadId = await leadModel.getLeadIdForBulkBlock(customerId);

                if (blockingCode == '0') {
                  if (leadId.length && leadId[0]['id']) {
                    closedBlockedLeadModel.unblockBlockedCustomer(updatedBy, leadId[0]['id'], customerId, customerName, customerMobile, blockingCode);
                    let updateBlockedLeads = await closedBlockedLeadModel.updateOne(customerId, { 'user_id': updatedBy, 'updated_date': dateFormat('yyyy-mm-dd'), 'status': '2' });
                  }
                } else {

                  let customerArray = { 'status': 0, 'updated_on': curTime };
                  //UPDATE CUSTOMER
                  let updateVal = await customerModel.updateOne(customerId, customerArray);

                  if (leadId.length && leadId[0]['id']) {

                    let insertVal = await closedBlockedLeadModel.createOne(
                      {
                        'lead_id': leadId[0]['id'],
                        'customer_id': customerId,
                        'blocked_reason': blockingCode,
                        'user_id': updatedBy,
                        'created_date': curTime,
                        'lead_comment': '',
                        'status': '1',
                        'is_sent_gaadi': '0',
                        'is_sent_cardekho': '0',
                        'sent_gaadi_time': dateFormat('yyyy-mm-dd'),
                        'sent_cardekho_time': dateFormat('yyyy-mm-dd'),
                        'updated_date': dateFormat('yyyy-mm-dd')

                      }
                    );

                    let leadArray = { 'status_id': '7', 'sub_status_id': '21', 'blocked_reason': blockingCode };
                    let updateLeadVal = await leadModel.updateOne(leadId[0]['id'], leadArray);

                    let leadCommentText = 'Closed-blocked marked from import block csv.';
                    let insertConvVal = await conversionModel.createOne({ 'closed_blocked_leads_id': insertVal.id, 'lead_id': leadId[0]['id'], 'status_id': '7', 'sub_status_id': '21', 'comment': leadCommentText, 'added_on': curTime, 'added_by': authUser['user_id'] });
                  }
                }
                data['reason'] = validData['errorMessage']
              } else {
                data['reason'] = validData['errorMessage'];
              }

              parsedData.push([
                customerName,
                customerMobile,
                blockingCode,
                data['reason']
              ])
              iteration++;

              if (iteration == results.length) {
                requiredformat.push('Rejected Reason');
                const csvWriter = createCsvWriter({
                  header: requiredformat,
                  path: paths + fileName
                });

                //WRITW CSV
                csvWriter.writeRecords(parsedData)
                  .then(() => {
                    ApiController.sendResponse(req, res, 200, 'data imported successfully', { imported: true });
                  });
              }
            });
          }
        } else {
          ApiController.sendResponse(req, res, 200, res.__("no_data_found_in_csv_file"), { imported: false });
        }

      } catch (err) {
        next(err);
      }
    });
}

exports.getWalkinDetails = async (req, res, next) => {
  try {
    let pagination = {};
    let data = await leadModel.getWalkinDetails(req.body);
    ApiController.sendPaginationResponse(req, res, data, pagination);
  } catch (error) {
    next(error);
  }
}

exports.saveSubLead = async (req, res, next) => {
  let responseData = '';
  //set all the values captured by form detail::start here
  //walkin_status, walkin_sub_status, walkin_call_status, walkinform_call_later_date, dd, purchaseTime, wiComp, mainleadId, subleadId, reopenFlag, isConfirm
  let params = _.cloneDeep(req.body);

  let walkinStatus = params['walkin_status'];
  let walkinSubstatus = params['walkin_sub_status'];
  let walkinCallStatus = params['walkin_call_status'];
  let walkinform_call_later_date = params['walkinform_call_later_date'] || null;
  let walkinform_walkin_date_time = params['walkinform_walkin_date_time'] || null; 
  let purchaseTime = params['purchaseTime'];
  let walkinCompleted = params['walkinCompleted'];
  let mainLeadId = params['main_lead_id']; //ublms_lead primary key
  let subLeadId = params['sub_lead_id']; //ublms_leads_car_id primary key
  let walkinCommentText = '';
  let booked_amount = 0;
  let reopenFlag = params['reopenFlag'];
  let isConfirm = params['isConfirm'];
  let subleadDealerId = params['subleadDealerId'];
  let carDetails = params.carDetails;
  let walkinPurchaseData = await leadCarModel.getWalkinPurchaseInfo(subLeadId);
  let subLeadData = await leadCarModel.getLeadsCarsData(subLeadId);

  if (walkinPurchaseData.length == 0 && walkinCompleted == '1' && (walkinStatus == '6' || walkinStatus == '7')) {
    responseData = 3;
    ApiController.sendPaginationResponse(req, res, responseData, {});
    return;
  }
  
  let walkin_dealer_info_ids = '', walkin_feedback = '', walkin_feedback_comment = '', purchased = '', walkin_asking_price = '';
  let walkinform_purchase_date = '', walkin_purchase_price = '', mainLeadComment = '', walkin_offer_price = '', conversion_source = '';
  let info = (walkinPurchaseData.length) ? JSON.parse(walkinPurchaseData[0].data) : {};
  let mmv_data = await InventoryService.getMMVList(req);
  let mmv = (mmv_data && mmv_data.data) ? mmv_data.data : {};
  let makeList = (mmv && mmv.make) ? mmv.make : [];
  let modelList = (mmv && mmv.model) ? mmv.model : [];
  let versionList = (mmv && mmv.version) ? mmv.version : [];
  
  if (walkinPurchaseData.length > 0) {

    //$info = json_decode($walkinPurchaseData['0']['data'], true); 
    let carId = (carDetails && carDetails.id == info['find_car']) ? true : false
    if (!carId && info['find_car']) {
      let carCond = {
        "user_id": subleadDealerId,
        "stock_used_car_id": info['find_car']
      }

      let result = await InventoryService.getCarDetails(req, carCond);
      carDetails = (result && result.data) ? result.data : {};
    }

    walkin_offer_price = info['walkin_offer_price'];
    walkin_asking_price = info['walkin_asking_price'];
    purchased = info['checkcar'];
    walkin_purchase_price = info['purchase_carprice'];
    walkinform_purchase_date = info['purchase_date'];
    conversion_source = info['conversion_source'];
    let booked_amount = info['booked_amount'];
    let sub_status = info['sub_status'];
    let selectedCar = info['SelectedCar'];
    let walkin_dealer_name = info['walkin_dealer_name'];
    let walkinform_walkin_date_time_arr = walkinform_walkin_date_time ? walkinform_walkin_date_time.split(' ') : [];
    
    if ((walkinStatus == 6 && walkinSubstatus == 12) && (moment(walkinform_purchase_date, 'dd-mm-yyyy') < moment(walkinform_walkin_date_time_arr['0'], 'dd-mm-yyyy'))) {
      responseData = 4;
      ApiController.sendPaginationResponse(req, res, responseData, {});
      return;
    }
    
    let isPurchased = leadCarModel.isPurchased(params.carId)
    if ((walkinStatus == 6 && walkinSubstatus == 12) && (purchased && purchased == 1) && (isPurchased && isPurchased.length && isPurchased['totalRecord']) > 0) {
      responseData = 6;
      ApiController.sendPaginationResponse(req, res, responseData, {});
      return;
    }  
    
    if ((walkinCompleted && walkinCompleted == 1) && ([5, 6, 7].indexOf(walkinStatus) != -1)) {
      walkin_dealer_info_ids = info['walkin_dealer_msg_id'];
      mainLeadComment = '';
      walkin_feedback = info['walkin_feedback'];
      if ((walkin_feedback || walkin_dealer_info_ids)) {

        let walkinDealerMessageIdArr = (walkin_dealer_info_ids.length) ? leadCarModel.getWalkinDealerInfo(walkin_dealer_info_ids) : [];
        let dealerMsg = '';
        if (walkinDealerMessageIdArr.length > 0) {
          for (let objWalkin of walkinDealerMessageIdAr) {
            dealerMsg += objWalkin['walkin_feedback'] + ". ";
          }
        } else {
          responseData = 5;
          ApiController.sendPaginationResponse(req, res, responseData, {});
          return;
        }
        if (dealerMsg != '') {
          dealerMsg = dealerMsg.substring(0, (dealerMsg.length - 2));
        }
        walkin_feedback_comment = "Walk-in date: " + (moment(walkinform_walkin_date_time, "d M Y")) + '. Walk-in Feedback: ' + walkin_feedback + '. ';


        if (walkin_offer_price) {
          walkin_feedback_comment += ' Customer offer: ' + Number(walkin_offer_price).toLocaleString('en-IN') + '.';
        }
        if (info['no_information']) {
          walkin_feedback_comment += ' Customer didn"t share any offer. ';
        }
        if (!walkin_offer_price && info['no_information'] != '1') {
          walkin_feedback_comment += ' Customer offer not entered. ';
        }
        if (walkin_asking_price) {
          walkin_feedback_comment += ' Dealer asking price: ' + Number(walkin_asking_price).toLocaleString('en-IN') + '.';
        }
        if (!(walkin_asking_price)) {
          walkin_feedback_comment += ' Dealer asking price not entered. ';
        }
        if (walkinform_purchase_date && sub_status == '12') {
          walkin_feedback_comment += ' Purchase date: '.moment(walkinform_purchase_date, "d M Y") + '.';
        }
        if (!(walkinform_purchase_date) && sub_status == '12') {
          walkin_feedback_comment += ' Purchase date not entered. ';
        }
        if (walkin_purchase_price && sub_status == '12') {
          walkin_feedback_comment += ' Purchase amount: ' + Number(walkin_purchase_price).toLocaleString('en-IN') + '.';
        }
        if (!(walkin_purchase_price) && sub_status == '12') {
          walkin_feedback_comment += ' Purchase amount not entered. ';
        }

        if (info['walkin_model'] == '' && sub_status == '12') {
          let carDetails = carDetails //$this->getcarDtailsByCarId($info['find_car']);
          //$model = $this->getParentModelDetails($carDetails[0]['model_id']);
          let model = await leadModel.getMakeModelVersionById(modelList, carDetails.model_id, 'model');
          let parentModelName = (model && model.m) ? model.m : '';
          walkin_feedback_comment += ' Purchased ' + carDetails['make'] + ' ' + parentModelName + ' ' + carDetails['carversion'] + '.';
          mainLeadComment = 'Cx purchased ' + carDetails['make'] + ' ' + parentModelName + ' ' + carDetails['carversion'] + ' from ' + walkin_dealer_name + '.';

        }
        if (info['walkin_model'] != '' && sub_status == '12') {
          // $model = $this->getParentModelDetails($info['walkin_model']);
          let model = await leadModel.getMakeModelVersionById(modelList, info['walkin_model'], 'model');
          let parentModelName = (model && model.m) ? model.m : '';
          walkin_feedback_comment += ' Purchased ' + info['make_filter'] + ' ' + parentModelName + ' '.info['version_filter'] + '.';
          mainLeadComment = 'Cx purchased ' + info['make_filter'] + ' ' + parentModelName + ' ' + info['version_filter'] + ' from ' + walkin_dealer_name + '.';

        }

      }
    }
  }
  //code to save the record inside the table::start here
  //save the record in conversation table lead_id,leads_cars_id,status_id,sub_status_id,calling_status_id,call_later_date,rating_value,comment
  let addedBy = _.get(req.headers, 'userid');
  // if ([34, 35].indexOf(walkinSubstatus) != -1) {
  //   responseData = 34;
  //   ApiController.sendPaginationResponse(req, res, responseData, {});
  //   return;
  // }
  let addedOn = new Date().toISOString();
  let updatedOn = new Date().toISOString();
  let updatedBy = _.get(req.headers, 'userid');
  /*check for valid purchase*/

  let purchaseRes = await leadCarModel.isValidPurchase(mainLeadId, reopenFlag, subLeadId);
  
  if (purchaseRes != '1') {
    responseData = purchaseRes;
    ApiController.sendPaginationResponse(req, res, responseData, {});
    return;
  }
  /*check for valid purchase*/
  /*Reopen leads*/
  if (reopenFlag == '1' && isConfirm) {
    let resp = await walkingInfoModel.inactiveWalkin(mainLeadId, subLeadId);
    resp = await conversionInfoModel.inactivePurchase(mainLeadId);
  } else if (reopenFlag == '1') {
    let resp = await conversionInfoModel.inactivePurchase(mainLeadId);
  }
  if(subLeadData && subLeadData[0]['is_new_assigned'] == 0){
     /*Reopen leads*/
      let conversionDataToInsert = {
        'lead_id': mainLeadId,
        'leads_cars_id': subLeadId,
        'dealer_id': subleadDealerId || 0,
        'status_id': walkinStatus,
        'sub_status_id': walkinSubstatus,
        'calling_status_id': walkinCallStatus,
        'call_later_date': walkinform_call_later_date,
        'comment': walkin_feedback_comment,
        'added_on': addedOn,
        'added_by': addedBy
      };
      let insertVal = await conversionModel.createOne(conversionDataToInsert);
  }
 
  //$insertVal = 1;
  // if (insertVal && insertVal.dataValues && insertVal.dataValues.id) {
    //if  car available and purchase time
    let updateUblmsLeadsCars = {};
    updateUblmsLeadsCars['car_availability'] = '0';
    updateUblmsLeadsCars['is_new_assigned'] = 0;
    updateUblmsLeadsCars['is_dealer_sent'] = params['is_dealer_sent'];
    if (purchaseTime) {
      if ((walkinStatus == 6) && ((walkinSubstatus == 10) || (walkinSubstatus == 12))) {
        updateUblmsLeadsCars['purchase_time'] = purchaseTime;
      }
    }
    updateUblmsLeadsCars['status_id'] = walkinStatus;
    updateUblmsLeadsCars['sub_status_id'] = walkinSubstatus;
    updateUblmsLeadsCars['calling_status_id'] = walkinCallStatus;
    if (['5', '11', '16', '22', '27', '32', '37', '42', '47', '52'].indexOf(walkinCallStatus)) {
      updateUblmsLeadsCars['call_later_date'] = walkinform_call_later_date;
    } else {
      updateUblmsLeadsCars['call_later_date'] = "";
    }
    if (walkinStatus == 6 && walkinSubstatus == 26) {
      updateUblmsLeadsCars['booked_amount'] = booked_amount;
    }
    if (updateUblmsLeadsCars) {
      updateUblmsLeadsCars['updated_by'] = updatedBy;
      updateUblmsLeadsCars['updated_at'] = updatedOn; 
      let updateVal = await leadCarModel.updateLeadsCar({ 'lead_id': mainLeadId, 'id': subLeadId }, updateUblmsLeadsCars)
      //code to update the main lead table status:: start
      if (updateVal[0]) {
        let statusRec = await leadCarModel.getUpdateMainLeadStatus(mainLeadId);
        
        let upMainStatus = statusRec.length ? statusRec[0]['status_id'] : '';
        let upMainSubStatus = statusRec.length ? statusRec[0]['sub_status_id'] : '';
        let upMainCallStatus = statusRec.length ? statusRec[0]['calling_status_id'] : '';
        let upMainCallLaterDate = ""
        if (['5', '11', '16', '22', '27', '32', '37', '42', '47', '52'].indexOf(upMainCallStatus) != -1) {
          upMainCallLaterDate = walkinform_call_later_date;
        } else {
          upMainCallLaterDate = "";
        }
        //update main lead table status:: start
        let updateUblmsLeads = {
          'updated_at': updatedOn,
          'updated_by': updatedBy,
          'call_later_date': upMainCallLaterDate,
          'is_main_lead_save': '2'
        }
        if (upMainStatus) {
          updateUblmsLeads['status_id'] = upMainStatus;
          updateUblmsLeads['sub_status_id'] = upMainSubStatus;
          updateUblmsLeads['calling_status_id'] = upMainCallStatus;
        }
        if ((walkinCompleted && walkinCompleted == 1) && ([5, 6, 7].indexOf(walkinStatus))) {
          updateUblmsLeads['ratings'] = '3';
        } 
        
        let updatemainLeadVal = await leadModel.updateOne(mainLeadId, updateUblmsLeads);

      }

    }
    // need to get used car details
    let subMakeName = '', subModelName = '', subVariantName = '';//, conversion_source = '';
    if ((walkinStatus == 6) && (walkinSubstatus == 12)) {
      //insert the record in the purchase detail table
      if (purchased && purchased == 1) {
        subMakeId = subLeadData[0]['make_id'];
        subModelId = subLeadData[0]['model_id'];
        subVariantId = subLeadData[0]['variant_id'];
        subMakeName = subLeadData[0]['make'];
        subModelName = subLeadData[0]['model'];
        subVariantName = subLeadData[0]['carversion'];
        walkin_reg_no = carDetails['reg_no'];
        subfuelType = carDetails['fuel_type'];
        myear = carDetails['make_year'];
        km = carDetails['km_driven'];
        sub_car_id = carDetails['id'];
        dealer_id = carDetails['dealer_id'];
        //eoc                           
      } else {

        if (info['find_car'] && info['find_car'] != '') {
          capturedcarId = info['find_car'];
          let carinfo = carDetails;
          sub_car_id = capturedcarId;
          subMakeId = carinfo['make_id'];
          subMakeName = carinfo['make'];
          subModelId = carinfo['model_id'];
          subModelName = carinfo['model'];
          subVariantId = carinfo['db_version_id'];
          subVariantName = carinfo['carversion'];
          walkin_reg_no = carinfo['reg_no'];
        } else {
          subMakeId = info['walkin_make'];
          subModelId = info['walkin_model'];
          subVariantId = info['walkin_variant'];
          walkin_reg_no = info['registration_no'];
          if ((subMakeId && makeList.length)) {
            let makeobj = getMakeModelVersionById(makeList, subMakeId, 'make');
            if (makeobj)
              subMakeName = makeobj.make;
          }
          if ((subModelId && modelList.length)) {
            let modelObj = getMakeModelVersionById(modelList, subModelId, 'model');//_.find(modelList,{'id':subModelId});
            if (modelObj)
              subModelName = modelObj.m;
          }
          if ((subVariantId && versionList.length)) {
            let versionObj = getMakeModelVersionById(versionList, subVariantId, 'version');// _.find(mmv.version,{'id':subVariantId});
            if (versionObj)
              subVariantName = versionObj.vn;
          }

          sub_car_id = '';
        }
        subfuelType = '';
        myear = '';
        km = '';
        dealer_id = carDetails['dealer_id'];
      }
      purchaseDate = "";
      if (walkinform_purchase_date) {
        purchaseDate = new Date(walkinform_purchase_date).toISOString();
      };
      let purchaseInsertArray = {
        'lead_id': mainLeadId,
        'car_id': sub_car_id || 0,
        'leads_cars_id': subLeadId || 0,
        'dealer_id': dealer_id || 0,
        'make_id': subMakeId || 0,
        'model_id': subModelId || 0,
        'model_id': subModelId || 0,
        'variant_id': subVariantId || 0,
        'make_name': subMakeName,
        'model_name': subModelName,
        'variant_name': subVariantName,
        'registration_no': walkin_reg_no,
        'purchased_price': walkin_purchase_price,
        'purchased_date': purchaseDate || null,
        'conversion_source': conversion_source || 0,
        'kms_driven': km || 0,
        'manufacture_year': myear || 0,
        'fuel_type': subfuelType,
        'buyer_user_type': 'D',
        'seller_user_type': 'D',
        'conversion_comments': walkinCommentText,
        'added_on': addedOn,
        'added_by': addedBy,
        'status': 1
      }; 
      let conditionPurchaseInsert = await conversionInfoModel.createOne(purchaseInsertArray);
    }
    //insert the records inside the walkin table::Start
    let walkinInsertValueArray = {}; 
    if (walkinform_walkin_date_time && [5, 6, 7].indexOf(walkinStatus) != -1) {
      walkinInsertValueArray['leads_cars_id'] = subLeadId;
      walkinInsertValueArray['lead_id'] = mainLeadId;
      walkinDateTime = new Date(walkinform_walkin_date_time).toISOString()//moment(walkinform_walkin_date_time).toDate().toISOString();
      walkinInsertValueArray['walkin_datetime'] = walkinDateTime;
      modal_walkinStatus = "";

      if (walkinSubstatus == 7) {
        modal_walkinStatus = "1";
      } else if (walkinSubstatus == 8) {
        modal_walkinStatus = "2";
      } else if (walkinSubstatus == 9) {
        modal_walkinStatus = "3";
      }
      if (walkinCompleted && walkinCompleted == 1) {
        modal_walkinStatus = 4;
        walkinInsertValueArray['walkin_feedback'] = walkin_feedback;
      }
      walkinInsertValueArray['walkin_status'] = modal_walkinStatus;
      walkinInsertValueArray['offer_price'] = walkin_offer_price || null;
      walkinInsertValueArray['asking_price'] = walkin_asking_price;
      walkinInsertValueArray['added_date'] = addedOn;
      walkinInsertValueArray['added_by'] = addedBy;
      
      let resp = await walkingInfoModel.updateWalkinInfoData(walkinInsertValueArray);

      //check if the records is already exists:: start
      if (modal_walkinStatus) {
        let walkinCond = {
          'lead_id': mainLeadId,
          'leads_cars_id': subLeadId,
          'walkin_datetime': walkinDateTime,
          'walkin_status': modal_walkinStatus
        }
        let existwalkData = await walkingInfoModel.getWalkinInfoData(walkinCond);
        if (existwalkData.length == 0) {
          let insertWalkinInInfo = await walkingInfoModel.createOne(walkinInsertValueArray);
          let walkin_id = insertWalkinInInfo.dataValues.id   //$this->db->lastInsertId();
          if (walkin_id) {
            let walkinDealerMsgIdArr = [];
            if (walkin_dealer_info_ids) {
              walkinDealerMsgIdArr = walkin_dealer_info_ids;
            }
            let walkinDealerMsgIdArrCount = walkinDealerMsgIdArr.length;
            if (walkinDealerMsgIdArrCount > 0) {
              let today = new Date().toISOString();
              let walkinFeedbackArr = {
                'walkin_id': walkin_id,
                'added_date': today,
                'dealer_id': subleadDealerId,
                'lead_car_id': subLeadId,
                'lead_id': mainLeadId
              }
              _.forEach(walkinDealerMsgIdArrCount, async (walk) => {
                walkinFeedbackArr['walkin_dealer_msg_id'] = walk;
                let resp = await walkinFeedbackDealerInfoModel.createOne(conditionWalkinFeedbackInsert);
              })
            }
          }
        } else {
          if (modal_walkinStatus == '4') {
            walkin_id = existwalkData['0']['walkin_id'];
            let updateWalkinFeedback = {
              'status': '2',
              'updated_at': new Date().toISOString()
            }
            let updateVal = await walkinFeedbackDealerInfoModel.updateOne({ 'walkin_id': walkin_id, 'status': '1' }, updateWalkinFeedback)
            let updateWalkinInfo = {
              'walkin_feedback': walkin_feedback,
              'updated_at': new Date().toISOString()
            }
            updateVal = await walkingInfoModel.updateOne(walkin_id, updateWalkinInfo);
            let walkinDealerMsgIdArr = [];
            if (walkin_dealer_info_ids) {
              walkinDealerMsgIdArr = walkin_dealer_info_ids;
            }

            if (walkinDealerMsgIdArr.length > 0) {
              let walkinFeedbackArr = {
                'walkin_id': walkin_id,
                'created_at': new Date().toISOString(),
                'dealer_id': subleadDealerId,
                'lead_car_id': subLeadId,
                'lead_id': mainLeadId
              };
              _.forEach(walkinDealerMsgIdArr, async (walk) => {

                walkinFeedbackArr['walkin_dealer_msg_id'] = walk;
                let resp = await walkinFeedbackDealerInfoModel.createOne(walkinFeedbackArr);

              })
            }
          }
        }
      }
    }
  // }

  //For PH Mail and notification
  /*
  if (conf.EMAIL_NOTIFICATIONS && conf.COUNTRY_CODE == 'ph') {
    try {
      let customerInfo = await customerModel.getCustomerDetailByLeadId(mainLeadId);      
      if ((customerInfo.length) && (walkinStatus === 3 && walkinSubstatus === 3 && customerInfo[0].otp_verified == 0) || (walkinStatus === 5 && walkinSubstatus === 9)) {
        let params = {
          custInfo: customerInfo[0],
          subleadInfo: subLeadData[0],
          walkinStatus: walkinStatus,
          walkinSubStatus: walkinSubstatus,
          lead_id: mainLeadId
        }
        await subLeadNotification.SendMailAndSMS(req, params);
      }
    } catch (error) {
      console.log('Send notification errors------------', error);
    }
  }
  */
  //END
  //SEND WHATSAPP NOTIFICATION FOR Walkin/Schedule
      if(walkinStatus == 5 && walkinSubstatus == 9 && WA_NOTIFICATION_ENABLE){
          let notificationParams = {lead_id: mainLeadId, leads_car_id:subLeadId, car_ids:[params['carId']]};
          // notificationParams['car_ids'] = slicedArray.map(el=>el.car_id);//[481174, 481170];
          let newReq = Object.assign({}, req);
          newReq['body'] = notificationParams;
          newReq['body']['template_name'] = 'walking_scheduled_notification';
          newReq['body']['template_id']   = '12';
          WhatsappController.sendWhatsappNotification(newReq,res);
      }
 
  //Get leads & customer detail only
  let leadData = await leadModel.getLeadDetailsInfo(mainLeadId);
  if (leadData.length) {
    leadData['0']['comment'] = walkin_feedback_comment;
    leadData['0']['mainLeadComment'] = mainLeadComment;
    responseData = leadData; //need to send response
  }
  ApiController.sendPaginationResponse(req, res, responseData, {});
}

exports.conversionPanelList = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body);
    const authUser = _.get(req, 'user');

    if (params.page_number) {
      page_number = params.page_number;
      delete req.body.page_number;
    } else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await conversionInfoModel.get(value, page_number);
    let conversionList = leadModel.encrypt(data[0]);
    if (conversionList && conversionList.length) {
      req.body['cb_function'] = true;
      //let carDetail = await carMakeController.listCarMakeModelVariant(req, res, next);
      //let dealerList = await dealerController.getDealerList(req, res, next);
      let cityList = await cityController.getCityStateList(req, res, next);
      
      conversionList.forEach((el, key) => {
        
        /*let getMake = '';
        let getModel = '';
        let getVariant = '';
        let getDealer = '';
        if (carDetail && carDetail['data'] && Object.keys(carDetail['data']).length) {
          getMake = _.find(carDetail['data']['make'], (make) => {
            return (make.id === el.make_id);
          });
          getModel = _.find(carDetail['data']['model'], (model) => {
            return (model.id === el.model_id);
          });
          getVariant = _.find(carDetail['data']['version'], (version) => {
            return (version.vn_id === el.variant_id);
          });
        }

        if (dealerList && dealerList['data']) {
          getDealer = _.find(dealerList['data'], (dealer) => {
            return (dealer.id === el.dealer_id);
          });
        } */

        if (cityList && cityList['city']) {
          getCity = _.find(cityList['city'], (city) => {
            return (city.id === el.dealer_id);
          });
        }

        conversionList[key]['cluster'] = (el.cluster_name) ? el.cluster_name: 'Unassigned';
        conversionList[key]['car_make'] = el.make_name;
        conversionList[key]['car_model'] = el.model_name;
        conversionList[key]['car_variant'] = el.variant_name;
        conversionList[key]['dealer'] = el.dealer;
        conversionList[key]['city_name'] = '';
        conversionList[key]['conversion_date'] = el.added_on;
        conversionList[key]['approve_denied_by'] = authUser.name;
        conversionList[key]['cnfm_status_sec'] = el.cnfm_status_sec;
      });
    } 
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;

    if (params.importXls) {
      return conversionList;
    } else {
      ApiController.sendPaginationResponse(req, res, conversionList, pagination);
    }

  } catch (error) {
    next(error);
  }

}

exports.updateConversion = async (req, res, next) => {

  try {
    let params = _.cloneDeep(req.body);
    const authUser = _.get(req, 'user');

    let checkConversion = await conversionInfoModel.get({ 'id': params.id, 'cnfm_status': ['1', '2'], 'status': '1' });

    let updateFields = { "registration_no": params.registration_no, "cnfm_comment": params.cnfm_comment, "cnfm_status": +params.cnfm_status['value'], updated_date: dateFormat("yyyy-mm-dd"), updated_by: authUser['user_id'] };
    let updateConversion = '';
    if (!checkConversion || authUser['role_id'] === ADMIN_ROLE_ID) {
      updateConversion = await conversionInfoModel.updateOne(params.id, updateFields);
    }
    ApiController.sendResponse(req, res, 200, res.__("conversion_updated_successfully"));
  } catch (err) {
    next(err);
  }

}

exports.copyCarDetails = async (req, res, next) => {
  let reqBody = _.cloneDeep(req.body);
  let authUser = _.get(req, 'user')
  let userId = authUser['user_id'];
  let params = reqBody.used_car_ids;
  let tempLead = params.temp_lead_id;
  let car_id = params[0].car_id;
  let dealer_id = params[0].dealer_id;
  let lead_id = (params[0].lead_id) ? params[0].lead_id : '';
  let message = '';
  let carCond = {
    "user_id": dealer_id,
    "stock_used_car_id": car_id
  }
  let result = await InventoryService.getCarDetails(req, carCond);
  let carDetails = (result & result.data) ? result.data : {}
  if (carDetails && carDetails.id) {
    message = 'We have ' + carDetails.make_year + ' ' + carDetails.make + ' ' + carDetails.model + ' ' + carDetails.carversion + ' (' + carDetails.fuel_type + '), ' + carDetails.km_driven + ' kms, ' + carDetails.transmission + ' transmission @ Rs ' + carDetails.display_price + '.';
    message += '<br><br>To checkout car photos and view seller details, click ' + carDetails.web_url + '';
    let updateArrayFields = {
      'lead_id': (tempLead != '1') ? lead_id : '',
      'temp_lead_id': (tempLead != '1') ? lead_id : '',
      'car_id': carDetails.id,
      'customer_id': (tempLead != '1') ? params.custmer_id : '',
      'temp_customer_id': (tempLead == '1') ? params.custmer_id : '',
      'agent_id': userId,
      'message': message,
      'created_at': new Date().toISOString()
    };
    recomendCarDetailsSharedModel.createOne(updateArrayFields)
      .then(response => {
        if (response) {
          ApiController.sendResponse(req, res, 200, 'car_details_updated_successfully');
        } else {
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        next(error);
      })

    // }
  }
  let resp = {
    'status': 'false',
    'message': message
  }
  ApiController.sendPaginationResponse(req, res, resp, {});
}

exports.saveRecomendedCar = async (req, res, next) => {
  let authUser = _.get(req, 'user');
  let params = _.cloneDeep(req.body);
  let usedCarIds = params.used_car_ids;

  if (params) {

    if (usedCarIds) {
       
      let lead_id = (params['lead_id']) ? params['lead_id'] : '';
      for (let val of usedCarIds) {
        let isCarExist = await leadCarModel.isLeadCarExist(val.lead_id, val.car_id)
        if (isCarExist.length == 0) {
          let carCond = {
            "user_id": val.dealer_id,
            "stock_used_car_id": val.car_id
          }
          let result = await InventoryService.getCarDetails(req, carCond);
          let carDetails = (result && result.data) ? result.data : {};
          let makeId = carDetails.make_id
          let parent_model_id = carDetails.model_id;
          let parent_model_name = carDetails.model;
          
          let versionId = carDetails.version_id;
          let insertArr = {
            'lead_id': val.lead_id,
            'car_id': carDetails.id,
            'dealer_id': carDetails.dealer_id,
            'make_id': makeId,
            'model_id': parent_model_id,
            'variant_id': versionId,
            'make_name': carDetails.make,
            'model_name': parent_model_name,
            'variant_name': carDetails.carversion,
            'distance': carDetails.km_driven,
            'price_to': carDetails.car_price,
            'added_by': authUser['user_id'],
            'is_new_assigned': 1,
            'created_at': new Date().toISOString()
          };

          if (carDetails['user_type'] == 'D' || carDetails['user_type'] == 'P') {
            insertArr['status_id'] = '3';
            insertArr['sub_status_id'] = '3';
            insertArr['calling_status_id'] = '7';
          }
          let slot_num = 0;
          // if((positionArr[carDetails.id])) {
          //     slot_num = positionArr[carDetails.id];
          // }
          insertArr['slot_num'] = slot_num;
          let res = await leadCarModel.createOne(insertArr);

          //SAVE CONVERSION
          let conversationData = {};
    
          conversationData = {
            'lead_id': val.lead_id,
            'leads_cars_id': res['id'],
            'dealer_id': carDetails.dealer_id || 0,
            'status_id': 3,
            'sub_status_id': 3,
            'calling_status_id': 0,
            'comment': 'Car moved from recommended to assigned list',
            'latest': '1',
            'added_on': dateFormat("yyyy-mm-dd"),
            'added_by': authUser['user_id']
          };
          let addConversion = await conversionModel.createOne(conversationData);

        }
      }

      //SEND WHATSAPP NOTIFICATION
      if(WA_NOTIFICATION_ENABLE){
          let slicedArray = usedCarIds.slice(0,2);
          let notificationParams = {lead_id: slicedArray[0]['lead_id'], car_ids:[]};
          notificationParams['car_ids']       = slicedArray.map(el=>el.car_id);//[481174];
          notificationParams['template_name'] = 'cross_sell_usedcar';
          notificationParams['template_id']   = '12';

          let newReq = Object.assign({}, req);
          newReq['body'] = notificationParams;

          WhatsappController.sendWhatsappNotification(newReq,res);
      }
      
    }
  }
  ApiController.sendResponse(req, res, 200, res.__("success"));
}

saveSearching = async (srhData) => {
  let authUser = _.get(req, 'user');
  let srh_budget_min = (srhData['srh_budget_min']) ? srhData['srh_budget_min'] : '';
  let srh_budget_max = (srhData['srh_budget_max']) ? srhData['srh_budget_max'] : '';
  let srh_min_year = (srhData['srh_min_year']) ? srhData['srh_min_year'] : '';
  let srh_max_year = (srhData['srh_max_year']) ? srhData['srh_max_year'] : '';
  let srh_city = (srhData['srh_city']) ? srhData['srh_city'] : '';
  let srh_max_km = (srhData['srh_max_km']) ? srhData['srh_max_km'] : '';
  let srh_owner = (srhData['srh_owner']) ? srhData['srh_owner'].toString() : '';
  let srh_dealer = (srhData['srh_dealer']) ? srhData['srh_dealer'].toString() : '';
  let srh_trust_mark_cert = (srhData['srh_trust_mark_cert']) ? srhData['srh_trust_mark_cert'] : '';
  let srh_results_pic = (srhData['srh_results_pic']) ? srhData['srh_results_pic'] : '';
  let srh_filter_dealer = (srhData['srh_filter_dealer']) ? srhData['srh_filter_dealer'] : '';
  let srh_paid_individual = (srhData['srh_paid_individual']) ? srhData['srh_paid_individual'] : '';
  let srh_free_individual = (srhData['srh_free_individual']) ? srhData['srh_free_individual'] : '';
  let makeId = (srhData['srh_make']) ? srhData['srh_make'].toString() : '';
  let modelId = (srhData['srh_model']) ? srhData['srh_model'].toString() : '';
  let fuelType = (srhData['srh_fuel']) ? srhData['srh_fuel'].toString() : '';
  let colorData = (srhData['srh_color']) ? srhData['srh_color'].toString() : '';
  let bodyType = (srhData['srh_body_type']) ? srhData['srh_body_type'].toString() : '';
  let transmission = (srhData['srh_transmission']) ? srhData['srh_transmission'].toString() : '';
  let purpose = (srhData['purpose']) ? srhData['purpose'] : '';
  let req_body_type = (srhData['srh_body_type']) ? srhData['srh_body_type'] : '';
  let custmer_location_id = (srhData['locationId']) ? srhData['locationId'] : '';
  let req_owner = (srhData['srh_owner']) ? srhData['srh_owner'].toString() : '';
  let currentDate = new Date().toISOString();
  if (srh_city && srh_city != '') {
    let srh_city_arr = srh_city;
    if (srh_city_arr.length) {
      _.forEach(srh_city_arr, (val, index) => {
        if (val == CLUSTER_DELHI_NCR) {
          srh_city_arr[index] = DELHI_NCR_CITY_IDS;
        } else {
          srh_city_arr[index] = "'" + srh_city_arr[$j] + "'";
        }
      })

    }
    $cityIds = srh_city_arr.toString();
  }

  let budget = -1;

  if (srh_budget_max != '-1') {
    budget = srh_budget_max;
  } else if (srh_budget_min != '-1') {
    budget = srh_budget_min;
  }

  let insertReqAgentArr = {
    'lead_id': srhData.lead_id,
    'customer_id': params.custmer_id,
    'budget': srh_budget_max,
    'model_ids': modelId,
    'min_year': srh_min_year,
    'max_year': srh_max_year,
    'max_km': srh_max_km,
    'fuel': fuelType,
    'owner': req_owner,
    'color': colorData,
    'transmission': transmission,
    'purpose': purpose,
    'req_body_type': req_body_type,
    'added_date': currentDate
  };
  let insertAgent = await customerReqAgentModel.saveCustomerReCommondation(insertReqAgentArr, authUser['user_id']);

  if (insertAgent) {

    let insertReqArr = {
      'lead_id': srhData.lead_id,
      'customer_id': srhData.custmer_id,
      'model_ids': modelId,
      'min_year': srh_min_year,
      'max_year': srh_max_year,
      'max_km': srh_max_km,
      'fuel': fuelType,
      'owner': srh_owner,
      'color': colorData,
      'transmission': transmission,
      'added_date': currentDate
    };

    if (budget != '-1') {
      insertReqArr['budget'] = budget;
    }
    let insert_condition = await customerRequirementModel.createOne(insertReqArr);

    let updateReqArr = {
      'model_ids': modelId,
      'min_year': srh_min_year,
      'max_year': srh_max_year,
      'max_km': srh_max_km,
      'fuel': fuelType,
      'owner': srh_owner,
      'color': colorData,
      'transmission': transmission,
      'updated_on': currentDate,
      'updated_by': authUser['user_id']
    };
    if (budget != '-1') {
      updateReqArr['budget'] = budget;
    }
    if (custmer_location_id && custmer_location_id == '-1') {
      updateReqArr['custmer_location_name_other'] = srhData['custmer_location_name_other'];
      updateReqArr['other_location_lat'] = srhData['locOtherLat'];
      updateReqArr['other_location_lng'] = srhData['locOtherLng'];
      updateReqArr['customer_location_id'] = -1;
    }
    let conditionUpdate = await customerModel.updateOne(srhData['customer_id'], conditionUpdate)
  }

  /*Save the recommendation search*/
  let insertArr = {
    'lead_id': srhData['lead_id'],
    'custmer_id': srhData['custmer_id'],
    'min_price': srh_budget_min,
    'max_price': srh_budget_max,
    'make_id': makeId,
    'model_id': modelId,
    'min_year': srh_min_year,
    'max_year': srh_max_year,
    'max_km': srh_max_km,
    'city': cityIds,
    'dealer_id': srh_dealer,
    'fuel_type': fuelType,
    'owner': srh_owner,
    'color': colorData,
    'body_type': bodyType,
    'transmission': transmission,
    'filter_certified': srh_trust_mark_cert,
    'filter_picture': srh_results_pic,
    'filter_dealer': srh_filter_dealer,
    'filter_paid_individual': srh_paid_individual,
    'filter_free_individual': srh_free_individual,
    'user_id': authUser['user_id'],
    'created_date': currentDate
  };
  let insert = await recomendCarSearchLogModel.createOne(insertArr)
  //uncomment this line on live server:: start
  //$status = $this->mongodb->insertData(UBLMS_RECOMMEND_CAR_SEARCH_LOG, $insertArr);
  //eoc
}


/**
 * Import Conversion Panel conversion
 */
exports.importConversion = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body); //Request params
    const authUser = _.get(req, 'user');

    let requiredformat = ['Cluster', 'Customer Name', 'Customer Mobile', 'Car Detail', 'Reg No', 'City', 'Organization', 'GCD Code', 'Lead ID', 'Conversion Date', 'Confirmation Status', 'Denied Reason', 'Comments', 'Approved/Denied By', 'TAT'];

    req.body['importXls'] = true;
    let getConversioList = await _this.conversionPanelList(req, res, next);

    let excelData = [];
    let saveExportedData = {};

    await getConversioList.forEach(async (el, key) => {

      let addedOn = new Date(el.added_on).getTime();
      let updatedOn = new Date(el.updated_date).getTime();
      let tat = await CommonHelper.timeDifference(updatedOn, addedOn);

      let confirmationStatus = _.find(siteSetting.conversionPanelStatus, (st) => {
        return (typeof el.cnfm_status === 'object' ? st.value === (el.cnfm_status) : st.value === (el.cnfm_status).toString());
      });

      let deniedReason = _.find(siteSetting.conversionDeclineReason, (dr) => {
        return (dr.value === (el.cnfm_status).toString());
      });
      excelData[key] = ["Unassigned", el.name, el.customer_mobile, el.car_make + ' ' + el.car_model, el.registration_no, el.city_name, el.dealer && el.dealer['organization'], el.dealer && el.dealer['gcd_code'], el.leadId, dateFormat(el.conversion_date, "dd-mm-yyyy, h:MM:ss TT"), (confirmationStatus && confirmationStatus['label'] || ''), (deniedReason && deniedReason['label'] || ''), el.cnfm_comment, el.approve_denied_by, (tat.year || tat.month || tat.week || tat.day || '')];
    });

    if ((excelData).length) {

      const fileName = 'Conversion_pannel_' + dateFormat('dd_mm_yyyy_h_MM_ss_TT') + '.csv';
      const paths = './public/conversion-files/';

      if (!fs.existsSync(paths)) {
        fs.mkdirSync(paths);
      }

      const csvWriter = await createCsvWriter({
        header: requiredformat,
        path: paths + fileName
      });
      await csvWriter.writeRecords(excelData);

      //UPLOAD FILE IN S3 BUCKET
      // let awsUpload = await AwsConfig.awsFileUpload(paths+fileName, fileName);

      //SAVE LOGS IN MONGO
      saveExportedData['downloaded_by'] = authUser['user_id'];
      saveExportedData['download_filename'] = fileName;
      saveExportedData['added_on'] = dateFormat("yyyy-mm-dd hh:MM:ss");
      saveExportedData['records_write'] = getConversioList.length;

      await ConversionPanelExportLogModel.create(saveExportedData);

      ApiController.sendResponse(req, res, 200, res.__("conversion_updated_successfully"));

    }

  } catch (error) {

  }
}

exports.saveMainLead = async (req, res, next) => {
  let responseData = '';
  let params = _.cloneDeep(req.body); 
  let dealerSentLeadCarIdArr = [], dealerOtpSentOrgLeadCarIdArr = [];
  let leadId = params['leadId'];
  let inStatus = params['inStatus'];
  let inSubStatus = params['inSubStatus'];
  if (inSubStatus == PURCHASED_USED_CAR) {
    inSubStatus = await closedLeadPurchaseModel.checkClosedLeadSubStatus(leadId);
    inSubStatus = (inSubStatus) ? inSubStatus : PURCHASED_USED_CAR;
  }
  let inCallstatus = (params['inCallstatus'] ? params['inCallstatus'] : 0);
  let customer_email = params['customer_email'];
  let customer_name = params['customer_name'];
  let customer_mobile = params['customer_mobile'];
  let customer_city = params['customer_city'];
  let source_id = params['source_id'];
  let followup_reason = params['followup_reason'];
  let blocked_reason = params['blocked_reason'];
  let customerId = params['customerId'];
  let agent_rating = params['agent_rating'];
  let budget = params['budget'];
  let reqMatchLeadScore = params['reqMatchLeadScore'];
  let isExchange = params['valExchange'];
  let isEmail = params['valIsEmail'];
  let isSms = params['valIsSms'];
  let alternate_email = params['alternate_email'];
  let alternate_mobile = params['alternate_mobile'];
  let whatsapp_mobile = params['whatsapp_mobile'];
  let outstation = (params['outstation'] == 'true') ? '1' : '0';
  if ((params['whatsappChecked']) && params['whatsappChecked'] == 'true') {
    whatsappNumber = customer_mobile;
  }
  else if (whatsapp_mobile) {
    whatsappNumber = whatsapp_mobile;
  } else {
    whatsappNumber = customer_mobile;
  }

  let leadCommentText = params['leadCommentText'];
  
  let prevCallstatus = params['prevCallstatus'];
  let prevSubStatus = params['prevSubStatus'];
  let prevStatus = params['prevStatus'];
   
  if (inStatus == 3 || inStatus == 5) {
    if ((inStatus != prevStatus) || (inSubStatus != prevSubStatus)) {      
      let isValidReq = await customerReqAgentModel.isValidCustomerReq(customerId, leadId);    
    
      if (isValidReq == '2') {
        responseData = 'invalidReq';
        ApiController.sendPaginationResponse(req, res, responseData, {});
        return;
      } else if (isValidReq == '3') {
        responseData = 'emptyInvalidReq';
        ApiController.sendPaginationResponse(req, res, responseData, {});
        return;
      }
    }
  }
  let isMobileVerified = ''
  if (customer_mobile && (params['tempLead'])) {
    isMobileVerified = await customerModel.isCustomerMobileVerified(customer_mobile);
  }
  let walkinCount = await walkingInfoModel.getWalkinCount(leadId);  
  if (walkinCount == 0) {
    walkinLeadScore = 0;
  } else if (walkinCount == 1) {
    walkinLeadScore = 1;
  } else if (walkinCount == 2) {
    walkinLeadScore = 0.5;
  } else {
    walkinLeadScore = 0.25;
  }
  let offline_lead_score = Math.round(((agent_rating / 3) + (walkinLeadScore / 3) + (reqMatchLeadScore / 3)), 2);
  offline_lead_score = offline_lead_score ? offline_lead_score : 0 // offline lead score set to 0 avoid error
  let call_later_date = ''
  if (params['call_status_date'] && params['call_status_date'] != '') {
    call_later_date = moment(params['call_status_date']).toISOString();
  } else {
    call_later_date = null; //// as format of 00000 is invalid
  }
  //$call_later_time       = $_REQUEST['call_status_time'];
  let ratingValue = params['ratingValue'];
  let valFinance = params['valFinance'];
  let valueFin = '', is_premium = ''
  if (valFinance == 1) {
    valueFin = 'yes';
  } else {
    valueFin = 'no';
  }
  is_premium = params['valPremium'];
  const authUser = _.get(req, 'user');
  let addedBy = authUser['user_id'];
  let addedOn = new Date().toISOString();
  let dueDate = "";
  if (params['dueDate']) {
    dueDate = moment(params['dueDate']).toISOString();
  } else {
    dueDate = "";
  }

  let conversionObj = {
    'lead_id': leadId,
    'status_id': inStatus,
    'dealer_id': params['main_lead_dealer_id'] || 0,
    'sub_status_id': inSubStatus || 0,
    'calling_status_id': inCallstatus || 0,
    'call_later_date': call_later_date,
    'rating_value': ratingValue,
    'comment': leadCommentText,
    'followup_reason': followup_reason,
    'added_on': addedOn,
    'added_by': addedBy
  };
  if(dueDate){
    conversionObj['due_date'] = dueDate;
  } 
  let insertVal = await conversionModel.createOne(conversionObj);
  
  if (insertVal && insertVal.dataValues && insertVal.dataValues.id) {
    /*Update Main lead Status*/
    let updatedOn = new Date().toISOString();
    let updatedBy = authUser['user_id'];
    let arrSubLeadStatus = await leadCarModel.getSubLeadHigherStatus(leadId);
    if (arrSubLeadStatus && arrSubLeadStatus.length && (arrSubLeadStatus[0]['status_id'])) {
      if (arrSubLeadStatus[0]['status_id'] > inStatus) {
        inStatus = arrSubLeadStatus[0]['status_id'];
        inSubStatus = arrSubLeadStatus[0]['sub_status_id'];
        inCallstatus = await leadModel.getNYFCallStatus(arrSubLeadStatus[0]['sub_status_id']);
        systemComment = "Agent didn't save this lead properly. System has updated this lead.";
        let convObj = {
          'lead_id': leadId,
          'status_id': inStatus,
          'sub_status_id': inSubStatus,
          'calling_status_id': inCallstatus,
          'call_later_date': call_later_date,
          'due_date': dueDate,
          'rating_value': ratingValue,
          'comment': systemComment,
          'followup_reason': followup_reason,
          'added_on': addedOn,
          'added_by': '-1'
        }
      }
    }
    /*Update Main lead Status*/
    //update customer
    if (inStatus == 7 && inSubStatus == 21) {
      let custData = {
        'status': 0,
        'updated_at': new Date().toISOString()
      }
      let updateVal = await customerModel.updateOne(customerId, custData);
      let closedBlockedObj = {
        'lead_id': leadId,
        'customer_id': customerId,
        'blocked_reason': blocked_reason,
        'lead_comment': leadCommentText,
        'user_id': updatedBy,
        'created_date': updatedOn
      }
      insertVal = await closedBlockedLeadModel.createOne(closedBlockedObj);
    } else {
      blocked_reason = '0'; /// blocked reason set to null to avoid error
    }
    //update the main ublms_lead table:: start

    let mainLeadUpdateArray = {
      'status_id': inStatus,
      'sub_status_id': inSubStatus,
      'calling_status_id': inCallstatus,
      'call_later_date': call_later_date,
      'is_finance_req': valueFin,
      'is_premium': is_premium,
      'due_date': dueDate || null,
      'ratings': ratingValue,
      'followup_reason': (inSubStatus == '5') ? followup_reason : '',
      'blocked_reason': blocked_reason,
      'agent_rating': agent_rating,
      'budget': budget || 0,
      'lead_offline_score': offline_lead_score,
      'updated_at': updatedOn,
      'updated_by': updatedBy,
      'is_main_lead_save': '1',
      'isExchange': isExchange,
      'isEmail': isEmail,
      'isSms': isSms,
      'is_outstation': outstation
    };
    if (inStatus == 7 || (inStatus == 6 && inSubStatus == 12)) {
      mainLeadUpdateArray['leads_status'] = '0';
    } else {
      mainLeadUpdateArray['leads_status'] = '1';
    }
    if (inStatus == params['prevStatus'] && inSubStatus == params['prevSubStatus']) {
      mainLeadUpdateArray['num_attempt'] = params['num_attempt'] + 1;
    } else {
      mainLeadUpdateArray['num_attempt'] = 0;
    } 

    updateVal = await leadModel.updateOne(leadId, mainLeadUpdateArray);
    let arrUpdateSubLeadSubStatus = ['4', '25', '29', '32', '33'];
    if (arrUpdateSubLeadSubStatus.indexOf(inSubStatus) != -1) {
      let leadCarObj = {
        'status_id': inStatus,
        'sub_status_id': inSubStatus,
        'calling_status_id': inCallstatus
      }
      await leadCarModel.updateSubLeadStatusInfo(leadId, leadCarObj)
    }
    
    //save customer info
    let arrVal = {};
    ((customer_email)) ? arrVal['customer_email'] = customer_email : '';
    arrVal['alternate_email'] = alternate_email;
    (customer_mobile) ? arrVal['customer_mobile'] = customer_mobile : '';
    arrVal['alternate_mobile'] = alternate_mobile;
    arrVal['updated_at'] = new Date().toISOString();
    arrVal['updated_by'] = authUser['user_id'];
    arrVal['whatsapp_mobile'] = ((whatsappNumber) && (whatsappNumber) && (whatsappNumber != 'undefined')) ? whatsappNumber : '';
    let updateCustVal = await customerModel.updateOne(customerId, arrVal);
    if (params['leadCarId'] && ([1, 2, 7].indexOf(inStatus) == -1) && inSubStatus != '12') {
      let arrCarIds = [], dealerSentLeadCarIdArr = [], dealerNotSentLeadCarIdArr = [], dealerSentOrgLeadCarIdArr = '', dealerOtpSentOrgLeadCarIdArr = '', dealerSentConvOrgLeadCarIdArr = "";
      //let resultArr = $this->getLeadCarDealerResult($_REQUEST['leadCarId']);
      let leadResultArr = await leadCarModel.getLeadDetails(params.leadCarId);
      if (leadResultArr && leadResultArr.length) {
        for (leadCar of leadResultArr) {
          leadCar['carDetails'] = {}, leadCar['dealerDetails'] = {};
          let organicLeadFlag = 0, only_update_flag = '0';
          let carCond = {
            "user_id": leadCar.dealer_id,
            "stock_used_car_id": leadCar.car_id
          }
          let result = await InventoryService.getCarDetails(req, carCond);
          let carDetails = (result && result.data) ? result.data : {};
          if (carDetails)
            leadCar.carDetails = carDetails
          if (leadCar.carDetails && leadCar.carDetails.dealer_id) {

            let fetchDealerDetail = await LeadService.getDealerDetailsByDealerId(req, {dealer_id:leadCar.carDetails.dealer_id});
            leadCar.dealerDetails = {};
            if(fetchDealerDetail.status === 200){
              leadCar.dealerDetails = fetchDealerDetail.data;
            }

          }
          if ((leadCar['otp_verified'] != '1') && (leadCar['added_by'] == '0') && (leadCar['carDetails']['user_type'] == 'D') && moment(leadCar['created_at']).toISOString() >= moment(ADDED_DATE_LEAD_OTP).toISOString()) {
            only_update_flag = '0';
            let organicLeadFlag = 1;
            if (VALIDPLATFORM.indexOf(leadCar['lead_platform']) != -1) {
              only_update_flag = '1';
            }
            let api_data = {
              'lead_dealer_id': leadCar['dealer_id'],
              'api_data': JSON.stringify({ 'otp_verified': '1', 'only_update_flag': only_update_flag, 'gaadi_verified': '0', 'customer_name': params['customer_name'], 'customer_email': params['customer_email'], 'customer_mobile': params['customer_mobile'], 'car_id': leadCar['car_id'], 'is_outstation': leadCar['is_outstation'], 'lead_comment': leadCommentText }),
              'api_type': 'verify_buyer_lead',
              'flag': '0',
              'mobile': params['customer_mobile'],
              'car_id': leadCar['car_id'],
              'lead_id': leadId,
              'created_at': new Date().toISOString(),
              'budget':0
            };   
            await asyncLeadModel.createOne(api_data)
            dealerOtpSentOrgLeadCarIdArr = leadCar['leads_cars_id'];
          }
          let isDealerSentQueryString = (params['isDialerSent']) ? params['isDialerSent'].split('&') : [];
          let dealerSentCarsArray = {};
          isDealerSentQueryString.forEach(el => {
            if(el){
              let splitString =  el.split('=');
              dealerSentCarsArray[splitString[0]] = splitString[1];
            }
          })

          if ((dealerSentCarsArray['is_dealer_sent_' + leadCar['leads_cars_id']]) && dealerSentCarsArray['is_dealer_sent_' + leadCar['leads_cars_id']] == 1) {
            let api_data = {};
            if (leadCar['added_by'] == '0' && leadCar['status_id'] >= '3') {
              only_update_flag = '0';
              if (organicLeadFlag != '1' && leadCar['distance'] > CAR_DISTANCE_CUSTOMER) {
                only_update_flag = '0';
              } else if ((VALIDPLATFORM.indexOf(leadCar['lead_platform']) != -1 || leadCar['otp_verified'] == '1')) {
                only_update_flag = '1';
              } else if (organicLeadFlag == '1') {
                only_update_flag = '1';
              }

              api_data = {
                'lead_dealer_id': leadCar['dealer_id'],
                'api_data': JSON.stringify({ 'gaadi_verified': '1', 'only_update_flag': only_update_flag, 'verified_only': '1', 'customer_name': params['customer_name'], 'customer_email': params['customer_email'], 'customer_mobile': params['customer_mobile'], 'car_id': leadCar['car_id'], 'is_outstation': leadCar['is_outstation'], 'lead_comment': leadCommentText }),
                'api_type': 'verify_buyer_lead',
                'flag': '0',
                'mobile': params['customer_mobile'],
                'car_id': leadCar['car_id'],
                'lead_id': leadId,
                'created_at': new Date().toISOString()
              };
            } else if (leadCar['added_by'] > '0') {
              api_data = {
                'lead_dealer_id': leadCar['dealer_id'],
                'api_data': JSON.stringify({ 'gaadi_verified': '1', 'customer_name': params['customer_name'], 'customer_email': params['customer_email'], 'customer_mobile': params['customer_mobile'], 'car_id': leadCar['car_id'], 'is_outstation': leadCar['is_outstation'], 'lead_comment': leadCommentText }),
                'api_type': 'verify_buyer_lead',
                'flag': '0',
                'mobile': params['customer_mobile'],
                'car_id': leadCar['car_id'],
                'lead_id': leadId,
                'created_at': new Date().toISOString()
              };

            }
            // if ((api_data)) {
            //   await asyncLeadModel.createOne(api_data)
            // }
            //boost dealer
            //$arrCarIds = array($val['car_id']);
            arrCarIds = [leadCar['dealer_id']]

            if (leadCar['status_id'] >= '3') {
              dealerSentLeadCarIdArr.push(leadCar['leads_cars_id']);
            }

          } else {
            dealerNotSentLeadCarIdArr.push(leadCar['leads_cars_id']);
          }
        }

        if ((dealerOtpSentOrgLeadCarIdArr)) {
          let lcData = {
            'otp_verified': '1',
            'updated_at': new Date().toISOString()
          };
          await leadCarModel.updateOne(dealerOtpSentOrgLeadCarIdArr, lcData)
        }
        if (dealerSentLeadCarIdArr.length) {
          dealerSentLeadCarIdArr.map(async elm=>{
            let lcData = {
              'is_dealer_sent': '1',
              'otp_verified': '1',
              'updated_at': new Date().toISOString()
            };
            let updateStatus = await leadCarModel.updateOne(elm, lcData)
          });
        }
        //without dealer sent
        if (dealerNotSentLeadCarIdArr.length) {
          dealerNotSentLeadCarIdArr.map(async elmn=>{
            let lcData = {
              'is_dealer_sent': '2',
              'otp_verified': '1',
              'updated_at': new Date().toISOString()
            };
            let updateStatus = await leadCarModel.updateOne(elmn, lcData)
          });
        }

        // if (arrCarIds && arrCarIds.length) {
        //   for (let val of arrCarIds) {
        //     await updateFTDofLeads(val);
        //   }
        // }

      }
    }
    if (inStatus != '7' && inSubStatus != '12') {
      this.managecarleaddetail(req);
    }
    //eoc    
    if (updateVal && updateVal.length) {
      //code to update the leads activity:: start
      // $activityStatus = $this->countActivityStatus($leadId, $dueDate);
      let recArray = {
        'lead_id': leadId,
        'status': inStatus,
        'sub_status': inSubStatus,
        'calling_status': inCallstatus,
        'isVerifiedDue': 2,
        'due_date': dueDate,
        'conversation_added_on': addedOn
      }
      if ((inStatus == 3) && [3, 5].indexOf(inSubStatus) != -1) {
        recArray['isVerifiedDue'] = 1;
      }
      if ((inStatus == 5) && (inSubStatus == 8)) {
        recArray['isWalkinReminder'] = 1;
      }
      if ((inStatus == 5) && (inSubStatus == 9)) {
        recArray['IsWalkinFeedback'] = 1;
      }
      if ((inStatus == 6) && [10, 11].indexOf(inSubStatus) != -1) {
        recArray['isPurchase'] = 1;
      }
      recArray['prev_status'] = params['prevStatus'];
      recArray['prev_sub_status'] = params['prevSubStatus'];
      recArray['prev_calling_status'] = params['prevCallstatus'];
      //check for L1 status:: start
      let l1_status = 'no';
      if ([1, 2].indexOf(inStatus) != -1) {
        l1_status = 'yes';
      } else if ((inStatus == 3) && (inSubStatus == 4)) {
        l1_status = 'yes';
      }
      //eoc
      //check for L2 Status:: start
      let l2_status = 'no';
      if (inStatus == 6) {
        l2_status = 'yes';
      } else if ((inStatus == 3) && inSubStatus == 5) {
        l2_status = 'yes';
      }
      // console.log("auth user",authUser);
      //eoc
      //if status is verified-interested::then
      if ((inStatus == 3) && (inSubStatus == 3)) {
        if (authUser['role_id'] == L1_USER_ROLE_ID) {
          l1_status = 'yes';
          //  *********************** TO DO********************************
          let l2UserId = await getL2UserId(leadId, customer_city);
          if (l2UserId != 'no') {
            recArray['l2_user_id'] = l2UserId;
          }
        }
        if (authUser['role_id'] == L2_USER_ROLE_ID) {
          l2_status = 'yes';
        }
      }
      //if status is walkin:: then
      if (inStatus == 5) {
        if (authUser['role_id'] == L1_USER_ROLE_ID) {
          l1_status = 'yes';
          //  *********************** TO DO********************************
          l2UserId = await getL2UserId(leadId, customer_city);
          if (l2UserId != 'no') {
            recArray['l2_user_id'] = l2UserId;
          }
        }
        if (authUser['role_id'] == L2_USER_ROLE_ID) {
          l2_status = 'yes';
        }
      }
      //if status is closed then check what was prev status to know lead is in l1 status or l2 status
      if (inStatus == 7) {
        if ([1, 2].indexOf(recArray['prev_status']) != -1) {
          l1_status = 'yes';
        } else if ((recArray['prev_status'] == 3) && (recArray['prev_sub_status'] == 4)) {
          l1_status = 'yes';
        } else if (recArray['prev_status'] == 6) {
          l2_status = 'yes';
        } else if ((recArray['prev_status'] == 3) && (recArray['prev_sub_status'] == 5)) {
          l2_status = 'yes';
        }
      }
      if (l1_status == 'yes') {
        recArray['added_by'] = authUser['user_id'];
      }
      if (l2_status == 'yes') {
        recArray['l2_user_id'] = authUser['user_id'];
      }
      //eoc
      //get the walkin-detail if the status is of walkin-type:: start
      if ((inStatus == 5) && [8, 9].indexOf(inSubStatus) != -1) {
        let walkinData = await walkingInfoModel.LastWalkinDeatils(leadId);
        if (walkinData && walkinData.length > 0) {
          recArray['walkin_lead_car_id'] = walkinData[0]['leads_cars_id'];
          recArray['walkin_status'] = walkinData[0]['walkin_status'];
          recArray['walkin_date'] = walkinData[0]['walkin_datetime'];
          if ((walkinData.length > 1)) {
            recArray['prev_walkin_lead_car_id'] = walkinData[1]['leads_cars_id'];
            recArray['prev_walkin_date'] = walkinData[1]['walkin_datetime'];
          }
        }
      }      
      //params.tempLead = '1';   
  
      if (customer_mobile && params['tempLead']) {
        params['user_id'] = authUser['user_id'];        
        let leadRes = await makeNewLead(leadId, customer_mobile, customerId, isMobileVerified, params);
        responseData = leadRes;
        ApiController.sendPaginationResponse(req, res, responseData, {});
        return;
      }
     
      responseData = "yes";  //need to send response as yes to static added 
      if (responseData == 'yes') {
        sendLeadStatusToDC(req, params);
      }
      
      ApiController.sendPaginationResponse(req, res, responseData, {});
      // return;
    } else {
      responseData = "no"; //need to send response
    }
  } else {
    responseData = "no";
  }
  ApiController.sendPaginationResponse(req, res, responseData, {});
  return;

}

makeNewLead = async (leadId, customerMobile, customerId, isMobileVerified, params) => {
  let customer_city = params['customer_city'];
  let customer_email = params['customer_email'];
  let alternate_email = params['alternate_email'];
  let customer_mobile = params['customer_mobile'];
  let alternate_mobile = params['alternate_mobile'];
  let customer_name = params['customer_name'];
  let whatsapp_mobile = params['whatsapp_mobile'];
  let language = params['language'];
  let user_id = params.user_id;
  let inStatus = params['inStatus'];
  let arrCustomer = await customerModel.getCustomerInfo(customerMobile);

  let lead_id = leadId, customer_id = customerId;
  if ((arrCustomer.length) > 0) {
    customer_id = arrCustomer['0']['customer_id'];
    lead_id = arrCustomer['0']['lead_id'];

    let orig_status_id = arrCustomer['0']['status_id'];

    let arrCustomerInfo = await customerModel.getOne({ 'id': customerId });
    let customerUpdateSqlPart = '';
    if (!customer_city) {
      delete arrCustomerInfo['customer_city_id'];
      delete arrCustomerInfo['customer_city_name'];
    }
    if (!customer_email) {
      delete arrCustomerInfo['customer_email'];
    }
    if (!alternate_email) {
      delete arrCustomerInfo['alternate_email'];
    }
    if (!customer_mobile) {
      delete arrCustomerInfo['customer_mobile'];
    }
    if (!alternate_mobile) {
      delete arrCustomerInfo['alternate_mobile'];
    }
    if (!customer_name) {
      delete arrCustomerInfo['customer_name'];
    }
    if (!(language)) {
      delete arrCustomerInfo['language'];
    }
    if (!whatsapp_mobile) {
      delete arrCustomerInfo['whatsapp_mobile'];
    }
    let updateVal = await customerModel.updateOne(customer_id, arrCustomerInfo);
    if (orig_status_id <= inStatus) {
      let arrLeadInfo = await leadModel.getLeadInfo(leadId);
      if (arrLeadInfo.length)
        await leadModel.updateOne(lead_id, arrLeadInfo[0])
    }
  } else {
    let customerData = await customerModel.getOne({ 'customer_mobile': customer_mobile });

    let customerInfo = (customerData && customerData.dataValues) ? customerData.dataValues : ''

    if (customerInfo) {
      let arrCustomerInfo = await customerModel.getOne({ 'id': customerInfo.id });
      if (!customer_city) {
        delete arrCustomerInfo['customer_city_id'];
        delete arrCustomerInfo['customer_city_name'];
      }
      if (!customer_email) {
        delete arrCustomerInfo['customer_email'];
      }
      if (!alternate_email) {
        delete arrCustomerInfo['alternate_email'];
      }
      if (!customer_mobile) {
        delete arrCustomerInfo['customer_mobile'];
      }
      if (!alternate_mobile) {
        delete arrCustomerInfo['alternate_mobile'];
      }
      if (!customer_name) {
        delete arrCustomerInfo['customer_name'];
      }
      if (!language) {
        delete arrCustomerInfo['language'];
      }
      if (!whatsapp_mobile) {
        delete arrCustomerInfo['whatsapp_mobile'];
      }
      arrCustomerInfo['last_became_lead_on'] = new Date().toISOString();
      let custUpt = await customerModel.updateOne(customerInfo.id, arrCustomerInfo);
    } else {
      
      let custInfo = await customerModel.getCustDetails(customerInfo.id);
      if (custInfo.length) {
        custInfo[0]['created_at'] = new Date().toISOString()
      }
      let insRes = await customerModel.createOne(custInfo[0]);
      if (insRes && insRes.dataValues)
        customer_id = insRes.dataValues.id;

    }
    let leadInfo = await leadModel.getLeadInfoToInsert(leadId);
    if (leadInfo.length) {
      leadInfo[0]['customer_id'] = customerInfo.id
      leadInfo[0]['created_at'] = new Date().toISOString();
      leadInfo[0]['added_by'] = params.user_id
    }
    // Insert into Ublms_lead table
    let insResLead = await leadModel.createOne(leadInfo[0]);
    if (insResLead && insResLead.dataValues)
      lead_id = insResLead.dataValues.id;
  }
  let custReq = await customerRequirementModel.getCustReqDetails(customerId, leadId);
  if (custReq.length) {
    custReq[0]['customer_id'] = customer_id;
    custReq[0]['lead_id'] = lead_id;
    custReq[0]['created_at'] = new Date().toISOString();
    custReq[0]['added_by'] = user_id
  }
  // Insert into Ublms_customer_req table
  let inscustReq = await customerRequirementModel.createOne(custReq[0]);

  let checkReqAgent = await customerReqAgentModel.checkReqAgentdetails(customer_id, lead_id)
  if (checkReqAgent && checkReqAgent.length) {
    let arrReqCustomerInfo = await customerReqAgentModel.getReqAgentdetails(customerId, leadId)
    await customerReqAgentModel.updateCustomerReqAgent({ 'customer_id': checkReqAgent[0]['customer_id'] }, arrReqCustomerInfo)
  } else {
    let reqAgentInfo = await customerReqAgentModel.getReqAgentInfoToInsert(customerId, leadId);
    if (reqAgentInfo && reqAgentInfo.length)
      await customerReqAgentModel.createOne(reqAgentInfo[0])

  }
  let carSearchLog = await recomendCarSearchLogModel.getCarSearchInfoToInsert(customerId, leadId);
  if (carSearchLog && carSearchLog.length) {
    carSearchLog[0]['created_at'] = new Date().toISOString();
    await recomendCarSearchLogModel.createOne(carSearchLog[0])
  }
  let leadCarArr = await leadCarModel.getLeadCarInfo(leadId);

  let arrLeadCarId = {};
  if ((leadCarArr.length)) {
    let arrLeadCarInsert = {}
    for (let obj of leadCarArr) {
      arrLeadCarInsert = await leadCarModel.getLeadCarModel(obj, lead_id);
      let insert_Status = await leadCarModel.createOne(arrLeadCarInsert);
      arrLeadCarId[obj['id']] = (insert_Status && insert_Status.dataValues) ? insert_Status.dataValues.id : 0;

      let conversionObj = await conversionInfoModel.getConversionInfoToInsert(obj['id'], leadId);
      if (conversionObj && conversionObj.length) {
        await conversionInfoModel.createOne(conversionObj[0])
      }

      // let purchaseInfoObj = await walkinPurchaseInfoModel.getWalkinPurchaseInfoData(obj['id']);
      // console.log('purchaseInfoObj-------------------------', purchaseInfoObj);
      // if (purchaseInfoObj && purchaseInfoObj.length) { 
      //   await walkinPurchaseInfoModel.createOne(purchaseInfoObj[0])
      // }

      let walkingInfoArr = await walkingInfoModel.getWalkingInfo(obj['id'])
      if ((walkingInfoArr && walkingInfoArr.length)) {
        for (let walkingObj of walkingInfoArr) {
          let arrWalkinInsert = {
            'leads_cars_id': obj['id'],
            'lead_id': lead_id,
            'walkin_datetime': walkingObj['walkin_datetime'],
            'walkin_feedback': walkingObj['walkin_feedback'],
            'walkin_status': walkingObj['walkin_status'],
            'offer_price': walkingObj['offer_price'],
            'asking_price': walkingObj['asking_price'],
            'status': walkingObj['status'],
            'updated_at': walkingObj['updated_at'],
            'created_at': walkingObj['created_at'],
            'added_by': walkingObj['added_by'],
            'api_flag': walkingObj['api_flag']
          };
          let walkinSt = await walkingInfoModel.createOne(arrWalkinInsert);
          let walkin_id = (walkinSt && walkinSt.dataValues) ? walkinSt.dataValues.id : 0

          let walkinFeedbackInfo = walkinFeedbackDealerInfoModel.getWalkingFeedbackInfo(obj['id'], walkingObj['walkin_id']);
          if (walkinFeedbackInfo && walkinFeedbackInfo.length) {
            await walkinFeedbackDealerInfoModel.createOne(walkinFeedbackInfo[0])
          }

        }
      }
    }
  }
  let arrConversation = await conversionModel.getConversation(leadId);
  if (arrConversation && arrConversation.length) {
    for (let objConv of arrConversation) {
      let leads_cars_id = 0;
      if (objConv['leads_cars_id']) {
        leads_cars_id = arrLeadCarId[objConv['leads_cars_id']];
      }
      let arrConvInsert = {
        'lead_id': lead_id,
        'leads_cars_id': leads_cars_id,
        'status_id': objConv['status_id'],
        'sub_status_id': objConv['sub_status_id'],
        'calling_status_id': objConv['calling_status_id'],
        'call_later_date': objConv['call_later_date'],
        'due_date': objConv['due_date'],
        'rating_value': objConv['rating_value'],
        'purchase_intention': objConv['purchase_intention'],
        'comment': objConv['comment'],
        'followup_reason': objConv['followup_reason'],
        'latest': objConv['latest'],
        'dialer_call_datetime': objConv['dialer_call_datetime'],
        'dialer_call_status': objConv['dialer_call_status'],
        'dialer_call_duration': objConv['dialer_call_duration'],
        'dialer_hold_time': objConv['dialer_hold_time'],
        'dialer_talk_time': objConv['dialer_talk_time'],
        'dialer_nextcall_time': objConv['dialer_nextcall_time'],
        'dialer_wrapup_time': objConv['dialer_wrapup_time'],
        'dialer_fail_reason': objConv['dialer_fail_reason'],
        'added_on': objConv['added_on'],
        'updated_on': objConv['updated_on'],
        'added_by': objConv['added_by'],
        'is_dialer_count': objConv['is_dialer_count'],
        'dc_user_id': objConv['dc_user_id'],
        'dc_user_type': objConv['dc_user_type']
      };
      await conversionModel.createOne(arrConvInsert);
    }
  }

  let arrAsyncData = await asyncLeadModel.getLeadAsyncInfo(leadId);
  if (arrAsyncData && arrAsyncData.length) {
    for (let objAsync of arrAsyncData) {
      let apiData = "";
      if (objAsync['api_data']) {
        apiData = JSON.parse(objAsync['api_data']);
        if (apiData['lead_id']) {
          apiData['lead_id'] = lead_id;
        }
        if (apiData['leads_cars_id']) {
          apiData['leads_cars_id'] = arrLeadCarId[apiData['leads_cars_id']];
        }
        apiData = JSON.stringify(apiData);
      }
      let arrAsyncInsert = {
        'mobile': objAsync['mobile'],
        'car_id': objAsync['car_id'],
        'lead_id': lead_id,
        'api_data': apiData,
        'api_type': objAsync['api_type'],
        'flag': objAsync['flag'],
        'api_response': objAsync['api_response'],
        'created_at': objAsync['created_at'],
        'queued_date': objAsync['queued_date'],
        'updated_at': objAsync['updated_at']
      };
      await asyncLeadModel.createOne(arrAsyncInsert);
    }
  }
  let emailTrackInfo = await emailTrackingLogModel.getEmailTrackInfo(leadId);
  if (emailTrackInfo && emailTrackInfo.length) {
    await emailTrackingLogModel.createOne(emailTrackInfo[0])
  }

  let smsInfo = await smsInfoModel.getSmsInfo(leadId);
  if (smsInfo && smsInfo.length) {
    await smsInfoModel.createOne(smsInfo[0])
  }

  //communication history log

  let historyLogInfo = await communicationHistoryLogModel.getHistoryLogToInsert(leadId);
  if (historyLogInfo && historyLogInfo.length) {
    await communicationHistoryLogModel.createOne(historyLogInfo[0])
  }

  let arrClosedPurchase = await closedLeadPurchaseModel.getClosedLeadPurchase(leadId);
  if (arrClosedPurchase && arrClosedPurchase.length) {
    for (let objClosed of arrClosedPurchase) {
      leads_cars_id = 0;
      if (objClosed['leads_cars_id']) {
        leads_cars_id = arrLeadCarId[objClosed['leads_cars_id']];
      }
      let arrClosedInsert = {
        'lead_id': lead_id,
        'lead_car_id': leads_cars_id,
        'car_id': objClosed['car_id'],
        'closed_sub_status_id': objClosed['closed_sub_status_id'],
        'assigned': objClosed['assigned'],
        'price': objClosed['price'],
        'model_id': objClosed['model_id'],
        'reg': objClosed['reg'],
        'reg_car_id': objClosed['reg_car_id'],
        'dealer_name': objClosed['dealer_name'],
        'city_id': objClosed['city_id'],
        'isBroker': objClosed['isBroker'],
        'added_by': objClosed['added_by'],
        'created_at': objClosed['created_at'],
        'updated_at': objClosed['updated_at']
      }
      await closedLeadPurchaseModel.createOne(arrClosedInsert)

    }
  }
  if ((customer_city && !isMobileVerified) || (params['source_id'] == '90' && ['1', '2'].indexOf(params['inStatus']) != -1)) {
    // Need to do
    // let listId = await cityModel.getCompaignListIDByCityId(customer_city);
    // if(listId) {
    // 	
    // 	//$this->pushLeadToL1Dialer($customer_mobile, $listId, $customer_name, $lead_id,$customer_city);
    // }
  }
  return 'new_' + lead_id;
  //redirection(BASE_URL.'index.php?module=leadfinder&control=leaddetailcontrol&action=leaddetailinfo&leadId='.$lead_id);
}

updateFTDofLeads = async (dealer_id) => {
  // Get Dealer ID for car
  let todatedate = '', todatedatetime = '', currentdate = '';
  let capingtime = moment("19:30:00", "HH:mm:ss").format("HH:mm:ss");
  let currenttime = moment().format("HH:mm:ss");
  if (currenttime > capingtime) {
    todatedate = moment().format('Y-MM-D')//new date('Y-m-d');
    todatedatetime = todatedate + " " + capingtime;
    currentdate = moment().add(1, 'days').format('Y-MM-D') + " " + capingtime;
  } else {
    todatedate = moment().subtract(1, 'days').format('Y-MM-D');;
    todatedatetime = todatedate + " " + capingtime;
    currentdate = moment().format("Y-m-d") + " " + capingtime;
  }

  if (dealer_id) {
    // check dealer car is already assigned or not
    let dealerChkData = await leadCarModel.isDealerCarAssigned(dealer_id)

    let totalCount = (dealerChkData && dealerChkData.length) ? dealerChkData[0]['total'] : 0;

    if (totalCount > 0) {
      let dealerBoostData = await dealerBoostModel.getDealerBoostInfo({ dealer_id: dealer_id });
      if (dealerBoostData && dealerBoostData.length) {
        let ftd_lead_count = dealerBoostData[0]['ftd_lead_count'];
        boost_status = dealerBoostData[0]['boost_status'];
        let boost_leads_count = totalCount;
        end_date = moment(dealerBoostData[0]['end_date']).toISOString();

        currentdate = moment(currentDate).toISOString();//strtotime(date("Y-m-d"));
        let updateBoost = {}
        if (end_date >= currentdate && ftd_lead_count <= boost_leads_count) {
          updateBoost['boost_status'] = '1';
          updateBoost['boost_active'] = '0';
        }
        updateBoost['boost_leads_count'] = boost_leads_count;
        updateBoost['updated_at'] = new Date().toDateString();
        updateBoost['updated_ftd_date'] = new Date().toISOString();
        if (dealerBoostData[0]['dealer_id'] > 0) {
          await dealerBoostModel.updateOne(dealer_id, updateBoost)
        } else {
          updateBoost['dealer_id'] = dealer_id;
          await dealerBoostModel.createOne(updateBoost);
        }
      }
    }
  }
}

exports.saveSubOutstationLead = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body);
    let subLeadId = params['sub_lead_id'];
    let leadId = params['lead_id'];
    let isOutstation = params['is_outstation'];
    let updateArrayFields = {
      'is_outstation': isOutstation,
      'updated_at': new Date().toISOString()
    };
    let cond = {
      'id': subLeadId,
      'lead_id': leadId
    }
    leadCarModel.updateLeadsCar(cond, updateArrayFields)
      .then(response => {
        if (response && response.length) {
          ApiController.sendResponse(req, res, 200, 'lead_updated_successfully');
        } else {
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        next(error);
      })

  } catch (error) {
    next(error);
  }
}

exports.getWalkinFeedback = async (req, res, next) => {
  try {
    let walkinFeedback = await walkingInfoModel.getWalkinFeedback()
    ApiController.sendPaginationResponse(req, res, walkinFeedback, {});
  } catch (error) {
    next(error);
  }
}

// exports.saveWalkinPurchaseInfo = async (req, res, next) => {
//   try {

//     let params = _.cloneDeep(req.body);
//     if (params['wp_lead_car_id']) {

//       let walkinPurchaseData = await walkinPurchaseInfoModel.getWalkinPurchaseInfoData(params['wp_lead_car_id']);
//       walkinPurchaseData = walkinPurchaseData[0]
//       let arr = params;
//       let modelHtml = '<option value="">Select Model</option>';
//       let variantHtml = '<option value="">Select Variant</option>';
//       let mmv_data = await InventoryService.getMMVList(req);
//       let mmvList = (mmv_data && mmv_data.data) ? mmv_data.data : {};
//       if ((params['walkin_make'])) {
//         let modelDrpDown = await leadModel.getModelsByMakeID(mmvList, params['walkin_make']);
//         if (modelDrpDown) {
//           for (let mod of modelDrpDown) {
//             let checked = "";
//             if (params['walkin_model'] == mod['id']) {
//               checked = "selected";
//             }

//             modelHtml += `<option value='${mod['id']}' ${checked}> ${mod['m']} </option>`;
//           }
//         }
//       }

//       if (params['walkin_model']) {
//         let variantDrpDown = await leadModel.getVariantByModelID(mmvList, params['walkin_model']);
//         if (variantDrpDown && variantDrpDown.length) {
//           for (let variant of variantDrpDown) {
//             let checked = "";
//             if (params['walkin_variant'] == variant['vn_id']) {
//               checked = "selected";
//             }
//             variantHtml += `<option value='${variant['vn_id']}' ${checked}> ${variant['vn']} </option>`;
//           }
//         }
//       }
//       arr['modelHtml'] = modelHtml;
//       arr['variantHtml'] = variantHtml;
//       let data = JSON.stringify(arr);
//       if (walkinPurchaseData) {
//         //update                
//         let arrVal = {
//           'data': data,
//           'created_at': new Date().toISOString()
//         };
//         await walkinPurchaseInfoModel.updateOne(walkinPurchaseData.id, arrVal)

//       } else {
//         //insert
//         let arrIns = {
//           'lead_car_id': params['wp_lead_car_id'],
//           'created_at': new Date().toISOString(),
//           'data': data
//         }
//         await walkinPurchaseInfoModel.createOne(arrIns)
//       }

//     }
//     ApiController.sendResponse(req, res, 200, 'Walking saved successfully');
//     // ApiController.sendPaginationResponse(req, res, walkinFeedback,{});
//   } catch (error) {

//     next(error);
//   }
// }

exports.getNewCarPriceAndSimilarCarInfo = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body)
    let newCarInfo = await leadCarModel.getNewCarPrice(params.city, params.versionId, params.priceTo)
    ApiController.sendPaginationResponse(req, res, newCarInfo, {});
  } catch (error) {
    next(error);
  }
}

exports.getUserNameByUserType = async (req, res, next) => {
  try {
    let response = {
      'userName': ''
    };
    let params = _.cloneDeep(req.body);
    let userType = params.user_type.toUpperCase();
    if (userType == 'DC') {
      let DCUserInfo = await leadModel.getDCUserName(params.user_id);
      response['userName'] = (DCUserInfo && DCUserInfo.length) ? DCUserInfo[0].name : '';
    } else if (userType == 'BM') {
      let BMUserInfo = await leadModel.getBMUserName(params.user_id);
      response['userName'] = (BMUserInfo && BMUserInfo.length) ? BMUserInfo[0].name : '';
    }
    else if (userType == 'DEALER') {
      let dealerInfo = {};
      let fetchDealerDetail = await LeadService.getDealerDetailsByDealerId(req, {dealer_id:leadCar.carDetails.dealer_id});
      if(fetchDealerDetail.status === 200){
        dealerInfo = fetchDealerDetail.data;
      }
      response['userName'] = (dealerInfo) ? dealerInfo.organization : '';
    }
    ApiController.sendPaginationResponse(req, res, response, {});
  } catch (error) {
    next(error);
  }
}

exports.getWalkinPurchaseInfoData = async (req, res, next) => {
  try {
    const leadId = req.query.lead_id;
    let purchaseInfoObj = await walkinPurchaseInfoModel.getWalkinPurchaseInfoData(leadId);
    purchaseInfoObj = purchaseInfoObj[0]
    if (purchaseInfoObj && purchaseInfoObj.data) {
      purchaseInfoObj = JSON.parse(purchaseInfoObj.data)
    }
    ApiController.sendPaginationResponse(req, res, purchaseInfoObj, {});
  } catch (error) {
    next(error);
  }
}
exports.getConversionStatus = async (req, res, next) => {
  try {
    let lead_id = req.body.lead_id;
    let convStatus = await conversionInfoModel.getConverstionStatus(lead_id)
    ApiController.sendPaginationResponse(req, res, convStatus, {});
  } catch (error) {
    next(error);
  }
}
/**
 * Bulk Upload CSV
 */

exports.leadBulkUpload = async (req, res, next) => {
  let params = _.cloneDeep(req.body);

  const results = [];
  let headers = [];
  const requiredformat = ['Customer Name', 'Customer Mobile', 'Customer Email', 'Customer City', 'Source', 'Sub Source', 'Car ID', 'Platform', 'GCD Code'];

  const fileName = Date.now() + '_' + (params['filename'] || 'bulk-upload-leads'); //FOLDER NAME
  const paths = './public/leads/'; //FOLDER PATH

  //IF PATH NOT EXISTS THEN CREATE
  if (!fs.existsSync(paths)) {
    fs.mkdirSync(paths);
  }
  //UPLOAD FILE ON PATH
  let fileUpload = fs.writeFileSync(paths + fileName, params.myfile, { encoding: 'base64' });

  let dealerList = await DealerService.getDealerList(req); //DEALER LIST
  let blockingReasonsArry = CommonHelper.blockingReasons(); //BLOCKING REASON
  let platform = CommonHelper.platform(); //PLATFORMS
  let getCities = await InventoryService.getStateCityList(req); //CITY STATE LIST
  let getCarmmvList = await InventoryService.getMMVList(req); //CAR MMV LIST 

  //MAKE HELPER OBJ TO PASS
  let helperData = { platform: platform, blockingReasonsArry: blockingReasonsArry, getCities: getCities, getCarmmvList: getCarmmvList };

  //READ FILE
  fs.createReadStream(paths + fileName)
    .pipe(csv.parse({ headers: true }))
    .on('error', error => console.error('error', error))

    .on('data', async (row) => {

      //PARSE ROWS AND CREATE ARRAY
      let newRow = {};
      headers = Object.keys(row); //PARSED HEADERS

      newRow = Object.keys(row).map(rw => {
        return row[rw];
      });
      results.push(newRow);

    })
    // .pipe(process.stdout)
    .on('end', ed => {

      try {
        let parsedData = [];

        if (results.length) {
          // //DIFFERENCE BETWEEN REQUIRED HEADERS & INCOMING HEADERS
          let differences = _.difference(requiredformat, headers);

          if (differences.length) {
            //Mismatched headers
            ApiController.sendResponse(req, res, 200, res.__("headers_not_matched"), { imported: false });
          } else {

            let iteration = 0;

            results.forEach(async (el, key) => {
              let leadData = {};

              leadData['customer_name'] = el[0];
              leadData['customer_mobile'] = el[1];
              leadData['customer_email'] = el[2];
              leadData['customer_city'] = el[3];
              leadData['source_name'] = el[4];
              leadData['sub_source_name'] = el[5];
              leadData['car_id'] = el[6];
              leadData['lead_platform'] = el[7];
              leadData['dealer_id'] = '';

              //IF DEALER CODE GIVEN GET ID
              if (el[8]) {
                let dcDealerId = _.find((dealerList['data'] || []), { 'gcd_code': el[8] });
                leadData['dealer_id'] = (dcDealerId && dcDealerId['id']) || '';
              }

              //VALIDATE GIVEN DATA
              let validateCsvData = await leadModel.checkLeadValidation(req, res, leadData, helperData);

              leadData['reason'] = '';
              //ERROR MESSAGE PARSE
              if (Object.keys(validateCsvData['errorMessage']).length) {
                let validationErr = Object.keys(validateCsvData['errorMessage']).map((err) => { let error = ''; return error += validateCsvData['errorMessage'][err] + '  ' })
                leadData['reason'] = validationErr.join(',');
              }

              //NOT ERROR THE CREATE RAW LEAD
              if (!Object.keys(validateCsvData['errorMessage']).length) {
                leadData['source_id'] = validateCsvData['response']['source_id'] || '';
                leadData['sub_source_id'] = validateCsvData['response']['sub_source_id'] || '';
                leadData['customer_city'] = (validateCsvData['response']['customer_city']) ? validateCsvData['response']['customer_city']['name'] : '';

                let addLeadData = {};
                addLeadData['auth'] = { 'apiKey': LEAD_ADD_API_KEY }

                if (params['isPremium']) {
                  addLeadData['others'] = { ...addLeadData['others'], 'is_premium': 1 || 0 };
                }
                if (leadData['customer_name']) {
                  addLeadData['customer'] = { 'name': leadData['customer_name'] };
                }
                if (leadData['customer_mobile']) {
                  addLeadData['customer'] = { ...addLeadData['customer'], 'mobile': leadData['customer_mobile'] };
                }
                if (leadData['customer_email']) {
                  addLeadData['customer'] = { ...addLeadData['customer'], 'email': leadData['customer_email'] };
                }
                if (leadData['customer_city']) {
                  addLeadData['customer'] = { ...addLeadData['customer'], 'city_id': leadData['customer_city'] };
                }
                if (leadData['otp_verified']) {
                  addLeadData['customer'] = { ...addLeadData['customer'], 'otp_verified': leadData['otp_verified'] };
                }
                if (leadData['car_model']) {
                  addLeadData['others'] = { 'car_model': leadData['car_model'] };
                }
                if (leadData['lead_platform']) {
                  addLeadData['source'] = { 'platform': leadData['lead_platform'] };
                }
                if (leadData['source_id']) {
                  addLeadData['source'] = { ...addLeadData['source'], 'source_id': leadData['source_id'] };
                }
                if (leadData['sub_source_id']) {
                  addLeadData['source'] = { ...addLeadData['source'], 'sub_source_id': leadData['sub_source_id'] };
                }
                if (leadData['dealer_id']) {
                  addLeadData['dealer'] = { 'dealer_id': leadData['dealer_id'] };
                }
                if (leadData['maxprice']) {
                  addLeadData['filters'] = { 'price': { 'upper_bound': leadData['maxprice'] } };
                }
                if (leadData['body']) {
                  addLeadData['filters'] = { ...addLeadData['filters'], 'body_type': leadData['body'] };
                }
                if (leadData['car_id']) {
                  addLeadData['cars'] = { 'car_id': leadData['car_id'] };
                }
                if (leadData['slot']) {
                  addLeadData['place'] = { 'slot': leadData['slot'] };
                }


                let conditionInsert = {
                  'source_id': +leadData['source_id'] || 0,
                  'sub_source_id': +leadData['sub_source_id'] || 0,
                  'mobile': leadData['customer_mobile'],
                  'leads_data': JSON.stringify(addLeadData),
                  'city_id': +leadData['customer_city'] || 0,
                  'apitype': '1',
                  'log_date': new Date().toISOString(),
                  'pushed_status': '0',
                  'is_ub': '1',
                  'type': '3',
                  'reason': leadData['reason']
                }

                await leadRawLogModel.createOne(conditionInsert)
                //   .then(response =>{ console.log(response);

                // })
                // .catch(err => { });

              }

              //GET CALCULATED VALUES FROM ABOVE
              let updatedData = Object.keys(leadData).map(data => leadData[data]);
              parsedData.push(updatedData);

              iteration++;

              //AT LAST WRITE CSV
              if (iteration == results.length) {
                requiredformat.push('Rejected Reason');
                const csvWriter = createCsvWriter({
                  header: requiredformat,
                  path: paths + fileName
                });

                //WRITE CSV FILE
                csvWriter.writeRecords(parsedData.reverse())
                  .then(() => {
                    ApiController.sendResponse(req, res, 200, res.__('data_imported_successfully'), { imported: true });
                  });
              }
            });
          }
        } else {
          ApiController.sendResponse(req, res, 200, res.__("no_data_found_in_csv_file"), { imported: false });
        }


      } catch (err) {
        // next(error);
      }
    });
}

pushLeadToL1Dialer = async (customer_mobile, listId, name, lead_id, city_id) => {
  let cityName = ''
  //let cityName  = $this->getCityName($city_id);
  customer_mobile = trim(customer_mobile);
  let dialshreeAddApiArr = DIALSHREE_ADDAPI_URL;
  let curl_data = {};
  curl_data['function'] = dialshreeAddApiArr['function'];
  curl_data['source'] = dialshreeAddApiArr['source'];
  curl_data['user'] = dialshreeAddApiArr['user'];
  curl_data['pass'] = dialshreeAddApiArr['pass'];
  curl_data['dnc_check'] = dialshreeAddApiArr['dnc_check'];
  curl_data['source_id'] = lead_id;
  curl_data['vendor_lead_code'] = dialshreeAddApiArr['vendor_lead_code'];
  curl_data['list_id'] = listId;
  curl_data['phone_number'] = customer_mobile;
  curl_data['first_name'] = name;
  curl_data['last_name'] = '';
  curl_data['lead_id'] = lead_id;
  curl_data['city'] = cityName;
  let dialshreeAddUrl = dialshreeAddApiArr['dialerUrl'];
  let lead_dialer_status = 0;
  if (listId && dialshreeAddUrl) {
    let reponse = '' //$this->curlPostDatatoDailer($dialshreeAddUrl, $curl_data);
    let data = {};
    data['curl_data'] = JSON.stringify(curl_data);
    data['customer_name'] = CommonHelper.capitalizeFirstLetter(name);
    data['list_id'] = listId;
    data['dialshree_url'] = dialshreeAddUrl;
    data['lead_id'] = String(lead_id);
    data['city'] = cityName;
    data['created_at'] = new Date().toISOString();
    data['response'] = reponse;
    data['type'] = 'leaddetail_chat';
    // Loging Data of Dailer  Mongo DB INSERT
    //$this->mongodb->insertData('ublms_dailer_log', $data);
    await DialerLog.create(data)
    /* For dialer status */
    if (reponse) {
      let responseArr = JSON.parse(reponse, 1);
      if (responseArr['success'] == '1') {
        lead_dialer_status = 1; //For success
      } else {
        lead_dialer_status = 2; //For fail    
      }
    }
    /* For dialer status */
  }
  /* Enter dialer status */
  let data_to_update = {
    'dialer_status': lead_dialer_status,
    'isIndividualLeadDialer': '1'
  }
  await leadModel.updateOne(lead_id, data_to_update)

}

async function getL2UserId(req, leadId, cityId) {
  
  let cityCluster = await cityModel.getClusterIdFromCity(cityId)
  let clusterId = (cityCluster && cityCluster.length) ? cityCluster[0]['cluster_id'] : '';

  let priceData = await leadCarModel.getPriceInfoFromLeadsCar(leadId)
  let minPriceTo = (priceData && priceData.length) ? priceData[0]['price_to'] : '';
  //get the user_id on the basis of price and cluster id::start
  let filterList = await InventoryService.getFilterList(req);
  let pRangeData = _.find(filterList, { 'key': 'max_car_price' });
  let pRange = (pRangeData && pRangeData.length > 0) ? _.find(filterList, { 'id': 'minPriceTo' }) : ''
  let pRangeId = (pRange) ? pRange.id : '' //$pRangeId = $pRangeData[0]['price_id'];
  
  //get the user_id on the basis of price-range and cluster id
  if ((clusterId && clusterId != "") && (pRangeId && pRangeId != "")) {
    let uData = await leadModel.getUserByClusterAndPrice(clusterId, pRangeId);
    if (uData && uData.length) {
      return uData['0']['id'];
    } else {
      return 'no';
    }
  } else {
    return 'no';
  }
  //eoc
}
// exports.getactiveCarList = (req,res) => {
//   try{

//   }catch(err){

//   }
// }
exports.makeSellerLead = async (req, res, next) => {
  try {
    let authUser = _.get(req, 'user');
    let updated_by = authUser['user_id'];
    let lead_id = req.body.lead_id;
    //// if success from api ///
    await leadModel.update({
      isExchange: 1,
      last_updated_date: new Date(),
      last_updated_by: updated_by
    }, {
      where: {
        id: lead_id
      }
    })
    ApiController.sendResponse(req, res, 200, 'Selled Lead Saved');
  } catch (error) {
    
    next(error);
  }
}
exports.saveClosedLeadInfo = async (req, res, next) => {
  try {
    // const transaction = await sequelize.transaction();
    let authUser = _.get(req, 'user');
    let { value } = schemas.saveClosedLeadInfo.validate(req.body);
    let message = "Success";
    let arr_post = {};

    ///////////////////////// GLOBAL Values ///////////////////
    arr_post['make_id'] = value.make;
    arr_post['model_id'] = value.model;
    arr_post['is_broker'] = value.is_broker ? value.is_broker : 2;
    arr_post['added_by'] = authUser.user_id;
    arr_post['lead_id'] = value.lead_id;
    // closed_sub_status_id////// 36 for didn't tell

    if (value.closed_sub_status_id === PURCHASED_USED_CAR) {
      /////////////////// old car purchased //////////////////
      arr_post['assigned'] = value.assigned_arr ? value.assigned_arr : '';
      arr_post['car_id'] = value.car_id ? value.car_id : 0;
      arr_post['lead_car_id'] = value.lead_car_id ? value.lead_car_id : 0;
      arr_post['price'] = value.price ? value.price : 0;
      arr_post['reg'] = value.reg ? value.reg : '';
      arr_post['reg_number'] = value.registration_no ? value.registration_no : '';
      arr_post['dealer_name'] = value.dealer_name ? value.dealer_name : '';
      arr_post['by_whom'] = value.by_whom


      if (value.by_whom === 1 && value.make_id && value.model_id) {
        arr_post['closed_sub_status_id'] = CLOSED_NON_GAADI_DEALER
      }
      else if (value.by_whom === 1 && value.assigned_arr) {
        arr_post['closed_sub_status_id'] = CLOSED_NON_GAADI_DEALER
      }
      else if (value.by_whom === 2 && value.assigned_arr) {
        arr_post['closed_sub_status_id'] = CLOSED_GAADI_INDIVIDUAL //// or 
      }
      else if (value.by_whom === 2 && value.make_id && value.model_id) {
        arr_post['closed_sub_status_id'] = CLOSED_NON_GAADI_INDIVIDUAL
      }
      else if (value.by_whom === 3 && value.make_id && value.model_id) {
        arr_post['closed_sub_status_id'] = PURCHASED_USED_CAR
      }

      if (value.lead_close_date) {
        arr_post['purchase_date'] = value.lead_close_date
      }

    } else if (value.closed_sub_status_id === 20) {
      /////////////////// GET name for make model/////////////
      arr_post['closed_sub_status_id'] = 20;
      arr_post['price'] = 0;
    }

    if (value.closed_lead_info_id) {
      arr_post['updated_date'] = new Date();
      await closedLeadPurchaseModel.updateOne(value.closed_lead_info_id, arr_post)
    } else {
      arr_post['added_date'] = new Date();
      arr_post['updated_date'] = new Date();
      await closedLeadPurchaseModel.create(arr_post);
    }

    ApiController.sendResponse(req, res, 200, message);
  } catch (err) {
    
    next(err)
  }
}
exports.managecarleaddetail = async (req) => {
  const transaction = await sequelize.transaction();
  try {

    const { leadCarId, source_id, isResend, customer_name, customer_email, customer_mobile, isSms, whatsappFlag, isEmail, leadId } = req.body;

    if (leadCarId) {
      let resultArr = []; 
      resultArr = await this.getLeadCarResult(req);
      const totalResult = this.isSameDealerCar(resultArr);

      if (resultArr && resultArr.length) {
        let arrLeadCarEmail = {}, smsContent = "", notDealerInfoSms = "", mailContent = "", smsStatus = "", yearSmsText = "", regardsText = "";
        let z = 1, y = 1, x = 1, sms_dealer_id = 0, sendedDealerLeadCarId = [], dealerSms = "", sendedNonDealerLeadCarId = [];


        for (let i of resultArr) {

          let trustmark = "", rsa = "", dealerOrg = "", carImageInfo = "", yearInfo = "", priceFromInfo = "", certificationSmsText = "", yearSmsText = "", kmInfo = "", colorInfo = "", fuelTypeInfo = "", dealerAddress = "", dealerMobile = '', dealerMapUrl = "", dealerName = "", emailStatus = 0, isEmailSms = 3, dataUrl = "", smsCustomerName = "", vdpUrl = "";

          const carInfo = {

          }

          if (+i.certification_id === 18) {
            trustmark = 'trustmark'
          }
          if (i.organization || i.address) {
            if (i.organization) {
              dealerAddress += i.organization
            }
            if (i.address) {
              if (dealerAddress) dealerAddress = `${dealerAddress},${i.address}`;
              dealerMapUrl = 'MAp INFO'
            }
          }
          if (i.car_profile_image) {
            carInfo.car_image = i.car_profile_image
          }
          if (i.make_year) {
            carInfo.make_year = i.make_year
            yearSmsText = i.make_year
          }
          if (i.km_driven) {
            carInfo.km_driven = i.km_driven
          }
          if (i.colour) {
            carInfo.colour = i.colour
          }
          if (i.price) {
            carInfo.price = i.price
          }
          if (i.fuel_type) {
            carInfo.fuel_type = i.fuel_type
          }
          if (i.owner_type) {
            carInfo.owner_type = OWNER_TYPE[i.owner_type];
          }
          if (i.organization || i.mobile) {
            ////include dealer name also
            if ((i.organization) || (i.mobile) || (i.fname) || (i.individualMobile)) {
              if ((i.user_type && i.user_type === 'P')) {
                dealerName = i.fname
                dealerMobile = i.individualMobile
              } else {
                dealerName = i.organization;
                if (i.priority && i.priority === 1) {
                  dealerName = `${i.organization} ${PDP_DEALER_EMAIL_TEXT}`
                }
                dealerMobile = i.mobile;
              }
            }
          }

          ///////////////////// if customer email present send email ///////////////

          if (i.is_dealer_sent && i.is_dealer_sent === 1) {
            isEmailSms = 1;
          }
          arrLeadCarEmail[i['id']] = isEmailSms;

          let trackingUrl = ""
          ///////////////// generate url TO DO///////
          if (source_id === GAADI_SOURCE_ID) {
            dataUrl = trackingUrl + 'URL'
          }
          else if (source_id === ZIGWHEELS_SOURCE_ID) {
            dataUrl = trackingUrl + 'URL'
          }
          else {
            dataUrl = trackingUrl + 'URL'
          }

          ////////////// MAKE TRACKING URL /////////////

          if (customer_email && isEmail === 1) {
            ////////// send email to customer////////////
            let emailerData = {

            }
            mailContent += await CommonEmailHelper.generateCarLeadEmailData(emailerData);
          }
          if (customer_mobile && isSms === 1) {
            let parentModelName = "", vdpUrl = "", dealerPrioritySmsText = "";
            if (i.priority_dealer && i.priority_dealer === 1) {
              dealerPrioritySmsText = PDP_DEALER_SMS_TEXT
            }
            parentModelName = `${i.make} ${i.model}`;

            if (i.car_id) {
              let arrCarUrl = this.getCarUrl(i.car_id, i.usedcar_id);
            if (source_id === 90) {
                vdpUrl = arrCarUrl['cardekho_url']
              } else if (source_id === 368) {
                vdpUrl = arrCarUrl['zigwheel_url']
              } else {
                vdpUrl = arrCarUrl['cardekho_url']
              }
            }

            if (vdpUrl) {
              vdpUrl = `-NL2BR-(${vdpUrl})`
            }

            /*
            *
            * below code snippet added to get the dealer and individual name and mobile in a single single variables to use in sms (Tauqeer). 
            *                                  */
            let mobile_val = "", sellerName = "";
            if (i.dealer_id === '' && i.user_type === 'P' && i.listingType == 'Paid' && i.domain_id === '2') {
              mobile_val = i['individualMobile'];
              sellerName = i['fname'];
            } else if (i['user_type'] == 'D') {
              mobile_val = i['mobile'];
              sellerName = i['organization'];
            }

            if ((i.is_dealer_sent && i.is_dealer_sent === 1) || (!i.dealer_id && i.user_type === 'P' && i.listingType === 'PAID' && i.domain_id === 2)) {
              if (totalResult > 1) {
                if (z === 1) {
                  smsContent = `Dear ${smsCustomerName} following are the seller's contact and car details matching your requirements.-NL2BR--NL2BR-`;
                }
                if (sms_dealer_id !== mobile_val) {
                  if (z > 1) {
                    smsContent += "-NL2BR-"
                  }
                  smsContent += `${x}.${sellerName}${dealerPrioritySmsText}-NL2BR-`
                  smsContent += `Ph: ${mobile_val}-NL2BR-`;
                  x++;
                }
                smsContent += `${i.make} ${parentModelName} (${yearSmsText})${vdpUrl}-NL2BR-`
              }
              else {
                if (z === 1) {
                  smsContent = `Dear ${smsCustomerName} check out car photos and price by clicking on the link.-NL2BR--NL2BR-`;
                }
                if (sms_dealer_id !== mobile_val) {
                  smsContent += `Seller:${sellerName}${dealerPrioritySmsText}-NL2BR-`;
                  smsContent += `Ph: ${mobile_val}-NL2BR-`;
                }
                smsContent += `${i.make} ${parentModelName} (${yearSmsText})${vdpUrl}-NL2BR-`;
                // $carStock = $this->getDealerCarStock($val['dealer_id']);
              }
            }


            if ((i.individualMobile) && !(i.dealer_id) && (i.user_type === 'P') && (i.listingType === 'Paid') && i.domain_id === 2) {
              const Individsmstext = `Hey ${i.fname}, a buyer is interested in your  ${i.make} ${i.model} on CarDekho.com. Contact ${smsCustomerName} ${customer_mobile} now!`;
              ///send sms
            }
            sendedDealerLeadCarId.push(i.id);
            sms_dealer_id = mobile_val
            z++;
          } else {
            if (y === 1) {
              notDealerInfoSms += `Dear ${smsCustomerName} check out car photos and price by clicking on the link.-NL2BR--NL2BR-`
            }
            notDealerInfoSms += ` ${y}.${yearSmsText} ${i.make} ${i.model} ${vdpUrl}-NL2BR-`;
            y++;
            nonDealerSms = ""
            sendedNonDealerLeadCarId.push(i.id)
          }
        }
        if (sendedDealerLeadCarId && sendedDealerLeadCarId.length > 0) {
          sendedDealerLeadCarId = sendedDealerLeadCarId.join(",")
          dealerSms = sendedDealerLeadCarId;
        }
        if (smsContent) {
          if (totalResult > 1) {
            smsContent += "-NL2BR-Call now to book your visit to the showroom. Hurry up before your dream car gets sold. ";
          } else if (i.is_dealer_sent === 1 && totalResult === 1) {
            smsContent += "-NL2BR-Hurry up! Contact the seller now before your dream car gets sold. ";
          }
          smsContent += regardsText;
          // send sms from service here 
        }
        let checkOptIn = this.checkCustomerOptIn(leadId);
        if (whatsappFlag === 1) {
          if (checkOptIn !== 1) {
            ////save whatsapp message in sms info
          } else {
            //// send whatsapp message
          }
        }
        if (notDealerInfoSms) {
          notDealerInfoSms += regardsText;
          // notDealerSmsStatus = 
          // send sms
        }
        if (arrLeadCarEmail && arrLeadCarEmail.length) {
          /////////////// update lead car table//////////////
          for (let i in arrLeadCarEmail) {

            leadCarModel.update({
              is_email_sms: arrLeadCarEmail[i],
              updated_at: new Date()
            }, {
              where: {
                id: {
                  [Op.in]: i
                }
              },
              transaction: transaction
            })
          }

        }
      }
      await transaction.commit();
    }
  } catch (error) {
    await transaction.rollback();
    //next(error)
  }
}
exports.getLeadCarResult = async (req) => {
  const requestParams = req.body;
  let LeadsNotIn = '';
  const { leadId, leadCarId, isResend } = requestParams;
  const checkSmsSent = await this.checkValidSmsExists(leadId);
  const checkIsSmsEmailSent = await this.checkIsSmsEmailSent(leadId, leadCarId);
  let leadCarIds = [];
  if (checkIsSmsEmailSent) {
    leadCarIds.push(checkIsSmsEmailSent);
  }
  const leadCarIdCheck = await this.getLeadcarIds(leadId);
  if (leadCarIdCheck) {
    leadCarIds.push(leadCarIdCheck);
  }
  if (checkSmsSent) {
    LeadsNotIn += ` and ulc.id NOT IN (${checkSmsSent}) `;
  }
  const field = 'ulc.id,ulc.car_id,ulc.model_id,ulc.make_name as make, ulc.model_name as model, ulc.variant_name as carversion, ulc.price_from, ulc.price_to, ulc.is_dealer_sent,ulc.dealer_id ';
  let sql = `Select ${field} from ${UBLMS_LEADS_CARS} as ulc where ulc.status_id!='7' and ulc.sub_status_id!='12' ${LeadsNotIn} `;
  if (leadCarIds) {
    sql += `and ulc.id in (${leadCarIds.join()})`;
  }
  if (isResend && +isResend !== 1 && !checkIsSmsEmailSent) {
    sql += ` and (ulc.is_email_sms NOT IN('1','3') OR (ulc.is_email_sms = '3' && is_dealer_sent = '1')) `;
  }
  let response = await sequelize.query(sql, { raw: true, type: sequelize.QueryTypes.SELECT });
  let leadCars = [];
  for (let i of response) {
    leadCars.push(i.car_id);
  }
  let leadsCarDetails = await InventoryService.getStockList(req, { stock_used_car_id_arr: leadCars, call_server: 'ub',car_status: [1,2]}); 
  let leadsCar = leadsCarDetails.data;
  let leadCarDealer = [];
  for (i of leadsCar) {
    let { dealer_id } = i;
    leadCarDealer.push(dealer_id);
  }
  let dealerDeatils = await DealerService.getDealerDetailsByIds(req, {dealer_id:leadCarDealer});
  let leadCarDealerData = dealerDeatils.data;
  let priority= '';
  if (leadsCar && response) {
    for (i of response) {
      if (leadsCar.length > 0) {
        leadsCar.map(carData => {
          if (carData.id == i.car_id) {
            if (carData.paid_score == 1)
              priority = "Free"
            else if (carData.paid_score == 2)
              priority = "Paid"
            else if (carData.paid_score == 3)
              priority = "Paid Pro"
            i.make_year = carData.make_year
            i.km_driven = carData.km_driven
            i.uc_colour = carData.uc_colour
            i.car_profile_image = carData.car_profile_image
            i.fuel_type = carData.fuel_type
            i.owner_type = carData.owner_type
            i.user_type = carData.user_type
            i.transmission = carData.transmission
            i.listingType = carData.listingType
            i.domain_id = carData.domain_id
            i.certification_id = carData.certification_id
            i.price = carData.display_price
            i.make = carData.make
            i.model = carData.model
            i.version = carData.carversion
            i.dealer_id = carData.dealer_id
            i.certification_id = carData.reg_no
            i.priority = priority
          }
        });
      }
    }    
  }
  if (response && leadCarDealerData) {
    for (i of response) {
      leadCarDealerData.map(dealer => {
        if (i.dealer_id == dealer.id) {
          i.organization = dealer.organization
          i.mobile = dealer.dealership_contact
          // i.lead_count = ftd_lead_count
          // i.is_sent_leads = is_sent_leads
        }
      });
    }
  }  
  return response;
}
exports.checkValidSmsExists = async (leadId) => {

  const sql = `Select group_concat(lead_car_id) as lead_car_id from ${UBLMS_SMS_INFO} where lead_id ='${leadId}' and type='1' and sent_flag in ('1','0')  and date(created_at) = '${moment().format("YYYY-MM-DD")}' order by id desc limit 0,1`;

  let response = await sequelize.query(sql, { raw: true, type: sequelize.QueryTypes.SELECT });
  response = response[0].lead_car_id;
  return response;
}
exports.checkIsSmsEmailSent = async (leadId, leadCarIds) => {
  const sql = ` Select group_concat(id) as leads_car  from ${UBLMS_LEADS_CARS} where lead_id ='${leadId}' and id IN (${leadCarIds}) and status_id != '7' and sub_status_id != '12' and  is_email_sms='0' group by lead_id`;

  let response = await sequelize.query(sql, { raw: true, type: sequelize.QueryTypes.SELECT });
  if (response && response[0]) {
    response = response[0].leads_car;
  }
  return response
}
exports.getLeadcarIds = async (leadId) => {
  let lead_car_id = '';
  const sql = `Select id,group_concat(lead_car_id) as lead_car_id,sms_type from ${UBLMS_SMS_INFO} where lead_id ='${leadId}' and type='1' and is_sent ='2' and sent_flag='-1' and date(created_at)=${moment().format("YYYY-MM-DD")}  order by id desc limit 0,1`;
  let response = await sequelize.query(sql, { raw: true, type: sequelize.QueryTypes.SELECT });
  if (response && response[0] && response[0].lead_car_id > 0) {
    lead_car_id = response[0].lead_car_id
  }
  return lead_car_id;
}
exports.isSameDealerCar = (resultArr) => {
  let totalRes = 1;
  if (resultArr && resultArr.length) {
    let dealer_id = 0, z = 1;
    for (let i of resultArr) {
      if ((+i.is_sent_leads === 1) || (!i.dealer_id && i.user_type && i.user_type === 'P' && i.listingType && i.listingType === 'Paid' && i.domain_id && i.domain_id === '2')) {
        if (i.dealer_id !== dealer_id && z > 1) {
          totalRes = 2;
          break;
        }
        dealer_id = i.dealer_id
        if (!i.dealer_id && i.user_type && i.user_type === 'P' && i.listingType && i.listingType === 'Paid' && i.domain_id && i.domain_id === '2') {
          dealer_id = z;
        }
        z++;
      }

    }
  }
  return totalRes;
}
exports.getCarUrl = async (carId, usedCarId) => {
  const sql = `Select cardekho_url,gaadi_url,zigwheel_url from ${UBLMS_CAR_URL} where car_id=${carId}`;
  let response = await sequelize.query(sql, { raw: true, type: sequelize.QueryTypes.SELECT });
  response = response[0];
  const urls = {
    cardekho_url: '',
    gaadi_url: '',
    zigwheel_url: ''
  }
  if (response) {
    if (response.gaadi_url) {
      urls.gaadi_url = response.gaadi_url;
      if (!response.cardekho_url) {
        urls.cardekho_url = response.cardekho_url
      }
      else if (usedCarId) {
        urls.cardekho_url = `${CAR_DEKHO_CAR_DETAIL_LINK} ${usedCarId}.htm`
      }
      else {
        urls.cardekho_url = response.gaadi_url
      }
      if (response.zigwheel_url) {
        urls.zigwheel_url = response.zigwheel_url
      } else {
        urls.zigwheel_url = `${ZIGWHEEL_CAR_DETAIL_LINK}${carId}`
      }
    } else {
      let gaadiURL = `${USED_CAR_EMAIL_DETAIL_LINK}-${carId}`
      urls = {
        cardekho_url: gaadiUrl,
        gaadi_url: gaadiUrl,
        zigwheel_url: gaadiUrl
      }
    }
  }
  return urls;
}

const getCityNameByIdAndlang = async (req, params) => {
  

}

const sendLeadStatusToDC = async (req, params) => {
  // let customerData = await customerModel.getbudgetandlocation(customer_mobile);
  return new Promise(async(resolve, reject) => {
    let arr_data = {};
    let dueDate = "";
    if (params['dueDate']) {
      dueDate = new Date(params['dueDate']).toISOString();
    } else {
      dueDate = "";
    }
 
    arr_data = { mobile: params['customer_mobile'], name: params['customer_name'], email: params['customer_email'], budget: params['budget'], followup_date: params['dueDate'] };

    //console.log('params------------------------------', params);
    let leads_car_ids = await getCarIdsFromLeadsCarId(params['leadCarId']);
    let carDetails = await InventoryService.getStockList(req, { stock_used_car_id_arr: leads_car_ids, call_server: 'ub', car_status: [1, 2] });
    let cars = carDetails.data;
    let carsdata = {};
    for (i of cars) {      
      let update_dealerId =  await LeadModel.updateDealerId(i.id, i.dealer_id);     
      let dealer_id = i.dealer_id;
      if (!carsdata[dealer_id]) {
        carsdata[dealer_id] = [i.id];
      } else {
        carsdata[dealer_id].push(i.id);
      }
    }
    let subLeadsStatus = await LeadModel.getSubLeadsStatus(params.leadId);
    var output = Object.entries(carsdata).map(([key, value]) => ({ key, value }));
    let dc = [];
    let k = 0;
    let source = 'OTO';
    if (params.source_id == 90 && process.env.COUNTRY_CODE == 'ph') {
      source = 'CARMUDI';
    } else if (params.source_id == 368 && process.env.COUNTRY_CODE == 'ph') {
      source = 'ZIGWHEELS';
    }
 
    for (j of output) {
      let info = {};
      const found = subLeadsStatus.find(element => {
        if(element.dealer_id==j.key)
        return element.status_id;
      });
    
      if(found){
        let arr = {...arr_data};
        arr['lead_status'] = found.status_id;
        arr['lead_sub_status'] = found.sub_status_id;
        info['arr_data'] = arr;
        info['dealer_id'] = j.key;
        info['car_id'] = j.value.join();
        info['lead_source'] = source;
        info['lead_comment'] = params.leadCommentText;
        info['lead_id'] = params.leadId;
        dc.push(info);
      }
      
    }
    let bodydata = { info: dc };
    let dcRes = await LeadService.sendLeadStatusToDC(req, bodydata); //DONT WAIT FOR THIS
    resolve(dcRes);
  }).catch(err => {
    // console.log('lead Controller sendLeadStatusToDC error--------------', err);
  });

}
const getCarIdsFromLeadsCarId = async (leadCarId) => {
  let car_id = [];
  let leadResultArr = await leadCarModel.getLeadDetails(leadCarId, is_sent_to_dealer=1);
      if (leadResultArr && leadResultArr.length) {
        for (leadCar of leadResultArr) {
          car_id.push(leadCar.car_id);
        }
      }
   return car_id; 
}
exports.checkCustomerOptIn = async (leadId) => {
  let sql = `Select whatsapp_opt_in from ${UBLMS_LEADS} as l INNER JOIN ${UBLMS_CUSTOMER_INFO}  ci on ci.customer_id=l.customer_id where l.id = '${leadId}'  and ci.whatsapp_opt_in='1' and l.source_id IN ('90','6') limit 0,1`;
  let response = await sequelize.query(sql, { raw: true, type: sequelize.QueryTypes.SELECT });
  response = response[0];
  if (response && response.whatsapp_opt_in) return true;
  return false;
}

exports.getWalkinFeedbackList = async (req, res) => {
  const requestParams = req.body;

  let walkinFeedbacks = await walkinFeedbackModel.get();
  ApiController.sendSuccessResponse(req, res, walkinFeedbacks, '');

}

exports.individualLeadDetails = async (req, res) => {
  try {
    let msg = '';
    const params = req.body;
    let resultData = {};
    let data = [];
    let cars = [];
    let inCar = [];
    let individualCarIds = await InventoryService.getStockList(req, { stock_used_car_id_arr: params.carIds, call_server: "ub" });
    if (individualCarIds.status == 200 && individualCarIds.data && individualCarIds.data.length > 0) {
      let carDetails = individualCarIds.data;
      cars = carDetails.filter(element => {
        if (element.user_type != 'D') {
          inCar.push(element.id);
        }
      });
    }
    
    if (inCar.length > 0) {
      let res = await leadModel.getLeadDetailsByCarId(inCar);      
      if (res && res.length > 0) {
        for (j of res) {
          resultData[j.car_id] = j;
        }
      }
      data.push(resultData);
    } else {
      msg = "CarIds are mandatory";
    }
    ApiController.sendResponse(req, res, 200, msg, data);
  } catch (error) {
    // console.log('Error ', error);
  }
}

exports.saveWalkinPurchaseInfo = async (req, res) => {
  try{
      const params = req.body;
      const authUser = _.get(req, 'user');

      params['comments'] = (params['comments']) ? (params['comments']).replace(/,\s*$/, ""):'';
      
        let walkinPurchaseData = await walkinPurchaseInfoModel.getWalkinPurchaseInfoData(params['lead_car_id']);
        let result;
        if(walkinPurchaseData.length > 0) { 
            let conditionUpdate = {'data' : JSON.stringify(params)};
            result = await walkinPurchaseInfoModel.updateOne(walkinPurchaseData[0]['id'], conditionUpdate);

        } else {
            //insert
            let conditionInsert = {'lead_car_id' : params['lead_car_id'], 'created_at' : dateFormat('yyyy-mm-dd HH:MM:ss'), 'data' : JSON.stringify(params)};
            result= await walkinPurchaseInfoModel.createOne(conditionInsert);

        }

          //SAVE CONVERSION
          let conversationData = {};
  
          conversationData = {
            'lead_id': params['lead_id'],
            'leads_cars_id': params['lead_car_id'],
            'dealer_id': params['dealer_id'] || 0,
            'status_id': params['status_id'] || 6,
            'sub_status_id': params['sub_status_id'] || 12,
            'calling_status_id': 0,
            'comment': params['comments'],
            'latest': '1',
            'added_on': new Date().toISOString(),
            'added_by': authUser['user_id'],
            'dc_user_id': params['conversionSource'] || 0,
            'dc_user_type': params['dc_user_type'] || ''
          };
          await conversionModel.createOne(conversationData);
          
          ApiController.sendResponse(req, res, 200, 'Feedback saved successfully');       

  }catch(error){
    // next(error);
    ApiController.sendResponse(req, res, 400, 'Please try again');

  }
}

exports.sendMailAndNotifications = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
    params=req.body;
    let dc_lead_id = params.dc_lead_id;
    let lead_id = params.lead_id;
    let car_ids = params.car_id;    
    let subLeads = await LeadModel.getSubleadsFromLeadId(lead_id);
    let customerInfo = await customerModel.getCustomerDetailByLeadId(lead_id);
    subLeads.forEach(async element => {
      if ((element.status_id == 3 && element.sub_status_id == 3 && customerInfo[0].otp_verified == 0) || (element.status_id == 5 && element.sub_status_id == 9)) {       
        let new_params = {
          custInfo: customerInfo[0],
          subleadInfo: element,
          walkinStatus: element.status_id,
          walkinSubStatus: element.sub_status_id,
          lead_id: dc_lead_id
        }
        subLeadNotification.SendMailAndSMS(req, new_params);        
      }
    });
    ApiController.sendResponse(req, res, 200);
  } catch (error) {    
    reject(error); 
  }
  });
}


/**
 * Lead List 
 */
exports.importLeadFinderData = async (req, res, next) => {
  try {

    let requiredformat = ['Lead ID', 'Dealer ID', 'Source', 'Name of the customer', 'Mobile Number', 'Vehicle Inquired', 'Car ID', 'Make Year', 'Cluster/City', 'Date Created', 'Follow up date', 'Inquiry Date', 'Status', 'Sub Status', 'Call Status', 'Modified by', 'Modified Date', 'Added By', 'Dealer name inquire', 'Dealer Priority', 'Walk-in Schedule Date', 'Walk-in Completed Date'];

    let newReq = Object.assign({}, req);

    newReq.body['importXls'] = true;
    newReq['body']['isPaginationApply'] = false;
    newReq['body']['groupByCarAndleadId'] = true; //GET ALL CARS OF LEADS SO GROUP BY LEAD ID AND CAR ID

    let getLeadListData = await axios.post(`${newReq.services.CORE_HOST}/ub/lead/listLead`, newReq.body)

    let excelData = [];

    if(getLeadListData && getLeadListData.data && getLeadListData.data.data && getLeadListData.data.data.length){
      let csvLeadData = [];
      async.forEach(getLeadListData.data.data, async (el, callback) => {
        let newObj = Object.assign({}, el);

          if(!newObj['lead_details']){
            newObj['lead_details'] = {};
          }

          if(newObj.lead_cars_id){
            //FIND WALKIN COMPLETED DATE
            let walkinInfo = await walkinPurchaseInfoModel.getWalkinPurchaseInfoData(newObj.lead_cars_id);
            if(walkinInfo && walkinInfo.length){
              let walkinData = walkinInfo[0]['data'] ? JSON.parse(walkinInfo[0]['data']) : {};
              if(walkinData && walkinData.walkInDate){
                newObj['lead_details']['walkin_completed_date'] = walkinData.walkInDate;
              }
            }

            //FIND ADDEDBY AND MODIFIED BY DETAILS
            let addedByDetails = await leadCarModel.getLeadDetailsData({id: newObj.lead_cars_id});
            if(addedByDetails && addedByDetails.length){
              newObj['lead_details'].added_by_name        = addedByDetails[0].added_by_name || '';
              // newObj['lead_details'].modified_by_name     = addedByDetails[0].modified_by_name || '';
              newObj['lead_details'].status_id            = addedByDetails[0].status_id || '';
              newObj['lead_details'].status_name          = addedByDetails[0].status_name || '';
              newObj['lead_details'].sub_status_name      = addedByDetails[0].sub_status_name || '';
            }


            //FIND LEAD CAR WALKIN SCHEDULE DATE
            let walkingInfoArr = await walkingInfoModel.getWalkingInfoByLeadCarId(newObj.lead_cars_id);
            if(walkingInfoArr && walkingInfoArr.length){
              newObj['lead_details']['walkin_scheduled_date'] = walkingInfoArr[0].walkin_datetime;
            }

          }
          
          //FIND CLUSTER CITY
          if(newObj.reg_place_city_id){
            clusterData = await clusterModel.getCityListAndCluster({cityId: [newObj.reg_place_city_id]});
            if(clusterData && clusterData.length){
              newObj['lead_details'].reg_car_city_cluster_name = clusterData[0]['name']; 
            }
          }
          csvLeadData.push(newObj);

      callback();
          
    }, async (err) => {
          csvLeadData.forEach(async (el, key) => {
            //CSV DATA
            excelData[key] = [
              el.id || '',
              el.dealer_id || '',
              el.source_name || '',
              el.customer_name || '', 
              el.customer_mobile || '',
              ((el.make_name || '') + " " + (el.model_name || '')),
              el.car_id || '',
              el.make_year || '',
              ((el.user_type && el.user_type === 'D') ? (el.customer_city_name ? ((el.reg_car_city_cluster_name ? el.reg_car_city_cluster_name+'|' : '')+el.customer_city_name) : (el.reg_car_city_cluster_name || '')) : (el.reg_car_city_name ? ((el.reg_car_city_cluster_name ? el.reg_car_city_cluster_name+'|' : '')+el.reg_car_city_name) : (el.reg_car_city_cluster_name||''))),
              moment(el.created_at).format("DD MMM YYYY hh:mm A"),
              (el['lead_details'] && el['lead_details'].status_id !== 7 ? moment(el.due_date).format("DD MMM YYYY hh:mm A") : ''),
              moment(el.created_at).format("DD MMM YYYY hh:mm A"),
              (el['lead_details'] && el['lead_details'].status_name) ? el['lead_details'].status_name : (el.status_name || ''),
              (el['lead_details'] && el['lead_details'].sub_status_name) ? el['lead_details'].sub_status_name : (el.sub_status_name || ''),
              (el.call_status_name || ''),
              (el.modified_by || ''),
              (el.modified_by) ? moment(el.updated_at).format("DD MMM YYYY hh:mm A") : '',
              (el['lead_details'] && el['lead_details'].added_by_name) ? el['lead_details'].added_by_name : '',
              el.seller_organization || '',
              (el.car_dealer_priority==3) ? 'Paid Pro' : ((el.car_dealer_priority==2)?'Paid':((el.car_dealer_priority==1)?'Free':'')),
              (el['lead_details'] && el['lead_details'].walkin_scheduled_date) ? moment(el['lead_details'].walkin_scheduled_date).format("DD MMM YYYY hh:mm A") : '',
              (el['lead_details'] && el['lead_details'].walkin_completed_date) ? moment(el['lead_details'].walkin_completed_date).format("DD MMM YYYY") : '' 

            ];
          });

          if ((excelData).length) {            

            const fileName = 'Lead_Finder_' + dateFormat('dd_mm_yyyy_h_MM_ss_TT') + '.csv';

            //AWS
            let finalData = [requiredformat, ...excelData]; //COMBINE HEADERS AND DATA
            let stringData = '';
            //CONVERT ARRAY DATA TO STRING
            finalData.forEach(el=>{
              stringData += el.join(',')+'\n';
            });

            let s3Csv = await InventoryService.writeCsS3(req, {upload_type: 'lead_csv', fileName: fileName, data: stringData});

            if(s3Csv && s3Csv.status == 200 && s3Csv.data && s3Csv.data.Location){
              
              setTimeout(()=>{
                //DELETE CSV FILE FROM FOLDER
                  InventoryService.removeLeadCsvS3(req, {upload_type: 'lead_csv', fileKey: s3Csv.data.Key});
              },10000);

              ApiController.sendResponse(req, res, 200, '', s3Csv.data.Location);
            }else{
              ApiController.sendResponse(req, res, 200, '', '');
            }
          }else{
            ApiController.sendResponse(req, res, 200, "No leads found");
          }
      });

    }else{
      ApiController.sendResponse(req, res, 200, "No leads found");
    }

  } catch (error) {
// console.log(error)
  }
}

exports.getLeadCountByCarIdAndDate = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    try {
      let params = req.body;
      let errmsg = [];
      let carIds;
      let param = [];

      if (params.carIds != undefined && errmsg.length > 0) {
        ApiController.sendErrorResponse(req, res, errmsg);
      }
      let result = await LeadModel.getLeadCountByCarIDandDate(params);

      let carResult = {};
      if (result.length > 0) {
        for (let i = 0; i < result.length; i++) {
          carResult[result[i].car_id] = result[i].total_leads;
        }
        ApiController.sendResponse(req, res, 200, "Success", carResult);
      } else {
        ApiController.sendResponse(req, res, 200, "Success", []);
      }
    } catch (error) {
      reject(error);
    }
  });
}
