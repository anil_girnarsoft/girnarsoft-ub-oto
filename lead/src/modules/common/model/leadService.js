const rp = require('request-promise');
const { async } = require('q');
const config = require("../../../../config/config");

module.exports.getDealerDetailsByCarId = async function (req, params) {    
  if (!req.services || !req.services.LEAD_HOST) return Promise.reject("LEAD_HOST_NOT_DEFINE");
      let options = {
          method: 'POST',
          uri: `${req.services.LEAD_HOST}/cron/getDealerDetailsByCarId`,
          body: params,
          headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services']},
          json: true,
          simple: false
      };
      return rp(options);
}

module.exports.sendLeadStatusToDC = async (req, params)=>{
    let options = {
        method: 'POST',
        uri: `${req.services.LEAD_HOST}/cron/ub-lead`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services']},
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.getDealerDetailsByDealerId = async function (req, params) {    
    if (!req.services || !req.services.DEALER_HOST) return Promise.reject("DEALER_HOST_NOT_DEFINE");

        let options = {
            method: 'POST',
            uri: `${req.services.LEAD_HOST}/cron/getDealerDetailsByDealerId`,
            body: params,
            headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services']},
            json: true,
            simple: false
        };
        return rp(options);
}

module.exports.addToDialer = async function (req, params) {

        let options = {
            method: 'GET',
            uri: `${config.DIALSHREE_ADDAPI_HOST}`,
            qs: params,
            json: true,
            simple: false
        };
        return rp(options);
}


module.exports.getFacebookLeads = async function (formID, params) {

    let options = {
        method: 'GET',
        uri: `${config.FB_GRAPH_URL+formID}/leads`,
        qs: params,
        json: true,
        simple: false,
        resolveWithFullResponse: true 
    };
    return rp(options);

}

module.exports.refreshFbToken = async function (params) {

    let options = {
        method: 'GET',
        uri: `${config.REFRESH_TOKEN_URL}`,
        qs: params,
        json: true,
        simple: false,
        resolveWithFullResponse: true 
    };
    return rp(options);

}


module.exports.sendFbLeadToDC = async function (req, params) {
    if (!req.services || !req.services.LEAD_HOST) return Promise.reject("LEAD_HOST_NOT_DEFINE");

    let options = {
        method: 'POST',
        uri: `${req.services.LEAD_HOST}/lead/createLead`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'], "apiKey": OTO_API_KEY},
        json: true,
        simple: false
    };
    return rp(options);

}

