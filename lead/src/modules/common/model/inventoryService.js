const rp = require('request-promise');

module.exports.getFilterList = async function (req, params) {
    if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");

        let options = {
            method: 'GET',
            uri: `${req.services.INVENTORY_HOST}/inventory/getfilter`,
            headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'], "apiKey": OTO_API_KEY},
            json: true,
            simple: false
        };
        return rp(options);
}

module.exports.getStockList = async function (req, params) { 
    if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");

        let options = {
            method: 'POST',
            uri:`${req.services.INVENTORY_HOST}/inventory/v2/sfa_stocklist`,
            body: params,
            headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'], "apiKey": OTO_API_KEY},
            json: true,
            simple: false
        };
    //    console.log('*******************-------------------------**********************************', options);
        return rp(options);
}

module.exports.getCarDetails = async (req, params)=>{
    if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");
    if (!params || !params.stock_used_car_id) return Promise.reject("INVALID_REQUEST");  

    let options = {
        method: 'POST',
        uri: `${req.services.INVENTORY_HOST}/inventory/usedstockcardetails`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'],'apiKey': OTO_API_KEY},
        json: true,
        simple: false
    };    
    return rp(options);
}

module.exports.getMMVList = async (req)=>{
if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");

    let options = {
        method: 'GET',
        uri: `${req.services.INVENTORY_HOST}/commonservice/mmv_all`,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'],'apiKey': OTO_API_KEY},
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.getStateCityList = async (req, params)=>{

    if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE"); 
    let options = {
        method: 'GET',
        uri: `${req.services.INVENTORY_HOST}/commonservice/state_city_all`,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'],'apiKey': OTO_API_KEY},
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.getLocalityByCityId = async (req, params)=>{
    if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");

    let options = {
        method: 'POST',
        uri: `${req.services.INVENTORY_HOST}/commonservice/locality_list`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'],'apiKey': OTO_API_KEY},
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.getCarDekhoBlockLead = async (req, params)=>{
    if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");
    let options = {
        method: 'GET',
        uri: `${req.services.INVENTORY_HOST}/getIPhoneFeedsDispatchAction.do`,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services']},
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.getCityListById = async (req, params)=>{

    //if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");
    let options = {
        method: 'GET',
        uri: `${req.services.INVENTORY_HOST}/commonservice/city_list?city_ids[]=${params.city_ids}`,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'], "apiKey": OTO_API_KEY},
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.getGaadiBlockLead = async (req, params)=>{
    if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");
    let options = {
        method: 'GET',
        uri: `${req.services.INVENTORY_HOST}/v1/used-cars/blockUserUb`,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services']},
        json: true,
        simple: false
    };
    return rp(options);
}


module.exports.getStockCountByDealerIdWithDate = async(req, params)=> {
    if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");
    let options = {
        method: 'POST',
        uri: `${req.services.INVENTORY_HOST}/inventory/stock_count_by_dealer`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'], "apikey": UB_API_KEY},
        json: true,
        simple: false
    };
    //console.log('getStockCountByDealerIdWithDate----------------------------------------', options);
    return rp(options);
}


module.exports.writeCsS3 = async (req, params)=>{
    if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");
    
    let options = {
        method: 'POST',
        uri: `${req.services.INVENTORY_HOST}/commonservice/s3_csv_write`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'],'apiKey': OTO_API_KEY},
        json: true,
        simple: false
    };
    return rp(options)
}

module.exports.removeLeadCsvS3 = async (req, params)=>{
    if (!req.services || !req.services.INVENTORY_HOST) return Promise.reject("INVENTORY_HOST_NOT_DEFINE");
    
    let options = {
        method: 'POST',
        uri: `${req.services.INVENTORY_HOST}/commonservice/s3_csv_remove`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'],'apiKey': OTO_API_KEY},
        json: true,
        simple: false
    };
    return rp(options);
}
