const _ = require("lodash");
const rp = require('request-promise');
const fs = require('fs');
const conf = require("../../../../config/config");
module.exports.sendEmailNotification = async function (req, params) { 
    if (!req.services || !req.services.NOTIFICATION_HOST) return Promise.reject("NOTIFICATION_HOST_NOT_DEFINE");
    if (!params || !params.sender || !params.receiver || !params.subject || !params.html) return Promise.reject("INVALID_REQUEST");
        let options = {
            method: 'POST',
            uri: `${req.services.NOTIFICATION_HOST}/notification/sendnotification`,
            body: params,
            headers: _.merge(_.pick(req.headers, ['gateway-user', 'gateway-services', 'mobile-country-code', 'accept-language', "tenant", "referer"]), {"from-headers":`originalUrl-${req.get('host') + req.originalUrl}`}),
            json: true,
            simple: false
        };
        return rp(options);

}

module.exports.sendSMS = async (req, params)=>{
    if (!req.services || !req.services.NOTIFICATION_HOST) return Promise.reject("NOTIFICATION_HOST_NOT_DEFINE");
    if (!params || !params.mobile || !params.message) return Promise.reject("INVALID_REQUEST");
    let options = {
        method: 'POST',
        uri: `${req.services.NOTIFICATION_HOST}/sms/sendsms`,
        body: {mobile: params.mobile, message: params.message, source:params.source,template_id:params.template_id, lead_id:params.lead_id,request_type:params.request_type},
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "mobile-country-code": req.headers['mobile-country-code'] },
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.sendMobileNotification = async (req, params)=>{
    if (!req.services || !req.services.NOTIFICATION_HOST) return Promise.reject("NOTIFICATION_HOST_NOT_DEFINE");
    if (!params || !params.user_id || !params.dealer_id) return Promise.reject("INVALID_REQUEST");
    let options = {
        method: 'POST',
        uri: `${req.services.NOTIFICATION_HOST}/fcm/send_notification`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "mobile-country-code": req.headers['mobile-country-code'] },
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.sendWhatsappNotification = async (req, params)=>{
    if (!req.services || !req.services.NOTIFICATION_HOST) return Promise.reject("NOTIFICATION_HOST_NOT_DEFINE");
    if (!params || !params.mobile || !params.template_name) return Promise.reject("INVALID_REQUEST");
    let dealer_language = (params.dealer_language) ? params.dealer_language : 'id';
    let options = {
        method: 'POST',
        uri: `${req.services.NOTIFICATION_HOST}/message/send_whatsapp_message`,
            body: {
                template_name: params.template_name, data: {
                    mobile: params.mobile, brandModelUrl: params.url, name: params.name, dealerMobile: params.dealer_mobile
                },
                source:params.source,template_id:params.template_id, lead_id:params.lead_id,request_type:params.request_type, dealerMobile:params.dealer_mobile
            },
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":dealer_language },
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.sendWhatsapp = async (req, params)=>{
    if (!req.services || !req.services.NOTIFICATION_HOST) return Promise.reject("NOTIFICATION_HOST_NOT_DEFINE");

    let options = {
        method: 'POST',
        uri: `${req.services.NOTIFICATION_HOST}/message/send_event_message`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services']},
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.sendSfaNotification = async (req, params)=>{
    if(!conf.SFA_URL_ID) return Promise.reject("SFA HOST NOT DEFINED");
    let dealer_language = (params.dealer_language) ? params.dealer_language : 'id';
    params.usertype = [1,2];
    let options = {
        method: 'POST',
        uri: `${conf.SFA_URL_ID}v2/notification-bro-bm`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":dealer_language, "key": "SFAUQPlnGed0dQ8iWosxptjSbUJOJ1NEQ", "source": "sfa"},
        json: true,
        simple: false
    };
    rp(options).then((response) => {
        return response;
    }).catch(err=>{
        // console.log('Error from sendSfaNotification*******', err);
    });
}


module.exports.sendEmailWithAttachments = async function (req,data) {
    let gatewayService = (req.headers['gateway-services']) ? JSON.parse(req.headers['gateway-services']):'';
    if (!gatewayService || !gatewayService.NOTIFICATION_HOST) return Promise.reject("NOTIFICATION_HOST_NOT_DEFINE");
    let gatewayuser  = (req.headers['gateway-user']) ?JSON.parse(req.headers['gateway-user']):"";

    let options = {            
        method: 'POST',
        uri: `${req.services.NOTIFICATION_HOST}/notification/sendmailattachement`,
        json: true,
        simple: false                
    };

    var req = rp(options);
    var form = req.form(); 
    
    form.append('sender', data.sender);
    form.append('receiver', data.receiver);
    if(data.cc) form.append('ccreceiver', data.cc);
    if(data.bcc) form.append('bccreceiver', data.bcc);
    form.append('mail_subject', data.mail_subject);
    form.append('mail_text', data.mail_text);
    form.append('mail_html', data.mail_html);
    form.append('mail_attachment', fs.createReadStream(data.mail_attachment));
    form.append('source', 'UB');
    form.append('user_id', gatewayuser.user_id || 0);
    form.append('user_type', gatewayuser.user_type || '');
    form.append('template_id', data.template_id || 0);
    form.append('lead_id', data.lead_id || 0);
    return req;
}

module.exports.urlShortener = async (params)=>{

    params = {...params, key: URL_SHORTNER_KEY};

    let options = {
        method: 'GET',
        uri: `${URL_SHORTNER_API}`,
        qs: params,
        json: true,
        simple: false
    };
    return rp(options);
}


module.exports.checkWANotificationSubscriptionStatus = async (params)=>{

    params = {...params, 'lang_code': process.env.APP_LANG, 'country_code': process.env.COUNTRY_CODE};

    let options = {
        method: 'GET',
        uri: `${WA_NOTIFICATION_CHECK_URL}`,
        qs: params,
        json: true,
        simple: false
    };
    // return rp(options);

    return { status: true, statusCode: 200, statusText: 'OK', data: true };
}