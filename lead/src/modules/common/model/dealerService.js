const rp = require('request-promise');

module.exports.getDealerDetailsByHash = async function (req, params) {   

    if (!req.services || !req.services.DEALER_HOST) return Promise.reject("DEALER_HOST_NOT_DEFINE");
    if (!params.dealer_id_hash) return Promise.reject("INVALID_REQUEST");
        let options = {
            method: 'POST',
            uri: `${req.services.DEALER_HOST}/dealer/details`,
            body: params,
            headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services']},
            json: true,
            simple: false
        };
        return rp(options);
}

module.exports.getDealerDetailsByDealerId = async function (req, params) { 

    if (!req.services || !req.services.DEALER_HOST) return Promise.reject("DEALER_HOST_NOT_DEFINE");
        let options = {
            method: 'POST',
            uri: `${req.services.DEALER_HOST}/cron/getDealerDetailsByDealerId`,
            body: params,
            headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services']},
            json: true,
            simple: false
        };
        return rp(options);
}

module.exports.getDealerDetailsByIds = async(req, params)=>{

    if (!req.services || !req.services.DEALER_HOST) return Promise.reject("DEALER_HOST_NOT_DEFINE");
    params._with = ["owner","city_id","city_name","address","gcd_code","dealer_status","state_id","state_name", "paid_score"];

    let options = {
        method: 'POST',
        uri: `${req.services.DEALER_HOST}/dealer/detailsById`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'], "apikey": UB_API_KEY},
        json: true,
        simple: false
    };
    return rp(options);
}

module.exports.getDealerList = async function (req) {

    if (!req.services || !req.services.DEALER_HOST) return Promise.reject("DEALER_HOST_NOT_DEFINE");
        let headers = req.headers['apiKey']?req.headers:{"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'], "apikey": UB_API_KEY}
        
        let queryString = {_with: ['id', 'organization', 'city_id', "address", "gcd_code", "ac_manager", "booking", "quota", "bro_bm"]};
        
        if(req.query['page_no']){
            queryString = {
                ...queryString,
                page_no: req.query['page_no'] || null,
                record_per_page: req.query['record_per_page'] || null
            };
        }

        if(req.query['filter']){
            queryString = { ...queryString,
                filter: req.query['filter'] || null
            };
        }
        let options = {
            method: 'GET',
            uri: `${req.services.DEALER_HOST}/dealer/list-all`,
            headers: headers,
            qs: queryString,
            json: true,
            simple: false
        };
        return rp(options);
}

module.exports.getDealerDetailsByIdsForReport = async(req, params)=>{

    if (!req.services || !req.services.DEALER_HOST) return Promise.reject("DEALER_HOST_NOT_DEFINE");
    params._with = ["owner","city_id","city_name","address","gcd_code","dealer_status","state_id","state_name", "ac_manager", "booking", "quota", "bro_bm", "created_date", "paid_score"];
    let options = {
        method: 'POST',
        uri: `${req.services.DEALER_HOST}/dealer/detailsById`,
        body: params,
        headers: {"gateway-user": req.headers['gateway-user'], "accept-language":req.headers['accept-language'], "gateway-services": req.headers['gateway-services'], "apikey": UB_API_KEY},
        json: true,
        simple: false
    };
    return rp(options);
}