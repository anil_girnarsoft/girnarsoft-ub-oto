const CommonHelper = require(HELPER_PATH + 'CommonHelper');
const crypto = require("../../../lib/crypto");
const axios = require('axios');

class ApiController {

    constructor() {
        
    }

}

ApiController.encode = (text) => { return crypto.encode(text) };
ApiController.decode = (text) => { return crypto.decode(text) };

ApiController.sendSuccessResponse = function (req, res, data = false, msg = null) {
    CommonHelper.sendSuccessResponse(req, res, data, msg);
}

ApiController.sendPaginationResponse = function (req, res, data, pagination) {
    CommonHelper.sendPaginationResponse(req, res, data, pagination);
}

ApiController.sendErrorResponse = function (req, res, errors = false) {
    CommonHelper.sendErrorResponse(req, res, errors);
}

ApiController.sendResponse = function (req, res, status, message = false, data = false, errors = false) {
    CommonHelper.sendResponse(req, res, status, message, data, errors);
}

ApiController.formatError = function (errorCode, errorFieldName) {
//    let error = ErrorHelper.formatError(errorCode, errorFieldName);
//    return error;
}


axios.defaults.headers.common['apikey'] = 'e8fe3567-00cd-4121-b34b-495c797526a4'

ApiController.get = async (url, param) => {
    let resp = await axios.get(url,  {
                    params: param
                });

    return resp;
}

ApiController.post = async (url, param) => {
    let resp = await axios.post(url,  param);

    return resp;
}

module.exports = ApiController;