const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class ConversationModel extends Model{
}

ConversationModel.init({
    conv_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    leads_cars_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    status_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    sub_status_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    calling_status_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0
    },
    source_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    sub_source_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0
    },
    dealer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    call_later_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    rating_value:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    purchase_intention: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    comment:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    followup_reason: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    dialer_fail_reason: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    added_on: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    updated_on: {
        type:dbConfig.Sequelize.DATE,
        allowNull: true
    },
    dialer_call_duration:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    dialer_hold_time:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    dialer_talk_time:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    dialer_nextcall_time:{
        type:dbConfig.Sequelize.DATE,
        allowNull: true
    },
    dialer_wrapup_time:{
        type:dbConfig.Sequelize.STRING,
        allowNull: true
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    is_dialer_count: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    dc_user_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    dc_user_type: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    is_proccess: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CONVERSATION,
    freezeTableName: true
});


ConversationModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await ConversationModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

ConversationModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await ConversationModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


ConversationModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.added_on = new Date().toISOString();
    else
        dataModel.updated_on = new Date().toISOString();

        return dataModel;
}


ConversationModel.getSumBooked = async(params, srhLeadCarConditions, leadsCondition, leadsJoin, usedCarIds) =>{
    return new Promise((resolve, reject)=>{
        let sql = "select *, count(distinct l.leads_cars_id) as sumBooked from "+ UBLMS_CONVERSATION + " l "+leadsJoin+" where l.status_id = 6 and l.sub_status_id = 26 "+srhLeadCarConditions+" "+leadsCondition +" and l.leads_cars_id IN ('"+(usedCarIds.join("','"))+"') group by l.dealer_id";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

module.exports = ConversationModel;