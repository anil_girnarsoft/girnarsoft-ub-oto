const Joi = require('@hapi/joi')

module.exports = {
    createRole: Joi.object().keys({
        name: Joi.string().required(),
        description: Joi.string().optional(),
        status: Joi.string().valid('0', '1').optional()
    }),
    updateRole: Joi.object().keys({
        id: Joi.number().required(),
        name: Joi.string().required(),
        description: Joi.string().optional(),
        status: Joi.string().valid('0', '1').optional()
    }),
    updateStatus: Joi.object().keys({
        id: Joi.number().required(),
        status: Joi.string().valid('0', '1').required()
    }),
    getList: Joi.object().keys({
        id: Joi.number().required(),
        _with: Joi.array().items(Joi.string().required()).optional()
    }).unknown(true)
}