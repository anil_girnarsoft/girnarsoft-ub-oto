const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class SubStatusModel extends Model{
}

SubStatusModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    status_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    slug_name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    status: {
        type:dbConfig.Sequelize.ENUM('0', '1','2'),
        allowNull: false,
        defaultValue: '1'
        
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_SUB_STATUS,
    freezeTableName: true
});


SubStatusModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await SubStatusModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

SubStatusModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await SubStatusModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

SubStatusModel.get = async (_where,pagination) => {
    let paginationRequired = (typeof _where['pagination'] !== undefined) ? _where['pagination'] : true;  
    let whereCond = await SubStatusModel.bindCondition(_where,pagination,paginationRequired)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS uss.id,uss.name,uss.status_id,uss.slug_name,uss.created_at,uss.updated_at,uss.added_by,uss.updated_by,uss.status,us.name as status_name FROM "+UBLMS_SUB_STATUS+" uss LEFT JOIN "+UBLMS_STATUS+" us ON us.id = uss.status_id  "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await SubStatusModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

SubStatusModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if(params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
    if(params.name)
        dataModel.name = params.name;
    if(params.slug_name)
        dataModel.slug_name = params.slug_name;
    if(params.added_by)
        dataModel.added_by = params.added_by
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
        return dataModel;
}

SubStatusModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '',
        paginationCond = '';
    if(params.isPaginationRequired != false)
        paginationCond = await SubStatusModel.bindPagination(params,pagination,isPaginationApply);
    if(params && params.name)
        condition = "where uss.name like '%" + params.name + "%'";
    
    let st  = params.hasOwnProperty('status') ? params.status : 1 
    let status = "uss.status='"+st+"'";
    condition = (condition == '')? condition + "where "+status : condition+" and "+status 
        
    if(params && params.id){
        let id = "uss.id='"+ SubStatusModel.decrypt(params.id)+"'";
        condition = (condition == '')? condition + "where "+id : condition+" and "+id 
    }
    if(params && params.status_id && !Array.isArray(params.status_id)){
        let status_id = "uss.status_id='"+ params.status_id+"'";
        condition = (condition == '')? condition + "where "+status_id : condition+" and "+status_id 
    } else if(params && params.status_id && Array.isArray(params.status_id)){
        let status_id = "uss.status_id IN ('"+ (params.status_id).join("','")+"')";
        condition = (condition == '')? condition + "where "+status_id : condition+" and "+status_id 
    }
    if(params && params.created_at){
        let inputDate = dateFormat(params.created_at, "yyyy-mm-dd");
        let datetCond = "date_format(uss.created_at,'%Y-%m-%d') <= '" + inputDate+ "'"
        condition = (condition == '')? condition + "where "+datetCond : condition+" and "+datetCond 
    }
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

SubStatusModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

SubStatusModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

SubStatusModel.encrypt = (data) => {
    if(Array.isArray(data)) data = data.map(v => {
        if(v && v.id != null) v.sub_status_id_hash = crypto.encode(v.id);
        return v;
    })
    else if (data && data.id) data.sub_status_id_hash = crypto.encode(data.id);
    return data;
}

SubStatusModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

module.exports = SubStatusModel;