const express = require('express');
const router = express.Router();
const schemas = require('./validation_schema');
const validation = require(MIDDLEWARE_PATH + 'validation');
const StatusController = require('./controller/statusController');

router.post('/listStatus', StatusController.getStatusList);
router.post('/listSubStatus', StatusController.getSubStatusList);
router.post('/listCallStatus', StatusController.getCallStatusList);

module.exports = router;