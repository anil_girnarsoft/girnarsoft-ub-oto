const Q = require("q");
const _ = require("lodash");
const statusModel = require('./../model/statusModel');
const subStatusModel = require('./../model/subStatusModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../validation_schema");
const subStatusSchemas = require("../sub_status_validation_schema");
const callingStatusModel = require('../model/callStatusModel');


exports.getStatusList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await statusModel.get(value,page_number);
    let roleList = statusModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, roleList,pagination);
  }catch (error) {
    next(error);
  }
}

exports.getSubStatusList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = subStatusSchemas.getList.validate(req.body);
    let pagination = {};
    let data = await subStatusModel.get(value,page_number);
    let roleList = subStatusModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, roleList,pagination);
  }catch (error) {
    next(error);
  }
}

exports.getCallStatusList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let  value  = (req.body);
    let pagination = {};
    let data = await callingStatusModel.get(value,page_number);
    let listData = callingStatusModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, listData,pagination);
  }catch (error) {
    next(error);
  }
}







