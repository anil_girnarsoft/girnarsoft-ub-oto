const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const { param } = require("express-validator");
class WalkinInfoModel extends Model{
}

WalkinInfoModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    leads_cars_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    walkin_datetime:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    walkin_feedback:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
    },
    walkin_status:{
        type: dbConfig.Sequelize.ENUM('1','2','3','4'),
        allowNull: true
    },
    offer_price:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    user_type:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    api_flag:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    is_whatsapp_notified:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    status: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:1
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_WALKING_INFO,
    freezeTableName: true
});


WalkinInfoModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkinInfoModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

WalkinInfoModel.updateOne = async (id, data) => { 
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkinInfoModel.update(data, { where: { id: id } });
 
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


WalkinInfoModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if((params.status))
        dataModel.status = params.status;
    if(params.added_by)
        dataModel.added_by = params.added_by
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
        return dataModel;
}

WalkinInfoModel.inactiveWalkin = async (mainLeadId, subLeadId) => {
    let data = {
        'updated_at' : new Date().toISOString(),
        'status':2
    }
    let _where = {
        'lead_id':mainLeadId,
        'leads_cars_id' : subLeadId
    }

    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const res_data = await WalkinInfoModel.update(data, { where: _where });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

WalkinInfoModel.updateWalkinInfoData = async(data) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select id, leads_cars_id, lead_id,walkin_status from " + UBLMS_WALKING_INFO + "\n"+
        "where lead_id = '" + data['lead_id'] + "' and leads_cars_id = '" + data['leads_cars_id'] + "' and walkin_status ='" + data['walkin_status'] + "'  order by id desc LIMIT 0,1  ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async(walkinData)=>{
            let resp=[]
            if(walkinData && walkinData.length){
                let obj ={
                    'status' : '2',
                    'updated_at': new Date().toISOString()
                }
                resp = await WalkinInfoModel.updateOne(walkinData['0']['id'],obj)
            }
            resolve(resp || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

WalkinInfoModel.getWalkinInfoData = async(params) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select id from " + UBLMS_WALKING_INFO  + "\n"+
        "where lead_id = " + params.lead_id + " and leads_cars_id = " +params.leads_cars_id+" and  walkin_datetime = '" +params.walkin_datetime + "' and  walkin_status = " + params.walkin_status + " and status = 1 order by id ASC ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

WalkinInfoModel.getWalkingInfo = async(leads_cars_id) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select * from " + UBLMS_WALKING_INFO  + "\n"+
        "where leads_cars_id = " +leads_cars_id+"  ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

WalkinInfoModel.getWalkinCount = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let totalWalkin = 0;
        let sql = "SELECT count(id) as totalWalkin from "+ UBLMS_WALKING_INFO +" where lead_id = " +leadId + " and walkin_status = 4 and status = 1";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            if((result && result.length) > 0) {
                totalWalkin = result[0]['totalWalkin'];
            }
            resolve(totalWalkin);
        }).catch(error=>{
            reject(error);
        });
    })
}

WalkinInfoModel.getWalkinCountsss = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select id , leads_cars_id, lead_id, walkin_datetime, walkin_status  from  " + UBLMS_WALKING_INFO + " where lead_id = '" +leadId + "' order by id desc limit 0,2";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

WalkinInfoModel.getWalkinFeedback = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT id, walkin_feedback, walkin_feedback_point FROM " + UBLMS_WALKIN_FEEDBACK + " where status = 1  order by feedback_order";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

WalkinInfoModel.getSumWalkinSchedule = async(params,srhLeadCarConditions,leadsCondition,leadsJoin,usedCarJoin) =>{
    return new Promise((resolve, reject)=>{
        let sql = `SELECT 
            COUNT(DISTINCT w.lead_id) AS sumWalkinSchedule
        FROM
            ${UBLMS_WALKING_INFO} w
            INNER JOIN ${UBLMS_LEADS_CARS} l ON w.lead_id = l.lead_id
            ${leadsJoin}
            INNER JOIN ${UBLMS_DEALER_PRIORITY} dc on l.car_id=dc.car_id
        WHERE
            w.walkin_status IN (2 , 3) AND w.status = 1 AND dc.dealer_id IN (${params.dealerIds.join(',')}) ${srhLeadCarConditions} ${leadsCondition} `;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

WalkinInfoModel.getSumWalkinDone = async(params,srhLeadCarConditions,leadsCondition,leadsJoin,usedCarJoin) =>{
    return new Promise((resolve, reject)=>{
        let sql = `SELECT 
        COUNT(DISTINCT w.lead_id) AS sumWalkinDone
    FROM
        ${UBLMS_WALKING_INFO} w
        INNER JOIN ${UBLMS_LEADS_CARS} l ON w.lead_id = l.lead_id
        ${leadsJoin}
        INNER JOIN ${UBLMS_DEALER_PRIORITY} dc on l.car_id=dc.car_id
    WHERE
        w.walkin_status IN (4) AND w.status = 1 AND dc.dealer_id IN (${params.dealerIds.join(',')}) ${srhLeadCarConditions} ${leadsCondition}
    GROUP BY l.dealer_id`;

        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}
WalkinInfoModel.LastWalkinDeatils = async(leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select id, leads_cars_id, lead_id, walkin_datetime, walkin_status  from  "+ UBLMS_WALKING_INFO +" where lead_id = "+leadId+" order by id desc limit 0,2";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}


WalkinInfoModel.getWalkingInfoByLeadCarId = async(leads_cars_id) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select * from " + UBLMS_WALKING_INFO  + "\n"+
        "where leads_cars_id = " +leads_cars_id+" order by id DESC limit 1";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

/**
 * 
 * @param {*} params 
 * 4 Hours Before schedule cars
 */
WalkinInfoModel.getWalkingInfoForNotification = async(params) =>{
    return new Promise((resolve, reject)=>{
        let limit = params.limit ? " LIMIT "+params.limit : "";
        let sql = "select max(uwi.id) id, uwi.leads_cars_id, uwi.lead_id, uwi.walkin_datetime,uwi.is_whatsapp_notified, ulc.car_id, uwi.status from " + UBLMS_WALKING_INFO  + " as uwi LEFT JOIN "+UBLMS_LEADS_CARS+" ulc ON ulc.id = uwi.leads_cars_id WHERE (uwi.walkin_datetime BETWEEN NOW() AND DATE_ADD(NOW(),INTERVAL 5 HOUR)) AND uwi.status=1 AND uwi.is_whatsapp_notified=0 AND uwi.id IN (SELECT max(id) from ublms_walking_info group by leads_cars_id) GROUP BY uwi.leads_cars_id ORDER BY uwi.leads_cars_id DESC "+limit;

        // "WHERE CURDATE() = DATE(uwi.walkin_datetime) AND NOW() <= DATE_SUB(uwi.walkin_datetime,INTERVAL 4 HOUR) AND uwi.status=1 AND uwi.is_whatsapp_notified=0 GROUP BYuwi.leads_cars_id ORDER BY uwi.leads_cars_id DESC "+limit;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}




module.exports = WalkinInfoModel;