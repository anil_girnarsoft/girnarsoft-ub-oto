const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class WalkinFeedbackDealerInfoModel extends Model{
}

WalkinFeedbackDealerInfoModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    lead_car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    dealer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    walkin_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    walkin_dealer_msg_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    status:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:1
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
    
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_WALKIN_FEEDBACK_DEALER_INFO,
    freezeTableName: true
});


WalkinFeedbackDealerInfoModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkinFeedbackDealerInfoModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

WalkinFeedbackDealerInfoModel.updateOne = async (_where, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkinFeedbackDealerInfoModel.update(data, { where: _where });
            
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


WalkinFeedbackDealerInfoModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if((params.status))
        dataModel.status = params.status;
    if(params.added_by)
        dataModel.added_by = params.added_by
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
        return dataModel;
}

WalkinFeedbackDealerInfoModel.getWalkingFeedbackInfo = async(leads_cars_id,walkin_id) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select lead_id, lead_car_id, dealer_id, walkin_id, walkin_dealer_msg_id,\n"+
        "status, created_at, updated_at from " + UBLMS_WALKIN_FEEDBACK_DEALER_INFO  + "\n"+
        "where lead_car_id = '"+leads_cars_id+"' and walkin_id = '"+walkin_id+"'  ";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}




module.exports = WalkinFeedbackDealerInfoModel;