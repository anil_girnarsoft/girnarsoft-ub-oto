const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class WalkinPurchaseInfoModel extends Model{
}

WalkinPurchaseInfoModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    data:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    status:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:1
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_WALKIN_PURCHASE_INFO,
    freezeTableName: true
});


WalkinPurchaseInfoModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkinPurchaseInfoModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

WalkinPurchaseInfoModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkinPurchaseInfoModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


WalkinPurchaseInfoModel.getWalkinPurchaseInfoData = async(leadCarId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select id,lead_car_id, data, status, created_at from "+UBLMS_WALKIN_PURCHASE_INFO+" where lead_car_id = '"+leadCarId+"' order by id DESC limit 1";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}


module.exports = WalkinPurchaseInfoModel;