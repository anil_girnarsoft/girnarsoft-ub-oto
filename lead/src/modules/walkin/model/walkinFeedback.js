const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class WalkinFeedback extends Model{
}

WalkinFeedback.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    walkin_feedback:{
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue:""
    },
    walkin_feedback_point:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    status:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    feedback_order:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    }
    
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_WALKIN_FEEDBACK,
    freezeTableName: true
});


WalkinFeedback.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkinFeedback.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

WalkinFeedback.updateOne = async (_where, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkinFeedback.update(data, { where: _where });
            
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


WalkinFeedback.getModel = (params) => {
    let dataModel = {};

    if((params.status))
        dataModel.status = params.status;
    
        return dataModel;
}

WalkinFeedback.get = async() =>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT id, walkin_feedback, walkin_feedback_point FROM " + UBLMS_WALKIN_FEEDBACK + " where status = 1  order by feedback_order";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}




module.exports = WalkinFeedback;