const Q = require("q");
const _ = require("lodash");
const carMakeModel = require('./../model/carMakeModel');
const CarMake = require('./../model/carMake');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../validation_schema");


exports.getMakeModelList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await carMakeModel.get(value,page_number);
    let listData = carMakeModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, listData,pagination);
  }catch (error) {
    next(error);
  }
}

exports.getCarMake = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await CarMake.get(value,page_number);
    let listData = CarMake.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, listData,pagination);
  }catch (error) {
    next(error);
  }
}





