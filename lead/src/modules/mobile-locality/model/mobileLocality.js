const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const md5 = require('md5');
const crypto = require("../../../lib/crypto");
class MobileLocality extends Model{
}

MobileLocality.init({
    mobile_locality_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    mobile_locality_no: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    mobile_locality_code: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    mobile_telecom_circle: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    city_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    }
    
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_MOBILE_LOCALITY,
    freezeTableName: true
});



MobileLocality.getModel = (params) => {
    let dataModel = {};

    if(params.mobile_locality_id)
        dataModel.mobile_locality_id = params.mobile_locality_id;
    if(params.mobile_locality_no)
        dataModel.mobile_locality_no = params.mobile_locality_no
    if(params.mobile_locality_code)
        dataModel.mobile_locality_code = params.mobile_locality_code;
    if(params.mobile_telecom_circle)
        dataModel.mobile_telecom_circle = params.mobile_telecom_circle;
    if(params.city_id)
        dataModel.city_id = params.city_id;
    return dataModel;
}

MobileLocality.get = async (_where,pagination) => {

    let whereCond = await MobileLocality.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM "+UBLMS_MOBILE_LOCALITY+"\n\
        "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await MobileLocality.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

MobileLocality.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
        paginationCond = '';
        
    if(params.isPaginationRequired != false)
        paginationCond = await MobileLocality.bindPagination(params,pagination,isPaginationApply);
    if(params && params.name)
        condition = "where name like '%" + params.name + "%'";
    
    if(params && params.mobile_locality_id){
        let id = "mobile_locality_id='"+ MobileLocality.decrypt(params.mobile_locality_id)+"'";
        condition = (condition == '')? condition + "where "+id : condition+" and "+id 
    }
    if(params && params.mobile_locality_no){
        let mobileLocalityNo = "mobile_locality_no='"+ params.mobile_locality_no +"'";
        condition = (condition == '')? condition + "where "+mobileLocalityNo : condition+" and "+mobileLocalityNo 
    }
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

MobileLocality.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

MobileLocality.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

MobileLocality.encrypt = (data) => {
    if(Array.isArray(data)) data = data.map(v => {
        if(v && v.mobile_locality_id != null) v.car_make_id_hash = crypto.encode(v.mobile_locality_id);
        return v;
    })
    else if (data && data.mobile_locality_id) data.car_make_id_hash = crypto.encode(data.mobile_locality_id);
    return data;
}

MobileLocality.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}
module.exports = MobileLocality;