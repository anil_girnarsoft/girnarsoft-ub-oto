const express = require('express');
const router = express.Router();
const schemas = require('./validation_schema');
const validation = require(MIDDLEWARE_PATH + 'validation');
const ClusterController = require('./controller/clusterController');

router.post('/listCluster', ClusterController.getClusterList);
router.post('/saveCluster',validation(schemas.createCluster, 'body'), ClusterController.saveCluster);
router.post('/editCluster',validation(schemas.updateCluster, 'body'),ClusterController.saveCluster);
router.post('/updateStatus',validation(schemas.updateStatus, 'body'),ClusterController.updateStatus);

module.exports = router;