const Q = require("q");
const _ = require("lodash");
const clusterModel = require('./../model/clusterModel');
const cityClusterMappingModel = require('./../model/cityClusterMappingModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const schemas = require("../validation_schema");
const dbConfig = require(CONFIG_PATH + 'db.config');
const sequelize = dbConfig.sequelize;
const cityController = require('../../city/controller/cityController');
const { param } = require("../../lead/route");

exports.saveCluster = async (req, res, next)=>{
  try {
    const t = await sequelize.transaction();
    const userId = _.get(req.headers, 'userid');
    let params = _.cloneDeep(req.body);
    let id = params.id;
    let cluster_data;
    let isCityExist =  await cityClusterMappingModel.getOne(req.body.city_id,id);
    if(isCityExist)
      throw new Error('City already used in other cluster')
    if(!id){
      let isSameNameExist = await clusterModel.findOne({where:{'name':params.name}})
      if(isSameNameExist) throw new Error('NAME_SHOULD_BE_UNIQUE')
      params.added_by = userId;
      let clusterObj = clusterModel.getModel(params)
      clusterModel.createOne(clusterObj,t)
      .then(response =>{
        cluster_data = response.dataValues;
        if(cluster_data.id > 0){
          params.cluster_id = cluster_data.id;
          let city_cluster_data = cityClusterMappingModel.getModel(params);
          return cityClusterMappingModel.bulkInsert(city_cluster_data,t)
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .then(response =>{
        if(response.length){
          t.commit();
          ApiController.sendSuccessResponse(req, res, cluster_data,'cluster_created_successfully');
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .catch(err => {
        t.rollback();
        ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
      })
      
    }else{
      params.updated_by = userId;
      let clusterObjToUpdate = clusterModel.getModel(params);
      clusterModel.updateOne(id,clusterObjToUpdate,t)
      .then(async (response) => {
        if(response && response.length){
          let res = await cityClusterMappingModel.deleteCityFromCluster([params.id])
          let cityClusterObjToMapp = cityClusterMappingModel.getModel(params);
          return cityClusterMappingModel.bulkInsert(cityClusterObjToMapp,t)
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .then(response => {
        if(response && response.length){
          t.commit();
          ApiController.sendResponse(req, res,200,'cluster_updated_successfully');
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        next(error);
      })
      
      
    }
  } catch (error) {
      next(error);
  }
  
}

exports.getClusterList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await clusterModel.get(value,page_number);
    let clusterList = clusterModel.encrypt(data[0]);

    let clusterData = [];
    req.body['cb_function'] = true;

    let cityList = await cityController.getCityStateList(req, res, next);
  
    if(data[0].length){
      clusterData = data[0].map(el=>{
        let cityName = '';
        let cityObj = [];

        if(cityList && cityList['city']){
          let cityIds = (el.city_id).split(',');
              cityIds.forEach(elm=>{
                (cityList['city']).forEach(ct => {
                     if(ct.id == elm){
                        cityName +=  ((cityName.length) ? ',' : '') + ct.name;
                        cityObj.push({'value': ct.id, 'label': ct.name});
                     }
                });
              });
        }
        
              el.city = cityName;
              el.cityObj = cityObj;
              return el;
      });
    }
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    if(params.returnResponse){
      return clusterList;
    }else{
      ApiController.sendPaginationResponse(req, res, clusterList,pagination);
    }
  }catch (error) {
    next(error);
  }
}

exports.updateStatus = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    let id = req.body.id;
    let statusObj = clusterModel.getModel(params)
    clusterModel.updateOne(id,statusObj)
    .then(response => {
      if(response && response.length){
        ApiController.sendResponse(req, res,200,'status_updated_successfully');
      }else{
        ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
      }
    })
    .catch(error => {
      next(error);
    })
  } catch (error) {
      next(error);
  }
  
}



