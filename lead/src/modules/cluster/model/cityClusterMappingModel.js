const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
class CityClusterModel extends Model{
}

CityClusterModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    city_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    cluster_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    status: {
        type:dbConfig.Sequelize.ENUM('0', '1'),
        allowNull: true,
        defaultValue: '1'
        
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CITY_CLUSTER,
    freezeTableName: true
});

CityClusterModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await CityClusterModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

CityClusterModel.bulkInsert = async (data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const city_data = await CityClusterModel.bulkCreate(data,{returning: true},{ transaction: t });
            resolve(city_data);
        } catch (error) {
            reject(error)
        }
    })
}

CityClusterModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await CityClusterModel.update(data, { where: { cluster_id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

CityClusterModel.get = async (_where,pagination) => {

    let whereCond = await CityClusterModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS id,name,description,created_at,updated_at,added_by,updated_by,status FROM "+UBLMS_CITY_CLUSTER+"\n\
        "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await CityClusterModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

CityClusterModel.getModel = (params) => {
    let dataModel = {},retArr=[];
    if(params.city_id.length){
        _.forEach(params.city_id,(city) =>{
            dataModel = {};
            dataModel.cluster_id = (params.id) ? params.id : params.cluster_id;
            dataModel.created_at = new Date().toISOString();
            dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
            if(params.added_by)
                dataModel.added_by = params.added_by
            else
                dataModel.added_by = 1;
    
            dataModel.city_id = city;
            retArr.push(dataModel)
        })
    }
    return retArr;
}

CityClusterModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond =  await CityClusterModel.bindPagination(params,pagination,isPaginationApply);
    if(params && params.name)
        condition = "where name like '%" + params.name + "%'";
    
    if(params && params.hasOwnProperty('status')){
        let status = "status='"+params.status+"'";
        condition = (condition == '')? condition + "where "+status : condition+" and "+status 
    }
    if(params && params.id){
        let role_id = "id='"+params.id+"'";
        condition = (condition == '')? condition + "where "+role_id : condition+" and "+role_id 
    }
    if(params && params.created_at){
        let inputDate = dateFormat(params.created_at, "yyyy-mm-dd");
        let datetCond = "date_format(created_at,'%Y-%m-%d') <= '" + inputDate+ "'"
        condition = (condition == '')? condition + "where "+datetCond : condition+" and "+datetCond 
    }
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

CityClusterModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

CityClusterModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

CityClusterModel.getOne = async (cityIds,id) => {
    let condition = '';
    if(cityIds && cityIds.length)
        condition = "where city_id IN (" + cityIds.toString() + ")";
    if(id){
        let cluster_id = " AND cluster_id != "+id;
        condition = condition+cluster_id;
    }
    
    return new Promise(function (resolve, reject) {
        var sql = "SELECT count(id) as total FROM "+UBLMS_CITY_CLUSTER+"\n\
        "+condition;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await CityClusterModel.getRowsCount();
            resolve(result[0].total);
        }).catch(function (error) {
            reject(error);
        });
    });
}

CityClusterModel.deleteCityFromCluster = async (id) => {
    return await CityClusterModel.destroy({ where: { cluster_id: id }})
}

module.exports = CityClusterModel;