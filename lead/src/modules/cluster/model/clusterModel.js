const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class ClusterModel extends Model{
}

ClusterModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    status: {
        type:dbConfig.Sequelize.ENUM('0', '1'),
        allowNull: false,
        defaultValue: '1'
        
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CLUSTER,
    freezeTableName: true
});


ClusterModel.createOne = async (data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await ClusterModel.create(data,{transaction:t});
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

ClusterModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await ClusterModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

ClusterModel.get = async (_where,pagination,isPaginationApply=true) => {

    isPaginationApply = (typeof _where.isPaginationApply !== 'undefined') ? _where.isPaginationApply : isPaginationApply;

    let whereCond = await ClusterModel.bindCondition(_where,pagination,isPaginationApply)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {

        var sql= "SELECT cluster.id,cluster.name,cluster.description,cluster.added_by,cluster.created_at,cluster.updated_at,cluster.updated_by,"+"\n\
        cluster.status, ctyMap.id as cityMapId, GROUP_CONCAT(ctyMap.city_id) as city_id "+"\n\
        from "+UBLMS_CLUSTER+" as cluster INNER JOIN "+UBLMS_CITY_CLUSTER+"\n\
        as ctyMap on cluster.id=ctyMap.cluster_id"+"\n\
        "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await ClusterModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

ClusterModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if(params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
    if(params.name)
        dataModel.name = params.name;
    if(params.description)
        dataModel.description = params.description;
    if(params.added_by)
        dataModel.added_by = params.added_by
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
        return dataModel;
}

ClusterModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond =  await ClusterModel.bindPagination(params,pagination,isPaginationApply);
    let groupBy = 'GROUP BY ctyMap.cluster_id';
    let having = "";
    if(params && params.name)
        condition = "where cluster.name like '%" + params.name + "%'";

    if(params && params.city){
        having = " having find_in_set("+params.city+", city_id) " 
    }
    
    // if(params && params.city){
    //     let city = "ctyMap.city_id ='" + params.city + "'";
    //     condition = (condition == '')? condition + "where "+city : condition+" and "+city 
    // }
    if(params && params.city_ids){
        let city = "ctyMap.city_id IN ('" + params.city_ids.join("','") + "')";
        condition = (condition == '')? condition + "where "+city : condition+" and "+city 
    }
    if(params && params.hasOwnProperty('status')){
        let status = "cluster.status='"+params.status+"'";
        condition = (condition == '')? condition + "where "+status : condition+" and "+status 
    }
    if(params && params.id){
        let cluster_id = "cluster.id='"+ ClusterModel.decrypt(params.id)+"'";
        condition = (condition == '')? condition + "where "+cluster_id : condition+" and "+cluster_id 
    }
    if(params && params.cluster){
        let clusters = ((params.cluster).toString()).split();
            clusters = clusters.join("','"); 
        let cluster_id = "cluster.id IN ('"+ clusters +"')";
        condition = (condition == '')? condition + "where "+cluster_id : condition+" and "+cluster_id 
    }
    if(params && params.created_at){
        let inputDate = dateFormat(params.created_at, "yyyy-mm-dd");
        let datetCond = "date_format(created_at,'%Y-%m-%d') <= '" + inputDate+ "'"
        condition = (condition == '')? condition + "where "+datetCond : condition+" and "+datetCond 
    }
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }else{
        sortCond = 'ORDER BY updated_at desc';
    }
    return condition+" "+groupBy+" "+having+" "+sortCond+" "+paginationCond;
    // return condition+" "+groupBy+" "+sortCond;
}

ClusterModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

ClusterModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

ClusterModel.encrypt = (data) => {
    if(Array.isArray(data)) data = data.map(v => {
        if(v && v.id != null) v.cluster_id_hash = crypto.encode(v.id);
        return v;
    })
    else if (data && data.id) data.cluster_id_hash = crypto.encode(data.id);
    return data;
}

ClusterModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

ClusterModel.getClusterCity = (clusterIds) => {
    return new Promise((resolve, reject)=>{
        let sql = "SELECT city_id FROM "+UBLMS_CITY_CLUSTER+" WHERE cluster_id IN "+'('+clusterIds.toString()+')';
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

ClusterModel.getCityListAndCluster = async (params) => {
    return new Promise(function (resolve, reject) {
        let _where = '';
        if((params.cluster !== undefined) && (params.cluster.length)){
            _where = " where cluster.id IN ('"+ params.cluster.join("','") + "')";
        }
        if((params.cityId !== undefined) && (params.cityId.length)){
            _where += (_where) ? ' and ' : ' where ';
            _where += " city.city_id IN ('"+ params.cityId.join("','") + "')";
        }
        var sql = "SELECT cluster.name, cluster.id, group_concat(city.city_id) city_ids from "+UBLMS_CLUSTER+" cluster left join "+UBLMS_CITY_CLUSTER+" city ON cluster.id = city.cluster_id "+_where+" group by cluster.id";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            resolve(result);
        }).catch(function (error) {
            reject(error);
        });
    });
}

module.exports = ClusterModel;