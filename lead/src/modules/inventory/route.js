const express = require('express');
const router = express.Router();
const InventoryController = require('./controller/inventoryController');

router.get('/getfilter', InventoryController.getfilter);
router.post('/listInventoryStock', InventoryController.listInventoryStock);
router.post('/getCarDetails', InventoryController.getCarDetails);
router.get('/getMMVList', InventoryController.getMMVList);
router.get('/getStateCityList', InventoryController.getStateCityList);
router.post('/listInventoryStockByIds', InventoryController.listInventoryStockByIds);

module.exports = router;