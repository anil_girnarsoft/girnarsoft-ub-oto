const _ = require("lodash");
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const InventoryService    = require(COMMON_MODEL + 'inventoryService');
var async = require("async");
const LeadCarModel = require("../../lead/model/leadCarModel");
const DealerService = require(COMMON_MODEL + 'dealerService');
const ReportController = require('../../report/controller/reportController');
const DealerBoostModel = require('../../dealer/model/dealerBoostModel');
const { conversionPanelList } = require("../../lead/controller/leadController");

exports.getfilter = async (req, res, next) => {
  try {
    let result = await InventoryService.getFilterList(req);

    ApiController.sendResponse(req, res, 200, res.__(''), result && result['data'], '');


  } catch (error) {
    next(error);
  }
}

exports.listInventoryStock = async (req, res, next) => {
  try {
    let locality;
    let params = req.body;
    let city_ids = params.city;
        params['call_server'] =   'ub';
        params['car_status']  =   [1,2];
        params['classified']  =   '1';
        params['sort_on']     =   'score';
        if(params.body_type_id) {
          params['body_type']     =   params.body_type_id;
        }
    let parameter = {};  
    if(params.dealer_type != 'undefined' && params.dealer_type){
      parameter.dealerPriorityType = params.dealer_type;
      let boostDealerData = await DealerBoostModel.getDealerBoostInfo(parameter);
      let dealer_id = [];
      for(let i of boostDealerData) {
        dealer_id.push(i.dealer_id);
      }  
      let allDealerIds; 
      if(params.dealer_ids){        
        allDealerIds = params.dealer_ids.concat(dealer_id);
      }else{
        allDealerIds = dealer_id;
      }   
      
      params.dealer_ids = allDealerIds;
      params.user_type = "D";
    }    
    let result = await InventoryService.getStockList(req, params);
    let stockData = (result && result['data']) ? result['data'] : [];
    if (params.customer_locality) {
      let localityRes = await InventoryService.getLocalityByCityId(req, { city: city_ids });
      let lc = localityRes.data;
      locality = lc.filter(location => location.id == params.customer_locality);
    }

    if(stockData.length){
        let dealerIds = [];
        let carIds = [];
        let dealerBoostData;
        //LOOP THROUGH STOCKDATA
        async.forEach((stockData), async (el, callback) => { 
          dealerIds.push(el.dealer_id); //DEALER ID ARRAY
          carIds.push(el.id); //CAR ID ARRAY

          callback();
        },  async (err) => {
          dealerIds = (dealerIds.length) ? _.uniq(dealerIds) : []; //UNIQUE DEALERS
          
          if(dealerIds.length){

            let dealerDetails = await DealerService.getDealerDetailsByIds(req, {dealer_id: dealerIds}); //DEALER DETAILS
            
            let organicLeadsFind = await LeadCarModel.totalOrganicAndBackendLeads({'carsId':carIds}); //FIND ORGANIC BACKEND LEADS
            
            let totalUniqueLeadFind = await LeadCarModel.totalUniqueLeads({'carsId':carIds}); //FIND UNIQUE TOTAL LEADS
            
            let totalFTDFind = await LeadCarModel.totalFTD({'carsId':carIds}); //FIND TODAY LEADS
            if(dealerDetails['data'] && dealerDetails['data'].length){
            
              async.each(stockData, async (stock, callback) => { //loop through array
                
                
                let stockCounter = _.findIndex(stockData, stock);//FIND INDEX
                
                let detailDealer = _.find(dealerDetails['data'], function (d) { return stock.dealer_id == d.id; }); //FIND DEALER DETAIL OBJ
                if(detailDealer){
                  detailDealer.dealer_type = '';
                  let param = {};
                  
                  param.dealer = detailDealer.id;
                  dealerBoostData = await DealerBoostModel.getDealerBoostInfo(param);
                  if (dealerBoostData.length) {
                    let dealer_boost = _.find(dealerBoostData, function (d) { return detailDealer.id == d.dealer_id; });
                    if (dealer_boost.priority == 1) {
                      detailDealer.dealer_type = "Low";
                    } else if (dealer_boost.priority == 2) {
                      detailDealer.dealer_type = "Medium";
                    } else if (dealer_boost.priority == 3) {
                      detailDealer.dealer_type = "High";
                    }
                  }
                  let organicBackendLeads = _.find(organicLeadsFind, function (obl) { return  stock.id == obl.car_id; }); //FIND MATCHED ORGANIC BACKEND LEADS
                  
                  let totalUniqueLead = _.find(totalUniqueLeadFind, function (tul) { return  stock.id == tul.car_id; }); //FIND MATCHED ORGANIC BACKEND LEADS
                  
                  let totalFTD = _.find(totalFTDFind, function (ftd) { return  stock.id == ftd.car_id; }); //FIND MATCHED ORGANIC BACKEND LEADS
                  //console.log('-------detailDealer****************', detailDealer);
                  if (locality) {
                    detailDealer['dis'] = Math.round(calcCrow(detailDealer.latitude, detailDealer.longitude, locality[0].latitude, locality[0].longitude));
                  }
                  //console.log('-------detailDealer****************', detailDealer);
                  stockData[stockCounter]['dealer_detail'] = detailDealer || {};

                  //MAP BACKEND ORGANIC LEADS
                  stockData[stockCounter]['totalBackendLeads'] = (organicBackendLeads && organicBackendLeads['totalBackendLeads']) || '';
                  stockData[stockCounter]['totalOrganicLeads'] = (organicBackendLeads && organicBackendLeads['totalOrganicLeads']) || '';
                  stockData[stockCounter]['totalLeads'] = (organicBackendLeads ? organicBackendLeads['totalBackendLeads'] + organicBackendLeads['totalOrganicLeads'] : '');
                  
                  //MAP UNIQUELEADS & FTD LEAD COUNT
                  stockData[stockCounter]['totalUniqueLeadsCount'] = (totalUniqueLead ? totalUniqueLead['totalLeads'] : '0');
                  stockData[stockCounter]['ftdCount'] = (totalFTD ? totalFTD['ftd'] : '0');

                }
                
                
                callback();
                }, function(err) {

                  ApiController.sendPaginationResponse(req, res, stockData, (result && result['pagination'] || {}));

              });
            }else{

              ApiController.sendPaginationResponse(req, res, stockData, (result && result['pagination'] || {}));

            }
          }else{
            ApiController.sendResponse(req, res, 200, 'No records found', [], '');

          }
        });
    }else{
      
      ApiController.sendResponse(req, res, 200, 'No records found', [], '');
    }
  } catch (error) {
    next(error);
  }
}

exports.listInventoryStockByIds = async (req, res, next) => {
  try {
    let params = req.body;
        params['dealerId'] = params['dealer_ids'];
        
    let usedCarData = await ReportController.findUsedCars(req, params);

    ApiController.sendPaginationResponse(req, res, usedCarData['allUsedCars'], (usedCarData['totalCars'] || {}));

  } catch (error) {
    next(error);
  }
}

const calcCrow=(lat1, lon1, lat2, lon2) =>
    {
      var R = 6371; // km
      var dLat = toRad(lat2-lat1);
      var dLon = toRad(lon2-lon1);
      var lat1 = toRad(lat1);
      var lat2 = toRad(lat2);

      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c;
      return d;
    }
    function toRad(Value) 
    {
        return Value * Math.PI / 180;
    }

exports.getCarDetails = async (req, res, next) => {
  try{
    let params = req.body;
    
    let result = await InventoryService.getCarDetails(req,params);

    ApiController.sendResponse(req, res, 200, res.__(''), result && result['data'], '');
  }catch(error) {
    next(error);
  }
}

exports.getMMVList = async (req,res, next) => {
  try{
    let result = await InventoryService.getMMVList(req);
    ApiController.sendResponse(req, res, 200, res.__(''), result && result['data'], '');
  }catch(error) {
    next(error);
  }
}

exports.getStateCityList = async (req,res, next) => {
  try{
    let result = await InventoryService.getStateCityList(req);
    ApiController.sendResponse(req, res, 200, res.__(''), result && result['data'], '');
  }catch(error) {
    next(error);
  }
}