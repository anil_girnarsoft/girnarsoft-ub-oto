const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const { async } = require("q");
class CustomerRequirementModel extends Model{
}

CustomerRequirementModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    customer_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    feedback_type:{
        type:dbConfig.Sequelize.ENUM('0','1','2'),
        allowNull:false,
        defaultValue:'0'
    },
    budget: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    min_price: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    max_price: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    min_km:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true,
        defaultValue : '0'
    },
    max_km:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    min_year: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    max_year:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull:true
    },
    fuel: {
        type:dbConfig.Sequelize.STRING,
        allowNull: true
        
    },
    make_ids: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue : ''
    },
    model_ids: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    variant_ids: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    trustmark:{
        type:dbConfig.Sequelize.ENUM('0','1'),
        allowNull:false,
        defaultValue:'0'
    },
    seller_type:{
        type:dbConfig.Sequelize.ENUM('individual','dealer'),
        allowNull:true
    },
    owner:{ 
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    color: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    transmission:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    purpose: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    req_body_type:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    drive_car: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    min_avg_daily_travel_km: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    max_avg_daily_travel_km: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    seat_num: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    buyer_type: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    status:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull:false,
        defaultValue:1
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        defaultValue : new Date()
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CUSTOMER_REQUIREMENT,
    freezeTableName: true
});


CustomerRequirementModel.createOne = async (data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const req_data = await CustomerRequirementModel.create(data,{ transaction: t });
            resolve(req_data);
        } catch (error) {
            reject(error)
        }
    })
}

CustomerRequirementModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await CustomerRequirementModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


CustomerRequirementModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if(params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
    if(params.lead_id)
        dataModel.lead_id = params.lead_id;
    if(params.customer_id)
        dataModel.customer_id = params.customer_id;
    if(params.hasOwnProperty('feedback_type'))
        dataModel.feedback_type = params.feedback_type;        
    if(params.budget)
        dataModel.budget = params.budget;
    if(params.min_price)
        dataModel.min_price = params.min_price;
    if(params.max_price)
        dataModel.max_price = params.max_price;        
    if(params.min_km)
        dataModel.min_km = params.min_km;
        
    if(params.make_ids)
        dataModel.make_ids = params.make_ids;
    if(params.model_ids)
        dataModel.model_ids = params.model_ids;
    if(params.variant_ids)
        dataModel.variant_ids = params.variant_ids;        
    if(params.hasOwnProperty('trustmark'))
        dataModel.trustmark = params.trustmark;
    if(params.seller_type)
        dataModel.seller_type = params.seller_type;
    if(params.max_km)
        dataModel.max_km = params.max_km;        
    if(params.min_year)
        dataModel.min_year = params.min_year;
    if(params.max_year)
        dataModel.max_year = params.max_year;
    if(params.fuel)
        dataModel.fuel = params.fuel; 
    if(params.model_ids)
        dataModel.model_ids = params.model_ids;
    if(params.owner)
        dataModel.owner = params.owner;
    if(params.color)
        dataModel.color = params.color;       
    if(params.transmission)
        dataModel.transmission = params.transmission;
    if(params.purpose)
        dataModel.purpose = params.purpose;
    if(params.req_body_type)
        dataModel.req_body_type = params.req_body_type;
    if(params.drive_car)
        dataModel.drive_car = params.drive_car;
    if(params.min_avg_daily_travel_km)
        dataModel.min_avg_daily_travel_km = params.min_avg_daily_travel_km;
    if(params.max_avg_daily_travel_km)
        dataModel.max_avg_daily_travel_km = params.max_avg_daily_travel_km;
    if(params.seat_num)
        dataModel.seat_num = params.seat_num;
    if(params.buyer_type)
        dataModel.buyer_type = params.buyer_type;
    if(params.added_by)
        dataModel.added_by = params.added_by
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
        return dataModel;
}

CustomerRequirementModel.getCustReqDetails = async (customerId,leadId) => {

    return new Promise((resolve, reject)=>{
        let sql = "Select lead_id, customer_id, budget, max_km, min_year, max_year, fuel, model_ids, owner, color,\n"+
        "transmission, purpose, req_body_type, drive_car, min_avg_daily_travel_km, max_avg_daily_travel_km,\n"+
        "seat_num, buyer_type, created_at, status from " +UBLMS_CUSTOMER_REQUIREMENT +" where customer_id = '"+customerId+"' and lead_id = '"+leadId+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}


CustomerRequirementModel.saveRequirements = async (params,t) => {
    return new Promise(async (resolve, reject)=>{
        let sql = '';
        if(params['customer_id'] && params['lead_id']){
            sql = "Select id from " +UBLMS_CUSTOMER_REQUIREMENT +" where customer_id = '"+params['customer_id']+"' and lead_id = '"+params['lead_id']+"' order by id DESC limit 1";
            
            AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async result=>{
                if(result && result.length){
                    CustomerRequirementModel.updateOne(result[0].id, params);
                    resolve(result.length && result || []);
                }else{ 
                   let data =  await CustomerRequirementModel.createOne(params,t);
                   resolve(data)
                }
            }).catch(error=>{
                reject(error);
            });
        
        }else{
            let data =  await CustomerRequirementModel.createOne(params,t);
            resolve(data)
        }
    })
}


module.exports = CustomerRequirementModel;