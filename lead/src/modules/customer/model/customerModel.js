const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const { async } = require("q");
class CustomerModel extends Model{
}

CustomerModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    customer_city_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    customer_city_name:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true,
        defaultValue:''
    },
    customer_sub_city_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    customer_email:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    customer_mobile: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    alternate_email:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    alternate_mobile: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    whatsapp_mobile:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    language: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    customer_location_id:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    customer_location_name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    custmer_location_name_other:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    other_location_lat: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    other_location_lng:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    status: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: true
        
    },
    ndnc: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    mobile_true_location:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    budget: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    max_km:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    min_year: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    max_year:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull:true
    },
    fuel: {
        type:dbConfig.Sequelize.STRING,
        allowNull: true
        
    },
    model_ids: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    owner:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    color: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    transmission:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    purpose: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    req_body_type:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    drive_car: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    min_avg_daily_travel_km: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    max_avg_daily_travel_km: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    seat_num: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    buyer_type: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    is_email_sent_cron: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:2
        
    },
    otp_verified: {
        type:dbConfig.Sequelize.ENUM('0','1'),
        allowNull:false,
        defaultValue:'0'
    },
    finance_required: {
        type:dbConfig.Sequelize.ENUM('0','1'),
        allowNull:false,
        defaultValue:'0'
    },
    connecto_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    app_id:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    last_became_lead_on: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    calling_number:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_on: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    status: {
        type:dbConfig.Sequelize.ENUM('0', '1'),
        allowNull: false,
        defaultValue: '1'
        
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CUSTOMER,
    freezeTableName: true
});


CustomerModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await CustomerModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

CustomerModel.updateOne = async (id, data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await CustomerModel.update(data, { where: { id: id }},{ transaction: t  });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


CustomerModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_on = new Date().toISOString();
    if(params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
    if(params.name)
        dataModel.name = params.name;
    if(params.customer_city_id)
        dataModel.customer_city_id = params.customer_city_id;
    if(params.customer_city_name)
        dataModel.customer_city_name = params.customer_city_name;   
    if(params.customer_email)
        dataModel.customer_email = params.customer_email;
    if(params.customer_mobile)
        dataModel.customer_mobile = params.customer_mobile;
    if(params.alternate_email)
        dataModel.alternate_email = params.alternate_email;
    if(params.alternate_mobile)
        dataModel.alternate_mobile = params.alternate_mobile;
    if(params.whatsapp_mobile)
        dataModel.whatsapp_mobile = params.whatsapp_mobile;
    if(params.language)
        dataModel.language = params.language;
    if(params.customer_location_id)
        dataModel.customer_location_id = params.customer_location_id;
    if(params.customer_location_name)
        dataModel.customer_location_name = params.customer_location_name;
    if(params.custmer_location_name_other)
        dataModel.custmer_location_name_other = params.custmer_location_name_other;
    if(params.other_location_lat)
        dataModel.other_location_lat = params.other_location_lat;
    if(params.other_location_lng)
        dataModel.other_location_lng = params.other_location_lng;
    if(params.ndnc)
        dataModel.ndnc = params.ndnc; 
    if(params.mobile_true_location)
        dataModel.mobile_true_location = params.mobile_true_location;     
    if(params.budget)
        dataModel.budget = params.budget;
    if(params.min_price)
        dataModel.min_price = params.min_price;
    if(params.max_price)
        dataModel.max_price = params.max_price;        
    if(params.min_km)
        dataModel.min_km = params.min_km;
    if(params.make_ids)
        dataModel.make_ids = params.make_ids;
    if(params.variant_ids)
        dataModel.variant_ids = params.variant_ids;        
    if(params.hasOwnProperty('trustmark'))
        dataModel.trustmark = params.trustmark;
    if(params.seller_type)
        dataModel.seller_type = params.seller_type;
    if(params.max_km)
        dataModel.max_km = params.max_km;        
    if(params.min_year)
        dataModel.min_year = params.min_year;
    if(params.max_year)
        dataModel.max_year = params.max_year;
    if(params.fuel)
        dataModel.fuel = params.fuel; 
    if(params.model_ids)
        dataModel.model_ids = params.model_ids;
    if(params.owner)
        dataModel.owner = params.owner;
    if(params.color)
        dataModel.color = params.color;       
    if(params.transmission)
        dataModel.transmission = params.transmission;
    if(params.purpose)
        dataModel.purpose = params.purpose;
    if(params.req_body_type)
        dataModel.req_body_type = params.req_body_type;
    if(params.drive_car)
        dataModel.drive_car = params.drive_car;
    if(params.min_avg_daily_travel_km)
        dataModel.min_avg_daily_travel_km = params.min_avg_daily_travel_km;
    if(params.max_avg_daily_travel_km)
        dataModel.max_avg_daily_travel_km = params.max_avg_daily_travel_km;
    if(params.seat_num)
        dataModel.seat_num = params.seat_num;
    if(params.buyer_type)
        dataModel.buyer_type = params.buyer_type;
    if(params.is_email_sent_cron)
        dataModel.is_email_sent_cron = params.is_email_sent_cron;       
    if(params.otp_verified)
        dataModel.otp_verified = params.otp_verified;
    if(params.finance_required)
        dataModel.finance_required = params.finance_required;
    if(params.connecto_id)
        dataModel.connecto_id = params.connecto_id;
    if(params.app_id)
        dataModel.app_id = params.app_id;
    if(params.last_became_lead_on)
        dataModel.last_became_lead_on = params.last_became_lead_on;
    if(params.calling_number)
        dataModel.calling_number = params.calling_number;
    if(params.added_by)
        dataModel.added_by = params.added_by
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
        return dataModel;
}

CustomerModel.getOne = async _where => {
    return await CustomerModel.findOne({where: _where});
}

CustomerModel.getCustomerLead = async (param) => {

    return new Promise((resolve, reject)=>{
        let sql = "SELECT ul.`status_id`,ul.`sub_status_id`,ul.`id`" 
        + " FROM `" + UBLMS_CUSTOMER + "` AS uc INNER JOIN `"+UBLMS_LEADS+"` AS ul ON ul.`customer_id`=uc.`id`" 
        + " WHERE uc.`customer_mobile`='"+param['customerMobile']+"' ORDER BY ul.`id` DESC LIMIT 0,1";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

CustomerModel.getCustomerInfo = async (customer_mobile) => {

    return new Promise((resolve, reject)=>{
        let sql = "Select c.id as customer_id, l.id as lead_id, l.status_id, l.sub_status_id, l.calling_status_id from "
        +UBLMS_CUSTOMER +" c INNER JOIN "+UBLMS_LEADS+" l ON c.id = l.customer_id where \n"+
        "c.customer_mobile = '"+customer_mobile+"' and l.status_id != 7 and l.sub_status_id != '12'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

CustomerModel.getCustDetails = async (customer_id) => {

    return new Promise((resolve, reject)=>{
        let sql = "Select name, customer_city_id, customer_city_name, customer_email, customer_mobile,\n"+
        "alternate_email, alternate_mobile,whatsapp_mobile, language, customer_location_id, customer_location_name,\n"+
        "custmer_location_name_other, other_location_lat, other_location_lng, status, ndnc, mobile_true_location,\n"+
        "budget, max_km, min_year, max_year, fuel, model_ids, owner, color, transmission, purpose,\n"+
        "req_body_type, drive_car, min_avg_daily_travel_km, max_avg_daily_travel_km, seat_num, buyer_type,\n"+
        "updated_on, is_email_sent_cron, updated_by,created_at from " +UBLMS_CUSTOMER +" where id = '"+customer_id+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result.length && result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}

CustomerModel.isCustomerMobileVerified = async (mobile) => {

    return new Promise((resolve, reject)=>{
        let resp = false;
        let sql = "SELECT count(*) as totalRec FROM "+UBLMS_OTP+" where mobile = '" +mobile + "' and is_verified = '1'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            if(result && result.length && result['0']['totalRec'] > 0) {
                resp = true;
            }
            resolve(resp);
        }).catch(error=>{
            reject(error);
        });
    })
}

CustomerModel.getbudgetandlocation = async (mobile) => {
    return new Promise((resolve, reject)=>{
        let sql = "SELECT l.budget,l.is_finance_req,c.name,c.customer_email,c.customer_location_id,c.id,l.lead_offline_score,l.lead_online_score, l.status_id,l.due_date AS followup_date, c.model_ids,c.fuel,c.transmission,c.req_body_type from "+UBLMS_LEADS+" l left join "+UBLMS_LEADS_CARS+" lc on lc.lead_id = l.id left join "+UBLMS_CUSTOMER+" c on c.id =l.customer_id  where c.customer_mobile = ? order by lc.id desc limit 1";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [mobile] })
        .then(result=>{
            resolve(result[0]);
        }).catch(error=>{
            reject(error);
        });
    });
}

CustomerModel.getCustomerLocationByCityId = async (cityId) => {

    return new Promise((resolve, reject)=>{
        let condition = "1=1";
        if ((cityId)) {
            condition += " and city_id = '" + +cityId + "'";
        }
        let sql = "SELECT lcl.name, lc.id FROM " + UBLMS_LOCALITY + " lc \n"+
        "INNER JOIN " + UBLMS_LOCALITY_LANG + " lcl ON lcl.locality_id = lc.id and lcl.lang_id = '1' \n"+
        " WHERE " +condition + " order by name ASC"
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

// CustomerModel.update = async (params, id) => { console.log('params', params)
//     try {
//         let allKeys = Object.keys(params);
//         let columns = allKeys.join("=?,");
//         let allValues = Object.values(params);
//         allValues.push(id);   
//         let sql = `update ${UBLMS_CUSTOMER} set ${columns} where id=?`;
//         console.log(columns,'------', allValues)
//         return new Promise((resolve, reject) => {
//             AppModel.dbObj.sequelize
//                 .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.UPDATE, replacements: allValues })
//                 .then(updateCust => {
//                     resolve(updateCust);
//                 }).catch(err => {
//                     reject(err);
//                 });
//         });

//     } catch (error) {

//     }
// }

CustomerModel.getCustomerDetailByLeadId = async (lead_id) => {
    return new Promise((resolve, reject) => {
        let sql = `SELECT cstr.name,  cstr.customer_email as email, concat('+63',cstr.customer_mobile) as mobile, lds.source_id, cstr.customer_city_name AS locality, lds.due_date as follow_up_date,cstr.otp_verified  FROM ${UBLMS_CUSTOMER} cstr
        INNER JOIN ${UBLMS_LEADS} lds ON lds.customer_id = cstr.id 
        WHERE lds.id=${lead_id}`;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            });
    });
}
module.exports = CustomerModel;