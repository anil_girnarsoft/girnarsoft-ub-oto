const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class CustomerReqAgentModel extends Model{
}

CustomerReqAgentModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    lead_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    customer_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    budget: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue:0
    },
    max_km:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    min_year: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    max_year:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull:true
    },
    fuel: {
        type:dbConfig.Sequelize.STRING,
        allowNull: true
        
    },
    make_ids : {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    model_ids: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    owner:{ 
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    color: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    transmission:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    purpose: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    req_body_type:{
        type:dbConfig.Sequelize.STRING,
        allowNull:true
    },
    drive_car: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    min_avg_daily_travel_km: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    max_avg_daily_travel_km: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    seat_num: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    buyer_type: {
        type:dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
        
    },
    status:{
        type:dbConfig.Sequelize.INTEGER,
        allowNull:false,
        defaultValue:1
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CUSTOMER_REQUIREMENT_AGENT,
    freezeTableName: true
});


CustomerReqAgentModel.createOne = async (data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const req_data = await CustomerReqAgentModel.create(data,{ transaction: t });
            resolve(req_data);
        } catch (error) {
            reject(error)
        }
    })
}

CustomerReqAgentModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await CustomerReqAgentModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


CustomerReqAgentModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if(params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
    if(params.lead_id)
        dataModel.lead_id = params.lead_id;
    if(params.customer_id)
        dataModel.customer_id = params.customer_id;
    if(params.hasOwnProperty('feedback_type'))
        dataModel.feedback_type = params.feedback_type;        
    if(params.budget)
        dataModel.budget = params.budget;
    if(params.max_km)
        dataModel.max_km = params.max_km;        
    if(params.min_year)
        dataModel.min_year = params.min_year;
    if(params.max_year)
        dataModel.max_year = params.max_year;
    if(params.fuel)
        dataModel.fuel = params.fuel; 
    if(params.make_ids)
        dataModel.make_ids = params.make_ids;
    if(params.model_ids)
        dataModel.model_ids = params.model_ids;
    if(params.owner)
        dataModel.owner = params.owner;
    if(params.color)
        dataModel.color = params.color;       
    if(params.transmission)
        dataModel.transmission = params.transmission;
    if(params.purpose)
        dataModel.purpose = params.purpose;
    if(params.req_body_type)
        dataModel.req_body_type = params.req_body_type;
    if(params.drive_car)
        dataModel.drive_car = params.drive_car;
    if(params.min_avg_daily_travel_km)
        dataModel.min_avg_daily_travel_km = params.min_avg_daily_travel_km;
    if(params.max_avg_daily_travel_km)
        dataModel.max_avg_daily_travel_km = params.max_avg_daily_travel_km;
    if(params.seat_num)
        dataModel.seat_num = params.seat_num;
    if(params.buyer_type)
        dataModel.buyer_type = params.buyer_type;
    return dataModel;
}

CustomerReqAgentModel.getOne = async _where => {
    return await CustomerReqAgentModel.findOne({where: _where});
}

CustomerReqAgentModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

CustomerReqAgentModel.getCustomerReqId = async(lead_id) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select id from "+UBLMS_CUSTOMER_REQUIREMENT_AGENT+" where lead_id = '"+lead_id+"' and status = '1'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            let reqId = 0;
            if(result && result.length){
                reqId = result['0']['id']  
            }
            resolve(reqId);
        }).catch(error=>{
            reject(error);
        });
    })
}

CustomerReqAgentModel.saveCustomerReCommondation = async (insertReqArr, userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (insertReqArr['lead_id']) {
                let customerReqId = await CustomerReqAgentModel.getCustomerReqId(insertReqArr['lead_id']);
                insertReqArr['updated_by'] = userId;
                if (!customerReqId) {
                    if ((insertReqArr['budget'] && insertReqArr['budget'] != '-1' ) && (insertReqArr['model_ids'] || insertReqArr['req_body_type']))
                    {
                        insertReqArr['created-at'] = new Date().toISOString();
                        const req_data = await CustomerReqAgentModel.create(insertReqArr);
                        resolve(1);
                        
                    }
                }else{
                    resolve(0);  
                }
                
            }
        } catch (error) {
            reject(error)
        }
    })
}

CustomerReqAgentModel.checkReqAgentdetails = async(customerId,leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select id,customer_id from "+UBLMS_CUSTOMER_REQUIREMENT_AGENT+" where customer_id = '"+customerId+"' and lead_id = '"+leadId+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

CustomerReqAgentModel.getReqAgentdetails = async(customerId,leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select budget, max_km, min_year, max_year, fuel, model_ids, owner, color, transmission,\n"+
        "purpose, req_body_type, drive_car, min_avg_daily_travel_km, max_avg_daily_travel_km, seat_num, \n"+
        "buyer_type from "+UBLMS_CUSTOMER_REQUIREMENT_AGENT+" where customer_id = '"+customerId+"' and lead_id = '"+leadId+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

CustomerReqAgentModel.updateCustomerReqAgent = async (_where, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await CustomerReqAgentModel.update(data, { where: _where});
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

CustomerReqAgentModel.getReqAgentInfoToInsert = async(customerId,leadId) =>{
    return new Promise((resolve, reject)=>{
        let sql = "Select lead_id, customer_id, budget, max_km, min_year, max_year, fuel, model_ids,\n"+
        "owner, color, transmission, purpose, req_body_type, drive_car, min_avg_daily_travel_km, \n"+
        "max_avg_daily_travel_km, seat_num, buyer_type, created_at, updated_by, status from "+UBLMS_CUSTOMER_REQUIREMENT_AGENT+" where customer_id = '"+customerId+"' and lead_id = '"+leadId+"'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

CustomerReqAgentModel.isValidCustomerReq = async (customerId,leadId) => {
    return new Promise((resolve, reject)=>{
        let days = Number(0); 
        let sql = "SELECT DATEDIFF(NOW(), updated_at) AS days FROM " + UBLMS_CUSTOMER_REQUIREMENT_AGENT + " where customer_id = '"+customerId+"' and lead_id = '"+leadId+"' order by id DESC limit 0,1";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{       
            let isValid = 2;
            if((result && result.length && (result[0]['days'] >= 0 && result[0]['days'] < 30))) {
                isValid = 1;
            } else if(result && result.length  && !(result['0']['days'])) {
                isValid = 3;
            }
            resolve(isValid);
        }).catch(error=>{
            reject(error);
        });
    })
}

module.exports = CustomerReqAgentModel;