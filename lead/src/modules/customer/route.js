const express = require('express');
const router = express.Router();
const schemas = require('./validation_schema');
const validation = require(MIDDLEWARE_PATH + 'validation');
const CustomerController = require('./controller/customerController');

router.post('/editCustomer',validation(schemas.updateCustomer, 'body'),CustomerController.saveCustomer);
router.post('/saveCustomerRequirement',validation(schemas.saveCustomerRequirement, 'body'),CustomerController.saveCustomerRequirement);
router.post('/getAgentDetails',validation(schemas.getAgentDetails, 'body'),CustomerController.getAgentDetails);
router.post('/getCustomer',validation(schemas.getCustomerByMobile, 'body'),CustomerController.getCustomer);
router.post('/getCustomerLocality',CustomerController.getCustomerLocatity);

module.exports = router;