const Q = require("q");
const _ = require("lodash");
const customerModel = require('./../model/customerModel');
const requirementModel = require('./../model/customerRequirementModel');
const reqAgentModel = require('./../model/customerReqAgentModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../validation_schema");
const dbConfig = require(CONFIG_PATH + 'db.config');
const sequelize = dbConfig.sequelize;
const InventoryService = require(COMMON_MODEL + 'inventoryService');

exports.saveCustomer = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    const userId = _.get(req.headers, 'userid');
    let id = params.id;
    if(!id){
      params.added_by = userId;
      let customerObj = customerModel.getModel(params)
      customerModel.createOne(customerObj)
      .then(response =>{
        const customer_data = response.dataValues;
        if(customer_data.id > 0){
          ApiController.sendSuccessResponse(req, res, customer_data,'customer_created_successfully');
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .catch(err => {
        ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
      })
      
    }else{
      params.updated_by = userId;
      let customerObjToUpdate = customerModel.getModel(params);
      if(customerObjToUpdate['updated_by']){
        customerObjToUpdate['updated_by'] = +customerObjToUpdate['updated_by']; 
        delete customerObjToUpdate['updated_by']
      }

      customerModel.updateOne(id,customerObjToUpdate)
      .then(response => {
        if(response && response.length){
          ApiController.sendResponse(req, res,200,'customer_updated_successfully');
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        next(error);
      })
      
      
    }
  } catch (error) {
      next(error);
  }
  
}

exports.saveCustomerRequirement = async (req,res,next) =>{
  try {
    let req_response='';
    const t = await sequelize.transaction();
    let params = _.cloneDeep(req.body);
    const userId = _.get(req.headers, 'userid');
      params.added_by = userId;
      let reqObj = requirementModel.getModel(params);

      requirementModel.saveRequirements(reqObj,t)
      .then(async (response) =>{
        req_response = response.dataValues ? response.dataValues : ((response && response.length) ? response[0] : {});

        if(req_response.id > 0){
          let customer_id = params.customer_id;
          let agentDetails = await reqAgentModel.getOne({'lead_id':params.lead_id,'status':1})
          if(agentDetails)
            params.id = agentDetails.dataValues.id
          let agent_data = reqAgentModel.getModel(params);
          if(agentDetails){
            agent_data.updated_by = userId;
            let resp = await reqAgentModel.updateOne(agentDetails.dataValues.id,agent_data)
          }else{
            agent_data.added_by = userId; 
            let resp = await reqAgentModel.createOne(agent_data)
          }
          params.id = customer_id;
          let cust_data = customerModel.getModel(params);

          let cust_data_filtered = {};
          if(Object.keys(cust_data).length){
            Object.keys(cust_data).forEach(el=>{ if(cust_data[el]) cust_data_filtered[el] = cust_data[el]  })
          }

          return customerModel.updateOne(customer_id,cust_data_filtered,t)
        }else{ 
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE1");
        }
      })
      .then(response =>{
        if(response && response.length){
          t.commit();
          ApiController.sendSuccessResponse(req, res, req_response,'requirement_created_successfully');
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE2");
        }
      })
      .catch(err => {
        t.rollback();
        ApiController.sendErrorResponse(req, res, err);
      })
      
  
  } catch (error) {
      next(error);
  }
}

exports.getAgentDetails = async (req, res, next) => {
  let params = req.body;
  if(params.lead_id){
    let lead_id = reqAgentModel.decrypt(params.lead_id);
    params.lead_id = lead_id;
  }
  
  let agentDetails = await reqAgentModel.getOne(params)
  ApiController.sendPaginationResponse(req, res, agentDetails,[]);
}

exports.getCustomer = async (req, res, next)=>{
  try{
    let result={},customerList=[];
    let { value } = schemas.getList.validate(req.body);

    result = await customerModel.getOne(value,1);
    if(result)
    customerList[0] = (result.dataValues);
    ApiController.sendPaginationResponse(req, res, customerList,{});
  } catch (error) {
    next(error);
  }
}

exports.getCustomerLocatity = async (req, res, next) => {
  let params = req.body;  
  let customer_locality = [];
  if(params.city && params.city.length) customer_locality = await  InventoryService.getLocalityByCityId(req, params);
  ApiController.sendPaginationResponse(req, res, customer_locality,[]);
}





