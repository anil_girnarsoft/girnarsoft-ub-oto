const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class SourceTypeModel extends Model{
}

SourceTypeModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    status: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue: '1'
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_SOURCE_TYPE,
    freezeTableName: true,
    underscored: true
});


SourceTypeModel.createOne = async (data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const category_data = await SourceTypeModel.create(data,{ transaction: t });
            resolve(category_data);
        } catch (error) {
            reject(error)
        }
    })
}

SourceTypeModel.updateOne = async (id, data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const category_data = await SourceTypeModel.update(data, { where: { id: id } },{ transaction: t });
            resolve(category_data);
        } catch (error) {
            reject(error)
        }
    })
}

SourceTypeModel.get = async (_where,pagination) => {

    let whereCond = await SourceTypeModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS src.id,srcLang.name,srcLang.description,src.created_at,src.updated_at,srcLang.lang_id,src.status,src.added_by,src.updated_by FROM "+UBLMS_SOURCE_TYPE+" AS src \n\
        INNER JOIN "+UBLMS_SOURCE_TYPE_LANG+" AS srcLang ON src.id=srcLang.source_type_id \n\
        "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await SourceTypeModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

SourceTypeModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if(params.id && (params.status))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = '1';
    if(params.added_by)
        dataModel.added_by = params.added_by;
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
    return dataModel;
}

SourceTypeModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond =  await SourceTypeModel.bindPagination(params,pagination,isPaginationApply);
    if(params && params.name)
        condition = "where srcLang.name like '%" + params.name + "%'";
    if(params && params.id){
        let id = "src.id='"+ SourceTypeModel.decrypt(params.id)+"'";
        condition = (condition == '')? condition + "where "+id : condition+" and "+id 
    }
    if(params && params.hasOwnProperty('status')){
        let status = "src.status='"+params.status+"'";
        condition = (condition == '')? condition + "where "+status : condition+" and "+status 
    }
    if(params && params.created_at){
        let inputDate = dateFormat(params.created_at, "yyyy-mm-dd");
        let datetCond = "date_format(src.created_at,'%Y-%m-%d') <= '" + inputDate+ "'"
        condition = (condition == '')? condition + "where "+datetCond : condition+" and "+datetCond 
    }
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            if(srtBy && srtBy == 'id')
                srtBy = "src."+srtBy
            if(srtBy && srtBy == 'name')
                srtBy = "srcLang."+srtBy
            if(srtBy && srtBy == 'created_at')
                srtBy = "src."+srtBy
            if(srtBy && srtBy == 'status')
                srtBy = "src."+srtBy
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }else{
        sortCond = 'ORDER BY updated_at desc';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

SourceTypeModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

SourceTypeModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

SourceTypeModel.getOne = async id => {
    return await SourceTypeModel.findOne({where: {id: id}, raw: true});
}


SourceTypeModel.encrypt = (data) => {
    if(Array.isArray(data)) data = data.map(v => {
        if(v && v.id != null) v.source_type_id_hash = crypto.encode(v.id);
        return v;
    })
    else if (data && data.id) data.source_type_id_hash = crypto.encode(data.id);
    return data;
}

SourceTypeModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

module.exports = SourceTypeModel;