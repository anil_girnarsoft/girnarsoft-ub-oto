const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;

class SourceLangModel extends Model{
}

SourceLangModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    lang_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    source_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    description: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_SOURCE_LANG,
    freezeTableName: true,
    underscored: true
});


SourceLangModel.createOne = async (data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const category_lang_data = await SourceLangModel.create(data,{ transaction: t });
            resolve(category_lang_data);
        } catch (error) {
            reject(error)
        }
    })
}

SourceLangModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const source_data = await SourceLangModel.update(data, { where: { source_id: id } });
            resolve(source_data);
        } catch (error) {
            reject(error)
        }
    })
}

SourceLangModel.getModel = (params) => {
    let dataModel = {};
    dataModel.source_id = (params.id) ? params.id : params.source_id;
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    dataModel.lang_id = (params.lang_id) ? params.lang_id : 1;
    dataModel.name = params.name;
    if(params.description)
        dataModel.description = params.description
    return dataModel;
}

module.exports = SourceLangModel;