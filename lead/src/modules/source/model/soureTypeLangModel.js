const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;

class SourceTypeLangModel extends Model{
}

SourceTypeLangModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    lang_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    source_type_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    description: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_SOURCE_TYPE_LANG,
    freezeTableName: true,
    underscored: true
});


SourceTypeLangModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const category_lang_data = await SourceTypeLangModel.create(data);
            resolve(category_lang_data);
        } catch (error) {
            reject(error)
        }
    })
}

SourceTypeLangModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const source_data = await SourceTypeLangModel.update(data, { where: { source_type_id: id } });
            resolve(source_data);
        } catch (error) {
            reject(error)
        }
    })
}

SourceTypeLangModel.getModel = (params) => {
    let dataModel = {};
    dataModel.source_type_id = (params.id) ? params.id : params.source_type_id;
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    dataModel.lang_id = (params.lang_id) ? params.lang_id : 1;
    dataModel.name = params.name;
    if(params.description)
    dataModel.description = params.description
    return dataModel;
}

module.exports = SourceTypeLangModel;