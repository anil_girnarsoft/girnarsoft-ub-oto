const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;

class SubSourceLangModel extends Model{
}

SubSourceLangModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    lang_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    sub_source_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    description: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    friendly_name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_SUB_SOURCE_LANG,
    freezeTableName: true,
    underscored: true
});


SubSourceLangModel.createOne = async (data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const category_lang_data = await SubSourceLangModel.create(data,{ transaction: t });
            resolve(category_lang_data);
        } catch (error) {
            reject(error)
        }
    })
}

SubSourceLangModel.updateOne = async (id, data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const source_data = await SubSourceLangModel.update(data, { where: { sub_source_id: id } },{ transaction: t });
            resolve(source_data);
        } catch (error) {
            reject(error)
        }
    })
}

SubSourceLangModel.getModel = (params) => {
    let dataModel = {};
    dataModel.sub_source_id = (params.id) ? params.id : params.sub_source_id;
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    dataModel.lang_id = (params.lang_id) ? params.lang_id : 1;
    dataModel.name = (params.name) ? params.name : null;
    dataModel.description = (params.description) ? params.description : null;
    dataModel.friendly_name = params.friendly_name;
    return dataModel;
}

module.exports = SubSourceLangModel;