const Q = require("q");
const _ = require("lodash");
const sourceTypeModel = require('../model/sourceTypeModel');
const sourceTypeLangModel = require('../model/soureTypeLangModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../validation_schema");
const moment = require("moment");
const dbConfig = require(CONFIG_PATH + 'db.config');
const sequelize = dbConfig.sequelize;

exports.getSourceList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await sourceTypeModel.get(value,page_number);
    let sourceTypeList = sourceTypeModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, sourceTypeList,pagination);
  }catch(error){

  }
}

exports.saveSourceType = async (req, res, next)=>{
  try {
    const userId = _.get(req.headers, 'userid');
    let params = _.cloneDeep(req.body);
    let id = params.id;
    const t = await sequelize.transaction();
    let isSameNameExist = await sourceTypeLangModel.findOne({where:{'lang_id':params.lang_id,'name':params.name}})
    if(isSameNameExist){
      if(!id || id != isSameNameExist.dataValues.source_type_id) throw new Error('NAME_SHOULD_BE_UNIQUE')
    }
    if(!id){
      params.added_by = userId;
      let sourceTypeObj = sourceTypeModel.getModel(params)
      sourceTypeModel.createOne(sourceTypeObj,t)
      .then(response =>{
        const source_data = response.dataValues;
        if(source_data.id > 0){
          params.source_type_id = source_data.id;
          let source_lang_data = sourceTypeLangModel.getModel(params)
          return sourceTypeLangModel.createOne(source_lang_data,t)
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .then(response =>{
        const srcType_lang_data = response.dataValues;
        if(srcType_lang_data.id > 0){
          t.commit();
          ApiController.sendSuccessResponse(req, res, srcType_lang_data,'source_type_created_successfully');
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .catch(err => {
        t.rollback();
        ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
      })
      
    }else{
      params.updated_by = userId;
      let categoryObjToUpdate = sourceTypeModel.getModel(params);
      sourceTypeModel.updateOne(id,categoryObjToUpdate,t)
      .then(response => {
        if(response && response.length){
          let categoryLanObjToUpdate = sourceTypeLangModel.getModel(params)
          return sourceTypeLangModel.updateOne(id,categoryLanObjToUpdate,t)
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .then(response => {
        if(response && response.length){
          t.commit();
          ApiController.sendResponse(req, res,200,'source_type_updated_successfully');
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        next(error);
      })
      
      
    }
  } catch (error) {
      next(error);
  }
  
}

exports.updateStatus = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    let id = req.body.id;
    const userId = _.get(req.headers, 'userid');
    params.updated_by = userId;
    let statusObj = sourceTypeModel.getModel(params)
    sourceTypeModel.updateOne(id,statusObj)
    .then(response => {
      if(response && response.length){
        ApiController.sendResponse(req, res,200,'status_updated_successfully');
      }else{
        ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
      }
    })
    .catch(error => {
      next(error);
    })
  } catch (error) {
      next(error);
  }
  
}


