const Q = require("q");
const _ = require("lodash");
const sourceModel = require('../model/sourceModel');
const subsourceLangModel = require('../model/subSourceLangModel');
const subsourceModel = require('../model/subSourceModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../source_validation_schema");
const moment = require("moment");
const dbConfig = require(CONFIG_PATH + 'db.config');
const sequelize = dbConfig.sequelize;

exports.saveSubSource = async (req, res, next)=>{
  try {
    const userId = _.get(req.headers, 'userid');
    let params = _.cloneDeep(req.body);
    const t = await sequelize.transaction();
    let id = params.id;
    let isSrcIdExist =  await sourceModel.getOne(req.body.source_id);
    if(!isSrcIdExist)
      throw new Error('Source id does not exist')
    let isSameNameExist = await subsourceLangModel.findOne({where:{'lang_id':params.lang_id,'name':params.name}})
    if(isSameNameExist){
      if(!id || id != isSameNameExist.dataValues.sub_source_id) throw new Error('NAME_SHOULD_BE_UNIQUE')
    }
    if(!id){
      params.added_by = userId;
      let sourceObj = subsourceModel.getModel(params);
      subsourceModel.createOne(sourceObj,t)
      .then(response =>{
        const source_data = response.dataValues;
        if(source_data.id > 0){
          params.sub_source_id = source_data.id;
          let source_lang_data = subsourceLangModel.getModel(params)
          return subsourceLangModel.createOne(source_lang_data,t)
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .then(response =>{
        const srcType_lang_data = response.dataValues;
        if(srcType_lang_data.id > 0){
          t.commit();
          ApiController.sendSuccessResponse(req, res, srcType_lang_data,'sub_source_created_successfully');
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .catch(err => {
        t.rollback();
        ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
      })
      
    }else{
      params.updated_by = userId;
      let sourceObjToUpdate = subsourceModel.getModel(params);
      subsourceModel.updateOne(id,sourceObjToUpdate,t)
      .then(response => {
        if(response && response.length){
          let sourceLanObjToUpdate = subsourceLangModel.getModel(params)
          return subsourceLangModel.updateOne(id,sourceLanObjToUpdate,t)
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .then(response => {
        if(response && response.length){
          t.commit();
          ApiController.sendResponse(req, res,200,'sub_source_updated_successfully');
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        t.rollback();
        next(error);
      })
      
      
    }
  } catch (error) {
      next(error);
  }
  
}

exports.getSourceList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await subsourceModel.get(value,page_number);
    let sourceList = subsourceModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, sourceList,pagination);
  }catch(err){
    
  }

}

exports.updateStatus = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    let id = req.body.id;
    const userId = _.get(req.headers, 'userid');
    params.updated_by = userId;
    let statusObj = subsourceModel.getModel(params)
    subsourceModel.updateOne(id,statusObj)
    .then(response => {
      if(response && response.length){
        ApiController.sendResponse(req, res,200,'status_updated_successfully');
      }else{
        ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
      }
    })
    .catch(error => {
      next(error);
    })
  } catch (error) {
      next(error);
  }
  
}


