const Q = require("q");
const _ = require("lodash");
const sourceModel = require('../model/sourceModel');
const sourceLangModel = require('../model/sourceLangModel');
const sourceTypeModel = require('../model/sourceTypeModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../source_validation_schema");
const moment = require("moment");
const dbConfig = require(CONFIG_PATH + 'db.config');
const sequelize = dbConfig.sequelize;

exports.saveSource = async (req, res, next)=>{
  try {
    let srcResponse;
    const t = await sequelize.transaction();
    let params = _.cloneDeep(req.body);
    const userId = _.get(req.headers, 'userid');
    let id = params.id;
    let isSrcTypeIdExist =  await sourceTypeModel.getOne(req.body.source_type_id);
    if(!isSrcTypeIdExist)
      throw new Error('Source type id does not exist')
    let isSameNameExist = await sourceLangModel.findOne({where:{'lang_id':params.lang_id,'name':params.name}})
    if(isSameNameExist){
      if(!id || id != isSameNameExist.dataValues.source_id) throw new Error('NAME_SHOULD_BE_UNIQUE')
    }
    if(!id){
      params.added_by = userId;
      let sourceObj = sourceModel.getModel(params)
      sourceModel.createOne(sourceObj,t)
      .then(response =>{
        const source_data = response.dataValues;
        if(source_data.id > 0){
          srcResponse = source_data;
          params.source_id = source_data.id;
          let source_lang_data = sourceLangModel.getModel(params)
          return sourceLangModel.createOne(source_lang_data,t)
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .then(response =>{
        const srcType_lang_data = response.dataValues;
        if(srcType_lang_data.id > 0){
          t.commit();
          ApiController.sendSuccessResponse(req, res, srcResponse,'source_created_successfully');
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .catch(err => {
        t.rollback();
        ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
      })
      
    }else{
      params.updated_by = userId;
      let categoryObjToUpdate = sourceModel.getModel(params);
      sourceModel.updateOne(id,categoryObjToUpdate,t)
      .then(response => {
        if(response && response.length){
          let categoryLanObjToUpdate = sourceLangModel.getModel(params)
          return sourceLangModel.updateOne(id,categoryLanObjToUpdate,t)
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .then(response => {
        if(response && response.length){
          t.commit();
          ApiController.sendResponse(req, res,200,'source_updated_successfully');
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        t.rollback();
        next(error);
      })
      
      
    }
  } catch (error) {
      next(error);
  }
  
}

exports.getSourceList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;

    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await sourceModel.get(value,page_number);
    let categoryList = sourceModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, categoryList,pagination);
  }catch(err){

  }
}

exports.updateStatus = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    let id = req.body.id;
    const userId = _.get(req.headers, 'userid');
    params.updated_by = userId;
    let statusObj = sourceModel.getModel(params)
    sourceModel.updateOne(id,statusObj)
    .then(response => {
      if(response && response.length){
        ApiController.sendResponse(req, res,200,'status_updated_successfully');
      }else{
        ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
      }
    })
    .catch(error => {
      next(error);
    })
  } catch (error) {
      next(error);
  }
  
}


