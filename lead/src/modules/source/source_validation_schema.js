const Joi = require('@hapi/joi')

module.exports = {
    createSource: Joi.object().keys({
        name: Joi.string().optional(),
        description: Joi.string().optional(),
        lang_id : Joi.number().required(),
        source_type_id : Joi.number().required(),
        status : Joi.string().valid('0', '1').optional()
    }),
    updateSource: Joi.object().keys({
        id: Joi.number().required(),
        name: Joi.string().optional(),
        description: Joi.string().optional(),
        status: Joi.string().valid('0', '1').optional(),
        lang_id: Joi.number().required(),
        source_type_id : Joi.number().required()

    }),
    updateStatus: Joi.object().keys({
        id: Joi.number().required(),
        status: Joi.string().valid('0', '1').required()
    }),
    getList: Joi.object().keys({
        id: Joi.number().required(),
        _with: Joi.array().items(Joi.string().required()).optional()
    }).unknown(true)
}