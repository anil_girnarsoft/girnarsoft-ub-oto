const express = require('express');
const router = express.Router();
const schemas = require('./validation_schema');
const srcSchemas = require('./source_validation_schema');
const subSrcSchemas = require('./sub_source_validation_schemas');
const validation = require(MIDDLEWARE_PATH + 'validation');
const SourceTypeController = require('./controller/sourceTypeController');
const SourceController = require('./controller/sourceController');
const SubSourceController = require('./controller/subSourceController');

router.post('/listSourceType', SourceTypeController.getSourceList);
router.post('/saveSourceType',validation(schemas.createSourceType, 'body'), SourceTypeController.saveSourceType);
router.post('/editSourceType',validation(schemas.updateSourceType, 'body'),SourceTypeController.saveSourceType);
router.post('/saveSource',validation(srcSchemas.createSource, 'body'), SourceController.saveSource);
router.post('/editSource',validation(srcSchemas.updateSource, 'body'),SourceController.saveSource);
router.post('/listSource', SourceController.getSourceList);
router.post('/saveSubSource',validation(subSrcSchemas.createSubSource, 'body'), SubSourceController.saveSubSource);
router.post('/editSubSource',validation(subSrcSchemas.updateSubSource, 'body'), SubSourceController.saveSubSource);
router.post('/listSubSource', SubSourceController.getSourceList);
router.post('/updateSourceStatus',validation(srcSchemas.updateStatus, 'body'), SourceController.updateStatus);
router.post('/updateSourceTypeStatus',validation(schemas.updateStatus, 'body'), SourceTypeController.updateStatus);
router.post('/updateSubSourceStatus',validation(subSrcSchemas.updateStatus, 'body'), SubSourceController.updateStatus);

module.exports = router;