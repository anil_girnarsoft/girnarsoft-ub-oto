const Joi = require('@hapi/joi')

module.exports = {
    createSubSource: Joi.object().keys({
        name: Joi.string().optional(),
        description: Joi.string().optional(),
        lang_id : Joi.number().required(),
        source_id : Joi.number().required(),
        status : Joi.string().valid('0', '1').optional(),
        friendly_name : Joi.string().required(),
        paid_details : Joi.string().valid('0', '1','2','3').optional(),
        added_to_l1 : Joi.string().valid('0', '1').optional(),
        added_to_l2 : Joi.string().valid('0', '1').optional()
    }),
    updateSubSource: Joi.object().keys({
        id: Joi.number().optional(),
        name: Joi.string().optional(),
        description: Joi.string().optional(),
        lang_id : Joi.number().optional(),
        source_id : Joi.number().required(),
        status : Joi.string().valid('0', '1').optional(),
        friendly_name : Joi.string().required(),
        paid_details : Joi.string().valid('0', '1','2','3').optional(),
        added_to_l1 : Joi.string().valid('0', '1').optional(),
        added_to_l2 : Joi.string().valid('0', '1').optional()

    }),
    updateStatus: Joi.object().keys({
        id: Joi.number().required(),
        status: Joi.string().valid('0', '1').required()
    }),
    getList: Joi.object().keys({
        id: Joi.number().required(),
        _with: Joi.array().items(Joi.string().required()).optional()
    }).unknown(true)
}