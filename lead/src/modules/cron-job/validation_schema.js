const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));
const array_items = Joi.number()
module.exports = {
    updateDialerCallStatus: Joi.object().keys({
        lead_id: Joi.string().required(),
        List_id: Joi.string().required(),
        Call_status: Joi.string().required(),
        apikey: Joi.optional(),
        Agent_ID: Joi.optional(),
        Call_StartTime: Joi.optional(),
        Call_EndTime: Joi.optional(),
        Callback_date_and_time: Joi.optional(),
        Recording_url: Joi.optional(),
    }),
    whatsappCallback: Joi.object().keys({
        notification_id: Joi.string().required(),
        called_option: Joi.string().required()
    })
}