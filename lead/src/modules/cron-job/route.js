const express = require('express');
const router = express.Router();
const ProcessRawLeadCont = require(`./controller/processRawLead`);
const ProcessNewLeadCntrlr =  require(`./controller/processNewLead`);
const DealerPriorityCtrl   = require(`./controller/syncDealerPriority`);
const DialerLeadsCtrl   = require(`./controller/processDialerLeads`);
const ProcessFacebookLeadsCtrl   = require(`./controller/processFacebookLeads`);
const ProcessDealerDeliveryReportCtrl = require(`./controller/processDealerDeliveryReport`);
const NotificationCtrl = require(`./controller/notificationController`);
const schemas = require('./validation_schema');
const validation = require(MIDDLEWARE_PATH + 'validation');
var fs = require('fs');

const asyncHandler = fn => (req, res, next) => {
  return Promise
      .resolve(fn(req, res, next))
      .catch(next);
};

router.get('/processRawLead', asyncHandler(ProcessRawLeadCont.ProcessRawLead));
router.get('/processNewLead', asyncHandler(ProcessNewLeadCntrlr.processNewLeads));
router.get('/syncDealerPriority', asyncHandler(DealerPriorityCtrl.syncDealerInvPriority))
router.get('/processDialerLeads', asyncHandler(DialerLeadsCtrl.processDialerLeads))
router.get('/addLeadsToDialer', asyncHandler(DialerLeadsCtrl.addLeadsToDialer))
router.post('/updateDialerCallStatus',validation(schemas.updateDialerCallStatus, 'body'), asyncHandler(DialerLeadsCtrl.updateDialerCallStatus))
router.get('/processFacebookLeads', asyncHandler(ProcessFacebookLeadsCtrl.processFacebookLeads))
router.get('/processSavedFbLeads', asyncHandler(ProcessFacebookLeadsCtrl.processSavedFbLeads))
router.get('/refreshFbToken', asyncHandler(ProcessFacebookLeadsCtrl.refreshFbToken))
router.get('/sendDealerDeliveryReport', asyncHandler(ProcessDealerDeliveryReportCtrl.sendDealerDeliveryReport))
router.get('/sendWalkinScheduleNotification', asyncHandler(NotificationCtrl.sendWalkinScheduleNotification))
router.post('/whatsappCallback',validation(schemas.whatsappCallback, 'body'), asyncHandler(NotificationCtrl.whatsappCallback))
router.get('/downloadLeadFinderFiles', function(req, res){
  let params = req.query;
  if(params.fileName){
    var f = __dirname+'../../../../public/lead-finder-files/'+params.fileName;
    res.download(f); // Set disposition and send it.
  }else{
    res.end();
  }
});

router.get('/downloadCsvFiles', function(req, res){
  let params = req.query;
  if(params.fileName && params.dir){
    var f = `${__dirname}../../../../public/${params.dir}/${params.fileName}`;
    res.download(f); // Set disposition and send it.
  }else{
    res.end();
  }
});

router.post('/waCallback', asyncHandler(DealerPriorityCtrl.waCallback))

module.exports = router;