const Q = require("q");
const _ = require("lodash");
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const InventoryService = require(COMMON_MODEL + 'inventoryService');
const DealerSync = require('./../model/dealerCarPriority');
const dateFormat = require('dateformat');

exports.syncDealerInvPriority = async (req, res)=>{
    let page_no             = 1;
    let carIds              = [];
    let next_page           = false;
    let classified_check    = 1;
    let fullSyncUpdated     = (req.query.skip_full_sync && req.query.skip_full_sync == 0) ? '1' : '0'; //IF skip_full_sync IS 0, THEN SET 1


    //RESET full_sync_updated TO 0
    if(req.query.skip_full_sync && req.query.skip_full_sync == 0){
        await DealerSync.resetFullSyncUpdated();
    }

    if(req.query.skip_full_sync && req.query.skip_full_sync == 1){
        let maxCarId = await DealerSync.getMaxCarId();
        classified_check = 0;
        if(maxCarId && maxCarId > 0){
            let nextRecord = maxCarId+10;
            carIds = Array(nextRecord - maxCarId + 1).fill().map((_, idx) => maxCarId+1 + idx);
        }
    }
    do{
        let stockPostData = {};
        stockPostData['call_server'] = 'ub';
        stockPostData['car_status'] = [1,2];
        stockPostData['stock_used_car_id_arr'] = carIds;
        stockPostData['page_no'] = page_no;
        if(classified_check && classified_check == 1){
            stockPostData['classified'] = '1';
        }
        let promisesRes = await Q.allSettled([InventoryService.getStockList(req, stockPostData)]);
        let carDetailsdata = promisesRes && _.get(promisesRes, "[0].value.data"); 
        let paginationData = promisesRes && _.get(promisesRes, "[0].value.pagination");
        if(carDetailsdata && carDetailsdata.length > 0){
            carDetailsdata.forEach(async (carData) => { 
                let carExists = await DealerSync.findOne({where:{'car_id':carData.id}})
                if(carExists){
                    await DealerSync.updateOne(carData.id, {
                        dealer_id: carData.dealer_id,
                        priority: carData.paid_score,
                        city_id: (carData.user_type == 'I') ?carData.reg_place_city_id :carData.city,
                        city: (carData.user_type == 'I') ?carData.reg_car_city_name :carData.city_name,
                        updated_at: dateFormat('yyyy-mm-dd HH:MM:ss'),
                        type: carData.user_type,
                        car_status: carData.car_status,
                        is_classified: carData.isclassified,
                        full_sync_updated: fullSyncUpdated
                    });
                }else{
                    await DealerSync.createOne({
                        car_id: carData.id,
                        dealer_id: carData.dealer_id,
                        city_id: (carData.user_type == 'I') ?carData.reg_place_city_id :carData.city,
                        city: (carData.user_type == 'I') ?carData.reg_car_city_name :carData.city_name,
                        priority: carData.paid_score, 
                        created_at: dateFormat('yyyy-mm-dd HH:MM:ss'), 
                        updated_at: dateFormat('yyyy-mm-dd HH:MM:ss'),
                        type: carData.user_type,
                        car_status: carData.car_status,
                        is_classified: carData.isclassified,
                        full_sync_updated: fullSyncUpdated

                    });
                }
            });
        }
        if(paginationData.next_page && paginationData.next_page == true){
            page_no ++;
            next_page = true;
        }else{
            next_page = false;
        }
        
    }while(next_page);

    //IN CASE OF FULL SYNK (skip_full_sync=0) IF ANY OF THE full_sync_updated IS 0 THEN UPDATE is_classified: 0
    if(req.query.skip_full_sync && req.query.skip_full_sync == 0){
        DealerSync.updateIsClassified();
    }
    ApiController.sendResponse(req, res, 200, res.__('SUCCESS'));
}

exports.waCallback = async (req, res, next)=>{
    try{
        let params = req.body;
        let message = "";
        if(params.option && params.notification_id){
            message = `You have replyed with option ${params.option} with notification ID: ${params.notification_id}`;
        }else{
            message = "No option choosen";
        }
        ApiController.sendResponse(req, res, 200, message);
    }catch(error) {
        ApiController.sendResponse(req, res, 400, 'Error called');
    }
}