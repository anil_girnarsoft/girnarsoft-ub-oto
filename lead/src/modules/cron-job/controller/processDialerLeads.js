const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const LeadService = require(COMMON_MODEL + 'leadService');
const leadModel = require('../../lead/model/leadModel');
const leadDialerModel = require('../../lead/model/leadDialerModel');
const leadDialerHistoryModel = require('../../lead/model/leadDialerHistoryModel');
const { PROCESS_DIALSHREE_LEAD_LIMIT } = require("../../../../config/config");
const dialerController = require('../../lead/controller/dialerController');
const conversionModel = require('../../lead/model/conversionModel');
const CommonHelper = require('../../../helpers/CommonHelper');
const callStatusModel = require('../../status/model/callStatusModel');
var async = require("async");

exports.processDialerLeads = async (req, res)=>{
    try{

        let params = {};
        let reqParams = req.query;

        params['page_limit'] = reqParams['limit'] || PROCESS_DIALSHREE_LEAD_LIMIT;
        params['isPaginationApply'] = true;
        // let pageNum = params.page_number || null;

        //GET DIALER LEADS
        let getDialerLeads = await leadDialerModel.getDialerLeadPendingStatus(params);

        if(getDialerLeads && getDialerLeads[0] && getDialerLeads[0].length){

            let dialerParams = await CommonHelper.DialshreeAddApiConfig(process.env.COUNTRY_CODE) || {};
            
            async.forEach(getDialerLeads[0], async (dialerLead, callback) => {

                //IF PENDING & ATTEMPTS LESS THAN 3 THEN PROCESS
                if(dialerLead.status == 'pending' && dialerLead.attempt < 3){
                    let sendToDialerLead = {};
                    let customerName = (dialerLead.name !== '') ? dialerLead.name.split(' ') : [];
                    
                    sendToDialerLead['first_name']        =     (customerName[0]) ? customerName[0] : '';
                    sendToDialerLead['last_name']         =     (customerName[1]) ? customerName[1] : '';
                    sendToDialerLead['source_id']         =     dialerLead.lead_id_hash;
                    sendToDialerLead['phone_number']      =     dialerLead.phone;
                    sendToDialerLead['city']              =     dialerLead.customer_city_name;
                    sendToDialerLead['list_id']           =     dialerLead.list_id;
                    sendToDialerLead['vendor_lead_code']  =     new Date().toJSON().slice(0,10).replace(/-/g,'-')
                    sendToDialerLead['source_id']         =     dialerLead.lead_id_hash;
                    
                    //FOR ID SEND ALL DATA IN SINGLE OBJECT
                    if(process.env.COUNTRY_CODE == 'id') {
                        dialerParams = {...dialerParams, ...sendToDialerLead};
                        
                    }
                    //IN CASE OF PH SEND MULTI LEAD KEY
                    else if(process.env.COUNTRY_CODE == 'ph') {
                        dialerParams['multi_leads']     =   JSON.stringify([sendToDialerLead]);
                    }

                    await LeadService.addToDialer(req, dialerParams).then(async processDialerLead=>{
                        let updateDialerLeadTable = {};
                        updateDialerLeadTable['attempt']            = (dialerLead['attempt']+1);
                        updateDialerLeadTable['last_attempt']       = new Date().toISOString();
                        updateDialerLeadTable['dialer_response']    = JSON.stringify(processDialerLead);

                        if((process.env.COUNTRY_CODE == 'id' && processDialerLead.success) || (process.env.COUNTRY_CODE == 'ph' && processDialerLead['lead 1'] && processDialerLead['lead 1']['success'] == 'true')) {
                            updateDialerLeadTable['status'] = 'processed';
                            
                            //SET IN LEAD TABLE DIALER STATUS 1(SUCCESS)
                            await leadModel.updateOne(dialerLead['lead_id'], {dialer_status:'1'});
                        }

                        if(updateDialerLeadTable['status'] != 'processed' && updateDialerLeadTable['attempt'] == 3){
                            updateDialerLeadTable['status'] = 'failed';
                        }

                        await leadDialerModel.updateOne(dialerLead['id'], updateDialerLeadTable);
                    }).catch(function (err) {
                        // Error
                        ApiController.sendResponse(req, res, 200, err.message);
                    });

                }else if(dialerLead.status == 'pending' && dialerLead.attempt == 3){
                    //SET IN LEAD TABLE DIALER STATUS 2(FAILURE)
                    await leadModel.updateOne(dialerLead['lead_id'], {dialer_status:'2'});
                }


                callback();
            
            }, function(err) {
                ApiController.sendResponse(req, res, 200, res.__('success'));
            });

        }else{
            
            ApiController.sendResponse(req, res, 200, res.__('success'));
        }


    }catch(error) {
        ApiController.sendResponse(req, res, 200, res.__('error'), error);
    }
}


exports.updateDialerCallStatus = async (req, res, next) => {

    try{
        let params = req.body;

        let encryptedLeadId = await leadDialerHistoryModel.decrypt(params.lead_id);
        if(encryptedLeadId){
            let dialerCallLog = {};
            dialerCallLog['lead_id'] = encryptedLeadId;
            dialerCallLog['lead_id_hash'] = params.lead_id;
            dialerCallLog['dialer_status'] = params.Recording_url ? 'processed' : 'failed';
            dialerCallLog['status'] = params.Recording_url ? 'processed' : 'failed';
            dialerCallLog['created_date'] = new Date().toISOString();
            dialerCallLog['call_start_time'] = params.Call_StartTime;
            dialerCallLog['call_end_time'] = params.Call_EndTime;
            dialerCallLog['recording_url'] = params.Recording_url;
            dialerCallLog['list_id'] = params.List_id;
            dialerCallLog['agent_id'] = params.Agent_ID;
            dialerCallLog['status_id'] = 0;
            dialerCallLog['sub_status_id'] = 0;
            dialerCallLog['calling_status'] = params.Call_status || '';
            dialerCallLog['call_back_date_time'] = params.Callback_date_and_time || null;
            await leadDialerHistoryModel.createOne(dialerCallLog);

            //SAVE RECORD IN CONVERSION
            totTimeDifference = '';
            if(params.Call_StartTime && params.Call_EndTime){
                var now  = new Date(params.Call_StartTime);
                var then = new Date(params.Call_EndTime);
                timeDifference = await CommonHelper.timeDifference(then, now);
                totTimeDifference =  (timeDifference.hour<10?'0'+timeDifference.hour:timeDifference.hour)+':'+(timeDifference.minute<10?'0'+timeDifference.minute:timeDifference.minute)+':'+(timeDifference.second<10?'0'+timeDifference.second:timeDifference.second);
            }

            let getCallStatusId = 0;
            if(params.Call_status){
                getCallStatus = await callStatusModel.getCallStatusByName({name: params.Call_status});
                getCallStatusId = (getCallStatus && getCallStatus[0] && getCallStatus[0]['id']) ? getCallStatus[0]['id'] : 0
            }

            let conversationData = {};
            conversationData = {
            'lead_id': encryptedLeadId,
            'calling_status_id': getCallStatusId,
            'comment': 'Updated from Dialer',
            'latest': '1',
            'dialer_call_datetime': params.Callback_date_and_time || null,
            'dialer_call_status': params.Call_status || '',
            'dialer_call_duration': totTimeDifference || null,
            'added_by': -1
            };
            await conversionModel.createOne(conversationData);
            
            ApiController.sendResponse(req, res, 200, 'success');

        }else{
            ApiController.sendResponse(req, res, 400, 'Invalid Lead id');
        }

    }catch(error) {
        ApiController.sendResponse(req, res, 400, 'error');
    }
}


exports.addLeadsToDialer = async (req, res) => {
    req['body']['callbackFunction'] = true;
    let addLeadsToDialer = await dialerController.getDialerLeads(req, res);
}