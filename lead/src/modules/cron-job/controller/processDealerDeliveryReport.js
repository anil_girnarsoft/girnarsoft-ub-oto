const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const DealerDeliveryReportEmails = require('../../report/model/dealerDeliveryReportEmails');
const axios = require('axios');

const createCsvWriter = require('csv-writer').createArrayCsvWriter;
var fs = require('fs');
const NotificationService = require(COMMON_MODEL + 'notificationService');
const MailTemplate = require("../../lead/mailTemplate/htmlTemplate");
const { ADMIN_SUPPORT_EMAIL } = require("../../../../config/config");
const Q = require("q");
const InventoryService = require(COMMON_MODEL + 'inventoryService');

exports.sendDealerDeliveryReport = async (req, res)=>{
  //FIND 1 Email record
  // let mailDataFromDb = await DealerDeliveryReportEmails.getDataToSendMail();

  // if(mailDataFromDb && mailDataFromDb.length){
    // let dealerDeliveryParams = (mailDataFromDb[0]['params']) ? JSON.parse(mailDataFromDb[0]['params']) : ''; //CHECK FILTERS
    let newReq = Object.assign({}, req);
    // if(dealerDeliveryParams){
      // newReq.body = dealerDeliveryParams;
    // }
        newReq.body.cb_function = true;
        newReq.body.total_limit = -1; //ALL
        //CALL DEALER DELIVERY REPORT FUNCTION
    let dealerReport = await axios.post(`${newReq.services.CORE_HOST}/ub/report/dealerDeliveryReport`, newReq.body)
    .then(async (response) => {
      if(response && response.data && response.data.status == 200 && response.data.data.length){
        //PREPARE DATA FOR CSV FILE
        let fileData = {};
        // fileData['id'] = mailDataFromDb[0]['id'];
        // fileData['email'] = mailDataFromDb[0]['email'];
        fileData['filename'] = 'dealer-delivery-report.csv';
        fileData['data'] = response.data.data.map(el=>  
            (
              [
                el['cluster_name'],
                el['city_name'],
                el['dealer_id'],  
                el['gcd_code'],  
                el['organization'], 
                el['paid_score'] ? ((el['paid_score'] == 1) ? 'Free' : 'Paid') : '',
                (el['dealer_status'] =='1'?'Active':'InActive'), 
                (el['total_car']||''), 
                (el['sumOrganic'] || ''), 
                (el['sumBackend']||''), 
                (el['totalLead']||''), 
                (el['sumWalkinSchedule']||""), 
                (el['sumWalkinDone']||''), 
                (el['sumBooked']||''), 
                (el['sumConversion']||'')
              ]
              ) 
            );

            //CALL CSV FILE FUNCTION
        let csvStatus = await createCSVFile(req, fileData);
        let msg = '';
        
        if(csvStatus.status == 200){
          msg = 'Mail sent successfully';
        }else if(csvStatus.status == 100){
          msg = 'Csv file creation error';
        }
        ApiController.sendResponse(req, res, 200, msg, csvStatus.data);


      }else{
        ApiController.sendResponse(req, res, 200, 'No dealers found');
      }
    })
    .catch(function (error) {
      ApiController.sendResponse(req, res, 200, error);
    });

  // }else{
  //   ApiController.sendResponse(req, res, 200, 'No data found');
  // }
}


createCSVFile = async (req, params) => {
  return new Promise(async(resolve, reject)=>{

    const requiredformat = ['Cluster', 'City', 'Dealer ID', 'GCD Code', 'Dealer', 'Dealer Type', 'Dealer Status', 'Total Car', 'Organic Leads', 'Backend', 'Total', 'Walkin Schedule', 'Walkin Done', 'Booked', 'Conversions'];

    const fileName = Date.now() + '_' + params['filename']; //FILE PATH
    const paths = './public/dealer-delivery-reports-emails/'; //STORED DIRECTORY PATH

    //IF FOLDER NOT EXISTS THEN CREATE
    if (!fs.existsSync(paths)) {
      fs.mkdirSync(paths);
    }

    const csvWriter = createCsvWriter({
      header: requiredformat,
      path: paths+fileName
    });
    
    // let records = params.data;
    let finalData = [requiredformat, ...params.data]; //COMBINE HEADERS AND DATA
    let stringData = '';
    //CONVERT ARRAY DATA TO STRING
    finalData.forEach(el=>{
      stringData += el.join(',')+'\n';
    });

    let s3Csv = await InventoryService.writeCsS3(req, {upload_type: 'lead_csv', fileName: fileName, data: stringData});

    if(s3Csv && s3Csv.status == 200 && s3Csv.data && s3Csv.data.Location){
      
      setTimeout(()=>{
        //DELETE CSV FILE FROM FOLDER
          InventoryService.removeLeadCsvS3(req, {upload_type: 'lead_csv', fileKey: s3Csv.data.Key});
      },10000);

      return resolve({status: 200, data: s3Csv.data.Location});
    }else{
      return resolve({status:100});
    }

    
    // let csvStatus = await csvWriter.writeRecords((records))
        // .then(async() => {

            // let fullFilePath = "ub-cron/cronjob/downloadCsvFiles?fileName="+fileName+"&dir=dealer-delivery-reports-emails";
            // let finalStatus = {status: 200, data: fullFilePath};
            // setTimeout(()=>{
            //   //DELETE CSV FILE FROM FOLDER
            //   fs.unlink(paths+fileName, (err => { 
                
            //   })); 
            // },5000);
            // return resolve(finalStatus);

          // if(mailResponse && mailResponse[0] && mailResponse[0]['value'] && mailResponse[0]['value']['status'] == 200){
          //       DealerDeliveryReportEmails.updateOne(params['id'], {mail_sent:1});
          // }else{
          //   return resolve(201);
          // }
        
      // }).catch(err=> {return resolve({status:100})});

    });
}