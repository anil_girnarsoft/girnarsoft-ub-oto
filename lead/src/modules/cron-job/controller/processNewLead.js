const Q = require("q");
const _ = require("lodash");
const { async } = require("q");
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
//const RawLeadLogModel = require(MODULE_PATH+"lead/model/leadRawLogModel");
const processLead = require('./../model/processLead');
const dateFormat = require('dateformat');
const hash = require('object-hash');
exports.processNewLeads = async (req, res) => {
    let platformArray = ['Web', 'web', 'WAP', 'AndroidApp', 'iOSApp', 'WindowsApp', 'wap', 'Wap', 'android', 'Android', 'ios','IOS','Ios', 'APP', 'app', 'App'];
    if (req.query.leaddata) {
        try {
            let returnValue = {};

            let leadRawData = req.query.leaddata;
            if (leadRawData.apikey == undefined) {
                returnValue = {
                    status: false,
                    msg: "Rejected, API key is blank."
                };
                
            } else {
                let leadStatus = [];
                let newLeads = leadRawData.leads;
                let i = 0;
                newLeads.length && newLeads.forEach(async (leadres) => {
                    let datalogs = {};
                    let data = {};
                    let dataLogsduplicate = {};
                    let mobile = leadres.customer.mobile;
                    let language = (leadres.customer.language) ? leadres.customer.language : '';
                    let sub_source_id = leadres.source.sub_source_id;
                    if (platformArray.indexOf(leadres.source.platform) == -1) {
                        ApiController.sendResponse(req, res, 200, "Invalid Platform");
                    }
                    if (sub_source_id == undefined || sub_source_id == "") {
                        sub_source_id = await processLead.getSubsourceByUTM(leadres.marketing.utm_source, leadres.source_id);
                    }
                    if (((leadres.marketing != undefined) && (leadres.marketing.utm_source)) || leadres.source_id == 90 || leadres.source_id == 368 || leadres.source_id == 4) {
                        let subsourcearray = await processLead.getSubsourceMapped(leadres.marketing.utm_source, leadres.source_id);
                        if (subsourcearray.id > 0) {
                            sub_source_id = subsourcearray.id;
                        }
                    }
                    if (isCustomerNameInvalid(leadres.customer.name)) {
                        leadStatus[i] = { status: false, error: 'Lead rejected due to invalid customer name.' }
                        datalogs.date = dateFormat("yyyy-mm-dd HH:MM:ss");
                        datalogs.source_id = leadRawData.source_id;
                        datalogs.source_lead_id = leadres.source.source_lead_id;
                        datalogs.response = JSON.stringify(leadStatus[i]);
                        processLead.insertOneMongo(datalogs);
                    } else {
                        if (leadres.cars.car_id > 0 && leadres.cars.ignore_car_id == 1) {
                            leadres.cars.car_id = 0;
                        }
                        data.source_id = leadRawData.source_id;
                        data.sub_source_id = sub_source_id;
                        data.mobile = mobile
                        if ((leadres.dealer) && (leadres.dealer.dealer_id != "undefined") && (leadres.dealer.dealer_id > 0)) {
                            leadres.dealer_id = leadres.dealer.dealer_id;
                        }
                        data.leads_data = leadres;
                        data.apitype = 1;
                        if (leadres.customer.city_id > 0) {
                            let city_id = processLead.getCityIDbyCentralID(leadres.customer.city_id);
                            data.city_id = city_id;
                        }
                        data.connecto_id = leadres.customer.connecto_id;
                        if (leadRawData.log_date != "") {
                            data.log_date = leadRawData.log_date;
                        }
                        if (leadres.customer.lead_created_at) {
                            data.log_date = dateFormat(leadres.customer.lead_created_at.toString(), "yyyy-mm-dd HH:MM:ss");
                        }

                        let enrypteddata = hash(leadres);
                        data.source_key = enrypteddata;
                        let checkDuplicacy = await processLead.checkDuplicateData(enrypteddata, leadRawData.source_id);
                        if (checkDuplicacy == 0) {
                            let raw_logid = await processLead.insertDataIntoRawLeadLogs(data);
                            leadres.added_date = dateFormat("yyyy-mm-dd HH:MM:ss");
                            await processLead.saveDataonRawleadsAdditionalParams(raw_logid, leadRawData.source_id, sub_source_id, mobile, language, leadres);
                            await processLead.saveDataonMarketingParams(raw_logid, leadres);
                        } else {
                            //let logid = await processLead.
                            leadStatus[i] = { status: "success", mobile: mobile, error: "Lead already exists,duplicate lead for source lead id " + leadres.source.source_lead_id }
                            dataLogsduplicate.date = dateFormat("yyyy-mm-dd HH:MM:ss");
                            dataLogsduplicate.source_id = leadres.source_id;
                            dataLogsduplicate.source_lead_id = leadres.source.source_lead_id;
                            dataLogsduplicate.data = JSON.stringify(leadres);
                            dataLogsduplicate.source_key = enrypteddata;
                            dataLogsduplicate.response = JSON.stringify(leadStatus[i]);
                            processLead.insertOneMongo(dataLogsduplicate);
                        }
                    }
                    i++;
                });
                let returnVal = { status: true, msg: "Saved Successfully." };
                ApiController.sendResponse(req, res, 200, res.__('SUCCESS'), returnVal);
            }
        } catch (e) {
            //console.log('Process New lead error 103333', e);
        }
    } else {
        var customer_id = lead_id = repo_lead_id = status = "";
        let dataLogsduplicate = {};
        let returnVal = {};
        let dataLogs = {};
        let params = req.query;
        let dataadditional = {};
        let returnValue = {};
        // if (params.apikey != DC_API_KEY) {
        //     returnValue = {
        //         status: false,
        //         msg: "API key is not matching"
        //     };
        //     console.log('Error ', returnValue);
        // }

        let post_source_detail = params.source_detail;
        let car_id = params.car_id;
        // if (car_id > 0) {
        //     let validateCarID = validateCarID($car_id, $_REQUEST);
        // }
        let car_make = params.car_make;
        let car_model = params.car_model;
        let car_city = params.car_city;
        let lead_platform = params.lead_platform;
        let lead_source = params.lead_source;
        let customer_city_lead = '';

        let customerOrgMob = params.customer_mobile;
        // if (strlen($_REQUEST['customer_mobile']) == 11) {
        //     $_REQUEST['customer_mobile'] = substr($_REQUEST['customer_mobile'], 1);
        // } else if (strlen($_REQUEST['customer_mobile']) == 12) {

        //     $_REQUEST['customer_mobile'] = substr($_REQUEST['customer_mobile'], 2);
        // } else if (strlen($_REQUEST['customer_mobile']) == 13) {
        //     $_REQUEST['customer_mobile'] = substr($_REQUEST['customer_mobile'], 3);
        // }
        let post_customer_detail = params.customer_name + "|" + params.customer_email + "|" + params.customer_mobile + "|" + params.customer_city;
        //$post_seller_detail    = escape( $_REQUEST['seller_detail'] );
        let post_seller_detail = params.seller_fname + "|" + params.seller_email + "|" + params.seller_mobile + "|" + params.seller_state + "|" + params.seller_city + "|" + params.seller_mobile + "|" + params.seller_user_type + "|" + params.seller_listing_type;

        let location = params.location;
        if (post_source_detail) {
            var [source_id, sub_source_id, user_action_id, lead_type_id] = post_source_detail.split("|");
        }
        if (params.apikey == '' || params.apikey == undefined) {
            returnVal = { status: false, msg: 'Please provide API Key.' };
        } else if (params.car_id == 1159564) {
            returnVal = { status: false, msg: 'Rejected Lead due to fake lead.' };
        } else if (params.source_detail == '90|475|1|1' && (params.car_id <= 0 || params.car_id == '' || params.dc_lead_check == '' || params.dc_lead_check == 0)) {
            returnVal = { status: false, msg: 'Rejected Leads due to SPAM from DC source' };
            dataLogs.date = dateFormat("yyyy-mm-dd HH:MM:ss");
            dataLogs.postdata = JSON.stringify(params);
            //dataLogs.server_request = json_encode($_SERVER);
            // $modb->insertData('dc_buyer_leads_rejected', $dataLogs);
            // echo json_encode($returnVal);
            // exit();
        } else {
            //let platformArray = array('Web', 'WAP', 'AndroidApp', 'iOSApp', 'WindowsApp');
            let subsourcearray = await processLead.getSubsourceMapped(params.lead_source, params.source_id);
            let sub_source_id = '';
            if ((params.lead_source != '') && (params.source_id == 90 || params.source_id == 368) && (subsourcearray.id > 0)) {
                let sub_source_id = subsourcearray.id;
            }
            if (params.lead_platform == '') {
                params.lead_platform = getPlatForm(sub_source_id);
            } else {
                params.lead_platform = filterPlatForm(params.lead_platform);
            }

            let pusheddata = JSON.stringify(params);
            data.source_id = params.source_id;
            if (params.source_id == 7) {
                data.sub_source_id = 1898;
            } else {
                data.sub_source_id = sub_source_id;
            }
            data.mobile = params.customer_mobile;
            if (params.dealer_id > 0) {
                data.dealer_id = params.dealer_id;
            }
            if (params.sub_source_dealer_id > 0) {
                data.dealer_id = params.sub_source_dealer_id;
            }

            data.data = pusheddata;

            let enrypteddata = md5(pusheddata);
            data.connecto_id = params.connecto_id;
            data.source_key = enrypteddata;
            
            if (isCustomerNameInvalid(params.customer_name) == false) {
                returnVal = { status: false, msg: 'Lead rejected due to invalid customer name.' };
                dataLogsduplicate.date = dateFormat("yyyy-mm-dd HH:MM:ss");
                dataLogsduplicate.source_id = params.source_id;
                dataLogsduplicate.source_lead_id = params.source_lead_id;
                dataLogsduplicate.data = pusheddata;
                dataLogsduplicate.response = returnVal;
                //$modb->insertData('buyer_leads_duplicate', $dataLogsduplicate);   
                processLead.insertOneMongo(dataLogsduplicate);
            } else {
                let checkDuplicacy = checkDuplicateData(enrypteddata, params.source_id);
                if (checkDuplicacy == 0) {
                    let log_id = await processLead.insertDataIntoRawLeadLogs;
                    returnVal = { status: true, msg: 'Saved Successfully.', lead_status: lead_status, 'lead_id': log_id };
                    // Additional Information          
                    dataadditional.raw_lead_id = log_id;
                    dataadditional.customer_mobile = data.mobile;
                    dataadditional.source_id = params.source_id;
                    dataadditional.sub_source_id = params.sub_source_id;

                    if (params.lead_source != '')
                        dataadditional.lead_source = params.lead_source;
                    else
                        dataadditional.lead_source = params.utm_source;

                    dataadditional.lead_platform = params.lead_platform;
                    dataadditional.lead_ux_source = params.lead_ux_source;

                    if (params.lead_campaign)
                        dataadditional.lead_campaign = params.lead_campaign;
                    else
                        dataadditional.lead_campaign = params.utm_campaign;

                    if (params.lead_content)
                        dataadditional.lead_content = params.lead_content;
                    else
                        dataadditional.lead_content = params.utm_content;

                    if (params.lead_device)
                        dataadditional.lead_device = params.lead_device;
                    else
                        dataadditional.lead_device = params.utm_device;

                    if (params.lead_term)
                        dataadditional.lead_term = params.lead_term;
                    else
                        dataadditional.lead_term = params.utm_term;

                    if (params.lead_medium)
                        dataadditional.lead_medium = params.lead_medium;
                    else
                        dataadditional.lead_medium = params.utm_medium;

                    dataadditional.lead_creative = params.lead_creative;
                    dataadditional.lead_keyword = params.lead_keyword;
                    dataadditional.lead_matchtype = params.lead_matchtype;
                    dataadditional.lead_network = params.lead_network;
                    dataadditional.lead_placement = params.lead_placement;
                    dataadditional.lead_adposition = params.lead_adposition;
                    dataadditional.is_whatsapp = (params.is_whatsapp) ? '1' : '0';
                    if ((params.otp_verified) && (params.otp_verified == '1' || params.otp_verified == '0')) {
                        dataadditional.otp_verified = params.otp_verified;
                    }
                    if ((params.ip_address) && !empty(params.ip_address)) {
                        dataadditional.ip_address = params.ip_address;
                    }

                    if (params.Location)
                        dataadditional.location = params.Location;
                    else
                        dataadditional.location = params.location;

                    dataadditional.source_lead_id = params.source_lead_id;

                    dataadditional.app_id = params.app_id;

                    //$DBNEWUB->insert('ublms_raw_leads_additional_params', $dataadditional);
                } else {
                    returnVal = { status: true, msg: 'Rejected Leads due to duplicate lead.', lead_status: lead_status, 'lead_id': log_id };
                    dataLogsduplicate.date = dateFormat("yyyy-mm-dd HH:MM:ss");
                    dataLogsduplicate.source_id = params.source_id;
                    dataLogsduplicate.source_lead_id = params.source_lead_id;
                    dataLogsduplicate.data = $_REQUEST;
                    dataLogsduplicate.source_key = enrypteddata;
                    dataLogsduplicate.response = returnVal;
                    //$modb->insertData('buyer_leads_duplicate', $dataLogsduplicate);
                    processLead.insertOneMongo(dataLogsduplicate);
                }
            }

        }
    }
}

const isCustomerNameInvalid = (name) => {
    let validRegEx = /^[A-Za-z\s]+$/;
    if (name.match(validRegEx)) {
        return false;
    } else {
        return true;
    }
}

