const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const WhatsappController = require("../../notifications/controller/WhatsappController");
const WhatsappNotificationModel = require("../../notifications/model/whatsappNotificationModel");
const WhatsappNotificationDetailsModel = require("../../notifications/model/whatsappNotificationDetailsModel");
const WalkinInfoModel = require("../../walkin/model/walkinInfoModel");
const LeadCarModel = require("../../lead/model/leadCarModel");
const ConversionModel = require("../../lead/model/conversionModel");


exports.sendWalkinScheduleNotification = async (req, res)=>{
  try{
    let params = req.query;
    let postParams = {};
        postParams['limit'] = params.limit || 100;

    //FIND SCHEDULED CARS 
    let walkinScheduleLeads = await WalkinInfoModel.getWalkingInfoForNotification(postParams);
    if(walkinScheduleLeads && walkinScheduleLeads.length){

        walkinScheduleLeads.forEach(async el=>{

          //SEND WHATSAPP NOTIFICATION
            let notificationParams = {lead_id: el['lead_id'],leads_car_id:el['leads_cars_id'], car_ids:[el['car_id']]};
            let newReq = Object.assign({}, req);
            newReq['body'] = notificationParams;
            newReq['body']['template_name'] = 'walking_reminder_cron';
            newReq['body']['template_id']   = '12';

            await WhatsappController.sendWhatsappNotification(newReq,res);

            //UPDATE NOTIFY FLAG
            await WalkinInfoModel.updateOne(el.id, {is_whatsapp_notified:1});

        });        
        
        ApiController.sendResponse(req, res, 200, "Notification sent successfully");

    }else{
        ApiController.sendResponse(req, res, 200, "No data found to process.");
    }
  }catch(error) {
    ApiController.sendResponse(req, res, 200, error);
  }
}

exports.whatsappCallback = async (req, res) => {
  try{

    let params = req.body;

    //GET INITIAL NOTIFICATION FROM NOTIFICATION ID
    let initialNotification = await WhatsappNotificationModel.get({id: params.notification_id});
    let calledOtion   = '';
    let detailsData   = {};
    let saveDetails   = {};
    let templateName  = '';
    let callbackReply = {};

    if(initialNotification && initialNotification[0].length){

      let parsedCarIds      = JSON.parse(initialNotification[0][0]['car_ids']);
      let notificationType  = initialNotification[0][0]['notification_type'];
      let requestData       = (initialNotification[0][0]['request_data']) ? JSON.parse(initialNotification[0][0]['request_data']) : {};

      templateName = requestData['data']['event_name'];

      if(notificationType == 'recommended_car'){
        calledOtion = parsedCarIds[params.called_option];
      }else{
        calledOtion = parsedCarIds['1'];
      }

      let parsedCarDetails = JSON.parse(initialNotification[0][0]['cars_detail']);
      let details = parsedCarDetails.filter(el=> el.car_id == calledOtion);

      if(details.length){
        detailsData['notification_id']  = params.notification_id;
        detailsData['called_option']    = params.called_option;
        detailsData['car_id']           = calledOtion;
        detailsData['car_details']      = JSON.stringify([details]);
        detailsData['request_data']     = '';

        //SAVE DETAILS
        saveDetails = await WhatsappNotificationDetailsModel.createOne(detailsData);
      }
      

      if(saveDetails && saveDetails.id){

        //SAVE CONVERSION DATA FOR SCHEDULE
        if(notificationType === 'walking_scheduled') {
            let conversationData = {};
        
            conversationData = {
              'lead_id': details[0]['lead_id'],
              'leads_cars_id': details[0]['id'],
              'dealer_id': details[0]['dealer_id'] || 0,
              'status_id': 5,
              'sub_status_id': 9,
              'calling_status_id': 0,
              'comment': (params.called_option == '1') ? 'Walk-In Re-scheduled by customer ' : 'Walk-In canceled by customer',
              'latest': '1',
              'added_on': new Date().toISOString(),
              'added_by': 0
            };
            ConversionModel.createOne(conversationData);
        }

        if((notificationType === 'walking_scheduled' || notificationType === 'walking_scheduled_reminder') && params.called_option == '2') {
          
          //CANCEL SCHEDULED WALKIN
          LeadCarModel.updateOne(details[0]['id'], {'walkin_reschedule_canceled': '1'});
          callbackReply['msg'] = 'Walkin canceled successfully';

        }else{
          //SEND WHATSAPP NOTIFICATION
          let notificationParams = {'template_name': templateName, notificationType, notification_id: params.notification_id, notification_detail_id: saveDetails.id, carDetails: details};
          let newReq = Object.assign({}, req);
          newReq['body'] = notificationParams;
          // newReq['body']['notification_type'] = 'walk_in_reminder_used';
          
          callbackReply = await WhatsappController.callbackReply(newReq,res);
        }

        

        ApiController.sendResponse(req, res, 200, callbackReply['msg']);
      }else{
        ApiController.sendResponse(req, res, 200, 'Details not found');
      }
    }else{
      ApiController.sendResponse(req, res, 200, 'Notification Id not found');
    }

  }catch(err){
    ApiController.sendResponse(req, res, 400, 'No data found.');
  }
}