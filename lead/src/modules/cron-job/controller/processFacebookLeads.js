const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const FBFormModel = require('../../lead/model/facebookFormModel');
const FBTokenModel = require('../../lead/model/facebookTokenModel');
const FBLeadsModel = require('../../lead/model/facebookLeadsModel');
const FBLeadTokenModel = require('../../lead/model/facebookLeadTokenModel');
const { FACEBOOK_LIMIT } = require("../../../../config/config");
const RawLeadLogs = require(MODULE_PATH+"lead/model/leadRawLogModel");
const LeadService = require(COMMON_MODEL + 'leadService');
var async = require("async");
const moment = require("moment");
const InventoryService = require(COMMON_MODEL + 'inventoryService');
const dateFormat = require('dateformat');
const CommonHelper = require('../../../helpers/CommonHelper');

exports.processFacebookLeads = async (req, res)=>{
    try{

        let params = req.query;
        

        let leadsFiltering = [];
        let fromDate = '';
        let toDate = '';

        if(params.from_date) {
            fromDate = params.from_date ;
            let parsedDate = (Date.parse(fromDate)).toString();
            parsedDate = parsedDate.substr(0, parsedDate.length-3);               

            leadsFiltering.push({"field": "time_created", "operator":"GREATER_THAN_OR_EQUAL", "value": ""+parsedDate})
        }else{
            let startOfDay = moment.utc().startOf('day'); //TODAY START DATE & TIME
            fromDate = startOfDay._d;
            let startOfDayDate = (Date.parse(startOfDay._d)).toString(); //CONVERT TO TIMRSTRING
                startOfDayDate = startOfDayDate.substr(0, startOfDayDate.length-3);
            
            leadsFiltering.push({"field": "time_created", "operator":"GREATER_THAN_OR_EQUAL", "value": ""+(startOfDayDate)})
        }

        toDate = new Date().toISOString(); //TODAY CURRENT DATE & TIME


        let fbToken = await FBTokenModel.get();


        let fbLeadParams = {
                "access_token"    :   (fbToken && fbToken[0] && fbToken[0][0] && fbToken[0][0]['access_token']) ? fbToken[0][0]['access_token'] : '' ,
                // "fields"          :   FACEBOOK_FIELDS,
                "filtering"       :   JSON.stringify(leadsFiltering),
                "limit"           :    FACEBOOK_LIMIT
        }

        
        let infoParams = { status: 1, isPaginationApply: false }
        let searchFromDate   = new Date(fromDate).toLocaleDateString();
        //SEND TO DATE TO FB GRAPH API              
        searchFromDate = searchFromDate.split('/');
        infoParams['fromDate'] = searchFromDate[2]+'-'+(searchFromDate[0]<10?'0'+searchFromDate[0]:searchFromDate[0])+'-'+(searchFromDate[1]<10?'0'+searchFromDate[1]:searchFromDate[1]); 
        fromDate = infoParams['fromDate'];


        if(params && params.form_id){
            infoParams['form_id'] = params.form_id 
        }

        //GET ALL FORMS
        let getFbInfo = await FBFormModel.get(infoParams);
        
        let fbLeadStatus = [];

        if(getFbInfo && getFbInfo.length) {
            async.forEach(getFbInfo[0], async (formsData, callback) => {

                let formID = formsData['form_id'];
                
                let prevCursorToken = '';

                //IF SAME DATE RECORD EXISTS THEN GET CREATED DATE AND FETCH LATEST RECORDS FROM THAT DATE
                if(formsData['lead_from_created_date'] ){
                    let UTCseconds = (Date.parse(formsData['lead_from_created_date'])).toString();
                    UTCseconds = UTCseconds.substr(0, UTCseconds.length-3);
                    leadsFiltering[0]["value"] =  ""+(UTCseconds);
                    fromDate = formsData['lead_from_created_date'];
                    fbLeadParams = {...fbLeadParams, filtering: leadsFiltering}

                }

                let fbLeadRetrievalCounter = 1
                await fetchFacebookLeads(formsData, formID, fbLeadParams, fbLeadRetrievalCounter , prevCursorToken, fromDate,toDate , leadsFiltering).then(status=>{
                    fbLeadStatus = status;
                });
               
                callback();
            
            }, function(err) {
                if(fbLeadStatus && fbLeadStatus.length){
                    ApiController.sendResponse(req, res, 200, res.__('success'), fbLeadStatus);
                }else{
                    ApiController.sendResponse(req, res, 200, res.__('No facebook leads found'));
                }
            });
        }else{
            ApiController.sendResponse(req, res, 200, 'No leads found');
        }


    }catch(error) { 
        ApiController.sendResponse(req, res, 200, res.__('error'), error);
    }
}

fetchFacebookLeads = async (formsData, formID, fbLeadParams, fbLeadRetrievalCounter, firstPagePrevCursor='' , fromDate,toDate,  leadsFiltering, resultedLeadsParam=[]) => {
    
    return new Promise((resolve, reject) => {
        let leadTokenStatus = {}

        //FETCH FB LEADS
        LeadService.getFacebookLeads(formID, fbLeadParams).then(async resp=>{

            let counter = 0;
            //STORE TOKENS
            // if(resp.body && resp.body.paging) {
                let fbLeadToken = {};
                    fbLeadToken['facebook_form_id'] = formsData['id'];
                    fbLeadToken['from_date']        =   fromDate; //(fromDate.length==10) ? fromDate+ ' 00:00:00' : fromDate;
                    fbLeadToken['prev_cursor']      =   firstPagePrevCursor || ((resp.body.paging && resp.body.paging.cursors && resp.body.paging.cursors.before) ? resp.body.paging.cursors.before : '');
                    fbLeadToken['next_cursor']      =   (resp.body.paging && resp.body.paging.cursors && resp.body.paging.cursors.after) ? resp.body.paging.cursors.after : '';
                    fbLeadToken['prev_token']       =   (resp.body.paging && resp.body.paging.cursors && resp.body.paging.previous) ? resp.body.paging.previous : '';
                    fbLeadToken['next_token']       =   (resp.body.paging && resp.body.paging.cursors && resp.body.paging.next) ? resp.body.paging.next : '';
                    fbLeadToken['response']         =   JSON.stringify(resp.body)
                    fbLeadToken['created_at']       =   new Date().toISOString();
                    
                    leadTokenStatus =  await FBLeadTokenModel.createOne(fbLeadToken);
            // }
            
            if(resp.body && resp.body.data && resp.body.data.length){

                // let logdata = {};
                // //insert to mongo
                // logdata['added_on']     = new Date().toISOString();
                // logdata['form_id']      = formID;
                // logdata['city_id']      = formsData['city_id'];
                // logdata['form_data']    = resp.body ? JSON.stringify(resp.body) : '';

                // FBLeadsMongoModel.create(logdata);
               

                let resultedLeads = resultedLeadsParam;

                //PROCESS FB LEADS & SAVE IN DATABASE
                async.forEach(resp.body.data, async (results, callback) => {
                    counter++;
                    let leadData = {
                        car_id: results['retailer_item_id'] || 0,
                        fb_lead_id: results['id'] || 0,
                        facebook_form_id: formsData['id'],
                        created_at: new Date().toISOString(),
                        updated_at: new Date().toISOString(),
                        lead_created_date: results['created_time'],
                        lead_token_id: leadTokenStatus.id
                    };
                    
                    let leadDate    = Date.parse(results['created_time']);                    

                    
                    let field_data = results['field_data'];
                    
                        field_data.forEach(async infoData => {
                            if (infoData['name'] == 'full_name') {
                                leadData['customer_name'] = (infoData['values'] && infoData['values'][0]) ? infoData['values'][0] : '';
                            } else if (infoData['name'] == 'email') {
                                leadData['customer_email'] = (infoData['values'] && infoData['values'][0]) ? infoData['values'][0] : '';
                            } else if (infoData['name'] == 'phone_number' && infoData['values'] && infoData['values'][0]) {
                                let phone = cleanDataMobile(infoData['values'][0]);
                                leadData['customer_phone'] = phone;
                            } else if (infoData['name'] == 'free_form_text') {
                                leadData['free_form_text'] = (infoData['values'] && infoData['values'][0]) ? infoData['values'][0] : '';
                            }
                        });

                        

                        leadData['success']  = true;
                        
                        try{
                            
                            let alreadyExists = await FBLeadsModel.get({fb_lead_id: results['id']});
                            let fbLeadDataResp = {}
                                fbLeadDataResp['car_id']            = leadData['car_id'];
                                fbLeadDataResp['fb_lead_id']        = leadData['fb_lead_id'];
                                fbLeadDataResp['lead_created_date'] = leadData['lead_created_date'];
                                fbLeadDataResp['customer_email']    = leadData['customer_email'];
                                fbLeadDataResp['customer_name']     = leadData['customer_name'];
                                fbLeadDataResp['customer_phone']    = leadData['customer_phone'];
                                fbLeadDataResp['free_form_text']    = leadData['free_form_text'];
                                
                            //CHECK IF NOT EXISTS THEN CREATE ELSE SHOW ALREADY EXISTS
                            if(alreadyExists && !alreadyExists[0].length){
                                FBLeadsModel.createOne(leadData);
                                leadData['success']         = true;
                                leadData['msg']             = 'Lead added in database';
                                fbLeadDataResp['msg']       = leadData['msg'];
                                fbLeadDataResp['success']   = leadData['success'];
                                resultedLeads.push(fbLeadDataResp);
                            }else if(alreadyExists && alreadyExists[0].length){
                                leadData['success']         = false;
                                leadData['msg']             = 'Lead exists in database';
                                fbLeadDataResp['msg']       = leadData['msg'];
                                fbLeadDataResp['success']   = leadData['success'];
                                resultedLeads.push(fbLeadDataResp);
                            }
                        }catch(err){

                        }  
                    callback();
                
                }, async (err) => { 
                    if(resp.body && resp.body.paging) {
                        //IN CASE OF PAGINATION PROCESS IN LOOP
                        if(resp.body.paging.cursors && resp.body.paging.next){
                            fbLeadParams['after'] = resp.body.paging.cursors.after;
                            if(fbLeadParams['before']) {
                                delete fbLeadParams['before'];
                            }
                            fbLeadParams["filtering"] =  JSON.stringify(leadsFiltering)
                            fbLeadRetrievalCounter++;
                            let d = await fetchFacebookLeads(formsData, formID, fbLeadParams, fbLeadRetrievalCounter, resp.body.paging.cursors.before , fromDate, toDate, leadsFiltering, resultedLeads).then(results=>{
                            
                            });
                            return resolve({data:resultedLeads})
                        }
                        //ELSE SEND ALL DATA
                        else if(resp.body && resp.body.paging && !resp.body.paging.next) {
                            let returnResp =  (resultedLeads.length) ? {data: resultedLeads} : {};
                            return resolve(returnResp);
                        }
                    }

                });

            }else{
                return resolve({data:resultedLeadsParam});
            }

        
            
        }).catch(function (err) {
            return reject({error: err})
            // Error
        });

    });
}

cleanDataMobile = (mobile) => {
    if (mobile && mobile.indexOf('+62') != -1) {
        mobile = mobile.replace('+62', '');
    }
    if (mobile && mobile.indexOf('+63') != -1 ) {
        mobile = mobile.replace('+63', '');
    }
    // let mobileNumberLen = mobile.length;
    // if (mobileNumberLen > 10) {
    //     mobile = mobile.substr(0 , 10);
    // }
    return mobile;
}

getCityId = (city) => {
    return 1;
}

formatAddleadDataJson = (leadData) => {

    let leadDataArr = {"cars": {}, "customer": {}, "source": {} };

    if((leadData['customer_name'])) {
        leadDataArr['customer']['name'] = leadData['customer_name'];
    }
    if(leadData['customer_phone']) {
        leadDataArr['customer']['mobile'] = leadData['customer_phone'];
    }
    if(leadData['customer_email']) {
        leadDataArr['customer']['email'] = leadData['customer_email'];
    }
    if(leadData['lead_created_date']) {
        leadDataArr['customer']['lead_created_at'] = leadData['lead_created_date'];
    }
    if(leadData['lead_platform']) {
        leadDataArr['source']['platform'] = leadData['lead_platform'];
    }
    if(leadData['source_id']) {
        leadDataArr['source']['source_id'] = leadData['source_id'];
    }
    if(leadData['sub_source_id']) {
        leadDataArr['source']['sub_source_id'] = leadData['sub_source_id'];
    }
    if(leadData['source_lead_id']) {
        leadDataArr['source']['source_lead_id'] = leadData['source_lead_id'];
    }
    if(leadData['car_id']) {
        leadDataArr['cars']['car_id'] = leadData['car_id'];
    }
    return JSON.stringify(leadDataArr);
}

/**
 * 
 * Process saved facebook leads save to raw leads table
 */

exports.processSavedFbLeads = async (req, res) => {

    let params = req.query;

    params['page_limit']            =   req.query.limit || 100;
    params['isPaginationApply']     =   true;
    params['is_sent_to_raw_leads']  =   '0';
    params['status']  =   '1';

    let getFbLeads = await FBLeadsModel.get(params);

    
    if(getFbLeads && getFbLeads[0] && getFbLeads[0].length) {
        // getFbLeads[0].forEach(async el=>{
            
        let processLeads = [];

        async.forEach(getFbLeads[0], async (el, callback) => {

            let leadData = el;

            leadData['source_id'] = '369';
            leadData['sub_source_id'] = '1928';
            // cityId = leadData['customer_city'] ? leadData['customer_city'] : 0;
            leadData['lead_platform'] = 'WEB';
            leadData['source_lead_id'] = el.id;
            let dataJson =  formatAddleadDataJson(leadData);

            //CHECK IF NAME/MOBILE/CAR ID EXISTS
            if(el.car_id && (el.customer_phone && (el.customer_phone).trim() != '' && (el.customer_phone).trim() != '0') && (el.customer_name).trim() !== '') {
                let conditionInsert = {
                    'source_id'     : leadData['source_id'],
                    'sub_source_id' : leadData['sub_source_id'],
                    'mobile'        : leadData['customer_phone'],
                    'leads_data'    : dataJson,
                    'log_date'      : new Date().toISOString(),
                    'city_id'       : 0,
                    'pushed_status' : '0',
                    'apitype'       : '0',
                    'is_ub'         : '1'
                }

                processLeads.push(conditionInsert);

                let rawLead = await RawLeadLogs.create(conditionInsert);

                if(rawLead) {
                    await FBLeadsModel.updateOne(el['id'], {is_sent_to_raw_leads:'1', raw_lead_id: rawLead.id});
                }


                //SEND TO DC
                let dates = new Date();
                let inc2hours = 2000 * 60 * 60 // 2 hours
                
                let dcParams = {};
                    dcParams['cust_name'] = leadData['customer_name'];
                    dcParams['cust_mobile'] = leadData['customer_phone'];
                    dcParams['cust_email'] = leadData['customer_email'];
                    dcParams['lead_source'] = 'Facebook';
                    dcParams['lead_status'] = 'New';
                    dcParams['followup_date'] = dateFormat(new Date( dates.getTime() + inc2hours ), 'yyyy-mm-dd HH:MM');
                    dcParams['sub_source'] = 'Marketplace';
                    dcParams['car_id'] = [leadData['car_id']];


                    //GET DEALER ID FROM CAR DETAIL
                    let stockPostData = {};
                    stockPostData['call_server'] = 'ub';
                    stockPostData['car_status'] = [1,2];
                    stockPostData['stock_used_car_id_arr'] = [leadData['car_id']];

                    let leadsCarDetails = await InventoryService.getStockList(req, stockPostData); 
                    if(leadsCarDetails && leadsCarDetails.status == 200 && leadsCarDetails.data.length){
                        dcParams['dealer_id'] = leadsCarDetails.data[0]['dealer_id'];
                        
                        //IF PAID SCIRE IS NOT 1 (FREE)
                        if(process.env.COUNTRY_CODE == 'id') {
                            let then = new Date(leadData['lead_created_date']);
                            let now = new Date();
                            let timeDifference = await CommonHelper.timeDifference(then, now);
                            if(timeDifference && timeDifference['month'] < 6){
                                await LeadService.sendFbLeadToDC(req, dcParams);
                            }

                        }else if(process.env.COUNTRY_CODE == 'ph' && leadsCarDetails.data[0]['paid_score'] != 1 ){
                                await LeadService.sendFbLeadToDC(req, dcParams);
                        }
                    }

            }else{
                //STATUS FAILED
                await FBLeadsModel.updateOne(el['id'], {status:'2'});
            }
                callback();
            
            }, function(err) {
                let msg = 'No leads found';
                if(processLeads.length){
                    msg = 'Leads added successfully';
                }
                ApiController.sendResponse(req, res, 200, msg);
            });
    }else{
        ApiController.sendResponse(req, res, 200, "No leads found to process");

    }

}


exports.refreshFbToken = async (req, res) => {
    try{
        let params = {};
        let result = {};

        params['grant_type']        =   'fb_exchange_token';
        
        //FIND TOKEN FROM DB
        let fbToken = await FBTokenModel.get();

        if(fbToken && fbToken[0] && fbToken[0].length){

            params['fb_exchange_token'] =   fbToken[0][0]['access_token'];
            params['client_id']         =   fbToken[0][0]['app_id'];
            params['client_secret']     =   fbToken[0][0]['app_secret'];

            //TOKEN REFRESH API CALL
            await LeadService.refreshFbToken(params).then(async resp=>{

                //UPDATE TOKEN
                if(resp.body && resp.body.access_token){
                    result = resp.body;
                    await FBTokenModel.updateOne(fbToken[0][0]['id'], {'access_token': resp.body.access_token,'expires_in':resp.body.expires_in});
                }
            });

            ApiController.sendResponse(req, res, 200, "Facebook token udpated successfully", result);

        }else{
            ApiController.sendResponse(req, res, 200, "APP ID and APP SECRET not Found");

        }        

    }catch(err){
        ApiController.sendResponse(req, res, 200, "please try again");
    }
}