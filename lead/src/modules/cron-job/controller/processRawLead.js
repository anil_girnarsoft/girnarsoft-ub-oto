const Q = require("q");
const _ = require("lodash");
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const processLead = require('./../model/processLead');
const rawLeadModel = require('./../../lead/model/leadRawLogModel');
const processLeadCron = require('../../../../cron/processLeadCron');
const otherDealerLeads = require('../../lead/model/otherDealerLeadsModel');
const rawLeadsAdditionalParamModel = require('../../lead/model/rawLeadsAdditionalParamModel');
const LeadRawLogModel = require("./../../lead/model/leadRawLogModel");
const asyncLeadModel = require("./../../lead/model/asyncLeadModel");
const carNotExistsMongoModel = require("./../../lead/model/carNotExistsMongoModel");
const blockedRawLeadsLogModel = require("./../../lead/model/blockedRawLeadsLogModel");
const otherDealersCarOrderModel = require("./../../lead/model/otherDealersCarOrderModel");

const InventoryService = require(COMMON_MODEL + 'inventoryService');
const DealerService = require(COMMON_MODEL + 'dealerService');
const dateFormat = require('dateformat');
const { response } = require("express");
const { async, resolve } = require("q");
const LeadModel = require('./../../lead/model/leadModel');

exports.ProcessRawLead = async (req, res, next) => {
    // let rawLeadData = await processLead.getRawData();

    let i = 0;
    let checkleads = await rawLeadModel.checktotalleads();

    // start Fetch unprocessed Leads and locked
    let fetchLeads = await rawLeadModel.unprocessedLeads();

    let logId = '';

    if (fetchLeads.length) {
        logId = fetchLeads.map(v => v.id);
        let updateLeads = await rawLeadModel.updateMultiple({ id: logId.toString(), 'pushedStatus': '2' });
    }

    // End Fetch unprocessed Leads and locked 

    // Start lead Processing
    for (let element of fetchLeads) {        
        try {
            let carId = checkCustomer = 0;
            let userType = dealerOwner = "";
            let otpVerified = isWhatsapp = leadDealerId = 0;
            let carMisMatchFlag = false;
            let logDataResp = JSON.parse(element['leads_data']);
            let response, blockingType = '';
            let logDataRes = {};
            let dealerDetailsData = null;
            let carDealerdata = {};

            //SET OBJECT OF CUSTOMER & SOURCE
            logDataRes['customer'] = {};
            logDataRes['source'] = {};

            logDataRes['customer']['name'] = (logDataResp['customer'] && logDataResp['customer']['name']) ? logDataResp['customer']['name'] : (logDataResp['customer_name'] || '');
            logDataRes['customer']['email'] = (logDataResp['customer'] && logDataResp['customer']['email']) ? logDataResp['customer']['email'] : (logDataResp['customer_email'] || '');
            logDataRes['customer']['mobile'] = (logDataResp['customer'] && logDataResp['customer']['mobile']) ? logDataResp['customer']['mobile'] : (logDataResp['customer_mobile'] || '');
            logDataRes['customer']['city'] = (logDataResp['customer'] && logDataResp['customer']['city']) ? logDataResp['customer']['city'] : (logDataResp['customer_city'] || '');
            logDataRes['source']['source_id'] = (logDataResp['source'] && logDataResp['source']['source_id']) ? logDataResp['source']['source_id'] : (logDataResp['source_id'] || '');
            logDataRes['source']['sub_source_id'] = (logDataResp['source'] && logDataResp['source']['sub_source_id']) ? logDataResp['source']['sub_source_id'] : (logDataResp['sub_source_id'] || '');
            logDataRes['source']['source_lead_id'] = (logDataResp['source'] && logDataResp['source']['source_lead_id']) ? logDataResp['source']['source_lead_id'] : (logDataResp['source_lead_id'] || '');
            logDataRes['lead_from'] = (logDataResp['source'] && logDataResp['source']['lead_from']) ? logDataResp['source']['lead_from'] : (logDataResp['lead_from'] || '');
            logDataRes['platform'] = (logDataResp['source'] && logDataResp['source']['platform']) ? logDataResp['source']['platform'] : (logDataResp['platform'] || '');
            logDataRes['log_date'] = element['log_date'];
            logDataRes['created_date'] = (logDataResp['customer'] && logDataResp['customer']['lead_created_at']) ? logDataResp['customer']['lead_created_at'] : (logDataResp['lead_created_at'] || new Date().toISOString());
            let premium = (logDataResp['customer'] && logDataResp['customer']['is_premium']) ? logDataResp['customer']['is_premium'] : (logDataResp['is_premium'] || 0);
            if(premium == 0){
                logDataRes['premium'] = 2;
            }else{
                logDataRes['premium'] = premium;
            }

            if (logDataResp && logDataResp['cars'] && logDataResp['cars']['car_id'] && logDataResp['cars']['car_id'] > 0) {
                carId = logDataResp['cars']['car_id'];
                logDataRes['car_id'] = carId;
            }
            if (logDataResp['car_id'] && logDataResp['car_id'] > 0) {
                carId = logDataResp['car_id'];
            }
            if (logDataResp && logDataResp['customer'] && logDataResp['customer']['otp_verified'] && logDataResp['customer']['otp_verified'] == 1) {
                otpVerified = 1;
            } else if (logDataResp['otp_verified'] && logDataResp['otp_verified'] == 1) {
                otpVerified = 1;
            }
            if (logDataResp && logDataResp['marketing'] && logDataResp['marketing']['is_whatsapp'] && logDataResp['marketing']['is_whatsapp'] == 1) {
                isWhatsapp = 1;
            } else if (logDataResp['is_whatsapp'] && logDataResp['is_whatsapp'] == 1) {
                isWhatsapp = 1;
            }

            if (logDataResp && logDataResp['dealer'] && logDataResp['dealer']['dealer_id']) {
                leadDealerId = logDataResp['dealer']['dealer_id'];
            } else if (logDataResp['dealer_id']) {
                leadDealerId = logDataResp['dealer_id'];
            }
            let rawLeadData = [];
            let carNotValid = 0;
            if (carId > 0) {
                let promisesRes = carDetailsdata = promisesDealerRes = dealerById = '';
                try {
                    promisesRes = await Q.allSettled([InventoryService.getStockList(req, { stock_used_car_id_arr: [carId], call_server: 'ub', car_status: [1, 2] })]);
                    carDetailsdata = promisesRes && _.get(promisesRes, "[0].value.data");
                    if (carDetailsdata.length == 0) {
                        carNotValid = 1;
                        rawLeadData = { 'pushed_status': '2', 'updated_date': new Date().toISOString(), 'reason': 'Car data not available', 'lead_id': 0, id: element['id'] };
                        await LeadRawLogModel.statusUpdate(rawLeadData);
                    }
                    carDealerdata = carDetailsdata[0];
                    if (carDealerdata) {
                        promisesDealerRes = await Q.allSettled([
                            DealerService.getDealerDetailsByIds(req, { dealer_id: [carDealerdata.dealer_id] })
                        ]);
                        dealerById = promisesDealerRes && _.get(promisesDealerRes, "[0].value.data");
                        dealerDetailsData = dealerById[0];

                        userType = (carDealerdata && carDealerdata['user_type']) || '';
                        dealerId = (carDealerdata && carDealerdata['dealer_id']) || 0;
                        if (userType == 'D') {
                            i++;
                        }
                    }
                } catch (error) {
                    // console.log('Error---:', error);
                }
            }
            /*
            * store leads of corporate dealers in ublms_other_dealer_leads
            */
            let otherDealerData = {};

            let responseData = [];
            let leadId = 0;
            if ((dealerDetailsData) && (logDataRes['customer']['mobile'] == dealerDetailsData.dealership_contact) && (dealerDetailsData.dealership_contact)) {
                response = "Rejected, Seller make buyer lead on own car.";
            } else {
                if (element['apitype'] == '1') {
                    // $responsedata = $this->proccssleadsNew($logdatares, $logdata['source_id'], $logdata['sub_source_id'], $logdata['log_id'], $logdata['mobile'], $logdata['city_id']);
                } else {
                    responseData = await proccssleads(req, logDataRes, element['source_id'], element['sub_source_id'], element['log_id']);
                    leadId = (responseData)?responseData.lead_id:0;
                    response = (leadId)?"lead added successfully":'Lead id not created';
                }
            }

            let current_date = new Date().toISOString();
            if (carId > 0 && userType == '') {
                if(carNotValid==0){
                    arrCarNotExist = {
                        'mobile': element['mobile'], 'log_id': element['id'],
                        'added_date': current_date
                    }
                    await carNotExistsMongoModel.create(arrCarNotExist, function (err, res) {
                        if (err) { }
                    });
                    rawLeadData = { 'pushed_status': '3', 'updated_date': current_date, 'reason': response, 'lead_id': leadId, id: element['id'] };
                    await LeadRawLogModel.statusUpdate(rawLeadData);
                }
            } else {
                rawLeadData = { 'pushed_status': '1', 'updated_date': new Date().toISOString(), 'reason': response, 'lead_id': leadId, id: element['id'] };
                await LeadRawLogModel.statusUpdate(rawLeadData);
                if (blockingType) {
                    let arrBlockedLeads = { 'raw_lead_id': element['log_id'], 'mobile': element['mobile'], 'blocked_type': blockingType, 'lead_added_date': element['log_date'], 'added_date': current_date }
                    await blockedRawLeadsLogModel.createOne(arrBlockedLeads);
                }
            }            
        } catch (error) {
            // console.log('Process Leads error----', error);
            let errlead = {'updated_date': new Date().toISOString(), 'reason': "Lead not created"};
            await blockedRawLeadsLogModel.createOne(errlead);
        }
    }

    ApiController.sendResponse(req, res, 200, 'leads are added successfully');
}

saveAsyncLeadData = async (params) => {
    let domainId = '';

    if (params['sub_source_id'] == 1937 || params['sub_source_id'] == 1938) {
        domainId = '4';
    } else if (params['source_id'] == 4) {
        domainId = '1';
    } else if (params['source_id'] == 368) {
        domainId = '3';
    } else {
        domainId = '2';
    }

    let apiType = {};
    let ayncLeadsData = {};

    apiType['is_whatsapp'] = params['is_whatsapp'];
    apiType['customer_name'] = params['name'];
    apiType['customer_email'] = params['email'];
    apiType['customer_mobile'] = params['mobile'];
    apiType['car_id'] = params['carId'];
    apiType['otp_verified'] = (params['otp_verified'] == '1') ? '1' : '0';
    apiType['is_outstation'] = (params['distance'] >= CAR_DISTANCE_CUSTOMER) ? '1' : '0';

    ayncLeadsData['api_data'] = JSON.stringify(apiType);
    ayncLeadsData['domain_id'] = domainId;
    ayncLeadsData['api_type'] = 'verify_buyer_lead';
    ayncLeadsData['mobile'] = params['mobile'];
    ayncLeadsData['car_id'] = params['carId'];
    ayncLeadsData['lead_dealer_id'] = params['leadDealerId'];
    ayncLeadsData['lead_id'] = params['lead_id'];
    ayncLeadsData['added_date'] = new Date().toISOString();
    ayncLeadsData['created_at'] = new Date().toISOString();
    await asyncLeadModel.createOne(ayncLeadsData);

}

updateOtherDealersCarOrder = async (carId) => {
    let otherDealerCarOrder = await otherDealersCarOrderModel.get({ carId: carId });

    let curDate = dateFormat('yyyy-mm-dd HH:MM:ss');
    if (otherDealerCarOrder['0'] && otherDealerCarOrder['0']['lead_count']) {
        let arrUpdate = { 'lead_count': otherDealerCarOrder['0']['lead_count'] + 1, 'last_updated_date': curDate };
        await otherDealersCarOrderModel.updateOne(cardId, arrUpdate);
    } else {
        let arrInsert = { 'car_id': carId, 'lead_count': 1, 'last_updated_date': curDate };
        await otherDealersCarOrderModel.createOne(arrInsert);
    }
}

const proccssleadsNew = async (leadLogData, sourceId, subSourceId, logId, mobile, cityId) => {
    // global $modb;
    let blockingType = 0;
    let isBlockedCustomerBypass = false;
    let customerName, customerEmail, customerCity, leadPlatform, customerLocation, adGroupID = '';
    let name, email, locationId, lat, lon, isWhatsapp = '';
    let dndCheck, otpVerified = 0;
    
    saveLeadFilter(logId, leadLogData);


    adGroupID = (leadLogData['marketing']['ad_group_id']) ? leadLogData['marketing']['ad_group_id'] : "";

    let customerMobile = mobile;
    customerName = leadLogData['customer']['name'];
    customerEmail = leadLogData['customer']['email'];
    locationId = leadLogData['customer']['location_id'];
    lat = leadLogData['customer']['lat'];
    lon = leadLogData['customer']['lon'];
    otpVerified = leadLogData['customer']['otp_verified'];
    financeRequired = leadLogData['customer']['finance_required'];
    connectoId = leadLogData['customer']['connecto_id'];
    isWhatsapp = (leadLogDat['marketing']['is_whatsapp'] && leadLogData['marketing']['is_whatsapp'] == '1') ? '1' : '0';
    deviceId = (leadLogData['customer'][PUSH_TOKEN]) ? leadLogData['customer'][PUSH_TOKEN] : null;

    let customerCentralCityId = leadLogData['customer']['city_id'];
    let whatsappOptIn = (leadLogData['customer']['whatsapp_opt_in']) ? leadLogData['customer']['whatsapp_opt_in'] : '0';

    let customerRes = '';
    if (customerCentralCityId > 0) {
        customerRes = getCityNameByCentralCityID(customerCentralCityId);
        customerCity = customerRes['city_name'];
    }

    currentDatetime = new Date().toISOString();

    // saving log data to mongo db

    let logData = {};
    logData['mobile'] = mobile;
    logData['source'] = sourceIid;
    logData['source_id'] = sourceId;
    logData['added'] = currentDatetime;
    logData['data'] = JSON.stringify(leadLogData);
    // $modb->insertData('ublms_leads_log', $logdata);

    // Checking customer(s) mobile number
    // Verifying if lead is originating from Dealers mobile
    dealerLeadStatus = checkMobileAuthentication(customerMobile, leadLogData);

    dndCheck = '';
    if (subSourceId == 487 || otpVerified != '1') {
        dndCheck = checkMobileNDNC(customerMobile);
        if (dndCheck == '1') {
            let arrDnc = { 'raw_lead_log_id': logId, 'created_date': dateFormat('yyyy-mm-dd HH:MM:ss') };
            // $this->db->insert(array('table' : UBLMS_DNC_RAW_LEAD_MAPPING, 'values' => $arrDnc));
        }
    }

    let returnVal = {};
    let response = {};

    if (dndCheck == 1 && subSourceId == 487) {
        returnVal = { 'status': false, 'msg': 'Lead Sub Source in Mobile Intrest and Mobile number in NDNC.' };
        response['response'] = JSON.stringify(returnVal);
        response['lead_id'] = 0;
    } else if (dealerLeadStatus) {
        returnVal = { 'lead_status': "Rejected", 'reason': "Rejected as lead from dealers mobile", 'lead': false };
        response['response'] = JSON.stringify(returnVal);
        response['lead_id'] = 0;
    } else if (sourceId == 475 && (leadLogData['cars']['car_id'] <= 0 || leadLogData['cars']['car_id'] == '')) {
        returnVal = { 'lead_status': "Rejected", 'reason': "Spam Leads ", 'lead': false };
        response['response'] = JSON.stringify(returnVal);
        response['lead_id'] = 0;
    } else {

        // When Source is `dealer` only if source detail is not Empty
        if (sourceId == '7')
            subSourceId = 1898;

        customerData = getCustomerDetail(customerMobile);

        customerCityId, customerLocationId, finance, purchaseTime = '';
        $lead_date = dateFormat('yyyy-mm-dd HH:MM:ss');
        checkCampaign = '';
        sourceLeadId = knowlarityDealerId = tempContactId = NULL;
        if (leadLogData['source']['source_lead_id'])
            sourceLeadId = (leadLogData['source']['source_lead_id']);

        if (customerName)
            customerName = cleanString(customerName);

        if (customerEmail)
            customerEmail = customerEmail;

        customerCityId = cityId;

        if (leadLogData['source']['platform'])
            leadPlatform = leadLogData['source']['platform'];

        if (leadLogData['customer']['finance_required'])
            finance = leadLogData['customer']['finance_required'];

        if (leadLogData['purchase_time'])
            purchase_time = (leadLogData['purchase_time']);

        if (leadLogData['dealer']['dealer_id'])
            knowlarity_dealer_id = leadLogData['dealer']['dealer_id'];

        if (leadLogData['temp_contact_id'])
            temp_contact_id = (leadLogData['temp_contact_id']);

        if (leadLogData['sub_source_dealer_id'])
            knowlarity_dealer_id = leadLogData['sub_source_dealer_id'];

        // Verifying if customer already exist or not
        insertLead = false;
        leadStatus = '';
        if (customerData == '') {
            // Inserting the New Customer detail
            leadStatus = 'New';
            leadStatusReason = "leadStatus customer lead";
            insertLead = true;
            mobileTrueLocation = 'Code Not Exist';
            telecomLocation = getMobileLocation(customerMobile);
            if (telecomLocation && typeof telecomLocation == 'object') {
                mobileTrueLocation = telecomLocation['mobile_telecom_circle'];
            }

            customerEmailId = isValidCustomerEmail(customerEmail) ? customerEmail : '';
            customerDetails = {
                'customer_name': customerName,
                'customer_city_id': customerCityId,
                'customer_city_name': customerCity,
                'customer_email': customerEmailId,
                'customer_mobile': customerMobile,
                'alternate_email': '',
                'alternate_mobile': '',
                'customer_location_id': customerLocationId,
                'cutomer_location_name': customerLocation,
                'customer_added': new Date().toISOString(),
                'active': '1',
                'ndnc': dndCheck,
                'mobile_true_location': mobileTrueLocation
            }
            //  debug($customer_details);die;
            // $this->db->insert(array('table' => UBLMS_CUSTOMER, 'values' => $customer_details));
            // customerId = $this->db->lastInsertId();
        } else {
            customerId = customerData[0]['customer_id'];
            customersLeadData = getCustomersLeadDetail(customerId, true);
            arrCustomerInfoUpdate = { 'ndnc': dndCheck, 'updated_on': dateFormat('yyyy-mm-dd HH:MM:ss') };
            if (customerData[0]['customer_city_id'] == '' && customerCityId) {
                arrCustomerInfoUpdate['customer_city_id'] = customerCityId;
            }
            if (customerData[0]['customer_email'] && customerEmail) {
                customerEmailId = isValidCustomerEmail(customerEmail) ? customerEmail : '';
                if (customerEmailId) {
                    arrCustomerInfoUpdate['customer_email'] = customerEmailId;
                }
            }

            // $customer_condition = array('table' => UBLMS_CUSTOMER,
            //     'values' => $arrCustomerInfoUpdate,
            //     'condition' => array('customer_id' => customerId));
            // $this->db->update($customer_condition);

            if (!(customerData[0]['active'])) {
                arrBlockingStatus = getBlockingInfo(customerId);
                if (arrBlockingStatus && arrBlockingStatus['0']['blocked_reason']) {
                    blockingType = arrBlockingStatus['0']['blocked_reason'];
                    if (arrBlockingStatus['0']['blocked_reason'] == '4') {
                        isBlockedCustomerBypass = true;
                    }
                }
            }
            if (customerData[0]['active'] || isBlockedCustomerBypass) {
                if (customersLeadData.length > 0) {
                    lastLeadDate = customersLeadData[0]['added_date'];
                    currentDate = dateFormat("yyyy-mm-dd");
                    dateDifference = dateDifference(lastLeadDate, currentDate);

                    if ((customersLeadData[0]['status_id'] == '7') || (customersLeadData[0]['status_id'] == '6' && customersLeadData[0]['sub_status_id'] == '12')) {
                        leadStatus = 'Accepted';
                        leadStatusReason = "leadStatus lead as entered new lead of existing customer as earlier lead was marked Closed/Purchased";
                        insertLead = true;
                    } else if (((customersLeadData[0]['status_id'] == '3' && (customersLeadData[0]['sub_status_id'] == '3' || customersLeadData[0]['sub_status_id'] == '5')) || customersLeadData[0]['status_id'] == '5' || customersLeadData[0]['status_id'] == '6') && (dateDifference <= L2_LEAD_EXPIRATION)) {
                        leadStatus = 'Duplicate';
                        leadStatusReason = "L2 staged leadStatus lead and it's life is $date_difference days";
                        insertLead = false;
                    } else if (((customersLeadData[0]['status_id'] == '3' && (customersLeadData[0]['sub_status_id'] == '3' || customersLeadData[0]['sub_status_id'] == '5')) || customersLeadData[0]['status_id'] == '5' || customersLeadData[0]['status_id'] == '6') && (dateDifference >= L2_LEAD_EXPIRATION)) {
                        leadStatus = 'Accepted';
                        // closeLeadCondition = array('table' => UBLMS_LEADS,
                        //     'values' => array('status_id' => '7', 'sub_status_id' => '19', 'leads_status' => '0'),
                        //     'condition' => array('lead_id' => customersLeadData[0]['lead_id']));
                        // $this->db->update($close_lead_condition);
                        leadStatusReason = "Lead Closed at L2 staged and it's life is $date_difference days";
                        insertLead = true;
                    } else if (dateDifference <= LEAD_EXPIRATION && (customersLeadData[0]['status_id'] != '7' && customersLeadData[0]['sub_status_id'] != '12')) {
                        leadStatus = 'Duplicate';
                        leadStatusReason = "leadStatus lead as it's life is $date_difference days only";
                        insertLead = false;
                    } else if (dateDifference > LEAD_EXPIRATION) {
                        // $close_lead_condition = array('table' => UBLMS_LEADS,
                        //     'values' => array('status_id' => '7', 'sub_status_id' => '19', 'leads_status' => '0'),
                        //     'condition' => array('lead_id' => customersLeadData[0]['lead_id']));
                        // $this->db->update($close_lead_condition);
                        leadStatus = 'Accepted';
                        leadStatusReason = "leadStatus lead as last lead was AutoClosed due to life of $date_difference days";
                        insertLead = true;
                    }
                } else {
                    leadStatus = 'Accepted';
                    leadStatusReason = "leadStatus added successfully.";
                    insertLead = true;
                }
            } else {
                //$arrBlockingStatus = $this->getBlockingInfo(customerId);
                leadStatus = 'Rejected';
                leadStatusReason = "leadStatus lead as customer is inactive";
                if (arrBlockingStatus && $arrBlockingStatus['0']['blocked_reason']) {
                    if (arrBlockingStatus['0']['blocked_reason'] == '3') {
                        blockingReason = 'Scraping agent';
                    } else {
                        // global $blockedReasonArr;
                        blockingReason = blockedReasonArr[arrBlockingStatus['0']['blocked_reason']];
                    }
                    leadStatusReason = "leadStatus it was a " + $blockingReason;
                }
                insertLead = false;
            }
        }




        if (['web', 'androidapp', 'iosapp'].includes(leadLogData['source']['platform']) && (deviceId)) {
            //source cardekho sub-source Android
            if ((leadLogData['source']['platform']).toLowercase() == 'androidapp') {
                customerInfoDetails = { 'is_android': '1', 'android_device_id': deviceId, 'updated_date': new Date().toISOString() };
            }
            //source cardekho sub-source ios
            if ((leadLogData['source']['platform']).toLowercase() == 'iosapp') {
                customerInfoDetails = { 'is_ios': '1', 'ios_device_id': deviceId, 'updated_date': new Date().toISOString() };
            }
            //source cardekho sub-source web
            if (['web'].includes(leadLogData['source']['platform'])) {
                customerInfoDetails = { 'is_chrome': '1', 'chrome_push_id': deviceId, 'updated_date': new Date().toISOString() };
            }
            saveCustomerInfo(customerId, customerInfoDetails);
        }
        //source id 90 = cardekho
        if (sourceId == '90' && whatsappOptIn == 1) {
            customerInfoDetails = { 'whatsapp_opt_in': whatsappOptIn, 'updated_date': new Date().toISOString() };
            saveCustomerInfo(customerId, customerInfoDetails);
        } else if (sourceId == '90') {
            customerInfoDetails = { 'updated_date': new Date().toISOString() };
            saveCustomerInfo(customerId, customerInfoDetails);
        }

        // Evaluating the SubSource ID when lead source is available
        if (leadLogData['marketing']['utm_source'] && leadLogData['marketing']['utm_source'] != "" && subSourceId == '') {
            leadSource = leadLogData['marketing']['utm_source'];
            // getLeadSourceCondition = array('table' => UBLMS_SUB_SOURCE,
            //     'fields' => array('sub_source_id', 'sub_source_description'),
            //     'condition' => array('source_id' => sourceId, 'sub_source_name' => $lead_source, 'active' => '1'));
            // $leadSourceResult = $this->db->select($getLeadSourceCondition);
            if (leadSourceResult) {
                subSourceId = leadSourceResult[0]['sub_source_id'];
            }
        }
        // Getting sub source through lead source is available by mapped table
        // Inserting the Leads log data
        let leadsLog = {
            'source_lead_id': sourceLeadId,
            'temp_contact_id': tempContactId,
            'customer_id': customerId,
            'customer_mobile': customerMobile,
            'customer_name': customerName,
            'customer_email': customerEmail,
            'customer_city': customerCity,
            'source_id': sourceId,
            'sub_source_id': subSourceId,
            'dealer_id': knowlarityDealerId,
            'added_on': new Date().toISOString(),
            'lead_status': leadStatus,
            'leads_status_reason': leadStatusReason,
            'lead_platform': leadLogData['source']['platform'],
            'params_string': JSON.stringify(leadLogData)
        }
        // $this->db->insert(array('table' : UBLMS_LEADS_LOG, 'values' : $leads_log));
        // $leads_log_id = $this->db->lastInsertId();

        // Inserting the accepted leads data
        leadId = '';
        if (insertLead && (leadStatus == 'New' || leadStatus == 'Accepted')) {
            let callingLead = {
                'source_lead_id': sourceLeadId,
                'customer_id': customerId,
                'source_id': sourceId,
                'sub_source_id': subSourceId,
                'status_id': '1',
                'sub_status_id': '1',
                'calling_status_id': '1',
                'dealer_id': knowlarityDealerId,
                'ratings': '',
                'is_finance_req': finance,
                'purchase_time': purchaseTime,
                'due_date': dueDateInLead(currentDatetime),
                'added_date': leadLogData['created_date'],
                'last_updated_by': '0',
                'platform': leadLogData['source']['platform'],
                'connecto_id': leadLogData['customer']['connecto_id'],
                'leads_status': '1'
            };
            // if (leadLogData['log_date'] != '') {
            //     callingLead['added_date'] = leadLogData['log_date'];
            // }
            // $this->db->insert(array('table' : UBLMS_LEADS, 'values' : $calling_lead));
            // leadId = $this->db->lastInsertId();
            // $arrCustomerLeadsUpdate['last_became_lead_on'] = date('Y-m-d H:i:s');
            //     $customer_condition = array('table' : UBLMS_CUSTOMER,
            //     'values' : $arrCustomerLeadsUpdate,
            //     'condition' : array('customer_id' : customerId));
            // $this->db->update($customer_condition);
            // adding Leads additional Params
            AddAdditionalLeadParams(logId, leadId, sourceId, subSourceId, adGroupID);
            leadDate = dateFormat("yyyy-mm-dd");

            let newLeadsConvData = {
                'lead_id': leadId,
                'status_id': '1',
                'sub_status_id': '1',
                'calling_status_id': '1',
                'comment': leadStatus + ' Lead',
                'latest': '1',
                'added_on': new Date().toISOString(),
                'added_by': '0'
            };
            // $this->db->insert(array('table' : UBLMS_CONVERSATION, 'values' : newLeadsConvData));
        } else {
            if (customersLeadData && customersLeadData) {
                leadId = customersLeadData[0]['lead_id'];
            }
        }

        // Inserting the customers car preference
        queryString, carId, carMake, carModel = '';
        carMakeId, carModelId, carVerId, gaadiVersionId = '';
        carMakeName, carModelName, carVerName = '';
        if (leadLogData['cars']['carId'] && leadLogData['cars']['carId'] != '' && leadLogData['cars']['carId'] != '0') {
            carId = leadLogData['cars']['carId'];
            sellertype = getSellerTypeOfCar(leadLogData['cars']['carId']);

        }

        // Setting customers car preference data
        dialerCity = customerCityId;
        bodyStyle, carCity, transmission, color, fuel, maxPrice, minPrice, maxYear, minYear, maxKm, minKm, listingType, certificationId = '';
        if (carId != "" && typeof carId == 'number') {
            usedCarData = getUsedCarDetail(carId);

            if (usedCarData) {
                carMakeName = (usedCarData[0]['make']) ? usedCarData[0]['make'] : '';
                carMakeId = (usedCarData[0]['make_id']) ? usedCarData[0]['make_id'] : '';
                carModel_name = (usedCarData[0]['model']) ? usedCarData[0]['model'] : '';
                carModelId = (usedCarData[0]['model_id']) ? usedCarData[0]['model_id'] : '';
                carVerName = (usedCarData[0]['db_version']) ? usedCarData[0]['db_version'] : '';
                carVerId = (usedCarData[0]['db_version_id']) ? usedCarData[0]['db_version_id'] : '';


                bodyStyle = ((usedCarData[0]['body_style'])) ? usedCarData[0]['body_style'] : '';
                carCity = ((usedCarData[0]['city'])) ? usedCarData[0]['city'] : '';
                $transmission = ((usedCarData[0]['transmission'])) ? usedCarData[0]['transmission'] : '';
                $color = ((usedCarData[0]['colour'])) ? usedCarData[0]['colour'] : '';
                $fuel = ((usedCarData[0]['fuel_type'])) ? usedCarData[0]['fuel_type'] : '';
                maxPrice = ((usedCarData[0]['pricefrom'])) ? usedCarData[0]['pricefrom'] : '';
                minPrice = ((usedCarData[0]['priceto'])) ? usedCarData[0]['priceto'] : '';
                maxYear = ((usedCarData[0]['year'])) ? usedCarData[0]['year'] : '';
                minYear = ((usedCarData[0]['year'])) ? usedCarData[0]['year'] : '';
                maxKm = ((usedCarData[0]['km'])) ? usedCarData[0]['km'] : '';
                minKm = ((usedCarData[0]['km'])) ? usedCarData[0]['km'] : '';
                listingType = ((usedCarData[0]['listingType'])) ? usedCarData[0]['listingType'] : '';
                certificationId = ((usedCarData[0]['certification_id'])) ? usedCarData[0]['certification_id'] : '';

                // Marking lead as a Premium
                premiumLead = '0';
                if ((maxPrice >= PREMIUM_LEAD_PRICE) || (minPrice >= PREMIUM_LEAD_PRICE) || (leadLogData['is_premium'] == 1)) {
                    premiumLead = '1';
                    // premiumLeadCondition = array('table' : UBLMS_LEADS,
                    //     'values' : array('is_premium' : '1'),
                    //     'condition' : array('lead_id' : leadId));
                    // $this->db->update(premiumLeadCondition);
                    // premiumLeadLogCondition = array('table' : UBLMS_LEADS_LOG,
                    //     'values' : array('is_premium' : '1'),
                    //     'condition' : array('lead_log_id' : $leads_log_id));
                    // $this->db->update(premiumLeadLogCondition);
                }

            }
        } else if (gaadiVersionId) {
            // $gaadiCarCondition = array('table' : GAADI_CAR_MODEL_VERSIONS,
            //     'condition' : array('db_version_id' : gaadiVersionId),
            //     'fields' : array('mk_id', 'model_id', 'db_version_id', 'db_version'),
            //     'join' : array(
            //         array('table' : GAADI_CAR_MAKE_MODELS, 'type' : 'inner', 'condition' : GAADI_CAR_MODEL_VERSIONS . '.model_id=' . GAADI_CAR_MAKE_MODELS . '.id', 'fields' : array('make', 'model'))
            // ));
            // gaadiResult = $this->db->select($gaadiCarCondition);
            if (gaadiResult) {
                carMakeName = gaadiResult['0']['make'];
                carMakeId = gaadiResult['0']['mk_id'];
                carModel_name = gaadiResult['0']['model'];
                carModelId = gaadiResult['0']['model_id'];
                carVerName = gaadiResult['0']['db_version_id'];
                carVerName = gaadiResult['0']['db_version'];
            }
        }
        // setting the customer's city when car-city & customer-city not set
        if (carCity == '' && customerCityId == '') {
            if (telecomLocation && telecomLocation && typeof telecomLocation == 'object') {
                if (telecomLocation['city_id'] != '0') {
                    customerCity = telecomLocation['city_name'];
                    customerCityId = telecomLocation['city_id'];
                    // $customerCityInsertCond = array('table' : UBLMS_CUSTOMER,
                    //     'values' : array('customer_city_name' : customerCity, 'customer_city_id' : customerCityId),
                    //     'condition' : array('customer_id' : customerId));
                    // $this->db->update($customerCityInsertCond);
                    dialerCity = customerCityId;
                }
            }
        }
        if (leadLogData['is_premium'] == 1) {
            premiumLead = 1;
            // premiumLeadCondition = array('table' : UBLMS_LEADS,
            //     'values' : array('is_premium' : '1'),
            //     'condition' : array('lead_id' : leadId));
            // $this->db->update(premiumLeadCondition);
        }

        //Dealer page leads
        leadDealerId = (leadLogData['dealer']['dealer_id']) ? trim(leadLogData['dealer']['dealer_id']) : 0;
        if (subSourceId == 1937 || subSourceId == 1938) {
            // $arrLeadDealerInfo = array('lead_id' : leadId,'dealer_id' : leadDealerId, 'added_date' : date('Y-m-d H:i:s'));
            // $this->db->insert(array('table' : UBLMS_LEADS_DEALER_MAPPING, 'values' : $arrLeadDealerInfo));
        }

        // Adding Dialer API for Hyderabad City
        if (sourceId == '4') {
            smsSource = 'iGAADI';
            smsSourceName = 'Gaadi.com';
        } else if (sourceId == '90') {
            smsSource = 'CARDEK';
            smsSourceName = 'CarDekho.com';
        } else if (sourceId == '368') {
            smsSource = 'ZGWILS';
            smsSourceName = 'Zigwheels.com';
        } else {
            smsSource = 'CARDEK';
            smsSourceName = 'CarDekho.com';
        }
        campaignName = '';
        clusterId = 0;

        if (dialerCity) {
            clusterData = getClusterByCityId(dialerCity);
            clusterId = clusterData[0]['cluster_id'];
        }
        if (premiumLead == '1' && clusterId != '5' && clusterId != '3') {
            campaignName = PREMIUM_L1_COMPAIGN;
        } else if (!empty(dialerCity)) {
            campaignName = getCompaignListIDByCityID(dialerCity);
        }
        checkleads = 0;
        if (trim(carId) != "" && typeof carId == 'number') {
            checkleads = getFordDealersDealerIds(carId);
        }
        isLeadDialer = false;
        //for L1
        isDialerEnabledforSubSource = isDialerEnabledForSubSource(subSourceId);
        if (insertLead && campaignName && (leadStatus == 'New' || leadStatus == 'Accepted') && (sourceId != 7) && isDialerEnabledforSubSource['L1_enabled'] == 'Y' && (![10, 11, 15].includes(clusterId))) {
            isLeadDialer = true;
            /*if (checkleads == 0) {
                $this->queeindailer(customerMobile, campaignName, date("Y-m-d"), customerName, leadId, premiumLead);
            }*/
        }

        // Getting CarMakeId using CarMakeName
        if (carMakeId == '' && carMake != '') {
            carMakeName = carMake;
            make = getCarMakeId(carMakeName);
            if (!empty(make)) carMakeId = make[0]['id'];

        }
        // Getting CarModelId using CarModelName
        if (carModelId == '' && carModel != '') {
            carModel_name = carModel;
            model = getCarModelId(carModelName);
            if (model) carModelId = model[0]['id'];
        }

        ucUserTypeData = getSellerTypeOfCar(carId);
        ucUserType = ucUserTypeData[0]['user_type'];

        prefResult = {}
        if (leadId > 0 && (carId > 0)) {
            if (carId != '') {
                // getCarPrefCondition = array('table' : UBLMS_LEADS_CARS,
                //     'condition' : array('lead_id' : leadId, 'car_id' : carId));
                // prefResult = $this->db->select(getCarPrefCondition);
            } else if (carMakeName != '' && carModel_name != '') {
                // getCarPrefCondition = array('table' : UBLMS_LEADS_CARS,
                //     'condition' : array('lead_id' : leadId, 'make_name' : carMakeName, 'model_name' : carModel_name));
                // prefResult = $this->db->select(getCarPrefCondition);
            }
            parentModelId = 0;
            parentModelName = '';
            if (carModelId) {
                parentModelArr = getParentModelId(carModelId);
                if (parentModelArr) {
                    parentModelId = parentModelArr['model_id'];
                    parentModelName = parentModelArr['model'];
                }
            }
            otpValidation = validateLeadsSendToDC(leadLogData['source']['platform'], otpVerified);
            distance = getDistance(carId, lat, lon);
            isDealerSent = '0';
            if (otpValidation != '1') {
                isDealerSent = '2';
            }
            carPreferenceData = {
                'lead_id': leadId,
                'car_id': carId,
                'make_id': carMakeId,
                'make_name': carMakeName,
                'model_id': parentModelId,
                'model_name': parentModelName,
                'variant_id': carVerId,
                'variant_name': carVerName,
                'distance': distance,
                'car_availability': '',
                'body_type': bodyStyle,
                'car_city': carCity,
                'transmission': $transmission,
                'color': $color,
                'fuel': $fuel,
                'price_from': maxPrice,
                'price_to': minPrice,
                'max_year': maxYear,
                'min_year': minYear,
                'max_km': maxKm,
                'min_km': minKm,
                'listing_type': listingType,
                'certification_id': certificationId,
                'status_id': '1',
                'sub_status_id': '1',
                'calling_status_id': '1',
                'source_id': sourceId,
                'sub_source_id': subSourceId,
                'cd_car_id': cdCarId,
                'otp_verified': otpVerified,
                'is_dealer_sent': isDealerSent,
                'is_email_sms': '1',
                'added_on': new Date().toISOString(),
                'lead_platform': leadLogData['source']['platform'],
                'added_by': '0', 'is_outstation': (distance >= CAR_DISTANCE_CUSTOMER) ? '1' : '0'
            }
            if (prefResult) {
                // $this->db->insert(array('table' : UBLMS_LEADS_CARS, 'values' : carPreferenceData));
                // $this->saveLeadFilterInfo($logid, leadId, customerId, carPreferenceData, 1);
            }
        } else {
            saveLeadFilterInfo(logid, leadId, customerId, leadLogData, 2);
        }
        if (leadId > 0) {
            if (carId > 0 || subSourceId == 1937 || subSourceId == 1938) {
                if (carId > 0) {
                    //if(distance <= CAR_DISTANCE_CUSTOMER) {
                    updateFTDofLeads(leadId, carId);
                    //}
                    if (ucUserType == 'D' && sourceId != 7 && otpVerified != '1' && dndCheck != '1') {
                        saveOtpVerifiedSms(customerName, customerMobile, leadId, carId, smsSource, smsSourceName);
                    }
                }
                if (((subSourceId == 1937 || subSourceId == 1938) || (carId > 0 && ucUserType == 'D' && sourceId != 7 && otpValidation == '1'))) {
                    saveCarsOnAsyncLeadAPI(leadId, customerName, customerEmail, customerMobile, carId, sourceId, subSourceId, leadDealerId, otpVerified, isWhatsapp, distance);
                }
            }
        }

        // If lead is Duplicate then insert it as REPEAT lead
        if (!insertLead && leadStatus == 'Duplicate') {
            if ((campaignName) && (sourceId != 7) &&
                (customersLeadData[0]['dialer_status']) && (customersLeadData[0]['dialer_status'] == '0' || customersLeadData[0]['dialer_status'] == '2') &&
                (customersLeadData[0]['l2_dialer_status']) && (customersLeadData[0]['l2_dialer_status'] == '0' || customersLeadData[0]['l2_dialer_status'] == '-1') && (isDialerEnabledforSubSource['L1_enabled'] == 'Y') && (![10, 11, 15].includes(clusterId))) {
                isLeadDialer = true;
            }
            comments = '';
            comments = getRepeatLeadsComments(sourceId, subSourceId, knowlarityDealerId);
            // $this->db->update(array('table' : UBLMS_CONVERSATION, 'condition' : array('lead_id' : leadId), 'values' : array('latest' : '0')));
            // $repeat_leads_data = array('lead_id' : leadId,
            //     'status_id' : '0',
            //     'sub_status_id' : '0',
            //     'calling_status_id' : '0',
            //     'comment' : 'Repeat Lead' . comments,
            //     'latest' : '1',
            //     'added_on' : dateFormat("yyyy-mm-dd HH:MM:ss"),
            //     'added_by' : '0');
            // $this->db->insert(array('table' : UBLMS_CONVERSATION, 'values' : $repeat_leads_data));

            //$updateduedateoflead = array('due_date' : date("Y-m-d 09:30:00", time() + 60 * 60 * 24));
            // $updateduedateoflead['due_date'] = $this->dueDateInLead(currentDatetime);
            // $this->db->update(array('table' : UBLMS_LEADS, 'values' : $updateduedateoflead, 'condition' : array('lead_id' : leadId)));

        }
        if (isLeadDialer || [10, 11, 15].includes(clusterId)) {
            if (checkleads == 0) {
                if (isValidIndividualLead(leadId)) {
                    if (isValidIndividualSms(leadId)) {
                        if (dndCheck != '1') {
                            smsMsgCustomerName = '';
                            if (!empty(customerName)) {
                                smsMsgCustomerName = customerName + ', ';
                            } else {
                                smsMsgCustomerName = 'Customer, ';
                            }
                            smsMsg = 'Dear ' + smsMsgCustomerName + 'interested in buying from a wide range of certified pre-owned cars? ';
                            // individualLeadData = array('lead_id' : leadId, 'mobile' : customerMobile,'sms_regards_text' : smsSourceName, 'message' : smsMsg, 'sender' : smsSource, 'l1_list_id' : campaignName, 'source_id':sourceId, 'created_date' : date('Y-m-d H:i:s'));
                            // $this->db->insert(array('table' : UBLMS_INDIVIDUAL_LEAD_SMS, 'values' : individualLeadData));
                        }
                        markLeadIndividualStatus(leadId, '0');
                    }
                } else {
                    markLeadIndividualStatus(leadId, '1');
                    if (!isBlockedCustomerBypass && isLeadDialer) {
                        queeindailer(customerMobile, campaignName, dateFormat("yyyy-mm-dd"), customerName, leadId, premiumLead, customerCity);
                    }
                }
            }
        }

        /* ==to push the unassigned cluster leads in IVR== */
        if (clusterId == 12 && sourceId == '90') {

            if (isValidIndividualLead(leadId)) {

                pushLeadsInIVR(leadId, customerId, customerCityId, clusterId);

            }
        }
        /* Unassigned Ends */

        leadscore = leadOnlineScore(leadId, customerMobile);
        // $updateonlinescore = array('lead_online_score' : leadscore);
        // $this->db->update(array('table' : UBLMS_LEADS, 'values' : $updateonlinescore, 'condition' : array('lead_id' : leadId)));

        response['response'] = JSON.stringify({ 'lead_status': leadStatus, 'reason': leadStatusReason, 'lead': insertLead });
        response['lead_id'] = leadId;
        response['blocking_type'] = blockingType;
    }

    if (subSourceId == '506') {
        /*$leadCurrentStatusdata = $this->getCurrentStatusofLead(leadId);
        if ($leadCurrentStatusdata['status_id'] <= 2) {
            $this->getCustomerRequirementDetails(leadId, customerId, leadLogData, subSourceId);
        }*/
    } else if (subSourceId == '1941') {
        /*carMakeId = (isset(leadLogData->filters->make_ids) && !empty(leadLogData->filters->make_ids)) ? leadLogData->filters->make_ids : '';
        parentModelId = (isset(leadLogData->filters->model_ids) && !empty(leadLogData->filters->model_ids)) ? leadLogData->filters->model_ids : '';
        $this->saveAuctionCustomerReq(carMakeId, parentModelId, leadId, customerId);*/
    }
    //saveCustomerReq
    return response;
    //exit();
}

const proccssleads = async (req, leadlogdata, sourceId, sub_source_id, logId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let blocking_type = 0;
            let isBlockedCustomerBypass = false;
            let otp_verified = 0;
            let customer_mobile = '';
            let logdata = [];
            let is_whatsapp = ((leadlogdata) && (leadlogdata.is_whatsapp == '1')) ? '1' : '0';
            let close_lead_condition = {};
            let smsSource = '';
            let smsSourceName = '';
            let customer_id = '';
            let current_datetime = new Date().toISOString();
            if (leadlogdata.customer.mobile) {
                customer_mobile = leadlogdata.customer.mobile;
            }
            let customers_lead_data = '';
            let response = [];

            // saving log data to mongo db
            logdata['mobile'] = customer_mobile;
            logdata['source'] = sourceId;
            logdata['source_id'] = sourceId;
            logdata['added'] = current_datetime;
            logdata['data'] = leadlogdata;
            let mongolog = await rawLeadModel.insertIntoUblmsLeadsLogMongo(logdata);
            let dnd_check = '0';
            let used_car_data = '';
            let is_car_exist = 0;
            let is_car_valid = 0;
            //echo $customer_mobile;
            /*
            let matches = customer_mobile.match(/^[1-9]\d{7,11}$/);
            if (matches && matches.length == 0) {
                let data_message = { status: false, message: 'Customer mobile is not valid', lead: false };
            }
            */
            // Verifying if lead is originating from Dealers mobile
            // $dealer_lead_status = $this->checkMobileAuthentication($customer_mobile, $leadlogdata);
            let car_id = '';
            if (leadlogdata.car_id && leadlogdata.car_id != '0') {
                is_car_exist = 1;
                car_id = leadlogdata.car_id;
                let carRes = await Q.allSettled([InventoryService.getStockList(req, { stock_used_car_id_arr: [car_id], call_server: 'ub', car_status: [1, 2] })]);
                let used_car_detail = carRes && _.get(carRes, "[0].value.data");
                is_car_valid = (used_car_detail[0])?1:0;
                used_car_data = used_car_detail[0];
            }
            //console.log('Used car data*******************', used_car_data);
            
            
            //console.log('--------899*******************************************************', leadlogdata)
            // if (sub_source_id == 487 || otp_verified != '1') {
            //     if (dnd_check == '1') {
            //         let arrDnc = { raw_lead_log_id: logid };
            //         await rawLeadModel.insertDncRawLeadMapping(arrDnc);
            //     }
            // }
            if((is_car_exist == 1 && is_car_valid == 1) || (is_car_exist == 0 || is_car_valid==0)){
                let customer_data = [];
                try {
                    customer_data = await rawLeadModel.getCustomerDetail(customer_mobile);
                } catch (error) {
                    // console.log('Error:---', error);
                }
                let customer_name = customer_email = customer_city = lead_platform = customer_location = '';
                var customer_city_id = customer_location_id = finance = purchase_time = '';
                let lead_date = current_datetime;
                checkCampaign = '';
                let source_lead_id = knowlarity_dealer_id = temp_contact_id = null;
                if ((leadlogdata.source_lead_id)) {
                    source_lead_id = leadlogdata.source_lead_id;
                }
                if (leadlogdata.customer.name) {
                    customer_name = leadlogdata.customer.name;
                }
                if (leadlogdata.customer.email) {
                    customer_email = leadlogdata.customer.email;
                }
                if (leadlogdata.customer.city)
                    customer_city = leadlogdata.customer.city;

                if (customer_city) {
                    try {
                        city_data = await rawLeadModel.getCityId(customer_city);
                        if (city_data && city_data.length > 0) {
                            customer_city_id = city_data['city_id'];
                        }
                    } catch (error) {
                        // console.log('Error in await rawLeadModel.getCityId: ', error);
                    }
                }
                /*
                    $checkCampaign = $this->getCompaignListIDByCityID($customer_city_id);
                    if ((customer_data[0]['customer_city_name'])) {
                        customer_city = customer_data[0]['customer_city_name'];
                        customer_city_id = customer_data[0]['customer_city_id'];
                    }
                */

                if (leadlogdata.platform) {
                    lead_platform = leadlogdata.platform;
                }
                if ((leadlogdata.location)) {
                    customer_location = leadlogdata.location;
                    location_data = await rawLeadModel.getLocationId(customer_location);
                    if ((location_data) && location_data.isArray) {
                        customer_location_id = location_data[0]['location_id'];
                    }
                }
                if (leadlogdata.finance) {
                    finance = leadlogdata.finance;
                }
                if (leadlogdata.purchase_time) {
                    purchase_time = leadlogdata.purchase_time;
                }
                if (leadlogdata.dealer_id) {
                    knowlarity_dealer_id = leadlogdata.dealer_id;
                }
                if (leadlogdata.temp_contact_id) {
                    temp_contact_id = leadlogdata.temp_contact_id;
                }
                if (leadlogdata.sub_source_dealer_id) {
                    knowlarity_dealer_id = leadlogdata.sub_source_dealer_id;
                }
                // Verifying if customer already exist or not
                let insert_lead = false;
                let lead_status = '';
                if (customer_data && customer_data.length == 0) {
                    
                    // Inserting the New Customer detail
                    try {
                        lead_status = 'New';
                        lead_status_reason = "lead_status customer lead";
                        insert_lead = true;
                        mobile_true_location = 'Code Not Exist';
                        // let telecomLocation = await rawLeadModel.getMobileLocation(customer_mobile);
                        // if (!empty($telecomLocation) && is_array($telecomLocation)) {
                        //     $mobile_true_location = $telecomLocation['mobile_telecom_circle'];
                        // }
                        //let customer_email_id = $this->isValidCustomerEmail($customer_email) ? $customer_email : '';
                        let customer_details = {
                            customer_name: customer_name,
                            customer_city_id: customer_city_id || 0,
                            customer_city_name: customer_city,
                            customer_email: customer_email,
                            customer_mobile: customer_mobile,
                            alternate_email: '',
                            alternate_mobile: '',
                            customer_location_id: customer_location_id || 0,
                            cutomer_location_name: customer_location,
                            customer_added: new Date().toISOString(),
                            active: '1',
                            ndnc: dnd_check
                        };

                        customer_id = await rawLeadModel.insertCustomer(customer_details);
                    } catch (error) {
                        // console.log('Error in await rawLeadModel.insertCustomer:----', error);
                    }
                } else {
                    customer_id = customer_data[0]['id'];
                    customers_lead_data = await rawLeadModel.getCustomersLeadDetail(customer_id);                       
                    let arrCustomerInfoUpdate = { ndnc: dnd_check, updated_on: new Date().toISOString() };
                    //console.log('arrCustomerInfoUpdate---------------------------', arrCustomerInfoUpdate);              
                    if (customer_data[0]['customer_city_id']) {
                        arrCustomerInfoUpdate['customer_city_id'] = customer_city_id;
                    } else if (customer_data[0]['customer_city_name']) {
                        let city = await getCityIdByName(req, customer_data[0]['customer_city_name']);
                        arrCustomerInfoUpdate['customer_city_id'] = (city.length) ? city[0].id : 0;
                    }
                    // if(customer_data[0]['customer_email']) {
                    //     $customer_email_id = $this->isValidCustomerEmail($customer_email) ? $customer_email : '';
                    //     if(!empty($customer_email_id)) {
                    //         $arrCustomerInfoUpdate['customer_email'] = $customer_email_id; 
                    //     }               
                    // }

                    let customer_condition = { custData: arrCustomerInfoUpdate, id: customer_id };
                    await rawLeadModel.updateCustomer(customer_condition);
                    if ((customer_data[0]['status']) != 1) {
                        let arrBlockingStatus = await rawLeadModel.getBlockingInfo(customer_id);
                        if (arrBlockingStatus && arrBlockingStatus['0']['blocked_reason']) {
                            if (arrBlockingStatus['0']['blocked_reason'] == '4') {
                                isBlockedCustomerBypass = true;
                            }
                        }
                    }

                    if (customer_data[0]['status'] || isBlockedCustomerBypass) {
                        if (customers_lead_data && customers_lead_data.length > 0) {
                            try {
                                lastLeadDate = customers_lead_data[0]['added_date'];
                                current_date = new Date().toISOString();
                                date_difference = dateDifference(lastLeadDate, current_date);
                                if ((customers_lead_data[0]['status_id'] == '7') || (customers_lead_data[0]['status_id'] == '6' && customers_lead_data[0]['sub_status_id'] == '12')) {
                                    lead_status = 'Accepted';
                                    lead_status_reason = "lead_status lead as entered new lead of existing customer as earlier lead was marked Closed/Purchased";
                                    insert_lead = true;
                                } else if (((customers_lead_data[0]['status_id'] == '3' && (customers_lead_data[0]['sub_status_id'] == '3' || customers_lead_data[0]['sub_status_id'] == '5')) || customers_lead_data[0]['status_id'] == '5' || customers_lead_data[0]['status_id'] == '6') && (date_difference <= L2_LEAD_EXPIRATION)) {
                                    lead_status = 'Duplicate';
                                    lead_status_reason = "L2 staged lead_status lead and it's life is date_difference days";
                                    insert_lead = false;
                                } else if (((customers_lead_data[0]['status_id'] == '3' && (customers_lead_data[0]['sub_status_id'] == '3' || customers_lead_data[0]['sub_status_id'] == '5')) || customers_lead_data[0]['status_id'] == '5' || customers_lead_data[0]['status_id'] == '6') && (date_difference >= L2_LEAD_EXPIRATION)) {
                                    lead_status = 'Accepted';
                                    close_lead_data = { status_id: 7, sub_status_id: 19, leads_status: 0 };
                                    let condition = { lead_id: customers_lead_data[0]['lead_id'] };

                                    await rawLeadModel.updateLeads(close_lead_data, condition);
                                    lead_status_reason = "Lead Closed at L2 staged and it's life is date_difference days";
                                    insert_lead = true;
                                } else if (date_difference <= LEAD_EXPIRATION && (customers_lead_data[0]['status_id'] != '7' && customers_lead_data[0]['sub_status_id'] != '12')) {
                                    lead_status = 'Duplicate';
                                    lead_status_reason = "lead_status lead as it's life is date_difference days only";
                                    insert_lead = false;
                                } else if (date_difference > LEAD_EXPIRATION) {
                                    close_lead_condition = { status_id: '7', sub_status_id: '19', leads_status: '0' };
                                    let condition = { lead_id: customers_lead_data[0]['lead_id'] };
                                    await rawLeadModel.updateLeads(close_lead_data, condition);
                                    lead_status = 'Accepted';
                                    lead_status_reason = "lead_status lead as last lead was AutoClosed due to life of date_difference days";
                                    insert_lead = true;
                                }

                            } catch (error) {
                                // console.log('Error*******************', error);
                            }
                        } else {
                            lead_status = 'Accepted';
                            lead_status_reason = "lead_status added successfully.";
                            insert_lead = true;
                        }
                    } else {
                        //arrBlockingStatus = this->getBlockingInfo(customer_id);
                        lead_status = 'Rejected';
                        lead_status_reason = "lead_status lead as customer is inactive";
                        if (arrBlockingStatus && arrBlockingStatus['0']['blocked_reason']) {
                            blocking_type = arrBlockingStatus['0']['blocked_reason'];
                            if (arrBlockingStatus['0']['blocked_reason'] == '3') {
                                blockingReason = 'Scraping agent';
                            }
                            //  else {
                            //     global blockedReasonArr;
                            //     blockingReason = blockedReasonArr[arrBlockingStatus['0']['blocked_reason']];
                            // }
                            lead_status_reason = "lead_status it was a ".blockingReason;
                        }
                        insert_lead = false;
                    }
                }
                // Evaluating the SubSource ID when lead source is available
                if (leadlogdata.lead_source) {
                    try {
                        let lead_source = leadlogdata.lead_source;
                        let getLeadSourceCondition = { source_id: sourceId, sub_source_name: lead_source, status: '1' };
                        let leadSourceResult = await rawLeadModel.getSubsourceBysourceId(getLeadSourceCondition);
                        if (leadSourceResult) {
                            sub_source_id = leadSourceResult[0]['sub_source_id'];
                        }
                    } catch (error) {
                        // console.log('Error--------1161------------', error);
                    }
                }

                // Inserting the Leads log data
                try {
                    let leads_log = {
                        source_lead_id: source_lead_id,
                        temp_contact_id: temp_contact_id,
                        customer_id: customer_id,
                        customer_mobile: customer_mobile,
                        customer_name: customer_name,
                        customer_email: customer_email,
                        customer_city: customer_city,
                        source_id: sourceId,
                        sub_source_id: sub_source_id,
                        dealer_id: knowlarity_dealer_id,
                        created_at: new Date().toISOString(),
                        lead_status: lead_status,
                        leads_status_reason: lead_status_reason,
                        lead_platform: lead_platform,
                        params_string: JSON.stringify(leadlogdata)
                    };
                    let leads_log_id = await rawLeadModel.insertRecord(leads_log, UBLMS_LEADS_LOG);
                } catch (error) {
                    // console.log('Error in await rawLeadModel.insertData----', error);
                }
                // Inserting the accepted leads data
                let todayDate = dateFormat('yyyy-mm-dd');
                let sixThirty = new Date(todayDate+" 18:30:00");
                let sixThirtyTS = sixThirty.getTime();
                let afterAddTwohrs = new Date().setHours(new Date().getHours() + 2);
                let followUpDtate =  new Date().toISOString();
                if(afterAddTwohrs > sixThirtyTS){
                   let tomorrowdate = await tomorrow(true);
                   followUpDtate = new Date(tomorrowdate).toISOString();
                   //console.log('tomorrowdate-------------------**************************************', followUpDtate);
                }else{
                    followUpDtate = new Date(afterAddTwohrs).toISOString();
                }
                
                let lead_id = '';
                //console.log('customers_lead_data-------------------------------', customers_lead_data);
                if (insert_lead && (lead_status == 'New' || lead_status == 'Accepted') && (typeof customers_lead_data === "undefined" || customers_lead_data == '')) {
                    //console.log('inserting leadssss*************************************');
                    try {
                        let calling_lead = {
                            source_lead_id: source_lead_id,
                            customer_id: customer_id,
                            source_id: sourceId,
                            sub_source_id: sub_source_id,
                            status_id: 1,
                            sub_status_id: 1,
                            calling_status_id: 1,
                            dealer_id: knowlarity_dealer_id,
                            ratings: '',
                            is_finance_req: finance,
                            purchase_time: purchase_time,
                            due_date: followUpDtate,
                            created_at: leadlogdata.created_date,
                            updated_by: '0',
                            platform: lead_platform,
                            connecto_id: leadlogdata.connecto_id,
                            leads_status: '1',
                            is_premium:leadlogdata.premium
                        };
                        // if (leadlogdata.log_date) {
                        //     calling_lead['created_at'] = leadlogdata.log_date;
                        // }
                        lead_id = await rawLeadModel.insertRecord(calling_lead, UBLMS_LEADS);
                        //console.log('leadsssssssssssssssssssssssssssssssss', lead_id);
                        response['lead_id'] = lead_id;
                    } catch (error) {
                        // console.log('Error in rawLeadModel.insertData------', error);
                    }

                    //lead_date = new Date().toISOString();
                    let new_leads_conv_data = {
                        lead_id: lead_id,
                        status_id: '1',
                        sub_status_id: '1',
                        calling_status_id: '1',
                        comment: lead_status + ' Lead',
                        latest: '1',
                        created_at: leadlogdata.created_date,
                        added_by: '0'
                    };
                    //$this->db->insert(array('table' => UBLMS_CONVERSATION, 'values' => $new_leads_conv_data));
                    await rawLeadModel.insertRecord(new_leads_conv_data, UBLMS_CONVERSATION);

                    let conversion_data = {lead_id: lead_id, status_id: '1', sub_status_id: '1', calling_status_id: '1', comment: lead_status + ' Lead', latest: '1', added_on: leadlogdata.created_date, added_by:0};
                    await rawLeadModel.insertRecord(conversion_data, UBLMS_CONVERSION);
                } else {
                    lead_id = customers_lead_data['id'];
                    
                    response['lead_id'] = lead_id;
                    let followupdate = {  due_date: followUpDtate};
                    let condition = { lead_id: lead_id}
                    await rawLeadModel.updateLeads(followupdate, condition);
                }
                // Inserting the customers car preference
                if (lead_id) {
                    let query_string = car_make = car_model = '';
                    let car_make_id = car_model_id = car_ver_id = dealer_id = gaadi_version_id = '';
                    let car_make_name = car_model_name = car_ver_name = '';
                    

                    // Setting customers car preference data
                    let dialerCity = customer_city_id;
                    let body_style = car_city = transmission = color = fuel = max_price = min_price = max_year = min_year = max_km = min_km = listing_type = '';
                    if (car_id) {


                        //  used_car_data =this->getUsedCarDetail($car_id);
                        //console.log('Car details&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&',used_car_data);
                        if (used_car_data) {
                            try {
                                car_make_name = (used_car_data['make']) ? used_car_data['make'] : '';
                                car_make_id = (used_car_data['make_id']) ? used_car_data['make_id'] : '';
                                car_model_name = (used_car_data['model']) ? used_car_data['model'] : '';
                                car_model_id = (used_car_data['model_id']) ? used_car_data['model_id'] : '';
                                car_ver_name = (used_car_data['carversion']) ? used_car_data['carversion'] : '';
                                car_ver_id = (used_car_data['db_version_id']) ? used_car_data['db_version_id'] : '';
                                body_style = (used_car_data['body_style']) ? used_car_data['body_style'] : '';
                                car_city = (used_car_data['city']) ? used_car_data['city'] : '';
                                transmission = (used_car_data['transmission']) ? used_car_data['transmission'] : '';
                                color = (used_car_data['colour']) ? used_car_data['colour'] : '';
                                fuel = (used_car_data['fuel_type']) ? used_car_data['fuel_type'] : '';
                                dealer_id = used_car_data['dealer_id'];
                                // Marking lead as a Premium
                                let premiumLead = '0';
                                if ((max_price >= PREMIUM_LEAD_PRICE) || (min_price >= PREMIUM_LEAD_PRICE) || (leadlogdata.is_premium == 1)) {
                                    let premiumLeadCondition = { is_premium: 1 };
                                    let cond = { lead_id: lead_id };
                                    await rawLeadModel.updateLeads(premiumLeadCondition, cond);
                                }

                            } catch (error) {
                                // console.log('Error---', error);
                            }
                        }

                    }
                    if (leadlogdata.is_premium == 1) {
                        let premiumLead = 1;
                        let premiumLeadCondition = { is_premium: '1' };
                        let condition = { lead_id: lead_id };
                        //$this->db->update($premiumLeadCondition);
                    }
                    let cd_car_id = 0;
                    if (sourceId == 90 && leadlogdata.source_car_id != '') {
                        let cd_car_id = leadlogdata.source_car_id;
                        //let car_id = $this->getCentralCarIDByCardekhoCarID($cd_car_id);
                    }
                    // Adding Dialer API for Hyderabad City
                    if (sourceId == '90') {
                        smsSource = 'OTO';
                        smsSourceName = 'ub.oto.com';
                    }
                    let campaign_name = '';
                    let cluster_id = 0;

                    let checkleads = 0;

                    let isLeadDialer = false;

                    let prefResult = [];
                    if ((lead_id > 0) && (used_car_data)) {
                        if (car_id != '') {
                            prefResult = await rawLeadModel.getLeadsCarByLeadId(lead_id, used_car_data.id);
                        }
                    }
                    let checkSubleadRes = await LeadModel.checkSubleadExistanceByCarIdAndMobile([car_id, customer_mobile]);
                    if ((used_car_data) && (checkSubleadRes.total == 0)) {
                        let is_sent_to_dealer = 0;
                        if (process.env.COUNTRY_CODE.toLowerCase() == 'ph') {
                            if (used_car_data.paid_score == 1) {
                                is_sent_to_dealer = 0;
                            } else if (used_car_data.paid_score == 2 || used_car_data.paid_score == 3) {
                                is_sent_to_dealer = 1;
                            }
                        }
                        if (process.env.COUNTRY_CODE.toLowerCase() == 'id') {
                            let sixMonthOldInventory = false;
                            sixMonthOldInventory = sixMonthsBefore(used_car_data.created_date);
                            if (sixMonthOldInventory) {
                                is_sent_to_dealer = 0;
                            } else {
                                is_sent_to_dealer = 1;
                            }
                        }
                        try {
                            let car_preferenceData = {
                                lead_id: lead_id,
                                car_id: used_car_data.id,
                                dealer_id: dealer_id,
                                make_id: car_make_id,
                                make_name: car_make_name,
                                model_id: car_model_id,
                                model_name: car_model_name,
                                variant_id: car_ver_id,
                                variant_name: car_ver_name,
                                car_availability: '',
                                body_type: body_style,
                                car_city: car_city,
                                transmission: transmission,
                                color: color,
                                fuel: fuel,
                                status_id: 1,
                                sub_status_id: 1,
                                calling_status_id: 1,
                                source_id: sourceId,
                                sub_source_id: sub_source_id,
                                cd_car_id: cd_car_id,
                                otp_verified: otp_verified,
                                is_dealer_sent: is_sent_to_dealer,
                                is_email_sms: 1,
                                created_at: leadlogdata.created_date,
                                lead_platform: leadlogdata.platform,
                                added_by: 0
                            };
                            if (prefResult == undefined) {
                                await rawLeadModel.insertRecord(car_preferenceData, UBLMS_LEADS_CARS);
                            }

                        } catch (error) {
                            // console.log('Error in Leads car----', error);
                        }
                    }
                } else {
                    response['error'] = "Lead id not genereated";
                }
            }
            resolve(response);
        } catch (error) {
            reject(error);  
        }
    });
}

const dateDifference = async (date1, date2) => {
    let start_ts = date1.getTime();
    let end_ts = date2.getTime();
    let diff = '';
    if (end_ts > start_ts) {
        diff = end_ts - start_ts;
    } else {
        diff = start_ts - end_ts;
    }
    return Math.round(diff / 86400);
}

const validateLeadsSendToDC = async (lead_platform, otp_verified) => {
    let flag = 0;
    if (otp_verified == 1) {
        flag = 1;
    } else if (otp_verified == 0 && validPlatform.includes(lead_platform)) {
        flag = 0;
    }
    return flag;
}
const saveCarsOnAsyncLeadAPI = async (lead_id, name, email, mobile, car_id, source_id, sub_source_id, leadDealerId, otp_verified, is_whatsapp = '0', distance = '0') => {
    let asyncLeadAPiId = 0
    let domain_id = '';
    let ayncLeadsData = {};
    let apitype = {};
    if (sub_source_id == 1937 || sub_source_id == 1938) {
        domain_id = '4';
    } else if (source_id == 4) {
        domain_id = '1';
    } else if (source_id == 368) {
        domain_id = '3';
    } else {
        domain_id = '2';
    }

    apitype['is_whatsapp'] = is_whatsapp;
    apitype['customer_name'] = name;
    apitype['customer_email'] = email;
    apitype['customer_mobile'] = mobile;
    apitype['car_id'] = car_id;
    apitype['otp_verified'] = (otp_verified == '1') ? '1' : '0';
    apitype['is_outstation'] = (distance >= CAR_DISTANCE_CUSTOMER) ? '1' : '0';

    ayncLeadsData['api_data'] = apitype;
    ayncLeadsData['domain_id'] = domain_id;
    ayncLeadsData['api_type'] = 'verify_buyer_lead';
    ayncLeadsData['mobile'] = mobile;
    ayncLeadsData['car_id'] = car_id;
    ayncLeadsData['lead_dealer_id'] = leadDealerId;
    ayncLeadsData['lead_id'] = lead_id;
    ayncLeadsData['added_date'] = dateFormat('yyyy-mm-dd HH:MM:ss');
    asyncLeadAPiId = await rawLeadModel.insertData(ayncLeadsData, UBLMS_ASYNC_LEADS_API);
    return asyncLeadAPiId;
}

const getCityIdByName = async(req, cityName)=>{
    let city = [];
    let resp = await InventoryService.getStateCityList(req);
    let result = (resp && resp.data) ? resp.data : [];
    let cityList = result.city;
    city = cityList.filter((c) =>c.name==cityName);
    return city;
}

const tomorrow = async(long = false) => {
    let t = new Date();
    t.setDate(t.getDate() + 1);
    const ret = `${t.getFullYear()}-${String(t.getMonth() + 1).padStart(2, '0')}-${String(t.getDate()).padStart(2, '0')}`;
    return !long ? ret : `${ret} 09:30:00`;
}

const sixMonthsBefore = (date) => {
    var flag = '', sixmonthBeforeDate = new Date(), carCreatedDate = new Date(date).getTime();
    sixmonthBeforeDate.setMonth(new Date().getMonth() - 6);// Six month
    sixmonthBeforeDate = sixmonthBeforeDate.getTime();
    if (sixmonthBeforeDate > carCreatedDate) {
        flag = true;
    } else {
        flag = false;
    }
    return flag;
}
