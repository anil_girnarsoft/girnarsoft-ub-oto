const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');

class DealerCarPriority extends Model{

}
DealerCarPriority.init({
    car_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    dealer_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
    },
    city_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    city: {
        type: dbConfig.Sequelize.STRING(200),
        allowNull: true,
    },
    priority: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue: '0'
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    type: {
        type: dbConfig.Sequelize.STRING(10),
        allowNull: true,
    },
    car_status: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    is_classified: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
    },
    full_sync_updated: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: true,
        default: '0'
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_DEALER_PRIORITY,
    freezeTableName: true
});

DealerCarPriority.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const car_data = await DealerCarPriority.create(data);
            resolve(car_data);
        } catch (error) {
            reject(error)
        }
    })
}

DealerCarPriority.updateOne = async (car_id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const car_data = await DealerCarPriority.update(data, { where: { car_id: car_id } });
            resolve(car_data);
        } catch (error) {
            reject(error)
        }
    })
}

DealerCarPriority.getMaxCarId = async ()=>{
    return new Promise((resolve, reject)=>{
        try {
            let sql = `SELECT MAX(car_id) as max_car_id FROM ${UBLMS_DEALER_PRIORITY}`;
            sequelize
            .query(sql, { raw: true, type: dbConfig.Sequelize.QueryTypes.SELECT,replacements:[] })
            .then(result => {
                resolve(result[0].max_car_id);
            }).catch((error)=> {
                reject(error);
            });
        } catch (error) {
            reject(error);
        }
    })
}

DealerCarPriority.getDealerPriority = async (params)=>{
    return new Promise((resolve, reject)=>{
        try {
            let sql = `SELECT priority  FROM ${UBLMS_DEALER_PRIORITY} where car_id=:carId and dealer_id=:dealerId`;
            sequelize
            .query(sql, { raw: true, type: dbConfig.Sequelize.QueryTypes.SELECT,replacements:{carId: params.carId, dealerId: params.dealerId} })
            .then(result => {
                resolve(result[0]);
            }).catch((error)=> {
                reject(error);
            });
        } catch (error) {
            reject(error);
        }
    })
}

DealerCarPriority.updateIsClassified = async ()=>{
    return new Promise((resolve, reject)=>{
        try {
            let sql = `UPDATE ${UBLMS_DEALER_PRIORITY} SET is_classified=0 where full_sync_updated='0'`;
            sequelize
            .query(sql, { raw: true, type: dbConfig.Sequelize.QueryTypes.UPDATE })
            .then(result => {
                resolve(result);
            }).catch((error)=> {
                reject(error);
            });
        } catch (error) {
            reject(error);
        }
    })
}

DealerCarPriority.resetFullSyncUpdated = async ()=>{
    return new Promise((resolve, reject)=>{
        try {
            let sql = `UPDATE ${UBLMS_DEALER_PRIORITY} SET full_sync_updated='0' WHERE full_sync_updated='1'`;
            sequelize
            .query(sql, { raw: true, type: dbConfig.Sequelize.QueryTypes.UPDATE })
            .then(result => {
                resolve(result);
            }).catch((error)=> {
                reject(error);
            });
        } catch (error) {
            reject(error);
        }
    })
}

module.exports = DealerCarPriority;