const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Schema = dbConfig.schema;

const buyerLeadDuplicates = new Schema ({
  date: { type: Date, required: false }, 
  source_id: { type: Number, required: false },
  source_lead_id: { type: String, required: false },
  response: { type: String, required: false },
  data: { type: String, required: false }
});
module.exports = dbConfig.mongoosedb.model('buyerLeadDuplicates', buyerLeadDuplicates)