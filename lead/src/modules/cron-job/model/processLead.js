const _ = require("lodash");
const Q = require("q");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const BuyerDuplicateModel = require("./buyerLeadsDuplicateSchema");

class ProcessLead extends Model {

}

ProcessLead.checkDuplicateData = async (pushedData, source_id) => {
    return new Promise((resolve, reject) => {
        var sql = "SELECT count(id) as total from ublms_raw_leads_log where source_key=? and source_id=?";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [pushedData, source_id] })
            .then(result => {
                resolve(result[0].total);
            }).catch((err) => {
                reject(err);
            })
    });
}

ProcessLead.insertDataIntoRawLeadLogs = async (data) => {
    try {
        //var returnObj = {};
        return new Promise((resolve, reject) => {
            let sql = "INSERT INTO " + UBLMS_RAW_LEADS_LOG + " (source_id, sub_source_id, mobile, leads_data, source_key) VALUES (?, ?, ?, ?,?)";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: [data.source_id, data.sub_source_id, data.mobile, JSON.stringify(data.leads_data), data.source_key] })
                .then((insertLead) => {
                    resolve(insertLead[0]);
                }).catch(error => {
                    reject(error)
                });
        });
    } catch (error) {
        // console.log('Error at insertDataIntoRawLeadLogs:--', error);
    }
}

ProcessLead.getSubsourceByUTM = async (getSubsourceMapped, sourceid) => {
    try {
        return new Promise((resolve, reject) => {
            let sql = "SELECT sub_source_id from " + UBLMS_SUB_SOURCE + " where sub_source_name=? AND source_id=?";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [getSubsourceMapped, sourceid] })
                .then((result) => {
                    let subSourceId = result[0].sub_source_id;
                    resolve(subSourceId);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {
        // console.log("Error at getSubsourceByUTM---", getSubsourceByUTM);
    }
}

ProcessLead.getSubsourceMapped = async (leadSource, sourceId) => {
    let returnObj = {};
    try {
        return new Promise((resolve, reject) => {
            let sql = "SELECT sm.sub_source_id,s.sub_source_name FROM " + UBLMS_SUB_SOURCE_MAPPING + " sm, ublms_sub_source s WHERE LOWER(sm.sub_source_slug)=? AND s.sub_source_id=sm.sub_source_id AND sm.source_id=?";
            AppModel.dbObj.sequelize
                .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [leadSource, sourceId] })
                .then((res) => {
                    returnObj.id = res[0].sub_source_id;
                    returnObj.title = res[0].sub_source_name;
                    resolve(returnObj);
                }).catch(err => {
                    reject(err);
                });
        });
    } catch (error) {

    }
}

ProcessLead.saveDataonRawleadsAdditionalParams = async (raw_id, source_id, sub_source_id, mobile, language, data) => {
    let platform = data.source.platform;
    let additionaldata = [];
    additionaldata['raw_lead_id'] = raw_id;
    additionaldata['customer_mobile'] = mobile;
    additionaldata['language'] = language;
    additionaldata['source_id'] = source_id;
    additionaldata['sub_source_id'] = sub_source_id;
    additionaldata['lead_source'] = (data['marketing'] != undefined) ? data['marketing']['utm_source'] : null;
    additionaldata['lead_platform'] = platform;
    additionaldata['lead_ux_source'] = (data['place'] != undefined) ? data['place']['lead_ux_source'] : null;
    additionaldata['lead_campaign'] = (data['marketing'] != undefined) ? data['marketing']['utm_campaign'] : null;
    additionaldata['lead_content'] = (data['marketing'] != undefined) ? data['marketing']['utm_content'] : null;
    additionaldata['lead_device'] = '';
    additionaldata['lead_creative'] = '';
    additionaldata['lead_keyword'] = '';
    additionaldata['lead_matchtype'] = '';
    additionaldata['lead_network'] = '';
    additionaldata['lead_placement'] = '';
    additionaldata['lead_adposition'] = '';
    additionaldata['lead_term'] = (data['marketing'] != undefined) ? data['marketing']['utm_term'] : null;
    additionaldata['ad_group_id'] = ((data['marketing'] != undefined)) ? data['marketing']['ad_group_id'] : null;
    additionaldata['location'] = (data['customer']['location_id'] != undefined) ? data['customer']['location_id'] : null;
    additionaldata['source_lead_id'] = (data['source']['source_lead_id']) ? data['source']['source_lead_id'] : null;
    additionaldata['lead_medium'] = (data['marketing'] != undefined) ? data['marketing']['utm_medium'] : null;
    additionaldata['app_id'] = (data['customer']['app_id'] != undefined) ? data['customer']['app_id'] : null;
    additionaldata['featured'] = (data['place'] != undefined) ? data['place']['featured'] : null;
    additionaldata['slot'] = (data['place'] != undefined) ? data['place']['slot'] : null;
    additionaldata['car_id'] = (data['cars']['car_id']) ? data['cars']['car_id'] : null;
    additionaldata['lead_url'] = (data['url'] != undefined) ? data['url']['lead_url'] : null;
    additionaldata['landing_url'] = (data['url'] != undefined) ? data['url']['landing_url'] : null;
    additionaldata['referring_url'] = (data['url'] != undefined) ? data['url']['referring_url'] : null;
    additionaldata['connecto_id'] = (data['customer']['connecto_id'] != undefined) ? data['customer']['connecto_id'] : null;
    additionaldata['latitude'] = null;
    additionaldata['longitude'] = null;
    if ((data['customer']['lat']) && (data['customer']['lon']) &&
        data['customer']['lat'] >= '5' && data['customer']['lat'] <= '40' &&
        data['customer']['lon'] >= '65' && data['customer']['lon'] <= '100') {
        additionaldata['latitude'] = data['customer']['lat'];
        additionaldata['longitude'] = data['customer']['lon'];
    }
    additionaldata['is_whatsapp'] = ((data['marketing'] != undefined) && data['marketing']['is_whatsapp'] == '1') ? '1' : '0';
    if ((data['customer']['otp_verified']) && (data['customer']['otp_verified'] == '1' || data['customer']['otp_verified'] == '0')) {
        additionaldata['otp_verified'] = data['customer']['otp_verified'];
    }
    additionaldata['ip_address'] = null;
    if ((data['customer']['ip_address']) && !empty(data['customer']['ip_address'])) {
        additionaldata['ip_address'] = data['customer']['ip_address'];
    }
    additionaldata['added_date'] = data.added_date;
    return new Promise((resolve, reject) => {
        let sql = "INSERT INTO " + UBLMS_RAW_LEADS_ADDITIONAL_PARAMS + " (raw_lead_id, customer_mobile, source_id, sub_source_id, language, lead_source, lead_platform, lead_ux_source, lead_campaign, ad_group_id, lead_content, lead_device, lead_creative, lead_keyword, lead_matchtype, lead_network, lead_placement, lead_adposition, lead_term, location, source_lead_id, lead_medium, app_id, is_whatsapp, otp_verified, ip_address, featured, slot, car_id, lead_url, landing_url, referring_url, latitude, longitude, added_date, connecto_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        AppModel.dbObj.sequelize
            .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: [additionaldata.raw_lead_id, additionaldata.customer_mobile, additionaldata.source_id, additionaldata.sub_source_id, additionaldata.language, additionaldata.lead_source, additionaldata.lead_platform, additionaldata.lead_ux_source, additionaldata.lead_campaign, additionaldata.ad_group_id, additionaldata.lead_content, additionaldata.lead_device, additionaldata.lead_creative, additionaldata.lead_keyword, additionaldata.lead_matchtype, additionaldata.lead_network, additionaldata.lead_placement, additionaldata.lead_adposition, additionaldata.lead_term, additionaldata.location, additionaldata.source_lead_id, additionaldata.lead_medium, additionaldata.app_id, additionaldata.is_whatsapp, additionaldata.otp_verified, additionaldata.ip_address, additionaldata.featured, additionaldata.slot, additionaldata.car_id, additionaldata.lead_url, additionaldata.landing_url, additionaldata.referring_url, additionaldata.latitude, additionaldata.longitude, additionaldata.added_date, additionaldata.connecto_id] })
            .then((insertLead) => {
                resolve(insertLead[0]);
            }).catch(error => {
                reject(error)
            });
    });
}


ProcessLead.saveDataonMarketingParams = async (raw_lead_id, data) => {
    let additionaldata = {};
    additionaldata['raw_lead_id'] = raw_lead_id;
    //additionaldata['lead_term'] = data['marketing']['utm_term'];
    if (data['marketing'] !== undefined) {
        additionaldata['utm_campaign'] = (typeof data['marketing']['utm_campaign'] !== 'undefined') ? data['marketing']['utm_campaign'] : '';
        additionaldata['utm_term'] = (typeof (data['marketing']['utm_term']) !== 'undefined') ? data['marketing']['utm_term'] : '';
        additionaldata['utm_medium'] = (typeof (data['marketing']['utm_medium']) !== 'undefined') ? data['marketing']['utm_medium'] : '';
        additionaldata['utm_source'] = (typeof (data['marketing']['utm_source']) !== 'undefined') ? data['marketing']['utm_source'] : '';
        additionaldata['utm_device'] = (typeof (data['marketing']['utm_device']) !== 'undefined') ? data['marketing']['utm_device'] : '';
        additionaldata['network'] = (typeof (data['marketing']['network']) !== 'undefined') ? data['marketing']['network'] : '';
        additionaldata['ad_group_id'] = (typeof (data['marketing']['ad_group_id']) !== 'undefined') ? data['marketing']['ad_group_id'] : '';
        additionaldata['ad_position'] = (typeof (data['marketing']['ad_position']) !== 'undefined') ? data['marketing']['ad_position'] : '';
        additionaldata['loc_interest_ms'] = (typeof (data['marketing']['loc_interest_ms']) !== 'undefined') ? data['marketing']['loc_interest_ms'] : '';
        additionaldata['campaign_id'] = (typeof (data['marketing']['campaign_id']) !== 'undefined') ? data['marketing']['campaign_id'] : '';
        additionaldata['creative'] = (typeof (data['marketing']['creative']) !== 'undefined') ? data['marketing']['creative'] : '';
        additionaldata['feed_item_id'] = (typeof (data['marketing']['feed_item_id']) !== 'undefined') ? data['marketing']['feed_item_id'] : '';
        additionaldata['loc_physical_ms'] = (typeof (data['marketing']['loc_physical_ms']) !== 'undefined') ? data['marketing']['loc_physical_ms'] : '';
        additionaldata['match_type'] = (typeof (data['marketing']['match_type']) !== 'undefined') ? data['marketing']['match_type'] : '';
        additionaldata['device_model'] = (typeof (data['marketing']['device_model']) !== 'undefined') ? data['marketing']['device_model'] : '';
        additionaldata['placement'] = (typeof (data['marketing']['placement']) !== 'undefined') ? data['marketing']['placement'] : '';
        additionaldata['target_id'] = (typeof (data['marketing']['target_id']) !== 'undefined') ? data['marketing']['target_id'] : '';
    }
    additionaldata['created_date'] = data.added_date;
    let allKeys = Object.keys(additionaldata);
    let valuesArr = [];
    let replacementVal = [];
    for (let i = 0; i < allKeys.length; i++) {
        valuesArr.push('?');
        replacementVal.push(additionaldata[allKeys[i]]);
    }
    return new Promise((resolve, reject) => {
        let sql = "INSERT INTO " + UBLMS_MARKETING_PARAMS + " (" + allKeys.join() + ") VALUES (" + valuesArr.join() + ")";
        AppModel.dbObj.sequelize
            .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.INSERT, replacements: replacementVal })
            .then((insertLead) => {
                resolve(insertLead[0]);
            }).catch(error => {
                reject(error)
            });
    });

}

ProcessLead.filterPlatForm = async () => {

}
ProcessLead.insertOneMongo = async (data, collection_name) => {
    //mongodb.in
    BuyerDuplicateModel.create(data, function (err, res) {
        if (err) {
            //   throw err
        }

    });
}
ProcessLead.getCityIDbyCentralID = async (central_city_id) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT city_id from ublms_city where central_city_id=?";
        AppModel.dbObj.sequelize
            .query(sql, { type: AppModel.dbObj.Sequelize.QueryTypes.SELECT, replacements: [central_city_id] })
            .then((res) => {
                resolve(res[0].city_id);
            }).catch(err => {
                reject(err);
            });
    });
}

ProcessLead.getRawData = async () => {

}

module.exports = ProcessLead;