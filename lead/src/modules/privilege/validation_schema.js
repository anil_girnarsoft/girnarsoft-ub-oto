const Joi = require('@hapi/joi')

module.exports = {
    createPrivilege: Joi.object().keys({
        name: Joi.string().required(),
        description: Joi.string().optional(),
        status: Joi.string().valid('0', '1').optional(),
        category_id: Joi.number().optional(),
        module: Joi.string().optional(),
        controller: Joi.string().optional(),
        action: Joi.string().optional()
    }),
    updatePrivilege: Joi.object().keys({
        id: Joi.number().required(),
        name: Joi.string().optional(),
        description: Joi.string().optional(),
        status: Joi.string().valid('0', '1').optional(),
        category_id: Joi.number().optional(),
        module: Joi.string().optional(),
        controller: Joi.string().optional(),
        action: Joi.string().optional()
    }),
    updateStatus: Joi.object().keys({
        id: Joi.number().required(),
        status: Joi.string().valid('0', '1').required()
    }),
    getList: Joi.object().keys({
        id: Joi.number().required(),
        _with: Joi.array().items(Joi.string().required()).optional()
    }).unknown(true)
}