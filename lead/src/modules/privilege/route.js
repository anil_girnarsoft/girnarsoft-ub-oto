const express = require('express');
const router = express.Router();
const schemas = require('./validation_schema');
const validation = require(MIDDLEWARE_PATH + 'validation');
const PrivilegeController = require('./controller/privilegeController');

router.post('/listPrivilege', PrivilegeController.getPrivilegeList);
router.post('/savePrivilege',validation(schemas.createPrivilege, 'body'), PrivilegeController.savePrivilege);
router.post('/editPrivilege',validation(schemas.updatePrivilege, 'body'),PrivilegeController.savePrivilege);
router.post('/updateStatus',validation(schemas.updateStatus, 'body'),PrivilegeController.updateStatus);
router.post('/getRolePrivilege',PrivilegeController.getRolePrivilegeList);

module.exports = router;