const Q = require("q");
const _ = require("lodash");
const privilegeModel = require('./../model/privilegeModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../validation_schema");


exports.savePrivilege = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    const userId = _.get(req.headers, 'userid');
    let id = params.id;
    let isSameNameExist = await privilegeModel.findOne({where:{'name':params.name}})
    if(isSameNameExist){
      if(!id || id != isSameNameExist.dataValues.id) throw new Error('NAME_SHOULD_BE_UNIQUE')
    }
    if(!id){
      params.added_by = userId;
      let roleObj = privilegeModel.getModel(params)
      privilegeModel.createOne(roleObj)
      .then(response =>{
        const role_data = response.dataValues;
        if(role_data.id > 0){
          ApiController.sendSuccessResponse(req, res, role_data,'privilege_added_successfully');
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .catch(err => {
        ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
      })
      
    }else{
      params.updated_by = userId;
      let roleObjToUpdate = privilegeModel.getModel(params);
      privilegeModel.updateOne(id,roleObjToUpdate)
      .then(response => {
        if(response && response.length){
          ApiController.sendResponse(req, res,200,'privilege_updated_successfully');
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        next(error);
      })
      
      
    }
  } catch (error) {
      next(error);
  }
  
}

exports.getPrivilegeList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await privilegeModel.get(value,page_number);
    let roleList = privilegeModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, roleList,pagination);
  }catch (error) {
    next(error);
  }
}

exports.updateStatus = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    let id = req.body.id;
    const userId = _.get(req.headers, 'userid');
    params.updated_by = userId;
    let statusObj = privilegeModel.getModel(params)
    privilegeModel.updateOne(id,statusObj)
    .then(response => {
      if(response && response.length){
        ApiController.sendResponse(req, res,200,'status_updated_successfully');
      }else{
        ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
      }
    })
    .catch(error => {
      next(error);
    })
  } catch (error) {
      next(error);
  }
  
}

exports.getRolePrivilegeList = async (req, res, next)=>{
  try{
    let params = req.body;
    //let moduleArr=[],controllerArr=[],actionArr=[],userCategoryArr=[];
    let responseData = {
      'moduleArr':[],
      'controllerArr':[],
      'actionArr' : [],
      'userCategoryArr' : []
    }
    let privilegeData = await privilegeModel.getUserprivileges(params.role_id);
    if (privilegeData && privilegeData.length) {
      for(let val of privilegeData) {
          if (val['privilege_id']) {
              let subPrivilegesData = await privilegeModel.getSubPrivleges(val['privilege_id']);
              if ((subPrivilegesData && subPrivilegesData.length)) {
                  for(let subval of subPrivilegesData) {
                      if ((subval['module'])) {
                        if(responseData.moduleArr.indexOf(subval['module'].toLowerCase()) == -1)
                          responseData.moduleArr.push(subval['module'].toLowerCase());
                      }
                      if (subval['controller']) {
                        if(responseData.controllerArr.indexOf(subval['controller'].toLowerCase()) == -1)
                          responseData.controllerArr.push(subval['controller'].toLowerCase());
                      }
                      if (subval['action']) {
                        if(responseData.actionArr.indexOf(subval['action'].toLowerCase()) == -1)
                          responseData.actionArr.push(subval['action'].toLowerCase());
                      }
                  }
              }
          }
          if (val['module']) {
            if(responseData.moduleArr.indexOf(val['module'].toLowerCase()) == -1)
              responseData.moduleArr.push(val['module'].toLowerCase());
          }
          if (val['controller']) {
            if(responseData.controllerArr.indexOf(val['controller'].toLowerCase()) == -1)
              responseData.controllerArr.push(val['controller'].toLowerCase());
          }
          if (val['action']) {
            if(responseData.actionArr.indexOf(val['action'].toLowerCase()) == -1)
              responseData.actionArr.push(val['action'].toLowerCase());
          }
          if (val['category_id']) {
            if(responseData.userCategoryArr.indexOf(val['category_id']) == -1)
              responseData.userCategoryArr.push(val['category_id']);
          }
      }
  }
    ApiController.sendPaginationResponse(req, res, responseData,{});
  }catch (error) {
    next(error);
  }
}



