const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class PrivilegeModel extends Model{
}

PrivilegeModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    status: {
        type:dbConfig.Sequelize.ENUM('0', '1'),
        allowNull: false,
        defaultValue: '1'
        
    },
    category_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    module: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    controller: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    action: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_PRIVILEGES,
    freezeTableName: true
});


PrivilegeModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await PrivilegeModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

PrivilegeModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await PrivilegeModel.update(data, { where: { id: id } });
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

PrivilegeModel.get = async (_where,pagination) => {

    let whereCond = await PrivilegeModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS id,category_id,name,module,controller,action,description,created_at,updated_at,added_by,updated_by,status FROM "+UBLMS_PRIVILEGES+"\n\
        "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await PrivilegeModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

PrivilegeModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if(params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : 1;
    if(params.name)
        dataModel.name = params.name;
    if(params.description)
        dataModel.description = params.description;
    if(params.category_id)
        dataModel.category_id = params.category_id;
    if(params.module)
        dataModel.module = params.module;
    if(params.controller)
        dataModel.controller = params.controller;
    if(params.action)
        dataModel.action = params.action;
    if(params.added_by)
        dataModel.added_by = params.added_by;
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
    return dataModel;
}

PrivilegeModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond =  await PrivilegeModel.bindPagination(params,pagination,isPaginationApply);
    if(params && params.name)
        condition = "where name like '%" + params.name + "%'";
    
    if(params && params.hasOwnProperty('status')){
        let status = "status='"+params.status+"'";
        condition = (condition == '')? condition + "where "+status : condition+" and "+status 
    }
    if(params && params.id){
        let id = "id='"+ PrivilegeModel.decrypt(params.id)+"'";
        condition = (condition == '')? condition + "where "+id : condition+" and "+id 
    }
    if(params && params.category_id){
        let category_id = "category_id='"+params.category_id+"'";
        condition = (condition == '')? condition + "where "+category_id : condition+" and "+category_id 
    }
    if(params && params.created_at){
        let inputDate = dateFormat(params.created_at, "yyyy-mm-dd");
        let datetCond = "date_format(created_at,'%Y-%m-%d') <= '" + inputDate+ "'"
        condition = (condition == '')? condition + "where "+datetCond : condition+" and "+datetCond 
    }
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }else{
        sortCond = 'ORDER BY updated_at desc';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

PrivilegeModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

PrivilegeModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

PrivilegeModel.encrypt = (data) => {
    if(Array.isArray(data)) data = data.map(v => {
        if(v && v.id != null) v.privilege_id_hash = crypto.encode(v.id);
        return v;
    })
    else if (data && data.id) data.privilege_id_hash = crypto.encode(data.id);
    return data;
}

PrivilegeModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

PrivilegeModel.getUserprivileges = async (roleId)=>{
    return new Promise((resolve, reject)=>{
        let sql = "select  up.id, up.category_id, up.module, up.controller, up.action  from \n"+
        UBLMS_ROLES_PRIVILEGE_MAP +" as upm  INNER JOIN " + UBLMS_PRIVILEGES + " as up on upm.privilege_id = up.id \n"+
        "LEFT JOIN " + UBLMS_SUB_PRIVILEGES + " as sp ON up.id = sp.privileges_id  where upm.role_id = '" +roleId + "' and upm.status = 1";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}

PrivilegeModel.getSubPrivleges = async (rolprivilegesIdeId)=>{
    return new Promise((resolve, reject)=>{
        let sql = "select module, controller, action  from " +UBLMS_SUB_PRIVILEGES+ " where status = '1' and privileges_id = '" +privilegesId+"";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}


module.exports = PrivilegeModel;