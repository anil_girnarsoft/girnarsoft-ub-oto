const Joi = require('@hapi/joi')

module.exports = {
    createCategory: Joi.object().keys({
        name: Joi.string().required(),
        description: Joi.string().optional(),
        lang_id : Joi.number().required(),
        status: Joi.string().valid('0', '1').optional()
    }),
    updateCategory: Joi.object().keys({
        id: Joi.number().required(),
        name: Joi.string().required(),
        description: Joi.string().optional(),
        status: Joi.string().valid('0', '1').optional(),
        lang_id: Joi.number().optional()

    }),
    updateStatus: Joi.object().keys({
        id: Joi.number().required(),
        status: Joi.string().valid('0', '1','2').required()
    }),
    getList: Joi.object().keys({
        id: Joi.number().required(),
        _with: Joi.array().items(Joi.string().required()).optional()
    }).unknown(true)
}