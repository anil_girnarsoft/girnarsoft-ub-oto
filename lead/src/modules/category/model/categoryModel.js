const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");

class CategoryModel extends Model{
}

CategoryModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    status: {
        type:dbConfig.Sequelize.ENUM('0', '1'),
        allowNull: false,
        defaultValue: '1'
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CATEGORY,
    freezeTableName: true,
    underscored: true
});


CategoryModel.createOne = async (data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const category_data = await CategoryModel.create(data,{ transaction: t });
            resolve(category_data);
        } catch (error) {
            reject(error)
        }
    })
}

CategoryModel.updateOne = async (id, data,t) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const category_data = await CategoryModel.update(data, { where: { id: id } },{ transaction: t });
            resolve(category_data);
        } catch (error) {
            reject(error)
        }
    })
}

CategoryModel.get = async (_where,pagination) => {

    let whereCond = await CategoryModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS cat.id,catLan.name,catLan.description,cat.created_at,cat.updated_at,catLan.lang_id,cat.status,cat.added_by,cat.updated_by FROM "+UBLMS_CATEGORY+" AS cat \n\
        INNER JOIN "+UBLMS_CATEGORY_LANG+" AS catLan ON cat.id=catLan.category_id \n\
        "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await CategoryModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

CategoryModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    if(params.id && (params.status))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = '1';
    if(params.added_by)
        dataModel.added_by = params.added_by;
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
    return dataModel;
}

CategoryModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond =  await CategoryModel.bindPagination(params,pagination,isPaginationApply);
    if(params && params.name)
        condition = "where catLan.name like '%" + params.name + "%'";
    if(params && params.id){
        let id = "cat.id='"+ CategoryModel.decrypt(params.id)+"'";
        condition = (condition == '') ? condition + "where "+id : condition+" and "+id 
    }
    if(params && params.hasOwnProperty('status')){
        let status = "cat.status='"+params.status+"'";
        condition = (condition == '')? condition + "where "+status : condition+" and "+status 
    }
    if(params && params.created_at){
        let inputDate = dateFormat(params.created_at, "yyyy-mm-dd");
        let datetCond = "date_format(cat.created_at,'%Y-%m-%d') <= '" + inputDate+ "'"
        condition = (condition == '')? condition + "where "+datetCond : condition+" and "+datetCond 
    }
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            if(srtBy && srtBy == 'id')
                srtBy = "cat."+srtBy
            if(srtBy && srtBy == 'name')
                srtBy = "catLan."+srtBy
            if(srtBy && srtBy == 'created_at')
                srtBy = "cat."+srtBy
            if(srtBy && srtBy == 'status')
                srtBy = "cat."+srtBy
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }else{
        sortCond = 'ORDER BY updated_at desc';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

CategoryModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

CategoryModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

CategoryModel.encrypt = (data) => {
    if(Array.isArray(data)) data = data.map(v => {
        if(v && v.id != null) v.category_id_hash = crypto.encode(v.id);
        return v;
    })
    else if (data && data.id) data.category_id_hash = crypto.encode(data.id);
    return data;
}

CategoryModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

module.exports = CategoryModel;