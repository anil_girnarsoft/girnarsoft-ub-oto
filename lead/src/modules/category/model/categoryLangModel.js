const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;

class CategoryLangModel extends Model{
}

CategoryLangModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    lang_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    category_id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_CATEGORY_LANG,
    freezeTableName: true,
    underscored: true
});


CategoryLangModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const category_lang_data = await CategoryLangModel.create(data);
            resolve(category_lang_data);
        } catch (error) {
            reject(error)
        }
    })
}

CategoryLangModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const category_data = await CategoryLangModel.update(data, { where: { category_id: id } });
            resolve(category_data);
        } catch (error) {
            reject(error)
        }
    })
}

CategoryLangModel.getModel = (params) => {
    let dataModel = {};
    dataModel.category_id = (params.id) ? params.id : params.category_id;
    if(!params.id)
        dataModel.created_at = new Date().toISOString();
    else
        dataModel.updated_at = new Date().toISOString();
    dataModel.lang_id = (params.lang_id) ? params.lang_id : 1;
    dataModel.name = params.name;
    if(params.description)
    dataModel.description = params.description
    return dataModel;
}

module.exports = CategoryLangModel;