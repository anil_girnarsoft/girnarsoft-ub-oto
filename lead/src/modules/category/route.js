const express = require('express');
const router = express.Router();
const schemas = require('./validation_schema');
const validation = require(MIDDLEWARE_PATH + 'validation');
const CategoryController = require('./controller/categoryController');

router.post('/listCategory', CategoryController.getCategoryList);
router.post('/saveCategory',validation(schemas.createCategory, 'body'), CategoryController.saveCategory);
router.post('/editCategory',validation(schemas.updateCategory, 'body'),CategoryController.saveCategory);
router.post('/updateStatus',validation(schemas.updateStatus, 'body'),CategoryController.updateStatus);

module.exports = router;