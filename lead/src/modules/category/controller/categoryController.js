const Q = require("q");
const _ = require("lodash");
const categoryModel = require('./../model/categoryModel');
const categoryLangModel = require('./../model/categoryLangModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../validation_schema");
const moment = require("moment");
const dbConfig = require(CONFIG_PATH + 'db.config');
const sequelize = dbConfig.sequelize;

exports.getCategoryList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body;
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    let data = await categoryModel.get(value,page_number);
    let categoryList = categoryModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, categoryList,pagination);
  }catch(error){
    
  }
}

exports.saveCategory = async (req, res, next)=>{
  try {
    const t = await sequelize.transaction();
    let params = _.cloneDeep(req.body);
    const userId = _.get(req.headers, 'userid');
    //let lang_id = _.get(req.headers, 'accept-language-id');
    let id = params.id;
    let isSameNameExist = await categoryLangModel.findOne({where:{'lang_id':params.lang_id,'name':params.name}})
    if(isSameNameExist){
      if(!id || id != isSameNameExist.dataValues.category_id) throw new Error('NAME_SHOULD_BE_UNIQUE')
    }
    if(!id){
      params.added_by = userId;
      let categoryObj = categoryModel.getModel(params,t)
      categoryModel.createOne(categoryObj)
      .then(response =>{
        const category_data = response.dataValues;
        if(category_data.id > 0){
          params.category_id = category_data.id;
          let cat_lang_data = categoryLangModel.getModel(params)
          return categoryLangModel.createOne(cat_lang_data,t)
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .then(response =>{
        const category_lang_data = response.dataValues;
        if(category_lang_data.id > 0){
          t.commit();
          ApiController.sendSuccessResponse(req, res, category_lang_data,'category_created_successfully');
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .catch(err => {
        t.rollback();
        ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
      })
      
    }else{
      params.updated_by = userId;
      let categoryObjToUpdate = categoryModel.getModel(params);
      categoryModel.updateOne(id,categoryObjToUpdate,t)
      .then(response => {
        if(response && response.length){
          let categoryLanObjToUpdate = categoryLangModel.getModel(params)
          return categoryLangModel.updateOne(id,categoryLanObjToUpdate,t)
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .then(response => {
        if(response && response.length){
          t.commit();
          ApiController.sendResponse(req, res,200,'category_updated_successfully');
        }else{
          t.rollback();
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        next(error);
      })
      
      
    }
  } catch (error) {
      next(error);
  }
  
}

exports.updateStatus = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    let id = req.body.id;
    const userId = _.get(req.headers, 'userid');
    params.updated_by = userId;
    let statusObj = categoryModel.getModel(params);
    categoryModel.updateOne(id,statusObj)
    .then(response => {
      if(response && response.length){
        ApiController.sendResponse(req, res,200,'status_updated_successfully');
      }else{
        ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
      }
    })
    .catch(error => {
      next(error);
    })
  } catch (error) {
      next(error);
  }
  
}


