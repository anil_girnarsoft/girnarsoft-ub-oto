const Q = require("q");
const _ = require("lodash");
const roleModel = require('./../model/roleModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../validation_schema");


exports.saveRole = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    const userId = _.get(req.headers, 'userid');
    let id = params.id;
    let isSameNameExist = await roleModel.findOne({where:{'name':params.name}})
    if(isSameNameExist){
      if(!id || id != isSameNameExist.dataValues.id) throw new Error('NAME_SHOULD_BE_UNIQUE')
    }
    if(!id){
      params.added_by = userId;
      let roleObj = roleModel.getModel(params)
      roleModel.createOne(roleObj)
      .then(response =>{
        const role_data = response.dataValues;
        if(role_data.id > 0){
          ApiController.sendSuccessResponse(req, res, role_data,'role_created_successfully');
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .catch(err => {
        ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
      })
      
    }else{
      params.updated_by = userId;
      let roleObjToUpdate = roleModel.getModel(params);
      roleModel.updateOne(id,roleObjToUpdate)
      .then(response => {
        if(response && response.length){
          ApiController.sendResponse(req, res,200,'role_updated_successfully');
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        next(error);
      })
      
      
    }
  } catch (error) {
      next(error);
  }
  
}

exports.getRoleList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    let pagination = {};
    const user = _.get(req, 'user');
    value.roleId = user.role_id
    let data = await roleModel.get(value,page_number);
    let roleList = roleModel.encrypt(data[0]);
    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, roleList,pagination);
  }catch (error) {
    next(error);
  }
}

exports.updateStatus = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    let id = req.body.id;
    const userId = _.get(req.headers, 'userid');
    params.updated_by = userId;
    
    let statusObj = roleModel.getModel(params)
    roleModel.updateOne(id,statusObj)
    .then(response => {
      if(response && response.length){
        ApiController.sendResponse(req, res,200,'status_updated_successfully');
      }else{
        ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
      }
    })
    .catch(error => {
      next(error);
    })
  } catch (error) {
      next(error);
  }
  
}



