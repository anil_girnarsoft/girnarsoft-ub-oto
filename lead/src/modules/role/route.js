const express = require('express');
const router = express.Router();
const schemas = require('./validation_schema');
const validation = require(MIDDLEWARE_PATH + 'validation');
const RoleController = require('./controller/roleController');

router.post('/listRole', RoleController.getRoleList);
router.post('/saveRole',validation(schemas.createRole, 'body'), RoleController.saveRole);
router.post('/editRole',validation(schemas.updateRole, 'body'),RoleController.saveRole);
router.post('/updateStatus',validation(schemas.updateStatus, 'body'),RoleController.updateStatus);

module.exports = router;