const express = require('express');
const router = express.Router();
// const schemas = require('./validation_schema');
// const validation = require(MIDDLEWARE_PATH + 'validation');
const ReportController = require('./controller/reportController');
const DealerReportController = require('./controller/dealerReportController');
const LeadReportController = require('./controller/leadReportController');
const DealerReportCronController = require('./controller/dealerReportCronController');
const ProcessDealerDeliveryReportCtrl = require(`../cron-job/controller/processDealerDeliveryReport`);

router.post('/dealerDeliveryReport', ReportController.getDealerDeliveryReport);
router.post('/getSalesFunnelReport',ReportController.getSalesFunnelReport);
router.post('/getLeadSnapshotReport',ReportController.getLeadSnapshotReport);
router.post('/getLocationWiseLeadCountReport',ReportController.getLocationWiseLeadCountReport);
router.post('/getL1CallingReport',ReportController.getL1CallingReport);
router.get('/dealerReport', DealerReportController.getDealerReport);
router.get('/leadReport', LeadReportController.getLeadReport);
router.get('/dealerReportCron', DealerReportCronController.getDealerReport);
router.post('/saveEmailAndFilters', ReportController.saveEmailAndFilters);
router.post('/importDealerDeliveryCsv', ProcessDealerDeliveryReportCtrl.sendDealerDeliveryReport);

module.exports = router;