const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class ReportSearchHistory extends Model{
}

ReportSearchHistory.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    data:{
        type: dbConfig.Sequelize.STRING,
        allowNull: false,
    },
    user_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    type:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    created_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull: false,
    },
    modified_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull: false,
    },
    search_title:{
        type: dbConfig.Sequelize.STRING,
        allowNull: false,
    },
    is_delete:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    },
    status: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_DEALER_BOOST,
    freezeTableName: true
});


ReportSearchHistory.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await ReportSearchHistory.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

ReportSearchHistory.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await ReportSearchHistory.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

ReportSearchHistory.get = async (_where,pagination) => {

    let whereCond = await ReportSearchHistory.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS * FROM \n\
        "+UBLMS_REPORT_SEARCH_HISTORY+" "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await ReportSearchHistory.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

ReportSearchHistory.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond =  await ReportSearchHistory.bindPagination(params,pagination,isPaginationApply);
    if(params && params.hasOwnProperty('status')){
        let status = "status='"+params.status+"'";
        condition = (condition == '')? condition + "where "+status : condition+" and "+status 
    }
    if(params && params.id){
        let id = "id='"+ReportSearchHistory.decrypt(params.id)+"'";
        condition = (condition == '') ? condition + "where "+id : condition+" and "+id 
    }
    
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

ReportSearchHistory.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

ReportSearchHistory.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}




module.exports = ReportSearchHistory;