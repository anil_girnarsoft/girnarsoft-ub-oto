const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const clusterModel = require("../../cluster/model/clusterModel");
const moment = require("moment");

class LocationWiseLeadCountModel extends Model{
}

LocationWiseLeadCountModel.get = async (_where,authUser) => {
    let sql='';
    //$searchConditions   =   array();
    //$searchConditions   =   $this->bindSearchConditions($srhData); //debug($searchConditions);exit;
    let srh_addedDateForm   = (_where['added_date_from']) ? moment(_where['added_date_from']).format('YYYY-MM-DD') : '' ;
    let srh_addedDateTo     = (_where['added_date_to'])? moment(_where['added_date_from']).format('YYYY-MM-DD') : '' ;    
    let srhLeadConditions = '';
    if(srh_addedDateForm && srh_addedDateTo){  
        srhLeadConditions += " l.created_at >= '"+srh_addedDateForm+"' and l.created_at <= '"+srh_addedDateTo+"'";
    } else if(srh_addedDateForm){  
        srhLeadConditions += " l.created_at >= '"+srh_addedDateForm+"'";
    } else if(srh_addedDateTo){  
        srhLeadConditions += " l.created_at <= '"+srh_addedDateTo+"'";
    } else if(srh_addedDateTo == '' && srh_addedDateForm == ''){  
        let defaultToDueDate = moment().format('YYYY-MM-DD') //dateTimeFormat(date('Y-m-d'));
        defaultFromDueDate = moment().format('YYYY-MM-DD');
        srhLeadConditions += " l.created_at >= '"+defaultToDueDate+"' and l.created_at <= '"+defaultFromDueDate+"'";
    }
    let whereCond = await LocationWiseLeadCountModel.bindCondition(_where,authUser);
    return new Promise(function (resolve, reject) {
        let sql_fields = "clust.name as cluster_name, ucity.city_id as city_id, l.location_name, lc.totalLeads,plc.totalPremiumLeads,nplc.totalnonPremiumLeads ";
       
        sql = "Select "+sql_fields+" from "+UBLMS_LOCATIONS+" l left join "+UBLMS_CITY_CLUSTER+" ucity on l.city_id = ucity.city_id left join "+
        UBLMS_CLUSTER+" clust on ucity.cluster_id = clust.id left join (select cus.customer_location_id, count(l.id) as totalLeads "+
        "from "+UBLMS_CUSTOMER+" cus INNER JOIN "+UBLMS_LEADS+" l ON cus.id = l.customer_id where "+srhLeadConditions+" group by cus.customer_location_id) lc "+
        "on l.id = lc.customer_location_id LEFT JOIN  (SELECT   cus.customer_location_id,COUNT(l.id) AS totalPremiumLeads "+
        "FROM  "+UBLMS_CUSTOMER+" cus INNER JOIN  "+UBLMS_LEADS+" l ON cus.id = l.customer_id WHERE  "+srhLeadConditions+" AND l.is_premium = 1 "+
        "GROUP BY  cus.customer_location_id) plc ON l.id = plc.customer_location_id LEFT JOIN  (SELECT  cus.customer_location_id,COUNT(l.id) AS totalnonPremiumLeads FROM "+
        UBLMS_CUSTOMER+" cus INNER JOIN  "+UBLMS_LEADS+" l ON cus.id = l.customer_id WHERE  "+srhLeadConditions+" AND l.is_premium = 2 "+
        "GROUP BY  cus.customer_location_id) nplc ON l.id = nplc.customer_location_id ";
        if (whereCond) {
            sql +=  whereCond;
        }            
        sql   += " order by l.id ASC";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await LocationWiseLeadCountModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            //console.log('error=========',error)
            reject(error);
        });
    });
}

LocationWiseLeadCountModel.bindCondition = async (params,authUser) => {
    let cluster        = (params['cluster'] && params['cluster'].length ) ? params['cluster']:'' ;
    let srh_city       = (params['city'] && params['city'].length ) ? params['city']:'' ;
    let srh_location   = (params['location'] && params['location'].length ) ? params['location']:'';
    let condition = '';
    let clustersCity = '';
    let userRoleId = authUser['role_id'];
    if (cluster) {
        let clustersCityIds = await clusterModel.getClusterCity(cluster);
        if(clustersCityIds && clustersCityIds.length){
            clustersCity = clustersCityIds.map(city=>{return city.city_id})
        }

   }
   if ((srh_city && srh_city != '') || (clustersCity)) {
       let srh_city_arr = srh_city;
       if ((srh_city_arr)) {
           _.forEach(srh_city_arr,(obj,index)=>{
               if (obj == CLUSTER_DELHI_NCR) {
                   srh_city_arr[index] = DELHI_NCR_CITY_IDS;
               }
           })
       }
       let str = '';
       if (srh_city_arr && srh_city_arr.length) {
           str = srh_city_arr.toString();
       }
       if (clustersCity) {
           if (str) {
               str = str + ',' + clustersCity;
           } else {
               str = clustersCity;
           }
       }
       
       let customerCity = " l.city_id IN (" + str + ")";
       condition = (condition == '')? condition + "where "+customerCity : condition+" and "+customerCity 
    }
         
    if(srh_location && srh_location.length){  
        let location = " l.id IN ("+srh_location+")";
        condition = (condition == '')? condition + "where "+location : condition+" and "+location 
    }                    
    
    return condition;
}

LocationWiseLeadCountModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

LocationWiseLeadCountModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

module.exports = LocationWiseLeadCountModel;