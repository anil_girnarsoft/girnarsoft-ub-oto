const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const clusterModel = require("../../cluster/model/clusterModel");
const moment = require('moment');

class SalesFunnelReportModel extends Model{
}


SalesFunnelReportModel.get = async (_where, pagination) => {
    let sql='',srhwalkinConditions = '',srhConversionConditions = '',srhLeadLogConditions = '',
    srhUniqueLeadConditions = '',srhConversationConditions = '';
    let isPremiumCondition = '1=1',isPremiumLeadLogCondition = '1=1';         
    let srh_addedDateForm = (_where['from_date']) ? moment(_where['from_date']).format('YYYY-MM-DD') : '';
    let srh_addedDateTo = (_where['to_date']) ? moment(_where['to_date']).format('YYYY-MM-DD') : '';
    srh_addedDateForm = (srh_addedDateForm)?srh_addedDateForm+' 00:00:00':'';
    srh_addedDateTo = (srh_addedDateTo)?srh_addedDateTo+' 23:59:59':'';
    if (srh_addedDateForm && srh_addedDateTo) {
        srhwalkinConditions += " w.created_at >= '" + srh_addedDateForm + "' and w.created_at <= '" + srh_addedDateTo + "'";
        srhConversionConditions += " ci.added_on >= '" + srh_addedDateForm + "' and ci.added_on <= '" + srh_addedDateTo + "'";
        srhLeadLogConditions += " ll.created_at >= '" + srh_addedDateForm + "' and ll.created_at <= '" +srh_addedDateTo + "'";
        srhUniqueLeadConditions += " l.created_at >= '" +srh_addedDateForm + "' and l.created_at <= '" + srh_addedDateTo + "'";
        srhConversationConditions += " cc.added_on >= '" +srh_addedDateForm + "' and cc.added_on <= '" +srh_addedDateTo + "'";
    } else if (srh_addedDateForm) {
        srhwalkinConditions += " w.created_at >= '" + srh_addedDateForm + "'";
        srhConversionConditions += " ci.added_on >= '" + srh_addedDateForm + "'";
        srhLeadLogConditions += " ll.created_at >= '"  +srh_addedDateForm + "'";
        srhUniqueLeadConditions += " l.created_at >= '" + srh_addedDateForm + "'";
        srhConversationConditions += " cc.added_on >= '" + srh_addedDateForm + "'";
    } else if (srh_addedDateTo) {
        srhwalkinConditions += " w.created_at <= '" +srh_addedDateTo + "'";
        srhConversionConditions += " ci.added_on <= '" + srh_addedDateTo + "'";
        srhLeadLogConditions += " ll.created_at <= '" + srh_addedDateTo + "'";
        srhUniqueLeadConditions += " l.created_at <= '" + srh_addedDateTo + "'";
        srhConversationConditions += " cc.added_on <= '" + srh_addedDateTo + "'";
    } 
    else if (srh_addedDateForm == '' && srh_addedDateTo == '' ) {
        let defaultToDueDate =  moment().format('YYYY-MM-DD');
        let defaultFromDueDate = moment().format('YYYY-MM-DD');
        srhwalkinConditions += " w.created_at >= '" + defaultToDueDate + "' and w.created_at <= '" + defaultFromDueDate + "'";
        srhConversionConditions += " ci.added_on >= '" + defaultToDueDate + "' and ci.added_on <= '" + defaultFromDueDate + "'";
        srhLeadLogConditions += " ll.created_at >= '" + defaultToDueDate + "' and ll.created_at <= '" + defaultFromDueDate + "'";
        srhUniqueLeadConditions += " l.created_at >= '" + defaultToDueDate + "' and l.created_at <= '" + defaultFromDueDate + "'";
        srhConversationConditions += " cc.added_on >= '" + defaultToDueDate + "' and cc.added_on <= '" + defaultFromDueDate + "'";
    }
    let isPremium = _where.lead_type;
    if(isPremium) {
        if(isPremium == '1') {
            isPremiumCondition = " l.is_premium = 1 ";
            isPremiumLeadLogCondition = " ll.is_premium = 1 ";
        } else if(isPremium == '2') {
            isPremiumCondition = " l.is_premium != 1 ";
            isPremiumLeadLogCondition = " ll.is_premium != 1 ";
        }
    }

    let paginationCond =  await SalesFunnelReportModel.bindPagination(_where,pagination,true);
    let whereCond = await SalesFunnelReportModel.bindCondition(_where);
    return new Promise(function (resolve, reject) {
       // _where['report_type']=2
        let sql_fields = "t.*,tl.totalLead,u.UniqueLead,e.eligibleDialerLead,d.dialedLead,c.totalContacted,v.totalVerified,ws.totalWalinSchd,wc.totalWalinDone,p.totalConversion,pConf.totalConfConversion";
            if(_where['report_type'] && _where['report_type'] == 2) {
                sql = 'select ' + sql_fields + ' from (select (case when cl.name IS NULL THEN "Unassigned" ELSE cl.name END) as cluster_name, \n'+
                'c.customer_city_id, ct.city_id as customer_city,l.is_premium,l.source_id, l.sub_source_id,s.name as source_name, ss.name as sub_source_name,\n'+
                'CASE WHEN ss.paid_details="1" THEN "Paid"  WHEN ss.paid_details="2" THEN "Organic" WHEN ss.paid_details = "3" \n'+
                'THEN "Others" ELSE "Unassigned" END AS category from '+UBLMS_LEADS+' l left join '+UBLMS_CUSTOMER+' c on \n'+
                'l.customer_id = c.id left join '+UBLMS_CITY_CLUSTER+' ct ON ct.city_id = c.customer_city_id left join \n'+
                UBLMS_CLUSTER+' cl ON cl.id = ct.cluster_id left join '+UBLMS_SOURCE+' s ON s.id = l.source_id \n'+
                'left join '+UBLMS_SUB_SOURCE+' ss ON ss.id = l.sub_source_id where '+isPremiumCondition+' group by \n'+
                'c.customer_city_id, l.source_id, l.sub_source_id)t  Left join (select c.customer_city_id,ll.source_id, \n'+
                'll.sub_source_id,count(ll.id) as TotalLead from '+UBLMS_LEADS_LOG+' ll left join '+UBLMS_CUSTOMER+
                ' c on ll.customer_id = c.id  where '+srhLeadLogConditions+' and '+isPremiumLeadLogCondition+
                ' group by c.customer_city_id, ll.source_id, ll.sub_source_id)tl on t.customer_city_id = tl.customer_city_id \n'+
                'and t.source_id = tl.source_id and t.sub_source_id = tl.sub_source_id Left join (select c.customer_city_id,\n'+
                'l.source_id, l.sub_source_id,count(l.id) as UniqueLead from '+UBLMS_LEADS+' l left join '+UBLMS_CUSTOMER+
                ' c on l.customer_id = c.id  where '+srhUniqueLeadConditions+' and '+isPremiumCondition+' group by c.customer_city_id,\n'+
                'l.source_id, l.sub_source_id)u on t.customer_city_id = u.customer_city_id and t.source_id = u.source_id and \n'+
                't.sub_source_id = u.sub_source_id Left join (select c.customer_city_id,l.source_id, l.sub_source_id,count(l.id) \n'+
                'as eligibleDialerLead from '+UBLMS_LEADS+' l left join '+UBLMS_CUSTOMER+' c on l.customer_id = c.id \n'+
                'where l.isIndividualLeadDialer = \'1\' and '+srhUniqueLeadConditions+' and '+isPremiumCondition+' group by \n'+
                'c.customer_city_id, l.source_id, l.sub_source_id)e on t.customer_city_id = e.customer_city_id and t.source_id = \n'+
                'e.source_id and t.sub_source_id = e.sub_source_id Left join (select c.customer_city_id,l.source_id, l.sub_source_id,\n'+
                'count(l.id) as dialedLead from '+UBLMS_LEADS+' l left join '+UBLMS_CUSTOMER+' c on l.customer_id = c.id \n'+
                'where l.calling_status_id <> 1 and '+srhUniqueLeadConditions+' and '+isPremiumCondition+' group by c.customer_city_id, \n'+
                'l.source_id, l.sub_source_id) d on t.customer_city_id = d.customer_city_id and t.source_id = d.source_id and t.sub_source_id = d.sub_source_id \n'+
                'Left join (select c.customer_city_id,l.source_id, l.sub_source_id,count(distinct l.id) as totalContacted from '+UBLMS_LEADS+
                ' l left join '+UBLMS_CUSTOMER+' c on l.customer_id = c.id inner join ' + UBLMS_CONVERSION + ' cc on l.id = \n'+
                'cc.lead_id where cc.status_id NOT IN(0,1) and cc.sub_status_id NOT IN(18,19) and cc.leads_cars_id = 0 and '+isPremiumCondition+
                ' and '+srhUniqueLeadConditions+' group by c.customer_city_id, l.source_id, l.sub_source_id) c \n'+
                'on t.customer_city_id = c.customer_city_id and t.source_id = c.source_id and t.sub_source_id = \n'+
                'c.sub_source_id Left join (select c.customer_city_id,l.source_id, l.sub_source_id,count(distinct l.id) as totalVerified from \n'+
                UBLMS_LEADS+' l left join '+UBLMS_CUSTOMER+' c on l.customer_id = c.id inner join ' + UBLMS_CONVERSION + ' cc on \n'+
                'l.id = cc.lead_id where cc.status_id IN(3,5,6) and cc.leads_cars_id = 0 and '+isPremiumCondition+' and '+srhUniqueLeadConditions+
                ' group by c.customer_city_id, l.source_id, l.sub_source_id) v on t.customer_city_id = v.customer_city_id and t.source_id = v.source_id \n'+
                'and t.sub_source_id = v.sub_source_id Left join (select c.customer_city_id,l.source_id, l.sub_source_id,count(distinct w.lead_id) \n'+
                'as totalWalinSchd from ' + UBLMS_WALKING_INFO + ' w inner join ' + UBLMS_LEADS + ' l ON(w.lead_id = l.id) left join \n'+
                UBLMS_CUSTOMER + ' c on l.customer_id = c.id where w.walkin_status IN(2,3) and w.status = 1 and '+isPremiumCondition+' and \n'+ 
                srhUniqueLeadConditions + ' group by c.customer_city_id, l.source_id, l.sub_source_id) ws on t.customer_city_id = ws.customer_city_id \n'+
                'and t.source_id = ws.source_id and t.sub_source_id = ws.sub_source_id Left join (select c.customer_city_id,l.source_id, l.sub_source_id,\n'+
                'count(distinct w.lead_id) as totalWalinDone from ' + UBLMS_WALKING_INFO + ' w inner join ' + UBLMS_LEADS + ' l ON(w.lead_id = l.id) left join \n'+
                UBLMS_CUSTOMER + ' c on l.customer_id = c.id where w.walkin_status = 4 and w.status = 1 and '+isPremiumCondition+' and \n'+
                srhUniqueLeadConditions + ' group by c.customer_city_id, l.source_id, l.sub_source_id) wc \n'+
                'on t.customer_city_id = wc.customer_city_id and t.source_id = wc.source_id and t.sub_source_id = wc.sub_source_id Left join \n'+
                '(select c.customer_city_id,l.source_id, l.sub_source_id,count(distinct ci.lead_id) as totalConversion from ' + UBLMS_CONVERSION_INFO + ' ci inner join \n'+
                UBLMS_LEADS + ' l ON(ci.lead_id = l.id) left join ' + UBLMS_CUSTOMER + ' c on l.customer_id = c.id where  ci.status = 1 and \n'+
                isPremiumCondition+' and ' +srhUniqueLeadConditions + ' group by c.customer_city_id, l.source_id, l.sub_source_id) p \n'+
                'on t.customer_city_id = p.customer_city_id and t.source_id = p.source_id and t.sub_source_id = p.sub_source_id left join \n'+
                '(select c.customer_city_id,l.source_id, l.sub_source_id,count(distinct ci.lead_id) as totalConfConversion from \n' + 
                UBLMS_CONVERSION_INFO + ' ci inner join ' + UBLMS_LEADS + ' l ON(ci.lead_id = l.id) left join ' + UBLMS_CUSTOMER + ' c on l.customer_id = c.id \n'+
                'where  ci.status = 1 and ci.cnfm_status = 1 and '+isPremiumCondition+' and ' +srhUniqueLeadConditions + ' group by c.customer_city_id, l.source_id, \n'+
                'l.sub_source_id) pConf on t.customer_city_id = pConf.customer_city_id and t.source_id = pConf.source_id and t.sub_source_id = pConf.sub_source_id '+whereCond;
            } else { 
                sql = 'select ' +sql_fields + ' from (select (case when cl.name IS NULL THEN "Unassigned" ELSE cl.name END) as cluster_name, c.customer_city_id, \n'+
                'l.is_premium,l.source_id, l.sub_source_id,s.name as source_name, ss.name as sub_source_name,CASE WHEN ss.paid_details="1" THEN "Paid"  \n'+
                'WHEN ss.paid_details="2" THEN "Organic" WHEN ss.paid_details = "3" THEN "Others" ELSE "Unassigned" END AS category from '+UBLMS_LEADS+' l left join \n'+
                UBLMS_CUSTOMER+' c on l.customer_id = c.id left join \n'+
                UBLMS_CLUSTER+' cl ON cl.id = c.customer_city_id left join '+UBLMS_SOURCE+' s ON s.id = l.source_id left join \n'+
                UBLMS_SUB_SOURCE+' ss ON ss.id = l.sub_source_id where '+isPremiumCondition+' group by c.customer_city_id, \n'+
                'l.source_id, l.sub_source_id)t Left join (select c.customer_city_id,ll.source_id, ll.sub_source_id,count(ll.id) \n'+
                'as TotalLead from '+UBLMS_LEADS_LOG+' ll left join '+UBLMS_CUSTOMER+' c on ll.customer_id = c.id  where \n'+
                srhLeadLogConditions+' and '+isPremiumLeadLogCondition+' group by c.customer_city_id, ll.source_id, ll.sub_source_id)tl \n'+
                'on t.customer_city_id = tl.customer_city_id and t.source_id = tl.source_id and t.sub_source_id = tl.sub_source_id Left join \n'+
                '(select c.customer_city_id,l.source_id, l.sub_source_id,count(l.id) as UniqueLead from '+UBLMS_LEADS+' l left join \n'+
                UBLMS_CUSTOMER+' c on l.customer_id = c.id  where '+srhUniqueLeadConditions+' and '+isPremiumCondition+' group by \n'+
                'c.customer_city_id, l.source_id, l.sub_source_id)u on t.customer_city_id = u.customer_city_id and t.source_id = u.source_id \n'+
                'and t.sub_source_id = u.sub_source_id Left join (select c.customer_city_id,l.source_id, l.sub_source_id,count(l.id) as \n'+
                'eligibleDialerLead from '+UBLMS_LEADS+' l left join '+UBLMS_CUSTOMER+' c on l.customer_id = c.id  where \n'+
                'l.isIndividualLeadDialer = \'1\' and '+srhUniqueLeadConditions+' and '+isPremiumCondition+' group by c.customer_city_id, \n'+
                'l.source_id, l.sub_source_id)e on t.customer_city_id = e.customer_city_id and t.source_id = e.source_id and t.sub_source_id = e.sub_source_id \n'+
                'Left join (select tc.customer_city_id,tc.source_id, tc.sub_source_id,count(tc.id) as dialedLead from (select cc.id as conv_id,l.id,\n'+
                'c.customer_city_id,l.source_id, l.sub_source_id, cc.added_on from ' + UBLMS_CONVERSION + ' cc inner join ' +UBLMS_LEADS + ' l ON(cc.lead_id = l.id) \n'+
                'left join ' +UBLMS_CUSTOMER + ' c on l.customer_id = c.id where cc.status_id <> 0 and cc.calling_status_id <> 1 and cc.leads_cars_id = 0 and \n'+
                isPremiumCondition+' group by l.id,c.customer_city_id,l.source_id, l.sub_source_id having cc.id = min(cc.id) and \n'+
                srhConversationConditions+') tc group by tc.customer_city_id,tc.source_id, tc.sub_source_id) d \n'+
                'on t.customer_city_id = d.customer_city_id and t.source_id = d.source_id and t.sub_source_id = d.sub_source_id Left join \n'+
                '(select tc.customer_city_id,tc.source_id, tc.sub_source_id,count(tc.id) as totalContacted from (select cc.id as conv_id,l.id,c.customer_city_id,l.source_id, \n'+
                'l.sub_source_id, cc.added_on from ' + UBLMS_CONVERSION + ' cc inner join ' +UBLMS_LEADS + ' l ON(cc.lead_id = l.id) left join \n' +
                UBLMS_CUSTOMER + ' c on l.customer_id = c.id where cc.status_id NOT IN(0,1) and cc.sub_status_id NOT IN(18,19) and cc.leads_cars_id = 0 \n'+
                'and '+isPremiumCondition+' group by l.id,c.customer_city_id,l.source_id, l.sub_source_id having cc.id = min(cc.id) and '+srhConversationConditions+') tc \n'+
                'group by tc.customer_city_id,tc.source_id, tc.sub_source_id) c on t.customer_city_id = c.customer_city_id and t.source_id = c.source_id and t.sub_source_id = c.sub_source_id Left join \n'+
                '(select tv.customer_city_id,tv.source_id, tv.sub_source_id,count(tv.id) as totalVerified from (select cc.id as conv_id,l.id,c.customer_city_id,l.source_id, \n'+
                'l.sub_source_id,cc.added_on from ' + UBLMS_CONVERSION + ' cc inner join ' +UBLMS_LEADS + ' l ON(cc.lead_id = l.id) left join ' +UBLMS_CUSTOMER + ' c on \n'+
                'l.customer_id = c.id where cc.status_id IN(3,5,6) and cc.leads_cars_id = 0 and '+isPremiumCondition+'  group by l.id,c.customer_city_id,\n'+
                'l.source_id, l.sub_source_id   having cc.id = min(cc.id) and '+srhConversationConditions+') tv group by tv.customer_city_id,tv.source_id, tv.sub_source_id) v \n'+
                'on t.customer_city_id = v.customer_city_id and t.source_id = v.source_id and t.sub_source_id = v.sub_source_id Left join \n'+
                '(select c.customer_city_id,l.source_id, l.sub_source_id,count(distinct w.lead_id) as totalWalinSchd from ' + UBLMS_WALKING_INFO + ' w inner join \n'+
                UBLMS_LEADS + ' l ON(w.lead_id = l.id) left join ' + UBLMS_CUSTOMER + ' c on l.customer_id = c.id where w.walkin_status IN(2,3) \n'+
                'and w.status = 1 and ' +srhwalkinConditions + ' and '+isPremiumCondition+' group by c.customer_city_id, l.source_id, l.sub_source_id) ws \n'+
                'on t.customer_city_id = ws.customer_city_id and t.source_id = ws.source_id and t.sub_source_id = ws.sub_source_id Left join \n'+
                '(select c.customer_city_id,l.source_id, l.sub_source_id,count(distinct w.lead_id) as totalWalinDone from ' + UBLMS_WALKING_INFO + ' w inner join \n' + 
                UBLMS_LEADS + ' l ON(w.lead_id = l.id) left join ' + UBLMS_CUSTOMER + ' c on l.customer_id = c.id where w.walkin_status = 4 and w.status = 1 and \n'+
                srhwalkinConditions + ' and '+isPremiumCondition+' group by c.customer_city_id, l.source_id, l.sub_source_id) wc on t.customer_city_id = wc.customer_city_id and \n'+
                't.source_id = wc.source_id and t.sub_source_id = wc.sub_source_id Left join (select c.customer_city_id,l.source_id, l.sub_source_id,count(distinct ci.lead_id) \n'+
                'as totalConversion from ' + UBLMS_CONVERSION_INFO + ' ci inner join ' + UBLMS_LEADS + ' l ON(ci.lead_id = l.id) left join ' + UBLMS_CUSTOMER + ' c on \n'+
                'l.customer_id = c.id where ci.status = 1 and ' +srhConversionConditions + ' and '+isPremiumCondition+' group by c.customer_city_id, \n'+
                'l.source_id, l.sub_source_id) p on t.customer_city_id = p.customer_city_id and t.source_id = p.source_id and t.sub_source_id = p.sub_source_id left join \n'+
                '(select c.customer_city_id,l.source_id, l.sub_source_id,count(distinct ci.lead_id) as totalConfConversion from ' + UBLMS_CONVERSION_INFO + ' ci inner join \n' +
                UBLMS_LEADS + ' l ON(ci.lead_id = l.id) left join ' + UBLMS_CUSTOMER + ' c on l.customer_id = c.id where ci.status = 1 and ci.cnfm_status = 1 and \n' +
                srhConversionConditions + ' and '+isPremiumCondition+' group by c.customer_city_id, l.source_id, l.sub_source_id) pConf on t.customer_city_id = pConf.customer_city_id \n'+
                'and t.source_id = pConf.source_id and t.sub_source_id = pConf.sub_source_id '+whereCond; 
            }
            if(paginationCond){
                sql += paginationCond;
            }
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await SalesFunnelReportModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

SalesFunnelReportModel.bindCondition = async (params) => {
    let cluster = (params['cluster'] && params['cluster'].length) ? params['cluster'] : '';
    let srh_city =(params['city'] && params['city'].length) ? params['city'] : '';        
    let srh_sourceId = (params['source'] && params['source'].length) ? params['source'].toString() : '';
    let srh_sub_sourceId = (params['sub_source'] && params['sub_source'].length) ? params['sub_source'].toString() : '';
    let srhConditions = {},condition='',clustersCity='';
    //$userRoleId = $_SESSION['AdminUsers']['role_id'];
    if (cluster) {
         let clustersCityIds = await clusterModel.getClusterCity(cluster);
         if(clustersCityIds && clustersCityIds.length){
             clustersCity = clustersCityIds.map(city=>{return city.city_id})
         }

    }
    if ((srh_city && srh_city != '') || (clustersCity)) {
        let srh_city_arr = srh_city;
        if ((srh_city_arr)) {
            _.forEach(srh_city_arr,(obj,index)=>{
                if (obj == CLUSTER_DELHI_NCR) {
                    srh_city_arr[index] = DELHI_NCR_CITY_IDS;
                }
            })
        }
        let str = '';
        if (srh_city_arr && srh_city_arr.length) {
            str = srh_city_arr.toString();
        }
        if (clustersCity) {
            if (str) {
                str = str + ',' + clustersCity;
            } else {
                str = clustersCity;
            }
        }
        
        let customerCity = " t.customer_city_id IN (" + str + ")";
        condition = (condition == '')? condition + "where "+customerCity : condition+" and "+customerCity 
    }else if(srh_city){

    }
    if (srh_sourceId) {
        let sourceId = " t.source_id IN (" + srh_sourceId + ")";
        condition = (condition == '')? condition + "where "+sourceId : condition+" and "+sourceId 
    }
    if (srh_sub_sourceId) {
        let sub_source = " t.sub_source_id IN (" + srh_sub_sourceId + ")";
        condition = (condition == '') ? condition + "where "+sub_source : condition+" and "+sub_source 
    }
    
    return condition;
}

SalesFunnelReportModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

SalesFunnelReportModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}




module.exports = SalesFunnelReportModel;