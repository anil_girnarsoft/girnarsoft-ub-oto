const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const clusterModel = require("../../cluster/model/clusterModel");
const moment = require('moment');

class LeadSnapshotReportModel extends Model{
}

LeadSnapshotReportModel.get = async (_where,authUser) => {
    let sql='';
    let srh_addedDateFrom   = (_where['added_date_from'] ) ? moment(_where['added_date_from']).format('YYYY-MM-DD') : '' ;
       let srh_addedDateTo     = _where['added_date_to'] ? moment(_where['added_date_to']).format('YYYY-MM-DD'):'' ;    
       let srh_dueDateFrom   = (_where['due_date_from']) ? moment(_where['due_date_from']).format('YYYY-MM-DD') : '' ;
       let srh_dueDateTo     = (_where['due_date_to']) ? moment(_where['due_date_to']).format('YYYY-MM-DD') : '' ; ;  
       let premium     =      (_where['is_premium'] ) ? _where['is_premium']:'' ;  
       let srhLeadDateConditions = '';
       
        if(srh_dueDateFrom && srh_dueDateTo){  
          srhLeadDateConditions += " l.due_date >= '"+srh_dueDateFrom+"' and l.due_date <= '"+srh_dueDateTo+"'";                   
        } else if(srh_dueDateFrom){ 
          srhLeadDateConditions += " l.due_date >= '"+srh_dueDateFrom+"'";                   
        } else if(srh_dueDateTo){  
          srhLeadDateConditions += " l.due_date <= '"+srh_dueDateTo+"'";                   
        }
        if(srh_addedDateFrom && srh_dueDateFrom){
           srhLeadDateConditions += " and ";
        }
                       
        if(srh_addedDateFrom && srh_addedDateTo){  
          srhLeadDateConditions += " l.created_at >= '"+srh_addedDateFrom+"' and l.created_at <= '"+srh_addedDateTo+"'";                   
        } else if(srh_addedDateFrom){ 
          srhLeadDateConditions += " l.created_at >= '"+srh_addedDateFrom+"'";                   
        } else if(srh_addedDateTo){  
          srhLeadDateConditions += " l.created_at <= '"+srh_addedDateTo+"'";                   
        } else if(srh_addedDateFrom == '' && srh_addedDateTo == '' && srh_dueDateFrom == '' && srh_dueDateTo == ''){  
          let defaultToDueDate = moment().format('YYYY-MM-DD');
          let defaultFromDueDate = moment().format('YYYY-MM-DD');
          srhLeadDateConditions += " l.created_at >= '"+defaultToDueDate+"' and l.created_at <= '"+defaultFromDueDate+"'";                   
        }  
       
        if(premium) {
            srhLeadDateConditions += " and l.is_premium='1' ";
        }
        
    let whereCond = await LeadSnapshotReportModel.bindCondition(_where,authUser);
    return new Promise(function (resolve, reject) {
        let sql_fields = " t.*, nyc.totalNyc,allL.allLeads, first.totalFirst, sec.totalSec, "+
        "third.totalThird, fourth.totalFourth,more.totalMore,reached.totalReached, reachedFirst.totalReachedFirst, reachedSec.totalReachedSec, "+
        "reachedThird.totalReachedThird,reachedFourth.totalReachedFourth,reachedMore.totalReachedMore, vi.totalVerifyInt, "+
        "vf.totalVerifyFollowup, vp.totalVerifyPostponed, ws.totalWalkinSchd, pp.totalPurchasePending,pf.totalPurchaseFollowup, "+
        "pc.totalPurchaseConverted, ci.totalClosedInt, cwn.totalClosedWrong, cgi.totalClosedGaadiIndividual, cngi.totalClosedNonGaadiIndividual, "+
        "cngd.totalClosedNonGaadiDealer,cpuc.totalClosedPurchaseCar, cae.totalClosedAttemptElapsed, cpnc.totalPurchasedNewCar,cb.totalClosedBlocked, coc.totalClosedOutCity  ";
       
        sql = "SELECT "+sql_fields+" FROM (select cl.id as cluster_id, cl.name as cluster_name from ublms_cluster cl)t left join  "+
        "(select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, "+
        "count(l.id) as totalNyc from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct "+
        "ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '1' and "+
        "l.sub_status_id = '1' and l.calling_status_id = '1' and l.source_id <> '7' and l.isIndividualLeadDialer = '1'  and "+
        srhLeadDateConditions+" group by cluster_name)nyc ON t.cluster_id = nyc.cluster_id Left join (select cl.id as cluster_id,(case when cl.name IS NULL "+
        "THEN 'Unassigned' ELSE cl.name END)as cluster_name,count(l.id) as allLeads FROM "+UBLMS_LEADS+" l LEFT JOIN "+UBLMS_CUSTOMER+" c ON l.customer_id = c.id "+
        "LEFT JOIN "+UBLMS_CITY_CLUSTER+" ct ON c.customer_city_id = ct.city_id LEFT JOIN "+UBLMS_CLUSTER+" cl ON ct.cluster_id  = cl.id "+
        "WHERE 1=1 and "+srhLeadDateConditions+" GROUP BY cluster_name) allL ON t.cluster_id = allL.cluster_id Left join (select cl.id as cluster_id,"+
        "(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalFirst from "+
        UBLMS_LEADS+" l left join "+UBLMS_CUSTOMER+" c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join "+
        UBLMS_CLUSTER+" cl ON cl.id = ct.cluster_id where l.status_id = '1'  and l.source_id <> '7'  and l.num_attempt = '1' and "+srhLeadDateConditions+" group by cluster_name)first "+
        "ON t.cluster_id = first.cluster_id Left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as "+
        "cluster_name, count(l.id) as totalSec from "+UBLMS_LEADS+" l left join ublms_customer c on l.customer_id = c.id left join "+
        UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join "+UBLMS_CLUSTER+" cl ON cl.id = ct.cluster_id where l.status_id = '1' "+
        "and l.source_id <> '7'  and l.num_attempt = '2' and "+srhLeadDateConditions+" group by cluster_name)sec ON t.cluster_id = sec.cluster_id Left join  "+
        "(select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalThird from "+
        UBLMS_LEADS+" l left join "+UBLMS_CUSTOMER+" c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join "+
        UBLMS_CLUSTER+" cl ON cl.id = ct.cluster_id where l.status_id = '1'  and l.source_id <> '7'  and l.num_attempt = '3' and "+srhLeadDateConditions+" group by cluster_name)third "+
        "ON t.cluster_id = third.cluster_id Left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, "+
        "count(l.id) as totalFourth from "+UBLMS_LEADS+" l left join "+UBLMS_CUSTOMER+" c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id "+
        "left join "+UBLMS_CLUSTER+" cl ON cl.id = ct.cluster_id where l.status_id = '1'  and l.source_id <> '7'  and l.num_attempt = '4' and "+srhLeadDateConditions+" group by cluster_name)fourth "+
        "ON t.cluster_id = fourth.cluster_id Left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalMore from "+
        UBLMS_LEADS+" l left join "+UBLMS_CUSTOMER+" c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join "+UBLMS_CLUSTER+" cl ON cl.id = "+
        "ct.cluster_id where l.status_id = '1'  and l.source_id <> '7'  and l.num_attempt > '4' and "+srhLeadDateConditions+" group by cluster_name)more ON t.cluster_id = more.cluster_id Left join "+
        "(select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalReached from ublms_leads l left join "+
        "ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where "+
        "l.status_id = '2'  and l.source_id <> '7'  and l.sub_status_id = '2' and l.num_attempt = '0' and "+srhLeadDateConditions+" group by cluster_name)reached "+
        "ON t.cluster_id = reached.cluster_id Left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, "+
        "count(l.id) as totalReachedFirst from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id "+
        "left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '2'  and l.source_id <> '7'  and l.sub_status_id = '2' and l.num_attempt = '1' and "+
        srhLeadDateConditions+" group by cluster_name)reachedFirst ON t.cluster_id = reachedFirst.cluster_id Left join (select cl.id as cluster_id,(case when cl.name IS NULL "+
        "THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalReachedSec from ublms_leads l left join ublms_customer c on l.customer_id = c.id "+
        "left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '2'  and l.source_id <> '7'  and "+
        "l.sub_status_id = '2' and l.num_attempt = '2' and "+srhLeadDateConditions+" group by cluster_name)reachedSec ON t.cluster_id = reachedSec.cluster_id Left join "+
        "(select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalReachedThird from ublms_leads "+
        "l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id "+
        "where l.status_id = '2' and l.sub_status_id = '2'  and l.source_id <> '7'  and l.num_attempt = '3' and "+srhLeadDateConditions+" group by cluster_name)reachedThird "+
        "ON t.cluster_id = reachedThird.cluster_id Left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name,"+
        "count(l.id) as totalReachedFourth from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id "+
        "left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '2' and l.sub_status_id = '2' and l.source_id<>'7' and l.num_attempt = '4' and "+
        srhLeadDateConditions+" group by cluster_name)reachedFourth ON t.cluster_id = reachedFourth.cluster_id Left join (select cl.id as cluster_id,(case when cl.name IS NULL "+
        "THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalReachedMore from ublms_leads l left join ublms_customer c on l.customer_id = c.id "+
        "left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '2' and l.sub_status_id = '2'  and "+
        "l.source_id <> '7'  and l.num_attempt > '4' and "+srhLeadDateConditions+" group by cluster_name)reachedMore ON t.cluster_id = reachedMore.cluster_id Left join (select cl.id as cluster_id,"+
        "(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalVerifyInt from ublms_leads l left join ublms_customer c "+
        "on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '3' and "+
        "l.sub_status_id = '3'  and l.source_id <> '7'    and "+srhLeadDateConditions+" group by cluster_name)vi "+
        "ON t.cluster_id = vi.cluster_id left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as "+
        "totalVerifyFollowup from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster "+
        "cl ON cl.id = ct.cluster_id where l.status_id = '3' and l.sub_status_id = '5'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)vf "+
        "ON t.cluster_id = vf.cluster_id left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as "+
        "totalVerifyPostponed from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster "+
        "cl ON cl.id = ct.cluster_id where l.status_id = '3' and l.sub_status_id = '4' and "+srhLeadDateConditions+" group by cluster_name)vp "+
        "ON t.cluster_id = vp.cluster_id left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as "+
        "totalWalkinSchd from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl "+
        "ON cl.id = ct.cluster_id where l.status_id = '5' and l.sub_status_id = '9'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)ws "+
        "ON t.cluster_id = ws.cluster_id left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) "+
        "as totalPurchasePending from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join "+
        "ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '6' and l.sub_status_id = '10'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)pp "+
        "ON t.cluster_id = pp.cluster_id left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as "+
        "totalPurchaseFollowup from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join "+
        "ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '6' and l.sub_status_id = '11'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)pf "+
        "ON t.cluster_id = pf.cluster_id left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(ci.lead_id) "+
        "as totalPurchaseConverted from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster "+
        "cl ON cl.id = ct.cluster_id left join ublms_conversion_info ci on ci.lead_id=l.id where l.status_id = '6' and l.sub_status_id = '12'  and l.source_id <> '7' AND ci.status='1' "+
        "and "+srhLeadDateConditions+" group by cluster_name)pc ON t.cluster_id = pc.cluster_id left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) "+
        "as cluster_name, count(l.id) as totalClosedInt from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id "+
        "left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '7' and l.sub_status_id = '13'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)ci "+
        "ON t.cluster_id = ci.cluster_id left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalClosedWrong "+
        "from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id "+
        "where l.status_id = '7' and l.sub_status_id = '14'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)cwn ON t.cluster_id = cwn.cluster_id left join "+
        "(select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalClosedGaadiIndividual from ublms_leads l left join "+
        "ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '7' "+
        "and l.sub_status_id = '15'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)cgi ON t.cluster_id = cgi.cluster_id left join "+
        "(select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalClosedNonGaadiIndividual from ublms_leads l left join "+
        "ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '7' and "+
        "l.sub_status_id = '16'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)cngi ON t.cluster_id = cngi.cluster_id left join "+
        "(select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalClosedNonGaadiDealer from ublms_leads l left join "+
        "ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '7' "+
        "and l.sub_status_id = '17'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)cngd ON t.cluster_id = cngd.cluster_id left join (select cl.id as cluster_id, "+
        "(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalClosedPurchaseCar from ublms_leads l left join ublms_customer c "+
        "on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '7' and "+
        "l.sub_status_id = '"+PURCHASED_USED_CAR+"'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)cpuc ON t.cluster_id = cpuc.cluster_id left join "+
        "(select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalClosedAttemptElapsed from ublms_leads l left join "+
        "ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '7' "+
        "and l.sub_status_id = '18'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)cae ON t.cluster_id = cae.cluster_id left join (select cl.id as cluster_id,(case when cl.name "+
        "IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalPurchasedNewCar from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+
        ""+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '7' and l.sub_status_id = '20'  and l.source_id <> '7'  and "+
        srhLeadDateConditions+" group by cluster_name)cpnc ON t.cluster_id = cpnc.cluster_id left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) "+
        "as cluster_name, count(l.id) as totalClosedBlocked from ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join "+ 
        "ublms_cluster cl ON cl.id = ct.cluster_id where l.status_id = '7' and l.sub_status_id = '21'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)cb "+
        "ON t.cluster_id = cb.cluster_id left join (select cl.id as cluster_id,(case when cl.name IS NULL THEN 'Unassigned' ELSE cl.name END) as cluster_name, count(l.id) as totalClosedOutCity from "+
        "ublms_leads l left join ublms_customer c on l.customer_id = c.id left join "+UBLMS_CITY_CLUSTER+" ct ON ct.city_id = c.customer_city_id left join ublms_cluster cl ON cl.id = ct.cluster_id where "+
        "l.status_id = '7' and l.sub_status_id = '25'  and l.source_id <> '7'  and "+srhLeadDateConditions+" group by cluster_name)coc ON t.cluster_id = coc.cluster_id "+whereCond+" group by(t.cluster_name)";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await LeadSnapshotReportModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

LeadSnapshotReportModel.bindCondition = async (params,authUser) => {
    let cluster = (params['cluster'] && params['cluster'].length ) ? params['cluster']:'' ; 		
    let condition = '';
    if(authUser['allowed_city_ids'] && !cluster ) {
        // let userClusters = authUser['allowed_city_ids'];
        // condition = "where t.cluster_id IN ("+userClusters+")";                
    } else if(cluster){ 
    condition = "where t.cluster_id IN ("+(cluster).toString()+")";
    }
    return condition;
}

LeadSnapshotReportModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

LeadSnapshotReportModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

module.exports = LeadSnapshotReportModel;