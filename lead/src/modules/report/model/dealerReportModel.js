const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class DealerBoostModel extends Model{
}

DealerBoostModel.init({
    dealer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    ftd_lead_count:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    boost_status:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    boost_leads_count:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    end_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull: false,
        defaultValue:'0000-00-00 00:00:00'
    },
    priority:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:1
    },
    is_boost:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    },
    is_front_boost:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    },
    boost_active:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    },
    is_sent_leads:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'1'
    },
    target_ftd:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:2
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
        defaultValue:'0000-00-00 00:00:00'
    },
    updated_ftd_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false,
        defaultValue:'0000-00-00 00:00:00'
    },
    deboost_status: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_DEALER_BOOST,
    freezeTableName: true
});


DealerBoostModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await DealerBoostModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

DealerBoostModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await DealerBoostModel.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

DealerBoostModel.getDealerBoostInfo = async(dealer_id) =>{
    return new Promise((resolve, reject)=>{
        let sql = "select * from UBLMS_DEALER_BOOST where dealer_id='" +dealer_id + "'";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}




module.exports = DealerBoostModel;