const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const clusterModel = require("../../cluster/model/clusterModel");
const moment = require("moment");
const conf = require('./../../../../config/config')

class L1CallingModel extends Model{
}

L1CallingModel.get = async (_where, pagination,authUser,isL1Agent) => {
    let sql='';
    let agentClusters = '',srhConversationConditions = '',srhConversionConditions = '',
    srhwalkinConditions = '',duration = '',l1AgentFilterDate='';
    if(isL1Agent){
        duration = (_where['duration']) ? _where['duration'] : '';
        //l1AgentFilterDate = getFilterDate($duration);
        if(duration == 'Y'){
            srhwalkinConditions += " w.created_at>= '"+l1AgentFilterDate +" 00:00:00' AND w.created_at<= '"+l1AgentFilterDate+" 23:59:59'"; 
            srhConversionConditions += " ci.added_on >= '"+l1AgentFilterDate +"' AND ci.added_on <= '"+l1AgentFilterDate+"'"; 
            srhConversationConditions += " c.added_on >= '"+l1AgentFilterDate+"' AND c.added_on <= '"+l1AgentFilterDate+"' ";
        }else{
            l1AgentFilterDate = moment().format('YYYY-MM-DD');
            srhwalkinConditions += " w.created_at >= '"+l1AgentFilterDate+"'"; 
            srhConversionConditions += " ci.added_on >= '"+l1AgentFilterDate+"'"; 
            srhConversationConditions += " c.added_on >= '"+l1AgentFilterDate+"'";
        }
   }
    if(isL1Agent == false){
        //get the data to load on the view page::start
            let srh_addedDateFrom   = (_where['added_date_from']) ? moment(_where['added_date_from']).format('YYYY-MM-DD')+' 00:00:00' : '' ;
            let srh_addedDateTo     = (_where['added_date_to'])? moment(_where['added_date_to']).format('YYYY-MM-DD')+' 23:59:59' : '' ;    
    
            if(srh_addedDateFrom && srh_addedDateTo){  
               srhwalkinConditions += " w.created_at >= '"+srh_addedDateFrom+"' and w.created_at <= '"+srh_addedDateTo+"'"; 
               srhConversionConditions += " ci.added_on >= '"+srh_addedDateFrom+"' and ci.added_on <= '"+srh_addedDateTo+"'"; 
               srhConversationConditions += " c.added_on >= '"+srh_addedDateFrom+"' and c.added_on <= '"+srh_addedDateTo+"'";                   
            } else if(srh_addedDateFrom){ 
               srhwalkinConditions += " w.created_at >= '"+srh_addedDateFrom+"'";
               srhConversionConditions += " ci.added_on >= '"+srh_addedDateFrom+"'";
               srhConversationConditions += " c.added_on >= '"+srh_addedDateFrom +"'";                   
            } else if(srh_addedDateTo){  
               srhwalkinConditions += " w.created_at <= '"+srh_addedDateTo+"'";
               srhConversionConditions += " ci.added_on <= '"+srh_addedDateTo+"'";
               srhConversationConditions += " c.added_on <= '"+srh_addedDateTo+"'";                   
            } else if(srh_addedDateFrom == '' && srh_addedDateTo == ''){  
                let defaultToDueDate = moment().format('YYYY-MM-DD')+' 00:00:00';
                let defaultFromDueDate = moment().format('YYYY-MM-DD')+' 23:59:59';
               srhwalkinConditions += " w.created_at >= '"+defaultToDueDate+"' and w.created_at <= '"+defaultFromDueDate+"'";
               srhConversionConditions += " ci.added_on >= '"+defaultToDueDate+"' and ci.added_on <= '"+defaultFromDueDate+"'";
               srhConversationConditions += " c.added_on >= '"+defaultToDueDate+"' and c.added_on <= '"+defaultFromDueDate+"'";
           }     
       }
       
    let extraWhereCond = await L1CallingModel.bindCondition(_where, pagination, true, authUser);
    return new Promise(function (resolve, reject) {
        if(isL1Agent){
            let sql_fields = " SQL_CALC_FOUND_ROWS u.id, uc.totalLead, ucont.totalContactedLead, uv.totalVerifiedLead, cw.totalWalkinSchd, wc.totalWalkinDone, c.totalConversionAssist,wk.totalWalkingAssist, uclose.totalClose, ls.leadSentToDealer ";
            sql = "SELECT "+sql_fields+" FROM "+UBLMS_USER+" u left join "
            + "(SELECT c.added_by, count(distinct lead_id) as totalLead FROM "+UBLMS_CONVERSION+" c where c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) uc "
            + "ON u.id = uc.added_by left join "
            + "(SELECT c.added_by, count(distinct lead_id) as totalContactedLead FROM "+UBLMS_CONVERSION+" c where c.status_id IN(2,3,5,6,7) and c.sub_status_id NOT IN(18,19) and c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) ucont  "
            + "ON u.id = ucont.added_by left join "
            + "(SELECT c.added_by, count(distinct lead_id) as totalVerifiedLead FROM "+UBLMS_CONVERSION+" c where c.status_id IN(3,5,6) and c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) uv  "
            + "ON u.id = uv.added_by left join "
            + "(SELECT c.added_by, count(distinct lead_id) as totalWalkinSchd FROM "+UBLMS_CONVERSION+" c where c.status_id = 5 and c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) cw "
            + "ON u.id = cw.added_by left join "
            + "(SELECT w.added_by, count(distinct lead_id) as totalWalkinDone FROM "+UBLMS_WALKING_INFO+" w where w.walkin_status = 4 and w.status = 1 and "+srhwalkinConditions+" group by w.added_by) wc "
            + "ON u.id = wc.added_by left join "
            + "(SELECT l.added_by, count(distinct l.id) as totalConversionAssist FROM "+UBLMS_LEADS_CARS+" l inner join "+UBLMS_CONVERSION_INFO+" ci on l.id = ci.leads_cars_id where ci.status = 1 and ci.cnfm_status = '1' and "+srhConversionConditions+" group by l.added_by) c "
            + "ON u.id = c.added_by left join "
            + "(SELECT l.added_by, count(distinct l.id) as totalWalkingAssist FROM "+UBLMS_LEADS_CARS+" l inner join "+UBLMS_WALKING_INFO+" w on l.id = w.leads_cars_id where w.status = 1 and w.walkin_status = 4 and "+srhwalkinConditions+" group by l.added_by) wk "
            + "ON u.id = wk.added_by left join "
            + "(SELECT c.added_by, count(distinct lead_id) as totalClose FROM "+UBLMS_CONVERSION+" c where c.status_id = 7 and c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) uclose "
            + "ON u.id = uclose.added_by left join ";
            if(duration == 'y'){
                sql += "(SELECT c.added_by, count(distinct c.id) as leadSentToDealer FROM "+UBLMS_LEADS_CARS+" c where c.car_id > 0 and date(c.created_at)='$l1AgentFilterDate' group by c.added_by) ls ON u.id = ls.added_by ";
            }else{
                sql += "(SELECT c.added_by, count(distinct c.id) as leadSentToDealer FROM "+UBLMS_LEADS_CARS+" c where c.car_id > 0 and c.created_at>='$l1AgentFilterDate' and is_dealer_sent=1 group by c.added_by) ls ON u.id = ls.added_by ";
            }
           }
           else{
            let sql_fields = " SQL_CALC_FOUND_ROWS u.id, u.name, u.allowed_city_ids,uc.totalLead, ucont.totalContactedLead, uv.totalVerifiedLead, cw.totalWalkinSchd, wc.totalWalkinDone, c.totalConversionAssist,wk.totalWalkingAssist, uclose.totalClose, vloc.totalLocVerifiedLead, vlang.totalLangVerifiedLead,ls.leadSentToDealer ";
            sql = "SELECT "+sql_fields+" FROM ublms_user u left join "
            + "(SELECT c.added_by, count(distinct lead_id) as totalLead FROM "+UBLMS_CONVERSION+" c where c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) uc "
            + "ON u.id = uc.added_by left join "
            + "(SELECT c.added_by, count(distinct lead_id) as totalContactedLead FROM "+UBLMS_CONVERSION+" c where c.status_id IN(2,3,5,6,7) and c.sub_status_id NOT IN(18,19) and c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) ucont  "
            + "ON u.id = ucont.added_by left join "
            + "(SELECT c.added_by, count(distinct lead_id) as totalVerifiedLead FROM "+UBLMS_CONVERSION+" c where c.status_id IN(3,5,6) and c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) uv  "
            + "ON u.id = uv.added_by left join "
            + "(SELECT c.added_by, count(distinct lead_id) as totalWalkinSchd FROM "+UBLMS_CONVERSION+" c where c.status_id = 5 and c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) cw "
            + "ON u.id = cw.added_by left join "
            + "(SELECT w.added_by, count(distinct lead_id) as totalWalkinDone FROM "+UBLMS_WALKING_INFO+" w where w.walkin_status = 4 and w.status = 1 and "+srhwalkinConditions+" group by w.added_by) wc "
            + "ON u.id = wc.added_by left join "
            + "(SELECT l.added_by, count(distinct l.id) as totalConversionAssist FROM "+UBLMS_LEADS_CARS+" l inner join "+UBLMS_CONVERSION_INFO+" ci on l.id = ci.leads_cars_id where ci.status = 1 and ci.cnfm_status = '1' and "+srhConversionConditions+" group by l.added_by) c "
            + "ON u.id = c.added_by left join "
            + "(SELECT l.added_by, count(distinct l.id) as totalWalkingAssist FROM "+UBLMS_LEADS_CARS+" l inner join "+UBLMS_WALKING_INFO+" w on l.id = w.leads_cars_id where w.status = 1 and w.walkin_status = 4 and "+srhwalkinConditions+" group by l.added_by) wk "
            + "ON u.id = wk.added_by left join "
            + "(SELECT c.added_by, count(distinct lead_id) as totalClose FROM "+UBLMS_CONVERSION+" c where c.status_id = 7 and c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) uclose "
            + "ON u.id = uclose.added_by left join "
            + "(SELECT c.added_by, count(distinct c.lead_id) as totalLocVerifiedLead FROM "+UBLMS_CONVERSION+" c inner join "+UBLMS_LEADS+" l ON c.lead_id = l.id inner join "+UBLMS_CUSTOMER+" cus ON l.customer_id = cus.id where cus.customer_location_id != '' and cus.customer_location_id IS NOT NULL and c.status_id IN(3,5,6) and c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) vloc  "
            + "ON u.id = vloc.added_by left join "
            + "(SELECT c.added_by, count(distinct c.lead_id) as totalLangVerifiedLead FROM "+UBLMS_CONVERSION+" c inner join "+UBLMS_LEADS+" l ON c.lead_id = l.id inner join "+UBLMS_CUSTOMER+" cus ON l.customer_id = cus.id where cus.language != '' and cus.language IS NOT NULL and c.status_id IN(3,5,6) and c.leads_cars_id = 0 and "+srhConversationConditions+" group by c.added_by) vlang  "
            + "ON u.id = vlang.added_by LEFT JOIN ";
            if(duration == 'y'){
                sql += "(SELECT c.added_by, count(distinct c.id) as leadSentToDealer FROM "+UBLMS_LEADS_CARS+" c where c.car_id > 0 and date(c.created_at)='"+l1AgentFilterDate+"' group by c.added_by) ls ON u.id = ls.added_by ";
            }else{
                sql += "(SELECT c.added_by, count(distinct c.id) as leadSentToDealer FROM "+UBLMS_LEADS_CARS+" c where c.car_id > 0 and c.created_at>='"+l1AgentFilterDate+"' and is_dealer_sent=1 group by c.added_by) ls ON u.id = ls.added_by ";
            }
            whereCond = "where 1=1 ";
            if(conf.SHOW_L1 == true){
                whereCond += " and u.role_id IN ('3', '17') ";
            }else{
                whereCond += " and u.role_id IN ('25')";
            }
                
           }
            if (whereCond) {
                sql +=whereCond;
            }
            sql += ' AND u.status=1';
            sql += "  ";
            sql += extraWhereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await L1CallingModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

L1CallingModel.bindCondition = async (params, pagination, isPaginationApply, authUser) => {
    let cluster = (params['cluster'] && params['cluster'].length ) ? params['cluster']:'' ;
    let condition = '';
    let clustersCity = '';
    let userRoleId = authUser['role_id'];
    let grpyBy = " group by u.id order by u.id desc ";

    let paginationCond = await L1CallingModel.bindPagination(params, pagination, isPaginationApply);

    if (cluster) {
        let clustersCityIds = await clusterModel.getClusterCity(cluster);
        if(clustersCityIds && clustersCityIds.length){
            clustersCity = clustersCityIds.map(city=>{return city.city_id});
            let customerCity = " u.allowed_city_ids IN (" + clustersCity + ")";
            condition = (condition == '')? condition + " and "+customerCity : condition+" and "+customerCity 
        }
        
    }
    return condition + " " + grpyBy + " " + paginationCond;
}

L1CallingModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

L1CallingModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

module.exports = L1CallingModel;