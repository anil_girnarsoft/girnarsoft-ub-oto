const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');

class DealerDeliveryReportEmails extends Model{

}

DealerDeliveryReportEmails.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    params: {
        type: dbConfig.Sequelize.TEXT,
        allowNull: false,
    },
    email: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false,
    },
    mail_sent: {
        type: dbConfig.Sequelize.TINYINT,
        allowNull: false,
        defaultValue: 0
    },
    status: {
        type: dbConfig.Sequelize.TINYINT,
        allowNull: false,
        defaultValue: 1
    },
    created_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_date: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_DEALER_DELIVERY_REPORT_EMAILS,
    freezeTableName: true
});

DealerDeliveryReportEmails.getDataToSendMail = async () => {
  return new Promise((resolve, reject)=>{
    try {
        let sql = `SELECT * FROM ${UBLMS_DEALER_DELIVERY_REPORT_EMAILS} WHERE mail_sent=0 ORDER BY id ASC LIMIT 1`;
        sequelize
        .query(sql, { raw: true, type: dbConfig.Sequelize.QueryTypes.SELECT,replacements:[] })
        .then(result => {
            resolve(result);
        }).catch((error)=> {
            reject(error);
        });
    } catch (error) {
        reject(error);
    }
})
}


DealerDeliveryReportEmails.get = async (_where, pagination) => {

    let isPaginationApply = (typeof _where.isPaginationApply !== 'undefined') ? _where.isPaginationApply : true;
    let whereCond = await DealerDeliveryReportEmails.bindCondition(_where, pagination, isPaginationApply)  //true is used to apply pagination limit

    return new Promise(function (resolve, reject) {
    

        var sql = `select SQL_CALC_FOUND_ROWS dre.id,dre.params,dre.email,dre.mail_sent,dre.status from  ${UBLMS_DEALER_DELIVERY_REPORT_EMAILS} dre ` + whereCond;
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(async (result) => {

                ////// map with car details for car price ////

                let totalCount = await DealerDeliveryReportEmails.getRowsCount();
                resolve([result, totalCount]);
            }).catch(function (error) {
                reject(error);
            });
    });
}


DealerDeliveryReportEmails.bindCondition = async (params, pagination, isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond = await DealerDeliveryReportEmails.bindPagination(params, pagination, isPaginationApply);
    let GROUPBY = 'GROUP BY dre.id';
    
    if (params  && typeof params.params !== 'undefined') {
        let filterParams = `dre.params ='${params.params}'`
        condition = (condition == '') ? condition + "where " + filterParams : condition + " and " + filterParams
    }

    if (params  && typeof params.email !== 'undefined') {
        let email = `dre.email ='${params.email}'`
        condition = (condition == '') ? condition + "where " + email : condition + " and " + email
    }

    if (params  && typeof params.mail_sent !== 'undefined') {
        let mailSent = `dre.mail_sent ='${params.mail_sent}'`
        condition = (condition == '') ? condition + "where " + mailSent : condition + " and " + mailSent
    }
    
    if (params && params.sort && params.sort.length) {
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort, (srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort + ', ' + sortData : sortData
        })
        sortCond = (finalSort != '') ? 'ORDER BY ' + finalSort : '';
    }
    return condition + " " + GROUPBY + " " + sortCond + " " + paginationCond;
}

DealerDeliveryReportEmails.bindPagination = (params, pagination, isPaginationApply) => {
    let condition = '';
    if (isPaginationApply) {
        let page_no = pagination;
        let limit = params.page_limit || PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT " + offset + "," + limit;
    }
    return condition;
}

DealerDeliveryReportEmails.getRowsCount = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result[0].total);
            }).catch(error => {
                reject(error);
            });
    })
}

DealerDeliveryReportEmails.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await DealerDeliveryReportEmails.update(data, { where: { id: id } });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

DealerDeliveryReportEmails.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const response = await DealerDeliveryReportEmails.create(data);
            resolve(response);
        } catch (error) {
            reject(error)
        }
    })
}
module.exports = DealerDeliveryReportEmails;