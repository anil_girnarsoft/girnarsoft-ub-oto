const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const _ = require("lodash");
const leadCarModel = require('../../lead/model/leadCarModel');
const clusterModel = require('../../cluster/model/clusterModel');
const InventoryService = require(COMMON_MODEL + 'inventoryService');
const conf = require('./../../../../config/config');
const { GoogleSpreadsheet } = require('google-spreadsheet');
const { promisify } = require("util");
const creds = require("./../../../../"+conf.REPORT_SHEET_CREDENTIAL);
const { reject } = require("lodash");
const { async } = require("q");
const LeadCarModel = require("../../lead/model/leadCarModel");

exports.getLeadReport = async (req, res, next) => {
  try {
    let resp = await InventoryService.getStateCityList(req);
    let result = (resp && resp.data) ? resp.data : [];
    let allCities = result.city;
  
    let allowedCities = allCities.filter(city => conf.CITY_FOR_REPORT.indexOf(city.name) !== -1);

    //console.log('allowed cities', allowedCities);
    await createHeader({allowedCities:allowedCities});

    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();
    let current_date = yyyy + '-' + mm + '-' + dd;
    let first_month_date = yyyy + '-' + mm + '-01';

    //console.log('Total leads (FTD)**********');
    let ftd_city_leads =  await leadCarModel.getCityWiseTotalLeads(current_date, current_date);
    let dataForReport = await restructureData(ftd_city_leads, allowedCities, 'Total leads (FTD)');
    await createReport({data: dataForReport});  
    
    //Paid Dealer leads(total) FTD
    //console.log('Paid Dealer leads(total) FTD**********');
    let ftd_paid_dealer_leads =  await leadCarModel.getCityWiseDealerLead(current_date, current_date, 'paid');
    let paidLeadReport = await restructureData(ftd_paid_dealer_leads, allowedCities, 'Paid Dealer leads(total) FTD');
    await createReport({data: paidLeadReport});      

    //Free Dealer leads(total) FTD
    //console.log('Free Dealer leads(total) FTD**********');
    let ftd_free_dealer_leads =  await leadCarModel.getCityWiseDealerLead(current_date, current_date, 'free');
    let freeLeadReport = await restructureData(ftd_free_dealer_leads, allowedCities, 'Free Dealer leads(total) FTD');
    await createReport({data: freeLeadReport});      

    //Ind. Leads(total) FTD
    //console.log('Ind. Leads(total) FTD**********');
    let ind_leads =  await leadCarModel.getCityWiseTotalLeads(current_date, current_date, 'I');

    let indLeadReport = await restructureData(ind_leads, allowedCities, 'Ind. Leads(total) FTD');
    await createReport({data: indLeadReport}); 

    //Unique leads (FTD)
    //console.log('Unique leads (FTD)**********');
    let ftd_unique_leads =  await leadCarModel.getUniquLeadsOfEachCity(current_date, current_date);
    let uniqueTotal =  await leadCarModel.getUniqueTotalLeadsReport(current_date, current_date);
    let to_day_UniqueLead = await restructureData(ftd_unique_leads, allowedCities, 'Unique leads (FTD)', (uniqueTotal.length)?uniqueTotal[0].total:0);
    await createReport({data: to_day_UniqueLead});

    //Unique Paid Dealer leads(total) FTD
    //console.log('Unique Paid Dealer leads(total) FTD**********');
    let ftd_unique_dealer_paid_leads =  await leadCarModel.getDealerUniqueLeadsOfEachCity(current_date, current_date, 'paid');
    let uniqueTotalP =  await leadCarModel.getUniqueTotalLeadsReport(current_date, current_date, 2);
    let to_day_unique_dpl = await restructureData(ftd_unique_dealer_paid_leads, allowedCities, 'Unique Paid Dealer leads(total) FTD', (uniqueTotalP.length)?uniqueTotalP[0].total: 0);
    await createReport({data: to_day_unique_dpl});

    //Unique Free Dealer leads(total) FTD 
    //console.log('Unique Free Dealer leads(total) FTD**********');
    let ftd_unique_dealer_leads =  await leadCarModel.getDealerUniqueLeadsOfEachCity(current_date, current_date, 'free');
    let uniqueTotalF =  await leadCarModel.getUniqueTotalLeadsReport(current_date, current_date, 1);
    let to_day_unique_dfl = await restructureData(ftd_unique_dealer_leads, allowedCities, 'Unique Free Dealer leads(total) FTD ', (uniqueTotalF.length)?uniqueTotalF[0].total: 0);
    await createReport({data: to_day_unique_dfl});

    //Unique Ind. Leads(total) FTD
    //console.log('Unique Ind. Leads(total) FTD**********');
    let ftd_unique_individual_leads =  await leadCarModel.getUniqueIndividualLeadsOfEachCity(current_date, current_date, 'I');
    let uniqueTotalI =  await leadCarModel.getUniqueTotalLeadsReport(current_date, current_date, -1);
    let to_day_unique_indl = await restructureData(ftd_unique_individual_leads, allowedCities, 'Unique Ind. Leads(total) FTD', (uniqueTotalI.length)?uniqueTotalI[0].total: 0);
    await createReport({data: to_day_unique_indl});

    //Total leads (MTD)
    //console.log('Total leads (MTD)**********');
    let mtd_total_leads =  await leadCarModel.getCityWiseTotalLeads(first_month_date, current_date);
    let mtd_tl = await restructureData(mtd_total_leads, allowedCities, 'Total leads (MTD)');
    await createReport({data: mtd_tl});

    //Paid Dealer leads(total) MTD
    //console.log('Paid Dealer leads(total) MTD**********');
    let mtd_paid_dealer_leads =  await leadCarModel.getCityWiseDealerLead(first_month_date, current_date, 'paid');
    let mtd_pdl = await restructureData(mtd_paid_dealer_leads, allowedCities, 'Paid Dealer leads(total) MTD');
    await createReport({data: mtd_pdl});

    //Free Dealer leads(total) MTD
    //console.log('Free Dealer leads(total) MTD**********');
    let mtd_free_dealer_leads = await leadCarModel.getCityWiseDealerLead(first_month_date, current_date, 'free');
    let mtd_fdl = await restructureData(mtd_free_dealer_leads, allowedCities, 'Free Dealer leads(total) MTD');
    await createReport({data: mtd_fdl});

    //Ind. Leads(total) MTD
    //console.log('Ind. Leads(total) MTD**********');
    let mtd_ind_leads = await leadCarModel.getCityWiseTotalLeads(first_month_date, current_date, 'I');
    let mtd_ind_dl = await restructureData(mtd_ind_leads, allowedCities, 'Ind. Leads(total) MTD');
    await createReport({data: mtd_ind_dl});

    //Unique leads (MTD)
    //console.log('Unique leads (MTD)**********');
    let mtd_unique_leads = await leadCarModel.getUniquLeadsOfEachCity(first_month_date, current_date);
    let uniqueTotalMT =  await leadCarModel.getUniqueTotalLeadsReport(first_month_date, current_date);
    let mtd_ul = await restructureData(mtd_unique_leads, allowedCities, 'Unique leads (MTD)', uniqueTotalMT[0].total);
    await createReport({data: mtd_ul});

    //Unique Paid Dealer leads(total) MTD
    //console.log('Unique Paid Dealer leads(total) MTD**********');
    let mtd_unique_dealer_paid_leads = await leadCarModel.getDealerUniqueLeadsOfEachCity(first_month_date, current_date, 'paid');
    let uniqueTotalPM =  await leadCarModel.getUniqueTotalLeadsReport(first_month_date, current_date, 2);
    let mtd_upl = await restructureData(mtd_unique_dealer_paid_leads, allowedCities, 'Unique Paid Dealer leads(total) MTD', uniqueTotalPM[0].total);
    await createReport({data: mtd_upl});

    //Unique Free Dealer leads(total) MTD
    //console.log('Unique Free Dealer leads(total) MTD**********');
    let mtd_unique_dealer_free_leads =  await leadCarModel.getDealerUniqueLeadsOfEachCity(first_month_date, current_date, 'free');
    let uniqueTotalFM =  await leadCarModel.getUniqueTotalLeadsReport(first_month_date, current_date, 1);
    let mtd_udfl = await restructureData(mtd_unique_dealer_free_leads, allowedCities, 'Unique Free Dealer leads(total) MTD', uniqueTotalFM[0].total);
    await createReport({data: mtd_udfl});

    //Unique Ind. Leads(total) MTD
    //console.log('Unique Ind. Leads(total) MTD**********');
    let mtd_unique_individual_leads = await leadCarModel.getUniqueIndividualLeadsOfEachCity(first_month_date, current_date, 'I');
    let uniqueTotalIM =  await leadCarModel.getUniqueTotalLeadsReport(first_month_date, current_date, -1);
    let mtd_uil = await restructureData(mtd_unique_individual_leads, allowedCities, 'Unique Ind. Leads(total) MTD', uniqueTotalIM[0].total);
    await createReport({data: mtd_uil});

    //Total Active Inventory
   
    let total_active_inventory =  await leadCarModel.getInventory({car_status: '1', is_classified: '1'});
    let totalAI = total_active_inventory[0].total;
    let cityWiseAI = await leadCarModel.getCityWiseInventory({car_status: '1', is_classified: '1'});
    let active_inventory = await restructureData(cityWiseAI, allowedCities, 'Active Inventory', totalAI);
    await createReport({data: active_inventory});
    //Paid Inventory
    let paid_inventory =  await leadCarModel.getInventory({car_status: '1', is_classified: '1', priority:2});
    let total_PI = paid_inventory[0].total;
    let cityWisePI = await leadCarModel.getCityWiseInventory({car_status: '1', is_classified: '1', priority:2});
    let paid_inventory_d = await restructureData(cityWisePI, allowedCities, 'Paid Inventory', total_PI);
    await createReport({data: paid_inventory_d});
    //console.log('paid_inventory-----------------', paid_inventory);
    //await createReport({paidInventory: paid_inventory[0].total});
    //Free Inventory
    let free_inventory = await leadCarModel.getInventory({car_status: '1', is_classified: '1', priority:1});
    let total_FI = free_inventory[0].total;
    let cityWiseFI = await leadCarModel.getCityWiseInventory({car_status: '1', is_classified: '1', priority:1});
    let free_inventory_d = await restructureData(cityWiseFI, allowedCities, 'Free Inventory', total_FI);
    await createReport({data: free_inventory_d});
    //await createReport({freeInventory: free_inventory[0].total});
    //Individual Inventory
    let ind_inventory = await leadCarModel.getInventory({car_status: '1', is_classified: '1', type:'I'});
    let ind_I = ind_inventory[0].total;
    let indIC = await leadCarModel.getCityWiseInventory({car_status: '1', is_classified: '1', type:'I'});
    let individual_inventory = await restructureData(indIC, allowedCities, 'Individual Inventory', ind_I);
    await createReport({data: individual_inventory});

    ApiController.sendPaginationResponse(req, res, '', '');
  } catch (error) {
    // console.log(' Leads new report errors: ', error);
  }
}

const createHeader = async (params) => {
  return new Promise(async (resolve, reject) => {
    const doc = new GoogleSpreadsheet(conf.LEAD_REPORT_ID);
    await doc.useServiceAccountAuth(creds);
    await doc.loadInfo();
    await doc.updateProperties({ title: conf.LEAD_REPORT_TITLE });
    const sheet = doc.sheetsByIndex[0];
    await sheet.clear();

    if (params.allowedCities) {
      let allowedCities = params.allowedCities;
      let cities = ['Column'];
      cities.push('Total');
      if(process.env.COUNTRY_CODE.toLowerCase() == 'ph'){
        cities.push('NCR');
      }
      if(process.env.COUNTRY_CODE.toLowerCase() == 'id'){
        cities.push('Jabodetabek');
      }
      for (i of allowedCities) {
        cities.push(i.name);
      }
      cities.push('Others');
      await sheet.setHeaderRow(cities);
    }
    resolve(1);
  }).catch((err)=>{
    reject(err);
  });
}

const createReport = async(params)=>{
  try {
    const doc = new GoogleSpreadsheet(conf.LEAD_REPORT_ID);
    await doc.useServiceAccountAuth(creds);
    await doc.loadInfo();
    const sheet = doc.sheetsByIndex[0];
    let formedData = [];    
    if (params.data) {       
      let aa = {};
      for(i of params.data) {        
        aa[i.name]=i.total_lds;
        if(i.name == 'Column') aa[i.name]= i.heading;    
      }
      formedData.push(aa);      
    }
    /*
    if(params.activeInventory){
      formedData.push({Column: 'Total Active Inventory', Total: params.activeInventory});
    }
    if(params.paidInventory){
      formedData.push({Column: 'Paid Inventory', Total: params.paidInventory});
    }
    if(params.freeInventory){
      formedData.push({Column: 'Free Inventory', Total: params.freeInventory});
    }
    if(params.indInventory){
      formedData.push({Column: 'Individual Inventory', Total: params.indInventory});
    }
    */
    await sheet.addRows(formedData);

  } catch (error) {
    // console.log('Error:----', error);
  }
}

const restructureData = async (reportData, allowCities, columnHead, totalR=null) => {
  try {
    //console.log('allowCities-------------------------------', allowCities);
    
    return new Promise((resolve, reject) => {
      var res = [];
      let total = 0;
      let others = 0;
      let allowedCitiesTotal = 0;
      let Organisedresults = [];
      //let int_notMatched = [];
      if (reportData.length > 0) {        
        let results = reportData.filter(({ city_id: id1 }) => allowCities.some(({ id: id2 }) => id2 === id1));
        let notMatched = reportData.filter(({ city_id: id1 }) => !allowCities.some(({ id: id2 }) => id2 === id1));
        for (i of results) {
          for (j of allowCities) {
            if (i.city_id == j.id) {
              i.name = j.name;
              total += i.total_lds;
            }
          }
        }
        if(process.env.COUNTRY_CODE.toLowerCase() == 'ph'){
          allowedCitiesTotal =  total; 
        }
        if(process.env.COUNTRY_CODE.toLowerCase() == 'id'){
          let clusterCities = ['Jakarta Barat', 'Jakarta Pusat', 'Jakarta Selatan', 'Jakarta Timur', 'Jakarta Utara', 'Bogor', 'Depok', 'Tangerang', 'Bekasi'];   
          let clusterCityWithId = allowCities.filter(city => clusterCities.indexOf(city.name) !== -1); 
          let resultsD = reportData.filter(({ city_id: id1 }) => clusterCityWithId.some(({ id: id2 }) => id2 === id1));
          for (i of resultsD) {
            allowedCitiesTotal += i.total_lds
          }
        }
             
        for (k of notMatched) {
          others += k.total_lds;
        }
        total += others;  
        Organisedresults =  results  
      }
      
      if (totalR) {
        total = totalR;
      }
      Organisedresults.push({ name: 'Others', total_lds: others });
      Organisedresults.push({ name: 'Column', heading: columnHead });
      Organisedresults.push({ name: 'Total', total_lds: total });
      if(process.env.COUNTRY_CODE.toLowerCase() == 'ph'){
        Organisedresults.push({ name: 'NCR', total_lds: allowedCitiesTotal });
      }
      if(process.env.COUNTRY_CODE.toLowerCase() == 'id'){
        Organisedresults.push({ name: 'Jabodetabek', total_lds: allowedCitiesTotal });
      }
      //res = results;
     // console.log('(************************restructureData-------------', Organisedresults);
      resolve(Organisedresults);
    });
  } catch (error) {
    console.log('error in restructureData', error);
  }
}