const _ = require("lodash");
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const DealerService    = require(COMMON_MODEL + 'dealerService');
// const csv = require('csv-parser');
//const { DEALER_LIST_API } = require("../../../../config/config");

exports.listDealers = async (req, res, next) => {
  try{
    let params = req.body;
    let dealerList = await ApiController.get(DEALER_LIST_API, (params.dealer || null));

    if(params['cb_function']){ 
      return dealerList['data'] || [];
    }else{
      ApiController.sendResponse(req, res, 200, res.__(''), dealerList['data'] && dealerList['data']['data'] || [], '');
    }
  }catch(error){
    next(error);
  }
}

exports.getDealerDeliveryReport = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body;
    let result = await DealerService.getDealerList(req,params);
    let dealersList = result['data'];

    ApiController.sendResponse(req, res, 200, res.__(''), result && result['data'] || [], '');
    
  }catch (error) {
    next(error);
  }
}