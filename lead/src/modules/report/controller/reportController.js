const _ = require("lodash");
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const salesFunnelReportModel = require('../model/salesFunnelReportModel');
const leadSnapshotReportModel = require('../model/leadSnapshotModel');
const locationWiseLeadCountModel = require('../model/locationWiseLeadCountModel');
const l1CallingModel = require('../model/l1CallingModel');
const ReportSearchHistory = require('../model/reportSearchHistory');
const InventoryService = require(COMMON_MODEL + 'inventoryService');
const DealerService = require(COMMON_MODEL + 'dealerService');
const dateFormat = require('dateformat');
const clusterModel = require('../../cluster/model/clusterModel');
const dealerBoostModel = require('../../dealer/model/dealerBoostModel');
const leadCarModel = require('../../lead/model/leadCarModel');
const walkinInfoModel = require('../../walkin/model/walkinInfoModel');
const conversionInfoModel = require('../../conversion/model/conversionInfoModel');
const cityController = require('../../city/controller/cityController');
const { param } = require("express-validator");
var async = require("async");
const DealerDeliveryReportEmailsModel = require('../model/dealerDeliveryReportEmails');

exports.listDealers = async (req, res, next) => {
  try{
    let params = req.body;
    let dealerList = await ApiController.get(DEALER_LIST_API, (params.dealer || null));

    if(params['cb_function']){ 
      return dealerList['data'] || [];
    }else{
      ApiController.sendResponse(req, res, 200, res.__(''), dealerList['data'] && dealerList['data']['data'] || [], '');
    }
  }catch(error){
    next(error);
  }
}

//FIND USED CARS DATA

exports.findUsedCars = async (req, params) => {
  try {
    let usedCarsParams = {};
    usedCarsParams['page_no'] = params.page;
    if(params.dealerId){
      usedCarsParams['dealer_ids'] = params.dealerId;
    }
    
    if (params['stock_used_car_id_arr']) {
      usedCarsParams['stock_used_car_id_arr'] = params.stock_used_car_id_arr;
    }
    usedCarsParams['call_server'] = 'ub';
    usedCarsParams['car_status'] = [1,2];
    //TRUE FOR ALL EXCEPT FROM LEAD CONTROLLED getAssignedCarDetails FUNCTION
    if(!params.classified){
      usedCarsParams['classified'] = '1';
    }
    
    var usedCarIds = [];
    var allUsedCars = [];
    let next_page = true;
    let total = 0;
    
    while (next_page) {      
      let findAllUsedCars = await InventoryService.getStockList(req, usedCarsParams);
      let  pagination = findAllUsedCars.pagination || {};      
      next_page = pagination && pagination.next_page;      
      if (next_page) {
        usedCarsParams.page_no = usedCarsParams.page_no + 1;
        //console.log('usedCarsParams-----------------------------', usedCarsParams);
      }
      if (findAllUsedCars && findAllUsedCars.status == 200) {
        findAllUsedCars['data'].forEach((usedCar, key) => {
          usedCarIds.push(usedCar.id);
          allUsedCars.push(usedCar);
        });
        total = pagination.total;
      }

    }
    return { 'usedCarIds': usedCarIds, allUsedCars: allUsedCars, 'totalCars': total};

  } catch (error) {
    // console.log('error ', error);
  }
}

exports.getDealerDeliveryReport = async (req, res, next) => {
  
    let page_number;
    let params = req.body;
    const authUser = _.get(req, 'authUser') || _.get(req, 'user');
    let totalLimit = params['total_limit'] || 10;     

    params['cityId'] = params['city'];
    let srhData = params;
    let dealerStatus       = (srhData['status'])? srhData['status']:'1';
    let srhAddedDateForm   = (srhData['created_from_date'] )? (srhData['created_from_date']).split('T')[0]:'' ;
    let srhAddedDateTo     = (srhData['created_to_date'] )? (srhData['created_to_date']).split('T')[0]:'' ;    
    let quickFilter        = (srhData['quickfilter'])? srhData['quickfilter']:'' ;		
    let srhDealer          = (srhData['dealer'] )? [srhData['dealer']]:[] ;
    //let dealerType       = (srhData['dealertype'] )? srhData['dealertype']:'' ;
    let cluster            = (srhData['cluster'] )? srhData['cluster']:'' ;
    let dealerPriority     = (srhData['priority'] )? srhData['priority']:'' ;            
      if(typeof(srhData['status']) == "object"){
        srhData['status'] = -1;
      }
      // console.log('srhData["statu"]*********************', srhData['status']);
    // if( (quickFilter) && (quickFilter) && quickFilter=='FTD'){
    //     srhAddedDateForm  = dateFormat('yyyy-mm-dd'); 
    //     srhAddedDateTo  = dateFormat('yyyy-mm-dd HH:MM:ss');           
    // }		
    // if( (quickFilter) &&  (quickFilter) && quickFilter=='ConvMTD'){
    //    srhAddedDateForm  = dateFormat('yyyy-mm')+'-01'; // 1st of the mnth
    //    srhAddedDateTo    = dateFormat('yyyy-mm-dd HH:MM:ss');  // Todaty adte.
    // }		
    // if( (quickFilter) &&  (quickFilter) && quickFilter=='ZeroBckendLeads5days'){
    //     srhAddedDateForm  = dateFormat('yyyy-mm')+"-"+(new Date().getDate()-5); 
    //     srhAddedDateTo  = dateFormat('yyyy-mm-dd HH:MM:ss');  
    // }		
    // if( (quickFilter) &&  (quickFilter) && quickFilter=='Conv_mtd_GREATET50'){	
    //    srhAddedDateForm  = dateFormat('yyyy-mm')+'-01'; // 1st of the mnth
    //    srhAddedDateTo    = dateFormat('yyyy-mm-dd HH:MM:ss');  // Todaty adte.
    // }            

             
    let srhwalkinConditions = '';
    let srhConversionConditions = '';
    let srhLeadCarConditions = '';
    let srhLeadConditions = '';
    srhAddedDateForm = (srhAddedDateForm)?srhAddedDateForm+' 00:00:00':'';
    srhAddedDateTo = (srhAddedDateTo)?srhAddedDateTo+' 23:59:59':'';

    if(srhAddedDateForm && srhAddedDateTo){  
        srhwalkinConditions += " and w.created_at >= '"+srhAddedDateForm+"' and w.created_at <= '"+srhAddedDateTo+"'"; 
        srhConversionConditions += " and l.updated_at >= '"+srhAddedDateForm+"' and l.updated_at <= '"+srhAddedDateTo+"'"; 
        srhLeadCarConditions += " and l.created_at >= '"+srhAddedDateForm+"' and l.created_at <= '"+srhAddedDateTo+"'";
        srhLeadConditions += " and ubl.created_at >= '"+srhAddedDateForm+"' and ubl.created_at <= '"+srhAddedDateTo+"'";
      } else if(srhAddedDateForm){  
            srhwalkinConditions += " and w.created_at >= '"+srhAddedDateForm+"'";
            srhConversionConditions += " and l.updated_at >= '"+srhAddedDateForm+"'";
            srhLeadCarConditions += " and l.created_at >= '"+srhAddedDateForm+"'";
            srhLeadConditions += " and ubl.created_at >= '"+srhAddedDateForm+"'";
      } else if(srhAddedDateTo){  
            srhwalkinConditions += " and w.created_at <= '"+srhAddedDateTo+"'";
            srhConversionConditions += " and l.updated_at <= '"+srhAddedDateTo+"'";
            srhLeadCarConditions += " and l.created_at <= '"+srhAddedDateTo+"'";
            srhLeadConditions += " and ubl.created_at <= '"+srhAddedDateTo+"'";
      } else if((params['customSearch'] == '')){  
        let defaultToDueDate = dateTimeFormat(new Date().toISOString().split('T')[0])+' 23:59:59';
        let defaultFromDueDate = dateTimeFormat(defaultToDueDate)+' 00:00:00';
        srhwalkinConditions += " and w.created_at >= '"+defaultToDueDate+"' and w.created_at <= '"+defaultFromDueDate+"'";
        srhConversionConditions += " and l.updated_at >= '"+defaultToDueDate+"' and l.updated_at <= '"+defaultFromDueDate+"'";
        srhLeadCarConditions += " and l.created_at >= '"+defaultToDueDate+"' and l.created_at <= '"+defaultFromDueDate+"'";
        srhLeadConditions += " and ubl.created_at >= '"+defaultToDueDate+"' and ubl.created_at <= '"+defaultFromDueDate+"'";
      } 


    let onlyPremiumCars = '';
    let leadsJoin ='';
    let leadsCondition = '';
    //FOR PREMIUM CHECKED
    if((params['premium_only'])){
        onlyPremiumCars = ' and uc.priceto >=1500000 ';
        leadsJoin = " inner join "+UBLMS_LEADS+" uld on l.lead_id=uld.id ";
        leadsCondition = " AND uld.is_premium='1' ";
    }


    //FIND ALL CITY/STATES 
    // req.body['cb_function'] = true;
    // let cityList = await cityController.getCityStateList(req, res, next);
    //  delete req.body['cb_function'];

    //FIND ALL DEALERS
    let findAllDealersTotalCount = 0;
    let findAllDealersTotal = [];
    let findAllDealersIdsArray = [];
    // console.log(params);
            //IF NO DEALER SELECTED
    let filters = {};
   // console.log(srhData['status']); return false;
   req['query']['filter'] = {};
  if (params['dealer']) {
    findAllDealersIdsArray = [params['dealer']];
    findAllDealersTotalCount = 1;
  } else {
    if (params.cityId && params.cityId.length) {
      //filters.city_ids = params.cityId;
      req['query']['filter']['city_ids'] = params.cityId;
    }
    if (srhData['status'] != -1 && srhData['status'] != '') {
      //filters.status = [(srhData['status']==0)?2:srhData['status']];
      req['query']['filter']['status'] = [(srhData['status'] == 0) ? 2 : srhData['status']];
    }
    // if (srhAddedDateForm) {
    //   req['query']['filter']['from_date'] = new Date(srhAddedDateForm).toISOString();
    // }
    // if (srhAddedDateTo) {
    //   req['query']['filter']['to_date'] = new Date(srhAddedDateTo).toISOString();
    // }
    // req['query']['filter']['status'] = [1];
    if(typeof params.status !== 'undefined' && params.status != '-1'){
      let statusDealer = (params.status == 0) ? 2 : 1; //2 InActive 1 Active
      req['query']['filter']['status'] = [statusDealer];
    }
    // findAllDealersTotalCount = await DealerService.getDealerList(req); //FIND ALL FOR TOTAL LENGTH
    // findAllDealersTotalCount = findAllDealersTotalCount['data'] ? findAllDealersTotalCount['data'].length : []

    //FIND BY 10 LIMIT
    if(totalLimit>0){
      req['query']['page_no'] = params['page_number'] || 1;
      req['query']['record_per_page'] = totalLimit;
    }
    let findAllDealersLimit = await DealerService.getDealerList(req);
    findAllDealersTotalCount = (findAllDealersLimit['pagination'] && findAllDealersLimit['pagination']['total']) ? findAllDealersLimit['pagination']['total'] : findAllDealersLimit.length;
    findAllDealersLimit = findAllDealersLimit['data'] ? findAllDealersLimit['data'] : [];
    //DEALER IDS
    findAllDealersIdsArray = findAllDealersLimit.map(el => el.id);

  }
    //GET DEALERS FROM DEALER IDS
    findAllDealersTotal = await DealerService.getDealerDetailsByIds(req, {dealer_id: findAllDealersIdsArray});
    let findAllDealersArray = (findAllDealersTotal['data'] && findAllDealersTotal['data'].length) ? findAllDealersTotal['data'] : []; 
    findAllDealersArray.sort((a,b)=>{
       return b.paid_score - a.paid_score;
    })
                           
            //FIND DEALER BY TYPE
            // if((dealerType) && dealerType.includes('pdp_dealers') && (dealerType.length)=='1'){
            //   findAllDealersArray  = findAllDealersArray.filter((el, key)=>{ if(el.priority_dealer == '1')  return el ; });
            // }

            //FIND DEALER BY TYPE
            if(dealerPriority){
              findAllDealersArray  = findAllDealersArray.filter((el)=>{ if(el.paid_score == dealerPriority)  return el ; });
            }           

             if(findAllDealersArray.length){

              let clusterData = [];
              req.body['cb_function'] = true;              
              clusterData = await clusterModel.getCityListAndCluster(params);

              
              //LOOP THROUGH DEALER DATA AND FETCH OTHER RELATED DATA
              // console.log('----------------findAllDealersArray----------------------', findAllDealersArray, findAllDealersArray.length);
              async.forEach((findAllDealersArray), async (el, callback) => { 
                
              let counter = _.findIndex(findAllDealersArray, el);//FIND INDEX

              findAllDealersArray[counter]['status'] = el['dealer_status'] || 1;

              //FIND DEALERS BASED ON CLUSTER CITY
              if(clusterData.length){
                clusterData.forEach(clust=>{
                    if(clust.city_ids.indexOf(el['city_id']) != -1){
                      findAllDealersArray[counter]['cluster_name'] = clust.name;                          
                      findAllDealersArray[counter]['city_data'] = {id: el['city_id'], name: el['city_name']};
                    }
                });
              }                  
                  //DEALER BOOST DATA
                // let listDealerBoost = await dealerBoostModel.getDealerBoostInfo({dealer_id:el.id});
                // findAllDealersArray[counter]['dealer_boost'] = listDealerBoost;
                let usedCarData = '';
                //FIND DEALER USED CARS
                try {
                  usedCarData = await this.findUsedCars(req, {page:1,dealerId:[el.id]});
                } catch (err) {
                  // console.log('erroorrrrrrrrrrrrrrrrrrr', err)
                }

                findAllDealersArray[counter]['total_car'] = (usedCarData && usedCarData['totalCars']) ? usedCarData['totalCars'] : 0;

                let usedCarJoin = "";
                if(usedCarData && usedCarData['usedCarIds'].length){
                  usedCarJoin = " and l.car_id IN ('"+(usedCarData['usedCarIds']).join("','")+"') ";
                }

                // let leadCars = await leadCarModel.getUniqueLeadsCars({dealerIds: [el.id]}, srhLeadCarConditions, leadsCondition,leadsJoin ,usedCarJoin);
                
                // findAllDealersArray[counter]['sumDistLead'] = (leadCars && leadCars[0]) ? leadCars[0]['sumDistLead'] : 0;
                //console.log('----------------------getUniqueTotalLeads*********');
                //let totalLeadCars = await leadCarModel.getUniqueTotalLeads({dealerIds: [el.id]}, srhLeadCarConditions, leadsCondition, leadsJoin,usedCarJoin);
                //findAllDealersArray[counter]['totalLead'] = (totalLeadCars && totalLeadCars[0]) ? totalLeadCars[0]['totalLead'] : 0;

                // let sumOrganicGreater = await leadCarModel.getSumOrgaicGreater({dealerIds: [el.id]}, srhLeadCarConditions, leadsCondition, leadsJoin,usedCarJoin);
                
                // findAllDealersArray[counter]['sumOrganicGreater100'] = (sumOrganicGreater && sumOrganicGreater[0]) ? sumOrganicGreater[0]['sumOrganicGreater100'] : 0;

                let sumOrganic = await leadCarModel.getSumOrgaic({dealerIds: [el.id]}, srhLeadCarConditions, leadsCondition, leadsJoin,usedCarJoin);
                findAllDealersArray[counter]['sumOrganic'] = (sumOrganic && sumOrganic[0]) ? sumOrganic[0]['sumOrganic'] : 0;

                let sumBackend = await leadCarModel.sumBackend({dealerIds: [el.id]}, srhLeadCarConditions, leadsCondition, leadsJoin,usedCarJoin);
                findAllDealersArray[counter]['sumBackend'] = (sumBackend && sumBackend[0]) ? sumBackend[0]['sumBackend'] : 0;
                findAllDealersArray[counter]['totalLead'] =  findAllDealersArray[counter]['sumBackend']+ findAllDealersArray[counter]['sumOrganic'];

                if((params['reportType']) && params['reportType'] == '2') {

                    let sumWalkinSchedule = await walkinInfoModel.getSumWalkinSchedule({dealerIds: [el.id]}, srhLeadConditions, leadsCondition,leadsJoin,usedCarJoin);
                    findAllDealersArray[counter]['sumWalkinSchedule'] = (sumWalkinSchedule && sumWalkinSchedule[0]) ? sumWalkinSchedule[0]['sumWalkinSchedule'] : 0;

                    let sumWalkinDone = await walkinInfoModel.getSumWalkinDone({dealerIds: [el.id]}, srhLeadConditions, leadsCondition,leadsJoin,usedCarJoin);
                    findAllDealersArray[counter]['sumWalkinDone'] = (sumWalkinDone && sumWalkinDone[0]) ? sumWalkinDone[0]['sumWalkinDone'] : 0;


                }else{
                      let sumWalkinSchedule = await walkinInfoModel.getSumWalkinSchedule({dealerIds: [el.id]}, srhwalkinConditions, leadsCondition,leadsJoin,usedCarJoin);
                      findAllDealersArray[counter]['sumWalkinSchedule'] = (sumWalkinSchedule && sumWalkinSchedule[0]) ? sumWalkinSchedule[0]['sumWalkinSchedule'] : 0;

                      let sumWalkinDone = await walkinInfoModel.getSumWalkinDone({dealerIds: [el.id]}, srhwalkinConditions, leadsCondition,leadsJoin,usedCarJoin);
                      findAllDealersArray[counter]['sumWalkinDone'] = (sumWalkinDone && sumWalkinDone[0]) ? sumWalkinDone[0]['sumWalkinDone'] : 0;

                      let sumConversion = await conversionInfoModel.getUniqueConversion({dealerIds: [el.id]}, srhConversionConditions, leadsCondition,leadsJoin,usedCarJoin);
                      findAllDealersArray[counter]['sumConversion'] = (sumConversion && sumConversion[0]) ? sumConversion[0]['sumConversion'] : 0;
                }               

                let sumBooked = await leadCarModel.getSumBooked({dealerIds: [el.id]}, srhLeadCarConditions, leadsCondition,leadsJoin,usedCarData['usedCarIds']);
                findAllDealersArray[counter]['sumBooked'] = (sumBooked && sumBooked[0]) ? sumBooked[0]['sumBooked'] : 0;

                // tell async that that particular element of the iterator is done
                 callback(); 
            
               }, function(err) {

                 let filterDealerArray = findAllDealersArray;
                // if( (quickFilter) &&  (quickFilter) && quickFilter=='FTD'){
                //      findAllDealersArray.forEach(el=> { if(!el.totalLead) filterDealerArray.push(el); });		
                // }		
                // if( (quickFilter) &&  (quickFilter) && quickFilter=='ConvMTD'){
                //    findAllDealersArray.forEach(el=> { if(!el.sumConversion) filterDealerArray.push(el); });		
	
                // }		
                // if( (quickFilter) &&  (quickFilter) && quickFilter=='ZeroBckendLeads5days'){
                //    findAllDealersArray.forEach(el=> { if(!el.sumBackend) filterDealerArray.push(el); });		

                // }		
                // if( (quickFilter) &&  (quickFilter) && quickFilter=='Conv_mtd_GREATET50'){	
                //     findAllDealersArray.forEach(el=> { if(!el.sumConversion && el.totalLead >=50) filterDealerArray.push(el); });		
                // }

              /*
                if(srhData['status'] != -1){
                  if(srhData['status'] == 0){
                    srhData['status'] = 2;// At DC status 2 for inactive
                  }
                  filterDealerArray  = findAllDealersArray.filter((el, key)=>{ if(el.dealer_status == srhData['status'])  return el ; });
                }
              */

            // if((params['cluster'] !== undefined) && (params['cluster'].length)){
            //   filterDealerArray  = findAllDealersArray.filter((el, key)=>{  if(el.city_data && el.city_data['id'])  return el ; });
            // }
            // let nextPage = ((findAllDealersTotalCount / totalLimit) > params['page_number']) ? true : false;
            // console.log('findAllDealersTotalCount', findAllDealersTotalCount)
            ApiController.sendPaginationResponse(req, res,  filterDealerArray, {total: findAllDealersTotalCount});
           
          });

        }else{
          ApiController.sendResponse(req, res, 200, res.__(''),  [], '');
        }
}

exports.saveSearch = (req) => {
    let params = req.body;
    const authUser = _.get(req, 'authUser') || _.get(req, 'user');


    data = JSON.stringify(params);
    historyId = ((params['history_id']) ? params['history_id'] : '');
    historyData = ReportSearchHistory.getSearchInfo(historyId);
    if(historyData.length > 0) {
        reportSearchHistoryId = historyData['0']['report_search_history_id'];
        searchArray = {'data' : data, 'type' : 1, 'modified_date' : dateFormat('yyyy-mm-dd HH:MM:ss')};
       
        ReportSearchHistory.updateOne(reportSearchHistoryId, searchArray);
    } else {
        conditionInsert = { 'data' : data, 'type' : 1,'search_title' : params['title'], 'user_id' : authUser['user_id'], 'created_date' : dateFormat('yyyy-mm-dd HH:MM:ss')};

        ReportSearchHistory.createOne(conditionInsert);
    }
}

exports.getSalesFunnelReport = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
      let pagination = {};
      let data = await salesFunnelReportModel.get(params);
     // let data = datas
    if(data.length){
      async.forEach((data[0]), async (el, callback) => { 

        let key = _.findIndex(data[0], el);//FIND INDEX

        req.body['cb_function'] = true;
        let cityList = await cityController.getCityStateList(req, res, next);
        // FIND MATCHED CITY
        if(cityList && cityList['city']){
          let cityData = cityList['city'].filter(city=>city.id==el.customer_city_id);
          data[0][key]['city_name'] = (cityData && cityData[0] && cityData[0]['name'] && cityData[0]['name']) || '';
        }

        callback(); 
            
      }, function(err) {
        let datas = data[0].filter(element=>element.totalLead !== null);
        //console.log('datas***************', datas);
        pagination['total'] = (datas && datas.length) ? datas.length : 0;
        pagination['limit'] = PAGE_LIMIT;
        pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
        pagination.cur_page = Number(page_number);
        pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
        pagination.next_page = (pagination.pages > page_number) ? true : false;
        let reportList = (datas && datas.length) ? datas:[];
        ApiController.sendPaginationResponse(req, res, reportList,pagination);

      });
      // data[0].forEach( async (el,key)=>{
        

      // });
    }

    
    
  }catch (error) {
    next(error);
  }
}

exports.getLeadSnapshotReport = async (req, res, next)=>{
  try{
    let authUser = _.get(req, 'user');
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let pagination = {};
    let data = await leadSnapshotReportModel.get(params,authUser);
    pagination['total'] = (data && data.length) ? data[1] : 0;
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    let reportList = (data && data.length) ? data[0]:[];
    ApiController.sendPaginationResponse(req, res, reportList,pagination);
    
  }catch (error) {
    next(error);
  }
}

exports.getLocationWiseLeadCountReport = async (req, res, next)=>{
  try{
    let authUser = _.get(req, 'user');
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let pagination = {};
    let data = await locationWiseLeadCountModel.get(params,authUser);
    let cityStateList = await InventoryService.getStateCityList(req);
    let cityList = (cityStateList && cityStateList.data && cityStateList.data.city) ? cityStateList.data.city : []
    pagination['total'] = (data && data.length) ? data[1] : 0;
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    let reportList = (data && data.length) ? data[0]:[];
    _.forEach(reportList,(obj)=>{
      let cityDetails = _.find(cityList,{'id':(obj.city_id)});
      obj['city_name'] = (cityDetails) ? cityDetails.name : null
    })
    ApiController.sendPaginationResponse(req, res, reportList,pagination);
    
  }catch (error) {
    next(error);
  }
}

exports.getL1CallingReport = async (req, res, next)=>{
  try{
    let authUser = _.get(req, 'user');
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let pagination = {};
    let isL1Agent = authUser['role_id'] == L1_USER_ROLE_ID ? true : false;
    let data = await l1CallingModel.get(params, page_number, authUser, isL1Agent);
    pagination['total'] = (data && data.length) ? data[1] : 0;
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    let reportList = (data && data.length) ? data[0]:[];
    ApiController.sendPaginationResponse(req, res, reportList,pagination);
    
  }catch (error) {
    next(error);
  }
}


exports.saveEmailAndFilters = async (req, res) => {
  try{
    let params = req.body;
        params['mail_sent'] = 0;
    let checkExists = await DealerDeliveryReportEmailsModel.get(params, false); 

    if(checkExists && checkExists[0] && checkExists[0].length){
      ApiController.sendSuccessResponse(req, res, {},'Record already exists');
    }else{
      let emailRegex = new RegExp(/\S+@\S+\.\S+/);

      if(emailRegex.test(params['email'])){
        let resp = await DealerDeliveryReportEmailsModel.createOne(params);
        ApiController.sendSuccessResponse(req, res, {},'Data saved Successfully');
      }else{
        ApiController.sendSuccessResponse(req, res, {},'Please enter valid email');
      }
    }

  }catch(error) {
    // console.log('error', error)
  }
}
