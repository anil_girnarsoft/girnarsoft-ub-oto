const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const _ = require("lodash");
const leadCarModel = require('../../lead/model/leadCarModel');
const ReportDealerModel = require('../../lead/model/reportDealerModel');
const clusterModel = require('../../cluster/model/clusterModel');
const InventoryService = require(COMMON_MODEL + 'inventoryService');
const DealerService = require(COMMON_MODEL + 'dealerService');
const conf = require('./../../../../config/config');
const { GoogleSpreadsheet } = require('google-spreadsheet');
const { promisify } = require("util");
const creds = require("./../../../../" + conf.REPORT_SHEET_CREDENTIAL);
const { reject } = require("lodash");
const { async } = require("q");

exports.getDealerReport = async (req, res, next) => {
  try {

    req['query']['filter'] = {};
    req['query']['filter']['status'] = [1];
    let dealerList = await DealerService.getDealerList(req);
    let dealers = dealerList['data'];
    let dealerIds = dealers.map(el => el.id);
    
    if (process.env.COUNTRY_CODE.toLowerCase() == 'id'){
      await createHeader({ headers: ['DealerID', 'Dealer_name', 'Dealer_Type', 'City', 'ActiveListings', 'Organic_lead-7days', 'Organic_lead-14days', 'Organic_lead-30days', 'Organic_Leads-MTD', 'Backend_lead-7days', 'Backend_lead-14days', 'Backend_lead-30days', 'Unique_BackendLeads-MTD', 'UniqueInventory_leads_30DAYS', 'New_Listings-7Days', 'New_Listings-14Days', 'New_Listings-30Days', 'New_Listings-MTD', 'Package_Type', 'Start_Date', 'End_Date', 'AM/BRO_Name'] });
    }
    if(process.env.COUNTRY_CODE.toLowerCase() == 'ph'){
      await createHeader({ headers: ['Onboarding_date', 'DealerID', 'package_id', 'Package_Sub_type', 'Package_Type', 'Revenue', 'Dealer_name', 'AccountManagerName', 'City', 'MaxListings', 'ActiveListings', 'Pack_status', 'Pack_expiry_date', 'last_lead_time', 'start_date', 'first_lead_date', 't_1', 't_3', 't_7', 't_14', 't_30', 'BL_1', 'BL_3', 'BL_7', 'BL_14', 'BL_30','NL_1', 'NL_3', 'NL_7', 'NL_14', 'NL_30', 'UniqueInventory_leads_30DAYS', 'RN', 'Renewal_Counter', 'Further_Renewal','Fresh_or_Renew', 'Renewal_Date', 'Report_update_date_time'] });
    }
    let alldeatails = [];
    if(req['query'].dealer_id){
      dealerIds = [req['query'].dealer_id];
    }
    while (dealerIds.length) {
      let dealerIdsArr = dealerIds.splice(0, 10);
      let dealerDetailsResp = await DealerService.getDealerDetailsByIdsForReport(req, { dealer_id: dealerIdsArr });

      if(req['query'].dealer_id){
        alldeatails.push(dealerDetailsResp.data);
      }

      let cur_date = new Date();
      let curMnth = (cur_date.getMonth() + 1);
      if (curMnth < 10) {
        curMnth = '0' + curMnth;
      }
      let curDate = cur_date.getDate();
      if (curDate < 10) {
        curDate = '0' + curDate;
      }
      let to_date = cur_date.getFullYear() + '-' + curMnth + '-' + curDate;
      let monthFirstDate = cur_date.getFullYear() + '-' + curMnth + '-01';
      let previous_date = await getBeforeDateBydays(1);
      let before3DayDate = await getBeforeDateBydays(3);
      let sevenDaysBeforeDate = await getBeforeDateBydays(7);
      let fourteenDaysBeforeDate = await getBeforeDateBydays(14);
      let thirtyDaysBeforeDate = await getBeforeDateBydays(30);
      let dates = [
        { date_from: previous_date, date_to: to_date, key: "prev_day" },
        { date_from: before3DayDate, date_to: to_date, key: "three_days" },
        { date_from: sevenDaysBeforeDate, date_to: to_date, key: "seven_days" },
        { date_from: fourteenDaysBeforeDate, date_to: to_date, key: "fourteen_days" },
        { date_from: thirtyDaysBeforeDate, date_to: to_date, key: "thirty_days" },
        { date_from: monthFirstDate, date_to: to_date, key: "mtd" }
      ];

      let allDaysStocks = await InventoryService.getStockCountByDealerIdWithDate(req, { dealer_ids: dealerIdsArr, stock_count: dates })
      let stocks = [];
      if (allDaysStocks.status == 200 && (allDaysStocks.data.length)) {
        stocks = allDaysStocks.data;
      }
      

      if (dealerDetailsResp.status == 200 && dealerDetailsResp.data) {
        
        let dataArr = [];
        let index = -1;
        for (let dealersData of dealerDetailsResp.data) {      

          let reportObj = {};
          reportObj['Onboarding_date'] = (dealersData.created_date) ? await dateFormat(dealersData.created_date) : '';
          reportObj['DealerID'] = dealersData.dealer_id;
          reportObj['Dealer_name'] = dealersData.organization;
          reportObj['City'] = (dealersData.city_name) ? dealersData.city_name : '';
          reportObj['AccountManagerName'] = (dealersData.ac_manager_name) ? dealersData.ac_manager_name : '';
          reportObj['AM/BRO_Name'] = (dealersData.bro_names) ? dealersData.bro_names : '';
          //let activeListing = await ReportDealerModel.getInventoryCount({ dealer_id: dealersData.dealer_id, car_status: '1', is_classified: '1' });
          //reportObj['ActiveListings'] = activeListing[0].total;

          let t_1 = await ReportDealerModel.getLeadsCount({ dealer_id: dealersData.dealer_id, days: 1, front_end_lead: 1 });
          let t_3 = await ReportDealerModel.getLeadsCount({ dealer_id: dealersData.dealer_id, days: 3, front_end_lead: 1 });
          let t_7 = await ReportDealerModel.getLeadsCount({ dealer_id: dealersData.dealer_id, days: 7, front_end_lead: 1 });
          let t_14 = await ReportDealerModel.getLeadsCount({ dealer_id: dealersData.dealer_id, days: 14, front_end_lead: 1 });
          let t_30 = await ReportDealerModel.getLeadsCount({ dealer_id: dealersData.dealer_id, days: 30, front_end_lead: 1 });
          reportObj['t_1'] = (t_1.length) ? t_1[0].total : 0;
          reportObj['t_3'] = (t_3.length) ? t_3[0].total : 0;
          reportObj['t_7'] = (t_7.length) ? t_7[0].total : 0;
          reportObj['t_14'] = (t_14.length) ? t_14[0].total : 0;
          reportObj['t_30'] = (t_30.length) ? t_30[0].total : 0;

          let bl_1 = await ReportDealerModel.getLeadsCount({ dealer_id: dealersData.dealer_id, days: 1, back_end_lead: 1 });
          let bl_3 = await ReportDealerModel.getLeadsCount({ dealer_id: dealersData.dealer_id, days: 3, back_end_lead: 1 });
          let bl_7 = await ReportDealerModel.getLeadsCount({ dealer_id: dealersData.dealer_id, days: 7, back_end_lead: 1 });
          let bl_14 = await ReportDealerModel.getLeadsCount({ dealer_id: dealersData.dealer_id, days: 14, back_end_lead: 1 });
          let bl_30 = await ReportDealerModel.getLeadsCount({ dealer_id: dealersData.dealer_id, days: 30, back_end_lead: 1 });
          reportObj['BL_1'] = (bl_1.length) ? bl_1[0].total : 0;
          reportObj['BL_3'] = (bl_3.length) ? bl_3[0].total : 0;
          reportObj['BL_7'] = (bl_7.length) ? bl_7[0].total : 0;
          reportObj['BL_14'] = (bl_14.length) ? bl_14[0].total : 0;
          reportObj['BL_30'] = (bl_30.length) ? bl_30[0].total : 0;

          let matchedDealerNL=[];
          if (stocks.length) {
            matchedDealerNL = stocks.filter(sc => sc.dealer_id == dealersData.dealer_id);
          }
            reportObj['NL_1'] = (matchedDealerNL.length && matchedDealerNL[0].prev_day) ? matchedDealerNL[0].prev_day : 0;
            reportObj['NL_3'] = (matchedDealerNL.length && matchedDealerNL[0].three_days) ? matchedDealerNL[0].three_days : 0;
            reportObj['NL_7'] = (matchedDealerNL.length && matchedDealerNL[0].seven_days) ? matchedDealerNL[0].seven_days : 0;
            reportObj['NL_14'] = (matchedDealerNL.length && matchedDealerNL[0].fourteen_days) ? matchedDealerNL[0].fourteen_days : 0;
            reportObj['NL_30'] = (matchedDealerNL.length && matchedDealerNL[0].thirty_days) ? matchedDealerNL[0].thirty_days : 0;

            //New list for ID
            reportObj['New_Listings-7Days'] = (matchedDealerNL.length && matchedDealerNL[0].seven_days) ? matchedDealerNL[0].seven_days : 0;
            reportObj['New_Listings-14Days'] = (matchedDealerNL.length && matchedDealerNL[0].fourteen_days) ? matchedDealerNL[0].fourteen_days : 0;
            reportObj['New_Listings-30Days'] = (matchedDealerNL.length && matchedDealerNL[0].thirty_days) ? matchedDealerNL[0].thirty_days : 0;
            reportObj['New_Listings-MTD'] = (matchedDealerNL.length && matchedDealerNL[0].mtd) ? matchedDealerNL[0].mtd : 0;
          
          let ol_7 = await ReportDealerModel.getOrganicLeads({ dealer_id: dealersData.dealer_id, days: 7 });
          let ol_14 = await ReportDealerModel.getOrganicLeads({ dealer_id: dealersData.dealer_id, days: 14 });
          let ol_30 = await ReportDealerModel.getOrganicLeads({ dealer_id: dealersData.dealer_id, days: 30 });
          let ol_mtd = await ReportDealerModel.getOrganicLeads({ dealer_id: dealersData.dealer_id, mtd: 1 });
          reportObj['Organic_lead-7days'] = (ol_7.length) ? ol_7[0].total : 0;
          reportObj['Organic_lead-14days'] = (ol_14.length) ? ol_14[0].total : 0;
          reportObj['Organic_lead-30days'] = (ol_30.length) ? ol_30[0].total : 0;
          reportObj['Organic_Leads-MTD'] = (ol_mtd.length) ? ol_mtd[0].total : 0;

          reportObj['Backend_lead-7days'] = (bl_7.length) ? bl_7[0].total : 0;
          reportObj['Backend_lead-14days'] = (bl_14.length) ? bl_14[0].total : 0;
          reportObj['Backend_lead-30days'] = (bl_30.length) ? bl_30[0].total : 0;
          let uniqueBL = await ReportDealerModel.getUniqueBackendLeads({ dealer_id: dealersData.dealer_id, mtd: 1 });
          reportObj['Unique_BackendLeads-MTD'] = (uniqueBL.length) ? uniqueBL[0].total : 0;

          let uniqueInventory = await ReportDealerModel.getUniqueInventory({ dealer_id: dealersData.dealer_id, added_by: 0, days: 30 });
          reportObj['UniqueInventory_leads_30DAYS'] = (uniqueInventory.length) ? uniqueInventory[0].total : 0;
          reportObj['Report_update_date_time'] = await dateFormat(new Date(), 1);

          let dealerType = 'FREE';
          if (dealersData.paid_score) {
            if (dealersData.paid_score == 2) {
              dealerType = 'PAID';
            } else if (dealersData.paid_score == 3) {
              dealerType = 'PAID PRO';
            }
          }
          reportObj['Dealer_Type'] = dealerType;

          if (dealersData.booking) {
            let counter = 0;
            let current = {};
            let cnt = 0;
            let sku_count = {};
            let packRnDate = {};
             
            dealersData.booking.map(dealersku => {
              if (sku_count[dealersku.sku_short_name]) {
                sku_count[dealersku.sku_short_name] += 1;
              } else {
                sku_count[dealersku.sku_short_name] = 1;
              }              
            });

            for (let element of dealersData.booking) {             
              index++;
              if (process.env.COUNTRY_CODE.toLowerCase() == 'id' && element.sku_short_name == 'buyer_listing' && (element.status_type == "existing")) {
                reportObj = { ...reportObj };
                reportObj['MaxListings'] = (element.quota) ? element.quota : '';
                reportObj['ActiveListings'] = (element.active_cars && element.active_cars.length) ? element.active_cars.length : 0;
                let packStartDate = await dateFormat(element.b_details_validity_from);
                let packExpireDate = await dateFormat(element.b_details_validity_to);
                if (current[element.sku_short_name]) {
                  current[element.sku_short_name] += 1;
                } else {
                  current[element.sku_short_name] = 1;
                }

                reportObj['End_Date'] = packExpireDate
                reportObj['start_date'] = packStartDate
                reportObj['Start_Date'] = packStartDate;
                
                if (element.fields.length) {
                  let substatus = element.fields.filter(subs => subs.attr_attribute_code == 'sub_status');
                  reportObj['Package_Type'] = (substatus.length) ? substatus[0].bd_attr_attribute_value : '';
                  reportObj['Revenue'] = (element.b_details_selling_price)?element.b_details_selling_price:0;
                }
                dataArr.push(reportObj);
                ++counter;
              } else if (process.env.COUNTRY_CODE.toLowerCase() == 'ph') {
                reportObj = { ...reportObj };
                reportObj['MaxListings'] = (element.quota) ? element.quota : '';
                reportObj['ActiveListings'] = (element.active_cars && element.active_cars.length) ? element.active_cars.length : 0;
                let packStartDate = await dateFormat(element.b_details_validity_from);
                let packExpireDate = await dateFormat(element.b_details_validity_to);
                reportObj['package_id'] = element.b_details_id;
                reportObj['MaxListings'] = (element.quota) ? element.quota : '';
                reportObj['Pack_status'] = element.status_type;
                reportObj['Pack_expiry_date'] = packExpireDate
                reportObj['End_Date'] = packExpireDate
                reportObj['start_date'] = packStartDate;
                reportObj['Start_Date'] = packStartDate;
                if (current[element.sku_short_name]) {
                  current[element.sku_short_name] += 1;
                } else {
                  current[element.sku_short_name] = 1;
                }
                let further_renewal = '';
                if ((sku_count[element.sku_short_name] == 1) || (sku_count[element.sku_short_name] == current[element.sku_short_name])) {
                  further_renewal = 0;
                } else {
                  further_renewal = 1;
                }
                if (sku_count[element.sku_short_name] == 1) {
                  reportObj['Renewal_Counter'] = 0
                } else {
                  reportObj['Renewal_Counter'] = sku_count[element.sku_short_name] - 1;
                }
                reportObj['RN'] = current[element.sku_short_name] - 1;
                reportObj['Fresh_or_Renew'] = (current[element.sku_short_name] == 1) ? 'Fresh' : 'ReNew';
                reportObj['Package_Sub_type'] = element.sku_name;

                reportObj['Further_Renewal'] = further_renewal;

                let date = (element.b_details_validity_from) ? await dateFormat(element.b_details_validity_from) : '';                
                reportObj['Renewal_Date'] = '';
                
                if( packRnDate[dealersData.dealer_id+'_'+element.sku_short_name] && packRnDate[dealersData.dealer_id+'_'+element.sku_short_name].last_index < index){
                  dataArr[packRnDate[dealersData.dealer_id+'_'+element.sku_short_name].last_index]['Renewal_Date'] = date;
                }
                packRnDate[dealersData.dealer_id+'_'+element.sku_short_name] = {last_index: index, created_date: date};

                if (element.linked_cars && element.linked_cars.length) {                  
                  let getFirstLeadDateTime = await ReportDealerModel.getLeadTime({ start_date: packStartDate, expire_date: packExpireDate, first_lead: 1, dealer_id: dealersData.dealer_id, package_car_ids: (element.linked_cars && element.linked_cars.length) ? element.linked_cars.join() : '' });
                  reportObj['first_lead_date'] = (getFirstLeadDateTime.length) ? await dateFormat(getFirstLeadDateTime[0].lead_date) : '';
                  let getLastLeadDateTime = await ReportDealerModel.getLeadTime({ start_date: packStartDate, expire_date: packExpireDate, last_lead: 1, dealer_id: dealersData.dealer_id, package_car_ids: (element.linked_cars && element.linked_cars.length) ? element.linked_cars.join() : '' });
                  reportObj['last_lead_time'] = (getLastLeadDateTime.length) ? await dateFormat(getLastLeadDateTime[0].lead_date, 1) : '';
                }

                if (element.fields.length) {
                  let substatus = element.fields.filter(subs => subs.attr_attribute_code == 'sub_status');
                  reportObj['Package_Type'] = (substatus.length) ? substatus[0].bd_attr_attribute_value : '';
                  // let vatp = element.fields.filter(subs => subs.attr_attribute_code == '_vat_price');
                  // if (vatp.length && vatp[0].bd_attr_attribute_value > 0) {
                  //   reportObj['Revenue'] = vatp[0].bd_attr_attribute_value;
                  // } else if(element.b_details_selling_price) {
                  //   //let sp = element.fields.filter(subs => subs.attr_attribute_code == '_selling_price');
                  //   reportObj['Revenue'] = element.b_details_selling_price;
                  // }
                  reportObj['Revenue'] = (element.b_details_selling_price)?element.b_details_selling_price:0;

                }
                dataArr.push(reportObj);
                ++counter;
              }
            }
          } else {
            if (process.env.COUNTRY_CODE.toLowerCase() == 'ph') {
              dataArr.push(reportObj);
            }
          }

        }
        //console.log('***************************dataArr----------------------------------------', dataArr);
        await createReport(dataArr);
      }

    }


    ApiController.sendPaginationResponse(req, res, alldeatails, '');

  } catch (error) {
    console.log('error--------------------------', error);
  }
}

const createHeader = async (params) => {
  try {

    return new Promise(async (resolve, reject) => {
      const doc = new GoogleSpreadsheet(conf.DEALER_REPORT_SHEET_ID);
      await doc.useServiceAccountAuth(creds);
      await doc.loadInfo();
      await doc.updateProperties({ title: conf.DEALER_REPORT_SHEET_TITLE });
      const sheet = doc.sheetsByIndex[0];
      await sheet.clear();
      if (params.headers) {
        let headerData = [];
        for (i of params.headers) {
          headerData.push(i);
        }
        await sheet.setHeaderRow(headerData);
      }

      resolve(1);
    });

  } catch (error) {
    console.log('Google sheet error-------', error);
  }
}

const createReport = async (params) => {
  try {
    const doc = new GoogleSpreadsheet(conf.DEALER_REPORT_SHEET_ID);
    await doc.useServiceAccountAuth(creds);
    await doc.loadInfo();
    const sheet = doc.sheetsByIndex[0];
    await sheet.addRows(params);
  } catch (error) {
    // console.log('Error:----', error);
  }
}

const dateFormat = async (sdate, time) => {
  return new Promise(async (resolve, reject) => {
   // console.log('sdate------------------------------', sdate); return false;
    let startsdate = new Date(sdate);

    let hoursInMins = 0;
    if(process.env.COUNTRY_CODE.toLowerCase() == 'ph'){
      hoursInMins = 8*60;
    }
    if(process.env.COUNTRY_CODE.toLowerCase() == 'id'){
      hoursInMins = 7*60;
    }    
    let millisecondsPerMinute = 60000;
    let newdate = startsdate;
    newdate.setTime(startsdate.getTime()+(hoursInMins*millisecondsPerMinute));
    let datesnew = new Date(newdate);
    //console.log('startdate---------------------------------------------------------------', startdate);
    let sdd = datesnew.getDate();    
    let smm = String(datesnew.getMonth() + 1).padStart(2, '0');
    let syyyy = datesnew.getFullYear();
    let times = datesnew.getHours() + ":" + datesnew.getMinutes() + ":" + datesnew.getSeconds();
    let date = syyyy + '-' + smm + '-' + sdd;
    if (time == 1) {
      date += ' ' + times;
    }
    resolve(date);
  });
}

const getBeforeDateBydays = async (days) => {
  return new Promise(async (resolve, reject) => {
    let curs_date = new Date();
    let req_date = new Date(curs_date.setDate(curs_date.getDate() - days));
    let mm = (req_date.getMonth() + 1);
    let month = '';
    if (mm < 10) {
      month = '0' + mm;
    } else if (mm > 9) {
      month = mm;
    }
    let dates = req_date.getDate();
    if (dates < 10) {
      dates = '0' + dates;
    }
    let final_date = req_date.getFullYear() + '-' + month + '-' + dates;
    resolve(final_date);
  });
}