const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const _ = require("lodash");
const leadCarModel = require('../../lead/model/leadCarModel');
const InventoryService = require(COMMON_MODEL + 'inventoryService');
const DealerService = require(COMMON_MODEL + 'dealerService');

exports.getDealerReport = async (req, res, next) => {
  try {
    let totalLimit = 10;
    let allDealersData = [];
    let dealersInfo = [];
    let particularDealersData = {};
    let usedCarData = '';
     //req['query']['page_no'] = 1;
     //req['query']['record_per_page'] = totalLimit;
    let dealerResp = await DealerService.getDealerList(req);    
    let dealerData = dealerResp['data']? dealerResp['data'] : [];
    let deakerIdsArray = dealerData.map(el => el.id);
    let allDealersDetail = await DealerService.getDealerDetailsByIds(req, {dealer_id: deakerIdsArray});
    allDealersData = allDealersDetail['data'];
    let toDay = new Date();
    let cur_month = ("0"+(toDay.getMonth()+1)).slice(-2);
    let cur_date = ("0"+toDay.getDate()).slice(-2);
    let year = toDay.getFullYear();

    if(allDealersData.length) {
      for(const dealer of allDealersData) {
        particularDealersData.id = dealer.id;
        particularDealersData.organization = dealer.organization;
        //dealersInfo.push(particularDealersData);
        //usedCarData = await this.findUsedCars(req, {page:1,dealerId:[dealer.id]});    
        //if(usedCarData['usedCarIds'].length){ }   
        let last_Seven_daysOrganicLeads = leadCarModel.organicLeads({dealer_id:dealer.id, days:7});
        let last_Forteen_daysOrganicLeads = leadCarModel.organicLeads({dealer_id: dealer.id, days:14});
        let last_Thirty_daysOrganicLeads = leadCarModel.organicLeads({dealer_id: dealer.id,  days:30 });

        let start_date = year+"-"+cur_month+"-01";
        let currentdate = year+"-"+cur_month+"-"+cur_date;
        let mtdOrganicLeads = leadCarModel.organicLeads({dealer_id: dealer.id,  start_date:start_date, cur_date:currentdate});

        particularDealersData.seven_days_organic_leads = last_Seven_daysOrganicLeads;
        particularDealersData.forteen_days_organic_leads = last_Forteen_daysOrganicLeads;
        particularDealersData.thirty_days_organic_leads = last_Thirty_daysOrganicLeads;
        particularDealersData.mtdOrganicLeads = mtdOrganicLeads;

        dealersInfo.push(particularDealersData);
      }
    }

    ApiController.sendPaginationResponse(req, res, '', '');
  } catch (error) {
    next(error);
  }


}

exports.findUsedCars = async (req, params) => {
  try {
    let usedCarsParams = {};
    usedCarsParams['page_no'] = params.page;
    usedCarsParams['dealer_ids'] = params.dealerId;
    if (params['stock_used_car_id_arr']) {
      usedCarsParams['stock_used_car_id_arr'] = params.stock_used_car_id_arr;
    }
    usedCarsParams['call_server'] = 'ub';
    usedCarsParams['car_status'] = [1,2];
    usedCarsParams['classified'] = '1';
    var usedCarIds = [];
    var allUsedCars = [];
    let next_page = true;
    let total = 0;
    while (next_page) {      
      let findAllUsedCars = await InventoryService.getStockList(req, usedCarsParams);
      let  pagination = findAllUsedCars.pagination || {};      
      next_page = pagination && pagination.next_page;      
      if (next_page) {
        usedCarsParams.page_no = usedCarsParams.page_no + 1;
        //console.log('usedCarsParams-----------------------------', usedCarsParams);
      }
      if (findAllUsedCars && findAllUsedCars.status == 200) {
        findAllUsedCars['data'].forEach((usedCar, key) => {
          usedCarIds.push(usedCar.id);
          allUsedCars.push(usedCar);
        });
        total = pagination.total;
      }

    }
    return { 'usedCarIds': usedCarIds, allUsedCars: allUsedCars, 'totalCars': total};

  } catch (error) {
    // console.log('error ', error);
  }
}