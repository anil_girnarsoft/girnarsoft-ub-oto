const express = require('express');
const router = express.Router();
const WhatsappController = require('./controller/WhatsappController');
const NotificationController = require('./controller/NotificationController');


router.post('/crossSellNotification', WhatsappController.sendWhatsappNotification);


module.exports = router;