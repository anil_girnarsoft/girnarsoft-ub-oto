
const NotificationService = require(COMMON_MODEL + 'notificationService');
const WhatsappNotificationModel = require('../model/whatsappNotificationModel');
const WhatsappNotificationDetailsModel = require('../model/whatsappNotificationDetailsModel');
const WalkinRescheduleLinkModel = require('../model/walkinRescheduleLinkModel');
const LeadModel = require('../../lead/model/leadModel');
const LeadController = require('../../lead/controller/leadController');
const moment = require('moment');
const { WA_CHANNEL_TYPE, COMPANY_HANDLE, ISD_CODE, FRONTEND_HOST } = require("../../../../config/config");


exports.sendWhatsappNotification = async (req, res) => {
    return new Promise(async (resolve, reject) => {
        try{

            let params = req.body;

            let newReq          = Object.assign({}, req);
            let leadReq         = Object.assign({}, req);
            let carDetails      = [];
            let leadDetails     = [];
            let templateParams  = {"data": { "channel_type"  : WA_CHANNEL_TYPE, "company_handle": COMPANY_HANDLE, "send_now": true}, "source":"ub", "sub_source":"ub", "template_id":params['template_id'], "lead_id": params['lead_id'] };

            if(params['car_ids'] && params['car_ids'].length){

              if(params.notification_id){
                  let getNotificationData = await WhatsappNotificationModel.get({hash_id: params.notification_id, isPaginationApply: false});

                  carDetails =  (getNotificationData[0][0]['cars_detail']) ? JSON.parse(getNotificationData[0][0]['cars_detail']) : [];
                  
                  if(carDetails && carDetails.length){
                    carDetails[0]['walkin_datetime']  = params['rescheduled_datetime'];
                  }
              }else{
                  //FIND LEAD CARS DETAILS
                  newReq['body'] = {};
                  newReq['body']['lead_id']         = params['lead_id'];
                  newReq['body']['car_ids']         = params['car_ids'];
                  newReq['body']['return_function'] = true;
                  carDetails = await LeadController.getAssignedCarDetails(newReq,res);
                }
                
                //FIND LEAD DETAILS
                leadReq = {};
                leadReq['id'] = WhatsappNotificationModel.encrypt(params['lead_id']);

                leadDetails = await LeadModel.get(leadReq);
                if(leadDetails && leadDetails.length && leadDetails[0] && leadDetails[0]){
                  leadDetails = leadDetails[0];
                }
                
                if(carDetails && carDetails.length){
                  
                  let phoneNumber = ISD_CODE+carDetails[0]['customer_mobile'];

                  let customerCitySlug = (leadDetails && leadDetails.length && leadDetails[0]['customer_city_name'])? leadDetails[0]['customer_city_name'] : '';
                  customerCitySlug = customerCitySlug ? customerCitySlug.toLowerCase().replace(/ /g, '-') : '';

                  let waCheckParams = {'business_unit': 'mobil', 'platform': 'web', 'mobile': carDetails[0]['customer_mobile'], 'citySlug': customerCitySlug};
                  let checkWANotificationSubscriptionStatus = await NotificationService.checkWANotificationSubscriptionStatus(waCheckParams);

                  if(checkWANotificationSubscriptionStatus && checkWANotificationSubscriptionStatus['status'] && checkWANotificationSubscriptionStatus['data']){
                      //SAVE WHATSAPP NOTIFICATION
                      let saveWhatsappRecord = {
                        "cars_detail": JSON.stringify(carDetails),
                      };
              
                      //CREATE OBJ FOR SENDING WHATSAPP MSG
                      templateParams['data']['event_name']  =   params['template_name'];
                      templateParams['data']['to']          =   phoneNumber;

                      let contextData = {};
                      if(params['template_name'] === 'cross_sell_usedcar'){
                        saveWhatsappRecord["notification_type"] = "recommended_car";

                        contextData = await crossSellNotificationDataHandler(res, leadDetails, carDetails, params);
                      }else if(params['template_name'] === 'walking_reminder_cron' || params['template_name']  === 'walking_scheduled_notification'){
                        saveWhatsappRecord["notification_type"] = (params['template_name']  === 'walking_scheduled_notification') ? "walking_scheduled" : "walking_scheduled_reminder";
                        
                        contextData = await walkinScheduleReminderDataHandler(res, leadDetails, carDetails, params);
                      }

                      templateParams['data'] = {...templateParams['data'], 'context_variables': contextData['contextVariables']};

                      templateParams['data']['context_variables']['customer_name'] = carDetails[0]['customer_name'];

              
                      saveWhatsappRecord['car_ids']       = JSON.stringify(contextData['carIdsObj']);
                      saveWhatsappRecord['lead_id']       = params['lead_id'];
                      saveWhatsappRecord['request_data']  = JSON.stringify(templateParams);
                      saveWhatsappRecord['status']        = 1;

                      //SAVE SENT NOTIFICATION DATA
                      let whatsappRec = await WhatsappNotificationModel.createOne(saveWhatsappRecord);

                      if(whatsappRec){
                        templateParams['data']['context_variables']['notification_id'] = whatsappRec.id;

                        //SEND WHATSAPP NOTIFICATION
                        let msgResponse = await NotificationService.sendWhatsapp(req, templateParams);

                        if(msgResponse){
                          //UPDATE RESPONSE
                          await WhatsappNotificationModel.updateOne({id: whatsappRec.id}, {response: JSON.stringify(msgResponse)});
                        }
                      }
                  }
                  resolve(200);
                }else{
                  resolve(400);
                }
          }else{
            resolve(400);
          }

      }catch(err){
        resolve(400);
      }
    });
}

/**
 * CROSS SELL(RECOMMED CAR) DATA HANDLER
 */
crossSellNotificationDataHandler = async (res, leadDetails, carDetails, params) => {

      let cityName          = '';
      let contextVariables  = {};
      let similarCarsLink   = USED_CAR_URL_SIMILAR_CARS;
      let carIdsObj         = {};

      if(leadDetails[0]['customer_city_name']){
        cityName = ((leadDetails[0]['customer_city_name']).toLowerCase()).replace(/ /g, '-');
        similarCarsLink +=  cityName;
      }

      //BUDGET CALCULATOR
      if(leadDetails[0]['budget'] && leadDetails[0]['budget']>0){
        let budget    = (leadDetails[0]['budget']).toString();
        let maxBudget = budget.substring(0, (budget).length - 6);
        let minBudget = +maxBudget - (maxBudget.length == 3 ? 100 : 10);
        similarCarsLink += ((cityName) ? '+' : '') + minBudget+'-'+maxBudget+'-juta';
      }

      let urlShortener = await NotificationService.urlShortener({url: similarCarsLink, campaignId: 'uc_'+params['template_name']});

      contextVariables = { 'simillar_car_link': urlShortener || similarCarsLink};
      contextVariables['car_details2'] = '.';

      let key = 0;
      for (const el of carDetails) {
          carIdsObj[key+1] = el.car_id;
          contextVariables['car_details'+(key+1)] = (el.make_year || '')+' '+(el.make_name || '')+' '+(el.model_name || '');
          if(el['display_price']){
            contextVariables['car_details'+(key+1)] += ' @ ' +el['display_price']; 
          }
          if(el['web_url']){
            let weburlShortener = await NotificationService.urlShortener({url: el['web_url'], campaignId: 'uc_'+params['template_name']});
            contextVariables['car_details'+(key+1)] += ', '+(weburlShortener || el['web_url']);
          }
          contextVariables['car_details'+(key+1)] += ' '+res.__('CrossSellMessageCar'+(key+1)+'Reply');
          key++;
      }

      return {contextVariables,  carIdsObj};
}

/**
 * WALKIN RESCHEDULE DATA HANDLER
 */
walkinScheduleReminderDataHandler = async (res, leadDetails, carDetails, params) => {

    let contextVariables  = {};
    let carIdsObj         = {"1": carDetails[0].car_id};
    
    contextVariables = {
        'customer_name' : carDetails[0]['name'],
        'car_details'   : (carDetails[0].make_year || '')+' '+(carDetails[0].make_name || '')+' '+(carDetails[0].model_name || ''),
        'dealer_info'   : (carDetails[0].organization || '')+' '+(carDetails[0].dealership_contact ? ISD_CODE+carDetails[0].dealership_contact : ''),
        'date_time'     : moment(carDetails[0]['walkin_datetime']).format("YYYY-MM-DD, hh:mm A")
    };

    if(carDetails[0]['display_price']){
      contextVariables['car_details'] += ' @ ' +carDetails[0]['display_price']; 
    }

    if(carDetails[0].seller_address){
      contextVariables['address_url'] =  (carDetails[0].seller_address || '');
      if(carDetails[0].latitude && carDetails[0].longitude){
        //MAP URL
        let mapUrl = 'https://maps.google.com/?q='+carDetails[0].latitude+','+carDetails[0].longitude;
        //SHORT URL
        let weburlShortener = await NotificationService.urlShortener({url: mapUrl, campaignId: 'uc_'+params['template_name']});
        
        contextVariables['address_url'] += ', '+weburlShortener;
      }
      
    }
    return {contextVariables,  carIdsObj};

}

exports.callbackReply = async (req, res) => {
  return new Promise(async (resolve, reject) => {

      try{
        let params = req.body;
        let templateParams  = {"data": { "channel_type"  : WA_CHANNEL_TYPE, "company_handle": COMPANY_HANDLE, "send_now": true}, "source":"ub", "sub_source":"ub" };
        let respData = '';
        let shortWebUrl = '';

        if(params['notificationType'] === 'walking_scheduled' || params['notificationType'] === 'walking_scheduled_reminder'){

          let url = FRONTEND_HOST;

          url += 'walkin-reschedule?notificationId='+WhatsappNotificationModel.encrypt(params['notification_id']);
          
          let walkinRescheduleLinkParams = {};
          walkinRescheduleLinkParams['notification_id'] = params['notification_id'];
          walkinRescheduleLinkParams['leads_car_id']    = params['carDetails'][0]['id'];
          walkinRescheduleLinkParams['link']            = url;
          walkinRescheduleLinkParams['is_updated']      = '0';

          await WalkinRescheduleLinkModel.createOne(walkinRescheduleLinkParams);

          let weburlShortener = await NotificationService.urlShortener({url: url, campaignId: 'uc_'+params['template_name']});

          respData = "Hai "+params['carDetails'][0]['customer_name']+"\n\n Silakan klik tautan di bawah ini untuk menjadwalkan ulang Walk-in Anda.\n\n"+ weburlShortener;
        }else{
          if(params['carDetails'][0].web_url){
            shortWebUrl  = await NotificationService.urlShortener({url: params['carDetails'][0].web_url, campaignId: 'uc_'+params['template_name']});
          }

          let carImages = (params['carDetails'][0].car_image && params['carDetails'][0].car_image.length) ? (params['carDetails'][0].car_image).join(', ') : ''
          templateParams['data']['to'] = ISD_CODE+params['carDetails'][0]['customer_mobile'];
          templateParams['data']['context_variables'] = {
            'car_details'     : params['carDetails'][0].make_year+' '+params['carDetails'][0].make_name+' '+params['carDetails'][0].model_name+' @ '+' Rp '+params['carDetails'][0]['car_price'] + ', '+(shortWebUrl || ''),
            'dealer_details'  : (params['carDetails'][0].organization || '')+', '+(params['carDetails'][0].dealership_contact ? ISD_CODE+params['carDetails'][0].dealership_contact : ''),
            'car_images'      : carImages
          };
          
          //CALL BACK MESSAGE
          respData = "Silahkan lihat rincian untuk unit *"+(params['carDetails'][0].make_name)+" "+(params['carDetails'][0].orig_model)+"* "+params['carDetails'][0].variant_name+ (params['carDetails'][0].fuel_type ? " ("+params['carDetails'][0].fuel_type+")":'') +" , "+params['carDetails'][0].make_year+" Model, "+params['carDetails'][0].km_driven+" kms, "+params['carDetails'][0].transmission+" transmission @ *"+ params['carDetails'][0].display_price+"*. dengan klik pada link berikut: "+shortWebUrl+"\n\n";
          respData += "*Salam,* \n"+(params['carDetails'][0].organization || '')+'\n';
          respData += (params['carDetails'][0].dealership_contact ? "📞 "+ISD_CODE+params['carDetails'][0].dealership_contact : '')+'\n\n';

        }

        //REPLIED SENT DATA 
        if(Object.keys(templateParams).length){
          await WhatsappNotificationDetailsModel.updateOne({id: params['notification_detail_id']}, {request_data: JSON.stringify(templateParams)});
        }

        resolve({status:200, msg: respData});
      }catch (err){
        resolve({status:400, msg: err});
      }
  });
}
