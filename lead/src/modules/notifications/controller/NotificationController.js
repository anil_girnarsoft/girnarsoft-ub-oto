const ApiController = require(COMMON_CONTROLLER + 'ApiController');

const WhatsappNotificationModel = require('../model/whatsappNotificationModel');
const WalkinInfoModel = require('../../walkin/model/walkinInfoModel');
const _ = require("lodash");
const WalkinRescheduleLinkModel = require('../model/walkinRescheduleLinkModel');
const LeadCarModel = require('../../lead/model/leadCarModel');
const WhatsappController = require('../controller/WhatsappController');


exports.getNotificationDetail = async (req, res) => {
    try{

      let params      = req.query;
      let reponseData = {};

      let getNotificationData = await WhatsappNotificationModel.get({hash_id: params.notificationId, isPaginationApply: false});

      if(getNotificationData && getNotificationData.length && getNotificationData[0][0]){
        let carDetails =  (getNotificationData[0][0]['cars_detail']) ? JSON.parse(getNotificationData[0][0]['cars_detail']) : {};

        if(Date.parse(new Date(carDetails[0]['walkin_datetime'])) < Date.parse(new Date())){
          ApiController.sendResponse(req, res, 200, "Walkin date has been expired", {success: false});
        }else if(getNotificationData[0][0]['is_updated'] == '1'){
          ApiController.sendResponse(req, res, 200, "Link has been expired", {success: false});
        }

        reponseData['customer_name']      =  carDetails[0]['customer_name'];
        reponseData['make_model']         =  carDetails[0]['make_name']+' '+carDetails[0]['model_name'];
        reponseData['price']              =  carDetails[0]['display_price'];
        reponseData['dealer']             =  carDetails[0]['organization'];
        reponseData['dealer_type']        =  carDetails[0]['user_type'];
        reponseData['walkin_datetime']    =  carDetails[0]['walkin_datetime'];
        reponseData['make_year']          =  carDetails[0]['make_year'];
        reponseData['km_driven']          =  carDetails[0]['km_driven'];
        reponseData['fuel_type']          =  carDetails[0]['fuel_type'];
        reponseData['lead_id']            =  carDetails[0]['lead_id'];
        reponseData['car_id']             =  carDetails[0]['car_id'];
        reponseData['leads_cars_id']      =  carDetails[0]['id'];
        reponseData['reschedule_link_id'] =  getNotificationData[0][0]['reschedule_link_id'];
        reponseData['success']            =  true;

        ApiController.sendResponse(req, res, 200, "", reponseData);


      }else{
        ApiController.sendResponse(req, res, 200, "Invalid Notification Id");
      }

    }catch(err){
      ApiController.sendResponse(req, res, 200, "Please try again");
    }
}


exports.updateRescheduledDate = async (req, res) => {
  try{
    let params = req.body;
    const authUser = _.get(req, 'user'); 

    if(Date.parse(new Date(params['rescheduled_datetime'])) < Date.parse(new Date())){
      ApiController.sendResponse(req, res, 200, "Re-schedule date & time should be greater than current date & time", {success: false});
    }else{

      let saveWalkinDate = {};

      saveWalkinDate['leads_cars_id']     = params['leads_cars_id'];
      saveWalkinDate['lead_id']           = params['lead_id'];
      saveWalkinDate['walkin_datetime']   = params['rescheduled_datetime'];
      saveWalkinDate['walkin_status']     = '3';
      saveWalkinDate['status']            = 1;
      saveWalkinDate['added_by']          = (authUser && authUser['user_id']) ? authUser['user_id'] : 0;

      let createStatus = await WalkinInfoModel.createOne(saveWalkinDate);

      if(createStatus && createStatus.id){

        //UPDATE is_updated FLAG
        if(params['reschedule_link_id']){
          await WalkinRescheduleLinkModel.updateOne({id: params['reschedule_link_id']}, {is_updated: '1'});
        }

        //SEND WHATSAPP NOTIFICATION
        let notificationParams = {"rescheduled_datetime": params['rescheduled_datetime'], "notification_id": params['notification_id'], "lead_id": params['lead_id'], "leads_car_id":params['leads_cars_id'], "car_ids":[params['car_id']]};
        // notificationParams['car_ids'] = slicedArray.map(el=>el.car_id);//[481174, 481170];
        let newReq = Object.assign({}, req);
        newReq['body'] = notificationParams;
        newReq['body']['template_name'] = 'walking_scheduled_notification';
        newReq['body']['template_id']   = '12';

        await WhatsappController.sendWhatsappNotification(newReq,res);

        //UPDATE CANCELED FLAG TO 0
        LeadCarModel.updateOne(params['leads_cars_id'], {'walkin_reschedule_canceled': '0'});

      }
      ApiController.sendResponse(req, res, 200, "Walkin Re-scheduled successfully", {success: true});

    }

  }catch(err){
    ApiController.sendResponse(req, res, 200, "Please try again");
  }
}