const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class WhatsappNotificationModel extends Model{
}

WhatsappNotificationModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    notification_type:{
        type: dbConfig.Sequelize.ENUM('recommended_car','walking_scheduled','walking_scheduled_reminder'),
        allowNull: true,
        defaultValue:"recommended_car"
    },
    cars_detail:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: false
    },
    car_ids:{
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    lead_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    request_data:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: false
    },
    response:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    status:{
        type: dbConfig.Sequelize.TINYINT,
        allowNull: false
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
    
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_WHATSAPP_NOTIFICATION,
    freezeTableName: true
});


WhatsappNotificationModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WhatsappNotificationModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

WhatsappNotificationModel.updateOne = async (_where, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WhatsappNotificationModel.update(data, { where: _where });

            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}


WhatsappNotificationModel.getModel = (params) => {
    let dataModel = {};

    if((params.status))
        dataModel.status = params.status;
    
        return dataModel;
}

WhatsappNotificationModel.get = async (_where, pagination) =>{
    let isPaginationApply = (typeof _where.isPaginationApply !== 'undefined') ? _where.isPaginationApply : true;
    let whereCond = await WhatsappNotificationModel.bindCondition(_where, pagination, isPaginationApply)  //true is used to apply pagination limit
    return new Promise((resolve, reject)=>{
    
        let sql = "SELECT SQL_CALC_FOUND_ROWS uwn.id, uwn.notification_type, uwn.cars_detail, uwn.car_ids,uwn.lead_id, uwn.request_data, uwrl.id as reschedule_link_id, uwrl.link, uwrl.is_updated FROM " + UBLMS_WHATSAPP_NOTIFICATION + " as uwn LEFT JOIN "+UBLMS_WALKIN_RESCHEDULE_LINK+" as uwrl ON uwrl.notification_id = uwn.id "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async result=>{
            let totalCount = await WhatsappNotificationModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(error=>{
            reject(error);
        });
    })
}

WhatsappNotificationModel.encrypt = (id) => {
    return crypto.encode(id);

}

WhatsappNotificationModel.decrypt = (hashId) => { 
    let id = '';
    id = crypto.decode(hashId);
    return id;
}

WhatsappNotificationModel.bindCondition = async (params, pagination, isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond = await WhatsappNotificationModel.bindPagination(params, pagination, isPaginationApply);
    let GROUPBY = 'GROUP BY uwn.id';

    let status = "uwn.status='1'";
    condition = (condition == '') ? condition + "where " + status : condition + " and " + status

    if (params && params.id && params.id) {
        let id = "uwn.id ='" + params.id + "'";
        condition = (condition == '') ? condition + "where " + id : condition + " and " + id
    }

    if (params && params.hash_id && params.hash_id) {
        let id = "uwn.id ='" + WhatsappNotificationModel.decrypt(params.hash_id) + "'";
        condition = (condition == '') ? condition + "where " + id : condition + " and " + id
    }
    
    if (params && params.sort && params.sort.length) {
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort, (srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort + ', ' + sortData : sortData
        })
        sortCond = (finalSort != '') ? 'ORDER BY ' + finalSort : '';
    }
    return condition + " " + GROUPBY + " " + sortCond + " " + paginationCond;
}

WhatsappNotificationModel.bindPagination = (params, pagination, isPaginationApply) => {
    let condition = '';
    if (isPaginationApply) {
        let page_no = pagination;
        let limit = params.page_limit || PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT " + offset + "," + limit;
    }
    return condition;
}

WhatsappNotificationModel.getRowsCount = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
            .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
            .then(result => {
                resolve(result[0].total);
            }).catch(error => {
                reject(error);
            });
    })
}




module.exports = WhatsappNotificationModel;