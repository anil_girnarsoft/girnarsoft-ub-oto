const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class WalkinRescheduleLinkModel extends Model{
}

WalkinRescheduleLinkModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    notification_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    leads_car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    link: {
        type: dbConfig.Sequelize.STRING,
        allowNull: false
    },
    is_updated: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        default:'0'
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
    
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_WALKIN_RESCHEDULE_LINK,
    freezeTableName: true
});


WalkinRescheduleLinkModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkinRescheduleLinkModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

WalkinRescheduleLinkModel.updateOne = async (_where, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WalkinRescheduleLinkModel.update(data, { where: _where });
            
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

WalkinRescheduleLinkModel.get = async() =>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT ubwnd.id, ubwnd.notification_id, ubwnd.called_option, ubwnd.car_id,ubwnd.car_details,ubwnd.request_data, ubwn.notification_type FROM " + UBLMS_WHATSAPP_NOTIFICATION_DETAILS + " LEFT JOIN " + UBLMS_WHATSAPP_NOTIFICATION + " as uwn ON uwn.id = uwnd.notification_id";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}




module.exports = WalkinRescheduleLinkModel;