const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
class WhatsappNotificationDetailsModel extends Model{
}

WhatsappNotificationDetailsModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    notification_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    called_option:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    car_details:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: false
    },
    car_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    request_data:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: false
    },
    response:{
        type: dbConfig.Sequelize.TEXT,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
    
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_WHATSAPP_NOTIFICATION_DETAILS,
    freezeTableName: true
});


WhatsappNotificationDetailsModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WhatsappNotificationDetailsModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

WhatsappNotificationDetailsModel.updateOne = async (_where, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await WhatsappNotificationDetailsModel.update(data, { where: _where });
            
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

WhatsappNotificationDetailsModel.get = async() =>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT ubwnd.id, ubwnd.notification_id, ubwnd.called_option, ubwnd.car_id,ubwnd.car_details,ubwnd.request_data, ubwn.notification_type FROM " + UBLMS_WHATSAPP_NOTIFICATION_DETAILS + " LEFT JOIN " + UBLMS_WHATSAPP_NOTIFICATION + " as uwn ON uwn.id = uwnd.notification_id";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}




module.exports = WhatsappNotificationDetailsModel;