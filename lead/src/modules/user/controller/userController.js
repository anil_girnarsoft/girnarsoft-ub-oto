const Q = require("q");
const _ = require("lodash");
const userModel = require('./../model/userModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const dateFormat = require('dateformat');
const schemas = require("../validation_schema");
const siteSetting = require(SITE_SETTINGS);
const cityController = require('../../city/controller/cityController');


exports.saveUser = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    const userId = _.get(req.headers, 'userid');
    let id = params.id;
    if(params.username){
      let isUserExist = await userModel.getOne({'username':params.username});
      if(isUserExist){
        if(!id || id != isUserExist.dataValues.id) throw new Error('user_already_exist')
      }
    }
    if(!id){
      params.added_by = userId;
      
      let userObj = userModel.getModel(params);
      userModel.createOne(userObj)
      .then(response =>{
        const user_data = response.dataValues;
        if(user_data.id > 0){
          ApiController.sendSuccessResponse(req, res, user_data,'user_created_successfully');
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_CREATE");
        }
      })
      .catch(err => {
        next(err);
      })
      
    }else{
      params.updated_by = userId;
      let isUserExist = await userModel.getOne({'id':params.id});
      if(!isUserExist)
        throw new Error('id_does_not_exist')
      let userObjToUpdate = userModel.getModel(params);
      userObjToUpdate['email'] =  userObjToUpdate['email'] || null; 
      userObjToUpdate['agent_id'] = userObjToUpdate['agent_id'] || ''; 
      userModel.updateOne(id,userObjToUpdate)
      .then(response => {
        if(response && response.length){
          ApiController.sendResponse(req, res,200,'user_updated_successfully');
        }else{
          ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
        }
      })
      .catch(error => {
        next(error);
      })
    }
  } catch (error) {
    next(error);
  }
  
}

exports.getUserList = async (req, res, next)=>{
  try{
    let page_number;
    let params = req.body
    if(params.page_number){
      page_number = params.page_number;
      delete req.body.page_number;
    }else
      page_number = PAGE_NUMBER;
    let { value } = schemas.getList.validate(req.body);
    const userId = _.get(req.headers, 'userid');
    let pagination = {};
    value.user_id =userId 
    const user = _.get(req, 'user');
    value.roleId = user ? user.role_id : 0
    let data = await userModel.get(value,page_number);
    let userList = userModel.encrypt(data[0]);

    let userListUpdated = [];
    req.body['cb_function'] = true;
    if(userList.length){
      let cityList = await cityController.getCityStateList(req, res);
      userListUpdated = userList.map(el=>{
        // let cityName = '';
        let cityObj = [];
        let cityIds = el.allowed_city_ids ? el.allowed_city_ids.split(',') : [];
              (cityIds && cityIds.length ) && cityIds.forEach(elm=>{
                (cityList && cityList['city'] && cityList['city'].length && cityList['city']).forEach(ct => {
                    if(ct.id == elm){
                        // cityName +=  ((cityName.length) ? ',' : '') + ct.name;
                        cityObj.push({value: ct.id, label: ct.name});
                    }
                });
              });
              el.cityObj = cityObj;
              return el;
      });
    }

    pagination['total'] = data[1];
    pagination['limit'] = PAGE_LIMIT;
    pagination['pages'] = Math.ceil(pagination.total / pagination.limit);
    pagination.cur_page = Number(page_number);
    pagination.prev_page = (page_number > 1) ? page_number - 1 : false;
    pagination.next_page = (pagination.pages > page_number) ? true : false;
    ApiController.sendPaginationResponse(req, res, userListUpdated,pagination);
  } catch (error) {
    next(error);
  }
}

exports.getUser = async (req, res, next)=>{
  try{
    let result={},userList=[];
    let { value } = schemas.getList.validate(req.body);
    result = await userModel.getOne(value,1);
    if(result)
      userList[0] = userModel.encrypt(result.dataValues);
    ApiController.sendPaginationResponse(req, res, userList,{});
  } catch (error) {
    next(error);
  }
}

exports.updateStatus = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    let id = req.body.id;
    let statusObj = userModel.getModel(params)
    userModel.updateOne(id,statusObj)
    .then(response => {
      if(response && response.length){
        ApiController.sendResponse(req, res,200,'status_updated_successfully');
      }else{
        ApiController.sendErrorResponse(req, res, "ERROR_IN_UPDATE");
      }
    })
    .catch(error => {
      next(error);
    })
  } catch (error) {
      next(error);
  }
  
}

exports.getAllUserList = async (req, res, next)=>{
  try{
    let { value } = schemas.getList.validate({});
    let userList = await userModel.getAll(value);
    ApiController.sendPaginationResponse(req, res, userList);
  } catch (error) {
    next(error);
  }
}

exports.getSiteSettings = async (req, res, next) => {
  try{
    ApiController.sendPaginationResponse(req, res, siteSetting);
  }catch(error) {
    ApiController.sendErrorResponse(req, res, res.__("server_error"));
  }
}

exports.getTlList = async (req, res, next)=>{
  try{
    let userList = await userModel.getTLnames();
    ApiController.sendPaginationResponse(req, res, userList);
  } catch (error) {
    next(error);
  }
}