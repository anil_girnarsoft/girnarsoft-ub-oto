const _ = require("lodash");
const userModel = require('../model/userModel');
const userTokenModel = require('../model/userTokenModel');
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const Captcha = require('node-captcha-generator');
const jwt = require('jsonwebtoken');
const jwtConfig = require('../../../../config/jwt_config');
const dateFormat = require('dateformat');
const md5 = require('md5');

exports.login = async (req, res, next)=>{
  try {
    let params = _.cloneDeep(req.body);
    let nowDate = dateFormat("yyyy-mm-dd HH:MM:ss");

    const postData = {};
    //SET DATA TO FETCH WITH CONDITION
    postData.attributes= ['id', 'agent_id', 'parent_user_id', 'role_id', 'head_id', 'username','first_name', 'last_name', 'name', 'gender', 'email', 'mobile', 'allowed_city_ids', 'is_premium','price_range','status'];
    postData.orCond = [{"email": params.username},{"username": params.username}];
    postData.andCond = [{"password": md5(params.password)}];

    await userModel.fetchUser(postData)
        .then(user => {
          
          if(user){
            user = user.toJSON();
            user.user_id = user.id;
            user.user_type_id = 0;
            user.user_type = "";
            user.source_id=9;

              if(user.status == "1"){
                const authUser = { id: user.id,user_id:user.user_id, email: user.email, username: user.username, name: user.name, mobile: user.mobile, role_id: user.role_id,allowed_city_ids:user.allowed_city_ids };
                const token = jwt.sign(authUser, jwtConfig.secret, { expiresIn: jwtConfig.tokenLife });
                const refreshToken = jwt.sign(authUser, jwtConfig.refreshTokenSecret, { expiresIn: jwtConfig.refreshTokenLife})
                ApiController.sendResponse(req, res, 200, res.__('user_login_successfully'), {"user_data": user, "token": token, "refreshToken": refreshToken}, '');

                //SAVE LAST LOGIN AND USER TOKEN
                let saveUserTable = userModel.updateOne(user.id, {"last_loggedIn": nowDate });
                let saveUserToken = userTokenModel.updateOne(user.id, { 'access_token': token}).then((resp) => {
                                      if(!resp[0]){
                                        return userTokenModel.createOne({'user_id':user.id, 'access_token': token});
                                      }
                                    });

                Promise.all([saveUserTable, saveUserToken]).then((results)=>{
                }).catch((err) => {
                  // return next(err); 
                });

              }else{ //USER IS NOT ACTIVE
                ApiController.sendErrorResponse(req, res, res.__("inactive_user"))
              }
              
          }else{
              ApiController.sendErrorResponse(req, res, res.__("invalid_login"))
          }  
        })
        .catch(err => next(err));
  } catch (error) {
    next(error);
  }
  
}

/**
 * Logout API
 */

 exports.logout = async (req, res, next) => {

      try{
        if(req.headers.userid){
          userTokenModel.delete(req.headers.userid); //DELETE USER TOKEN
        }
      }catch(err) {
      }

      ApiController.sendResponse(req, res, 200, res.__('user_logout_successfully'), {}, '');

 }


/**
 * Captcha Create Function
 */
exports.getCaptchaImage = async (req, res, next) => {
 
    var c = new Captcha({
        length:5, // number length
        size:{    // output size
            width: 450,
            height: 200
        }
    });

    try{
      // get base64 image as string
      await c.toBase64(function(err, base64){
        if(err){
          ApiController.sendResponse(req, res, 200, res.__('error_loading_captcha'), {'image': '', 'value': '', 'error': res.__('error_loading_captcha')}, '');
        }else{
          ApiController.sendResponse(req, res, 200, res.__('captcha_load_successfully'), {'image': base64, 'value': Buffer.from(c.value).toString('base64')}, '');
        }
      });
    }catch(err){
      ApiController.sendResponse(req, res, 200, res.__('error_loading_captcha'), {'image': '', 'value': '', 'error': res.__('error_loading_captcha')}, '');
    }

}

exports.getRefreshToken = async (req, res, next) => {
  const postData = req.body;

  try{
    let tokenData = jwt.verify(req.body.refreshToken, jwtConfig.refreshTokenSecret);
    
    if(tokenData.email == postData.authEmail){
        delete tokenData.iat;
        delete tokenData.exp;
        // tokenData.user_id = user.id;
        tokenData.user_type_id = 0;
        tokenData.user_type = "";
        tokenData.source_id=9;

        const token = jwt.sign(tokenData, jwtConfig.secret, { expiresIn: jwtConfig.tokenLife });
        const refreshToken = jwt.sign(tokenData, jwtConfig.refreshTokenSecret, { expiresIn: jwtConfig.refreshTokenLife});

        await userTokenModel.updateOne(tokenData.id, { 'access_token': token});

        ApiController.sendResponse(req, res, 200, res.__('token_refresh_successfully'), {"user_data": tokenData, "token": token, "refreshToken": refreshToken}, '')
    }else{
        ApiController.sendErrorResponse(req, res, res.__("invalid_refresh_token"));
    } 
    
  }catch(err) {
    ApiController.sendErrorResponse(req, res, res.__("invalid_refresh_token"));
  }

}

exports.forgotPassword = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body);
    const postData = {};
    //SET DATA TO FETCH WITH CONDITION
    postData.attributes = ['id', 'email', 'mobile', 'username', 'otp', 'otp_count', 'otp_expiry'];
    postData.orCond = [{"mobile": params.mobile}];
    
    let otp = (Math.floor(1000 + Math.random() * 9999)); //RANDOM 4 DIGITS OTP
        otp = otp.toString().slice(0,4);

    let nowDate = dateFormat((new Date().getTime() + 30*60000), "yyyy-mm-dd HH:MM:ss"); //ADD 30 MINUTES IN TIME
    
    let userData = await userModel.fetchUser(postData); //FETCH USER

    let otpSuccess = true;
          
          if(userData){

            let otpDbDate = new Date(userData.otp_expiry); //CONVERT DB DATE TO DATE OBJECT JAVASCRIPT
            let curDate = new Date(); //CURRENT DATE
            let updateOtp = {}; 

            if(userData.otp_count == 0 && curDate.getTime() > otpDbDate.getTime()){          
              
              updateOtp = {'otp':otp,'otp_expiry':nowDate, 'otp_count': (+userData.otp_count+1)};

            }
            else if((userData.otp_count > 0 && userData.otp_count < 3) && (curDate.getTime() < otpDbDate.getTime() )  ){
              updateOtp = {'otp':otp, 'otp_count': (+userData.otp_count+1)};
            }
            else if((userData.otp_count > 0 && userData.otp_count < 3) && (curDate.getTime() > otpDbDate.getTime() )  ){
              updateOtp = {'otp':otp, 'otp_count': '1','otp_expiry':nowDate};
            }
            else if( userData.otp_count == 3 &&  curDate.getTime() < otpDbDate.getTime()) {
              otpDbDate = new Date(nowDate);            
              updateOtp = {'otp':0,'otp_expiry':nowDate, 'otp_count': '0'};
              otpSuccess = false;
            }
            else if( userData.otp_count == 3 &&  curDate.getTime() > otpDbDate.getTime()) {            
              updateOtp = {'otp':otp,'otp_expiry':nowDate, 'otp_count': '1'};
              otpSuccess = true;
            }
            else{
              otpSuccess = false;
            }

            //UPDATE USER TABLE OTP VALUES
            if(Object.keys(updateOtp).length){
              await userModel.updateOne(userData.id, updateOtp);
            }

            if(otpSuccess){
      
              ApiController.sendResponse(req, res, 200, res.__('otp_sent_successfully'), {otp}, '');

            }else{
              let minutesLeft = (((otpDbDate.getTime() - curDate.getTime()) / 1000) / 60).toFixed(); //GET MINUTS LEFT TO OTHER REQUEST
              ApiController.sendErrorResponse(req, res, res.__("try_after_x_minutes", {'time': minutesLeft}));
              otpSuccess = false;
            }
            
          }else{
            ApiController.sendResponse(req, res, 400, res.__("invalid_username"), {otpMatchPerform:"no"});
          }  

  } catch (error) {
    next(error);
  }
}

exports.getResetPasswordToken = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body);
    const postData = {};
    //SET DATA TO FETCH WITH CONDITION
    postData.attributes = ['id', 'email', 'mobile', 'username', 'otp', 'otp_count', 'otp_expiry'];
    postData.orCond = [{"mobile": params.mobile}];
    // postData.andCond = [{"otp": params.otp}];

    let userData = await userModel.fetchUser(postData); //FETCH USER

    if(userData) {
      let token = md5((Math.floor(1000 + Math.random() * 999999))).slice(0, 30);
      if(userData.otp == params.otp) {
        await userModel.updateOne(userData.id, {'reset_token': token});
        ApiController.sendResponse(req, res, 200, res.__('otp_matched_successfully'), {"reset_token": token}, '');

      }else{
        ApiController.sendResponse(req, res, 400, res.__("otp_not_matched"), {otpMatchPerform:"no"});

      }
    }else{
      ApiController.sendErrorResponse(req, res, res.__("invalid_username"));
    }

  }catch(error) {
    next(error);
  }

}


exports.validateResetToken = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body);
    const postData = {};
    //SET DATA TO FETCH WITH CONDITION
    postData.attributes = ['id', 'email'];
    postData.orCond = [{"reset_token": params.resetToken}];

    let userData = await userModel.fetchUser(postData); //FETCH USER

    if(userData) {
     
      ApiController.sendResponse(req, res, 200, res.__('token_validated'), {}, '');

    }else{
      ApiController.sendErrorResponse(req, res, res.__("invalid_token"));
    }

  }catch(error) {
    next(error);
  }

}

exports.resetPassword = async (req, res, next) => {
  try {
    let params = _.cloneDeep(req.body);
    let nowDate = dateFormat("yyyy-mm-dd HH:MM:ss");

    const postData = {};
    //SET DATA TO FETCH WITH CONDITION
    postData.attributes = ['id', 'email'];
    postData.orCond = [{"reset_token": params.resetToken}];

    let userData = await userModel.fetchUser(postData); //FETCH USER

    if(userData) {
      if((params.password).trim().length >= 6){
        await userModel.updateOne(userData.id, {'otp':0,'otp_count':'0','otp_expiry':nowDate,'reset_token': "", "password": md5(params.password)});
        ApiController.sendResponse(req, res, 200, res.__('password_reset_successfully'), {}, '');
      }else{
        ApiController.sendErrorResponse(req, res, res.__("password_length"));
      }
      

    }else{
      ApiController.sendErrorResponse(req, res, res.__("invalid_token"));
    }

  }catch(error) {
    next(error);
  }

}


