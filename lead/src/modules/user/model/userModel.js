const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const md5 = require('md5');
const crypto = require("../../../lib/crypto");
class UserModel extends Model{
}

UserModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    agent_id: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    parent_user_id: {
        type: dbConfig.Sequelize.NUMBER,
        allowNull: true
    },
    role_id: {
        type: dbConfig.Sequelize.NUMBER,
        allowNull: true
    },
    head_id: {
        type: dbConfig.Sequelize.NUMBER,
        allowNull: true,
        defaultValue: 0
    },
    username: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    password: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    first_name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    last_name: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    gender: {
        type:dbConfig.Sequelize.ENUM('M','F'),
        allowNull: true,
    },
    email: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        validate: {
            isEmail: {
                args: true,
                msg: 'The email you entered is invalid.'
            }
        }
    },
    mobile: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    status: {
        type:dbConfig.Sequelize.ENUM('0', '1','2'),
        allowNull: true,
        defaultValue: '0',
        
    },
    allowed_city_ids: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    price_range: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    is_premium: {
        type: dbConfig.Sequelize.NUMBER,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: false
    },
    added_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_by: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    last_loggedIn: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    allowed_ip_address: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    otp: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0
    },
    otp_expiry: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    otp_count: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue: 0
    },
    reset_token: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true,
        defaultValue: '0',
    }
    
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_USER,
    freezeTableName: true
});


UserModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const user_data = await UserModel.create(data);
            resolve(user_data);
        } catch (error) {
            reject(error)
        }
    })
}

UserModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const user_data = await UserModel.update(data, { where: { id: id } });
            resolve(user_data);
        } catch (error) {
            reject(error)
        }
    })
}


UserModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id){
        dataModel.created_at = new Date().toISOString();
    }
    else{
        dataModel.updated_at = new Date().toISOString();
    }
    if(params.id && (params.hasOwnProperty('status')))
        dataModel.status = params.status;
    else if((!params.id))
        dataModel.status = (params.hasOwnProperty('status')) ? params.status : '1';
    if(params.agent_id)
        dataModel.agent_id = params.agent_id
    if(params.parent_user_id)
        dataModel.parent_user_id = params.parent_user_id;
    if(params.role_id)
        dataModel.role_id = params.role_id;
    if(params.head_id)
        dataModel.head_id = params.head_id;
    if(params.username)
        dataModel.username = params.username;
    if(params.password)
        dataModel.password = md5(params.password);
    if(params.name)
        dataModel.name = params.name;
    if(params.first_name)
        dataModel.first_name = params.first_name;
    if(params.last_name)
        dataModel.last_name = params.last_name;
    if(params.gender)
        dataModel.gender = params.gender;
    if(params.email)
        dataModel.email = params.email;
    if(params.mobile)
        dataModel.mobile = params.mobile;
    if(params.status)
        dataModel.status = params.status;
    if(params.allowed_city_ids)
        dataModel.allowed_city_ids = params.allowed_city_ids;
    if(params.price_range)
        dataModel.price_range = params.price_range;
    if(params.is_premium)
        dataModel.is_premium = params.is_premium;
    if(params.added_by)
        dataModel.added_by = params.added_by;
    if(params.updated_by)
        dataModel.updated_by = params.updated_by;
    return dataModel;
}

UserModel.get = async (_where,pagination) => {

    let whereCond = await UserModel.bindCondition(_where,pagination,true)  //true is used to apply pagination limit
    return new Promise(function (resolve, reject) {
        var sql = "SELECT SQL_CALC_FOUND_ROWS user.id,user.name,user.first_name,user.last_name,user.email,user.username,\n\
        user.mobile,user.gender,user.role_id,user.parent_user_id,user.agent_id,user.head_id,user.allowed_city_ids,user.price_range,\n\
        user.is_premium,user.created_at,user.updated_at,user.added_by,user.updated_by,user.status,user.last_loggedIn,user.allowed_ip_address, role.name as role_name, admin.name as admin_name FROM \n\
        "+UBLMS_USER+" user LEFT JOIN "+UBLMS_ROLES+" as role ON role.id =  user.role_id LEFT JOIN "+UBLMS_USER+" as admin ON admin.id =  user.updated_by "+whereCond;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(async (result) => {
            let totalCount = await UserModel.getRowsCount();
            resolve([result, totalCount]);
        }).catch(function (error) {
            reject(error);
        });
    });
}

UserModel.bindCondition = async (params,pagination,isPaginationApply) => {
    let condition = '',
        sortCond = '';
    let paginationCond =  await UserModel.bindPagination(params,pagination,isPaginationApply);
    if(params && params.hasOwnProperty('status')){
        let status = "user.status='"+params.status+"'";
        condition = (condition == '')? condition + "where "+status : condition+" and "+status 
    }
    if(params && params.id){
        let id = "user.id='"+UserModel.decrypt(params.id)+"'";
        condition = (condition == '') ? condition + "where "+id : condition+" and "+id 
    }

    if(params && params.role_id){
        let role_id = "user.role_id='"+params.role_id+"'";
        condition = (condition == '') ? condition + "where "+role_id : condition+" and "+role_id 
    }else{
        let role_id = "user.role_id != '"+ADMIN_ROLE_ID+"'";
        condition = (condition == '') ? condition + "where "+role_id : condition+" and "+role_id 
    }
    if(params && params.created_at){
        let inputDate = dateFormat(params.created_at, "yyyy-mm-dd");
        let datetCond = "date_format(user.created_at,'%Y-%m-%d') <= '" + inputDate+ "'"
        condition = (condition == '') ? condition + "where "+datetCond : condition+" and "+datetCond 
    }
    if(params && params.name){
        let nameCond = "(user.username like '%" + params.name + "%' or user.name like '%" + params.name + "%' or user.email like '%" + params.name + "%')";
        condition = (condition == '') ? condition + "where "+nameCond : condition+" and "+nameCond 
    }
    if(params && params.sort && params.sort.length){
        let srtType = '',
            srtBy = '',
            finalSort = '';
        _.forEach(params.sort,(srtKey) => {
            srtType = (srtKey.sort_type) ? srtKey.sort_type : 'ASC';
            srtBy = (srtKey.sort_by) ? srtKey.sort_by : '';
            let sortData = srtBy + ' ' + srtType;
            finalSort = (finalSort != '') ? finalSort+', '+sortData : sortData 
        })
        sortCond = (finalSort != '') ? 'ORDER BY '+finalSort : '';
    }else{
        sortCond = 'ORDER BY user.updated_at desc';
    }
    return condition+" "+sortCond+" "+paginationCond;
}

UserModel.bindPagination = (params,pagination,isPaginationApply) => {
    let condition = '';
    if(isPaginationApply){
        let  page_no  = pagination;
        let limit = PAGE_LIMIT;
        let page = (page_no) ? page_no : PAGE_NUMBER;  // DEFAULT_PAGE_NUMBER
        let offset = limit * (page - 1);
        condition = "LIMIT "+offset+","+limit;
    }
    return condition;
}

UserModel.getRowsCount = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT FOUND_ROWS() as total";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result[0].total);
        }).catch(error=>{
            reject(error);
        });
    })
}

UserModel.getAll = async () => {
    return await UserModel.findAll();
}

UserModel.getOne = async _where => {
    return await UserModel.findOne({where: _where});
}


UserModel.fetchUser = async data => {
    let attributes = data.attributes || [];
    let orCond     = data.orCond || [];
    let andCond    = data.andCond || [];

    return await UserModel.findOne({
        attributes,
        where: {
            [Op.or]: orCond,
            [Op.and]: andCond
        }
    });
}
    
UserModel.encrypt = (data) => {
    if(Array.isArray(data)) data = data.map(v => {
        if(v && v.id != null) v.user_id_hash = crypto.encode(v.id);
        return v;
    })
    else if (data && data.id) data.user_id_hash = crypto.encode(data.id);
    return data;
}

UserModel.decrypt = (hashId) => {
    let id = '';
    id = (typeof hashId == 'string') ? crypto.decode(hashId) : hashId;
    return id;
}

UserModel.getTLnames = async ()=>{
    return new Promise((resolve, reject)=>{
        let sql = "SELECT id,name from "+UBLMS_USER+" WHERE role_id = 2 and status='1' order by email ASC";
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result);
        }).catch(error=>{
            reject(error);
        });
    })
}


module.exports = UserModel;