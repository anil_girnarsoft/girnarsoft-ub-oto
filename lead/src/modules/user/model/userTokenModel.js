const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const dateFormat = require('dateformat');
const md5 = require('md5');
const crypto = require("../../../lib/crypto");
class UserTokenModel extends Model{
}

UserTokenModel.init({
    id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    user_id: {
        type: dbConfig.Sequelize.NUMBER,
        allowNull: true
    },
    access_token: {
        type: dbConfig.Sequelize.STRING,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_USER_TOKEN,
    freezeTableName: true
});


UserTokenModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const user_data = await UserTokenModel.create(data);
            resolve(user_data);
        } catch (error) {
            reject(error)
        }
    })
}

UserTokenModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const user_data = await UserTokenModel.update(data, { where: { user_id: id } });
            resolve(user_data);
        } catch (error) {
            reject(error)
        }
    })
}

UserTokenModel.delete = async (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            const userToken = await UserTokenModel.destroy( {where: { "user_id": userId }} );
            resolve(userToken);
        } catch (error) {
            reject(error)
        }
    })
}


UserTokenModel.getModel = (params) => {
    let dataModel = {};
    if(!params.id){
        dataModel.created_at = new Date().toISOString();
    }
    else{
        dataModel.updated_at = new Date().toISOString();
    }
    
    if(params.access_token)
        dataModel.access_token = params.access_token;
    if(params.user_id)
        dataModel.user_id = params.user_id;
    return dataModel;
}

UserTokenModel.getOne = async _where => {
    return await UserTokenModel.findOne({where: _where});
}


module.exports = UserTokenModel;