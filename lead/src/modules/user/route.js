const express = require('express');
const router = express.Router();
const schemas = require('./validation_schema');
const validation = require(MIDDLEWARE_PATH + 'validation');
const UserController = require('./controller/userController');
const AuthController = require('./controller/authController');
const NotificationController = require('../notifications/controller/NotificationController');

router.post('/listUser', UserController.getUserList);
router.post('/saveUser',validation(schemas.createUser, 'body'), UserController.saveUser);
router.post('/editUser',validation(schemas.updateUser, 'body'),UserController.saveUser);
router.post('/updateStatus',validation(schemas.updateStatus, 'body'),UserController.updateStatus);
router.post('/listAllUser',UserController.getAllUserList);
router.post('/getUser',UserController.getUser);

router.get('/getCaptchaImage', AuthController.getCaptchaImage);
router.post('/login',validation(schemas.login, 'body'), AuthController.login);
router.post('/logout', AuthController.logout);
router.post('/getRefreshToken', AuthController.getRefreshToken);
router.post('/forgotPassword',validation(schemas.forgotPassword, 'body'), AuthController.forgotPassword);
router.post('/resendOtp',validation(schemas.forgotPassword, 'body'), AuthController.forgotPassword);
router.post('/getResetPasswordToken',validation(schemas.getResetPasswordToken, 'body'), AuthController.getResetPasswordToken);
router.post('/validateResetToken', AuthController.validateResetToken);
router.post('/resetPassword',validation(schemas.resetPassword, 'body'), AuthController.resetPassword);
router.get('/getSiteSettings', UserController.getSiteSettings);
router.get('/get_tl_list', UserController.getTlList);
router.get('/getNotificationDetail', NotificationController.getNotificationDetail);
router.post('/updateRescheduledDate', NotificationController.updateRescheduledDate);

module.exports = router;