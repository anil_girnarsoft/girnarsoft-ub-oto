const Joi = require('@hapi/joi')

module.exports = {
    createUser: Joi.object().keys({
        agent_id: Joi.number().optional(),
        parent_user_id: Joi.number().optional(),
        role_id: Joi.number().optional(),
        head_id: Joi.number().optional(),
        username: Joi.string().optional(),
        password: Joi.string().optional().min(6),
        name: Joi.string().optional(),
        first_name: Joi.string().optional(),
        last_name: Joi.string().optional(),
        gender: Joi.string().valid('M', 'F').optional(),
        email: Joi.string().optional(),
        mobile: Joi.number().optional(),
        status: Joi.string().valid('0', '1','2').optional(),
        allowed_city_ids: Joi.string().label('City').optional(),
        price_range: Joi.string().label('Price Range').optional(),
        is_premium: Joi.number().optional()
    }),
    updateUser: Joi.object().keys({
        id: Joi.number().required(),
        agent_id: Joi.number().optional(),
        parent_user_id: Joi.number().optional(),
        role_id: Joi.number().optional(),
        head_id: Joi.number().optional(),
        username: Joi.string().optional(),
        password: Joi.string().optional().min(6),
        name: Joi.string().optional(),
        first_name: Joi.string().optional(),
        last_name: Joi.string().optional(),
        gender: Joi.string().valid('M', 'F').optional(),
        email: Joi.string().optional(),
        mobile: Joi.number().optional(),
        status: Joi.string().valid('0', '1','2').optional(),
        allowed_city_ids: Joi.string().label('City').optional(),
        price_range: Joi.string().label('Price Range').optional(),
        is_premium: Joi.number().optional()
    }),
    updateStatus: Joi.object().keys({
        id: Joi.number().required(),
        status: Joi.string().valid('0', '1','2').required(),
    }),
    getList: Joi.object().keys({
        id: Joi.number().required(),
        _with: Joi.array().items(Joi.string().required()).optional()
    }).unknown(true),

    login: Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required(),
    }),

    forgotPassword: Joi.object().keys({
        mobile: Joi.number().required(),
        isdCode: Joi.string().required(),
    }),

    resendOtp: Joi.object().keys({
        mobile: Joi.number().required(),
        isdCode: Joi.string().required(),
    }),

    getResetPasswordToken: Joi.object().keys({
        mobile: Joi.number().required(),
        isdCode: Joi.string().required(),
        otp: Joi.number().required()
    }),

    resetPassword: Joi.object().keys({
        password: Joi.string().required().min(6),
        confirm_password: Joi.ref('password'),
        resetToken: Joi.string().required()
    })
}