const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const CommonHelper = require('../../../helpers/CommonHelper');
let _this = this;

class DealerBoostModel extends Model{
}

DealerBoostModel.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    dealer_id:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    ftd_lead_count:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    boost_status:{
        type: dbConfig.Sequelize.ENUM('0','1','2'),
        allowNull: false,
        defaultValue:'0'
    },
    boost_leads_count:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    end_date:{
        type: dbConfig.Sequelize.DATE,
        allowNull: true
    },
    priority:{
        type: dbConfig.Sequelize.ENUM('1','2','3'),
        allowNull: false,
        defaultValue:'1'
    },
    is_boost:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    },
    is_front_boost:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    },
    boost_active:{
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    },
    is_sent_leads:{
        type: dbConfig.Sequelize.ENUM('1','2'),
        allowNull: false,
        defaultValue:'1'
    },
    achieved_ftd:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    after_boost_ftd:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    boost_target_ftd:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        defaultValue:0
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    deboost_status: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_DEALER_BOOST,
    freezeTableName: true
});


DealerBoostModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await DealerBoostModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

DealerBoostModel.updateOne = async (id, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await DealerBoostModel.update(data, { where: { id: id } });
            
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

DealerBoostModel.getDealerBoostInfo = async(_where) =>{
    return new Promise((resolve, reject)=>{
        let conds = ' where 1=1';
        if(_where.dealer_id){
            conds += "  and dealer_id='" +_where.dealer_id + "'";
        }
        if(typeof _where.boostActive != 'undefined' && _where.boostActive != '' && _where.boostActive>=0){
            conds += " and boost_active='" +_where.boostActive + "'";
        }
        if(typeof _where.fulfillmentStatus != 'undefined' && _where.fulfillmentStatus != '' && _where.fulfillmentStatus>=0){
            conds += " and boost_status='" +_where.fulfillmentStatus + "'";
        }
        if(typeof _where.dealer != 'undefined' && _where.dealer>0){
            conds += " and dealer_id = '" +_where.dealer + "'";
        }
        if(typeof _where.dealerPriorityType != 'undefined' && _where.dealerPriorityType.length > 0){
            conds += " and priority IN (" +_where.dealerPriorityType.join() + ")";
        }

        let sql = "select * from "+UBLMS_DEALER_BOOST+ conds;
        
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}


DealerBoostModel.mapDealerBoostsWithDealers = async(dealerIds=[], dealerData) =>{
    return new Promise(async (resolve, reject)=>{
        let result = await DealerBoostModel.getDealerBoostInfo(dealerIds);
       

        // let sql = "select * from "+UBLMS_DEALER_BOOST;
        // AppModel.dbObj.sequelize
        // .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        // .then(result=>{
            // findDealers
            let mappedDealers = [];
            if(result.length){
                mappedDealers = dealerData && dealerData.map(dealer=>{
               
                    let findBoost = _.find(result, {dealer_id: dealer.id});
                    if(findBoost){
                        dealer.ftd_lead_count = findBoost.ftd_lead_count; 
                        dealer.boost_status = findBoost.boost_status; 
                        dealer.boost_leads_count = findBoost.boost_leads_count; 
                        dealer.end_date = dateFormat(findBoost.end_date, 'yyyy-mm-dd'); 
                        dealer.priority = findBoost.priority; 
                        dealer.priorityObj = _.find(CommonHelper.dealerPriority(), {"value": dealer.priority}); 
    
                        dealer.is_boost = findBoost.is_boost; 
                        dealer.is_front_boost = findBoost.is_front_boost; 
                        dealer.boost_active = findBoost.boost_active; 
                        dealer.boost_active_obj = _.find(CommonHelper.dealerBoostActive(), {"value": dealer.boost_active}); 
                        dealer.achieved_ftd = findBoost.achieved_ftd; 
                        dealer.after_boost_ftd = findBoost.after_boost_ftd; 
                        dealer.boost_target_ftd = findBoost.boost_target_ftd; 
                        dealer.is_sent_leads = findBoost.is_sent_leads; 
                        dealer.dealer_id = findBoost.dealer_id; 
                        dealer.is_sent_leads_obj = _.find(CommonHelper.sendBackendLeads(), {"value": findBoost.is_sent_leads}); 
                    }
                    return dealer;
                });
            }
            

            resolve(mappedDealers || []);
    })
}




module.exports = DealerBoostModel;