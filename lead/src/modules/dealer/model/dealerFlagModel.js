const _ = require("lodash");
const dbConfig = require(CONFIG_PATH + 'db.config');
const Model = dbConfig.Sequelize.Model;
const sequelize = dbConfig.sequelize;
const Op = dbConfig.Sequelize.Op;
var AppModel = require(COMMON_MODEL + 'appModel');
const dateFormat = require('dateformat');
const crypto = require("../../../lib/crypto");
const CommonHelper = require('../../../helpers/CommonHelper');

class DealerFlagModel extends Model{
}

DealerFlagModel.init({
    id: {
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    carId:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    flagId:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    added_by:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: false
    },
    updated_by:{
        type: dbConfig.Sequelize.INTEGER,
        allowNull: true
    },
    created_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    updated_at: {
        type: dbConfig.Sequelize.DATE,
        allowNull: true,
    },
    status: {
        type: dbConfig.Sequelize.ENUM('0','1'),
        allowNull: false,
        defaultValue:'0'
    }
},{
    sequelize,
    timestamps: false,
    modelName: UBLMS_DEALER_FLAG,
    freezeTableName: true
});


DealerFlagModel.createOne = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await DealerFlagModel.create(data);
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

DealerFlagModel.updateOne = async (_where, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            data = data || {};
            const role_data = await DealerFlagModel.update(data, { where: _where });
            
            resolve(role_data);
        } catch (error) {
            reject(error)
        }
    })
}

DealerFlagModel.getDealerFlagInfo = async(carId,flagId) =>{

    return new Promise((resolve, reject)=>{
        let sql = "Select id from " + UBLMS_DEALER_FLAG + "   where carId = '" +carId + "' and flagId = '"+flagId+ "'" ;
        AppModel.dbObj.sequelize
        .query(sql, { raw: true, type: AppModel.dbObj.Sequelize.QueryTypes.SELECT })
        .then(result=>{
            resolve(result || []);
        }).catch(error=>{
            reject(error);
        });
    })
}





module.exports = DealerFlagModel;