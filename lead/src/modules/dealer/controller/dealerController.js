const _ = require("lodash");
const ApiController = require(COMMON_CONTROLLER + 'ApiController');
const DealerService    = require(COMMON_MODEL + 'dealerService');
const dealerFlagModel = require('../model/dealerFlagModel');
const DealerBoostModel = require('../model/dealerBoostModel');
const { param } = require("express-validator");

exports.getDealerDetailsByHash = async (req, res, next) => {
  try{
    let params = req.body;
    let result = await DealerService.getDealerDetailsByHash(req,params);
    ApiController.sendResponse(req, res, 200, res.__(''), result && result['data'] || [], '');
    
  }catch(error){
    next(error);
  }
}

exports.getDealerList = async (req, res, next) => {
  try{
    let params = req.body;
    let result = [];
    let dealerBoostSettings = [];
    let findAllDealersData = [];
    let filters = false;
    let dealerIds = [];

    //FIND ALL DEALERS
    if(params.page_number){
      // req['query']['page_no'] = params['page_number'];
      // req['query']['record_per_page'] = PAGE_LIMIT;
    }

    if(params.city_ids && params.city_ids.length){
      req['query']['filter'] = {};
      req['query']['filter']['city_ids'] = params.city_ids;
    }
    
    let findAllDealers = await DealerService.getDealerList(req);    
    //CHECK FOR FILTERS
    if((typeof params.boostActive != 'undefined' && params.boostActive!="") || ( typeof params.fulfillmentStatus != 'undefined' && params.fulfillmentStatus!="" ) || ( params.dealerPriorityType != 'undefined' && params.dealerPriorityType)){
      filters = true;      
      dealerBoostSettings = await DealerBoostModel.getDealerBoostInfo(params);
    }
    
    //IF FILTERS ARE APPLIED THEN FIND FROM DEALER BOOST TABLE
    findAllDealersData = findAllDealers['data'] || [];
    //console.log('findAllDealersData from boost---------------------------', findAllDealersData);
    //GET DEALERS FROM FILTER DEALER BOOST TABLE
    if(dealerBoostSettings.length && filters){
      dealerIds = dealerBoostSettings.map(dBoost=> dBoost.dealer_id); // GET DEALER IDs
    }else if(!filters && typeof params.dealer != 'undefined' && params.dealer >0){ //IF ONLY DEALER SELECTED FROM FILTERS
      dealerIds = [params.dealer];
    }else if(filters && typeof params.dealer != 'undefined' && params.dealer >0 && dealerBoostSettings.length){ //IF DEALER AND OTHER VALUES SELECTED FROM FILTERS
      dealerIds = dealerBoostSettings.map(dBoost=> dBoost.dealer_id);
    }else if(!dealerBoostSettings.length && filters){
      dealerIds = [];
    }
        
    if(dealerIds.length || filters){
      findAllDealersData  = findAllDealers['data'].filter((el, key)=>{ if(dealerIds.includes(el.id))  return el ; });
    }else{
      findAllDealersData = findAllDealersData;
    }
   
    //console.log('findAllDealersData**************************', findAllDealersData);
    //MAP DEALERS WITH DEALER BOOST
    result = await DealerBoostModel.mapDealerBoostsWithDealers(dealerIds, findAllDealersData || []);
    //if dealer is not available at boost then fetch dealer from dealer service with dealer id
    if(result.length == 0){
      let dealers = await DealerService.getDealerDetailsByIds(req, {dealer_id: [params.dealer]});      
      result = dealers['data'];      
    }
    
    //FOR CALLBACK FUNCTION RETURN RESULT
    if(params['cb_function']){ 
      return result || [];
    }else{
      if(params.page_number>1){
        ApiController.sendResponse(req, res, 200, res.__(''), [], '');
      }else{
        ApiController.sendResponse(req, res, 200, res.__(''), result || [], '');
      }
    }
    
  }catch(error){
    next(error);
  }
}

exports.getDealerDetailsById = async (req, res, next) => {
  try{
    let params = req.body;
    let result = await DealerService.getDealerDetailsByDealerId(req,params);
    ApiController.sendResponse(req, res, 200, res.__(''), result && result['data'] || {}, '');
    
  }catch(error){
    next(error);
  }
}

exports.updateDealerBoost = async (req, res, next) => {
  try{
    let params = req.body;
    let updateDealer = {};
    let dealerIsBoost = false;
    let dealer_id = params.dealer_id || params.id;
    let findDealerBoost = await DealerBoostModel.getDealerBoostInfo({dealer_id: dealer_id});
    updateDealer.priority = params.priorityObj ? params.priorityObj.value : (findDealerBoost.lenght ? findDealerBoost[0]['priority'] : '1'); 
    updateDealer.boost_active = params.boost_active_obj ? params.boost_active_obj.value : (findDealerBoost.lenght ? findDealerBoost[0]['boost_active'] : '0'); 
    updateDealer.boost_target_ftd = (typeof params.boost_target_ftd !== 'undefined') ? parseInt(params.boost_target_ftd) : (findDealerBoost.lenght ? findDealerBoost[0]['boost_target_ftd'] : 0); 
    updateDealer.after_boost_ftd = (typeof params.after_boost_ftd !== 'undefined') ? parseInt(params.after_boost_ftd) : (findDealerBoost.lenght ? findDealerBoost[0]['after_boost_ftd'] : 0); 
    updateDealer.end_date = params.end_date ? params.end_date : (findDealerBoost.lenght ? findDealerBoost[0]['end_date'] : null);
    updateDealer.is_sent_leads = params.is_sent_leads_obj ? params.is_sent_leads_obj.value : (findDealerBoost.lenght ? findDealerBoost[0]['is_sent_leads'] : '1');
    updateDealer.is_boost = (findDealerBoost.lenght ? findDealerBoost[0]['is_boost'] : '0');
    if(typeof params.is_boost !== 'undefined') {
      dealerIsBoost = true;
      updateDealer.is_boost = (params.is_boost).toString();
    } 
    updateDealer.updated_at = new Date().toISOString();
    updateDealer.dealer_id = dealer_id;
    if(findDealerBoost.length){
      let result = await DealerBoostModel.updateOne(findDealerBoost[0]['id'],updateDealer);
    }else{
      updateDealer.updated_at = new Date().toISOString();
      createBoost = await DealerBoostModel.createOne(updateDealer);
    }
    let msg = 'data_saved_successfully';
    // if(dealerIsBoost){
    //   msg = (updateDealer.is_boost === '1') ? 'dealer_boostup_successfully' : 'dealer_boostdown_successfully';
    // }
    ApiController.sendResponse(req, res, 200, res.__(msg), [], '');
  }catch(error){
    next(error);
  }
}

exports.saveDealerFlag = async (req, res, next) => {
  try{
    let params = _.cloneDeep(req.body);
    let carId = params.car_id;
    let dealerFlag = params.dealer_flag;
    const authUser = _.get(req, 'user'); 
    // Initially update the status 0
    let updateObj = {
      'updated_by' : authUser['user_id'],
      'updated_at' : new Date().toISOString(),
      'status'     : '0'
    }
    let resp = await dealerFlagModel.updateOne({'carId':carId},updateObj);
    for(let flag of dealerFlag){
      let dealerFlagInfo = await dealerFlagModel.getDealerFlagInfo(carId,flag);
      if(dealerFlagInfo.length){
        
        let updateObj = {
          'updated_by' : authUser['user_id'],
          'updated_at' : new Date().toISOString(),
          'status'     : '1'
        }
        await dealerFlagModel.updateOne({'id':dealerFlagInfo[0].id},updateObj)
      }else{
        let insertObj  = {
          'carId' : carId,
          'flagId' : flag,
          'added_by' : authUser['user_id'],
          'created_at' : new Date().toISOString(),
          'status'     : '1'
        };
        await dealerFlagModel.createOne(insertObj)
      }
    }
    ApiController.sendSuccessResponse(req, res, {},'Flags Are Updated Successfully');
    
  }catch(error){
    next(error);
  }
}


