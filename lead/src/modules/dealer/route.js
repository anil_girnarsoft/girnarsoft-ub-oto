const express = require('express');
const router = express.Router();
const DealerController = require('./controller/dealerController');

router.post('/listDealers', DealerController.getDealerList);
router.post('/getDealerDetailsByHash', DealerController.getDealerDetailsByHash);
router.post('/getDealerDetailsById', DealerController.getDealerDetailsById);
router.post('/updateDealerBoost', DealerController.updateDealerBoost);
router.post('/saveDealerFlag', DealerController.saveDealerFlag);

module.exports = router;