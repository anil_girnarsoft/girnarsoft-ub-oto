const http = require('http');
require('./constant');
require(CONFIG_PATH + 'db.constants');
const app = require('./app');
const CONFIG = require(CONFIG_PATH + 'config');
const crons = require("./cron/process");

const port = CONFIG.local_port;
const server = http.createServer(app);

server.listen(port, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Server running on http://%s:%s", host, port);
});
// server.on('listening',function(){
//     crons.startCron("syncLeads",'*/30 * * * * *'); /// run every second
// })