const ENV = process.env;
let  COUNTRY_CODE = ENV.COUNTRY_CODE || 'id';
const env_js = ENV.NODE_ENV ? require(`./${COUNTRY_CODE}/${ENV.NODE_ENV}_config.js`) : {};
let def_val = {
    local_port: ENV.SERVER_PORT || '7010',
    module_path: './modules/',
    dbConf: {
        db_host: ENV.DB_HOST || '172.16.105.34',
        db_name: ENV.DB_NAME || 'international_ub_oto',
        db_user: ENV.DB_USER || 'carbay_id_stage',
        db_pass: ENV.DB_PASS || 'Orxs@0^W^gMZ',
        db_port: ENV.DB_PORT || '3306',
        conn_type: ENV.DB_CONN_TYPE || 'mysql',
        db_conn_query: ENV.DB_CONN_QUERY || '100',
        db_timezone: ENV.DB_TIMEZONE || '+05:30',
        slave_db_host: ENV.DB_HOST_SLAVE || '172.16.105.34',
        slave_db_name: ENV.DB_NAME_SLAVE || 'international_ub_oto',
        slave_db_user: ENV.DB_USER_SLAVE || 'carbay_id_stage',
        slave_db_pass: ENV.DB_PASS_SLAVE || 'Orxs@0^W^gMZ',
        slave_db_port: ENV.DB_PORT_SLAVE || '3306',
        MONGODB_URL: ENV.MONGODB_URL || "mongodb://localhost:27017/international_ub_oto",
    },
    CRYPTO_KEY: ENV.CRYPTO_KEY || "U*oDwrIU!B#QX37%#24",
    EMAIL_NOTIFICATIONS: ENV.EMAIL_NOTIFICATIONS || false,
    HOST_NAME_CARMUDI : ENV.HOST_NAME_CARMUDI || "https://www.carmudi.com.ph/",
    HOST_TEXT_CARMUDI : ENV.HOST_TEXT_CARMUDI || "carmudi.com.ph",
    CARMUDI_HEADER_LOGO: ENV.CARMUDI_HEADER_LOGO || '',
    CARMUDI_SMALL_LOGO: ENV.CARMUDI_SMALL_LOGO || '',
    CARMUDI_FACEBOOK_IMG: ENV.CARMUDI_FACEBOOK_IMG || '',
    CARMUDI_TWITTER_IMG: ENV.CARMUDI_TWITTER_IMG || '',
    CARMUDI_INSTAGRAM_IMG: ENV.CARMUDI_INSTAGRAM_IMG || '',
    CARMUDI_LINKEDIN_IMG: ENV.CARMUDI_LINKEDIN_IMG || '',
    ZIGWHEELS_HEADER_LOGO: ENV.ZIGWHEELS_HEADER_LOGO || '',
    ZIGWHEELS_SMALL_LOGO: ENV.ZIGWHEELS_SMALL_LOGO || '',
    ZIGWHEELS_FACEBOOK_IMG: ENV.ZIGWHEELS_FACEBOOK_IMG || '',
    ZIGWHEELS_TWITTER_IMG: ENV.ZIGWHEELS_TWITTER_IMG || '',
    ZIGWHEELS_INSTAGRAM_IMG: ENV.ZIGWHEELS_INSTAGRAM_IMG || '',
    ZIGWHEELS_LINKEDIN_IMG: ENV.ZIGWHEELS_LINKEDIN_IMG || '',
    SHOW_L1: ENV.SHOW_L1 || false,
    CITY_FOR_REPORT: ENV.CITY_FOR_REPORT || [],
    LEAD_REPORT_ID: ENV.LEAD_REPORT_ID || '',
    LEAD_REPORT_TITLE: ENV.LEAD_REPORT_TITLE || '',
    REPORT_SHEET_CREDENTIAL: ENV.REPORT_SHEET_CREDENTIAL || '',
    DEALER_REPORT_COLUMN_HEAD: ENV.DEALER_REPORT_COLUMN_HEAD || '',
    DEALER_REPORT_SHEET_ID: ENV.DEALER_REPORT_SHEET_ID || '',
    DEALER_REPORT_SHEET_TITLE: ENV.DEALER_REPORT_SHEET_TITLE || '',
    DIALSHREE_ADDAPI_HOST: ENV.DIALSHREE_ADDAPI_HOST || '',
    PROCESS_DIALSHREE_LEAD_LIMIT: ENV.PROCESS_DIALSHREE_LEAD_LIMIT || '',
    WA_CHANNEL_TYPE: ENV.WA_CHANNEL_TYPE,
    COMPANY_HANDLE: ENV.COMPANY_HANDLE,
    WA_NOTIFICATION_ENABLE: ENV.WA_NOTIFICATION_ENABLE,
    ISD_CODE: ENV.ISD_CODE,
    FRONTEND_HOST: ENV.FRONTEND_HOST

}

const fbParams = {
    FB_GRAPH_URL: "https://graph.facebook.com/v8.0/",
    FACEBOOK_LIMIT: '100',
    REFRESH_TOKEN_URL: "https://graph.facebook.com/v8.0/oauth/access_token"
}

const finalObj = Object.keys(env_js).length ? env_js : def_val;

module.exports =  {...finalObj, ...fbParams};