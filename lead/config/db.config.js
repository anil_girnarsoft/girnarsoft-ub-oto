const { dbConf } = require("./config");
const Sequelize = require('sequelize');
const mongoose = require("mongoose");
//const { logger } = require("./../src/middleware/logger");
const Schema = mongoose.Schema;

Object.freeze(dbConf);
const logging = (['local', 'beta'].indexOf(process.env.NODE_ENV) == -1)
 // ? (msg) => logger.info(msg, { "sql": true }) : console.log;

const sequelize = new Sequelize(dbConf.db_name, dbConf.db_user, dbConf.db_pass, {
    host: dbConf.db_host,
    dialect: dbConf.conn_type,
    port: dbConf.db_port,
    maxConcurrentQueries: dbConf.db_conn_query,
    pool: {
        max: 5,
        min: 0,
        idle: 300000,
        acquire: 300000
    },
    // logging: logging,
    //timezone: dbConf.db_timezone
});

let mongoDB = dbConf.MONGODB_URL;
mongoose.connect(mongoDB, {
    useUnifiedTopology: true,
    useNewUrlParser: true
});
mongoose.Promise = global.Promise;
const mongoosedb = mongoose.connection;

const connection = {};
connection.Sequelize = Sequelize;
connection.sequelize = sequelize;

// connection.mongoose = mongoose;
connection.mongoosedb = mongoosedb;

connection.schema = Schema;

module.exports = connection;