module.exports = {
    language: 'en',
    header: {
            menuLinks: {
                //"dashboard": "/leads-list",
                "lead.leads": {"lead.leads": {
                                "/leads-list": "lead.leadFinder",
                                "/add-conversion": "lead.addConversion",
                                "/add-leads": "lead.addLeads",
                                //"/blocking-leads": "lead.blockingLeads",
                                //"/bulk-upload": "lead.bulkUpload",
                                "/conversion-panel": "lead.conversionPanel"
                            }
                    },
                "dealer.dealers": {"dealer.dealers": {
                            "/lead-control-panel": "dealer.leadControlPanel",
                            "/inventory": "dealer.inventory",
                        }
                    },
                "report.reports": {"report.reports": {
                        "report.dealerReports": {"/dealer-delivery-report": "report.dealerDeliveryReport"},
                        "report.leadReports": {
                            "/detailed-sales-funnel-report": "report.detailedSalesFunnelReport",
                            //"/Lead-Snapshot-Report": "report.leadSnapshotReport",
                            //"/Location-Wise-Lead": "report.locationwiseLeadCountReport",
                            //"/Conversion-Confirmation-Report": "report.conversionConfirmationReportControl",
                            //"/Google-Campaign-Report": "report.googleCampaignWiseReport"
                        },
                        "report.consultantReports": {
                            "/l1-Calling-Report": "report.l1CallingReport",    
                            //"/l2-Calling-Report": "report.l2CallingReport",
                            //"/Walk-In-Tracking-Report": "report.walkingTrackingReport",    
                        },
                        //"report.others": {
                            //"/Walkin-Feedback-Report": "report.walkinFeedBackReport",
                            //"/Communication-Details": "report.communicationDetails"
                        //}
                    }
                },

                "admin.administration": {"admin.administration": {
                                        "admin.category": {
                                            "/list-category": "admin.view",
                                            "/add-category": "admin.add",
                                        },
                                        "admin.sourceType": {
                                            "/list-sourceType": "admin.view",
                                            "/add-sourceType": "admin.add",
                                        },
                                        "admin.source": {
                                            "/list-source": "admin.view",
                                            "/add-source": "admin.add",
                                        },
                                        "admin.subSource": {
                                            "/list-sub-source": "admin.view",
                                            "/add-sub-source": "admin.add",
                                        },
                                        "admin.role": {
                                            "/list-role": "admin.view",
                                            "/add-role": "admin.add",
                                        },
                                        "admin.privilege": {
                                            "/list-privilege": "admin.view",
                                            "/add-privilege": "admin.add",
                                        },
                                        "admin.user": {
                                            "/list-user": "admin.view",
                                            "/add-user": "admin.add",
                                        },
                                        "admin.city": {
                                            "/list-city": "admin.view",
                                            // "/add-city": "admin.add",
                                        },
                                        "admin.cluster": {
                                            "/list-cluster": "admin.view",
                                            "/add-cluster": "admin.add",
                                        }
                                    }
                    }
                
            }
    },

    footer: {
            copyright: "copyright@carmudi.com.ph",
            phone: "9876543210",
            email: "support@carmudi.com.ph"
    },
    conversionDeclineReason: [
        {value: 'Dealer Declined', label: 'Dealer Declined'},
        {value: 'Conversion done through CarTrade/CarWale', label: 'Conversion done through CarTrade/CarWale'},
        {value: 'Conversion done through Olx', label: 'Conversion done through Olx'},
        {value: 'Loan Rejected', label: 'Loan Rejected'},
        {value: 'Duplicate lead', label: 'Duplicate lead'},
        {value: 'Other', label: 'Other'},
    ],

    conversionPanelStatus: [
        { value: '0', label: 'Pending'},
        { value: '1', label: 'Confirmed'},
        { value: '2', label: 'Denied'},
        { value: '3', label: 'Given Token'}
    ],

    mobileFormat: {
            "isdCode": "+63",
            "isdCodeWithoutSign": "63",
            "startsFrom": 9,
            "minLength": 8,
            "maxLength": 12
    }

}