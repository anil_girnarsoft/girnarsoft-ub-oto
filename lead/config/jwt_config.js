const ENV = process.env;
module.exports = {
    secret: ENV.JWT_SECRET || "international_cloud",
    refreshTokenSecret: ENV.JWT_SECRET || "international_cloud",
    tokenLife: '1d',
    refreshTokenLife: '2d'
}