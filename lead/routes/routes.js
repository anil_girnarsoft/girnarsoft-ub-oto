const express      = require('express');
const router       = express.Router();

var categoryRoute           = require(MODULE_PATH+'category/route');
var sourceTypeRoute         = require(MODULE_PATH+'source/route');
var roleRoute               = require(MODULE_PATH+'role/route');
var userRoute               = require(MODULE_PATH+'user/route');
var privilegeRoute          = require(MODULE_PATH+'privilege/route');
var cityRoute               = require(MODULE_PATH+'city/route');
var clusterRoute            = require(MODULE_PATH+'cluster/route');
var leadRoute               = require(MODULE_PATH+'lead/route');
var statusRoute             = require(MODULE_PATH+'status/route');
var carMakeModelRoute       = require(MODULE_PATH+'car-make-model/route');
var customerRoute           = require(MODULE_PATH+'customer/route');
var dealerRoute             = require(MODULE_PATH+'dealer/route');
var inventoryRoute          = require(MODULE_PATH+'inventory/route');
var reportRoute             = require(MODULE_PATH+'report/route');
const cronJob               = require(`${MODULE_PATH}cron-job/route`);
const whatsappRoute         = require(MODULE_PATH+'notifications/route');
const NotificationsRoute    = require(MODULE_PATH+'notifications/route');
router.use(['/health', '/metrics'], (req, res) => res.send("OK"));

router.use('/category',categoryRoute);
router.use('/source',sourceTypeRoute);
router.use('/role',roleRoute);
router.use('/user',userRoute);
router.use('/privilege',privilegeRoute);
router.use('/city',cityRoute);
router.use('/cluster',clusterRoute);
router.use('/lead',leadRoute);
router.use('/status',statusRoute);
router.use('/carMake',carMakeModelRoute);
router.use('/customer',customerRoute);
router.use('/dealer',dealerRoute);
router.use('/inventory',inventoryRoute);
router.use('/report',reportRoute);
router.use('/cronjob',cronJob);
router.use('/mobileNotifications',whatsappRoute);
router.use('/notification',NotificationsRoute);

// //////////////////// CROn testing /////////////////
// const cronProcess= require("../cron/processLeadCron");
//router.get("/runCron",cronProcess.processLeadTest);
const cronProcessToUb= require("../cron/processLeadToUbCron");
router.get("/runCron", cronProcessToUb.processLeadToUb);
module.exports = router;
