import React from 'react';

class InputField extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        active: (props.locked && props.active) || false,
        value: props.value || "",
        id: props.id || "",
        name: props.name || "",
        type: props.type || "type",
        error: props.error || "",
        label: props.label || "",
        maxLength:props.maxLength || ""
      };
    }
  
    changeValue = (event) => {
      const value = event.target.value;
      this.setState({ value, error: "" });
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(event);
      }
    }

    // componentWillReceiveProps(nextProps) {
    //     this.setState({ 
    //         error: nextProps.error,
    //         required: nextProps.required,
    //         value: nextProps.value
    //     });
    // }

    static getDerivedStateFromProps = (nextProps, prevState) => {
      let updatedStateData = {
          error: nextProps.error,
          required: nextProps.required,
          value: nextProps.value
      };

      return updatedStateData;
  }
  
    onKeyPress = (event) => {
      
      this.props.onKeyPress && this.props.onKeyPress(event)
      if (event.which === 13) {
        // this.setState({ value: this.props.predicted });

      }
    }
  
    render() {
      const { active, value, error, label, id, type, name, required,maxLength } = this.state;
      const { predicted, locked } = this.props;
      let fieldClassName = `form-field ${(locked ? active : active || value) &&
        "active"} ${locked && !active && "locked"}`;
      if(error) fieldClassName += ` field-error`
  
      return (
        <div className={fieldClassName}>
          {active &&
            value &&
            predicted &&
            predicted.includes(value) && <p className="predicted">{predicted}</p>}
            {/* <label htmlFor={id}>{label}</label> */}
            <label htmlFor={id}>{label} {required && <span className="required" style={{ color: 'red' }}>*</span>}</label>
          <input
            id={id}
            className="form-control"
            type={type}
            value={value || ""}
            name={name}
            placeholder={label}
            onChange={this.changeValue}
            disabled={this.props.disabled}
            onKeyPress={this.onKeyPress}
            onFocus={() => !locked && this.setState({ active: true })}
            onBlur={this.props.onBlur}
            maxLength={maxLength}
            autoComplete={this.props.autocomplete || ''}
            readOnly={this.props.readOnly}
          />
          {error && <span className="error show">{error}</span>}
        </div>
      );
    }
  }
  
 
 
export default InputField;

