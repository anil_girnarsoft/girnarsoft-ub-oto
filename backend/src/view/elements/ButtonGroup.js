import React, { Component } from "react";



//const carouselContainer = document.querySelector(".carousel-container");

// Data for carousel
const btngroup = [

  {
    label: 'Upload Photos',
    type: 'button',
    class:'btn btn-default btn-sm',
    key:"0",
    id:"1"
    },
    {
      label: 'Tag photos',
      type: 'button',
      class:'btn btn-default btn-sm',
      key:"1",
      id:"2"
      },
      {
        label: 'View Photos',
        type: 'button',
        class:'btn btn-primary btn-sm',
        key:"2",
        id:"3"
        }
]


// Carousel wrapper component
class ButtonGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    //let { buttongroup, className, name, id,label,index } = this.state;
    let { buttongroup, className, key, id, label } = this.props;
    return (
      <div>
          <div role="group" className="btn-group mrg-b15">
          {btngroup && btngroup.map((btngroup, index) =>            
            <button id={btngroup.id} type={btngroup.type} key={index} className={btngroup.class} >{btngroup.label} </button>            
          )}
          
            
        </div>
      </div>
    );
  }
}
//render(<Carousel slides={carouselSlidesData} />, carouselContainer);

export default ButtonGroup;
// Render Carousel component

