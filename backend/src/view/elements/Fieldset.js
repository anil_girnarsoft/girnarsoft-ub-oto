import React, { Component } from 'react';


class Fieldset extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          
            legend: props.legend || "",
        };
      }
    
    render() {
        const { legend } = this.state;
    return (
        <fieldset className="fieldset">
            <legend className="">{legend}</legend>
        </fieldset>
               
        );
    }
}
export default Fieldset;