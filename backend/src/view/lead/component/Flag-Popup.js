import React, { Component } from 'react';
import CheckBox from '../../elements/CheckBox';


class FlagReasons extends Component {
    constructor(props) {
        super(props);
        this.state = {}; 
    }

    render() {
        return (
            <div className="pd-15">
                <form>  
                    <div className="form-group clearfix">
                        <CheckBox type="checkbox" label="Photo & car not matching" name="iussue" id="iussue-1" checked />
                        <CheckBox type="checkbox" label="Very low price/Incorrect price" name="iussue" id="iussue-2" />
                        <CheckBox type="checkbox" label="Registration No. & Registration city not matching" name="iussue" id="iussue-3" />
                        <CheckBox type="checkbox" label="Phone No. visible on image" name="iussue" id="iussue-4" />
                        <CheckBox type="checkbox" label="Blurred photo" name="iussue" id="iussue-5" />
                        <CheckBox type="checkbox" label="Offensive content" name="iussue" id="iussue-6" />
                        <CheckBox type="checkbox" label="Incorrect model year" name="iussue" id="iussue-7" />
                        <CheckBox type="checkbox" label="Sunroof is not present" name="iussue" id="iussue-8" />
                        <CheckBox type="checkbox" label="Outstation" name="iussue" id="iussue-9" />
                        <CheckBox type="checkbox" label="CNG kit is not available" name="iussue" id="iussue-10" />
                    </div>
                    <div className=" form-group text-right">
                        <button type="button" name="submit" className="btn btn-default mrg-r15">Cancel</button>
                        <button type="submit" name="submit" className="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default FlagReasons;
