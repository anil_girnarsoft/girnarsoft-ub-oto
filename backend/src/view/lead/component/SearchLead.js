import React, { Component } from 'react';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import { withTranslation } from 'react-i18next';
import "react-datepicker/dist/react-datepicker.css";
import InputField from '../../elements/InputField';
import config from './../../../config/config';
import { Multiselect } from 'multiselect-react-dropdown';
import _  from 'lodash';
import { toast } from 'react-toastify';

const quickFilter = [
  { value: 'is_premium', label: 'Premium' },
  { value: 'is_finance_req', label: 'Finance' },
  { value: 'is_individual_lead', label: 'Interested In Dealer' },
  { value: 'is_whatsapp_optin', label: 'Viber Opt-In' },
];
const searchBy = [
    { value: 'lead_id', label: 'Lead Id' },
    { value: 'name', label: 'Name' },
    { value: 'email', label: 'Email' },
    { value: 'mobile_number', label: 'Mobile Number' }
  ];

class SearchLead extends Component {
    constructor(props) {
        super(props);
        this.multiselectRef={
            'city':React.createRef(),
            'makeModel': React.createRef(),
            'quickFilter': React.createRef()
        }
        this.state = {
        selectedOption: null,
        date: new Date(),
        advanceSearchEnable: false,
        errors:null,
        isSearchByInputDisabled:true,
        searchByPlaceholder:'Enter Search Key',
        cityDetails: props.cityDetails,
        statusList: props.statusList,
        subStatusList: props.subStatusList,
        sourceList: props.sourceList,
        subSourceList: props.subSourceList,
        carMakeModelList: props.carMakeModelList,
        filterData: {
            'search_input':''
        },
        selectedCity:[],
        priceRange:props.priceRange
      };
    }

    // componentWillReceiveProps(nextProps) {
    //     this.setState({
    //         cityDetails: nextProps.cityDetails,
    //         statusList: nextProps.statusList,
    //         subStatusList: nextProps.subStatusList,
    //         sourceList: nextProps.sourceList,
    //         subSourceList: nextProps.subSourceList ,
    //         carMakeModelList: nextProps.carMakeModelList ,
    //         priceRange:nextProps.priceRange
    //     });
    // }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        let updatedStateData = 
        {
            cityDetails: nextProps.cityDetails,
            statusList: nextProps.statusList,
            subStatusList: nextProps.subStatusList,
            sourceList: nextProps.sourceList,
            subSourceList: nextProps.subSourceList ,
            carMakeModelList: nextProps.carMakeModelList ,
            priceRange:nextProps.priceRange
        };

        return updatedStateData;

    }

    ToogleAdvanceSearch = () => {
        let advanceSearchEnable = this.state.advanceSearchEnable;
        if (advanceSearchEnable) {
          this.setState({ advanceSearchEnable: false });
        } else {
          this.setState({ advanceSearchEnable: true });
        }
    };

    handleDateChange = (selectedKey,date) =>{
        let filterData = {...this.state.filterData};
        filterData[selectedKey] = date;
        this.setState({ filterData:filterData });
    }

    handleChange = (selectedKey,event) => {
        let filterData = {...this.state.filterData};
        
        if(selectedKey === 'search_by'){
            filterData[selectedKey] = event.value;
            filterData.search_input='';
            this.setState({isSearchByInputDisabled:false,searchByPlaceholder:'Enter '+event.label})
        }else if(selectedKey === 'search_input'){
            filterData.search_input = event.target.value;
        }else{
            filterData[selectedKey] = event.value; 
        }
        if(selectedKey === 'status'){
            let params= {
                'status_id':event.value,
                'isPaginationRequired':false
            }
            if (typeof this.props.getSubStausDetails === 'function') {
                this.props.getSubStausDetails(params);
            }
        }
        if(selectedKey === 'source'){
            let params= {
                'source_id':event.value,
                'isPaginationRequired':false
            }
            if (typeof this.props.getSubSourceDetails === 'function') {
                this.props.getSubSourceDetails(params);
            }
        }
        this.setState({ filterData:filterData });
    };

    searchFilter = async() => {
        let formValid = await this.validateSearchInput();
        if(typeof this.props.getFilterList === 'function' && formValid) {
            this.props.getFilterList(this.state.filterData)
        }
    }

    validateSearchInput = () => {
        let filterData = this.state.filterData;
        let isSearchFormValid = true;
        if(filterData.follow_up_from_date > filterData.follow_up_to_date){
            isSearchFormValid=false;
            toast.error('To due date should be greater than from due date');
        }
        if(filterData.created_from_date > filterData.created_to_date){
            isSearchFormValid=false;
            toast.error('To created date should be greater than from created date');
        }
        if(filterData.updated_from_date > filterData.updated_to_date){
            isSearchFormValid=false;
            toast.error('To updated date should be greater than from updated date');
        }
        if(filterData.min_budget_range > filterData.max_budget_range){
            isSearchFormValid=false;
            toast.error('Max price should be greater than min price');
        }
        return isSearchFormValid;
    }

    onSelect(key,selectedList, selectedItem) {
        let filterData = this.state.filterData;
        if(key !== 'quick_filter'){
            let existingVal = (filterData[key]) ? filterData[key] : []
            existingVal.push(selectedItem.value);
            filterData[key] = existingVal;
            this.setState({ filterData: filterData });
        }else{
            if(selectedItem.value === 'is_premium')
                filterData[selectedItem.value] = '1';
            if(selectedItem.value === 'is_finance_req')
                filterData[selectedItem.value] = 'yes';
            if(selectedItem.value === 'is_individual_lead')
                filterData[selectedItem.value] = '1';
            if(selectedItem.value === 'is_whatsapp_optin')
                filterData[selectedItem.value] = '1';
        }
        
    }
     
    onRemove(key,selectedList, removedItem) {
        let filterData = this.state.filterData;
        if(key !== 'quick_filter'){
            let existingVal = (filterData[key]) ? filterData[key] : [];
            existingVal = _.filter(existingVal, function(currentVal) {
                return currentVal !== removedItem.value;
            });
            filterData[key] = existingVal;
            this.setState({ filterData: filterData });
        }else{
            delete filterData[removedItem.value];
        }
    }

    reset = () => {
        let filterData = {
            'search_input':''
        };
        this.multiselectRef.city.current.resetSelectedValues();
        this.multiselectRef.makeModel.current.resetSelectedValues();
        this.multiselectRef.quickFilter.current.resetSelectedValues();
        this.setState({ filterData: filterData });
    }

    render() {
        return (
            
            <div className="search-wrap">
                <ul className="search-flex">
                    <li className="searchitems doublew form-group">
                        <label>{this.props.t('search.searchBy')}</label>
                        <div className="row">
                            <div className="col-xs-6">
                                <Select
                                    id="search_by"
                                    name="search_by"
                                    placeholder={this.props.t('search.placeholder.searchBy')}
                                    onChange={this.handleChange.bind(this,'search_by')}
                                    options={searchBy}
                                    value={searchBy.filter(({ value }) => value === this.state.filterData.search_by)}
                                />
                            </div>
                            <div className="col-xs-6 btm-20">
                            <InputField
                                inputProps={{
                                    id: "search_input",
                                    type: "text",
                                    name: "search_input",
                                    value: this.state.filterData.search_input,
                                    disabled:this.state.isSearchByInputDisabled,
                                    placeholder:this.state.searchByPlaceholder
                                    
                                }}
                                onChange={this.handleChange.bind(this,'search_input')}
                                />
                            </div>
                        </div>
                    </li>
                    <li className="searchitems">
                        <label>{this.props.t('search.clusterCity')}</label>
                        <Multiselect
                            id='cluster_city'
                            options={this.state.cityDetails} 
                            onSelect={this.onSelect.bind(this,'cluster_city')} 
                            onRemove={this.onRemove.bind(this,'cluster_city')} 
                            displayValue="label" 
                            showCheckbox={true}
                            // placeholder="Custom Placeholder"
                            closeOnSelect={false}
                            ref={this.multiselectRef.city}
                        />
                    </li>
                    <li className="searchitems doublew form-group">                        
                        <div className="row">
                            <div className="col-xs-6">
                                <label>{this.props.t('search.status')}</label>
                                <Select
                                    id="status"
                                    name="status"
                                    placeholder={this.props.t('search.placeholder.status')}
                                    onChange={this.handleChange.bind(this,'status')}
                                    options={this.state.statusList}
                                    value={this.state.statusList.filter(({ value }) => value === this.state.filterData.status)}
                                />
                            </div>
                            <div className="col-xs-6">
                                <label>{this.props.t('search.subStatus')}</label>
                                <Select
                                    id="sub_status"
                                    name="sub_status"
                                    placeholder={this.props.t('search.placeholder.subStatus')}
                                    onChange={this.handleChange.bind(this,'sub_status')}
                                    options={this.state.subStatusList}
                                    value={this.state.subStatusList.filter(({ value }) => value === this.state.filterData.sub_status)}
                                />
                            </div>
                        </div>
                    </li>
                    <li className="searchitems">
                        <label>{this.props.t('search.selectCar')}</label>
                        <Multiselect
                            id='car_make_model'
                            options={this.state.carMakeModelList} 
                            onSelect={this.onSelect.bind(this,'car_make_model')} 
                            onRemove={this.onRemove.bind(this,'car_make_model')} 
                            displayValue="label" 
                            showCheckbox={true}
                            closeOnSelect={false}
                            ref={this.multiselectRef.makeModel}
                        />
                    </li>
                    <li className="searchitems doublew form-group">
                        <div className="row">
                            <div className="col-xs-6">
                                <label>{this.props.t('search.follow_up_date')}</label>
                                <DatePicker
                                    id='follow_up_from_date'
                                    className="form-control"
                                    onChange={this.handleDateChange.bind(this,'follow_up_from_date')}
                                    selected={this.state.filterData.follow_up_from_date}
                                    dateFormat={config.constants.dateFormatDMY}
                                    placeholderText={this.props.t('search.placeholder.from')}
                                    onKeyDown={e => e.preventDefault()}
                                />
                            </div>
                            <div className="col-xs-6">
                            <label>{this.props.t('search.dateTo')}</label>
                                <DatePicker
                                    id='follow_up_to_date'
                                    className="form-control"
                                    onChange={this.handleDateChange.bind(this,'follow_up_to_date')}
                                    selected={this.state.filterData.follow_up_to_date}
                                    dateFormat={config.constants.dateFormatDMY}
                                    placeholderText={this.props.t('search.placeholder.to')}
                                    onKeyDown={e => e.preventDefault()}
                                />
                            </div>
                        </div>
                    </li>

                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                    <label>{this.props.t('search.budgetRange')}</label>
                        <div className="row">
                            <div className="col-xs-6">
                                <Select
                                    id="min_budget_range"
                                    name="min_budget_range"
                                    placeholder={this.props.t('search.placeholder.priceMin')}
                                    onChange={this.handleChange.bind(this,'min_budget_range')}
                                    options={this.state.priceRange.min_price_range}
                                    value={this.state.priceRange.min_price_range.filter(({ value }) => value === this.state.filterData.min_budget_range)}
                                />
                            </div>
                            <div className="col-xs-6">
                                <Select
                                    id="max_budget_range"
                                    name="max_budget_range"
                                    placeholder={this.props.t('search.placeholder.priceMax')}
                                    onChange={this.handleChange.bind(this,'max_budget_range')}
                                    options={this.state.priceRange.max_price_range}
                                    value={this.state.priceRange.max_price_range.filter(({ value }) => value === this.state.filterData.max_budget_range)}
                                />
                            </div>
                        </div>
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                        <label>{this.props.t('search.createdDate')}</label>
                        <div className="row">
                            <div className="col-xs-6">                                
                                <DatePicker
                                    id='created_from_date'
                                    className="form-control"
                                    onChange={this.handleDateChange.bind(this,'created_from_date')}
                                    selected={this.state.filterData.created_from_date}
                                    dateFormat={config.constants.dateFormatDMY}
                                    placeholderText={this.props.t('search.placeholder.from')}
                                    onKeyDown={e => e.preventDefault()}
                                />
                            </div>
                            <div className="col-xs-6">
                                <DatePicker
                                    id='created_to_date'
                                    className="form-control"
                                    onChange={this.handleDateChange.bind(this,'created_to_date')}
                                    selected={this.state.filterData.created_to_date}
                                    dateFormat={config.constants.dateFormatDMY}
                                    placeholderText={this.props.t('search.placeholder.to')}
                                    onKeyDown={e => e.preventDefault()}
                                />
                            </div>
                        </div>
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                    <label>{this.props.t('search.subSource')}</label>
                        <div className="row">
                            <div className="col-xs-6">
                                <Select
                                    id="source"
                                    name="source"
                                    placeholder={this.props.t('search.placeholder.source')}
                                    onChange={this.handleChange.bind(this,'source')}
                                    options={this.state.sourceList}
                                    value={this.state.sourceList.filter(({ value }) => value === this.state.filterData.source)}
                                />
                            </div>
                            <div className="col-xs-6">
                                <Select
                                    id="sub_source"
                                    name="sub_source"
                                    placeholder={this.props.t('search.placeholder.subSource')}
                                    onChange={this.handleChange.bind(this,'sub_source')}
                                    options={this.state.subSourceList}
                                    value={this.state.subSourceList.filter(({ value }) => value === this.state.filterData.sub_source)}
                                />
                            </div>
                        </div>
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                        <label>{this.props.t('search.updatedDate')}</label>
                        <div className="row">
                            <div className="col-xs-6">                                
                                <DatePicker
                                    id="updated_from_date"
                                    className="form-control"
                                    placeholder={this.props.t('search.placeholder.from')}
                                    onChange={this.handleDateChange.bind(this,'updated_from_date')}
                                    selected={this.state.filterData.updated_from_date}
                                    dateFormat={config.constants.dateFormatDMY}
                                    placeholderText={this.props.t('search.placeholder.from')}
                                    onKeyDown={e => e.preventDefault()}
                                />
                            </div>
                            <div className="col-xs-6">
                                <DatePicker 
                                    id="updated_to_date"
                                    className="form-control"
                                    onChange={this.handleDateChange.bind(this,'updated_to_date')}
                                    selected={this.state.filterData.updated_to_date}
                                    dateFormat={config.constants.dateFormatDMY}
                                    placeholderText={this.props.t('search.placeholder.to')}
                                    onKeyDown={e => e.preventDefault()}
                                />
                            </div>
                        </div>
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                    <label>{this.props.t('search.quickFilter')}</label>
                        <div className="row">
                            <div className="col-xs-9">
                                <Multiselect
                                    id="quick_filter"
                                    options={quickFilter} 
                                    onSelect={this.onSelect.bind(this,'quick_filter')} 
                                    onRemove={this.onRemove.bind(this,'quick_filter')} 
                                    displayValue="label" 
                                    showCheckbox={true}
                                    closeOnSelect={false}
                                    ref={this.multiselectRef.quickFilter}
                                />
                            </div>
                        </div>
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                        <label>{this.props.t('search.attempt')}</label>
                        <Select
                            id="no_of_attemps"
                            name="no_of_attemps"
                            onChange={this.handleChange.bind(this,'no_of_attemps')}
                            options={config.constants.numAttemptArray}
                            value={config.constants.numAttemptArray.filter(({ value }) => value === this.state.filterData.no_of_attemps)}
                        />
                    </li>
                    <li className="searchitems form-group">
                        <label> &nbsp;</label>
                        <div>
                            <button type="submit" className="btn btn-primary mrg-r15 undefined" onClick={this.searchFilter}>{this.props.t('search.search')}</button>
                            <button type="reset" className="btn btn-default btn-reset" onClick={this.reset}>{this.props.t('search.reset')}</button>
                        </div>
                    </li>
                </ul>
                <span  className="advancesearch btn btn-link"  onClick={this.ToogleAdvanceSearch} >
                    <span>{this.state.advanceSearchEnable ? "- Less Search" : "+ Advance Search"}</span>
                </span>
            </div>
        );
    }
}

export default withTranslation('lead') (SearchLead);
