import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import DateFormat from 'dateformat';
import _  from 'lodash';
import ToolTip from './../../elements/Tooltip';

class SearchResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            pageloading: false,
            leadListData: props.leadListData,
            totalRecord:0,
            statusClassname: {
                'active': 'nav-item active',
                'inactive': 'nav-item',
                'incomplete': 'nav-item',
                'blacklist': 'nav-item',
                'all': 'nav-item'
            },
            filterData: {
            },
            usersRecord:[],
            currentIndex: null,
            current_org_name: null
        };
    }

    componentDidMount = async () => {
        
    }

    // componentWillReceiveProps(nextProps) {
        // this.setState({
        //     leadListData: nextProps.leadListData,
        //     loading: nextProps.loading,
        //     pageloading: nextProps.pageloading,
        //     totalRecord:nextProps.totalRecord
        // });
    // }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        let updatedStateData = {
            leadListData: nextProps.leadListData,
            loading: nextProps.loading,
            pageloading: nextProps.pageloading,
            totalRecord:nextProps.totalRecord
        };

        return updatedStateData;

    }

    render() {
        return (
            <div className="result-wrap">
                <div className="card">
                <div className="card-heading">
                    <div className="row">
                        <div className="col-sm-6">
                            <h2 className="card-title pad-t7">{this.state.totalRecord} {this.props.t('list.resultFound') }</h2>
                        </div>
                        
                    </div>
                </div>
            <div className="table-responsive">
                    <table className="table table-bordered table-hover table-striped table-lead">
                                <thead>
                                    <tr>
                                        <th>{this.props.t('list.leadId') }</th>
                                        <th>{this.props.t('list.leadDetails') }</th>
                                        <th>{this.props.t('list.interestedIn') }</th>
                                        <th>{this.props.t('list.city') }</th>
                                        <th>{this.props.t('list.status') }</th>
                                        <th>{this.props.t('list.dueDate') }</th>
                                        <th>{this.props.t('list.updatedDate') }</th>
                                    </tr>
                                </thead>

                                <tbody>
                                {(this.state.leadListData.length === 0 && !this.state.loading) ? <tr><td align="center" colSpan="7"><h6 className="text-center text-danger text-bold">{this.props.t('search.NoRecordFound')}</h6></td></tr> : null}
                                {(this.state.leadListData && this.state.leadListData.length !== 0) && this.state.leadListData.map((leaddata, k) =>
                                    <tr key={k}>
                                        <td><a href={`/lead-details/${leaddata.lead_id_hash}`} target="_blank" title="Lead ID">{leaddata.id}</a></td>
                                        <td>
                                            <div id="customer_name">                                            
                                            <strong className="mrg-r5">{leaddata.customer_name}</strong>
                                            <ToolTip  title="Premium Lead" ><span className="label green-status mrg-r5">PR</span></ToolTip>
                                            <i className="OTO source-icon"></i>
                                        </div>
                                        <div className="" id="customer_email">{leaddata.customer_mobile}</div>
                                        <div className="listingdate">{DateFormat(leaddata.created_at,"dd/mm/yyyy")}</div>
                                        </td>
                                        <td><strong>{leaddata.make_name + ' ' +leaddata.model_name}</strong><br/>Rp {leaddata.price_to}</td>
                                        <td>{leaddata.customer_city_name}</td>
                                        <td>
                                            <div className="mrg-B5">{leaddata.status_name}</div>
                                        </td>
                                        <td>{DateFormat(leaddata.due_date,"dd/mm/yyyy")}</td>
                                        <td>{DateFormat(leaddata.updated_at,"dd/mm/yyyy")}</td>
                                    </tr>
                                    )}
                                    {
                                        (this.state.loading) ? <tr><td className="loading" colSpan="7"></td></tr> : null
                                    }
                                </tbody>
                            </table>
                </div>
                </div>
            </div>
        )     
    }
}


 export default withTranslation('lead') (SearchResult);
