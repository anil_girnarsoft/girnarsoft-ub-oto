import React, { Component } from 'react';
import UserService from './../../../../service/userService';
import SearchUser from './searchUser';
import SearchResult from './searchResult';
import { withTranslation } from 'react-i18next';
import {debounce} from 'throttle-debounce';
import { toast } from 'react-toastify';
import _ from 'lodash';

class UserList extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            pageloading:true,
            userListData: [],
            searchByList: [],
            page: 0,
            filterData: {
                
            },
            redirect: false
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.onScroll = this.onScroll.bind(this);
        this.getFilterList = debounce(500, this.getFilterList);
        this.updateStatus = this.updateStatus.bind(this);
    }

    onScroll(event) {
        if ((window.innerHeight + window.scrollY > document.body.offsetHeight - 300)) {
            if (this.flag) {
                let current_page = this.state.page;
                let loading=false,pageloading=false;
                if(current_page===0) {
                    pageloading = true;
                }else{
                    loading = true;
                }
                if(this._isMounted){
                    this.setState({ page: current_page + 1,loading:loading,pageloading:pageloading}, () => {
                        this.getUserList();
                    });
                }
            }
            this.flag = false;
        }
    }
    
    getUserList(updatedStatus) {
        var thisObj = this;
        var filterData = thisObj.state.filterData;
        filterData.page_number = (thisObj.state.page === 0) ? 1 : thisObj.state.page;
        thisObj.setState({page:filterData.page_number,loading:true})
        UserService.getUserList(filterData)
            .then(function (response) {
                thisObj.flag = false;
                thisObj.setState({loading:false,pageloading:false})
                if (response.data.status === 200) {
                    if (response.data.data.length) {
                        if (thisObj.state.page <= 1) {
                            thisObj.setState({ userListData: response.data.data ,loading:false,pageloading:false}, () => {
                                thisObj.flag = true;
                            });
                        }
                        else {
                            thisObj.setState({
                                userListData: thisObj.state.userListData.concat(response.data.data)
                            ,loading:false,pageloading:false}, () => {
                                thisObj.flag = true;
                            });
                        }
                    } else {
                        if(updatedStatus){
                            let listData = this.state.userListData;
                            let matchedIndex = _.findIndex(listData, function(data) { return data.id === updatedStatus.id })
                            if(matchedIndex !== -1){
                                listData[matchedIndex] = updatedStatus;
                                thisObj.setState({userListData:listData})
                            }
                        }
                        if (thisObj.state.page <= 1) {
                            thisObj.setState({ userListData: response.data.data});
                        }
                    }
                }
            })
            .catch(function (response) {
                thisObj.setState({loading:false,pageloading:false})
            });
    }

    updateStatus(data){
        let params={};
        let thisObj=this;
        params.id=data.id;
        data.status = params.status = (data.status === '1') ? '0' : '1';
        UserService.updateStatus(params)
            .then(function (response) {
                if (response.data.status === 200) {
                    toast.success(response.data.message);
                    thisObj.getUserList(data);
                }else{
                    toast.error(response.data.message);
                }
            })
            .catch(function (response) {
            });

    }

    componentDidMount = async () => {
        this._isMounted = true;
        this.onScroll('Mounter');
        window.addEventListener('scroll', this.onScroll);
        await this.getUserList();
    }

    handleNameChange = async (searchParams) => {
        let validParams = this.validateSearchParams(searchParams);
        this.setState({ filterData:validParams });
        await this.getFilterList();
      
    }

    validateSearchParams(filterData){
        let returnFilter={};
        if(filterData.page_number)
            returnFilter.page_number = filterData.page_number;
        if(filterData.name){
            returnFilter.name = filterData.name;
        }
        if(filterData.status !== ''){
            returnFilter.status = filterData.status;
        }
        if(filterData.role_id !== ''){
            returnFilter.role_id = filterData.role_id;
        }
        return returnFilter;
      
    }

    getFilterList = async (searchParams) => {
        this.setState({page:1})
        await this.getUserList();
    }

    submitFilterForm = () => {
        this.setState({page:1});
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="result-wrap">
                    <div className="card">
                        <SearchUser  filterNameChange={this.handleNameChange} onSubmitFilter={this.submitFilterForm}/>
                        <div className="clearfix"/>
                        <SearchResult pageloading={this.state.pageloading} loading={this.state.loading} userListData={this.state.userListData} onUpdateStatus={this.updateStatus} />
                        
                    </div>
                </div>
            </div>
        )
    }

    componentWillUnmount = () => {
        this._isMounted = false;
    }
}

export default withTranslation('user') (UserList);