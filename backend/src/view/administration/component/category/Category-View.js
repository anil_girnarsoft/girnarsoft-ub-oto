import React, { Component } from 'react';
import Select from 'react-select';
const city = [
    { value: 'New Delhi', label: 'New Delhi' },
    { value: 'Mumbai', label: 'Mumbai' },
    { value: 'Gurugram', label: 'Gurugram' },
    { value: 'Chennai', label: 'Chennai' },
  ];


class CategoryView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: null,

        }
    }
    qcSearchCar = (event) => {
        this.setState({[event.target.name]: event.target.value},()=>{
        });
        
      }
      handleChange = selectedOption => {
        this.setState({ selectedOption });
        
      };
      render() {
          const { selectedOption } = this.state;

        return (
            <div className="container-fluid">
                <div className="result-wrap">
                    <div className="card">
                        <div className="card-heading">
                            <div className="row">
                                <div className="col-sm-8 col-md-9">
                                    <h2 className="card-title pad-t7">View Categories</h2>
                                </div>
                                <div className="col-sm-4 col-md-3">
                                    <form>
                                        <Select
                                            id="city"
                                            value="min_price"
                                            value={selectedOption}
                                            onChange={this.handleChange}
                                            options={city}
                                        />
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="table-responsive">
                            <table className="table table-bordered table-hover table-striped table-category">
                                <thead>
                                    <tr>
                                        <th>Category ID</th>
                                        <th>Category Name</th>
                                        <th>Created Date</th>
                                        <th>Created By</th>
                                        <th>Last Modified Date</th>
                                        <th>Last Modified By</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Dealer</td>
                                        <td>23/02/2016</td>
                                        <td>devender.choudhry@gmail.com</td>
                                        <td>04/04/2019</td>
                                        <td></td>
                                        <td>Active</td>
                                        <td>
                                            <div className="btn-group">
                                            <a className="btn btn-default" href="/Edit-Category" title="Edit"><i className="ic-createmode_editedit mrg-r5"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Dealer</td>
                                        <td>23/02/2016</td>
                                        <td>devender.choudhry@gmail.com</td>
                                        <td>04/04/2019</td>
                                        <td></td>
                                        <td>Active</td>
                                        <td>
                                            <div className="btn-group">
                                            <a className="btn btn-default" href="/Edit-Category" title="Edit"><i className="ic-createmode_editedit mrg-r5"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Dealer</td>
                                        <td>23/02/2016</td>
                                        <td>devender.choudhry@gmail.com</td>
                                        <td>04/04/2019</td>
                                        <td></td>
                                        <td>Active</td>
                                        <td>
                                            <div className="btn-group">
                                                <a className="btn btn-default" href="/Edit-Category" title="Edit"><i className="ic-createmode_editedit mrg-r5"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Dealer</td>
                                        <td>23/02/2016</td>
                                        <td>devender.choudhry@gmail.com</td>
                                        <td>04/04/2019</td>
                                        <td></td>
                                        <td>Active</td>
                                        <td>
                                            <div className="btn-group">
                                            <a className="btn btn-default" href="/Edit-Category" title="Edit"><i className="ic-createmode_editedit mrg-r5"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default CategoryView;