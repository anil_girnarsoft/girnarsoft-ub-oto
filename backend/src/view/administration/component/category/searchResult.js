import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import DateFormat from 'dateformat';
import CategoryService from '../../../../service/CategoryService';
import _  from 'lodash';


class SearchResult extends Component {
    categoryListData=[]
    constructor(props) {
        super(props);
        this.state = {
            categoryListData: props.categoryListData,
            loading: false,
            pageloading: false,
            statusClassname: {
                'active': 'nav-item active',
                'inactive': 'nav-item',
                'incomplete': 'nav-item',
                'blacklist': 'nav-item',
                'all': 'nav-item'
            },
            filterData: {
            },
            usersRecord:[],
            currentIndex: null,
            current_org_name: null
        };
    }

    componentDidMount = async () => {
        await this.getUserList();
    }

    // componentWillReceiveProps(nextProps) {
    //     this.setState({
    //         categoryListData: nextProps.categoryListData,
    //         loading: nextProps.loading,
    //         pageloading: nextProps.pageloading
    //     });
    // }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        let updatedStateData = {};

        if(nextProps.categoryListData){
            updatedStateData['categoryListData'] = nextProps.categoryListData;
        }

        if(nextProps.loading !== prevState.loading){
            updatedStateData['loading'] = nextProps.loading;
        }

        if(nextProps.pageloading !== prevState.pageloading){
            updatedStateData['pageloading'] = nextProps.pageloading;
        }

        return updatedStateData;

    }

    updateStatus(userData){
        if (typeof this.props.onUpdateStatus === 'function') {
            this.props.onUpdateStatus(userData);
        }

    }

    getUserName = (data,keyToMatch,keyToReturn) => {
        const user = _.find(this.state.usersRecord,{'id':data[keyToMatch]});
        if(user)
            return user[keyToReturn]
        else
            return null;
    }

    getUserList() {
        var thisObj = this;
        CategoryService.getAllUserList()
        .then(function (response) {
            if (response.data.status === 200) {
                if (response.data.data.length) {
                        thisObj.setState({ usersRecord: response.data.data });
                    
                } else {
                        thisObj.setState({ usersRecord: []});
                    
                }
            }
        })
        .catch(function (response) {
            
        });
    }
    
    render() {
        return (
            <div className="table-responsive">
                    <table className="table table-bordered table-hover table-striped table-category">
                        <thead>
                            <tr>
                                <th>{this.props.t('List.details.category_id') }</th>
                                <th>{this.props.t('List.details.category_name') }</th>
                                <th>{this.props.t('List.details.created_date') }</th>
                                <th>{this.props.t('List.details.created_by') }</th>
                                <th>{this.props.t('List.details.updated_date') }</th>
                                <th>{this.props.t('List.details.updated_by') }</th>
                                <th>{this.props.t('List.details.status') }</th>
                                <th>{this.props.t('List.details.action') }</th>
                            </tr>
                        </thead>
                        <tbody>
                        {(this.state.categoryListData.length === 0 && !this.state.loading) ? <tr><td align="center" colSpan="8"><h6 className="text-center text-danger text-bold">{this.props.t('List.Search_Category.No_Record_Found')}</h6></td></tr> : null}
                            {(this.state.categoryListData && this.state.categoryListData.length !== 0) && this.state.categoryListData.map((categorydata, k) =>
                            <tr key={k}>
                                <td>{categorydata.id}</td>
                                <td>{categorydata.name}</td>
                                <td>{DateFormat(categorydata.created_at,"dd/mm/yyyy")}</td>
                                <td>{this.getUserName(categorydata,'added_by','username')}</td>
                                <td>{(categorydata.updated_at)? (DateFormat(categorydata.updated_at,"dd/mm/yyyy")): ''}</td>
                                <td>{this.getUserName(categorydata,'updated_by','username')}</td>
                                <td>
                                    <label className="switch-btn btn btn-link ml-5" htmlFor={"active"+categorydata.id} >
                                        <input checked={(categorydata.status === "1") ? true : false} className="switch-btn" id={"active"+categorydata.id} value="open" name={"active"+categorydata.id} type="checkbox"  onChange={this.updateStatus.bind(this,categorydata)}  />
                                        <div className="slider round"></div>
                                        <span className="switch-label"></span>
                                        {(categorydata.status === "1") ? this.props.t('formData.active'):this.props.t('formData.inactive')}
                                    </label>
                                </td>
                                <td>
                                    <div className="btn-group">
                                    <Link to={`/edit-category/${categorydata.category_id_hash}`} className="btn btn-default"><i className="ic-createmode_editedit mrg-r5"></i></Link>
                                    </div>
                                </td>
                            </tr>
                            )}
                            {
                                (this.state.loading) ? <tr><td className="loading" colSpan="8"></td></tr> : null
                            }
                            
                        </tbody>
                    </table>
                </div>
                
            
        )
    }
}

export default withTranslation('category') (SearchResult);
