import React, { Component } from 'react';
import Select from 'react-select';
import InputField from '../../../elements/InputField';
import CheckBox from '../../../elements/CheckBox';
import SourceService from './../../../../service/sourceService';
import { toast } from 'react-toastify';
import { withTranslation } from 'react-i18next';
import DateFormat from 'dateformat';
import _  from 'lodash';
import * as config from './../../../../config/config';
import { Redirect } from "react-router-dom";
class EditSubSource extends Component {
    source_id;
    constructor(props) {
        super(props);
        this.state = {
            editFormData: {},
            successResponse: false,
            redirect: false,
            errors: {},
            show: false,
            renderSection: 'category',
            loaderClass: '',
            searchParams:{},
            sourceDetails:[],
            usersRecord:[],
            redirectTolist: false,
        }
        this.source_id = this.props.match.params.id;
        if(this.source_id)
            this.getSubSourceDetails = this.getSubSourceDetails.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this)
        this.getSourceDetails({'status':'1'});
    }

    componentDidMount = async () => {
        if(this.source_id){
            let searchParams = this.state.searchParams;
            searchParams.id = this.source_id;
            this.setState({searchParams:searchParams})
            await this.getSubSourceDetails();
            await this.getUserList();
        }else
            this.setState({editFormData:{}})
    }

    getSubSourceDetails() {
        var thisObj = this;
        SourceService.getSubSourceList(this.state.searchParams)
        .then(function (response) {
            thisObj.flag = false;
            thisObj.setState({loading:false,pageloading:false})
            if (response.data.status === 200) {
                if (response.data.data.length) {
                    thisObj.setState({ editFormData: response.data.data[0] ,loading:false,pageloading:false}, () => {
                        thisObj.flag = true;
                    });
                } else {
                    thisObj.setState({ editFormData: response.data.data});
                }
            }
        })
        .catch(function (response) {
            thisObj.setState({loading:false,pageloading:false})
        });
    }
    getSourceDetails(postData) {
        var thisObj = this;
        SourceService.getSourceListData(postData)
        .then(function (response) {
            if (response.data.status === 200) {
                const scrTypeArr = [];
                if (response.data.data.length) {
                    _.forEach((response.data.data),element => {
                        let srcTypeObj={}
                        srcTypeObj.value = element.id;
                        srcTypeObj.label = element.name;
                        scrTypeArr.push(srcTypeObj);
                    });
                    thisObj.setState({ sourceDetails: scrTypeArr });
                } else {
                    thisObj.setState({ sourceDetails: scrTypeArr});
                }
            }
        })
        .catch(function (response) {
            thisObj.setState({loading:false,pageloading:false})
        });
    }

    
    handleChange = (event) => {
        let editFormData = this.state.editFormData;
        if(event && (event === 'status' || event === 'added_to_l1' || event === 'added_to_l2'))
            editFormData[event] = (editFormData[event] === '1') ? '0' : '1';
        this.setState({editFormData:editFormData})
    };

    handleOptionChange = (key,event) => {
        let editFormData = this.state.editFormData;
        let errors = this.state.errors;
        if(key && key === 'status')
            editFormData.status = (editFormData.status === '0') ? '1' : '0';
        else
            editFormData[key] = event ? event.value : '';
            errors[key] = '';
        this.setState({editFormData:editFormData})
        this.setState({errors:errors});
    };
    
    submitForm = async (event) => {
        event.preventDefault();
        if (this.validForm()) {
            this.setState({ loaderClass : 'loading' });
            let postData = await this.filterPostData();
            if(postData.id){
                SourceService.updateSubSourceDetails(postData)
                .then((response) => {
                    if (response.status === 200 && response.data.status === 200) {
                        toast.success(response.data.message);
                        this.setState({ redirectTolist: true })
                    } else if (response.status === 401 && response.data.status === 401) {
                        this.setState({ redirect: true })
                    }
                })
                .catch((error) => {
                    
                });
            }else{
                SourceService.saveSubSourceDetails(postData)
                .then((response) => {
                    if (response.status === 200 && response.data.status === 200) {
                        toast.success(response.data.message);
                        this.setState({ redirectTolist: true })
                    } else if (response.status === 401 && response.data.status === 401) {
                        this.setState({ redirect: true })
                    }
                })
                .catch((error) => {
                });
            }
        }
    }

    filterPostData = () => {
        let params={};
        if(this.state.editFormData.id)
            params.id = this.state.editFormData.id;
        if(this.state.editFormData.source_id)
            params.source_id = this.state.editFormData.source_id;
        if(this.state.editFormData.lang_id)
            params.lang_id = this.state.editFormData.lang_id;
        if(this.state.editFormData.name)
            params.name = this.state.editFormData.name;
        if(this.state.editFormData.description)
            params.description = this.state.editFormData.description;
        if(this.state.editFormData.status)
            params.status = this.state.editFormData.status;
        if(this.state.editFormData.paid_details)
            params.paid_details = this.state.editFormData.paid_details;
        if(this.state.editFormData.added_to_l1)
            params.added_to_l1 = this.state.editFormData.added_to_l1;
        if(this.state.editFormData.added_to_l2)
            params.added_to_l2 = this.state.editFormData.added_to_l2;
            if(this.state.editFormData.friendly_name)
            params.friendly_name = this.state.editFormData.friendly_name;
        
        return params;
    }

    validForm = () => {
        let editFormData = {...this.state.editFormData};
        let errors = {...this.state.errors};
        let formIsValid = true;
        if (!editFormData["name"]) {
            errors['name'] = this.props.t('subSource.validation.name_required')
            formIsValid = false;
        }
        if (!editFormData["source_id"]) {
            errors['source_id'] = this.props.t('subSource.validation.source_id_required')
            formIsValid = false;
        }
        if (!editFormData["friendly_name"]) {
            errors['friendly_name'] = this.props.t('subSource.validation.friendly_name_required')
            formIsValid = false;
        }
        if (!editFormData["paid_details"]) {
            errors['paid_details'] = this.props.t('subSource.validation.paid_details_required')
            formIsValid = false;
        }
        if (!editFormData["lang_id"]) {
            errors['lang_id'] = this.props.t('subSource.validation.lang_id_required')
            formIsValid = false;
        }
        this.setState({errors: errors});
        return formIsValid;
    }

    handleChangeEvent = event => {
        let fieldRequired = event.target.getAttribute('validationreq');
        let editFormData = {...this.state.editFormData};
        let errors = {...this.state.errors};
        editFormData[event.target.name] = event.target.value;
        if(fieldRequired === 'yes'){
            if (event.target.value !== '') {
                delete errors[event.target.name];
            } else {
                errors[event.target.name] = this.props.t('subSource.validation.'+event.target.name)+this.props.t('subSource.validation.is_required');
            }
        }
        this.setState({ editFormData: editFormData, errors: errors });
    }

    getUserName = (userData,keyToMatch,keyToReturn) => {
        const user = _.find(this.state.usersRecord,{'id':userData[keyToMatch]});
        if(user)
            return user[keyToReturn]
        else
            return null;
    }
    
    getUserList = () => {
        var thisObj = this;
        SourceService.getAllUserList()
        .then(function (response) {
            if (response.data.status === 200) {
                if (response.data.data.length) {
                        thisObj.setState({ usersRecord: response.data.data });
                    
                } else {
                        thisObj.setState({ usersRecord: []});
                    
                }
            }
        })
        .catch(function (response) {
            
        });
    }
    
    render() {
          const {editFormData,errors,sourceDetails,redirectTolist}   = this.state;
          if(!editFormData.hasOwnProperty('status'))
            editFormData.status = "1";
          if (redirectTolist) return <Redirect to="/list-sub-source" />;
        return (
            <div className="container-fluid">
                <h1>{(editFormData.id) ? this.props.t('subSource.formDetails.editSource'):this.props.t('subSource.formDetails.addSource') }</h1>                
                <div className="card">
                    <div className="card-body">
                        <form method="post" id="formDetails" onSubmit={this.submitForm}>
                            <div className="row">
                                <div className=" col-sm-4 form-group">
                                    <label>{this.props.t('subSource.formDetails.sourceName')}</label>
                                    <span className="required">*</span>
                                    <Select
                                        id="source_id"
                                        options={sourceDetails}
                                        name="source_id"
                                        onChange={this.handleOptionChange.bind(this,'source_id')}
                                        value={sourceDetails.filter(({ value }) => value === editFormData.source_id)}
                                        
                                        
                                    />
                                    <span className="text-danger">{errors.source_id || ''}</span>
                                    </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-4 ">
                                <InputField
                                inputProps={{
                                    id: "name",
                                    type: "text",
                                    name: "name",
                                    label: this.props.t('subSource.formDetails.subSourceName'),
                                    value: editFormData.name || '',
                                    dataerror: errors.name || '',
                                    validationreq: "yes"
                                }}
                                onChange={this.handleChangeEvent}
                                />
                                </div>
                            </div>
                            <div className="row">
                                <div className=" col-sm-4 form-group">
                            <label>{this.props.t('subSource.formDetails.description')}</label>
                                    <textarea
                                        className="form-control" 
                                        type="text"
                                        name="description"
                                        id="description"
                                        value={editFormData.description || ''}
                                        onChange={this.handleChangeEvent}
                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className=" col-sm-4 form-group">
                                    <label>{this.props.t('subSource.formDetails.paidDetails')}</label>
                                    <span className="required">*</span>
                                    <Select
                                        id="paid_details"
                                        options={config.constants.paidDetails}
                                        name="paid_details"
                                        onChange={this.handleOptionChange.bind(this,'paid_details')}
                                        value={config.constants.paidDetails.filter(({ value }) => value === editFormData.paid_details)}
                                        
                                        
                                    />
                                    <span className="text-danger">{errors.paid_details || ''}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-4 ">
                                <InputField
                                inputProps={{
                                    id: "friendly_name",
                                    type: "text",
                                    name: "friendly_name",
                                    label: this.props.t('subSource.formDetails.friendlyName'),
                                    value: editFormData.friendly_name || '',
                                    dataerror: errors.friendly_name || '',
                                    validationreq: "yes"
                                }}
                                onChange={this.handleChangeEvent}
                                />
                                </div>
                            </div>
                            <div >
                                <label>{this.props.t('subSource.formDetails.AddedToDailer')} </label>
                                <div >
                                 <CheckBox type="checkbox" checked={(editFormData.added_to_l1 === '1') ? true : false} name="added_to_l1" id="added_to_l1"  onChange={this.handleChange.bind(this,'added_to_l1')} label={this.props.t('subSource.formDetails.addedToL1')}></CheckBox>
                                
                                 <CheckBox type="checkbox" checked={(editFormData.added_to_l2 === '1') ? true : false} name="added_to_l2" id="added_to_l2"  onChange={this.handleChange.bind(this,'added_to_l2')} label={this.props.t('subSource.formDetails.addedToL2')}></CheckBox>
                                </div>
                            </div>
                            <div className="row">
                                <div className=" col-sm-4 form-group">
                                    <label>{this.props.t('subSource.formDetails.language')}</label>
                                    <span className="required">*</span>
                                    <Select
                                        id="lang_id"
                                        options={config.constants.language}
                                        name="lang_id"
                                        onChange={this.handleOptionChange.bind(this,'lang_id')}
                                        value={config.constants.language.filter(({ value }) => Number(value) === Number(editFormData.lang_id))}
                                        
                                    />
                                    <span className="text-danger">{errors.lang_id || ''}</span>
                                </div>
                            </div>
                            {(editFormData.id) ?  
                            <div className="row form-group" >
                                <ul className="maindetail-list">
                                    <li className="maindetail-item">
                                        <div className="subheading">{this.props.t('subSource.formDetails.createdDate')}</div>
                                <div className="sub-value">{DateFormat(editFormData.created_at,"dd/mm/yyyy")}</div>
                                    </li>
                                    <li className="maindetail-item">
                                        <div className="subheading">{this.props.t('subSource.formDetails.createdBy')}</div>
                                        <div className="sub-value">{this.getUserName(editFormData,'added_by','username')}</div>
                                    </li>
                                    <li className="maindetail-item">
                                        <div className="subheading">{this.props.t('subSource.formDetails.updatedDate')}</div>
                                        <div className="sub-value">{(editFormData.updated_at) ? (DateFormat(editFormData.updated_at,"dd/mm/yyyy")) : ''}</div>
                                    </li>

                                </ul>
                            </div>
                            : ''}
                            
                            <div>
                                <label>{this.props.t('subSource.formDetails.status')} </label>
                                <div>  
                                    <label className="switch-btn btn btn-link ml-5" htmlFor="active" >
                                        <input checked={(editFormData.status === '0') ? false : true} className="switch-btn" id="active" value="open" name="active" type="checkbox" onChange={this.handleOptionChange.bind(this,'status')}  />
                                        <div className="slider round"></div>
                                        <span className="switch-label"></span>
                                        {(editFormData.status === "0") ? this.props.t('subSource.formDetails.inactive'):this.props.t('subSource.formDetails.active')}
                                    </label>
                                </div>
                            </div>
                            
                            <div className="row">
                                <div className=" col-sm-4 form-group text-center">
                            <button type="submit" onClick={this.submitForm} className="btn btn-primary">{this.props.t('subSource.formDetails.save')}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default withTranslation('source')(EditSubSource);
