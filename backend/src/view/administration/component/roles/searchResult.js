import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import RoleService from './../../../../service/roleService';
import DateFormat from 'dateformat';
import _  from 'lodash';

class SearchResult extends Component {
    roleData=[]
    constructor(props) {
        super(props);
        this.state = {
            roleListData: props.roleListData,
            loading: false,
            pageloading: false,
            statusClassname: {
                'active': 'nav-item active',
                'inactive': 'nav-item',
                'incomplete': 'nav-item',
                'blacklist': 'nav-item',
                'all': 'nav-item'
            },
            filterData: {
            },
            usersRecord:[],
            currentIndex: null,
            current_org_name: null
        };
        this.updateStatus = this.updateStatus.bind(this)
    }
    // componentWillReceiveProps(nextProps) {
    //     this.setState({
    //         roleListData: nextProps.roleListData,
    //         loading: nextProps.loading,
    //         pageloading: nextProps.pageloading
    //     });
    // }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        let updatedStateData = {};

        if(nextProps.roleListData){
            updatedStateData['roleListData'] = nextProps.roleListData;
        }

        if(nextProps.loading !== prevState.loading){
            updatedStateData['loading'] = nextProps.loading;
        }

        if(nextProps.pageloading !== prevState.pageloading){
            updatedStateData['pageloading'] = nextProps.pageloading;
        }

        return updatedStateData;

    }

    updateStatus(userData){
        if (typeof this.props.onUpdateStatus === 'function') {
            this.props.onUpdateStatus(userData);
        }
    }
    getUserName = (data,keyToMatch,keyToReturn) => {
        const user = _.find(this.state.usersRecord,{'id':data[keyToMatch]});
        if(user)
            return user[keyToReturn]
        else
            return null;
    }

    componentDidMount = async () => {
        await this.getUserList();
    }

    getUserList() {
        var thisObj = this;
        RoleService.getAllUserList()
        .then(function (response) {
            if (response.data.status === 200) {
                if (response.data.data.length) {
                    thisObj.setState({ usersRecord: response.data.data });
                }else {
                    thisObj.setState({ usersRecord: []});
                }
            }
        })
        .catch(function (response) {
            
        });
    }
    
    render() {
        return (
            <div className="table-responsive">
                    <table className="table table-bordered table-hover table-striped table-category">
                        <thead>
                            <tr>
                                <th>{this.props.t('List.details.role_id') }</th>
                                <th>{this.props.t('List.details.role_name') }</th>
                                <th>{this.props.t('List.details.created_date') }</th>
                                <th>{this.props.t('List.details.created_by') }</th>
                                <th>{this.props.t('List.details.updated_date') }</th>
                                <th>{this.props.t('List.details.updated_by') }</th>
                                <th>{this.props.t('List.details.status') }</th>
                                <th>{this.props.t('List.details.action') }</th>
                            </tr>
                        </thead>
                        <tbody>
                        {(this.state.roleListData.length === 0 && !this.state.loading) ? <tr><td align="center" colSpan="8"><h6 className="text-center text-danger text-bold">{this.props.t('List.Search_Role.No_Record_Found')}</h6></td></tr> : null}
                            {(this.state.roleListData && this.state.roleListData.length !== 0) && this.state.roleListData.map((roledata, k) =>
                            <tr key={k}>
                                <td>{roledata.id}</td>
                                <td>{roledata.name}</td>
                                <td>{DateFormat(roledata.created_at,"dd/mm/yyyy")}</td>
                                <td>{this.getUserName(roledata,'added_by','username')}</td>
                                <td>{(roledata.updated_at)? (DateFormat(roledata.updated_at,"dd/mm/yyyy")): ''}</td>
                                <td>{this.getUserName(roledata,'updated_by','username')}</td>
                                <td>
                                    <label className="switch-btn btn btn-link ml-5" htmlFor={"active"+roledata.id} >
                                        <input checked={(roledata.status === "1") ? true : false} className="switch-btn" id={"active"+roledata.id} value="open" name={"active"+roledata.id} type="checkbox"  onChange={this.updateStatus.bind(this,roledata)}  />
                                        <div className="slider round"></div>
                                        <span className="switch-label"></span>
                                        {(roledata.status === "1") ? this.props.t('formData.active'):this.props.t('formData.inactive')}
                                    </label>
                                </td>
                                <td>
                                    <div className="btn-group">
                                    <Link to={`/edit-role/${roledata.role_id_hash}`} className="btn btn-default"><i className="ic-createmode_editedit mrg-r5"></i></Link>
                                    </div>
                                </td>
                            </tr>
                            )}
                            {
                                (this.state.loading) ? <tr><td className="loading" colSpan="8"></td></tr> : null
                            }
                            
                        </tbody>
                    </table>
                </div>
                
            
        )
    }
}

 export default withTranslation('role') (SearchResult);
