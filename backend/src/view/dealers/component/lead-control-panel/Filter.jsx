import React, { Component } from 'react';
import Select from 'react-select';
// import CheckBox from '../../elements/CheckBox';
import * as config from '../../../../config/config';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { InventoryActions, DealerActions } from "../../../../store/action";
import { withTranslation } from 'react-i18next';
import { Multiselect } from 'multiselect-react-dropdown';
import CheckBox from '../../../elements/CheckBox';


class Filter extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = this.initialState();
        this.multiselectRef = React.createRef();
    }

    initialState = () => {
        return {
            filters: {
                cluster_id: [],
                city_id: [],
                dealer_id: [],
                boost_active: [],
                fulfilment_status: [],
                backend_boost: false,
                frontend_boost: false,
                dealers_backend_leads: false
            },
            loading: false,
            advanceSearchEnable: false,

        }
    }

    componentDidMount = () => {
        this._isMounted = true;
        this.getClusters();
        this.listCity();
        this.listDealers();
    }

    getClusters = () => {
        let postData = {};
        postData['isPaginationRequired']  = false;
        
        if(!this.props.clusterData.data){
            this.props.actions.getClusterList(postData);
        }
    }

    listCity = () => {
        if(!this.props.cityListData.data){
            this.props.actions.listCity();
        }
    }

    /**
     * List Dealers
     */

    listDealers = async () => {
        let params = {};
        params['_with'] = ['id', 'organization'];

        if(!this.props.listDealerData.data){

            await this.setState({loading: true});

            this.props.actions.listDealer(params).then(val=>{
                if(val['status'] === 200){
                    this.setState({loading: false});
                }
            }).catch(err=>{
                this.setState({loading: false});
            });
        }

    }

    /**
     * Handle select box change
     */
    handleChange =   param =>  e => {
        let val = (e.value) ? e : ((typeof e.target.checked !== 'undefined') ? e.target.checked : e.target.value) ;
        this.updateStateFun(param, val);
    }

    /**
     * Update state
     */
    updateStateFun = async (type, val) => {
        const updateState = this.state.filters;
        updateState[type] = val;

        await this.setState({filters: updateState});
    }

    ToogleAdvanceSearch = () => {
        let advanceSearchEnable = this.state.advanceSearchEnable;
        if (advanceSearchEnable) {
          this.setState({ advanceSearchEnable: false });
        } else {
          this.setState({ advanceSearchEnable: true });
        }
      };

    /**
     * On multi select option select
     */
    onSelect = type => (selectedList , selectedItem) => {
        this.updateStateFun(type, selectedList);
    }
    
    /**
     * On multi select option remove
     */
    onRemove = type => (selectedList , selectedItem) => {
        this.updateStateFun(type, selectedList);
    }

    /**
     * Filter Submit Handler
     */
    submitFilter =  (e) => {
        e.preventDefault();
        this.setState({ submitted: true });

        let postData = {};
        
        Object.keys(this.state.filters).forEach((el) => {
            let value = [];
            if(typeof this.state.filters[el] === 'object'){
                if(this.state.filters[el]['value']){
                    value = this.state.filters[el]['value'];
                }else{
                    (this.state.filters[el]).forEach((v, k)=>{
                        value.push(v.value || v.id || '');
                    })
                }

            }else{
                value = this.state.filters[el];
            }
            
            postData[el] = value;
        });
        
        //SUBMIT SEARCH FORM
        if(Object.keys(postData).length){
            this.props.submitLeadControlFilter(postData);
        }
    }

    /**
     * Reset Form
     */

    resetForm = () => {
        this.setState(this.initialState());
        this.multiselectRef.current.resetSelectedValues();
    }

    componentWillUnmount = () => {
        this._isMounted = false;
    }

    render() {
        const { cluster_id, city_id, boost_active, dealer_id, fulfilment_status } = this.state.filters;
        const { fulfilmentStatus, boostStatus } = config.constants;
        const { cityListData, clusterData, listDealerData } = this.props;

        return (
            <div className="search-wrap">
                <ul className="search-flex">
                    <li className="searchitems form-group ">
                        <label>{this.props.t('leadControl.cluster')}</label>
                        <Multiselect
                            id="cluster_id"
                            options={clusterData['data'] || []} // Options to display in the dropdown
                            selectedValues={cluster_id} // Preselected value to persist in dropdown
                            onSelect={this.onSelect('cluster_id')} // Function will trigger on select event
                            onRemove={this.onRemove('cluster_id')} // Function will trigger on remove event
                            displayValue="name"
                            ref={this.multiselectRef}
                        />
                    </li>
                    <li className="searchitems form-group ">
                        <label>{this.props.t('leadControl.city')}</label>
                        <Multiselect
                            id="city_ids"
                            options={(cityListData['data'] && cityListData['data']['city']) || []} // Options to display in the dropdown
                            selectedValues={city_id} // Preselected value to persist in dropdown
                            onSelect={this.onSelect('city_id')} // Function will trigger on select event
                            onRemove={this.onRemove('city_id')} // Function will trigger on remove event
                            displayValue="name"
                            ref={this.multiselectRef}
                        />
                    </li>
                    <li className="searchitems form-group ">
                        <label>{this.props.t('leadControl.dealer')}</label>
                        <Multiselect
                            id="dealer_ids"
                            options={listDealerData['data'] || []} // Options to display in the dropdown
                            selectedValues={dealer_id} // Preselected value to persist in dropdown
                            onSelect={this.onSelect('dealer_id')} // Function will trigger on select event
                            onRemove={this.onRemove('dealer_id')} // Function will trigger on remove event
                            displayValue="organization"
                            ref={this.multiselectRef}
                        />
                    </li>
                    <li className="searchitems form-group ">
                        <label>{this.props.t('leadControl.boost_active')}</label>
                        <Select
                            id="boost_active"
                            name="boost_active"
                            value={boost_active}
                            onChange={this.handleChange('boost_active')}
                            options={boostStatus}
                        />
                    </li>
                    <li className="searchitems form-group ">
                        <label>{this.props.t('leadControl.fulfillment_status')}</label>
                        <Select
                            id="fulfilment_status"
                            name="fulfilment_status"
                            value={fulfilment_status}
                            onChange={this.handleChange('fulfilment_status')}
                            options={fulfilmentStatus}
                        />
                    </li>

                    <li className="searchitems doublew form-group">
                        <label> &nbsp;</label>
                        <div>
                            <button type="button" onClick={this.submitFilter} className="btn btn-primary">{this.props.t('leadControl.search')}</button>
                        </div>
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems" : "searchitems hide"} >
                        <CheckBox type="checkbox" label="On Backend Boost" name="backend_boost" id="iussue-1" onChange={this.handleChange('backend_boost')} />
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems" : "searchitems hide"} >
                        <CheckBox type="checkbox" label="On Front End Boost" name="frontend_boost" id="iussue-1" onChange={this.handleChange('frontend_boost')} />
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems" : "searchitems hide"} >
                        <CheckBox type="checkbox" label="Dealers With Backend Leads as No" name="dealers_backend_leads" id="iussue-1" onChange={this.handleChange('dealers_backend_leads')} />
                    </li>
                </ul>
                <span  className="advancesearch btn btn-link"  onClick={this.ToogleAdvanceSearch} >
                    <span>{this.state.advanceSearchEnable ? "- Less Search" : "+ Advance Search"}</span>
                </span>
            </div>
        )
    }
}


const mapStateToProps = (state, ownProps) =>  ({
    clusterData: state.dealer.clusterData || {},
    cityListData: state.inventory.cityListData || {},
    listDealerData: state.dealer.dealerListData || {}
});

const mapDispatchToProps = dispatch => {
    return {
                actions: {
                    getClusterList: bindActionCreators(
                        DealerActions.getClusterList,
                        dispatch
                    ),
                    listCity: bindActionCreators(
                        InventoryActions.listCity,
                        dispatch
                    ),
                    listDealer: bindActionCreators(
                        DealerActions.listDealer,
                        dispatch
                    )
               }
            }
}
export default  withTranslation('common') (connect(mapStateToProps, mapDispatchToProps)(Filter));