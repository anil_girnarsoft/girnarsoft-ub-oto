import React, { Component } from 'react';
import InventoryFilter from './Inventory-Filter';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { InventoryActions } from "../../../../store/action";
import { withTranslation } from 'react-i18next';
// import * as config from '../../../../config/config';
import { withRouter } from "react-router-dom";
import FilterTable from './filter-table';

class Inventory extends Component {
    // formRef = React.createRef();

    constructor(props) {
        super(props);
        this.state = {
            inventoryList :[],
            page: 1,
            runPaginationFunc : false,
            loading: false
        }
    }

    /**
     * Listing API Handler
     */

    handleSearchSubmit = async (data={}, pageNo=1) => {
        await this.setState({page:pageNo});
        if(data['action'] && data['action'] === 'resetAll'){
            await this.setState({loading: false, runPaginationFunc: false, inventoryList: [], pagination:[]});
            return;
        }

        if(pageNo === 1){
            await this.setState({inventoryList: []});
        } 
        //////////////// MAP DATA TO REQUEST DATA HERE /////////////////
        
        let requestData = {}
        let filterParams = {
            'keyword'  : 'search_text',
            'car_model': 'car_model',
            'car_status': 'car_status',
            'fuel_type_id': 'fuel_type_id',
            'max_car_price': 'max_price',
            'max_km_driven': 'max_km_driven',
            'max_make_year': 'max_make_year',
            'min_car_price': 'min_price',
            'min_km_driven': 'min_km_driven',
            'min_make_year': 'min_make_year',
            'uc_transmission_id': 'uc_transmission_id',
            'make' : 'car_make',
            'model' : 'car_model',
            'owner_type' : 'owner_type',
            'certification_status' : 'certification_status',
            'city' : 'city_ids',
            'dealer' : 'dealer_ids',
            'inventory_age' : 'car_age_filter',
            'color' : 'uc_colour_id',
            'body_type': 'body_type'
        }
        for(let i in data){
            if(filterParams.hasOwnProperty(i)){
                requestData[filterParams[i]] =  data[i]
            }
        }

        // console.log(requestData)
        ////// appending default aprams ////

        requestData['sort_on']='price';
        requestData['order_by']='DESC';
        requestData['page_no']=pageNo;
        if(requestData['dealer_ids'] && requestData['dealer_ids'].length){
            requestData['source_id'] = 1
        }
        if(requestData['uc_colour_id']){
            requestData['uc_colour_id'] = [requestData['uc_colour_id']]
        }

        let updatedPostData = {};

        Object.keys(requestData).forEach(el=>{
            if(requestData[el] && typeof requestData[el] === 'object' && requestData[el].length){
                updatedPostData[el] = requestData[el]
            } 
            else if(requestData[el] && typeof requestData[el] !== 'object'){
                updatedPostData[el] = requestData[el]
            } 
        });

        this.setState({runPaginationFunc: false, loading: true, inventoryList: ((this.state.page!==1) ? this.state.inventoryList: []) , pagination: this.state.pagination});
        //DISPATCH ACTION
        this.props.actions.submitFilter(updatedPostData).then(resp=>{
            this.setState({loading: false, runPaginationFunc: true});
            if(resp.status===200){
                if(resp.data.length){
                   this.setState({ inventoryList: this.state.inventoryList.concat(resp.data), pagination: ((resp.pagination && resp.pagination) || {})})
                }else{
                    let paginate = (this.state.page === 1) ? {} : {...this.state.pagination}
                   this.setState({ page:pageNo-1, pagination: paginate})
                }
            }
        }).catch(err=>{
            this.setState({loading: false, runPaginationFunc: true})
        });

    }

    componentDidMount = () => {
        this._isMounted = true;
        this.handleSearchSubmit();
        window.addEventListener('scroll', this.onScroll);
    }

    onScroll = (event) => {
        if ((window.innerHeight + window.scrollY > document.body.offsetHeight - 300)) {
            //Handling multiple API call on single scroll & if total results are showing
            if (this.state.runPaginationFunc && this.props.pagination !== this.state.inventoryList.length) {
                let current_page = this.state.page;
                let loading=false;

                if(current_page!==0) {
                    loading = true;
                }

                if(this._isMounted){
                    this.setState({ page: current_page + 1,loading:loading}, (v) => {
                        //TRIGGER FILTER COMPONET SUBMIT HANDLER
                        if(this.child) this.child.submitFilter(this.state.page, 'pagination');
                    });
                }
            }
        }
    }


    componentWillUnmount() {
        this._isMounted = false;
    }
    
    render() {
        const {  inventoryList, loading , pagination} = this.state;

        return (
            <div className="inventory-wraper">
                <div className="left-bar">
                    <InventoryFilter childRef={ref => (this.child = ref)} onSearchSubmit={this.handleSearchSubmit} dataToChild={this.state.loading}/>
                </div>
                <div className="right-bar">
                    <FilterTable filterTableData={{'inventoryList': inventoryList, 'pagination': pagination, loading: loading}} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    inventoryList: state.inventory.inventoryList,
    pagination: state.inventory.pagination,
});

const mapDispatchToProps = dispatch => {
    return {
                actions: {
                    submitFilter: bindActionCreators(
                        InventoryActions.submitFilter,
                        dispatch
                    )
                }
    }
}
export default  withTranslation('common') (withRouter(connect(mapStateToProps, mapDispatchToProps)(Inventory)));