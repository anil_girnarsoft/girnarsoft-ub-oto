import React, { Component } from "react";

class DealerDeliveryResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
        searchResult:props.searchResult,
        loading: false,
        page: 1,
        runPaginationFunc: false,
        totalLengthWithoutPagination: 0,
    }
  }
  static getDerivedStateFromProps(nextprops, prevState) {
    var updatedObj = {}
    if (prevState.searchResult !== nextprops.searchResult) {
      updatedObj['searchResult'] = [...prevState.searchResult, ...nextprops.searchResult];
    }
    return updatedObj;
  }

  componentDidMount = () => {
    this._isMounted = true;
    window.addEventListener('scroll', this.onScroll);
  }

  onScroll = (event) => {
    if ((window.innerHeight + window.scrollY > document.body.offsetHeight - 300)) {
        try {
            //Handling multiple API call on single scroll & if total results are showing
            if (!this.state.runPaginationFunc && this.state.totalLengthWithoutPagination !== this.state.searchResult.length) {
                const { page } = this.state;
                let loading = false;

                if (page !== 1) {
                    loading = true;
                }
                
                this.props.parentCallback({ page_number: page+1 });
                // if (this._isMounted) {
                    this.setState({ page: page + 1, loading: loading, runPaginationFunc: true });
                // }
            }
        } catch (err) {
            //
        }

    }
  } 
  render() {
    const {searchResult, loading} = this.state;
    return (
      <div className="result-wrap">
        <div className="card">
          <div className="card-heading">
            <div className="row">
              <div className="col-sm-6">
                <h2 className="card-title pad-t7"></h2>
              </div>
              {/* <div className="col-sm-6 text-right">
                            <button type="button" name="refresh" id="refresh" className="btn btn-link mrg-r15">Refresh</button>
                            <a href="/add-lead"><button type="button" name="addlead" id="addlead" className="btn btn-success">Add Lead</button></a>
                        </div> */}
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-bordered table-hover table-striped deliveryTable">
              <thead>
                <tr>
                  <th rowSpan="2" colSpan="1">
                    Cluster
                  </th>
                  <th rowSpan="2" colSpan="1">
                    City
                  </th>
                  <th rowSpan="2" colSpan="1">
                    Dealer Id
                  </th>
                  <th rowSpan="2" colSpan="1">
                    GSD Code
                  </th>
                  <th rowSpan="2" colSpan="1">
                    Dealer
                  </th>
                  <th rowSpan="2" colSpan="1">
                    Dealer Status
                  </th>
                  <th rowSpan="2" colSpan="1">
                    Total Car
                  </th>
                  <th colSpan="4">Lead Sent</th>
                  <th rowSpan="2" colSpan="1">
                    Walk-ins scheduled
                  </th>
                  <th rowSpan="2" colSpan="1">
                    Walk-ins Done
                  </th>
                  <th rowSpan="2" colSpan="1">
                    Booked
                  </th>
                  <th rowSpan="2" colSpan="1">
                    Conversions
                  </th>
                  <th rowSpan="2" colSpan="1">
                    {" "}
                    8KM
                  </th>
                  <th rowSpan="2" colSpan="1">
                    Priority
                  </th>
                </tr>

                <tr>
                  <th>Organic Lead & 50 km</th>
                  <th>Organic Lead 50 km</th>
                  <th>Backend</th>
                  <th>Total</th>
                </tr>
              </thead>

              <tbody>
                {
                  searchResult.length
                  ?
                  searchResult.map(el=> {
                    return (
                      <tr key={el.id}>
                        <td>{el.cluster_name}</td>
                        <td>{(el.city_data && el.city_data['name']) || ''}</td>
                        <td>{el.id || ''}</td>
                        <td>{el.gcd_code || ''}</td>
                        <td>{el.organization || ''}</td>
                        <td>{(el.status==='1') ? 'Active' : 'InActive'}</td>
                        <td>{el.total_car}</td>
                        <td>{el.sumOrganic}</td>
                        <td>{el.sumOrganicGreater100}</td>
                        <td>{el.sumBackend}</td>
                        <td>{el.totalLead}</td>
                        <td>{el.sumWalkinSchedule}</td>
                        <td>{el.sumWalkinDone}</td>
                        <td>{el.sumBooked}</td>
                        <td>{el.sumConversion}</td>
                        <td>{el.sumDistLead + (el.sumBackend ? "("+((el['sumDistLead'] / el['sumBackend']) * 100, 2).toFixed(2) + "%)" : "") }</td>
                        <td>{el.priority || ''}</td>
                      </tr>)

                  })
                  :
                  (
                    (!searchResult.length && !loading) 
                      ?
                      <tr><td colSpan="17" className="text-center">No Record Found</td></tr>
                      :
                      <tr></tr>
                  )
                }
             </tbody>
            </table>

             {
                  (loading) ? <div className="loading" ></div> : ''
              }
          </div>
        </div>
      </div>
    );
  }
}
export default DealerDeliveryResult;
