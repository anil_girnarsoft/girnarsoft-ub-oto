import React, { Component } from 'react';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import CheckBox from './../../../elements/CheckBox';
import "react-datepicker/dist/react-datepicker.css";
import * as helper from '../../../../config/helper';
import * as config from './../../../../config/config';
//import { withTranslation } from 'react-i18next';
const status = [
    { value: '1', label: 'Active' },
    { value: '2', label: 'Inactive' },
    { value: '3', label: 'Both' },
  ];
  
class SearchDealerDelivery extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
        selectedOption: null,
        date: new Date(),
        advanceSearchEnable: false,
        formData: {},
        filterData: props.prefilledData,
        citieslist:[],
        clusterlist:[],
        dealerslist:[],
        citieslistClusters:[],
        allClusters:[],
        // page_no:1,
        loading:false
      };
    }
    ToogleAdvanceSearch = () => {
        let advanceSearchEnable = this.state.advanceSearchEnable;
        if (advanceSearchEnable) {
          this.setState({ advanceSearchEnable: false });
        } else {
          this.setState({ advanceSearchEnable: true });
        }
      };

      //onChange = date => this.setState({ date })
      handleDateChange = (selectedKey,date) =>{
        let formData = {...this.state.formData};
        formData[selectedKey] = date;
        this.setState({ formData:formData });
    }
      handleChange = (key, event) => {
       
        let formData = {...this.state.formData};
       
        formData[key] = event;
        
        this.setState({ formData: formData });   
        
        if(key === 'cluster'){
            this.listCities({type:'cluster', value: [event.value]});
        }
        
      };
      searchFilter = async() => {
          await this.setState({loading:true})
        //let formValid = await this.validateSearchInput();
        if(typeof this.props.getFilterList === 'function') {
            this.props.getFilterList(this.state.formData)
        }        
    }


    /**
     * List cities
     */

    listCities = async (param) => {
      

            let clusterIds = param['value']; //GET SELECTED CLUSTER
            let cityListObj = [];

            //GET CITIES FROM SELECTED CLUSTER IDs
            await this.state.allClusters.forEach(elm => {
                if(clusterIds.includes(elm.id))  cityListObj = [...cityListObj, ...elm.cityObj];
            })
            //NOW UPDATE CITY DROPDOWN 
            await this.setState({citieslistClusters: cityListObj});

    }


    runPaginateFunction = () => { 
        this.props.getFilterList(this.state.formData)
    }

    static getDerivedStateFromProps(nextprops, prevState) {
        var updatedObj = {}
        // if (prevState.citieslist !== nextprops.prefilledData.cities) {
        //     updatedObj['citieslist'] = helper.getDataInKeyValueFormat(nextprops.prefilledData.cities, 'city');
        // }
        if (prevState.clusterlist !== nextprops.prefilledData.cluster) {
            updatedObj['allClusters'] = nextprops.prefilledData.cluster;
            updatedObj['clusterlist'] = helper.getDataInKeyValueFormat(nextprops.prefilledData.cluster, 'cluster');
        }
        if (prevState.dealerslist !== nextprops.prefilledData.dealer) {
            updatedObj['dealerslist'] = helper.getDataInKeyValueFormat(nextprops.prefilledData.dealer, 'dealer');
        }
        if(prevState.loading !== nextprops.paginationData.loading){
            updatedObj['loading'] = nextprops.paginationData.loading;

        }
        return updatedObj;
    }

    render() {
        const {formData,citieslistClusters,clusterlist,dealerslist,loading}  = this.state;
        const {REPORT_TYPE,DEALER_TYPE,DEALER_REPORT_QUICKFILTER,DEALER_PRIORITY} = config.constants;
        
        return (
            
            <div className="search-wrap">
                <ul className="search-flex">
                   <li className="searchitems">
                        <label>Cluster</label>
                        <Select
                            id="cluster"
                            name="cluster"
                            value={formData.cluster}
                            onChange={this.handleChange.bind(this,'cluster')}
                            options={clusterlist}
                        />
                    </li>
                   <li className="searchitems">
                        <label>City</label>
                        <Select
                            id="city"
                            name="cityId"
                            value={formData.city}
                            onChange={this.handleChange.bind(this,'cityId')}
                            options={citieslistClusters}
                        />
                    </li>
                   <li className="searchitems">
                        <label>Dealer</label>
                        <Select
                            id="dealer"
                            value={formData.dealer}
                            onChange={this.handleChange.bind(this, 'dealer')}
                            options={dealerslist}
                        />
                    </li>
                    <li className="searchitems doublew form-group">
                        <div className="row">
                            <div className="col-xs-6">
                                <label>Start Date</label>
                                <DatePicker
                                    id='created_from_date'
                                    className="form-control"
                                    onChange={this.handleDateChange.bind(this,'created_from_date')}
                                    selected={this.state.formData.created_from_date}
                                    dateFormat={config.constants.dateFormatDMY}                                    
                                    onKeyDown={e => e.preventDefault()}
                                />                                
                            </div>
                            <div className="col-xs-6">
                            <label>End Date</label>
                                <DatePicker
                                    id='created_to_date'
                                    className="form-control"
                                    onChange={this.handleDateChange.bind(this,'created_to_date')}
                                    selected={this.state.formData.created_to_date}
                                    dateFormat={config.constants.dateFormatDMY}
                                    onKeyDown={e => e.preventDefault()}
                                />
                            </div>
                        </div>
                    </li>

                   <li className="searchitems">
                        <label>Dealer Status</label>
                        <Select
                            id="status"
                            name="status"
                            value={formData.status}
                            onChange={this.handleChange.bind(this, 'status')}
                            options={status}
                        />
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                        <label>Priority</label>
                        <Select
                            id="priority"
                            name="priority"
                            value={formData.priority}
                            onChange={this.handleChange.bind(this, 'priority')}
                            options={DEALER_PRIORITY}
                        />
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                        <label>Report Type</label>
                        <Select
                            id="reporttype"
                            name="reporttype"
                            value={formData.reporttype}
                            onChange={this.handleChange.bind(this, 'reporttype')}
                            options={REPORT_TYPE}
                        />
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                        <label>Dealer Type</label>
                        <Select
                            id="dealertype"
                            name="dealertype"
                            value={formData.dealertype}
                            onChange={this.handleChange.bind(this, 'dealertype')}
                            options={DEALER_TYPE}
                        />
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                        <label>Quick Filters</label>
                        <Select
                            id="quickfilter"
                            name="quickfilter"
                            value={formData.quickfilter}
                            onChange={this.handleChange.bind(this, 'quickfilter')}
                            options={DEALER_REPORT_QUICKFILTER}
                        />
                    </li>
                    <li className={ this.state.advanceSearchEnable ? "searchitems doublew form-group " : "searchitems doublew form-group hide"} >
                        <label></label>
                        <CheckBox type="checkbox" name="Premium" id="Premium" label="Premium"></CheckBox>
                    </li>
                    
                    <li className="searchitems form-group">
                        <label> &nbsp;</label>
                        <div>
                            {
                                !loading
                                ?
                                <button type="submit" className="btn btn-primary mrg-r15 undefined" onClick={this.searchFilter}>Search</button>
                                :
                                <button type="submit" className="btn btn-primary mrg-r15 undefined" disabled>Search</button>

                            }
                            <button type="reset" className="btn btn-default btn-reset">Reset</button>
                        </div>
                    </li>
                </ul>
                <span  className="advancesearch btn btn-link"  onClick={this.ToogleAdvanceSearch} >
                    <span>{this.state.advanceSearchEnable ? "- Less Search" : "+ Advance Search"}</span>
                </span>
            </div>
        );
    }
}

export default SearchDealerDelivery;
