import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import * as helper from '../../../../config/helper';
import * as config from './../../../../config/config';
import { withTranslation } from 'react-i18next';
import _ from 'lodash';
import { Multiselect } from 'multiselect-react-dropdown';

class SearchL1Calling extends Component {
    constructor(props) {
        super(props);
        this.state = {
        selectedOption: null,
        date: new Date(),
        formData: {
            added_date_from: new Date().toISOString().split('T')[0],
            added_date_to: new Date().toISOString().split('T')[0]
        },
        filterData: props.prefilledData,
        clusterlist:[]
      };
    }

    componentDidMount = () => {
        this.searchReport();
    }
    
    handleDateChange = (selectedKey) => async e => {
        let formData = {...this.state.formData};
        formData[selectedKey] = e ? await helper.dateFormat(e) : '';
        this.setState({ formData:formData });
    }
    handleChange = (key, event) => {
        let formData = {...this.state.formData};
        formData[key] = event.value
        this.setState({ formData: formData });      
        
      };
      searchFilter = async() => {
        //let formValid = await this.validateSearchInput();
        if(typeof this.props.getFilterList === 'function') {
            this.props.getFilterList(this.state.formData)
        }        
    }
    static getDerivedStateFromProps(nextprops, prevState) {
        var updatedObj = {}
        if (prevState.clusterlist !== nextprops.prefilledData.cluster) {
            updatedObj['clusterlist'] = helper.getDataInKeyValueFormat(nextprops.prefilledData.cluster, 'cluster');
        }
        return updatedObj;
    }

    onSelect(key,selectedList, selectedItem) {
        let formData = this.state.formData;
        let existingVal = (formData[key]) ? ((typeof formData[key] === 'string') ? formData[key].split(',') : formData[key]) : []
        existingVal.push(selectedItem.value);
        formData[key] = existingVal;
        this.setState({ formData: formData });
        
        
    }
     
    onRemove(key,selectedList, removedItem) {
        let formData = this.state.formData;
        let existingVal = (formData[key]) ? formData[key] : [];
        existingVal = _.filter(existingVal, function(currentVal) {
            return currentVal !== removedItem.value;
        });
        formData[key] = existingVal;
        this.setState({ formData: formData });
        
    }

    searchReport = ()=>{
        let formData = this.state.formData;

        let postData = {}
        Object.keys(formData).forEach(el=>{
            if(formData[el] && formData[el].length){
                postData[el] = formData[el]
            }
        })

        if(typeof this.props.getReportData === 'function'){
            this.props.getReportData(postData)
        }
        
    }

    render() {
        const {formData,clusterlist}  = this.state;
        return (
            
            <div className="search-wrap">
            <ul className="search-flex">
            
                
                <li className="searchitems doublew form-group" >
                    <label>{this.props.t('locationWiseLead.search.cluster')}</label>
                    <Multiselect
                        id='cluster'
                        options={clusterlist}
                        onSelect={this.onSelect.bind(this,'cluster')} 
                        onRemove={this.onRemove.bind(this,'cluster')} 
                        displayValue="label" 
                        showCheckbox={true}
                        closeOnSelect={false}
                    />
                </li>
                
                <li className="searchitems doublew form-group">
                    <label>{this.props.t('locationWiseLead.search.startDate')}</label>
                    <DatePicker 
                        className="form-control"
                        onChange={this.handleDateChange('added_date_from')}
                        // selected={(formData && formData.added_date_from) ? formData.added_date_from : this.state.date}
                        dateFormat={config.constants.dateFormatDMY}
                        // value={formData.added_date_from}                           
                        onKeyDown={e => e.preventDefault()}
                        placeholderText={this.props.t('locationWiseLead.from')}
                        selected={formData.added_date_from ? new Date(formData.added_date_from) : null}
                        isClearable
                    />  
                </li>
                <li className="searchitems doublew form-group">
                    <label>{this.props.t('locationWiseLead.search.endDate')}</label>
                    <DatePicker 
                        className="form-control"
                        onChange={this.handleDateChange('added_date_to')}
                        // selected={(formData && formData.added_date_to) ? formData.added_date_to : this.state.date}
                        dateFormat={config.constants.dateFormatDMY}
                        // value={formData.added_date_to}
                        onKeyDown={e => e.preventDefault()}
                        placeholderText={this.props.t('locationWiseLead.to')}
                        selected={formData.added_date_to ? new Date(formData.added_date_to) : null}
                        isClearable
                
                    />
                </li>
                
                <li className="searchitems form-group">
                    <label> &nbsp;</label>
                    <div>
                        <button type="submit" className="btn btn-primary mrg-r15 undefined" onClick={this.searchReport.bind(this)}>{this.props.t('locationWiseLead.search.search')}</button>
                    </div>
                </li>
            </ul>
            
        </div>
        );
    }
}

export default withTranslation('report')(SearchL1Calling);
