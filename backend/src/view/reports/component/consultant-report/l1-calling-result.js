import React, { Component } from "react";
import { withTranslation } from 'react-i18next';

class L1CallingResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
        searchResult:props.searchResult,
        totalRecord:props.totalRecord,
        loading: false,
        pageloading: false,
        totalCalculationObject:props.totalCalculationObject
    }
  }
  static getDerivedStateFromProps(nextprops, prevState) {
    var updatedObj = {}
    if (prevState.searchResult !== nextprops.searchResult) {
      updatedObj['searchResult'] = nextprops.searchResult;
    }
    if (prevState.totalRecord !== nextprops.totalRecord) {
        updatedObj['totalRecord'] = nextprops.totalRecord;
    }
    if(nextprops.loading !== prevState.loading){
        updatedObj['loading'] = nextprops.loading;
    }

    if(nextprops.pageloading !== prevState.pageloading){
        updatedObj['pageloading'] = nextprops.pageloading;
    }

    if(nextprops.totalCalculationObject !== prevState.totalCalculationObject){
        updatedObj['totalCalculationObject'] = nextprops.totalCalculationObject;
    }

    return updatedObj;
}


  render() {
    const {totalCalculationObject} = this.state;
    return (
      <div className="result-wrap">
                    <div className="card">
                        <div className="card-heading">
                            <div className="row">
                                <div className="col-sm-6">
                                    <h2 className="card-title pad-t7"> {this.state.totalRecord+' '+this.props.t('salesFunnel.resultsFound')}</h2>
                                </div>
                            </div>
                        </div>
                        <div className="table-responsive">
                            <table className="table table-bordered table-hover table-striped deliveryTable">
                                <thead>
                                    <tr>
                                        <th>{this.props.t('l1Calling.agentName')}</th>
                                        <th>{this.props.t('l1Calling.cluster')}</th>
                                        <th>{this.props.t('l1Calling.totalLead')}</th>
                                        <th>{this.props.t('l1Calling.contactedLeads')}</th>
                                        <th>{this.props.t('l1Calling.verified')}</th>
                                        <th>{this.props.t('l1Calling.walkinScheduled')}</th>
                                        <th>{this.props.t('l1Calling.walkinDone')}</th>
                                        <th>{this.props.t('l1Calling.conversionAssist')}</th>
                                        <th>{this.props.t('l1Calling.WalkinAssist')}</th>
                                        <th>{this.props.t('l1Calling.closed')}</th>
                                        <th>{this.props.t('l1Calling.location')}</th>
                                        <th>{this.props.t('l1Calling.language')}</th>
                                        
                                    </tr>
                                </thead>

                                <tbody>
                                {(this.state.searchResult.length === 0 && !this.state.loading) ? <tr><td align="center" colSpan="12"><h6 className="text-center text-danger text-bold">{this.props.t('salesFunnel.NoRecordFound')}</h6></td></tr> : null}
                                {(this.state.searchResult && this.state.searchResult.length !== 0) && this.state.searchResult.map((reportData, k) =>
                                    <tr key={k}>
                                        <td>{(reportData.name) ? reportData.name : ''}</td>
                                        <td>{(reportData.cluster_name) ? reportData.cluster_name : ''}</td>
                                        <td>{(reportData.totalLead) ? reportData.totalLead : ''}</td>
                                        <td>{(reportData.totalContactedLead) ? reportData.totalContactedLead : ''}</td>
                                        <td>{(reportData.totalVerifiedLead) ? reportData.totalVerifiedLead : ''}{(reportData.totalVerifiedLead && reportData.totalContactedLead) ? ' ('+((reportData.totalVerifiedLead/reportData.totalContactedLead)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>{(reportData.totalWalkinSchd) ? reportData.totalWalkinSchd : ''}{(reportData.totalVerifiedLead && reportData.totalWalkinSchd) ? ' ('+((reportData.totalWalkinSchd/reportData.totalVerifiedLead)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>{(reportData.totalWalkinDone) ? reportData.totalWalkinDone : ''}{(reportData.totalVerifiedLead && reportData.totalWalkinDone) ? ' ('+((reportData.totalWalkinDone/reportData.totalVerifiedLead)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>{(reportData.totalConversionAssist) ? reportData.totalConversionAssist : ''}{(reportData.totalVerifiedLead && reportData.totalConversionAssist) ? ' ('+((reportData.totalConversionAssist/reportData.totalVerifiedLead)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>{(reportData.totalWalkingAssist) ? reportData.totalWalkingAssist : ''}{(reportData.totalVerifiedLead && reportData.totalWalkingAssist) ? ' ('+((reportData.totalWalkingAssist/reportData.totalVerifiedLead)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>{(reportData.totalClose) ? reportData.totalClose : ''}{(reportData.totalClose && reportData.totalLead) ? ' ('+((reportData.totalClose/reportData.totalLead)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>{(reportData.totalLocVerifiedLead) ? reportData.totalLocVerifiedLead : ''}{(reportData.totalVerifiedLead && reportData.totalLocVerifiedLead) ? ' ('+((reportData.totalLocVerifiedLead/reportData.totalVerifiedLead)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>{(reportData.totalLangVerifiedLead) ? reportData.totalLangVerifiedLead : ''}{(reportData.totalVerifiedLead && reportData.totalLangVerifiedLead) ? ' ('+((reportData.totalLangVerifiedLead/reportData.totalVerifiedLead)*100).toFixed(2)+'%)' : ''}</td>
                                        
                                    </tr>
                                    )}
                                    {
                                        (this.state.loading) ? <tr><td className="loading" colSpan="12"></td></tr> : null
                                    }
                                    {(this.state.searchResult.length) ?
                                    <tr className="totalvalue">
                                        <td>{this.props.t('salesFunnel.total')}</td>
                                        <td></td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalLead) ? totalCalculationObject.sumTotalLead : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalContacted) ? totalCalculationObject.sumTotalContacted : ''}</td>
                                    <td>{(totalCalculationObject && totalCalculationObject.sumTotalVerifiedLead) ? totalCalculationObject.sumTotalVerifiedLead : ''} { (totalCalculationObject.sumTotalVerifiedLead && totalCalculationObject.sumTotalContacted) ? ' ('+(totalCalculationObject.sumTotalVerifiedLead && totalCalculationObject.sumTotalContacted ? ((totalCalculationObject.sumTotalVerifiedLead/totalCalculationObject.sumTotalContacted)*100).toFixed(2)+'%' : '') +')' : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalWalkinSchd) ? totalCalculationObject.sumTotalWalkinSchd : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalWalkinDone) ? totalCalculationObject.sumTotalWalkinDone : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalConversionAssist) ? totalCalculationObject.sumTotalConversionAssist : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalWalkinAssist) ? totalCalculationObject.sumTotalWalkinAssist : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalClose) ? totalCalculationObject.sumTotalClose : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalLoc) ? totalCalculationObject.sumTotalLoc : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalLang) ? totalCalculationObject.sumTotalLang : ''}</td>
                                    </tr> : null }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
    );
  }
}
export default withTranslation('report') (L1CallingResult);
