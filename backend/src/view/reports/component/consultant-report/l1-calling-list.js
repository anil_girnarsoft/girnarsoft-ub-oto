import React, { Component } from 'react';
import SearchL1Calling from './search-l1-calling';
import L1CallingResult from './l1-calling-result';
//import InputField from '../../../elements/InputField';
import ReportService from './../../../../service/reportService';
import { bindActionCreators } from 'redux';
import {withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { LeadActions } from "../../../../store/action";
import { CSVLink } from "react-csv";
import _ from 'lodash';

class L1CallingList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            saveFilter: false,
            reportData: [],
            searchByList: [],
            page: 0,
            totalRecord:0,
            filterData: {},
            loading:false,
            pageloading:true,
            next_page:false,
            allFliterData: {
                cluster: [],
            },
            searchResult: [],
            totalCalculationObject:{},
            CsvJsonData:[]
        }
        this.handleChangetext = this.handleChangetext.bind(this);
        this.onScroll = this.onScroll.bind(this);
    }

    componentDidMount = async () => {
        this._isMounted = true;
        this._isMounted = true;
        this.onScroll('Mounter');
        window.addEventListener('scroll', this.onScroll);
        // await this.getReportData({})
        await this.getClusterList();
        
    }

    onScroll(event) {
        if ((window.innerHeight + window.scrollY > document.body.offsetHeight - 300)) {
            if (this.flag) {
                let current_page = this.state.page;
                let loading=false,pageloading=false;
                if(current_page===0) {
                    pageloading = true;
                }else{
                    loading = true;
                }
                if(this._isMounted && this.state.next_page){
                    this.setState({ page: current_page + 1,loading:loading,pageloading:pageloading}, () => {
                        this.getReportData();
                    });
                }
            }
            this.flag = false;
        }
    }

    handleChangetext = () => {
        // let formData = { ...this.state.formData };   
    }
    ToogleSaveFilter = () => {
        this.setState({ saveFilter: event.target.value });
    };
    
    getClusterList = async() => {
        const {allFliterData} = this.state;
        let result = await this.props.actions.getClusterList({});
        let clusterList = []
        if (result && result.status === 200) {
            clusterList = (result && result.data && result.data.length) ? result.data : []
            allFliterData.cluster = clusterList
        }
        this.setState({ allFliterData: allFliterData });
       
    }

    getReportData = async (searchParams) => {
        var thisObj = this;
        var filterData = thisObj.state.filterData;
        filterData.page_number = (thisObj.state.page === 0) ? 1 : thisObj.state.page;
        thisObj.setState({page:filterData.page_number,loading:true})
        ReportService.getL1CallingReport(filterData)
            .then(function (response) {
                thisObj.flag = false;
                thisObj.setState({loading:false,pageloading:false})
                if (response.data.status === 200) {
                    if (response.data.data.length) {
                        let reportData = response.data.data;
                        let pagination = (response.data && response.data.pagination) ? response.data.pagination : {}
                        let next_page = (pagination) ? pagination.next_page : false
                        let totalRecord = (pagination) ? pagination.total : 0
                        if (thisObj.state.page <= 1) {
                            thisObj.setState({ reportData: reportData ,loading:false,pageloading:false,next_page:next_page,totalRecord:totalRecord}, () => {
                                thisObj.flag = true;
                            });
                        }
                        else {
                            thisObj.setState({
                                reportData: thisObj.state.reportData.concat(reportData)
                            ,loading:false,pageloading:false,next_page:next_page,totalRecord:totalRecord}, () => {
                                thisObj.flag = true;
                            });
                        }
                        thisObj.getTotalObject();
                        thisObj.setJSONObjectForReport();
                    } else {
                        if (thisObj.state.page <= 1) {
                            thisObj.setState({ reportData: [],next_page:false,totalRecord:0});
                        }
                    }
                }
            })
            .catch(function (response) {
                thisObj.setState({loading:false,pageloading:false})
            });
    };

    getFilterReport = async(searchParam) => {
        await this.setState({filterData : searchParam});
        this.getReportData();
    }

    getTotalObject = async()=>{
        let {reportData,totalCalculationObject} = this.state;
        totalCalculationObject['sumTotalLead'] = await this.calculateTotal('totalLead',reportData);
        totalCalculationObject['sumTotalContacted'] = await this.calculateTotal('totalContactedLead',reportData);
        totalCalculationObject['sumTotalVerifiedLead'] = await this.calculateTotal('totalVerifiedLead',reportData);
        totalCalculationObject['sumTotalWalkinSchd'] = await this.calculateTotal('totalWalkinSchd',reportData);
        totalCalculationObject['sumTotalWalkinDone'] = await this.calculateTotal('totalWalkinDone',reportData);
        totalCalculationObject['sumTotalConversionAssist'] = await this.calculateTotal('totalConversionAssist',reportData);
        totalCalculationObject['sumTotalWalkinAssist'] = await this.calculateTotal('totalWalkingAssist',reportData);
        totalCalculationObject['sumTotalClose'] = await this.calculateTotal('totalClose',reportData);
        totalCalculationObject['sumTotalLoc'] = await this.calculateTotal('totalLocVerifiedLead',reportData);
        totalCalculationObject['sumTotalLang'] = await this.calculateTotal('totalLangVerifiedLead',reportData);
        
        this.setState({totalCalculationObject});
         
    }

    calculateTotal = async(key,reportData) =>{
        let total=0
        let totalAllLeads = await (reportData && reportData.length && reportData).map(reportObj=>{
            return total += +(reportObj[key])
        })
        let totalVal = (totalAllLeads && totalAllLeads.length) ? totalAllLeads[totalAllLeads.length - 1] : 0;
        return totalVal;
    }

    setJSONObjectForReport=()=>{
        
        let reportList = Object.assign({},this.state.reportData);
        let reportObj={},LeadsList=[];
        _.forEach(reportList,(reportData)=>{
            reportObj={}
            reportObj['Agent Name']=(reportData.name) ? reportData.name : '';
            reportObj['Cluster']=(reportData.cluster_name) ? reportData.cluster_name : ''
            reportObj['Total leads']=(reportData.totalLead) ? reportData.totalLead : '';
            reportObj['Contacted Leads']=(reportData.totalContactedLead) ? reportData.totalContactedLead : '';
            reportObj['Verified']=(reportData.totalVerifiedLead) ? reportData.totalVerifiedLead : null+(reportData.totalVerifiedLead && reportData.totalContactedLead) ? ' ('+((reportData.totalVerifiedLead/reportData.totalContactedLead)*100)+'%)' : null;
            reportObj['Walkin Scheduled']=(reportData.totalWalkinSchd) ? reportData.totalWalkinSchd : null+(reportData.totalVerifiedLead && reportData.totalWalkinSchd) ? ' ('+((reportData.totalWalkinSchd/reportData.totalVerifiedLead)*100)+'%)' : null;
            reportObj['Walkin Done']=(reportData.totalWalkinDone) ? reportData.totalWalkinDone : null+(reportData.totalVerifiedLead && reportData.totalWalkinDone) ? ' ('+((reportData.totalWalkinDone/reportData.totalVerifiedLead)*100)+'%)' : null;
            reportObj['Conversion Assist']=(reportData.totalConversionAssist) ? reportData.totalConversionAssist : null+(reportData.totalVerifiedLead && reportData.totalConversionAssist) ? ' ('+((reportData.totalConversionAssist/reportData.totalVerifiedLead)*100)+'%)' : null;
            reportObj['Walk-in Assist']=(reportData.totalWalkingAssist) ? reportData.totalWalkingAssist : null+(reportData.totalVerifiedLead && reportData.totalWalkingAssist) ? ' ('+((reportData.totalWalkingAssist/reportData.totalVerifiedLead)*100)+'%)' : null;
            reportObj['Closed']=(reportData.totalClose) ? reportData.totalClose : null+(reportData.totalClose && reportData.totalLead) ? ' ('+((reportData.totalClose/reportData.totalLead)*100)+'%)' : null;
            reportObj['Location']=(reportData.totalLocVerifiedLead) ? reportData.totalLocVerifiedLead : null+(reportData.totalVerifiedLead && reportData.totalLocVerifiedLead) ? ' ('+((reportData.totalLocVerifiedLead/reportData.totalVerifiedLead)*100)+'%)' : null;
            reportObj['Language']=(reportData.totalLangVerifiedLead) ? reportData.totalLangVerifiedLead : null+(reportData.totalVerifiedLead && reportData.totalLangVerifiedLead) ? ' ('+((reportData.totalLangVerifiedLead/reportData.totalVerifiedLead)*100)+'%)' : null;
            LeadsList.push(reportObj);
            
        })
        this.setState({CsvJsonData:LeadsList})
    }
    

    render() {
        return (
            <div className="container-fluid">
                <div>
                    <div className="savefilterwrap">
                        <h1 className="mrg-rauto">{this.props.t('l1Calling.title')}</h1>
                        
                        {(this.state.CsvJsonData && this.state.CsvJsonData.length) ?          
                        <button type="button" className="btn btn-default btn-sm">
                        <CSVLink
                            data={this.state.CsvJsonData}
                            filename={"l1calling-report.csv"}
                            className="btn btn-primary"
                            target="_blank"
                            >{this.props.t('l1Calling.exportCsv')}
                        </CSVLink>
                        </button> :''}
                    </div>
                </div>
                <SearchL1Calling prefilledData={this.state.allFliterData} getReportData={this.getFilterReport} getSubSourceList={this.getSubSourceList}/>
                <L1CallingResult pageloading={this.state.pageloading} loading={this.state.loading} searchResult={this.state.reportData} 
                totalRecord={this.state.totalRecord} totalCalculationObject={this.state.totalCalculationObject}/>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    
});

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            getClusterList:bindActionCreators(
                LeadActions.getClusterList,
                dispatch
            )
            
    }   }
}

export default  withTranslation('report')(connect(mapStateToProps, mapDispatchToProps)(L1CallingList));
