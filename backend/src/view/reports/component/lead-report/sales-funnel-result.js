import React, { Component } from "react";
import { withTranslation } from 'react-i18next';

class SalesFunnelResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
        searchResult:props.searchResult,
        totalRecord:props.totalRecord,
        loading: false,
        pageloading: false,
        totalCalculationObject:props.totalCalculationObject
    }
  }
  static getDerivedStateFromProps(nextprops, prevState) {
    var updatedObj = {}
    if (prevState.searchResult !== nextprops.searchResult) {
      updatedObj['searchResult'] = nextprops.searchResult;
    }
    if (prevState.totalRecord !== nextprops.totalRecord) {
        updatedObj['totalRecord'] = nextprops.totalRecord;
    }
    if(nextprops.loading !== prevState.loading){
        updatedObj['loading'] = nextprops.loading;
    }

    if(nextprops.pageloading !== prevState.pageloading){
        updatedObj['pageloading'] = nextprops.pageloading;
    }
    if(nextprops.totalCalculationObject !== prevState.totalCalculationObject){
        updatedObj['totalCalculationObject'] = nextprops.totalCalculationObject;
    }
    return updatedObj;
}


  render() {
    const {totalCalculationObject} = this.state;
    return (
      <div className="result-wrap">
                    <div className="card">
                        <div className="card-heading">
                            <div className="row">
                                <div className="col-sm-6">
                                    {/* <h2 className="card-title pad-t7"> {this.state.totalRecord+' '+this.props.t('salesFunnel.resultsFound')}</h2> */}
                                </div>
                            </div>
                        </div>
                        <div className="table-responsive">
                            <table className="table table-bordered table-hover table-striped deliveryTable">
                                <thead>
                                    <tr>
                                        <th>{this.props.t('salesFunnel.cluster')}</th>
                                        <th>{this.props.t('salesFunnel.city')}</th>
                                        <th>{this.props.t('salesFunnel.source')}</th>
                                        <th>{this.props.t('salesFunnel.subSource')}</th>
                                        <th>{this.props.t('salesFunnel.category')}</th>
                                        <th>{this.props.t('salesFunnel.total')}</th>
                                        <th>{this.props.t('salesFunnel.unique')}</th>
                                        <th>{this.props.t('salesFunnel.eligibleForCalling')}</th>
                                        <th>{this.props.t('salesFunnel.dialed')}</th>
                                        <th>{this.props.t('salesFunnel.contacted')}</th>
                                        <th>{this.props.t('salesFunnel.verified')}</th>
                                        <th>{this.props.t('salesFunnel.walkinScheduled')}</th>
                                        <th>{this.props.t('salesFunnel.walkinCompleted')}</th>
                                        <th>{this.props.t('salesFunnel.converted')}*</th>
                                        <th>{this.props.t('salesFunnel.approvedConversions')}</th>
                                    </tr>
                                </thead>

                                <tbody>
                                {(this.state.searchResult.length === 0 && !this.state.loading) ? <tr><td align="center" colSpan="15"><h6 className="text-center text-danger text-bold">{this.props.t('salesFunnel.NoRecordFound')}</h6></td></tr> : null}
                            {(this.state.searchResult && this.state.searchResult.length !== 0) && this.state.searchResult.map((reportData, k) =>
                                    <tr key={k}>
                                        <td>{reportData.cluster_name}</td>
                                        <td>{reportData.city_name}</td>
                                        <td>{reportData.source_name}</td>
                                        <td>{reportData.sub_source_name}</td>
                                        <td>{reportData.category}</td>
                                        <td>{reportData.totalLead}</td>
                                        <td>{reportData.UniqueLead}</td>
                                        <td>
                                        {(reportData.eligibleDialerLead) ? reportData.eligibleDialerLead : ''}
                                            {(reportData.UniqueLead && reportData.eligibleDialerLead) ? ' ('+((reportData.eligibleDialerLead/reportData.UniqueLead)*100).toFixed(2)+'%)' : ''}</td>
                                        <td> {(reportData.dialedLead) ? reportData.dialedLead : ''}
                                            {(reportData.dialedLead && reportData.eligibleDialerLead) ?  ' ('+((reportData.dialedLead/reportData.eligibleDialerLead)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>{(reportData.totalContacted) ? reportData.totalContacted : ''}
                                            {(reportData.totalContacted && reportData.dialedLead) ? ' ('+((reportData.totalContacted/reportData.dialedLead)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>
                                        {(reportData.totalVerified) ? reportData.totalVerified : ''}
                                            {(reportData.totalVerified && reportData.totalContacted) ? ' ('+((reportData.totalVerified/reportData.totalContacted)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>
                                        {(reportData.totalWalinSchd) ? reportData.totalWalinSchd : ''}
                                            {(reportData.totalVerified && reportData.totalWalinSchd) ? '('+((reportData.totalWalinSchd/reportData.totalVerified)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>
                                        {(reportData.totalWalinDone) ? reportData.totalWalinDone : ''}
                                            {(reportData.totalWalinDone && reportData.totalWalinSchd) ? ' ('+((reportData.totalWalinDone/reportData.totalWalinSchd)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>
                                        {(reportData.totalConversion) ? reportData.totalConversion : ''}
                                            {(reportData.totalVerified && reportData.totalConversion) ? ' ('+((reportData.totalConversion/reportData.totalVerified)*100).toFixed(2)+'%)' : ''}</td>
                                        <td>{(reportData.totalConfConversion) ? (reportData.totalConfConversion) : ''}</td>
                                    </tr>
                                    )}
                                    {
                                        (this.state.loading) ? <tr><td className="loading" colSpan="15"></td></tr> : null
                                    }

                                    {(this.state.searchResult.length) ?
                                    <tr className="totalvalue">
                                        <td>{this.props.t('salesFunnel.total')}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalLead) ? totalCalculationObject.sumTotalLead : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumUniqueLeads) ? totalCalculationObject.sumUniqueLeads : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumEligibleDialerLead) ? totalCalculationObject.sumEligibleDialerLead : ''} { (totalCalculationObject.sumEligibleDialerLead && totalCalculationObject.sumUniqueLeads) ? '('+ ((totalCalculationObject.sumEligibleDialerLead / totalCalculationObject.sumUniqueLeads) * 100).toFixed(2) +')' : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumDialedLead) ? totalCalculationObject.sumDialedLead : ''} { (totalCalculationObject.sumDialedLead && totalCalculationObject.sumEligibleDialerLead) ? '('+((totalCalculationObject.sumDialedLead / totalCalculationObject.sumEligibleDialerLead) * 100).toFixed(2)+')' : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalContacted) ? totalCalculationObject.sumTotalContacted : ''} { (totalCalculationObject.sumTotalContacted && totalCalculationObject.sumDialedLead) ? '('+((totalCalculationObject.sumTotalContacted / totalCalculationObject.sumDialedLead) * 100).toFixed(2)+')' : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalVerified) ? totalCalculationObject.sumTotalVerified : ''} { (totalCalculationObject.sumTotalVerified && totalCalculationObject.sumTotalContacted) ? '('+((totalCalculationObject.sumTotalVerified / totalCalculationObject.sumTotalContacted) * 100).toFixed(2)+')' : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.SumTotalWalinSchd) ? totalCalculationObject.sumTotalWalinSchd : ''} {(totalCalculationObject.sumTotalWalinSchd && totalCalculationObject.sumTotalVerified) ? '('+((totalCalculationObject.sumTotalWalinSchd / totalCalculationObject.sumTotalVerified) * 100).toFixed(2)+')' : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalWalinDone) ? totalCalculationObject.sumTotalWalinDone : ''} {(totalCalculationObject.sumTotalWalinDone && totalCalculationObject.sumTotalWalinSchd) ? '('+((totalCalculationObject.sumTotalWalinDone / totalCalculationObject.sumTotalWalinSchd) * 100).toFixed(2)+')' : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalConversion) ? totalCalculationObject.sumTotalConversion : ''} {(totalCalculationObject.sumTotalConversion && totalCalculationObject.sumTotalVerified) ? '('+((totalCalculationObject.sumTotalConversion / totalCalculationObject.sumTotalVerified) * 100).toFixed(2)+')' : ''}</td>
                                        <td>{(totalCalculationObject && totalCalculationObject.sumTotalConfConversion) ? totalCalculationObject.sumTotalConfConversion : ''} </td>
                                    </tr> : null }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
    );
  }
}
export default withTranslation('report') (SalesFunnelResult);
