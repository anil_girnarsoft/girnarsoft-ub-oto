import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { withRouter } from "react-router-dom";

class Footer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      footerSettings: null
    };
  }

  static getDerivedStateFromProps = (nextProps, prevState) => {
    if(nextProps.siteSettings && !prevState.siteSettings ){
        return ({ footerSettings: nextProps.siteSettings.footer })
    }
    return null;
  }

  render() {
    const { footerSettings } = this.state;
    const currentPath = window.location.pathname;

    
          
        
              if(!currentPath.includes('walkin-reschedule') && !currentPath.includes('login') && !currentPath.includes('-password'))
              {
                return (<footer>
                  <div className="footer-container clearfix" id="footer">
                    <div className="pull-left">
                      <ul className="footer-dealer">
                        <li>
                        {
                          footerSettings && footerSettings.copyright
                          ?
                          <p><i className="fa fa-copyright" aria-hidden="true"></i> {footerSettings.copyright}</p>
                          :
                          ''
                        }
                        </li>
                      </ul>
                    </div>
                    <div className="pull-right">
                      {
                        footerSettings && footerSettings.phone
                        ?
                        <span className="f-image"><img src={require("./assets/images/phone.svg")} alt="call-img" /><span className="font-12 pad-L5">{footerSettings.phone}</span><span className="pad-L5 pad-R5">|</span></span>
                        :
                        ''
                      }
                      {
                        footerSettings && footerSettings.email
                        ?
                        <span className="f-image"><img src={require("./assets/images/email.svg")} alt="mail-img" /> <a href="mailto:someone@example.com?Subject=Hello%20again" target="_top">{footerSettings.email}</a></span>
                        :
                        ''
                      }
                    </div>
                  </div>
                </footer>)
              }else{
                return("");
              }
  }
}

const mapStateToProps = state => {
  return {
     siteSettings: state.auth.siteSettings
  }
}

export default withTranslation('common') (withRouter(connect(mapStateToProps)(Footer)));