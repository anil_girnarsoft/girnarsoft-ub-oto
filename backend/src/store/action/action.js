const ActionTypes = {
    CHANGE_CONTENT:'CHANGE_CONTENT',
    LOGIN_USER:'LOGIN_USER',
    LOGOUT_USER: 'LOGOUT_USER'
}
export default ActionTypes;