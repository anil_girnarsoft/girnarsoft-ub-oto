import React from 'react';
import { Route, Redirect } from "react-router-dom";
import secureStorage from '../config/encrypt';

const BeforeLogin = ({ component: Component, ...rest }) => {

    return (
      <Route
        {...rest}
        render={props =>
              <Component {...props} />
        }
      />
    );
};

export default BeforeLogin;