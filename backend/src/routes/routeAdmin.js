import React, { Component } from 'react';
import { Route } from 'react-router';
import DealerList from './../view/dealer/component/dealer-list';
import ResetAccountBlock from './../view/dealer/component/ResetAccountBlock';
// import NotFound from './../view/common/NotFound';

class AdminRoute extends Component {
    render() {
        const { match: { path } } = this.props;
        return (
            <div>
                <Route path={`${path}/dealer-list`} component={DealerList} />
                <Route path={`${path}/reset-account-block`} component={ResetAccountBlock} />
                {/* <Route path="/*"
                    component={NotFound} /> */}
            </div>
        );
    }
}

export default AdminRoute;